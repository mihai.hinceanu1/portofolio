export const environment = {
  production: true,
  DefaultUrlServer: 'https://wst.rompetrol.com/wst/',
  ImagePath: 'https://wst.rompetrol.com/img/'
};
