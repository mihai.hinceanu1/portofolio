import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { DataService } from '../service/data.service';
import { DxTabsComponent } from 'devextreme-angular';

@Component({
    selector: 'app-navbar',
    templateUrl: 'navbar.component.html',
    styleUrls: ['./navbar.css']
})

export class NavBarComponent {
    @ViewChild('navbarTabs') navbarTabs: DxTabsComponent;
    constructor(public ds: DataService, public router: Router, private activatedRoute: ActivatedRoute) {

        router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.updateNavActive();
            }
        });
        setTimeout(() => {
            let lastScrollTop = 0;
            document.getElementById('mainNavbar').addEventListener('scroll', function () {
                const st = window.pageYOffset || document.documentElement.scrollTop;
                if (st > lastScrollTop) {
                    // downscroll code
                    console.log('downscroll');
                } else {
                    // upscroll code
                    console.log('upscroll');
                }
                lastScrollTop = st;
            }, false);
        }, 0);
    }
    selectTab(e) {
        if (this.ds.navTabs[e.itemIndex].queryParam) {
            this.router.navigate([this.ds.navTabs[e.itemIndex].route], { queryParams: { type: this.ds.navTabs[e.itemIndex].queryParam } });
        } else {
            this.router.navigate([this.ds.navTabs[e.itemIndex].route]);
        }
    }

    contentReady(e) {
        this.updateNavActive();
        setTimeout(() => {
            e.component.focus();
        }, 300);
    }

    updateNavActive() {
        const nv = this.ds.navTabs.find(n => n.activeRoute === this.router.url || n.route === this.router.url);
        if (nv != null) {
            nv.active = true;
            this.navbarTabs.instance.option('selectedItem', nv);
            // this.navTabs.forEach(n => n.active = false);
            // nv.active = true;
        }
    }
}
