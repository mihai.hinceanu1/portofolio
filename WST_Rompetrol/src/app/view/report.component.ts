import { DataService } from '../service/data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
// import { DxListComponent } from 'index';

@Component({
    templateUrl: 'report.component.html',
})
export class ReportComponent {
    dxListInstDDBox: any;
    customClients: string;
    subnavTabs: any[];
    current: string;
    lastTab: string;
    tempDDBox: any;
    tempTTBox: any;
    dsTankType: any;
    dsStatusId: any;
    dsOrderStatusId: any;
    dsDepot: Array<any>;
    currentDate: string;

    dsReportDS: CustomStore;
    dsReportLS: CustomStore;
    dsReportDR: CustomStore;
    dsReportSS: CustomStore;
    dsReportRO: CustomStore;
    dsReportTU: CustomStore;
    dsReportVOL: CustomStore;
    dsReportLO: CustomStore;
    dsReportTR: CustomStore;
    dsReportRT: any;
    dsReportCC: CustomStore;
    dsReportAC: any;
    dsReportRA: any;
    dsReportDZ: any;
    dsTaxes: CustomStore;
    dsEta: CustomStore;
    dsCostCenter: CustomStore;

    @ViewChild('gridReportDS') gridReportDS: DxDataGridComponent;
    @ViewChild('gridReportLS') gridReportLS: DxDataGridComponent;
    @ViewChild('gridReportDR') gridReportDR: DxDataGridComponent;
    @ViewChild('gridReportSS') gridReportSS: DxDataGridComponent;
    @ViewChild('gridReportRO') gridReportRO: DxDataGridComponent;
    @ViewChild('gridReportTU') gridReportTU: DxDataGridComponent;
    @ViewChild('gridReportVOL') gridReportVOL: DxDataGridComponent;
    @ViewChild('gridReportLO') gridReportLO: DxDataGridComponent;
    @ViewChild('gridReportRT') gridReportRT: DxDataGridComponent;
    @ViewChild('gridReportCC') gridReportCC: DxDataGridComponent;
    @ViewChild('gridReportAC') gridReportAC: DxDataGridComponent;
    @ViewChild('gridReportRA') gridReportRA: DxDataGridComponent;
    @ViewChild('gridReportDZ') gridReportDZ: DxDataGridComponent;
    @ViewChild('gridReportTR') gridReportTR: DxDataGridComponent;
    @ViewChild('gridTax') gridTax: DxDataGridComponent;
    @ViewChild('gridEta') gridEta: DxDataGridComponent;

    filters: any;
    dsTransporter: any[];
    dsTransData: CustomStore;
    dsDepotData: CustomStore;
    displayMonth: (e: any) => void;
    dsRange: CustomStore;
    isRangePopupVisible: boolean;
    currentOrderVol: any;
    isCommentVisible: boolean;
    currentCommentDescription: string;
    newRangeId: any;

    constructor(public ds: DataService, private router: Router) {
        if (!ds.isAuth) {
            router.navigateByUrl('/login');
            return;
        }
        this.filters = {};
        this.tempDDBox = [];
        this.tempTTBox = [];
        for (const v of ['ds', 'vol', 'lo', 'rt', 'ac', 'ro', 'tu', 'dz', 'ls', 'ra', 'dr', 'ss', 'tr', 'tax', 'eta']) {
            this.filters[v] = this.createDefaultFilter();
        }

        this.currentDate = new Date().toLocaleDateString();
        this.dsCostCenter = this.ds.getCostCenter();
        this.subnavTabs = this.getSubnavs();
        this.dsTransData = this.ds.getTransporter();
        this.dsDepotData = this.ds.getDepot();
        this.dsTankType = ds.getMeterType();
        this.dsStatusId = ds.getTripStatusDescription();
        this.dsOrderStatusId = ds.getOrderStatusDescription();
        this.dsRange = ds.getRange();
        this.customClients = '{0} clients';

        this.ds.getPlainDepot().then((result) => {
            this.dsDepot = [];
            for (const it of result) {
                this.dsDepot.push(it.name);
            }
        });

        this.ds.getPlainTransporter().then((result) => {
            this.dsTransporter = [];
            for (const it of result) {
                this.dsTransporter.push(it.name);
            }
        });
        this.isRangePopupVisible = false;
        this.displayMonth = (e) => DataService.formatDateMonth(e);

        this.reloadData(sessionStorage.getItem('currentReportSubTab') || 'ds');
    }

    createDefaultFilter() {
        const result = {
            ransporterId: null,
            depotId: null,
            fromDate: new Date(),
            date: new Date(),
            thruDate: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1)
        };
        result.fromDate.setDate(1);

        return result;
    }

    onVolRowClick(e) {
        if (e.row && e.row.rowType === 'data' && e.column.dataField === 'r_value' && (!e.row.data.oldr_value || this.ds.isSupport())) {
            this.currentOrderVol = e.row.data;
            this.newRangeId = null;
            this.isRangePopupVisible = true;
        }
        if (e.row && e.row.rowType === 'data' && (e.column.dataField === 'cm_description' || e.column.dataField === 'ol_refNumber')) {
            this.currentOrderVol = e.row.data;
            this.isCommentVisible = true;
        }
    }

    onSaveNewRangeId(e: any, newRangeId: string) {
        this.ds.getOrderRangeDetails(this.currentOrderVol.ol_refNumber, this.currentOrderVol.r_value).then(data => {
            this.ds.updateModel('OrderLine', data[0]['f0'], { oldRangeId: data[0]['f1'], rangeId: newRangeId })
                .then(result => {
                    notify('Success', 'success', 3000);
                    this.gridReportVOL.instance.refresh();
                    this.isRangePopupVisible = false;
                }, (error) => {
                    this.ds.serverError(error);
                });
        });
    }

    onSaveCommonet(e: any) {
        this.ds.addEditComment({ ol_id: this.currentOrderVol.ol_id, description: this.currentOrderVol.cm_description }).then(val => {
            notify('Comment successfully added', 'success', 3000);
            this.gridReportVOL.instance.refresh();
            this.isCommentVisible = false;
        }, (error) => {
            this.ds.serverError(error);
        });
    }

    reloadData(e) {
        let status;
        if (this.ds.isTrans()) {
            // IMPORTANT de adaugat
            const vw = ['ds', 'ls', 'ss', 'tu', 'ro', 'vol', 'lo', 'ra', 'tax', 'eta'];
            const val = typeof e === 'string' ? e : e.itemData.activeRoute;
            const item = vw.find(x => x === val);
            if (!item) {
                e = 'ds';
            }
        }
        if (typeof e === 'string') {
            status = e;
        } else {
            status = this.subnavTabs[e.itemIndex].activeRoute;
        }
        this.current = status;
        this.initPage();
        sessionStorage.setItem('currentReportSubTab', status);
    }

    onVolRowPrepared(rowInfo) {
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-6') {
            rowInfo.rowElement.className += ' strike';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-7' &&
            (rowInfo.rowElement.className.indexOf('orderTransPen') < 0)) {
            rowInfo.rowElement.className += ' orderTransPen';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-8' &&
            (rowInfo.rowElement.className.indexOf('orderTransCan') < 0)) {
            rowInfo.rowElement.className += ' orderTransCan';
        }
    }

    initPage() {
        const filter = this.filters[this.current];
        if (this.lastTab !== this.current) {
            this.lastTab = this.current;
            this.tempDDBox = null;
            this.tempTTBox = null;
            if (this.filters[this.current]) { this.filters[this.current]['depotId'] = []; }
            if (this.filters[this.current]) { this.filters[this.current]['transporterId'] = []; }
        }
        switch (this.current) {
            case 'ds':
                this.dsReportDS = this.ds.getDSReport(filter);
                if (this.gridReportDS) { this.gridReportDS.instance.refresh(); }
                break;
            case 'ls':
                this.dsReportLS = this.ds.getLSReport(filter);
                if (this.gridReportLS) { this.gridReportLS.instance.refresh(); }
                break;
            case 'dr':
                this.dsReportDR = this.ds.getDRReport(filter.date);
                if (this.gridReportDR) { this.gridReportDR.instance.refresh(); }
                break;
            case 'ss':
                this.dsReportSS = this.ds.getSSReport(filter.transporterId, filter.date);
                if (this.gridReportSS) { this.gridReportSS.instance.refresh(); }
                break;
            case 'ro':
                this.dsReportRO = this.ds.getROReport(filter.fromDate, filter.thruDate);
                if (this.gridReportRO) { this.gridReportRO.instance.refresh(); }
                break;
            case 'tu':
                this.dsReportTU = this.ds.getTUReport(filter);
                if (this.gridReportTU) { this.gridReportTU.instance.refresh(); }
                break;
            case 'dz':
                this.ds.getDZReport(filter).then((result) => {
                    this.dsReportDZ = result;
                    if (this.gridReportDZ) { this.gridReportDZ.instance.refresh(); }
                });
                break;
            case 'ra':
                this.ds.getRAReport(filter).then(result => {
                    this.dsReportRA = result;
                    if (this.gridReportRA) { this.gridReportRA.instance.refresh(); }
                });
                break;
            case 'vol':
                this.dsReportVOL = this.ds.getVOLReport(filter);
                if (this.gridReportVOL) { this.gridReportVOL.instance.refresh(); }
                break;
            case 'lo':
                this.dsReportLO = this.ds.getLOReport(filter);
                if (this.gridReportLO) { this.gridReportLO.instance.refresh(); }
                break;
            case 'rt':
                this.ds.getRTReport(filter).then(result => {
                    this.dsReportRT = result;
                    if (this.gridReportRT) { this.gridReportRT.instance.refresh(); }
                });
                // this.dsReportRT = this.ds.getRTReport(filter);
                break;
            case 'cc':
                this.dsReportCC = this.ds.getCCReport();
                if (this.gridReportCC) { this.gridReportCC.instance.refresh(); }
                break;
            case 'ac':
                this.ds.getACReport(filter).then(result => {
                    this.dsReportAC = result;
                    if (this.gridReportAC) { this.gridReportAC.instance.refresh(); }
                });
                break;
            case 'tr':
                this.dsReportTR = this.ds.getTRReport(filter);
                if (this.gridReportTR) { this.gridReportTR.instance.refresh(); }
                break;
            case 'tax':
                this.dsTaxes = this.ds.getTaxes();
                if (this.gridTax) { this.gridTax.instance.refresh(); }
                break;
            case 'eta':
                this.dsEta = this.ds.getEta(filter);
                if (this.gridEta) { this.gridEta.instance.refresh(); }
                break;
        }
    }

    onInitTabs(e) {
        if (!this.current) { return; }
        const selIdx = this.getSubnavs().find(x => x.activeRoute === this.current).id;
        e.component.option('selectedIndex', selIdx);
    }

    onDelSchRowPrepared(rowInfo) {
        this.ds.alternateSoNumberColor(rowInfo, 't_refNumber');
    }

    onContentReady(e) {
        e.component.option('height', this.ds.customHeight());
    }

    shipToParty(rowData) {
        return rowData.ca_description + ', ' + rowData.ca_city + ', ' + rowData.ca_region;
    }

    calcTripOrder(rowData) {
        return 'CURSA ' + (rowData.t_orderIndex ? rowData.t_orderIndex : '0');
    }
    calcClientLoc(rowData) {
        return rowData.c_address + ', ' + rowData.ca_city + ', ' + rowData.ca_region;
    }

    calculateAmountEUR(rowData) {
        return (rowData.amount / rowData.exchangeRate).toFixed(2) || 0;
    }

    customTotalTxt() {
        return 'Grand total';
    }
    customTotalItem(itemInfo) {
        return itemInfo.value;
    }

    toolbarPreparing(event, type) {
        const filter = this.filters[type];

        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Clear filters', icon: 'filter', onClick: (e: any) => event.component.clearFilter() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                hint: 'Refresh data', icon: 'refresh',
                onClick: (e: any) => {
                    if (type === 'dz' || type === 'rt' || type === 'ra' || type === 'ac') {
                        this.initPage();
                    } else {
                        event.component.refresh();
                    }
                }
            },
            location: 'after'
        });
        switch (type) {
            case 'ds':
                this.addDateFilter(event, 'plannedDate', 'date', filter);
                this.addDropDownFilter(event, 'depotTemp', 'dropDownDepot');
                if (!this.ds.isTrans()) {
                    this.addDropDownFilter(event, 'transportTemp', 'dropDownTrans');
                }
                break;

            case 'vol':
            case 'lo':
            case 'rt':
            case 'ac':
            case 'ro':
                this.addDateFilter(event, 'thruDate', 'thruDate', filter);
                this.addDateFilter(event, 'fromDate', 'fromDate', filter);
                break;

            case 'tr':
                this.addDateFilter(event, 'thruDate', 'thruDate', filter);
                this.addDateFilter(event, 'fromDate', 'fromDate', filter);
                this.addDropDownFilter(event, 'transportTemp', 'dropDownTrans');
                break;

            case 'tu':
            case 'dz':
            case 'ra':
            case 'eta':
            case 'ls':
                this.addDateFilter(event, 'thruDate', 'thruDate', filter);
                this.addDateFilter(event, 'fromDate', 'fromDate', filter);
                this.addDropDownFilter(event, 'depotTemp', 'dropDownDepot');
                if (!this.ds.isTrans()) {
                    this.addDropDownFilter(event, 'transportTemp', 'dropDownTrans');
                }
                break;
            case 'dr':
                this.addDateFilter(event, 'ETADate', 'date', filter);
                break;

            case 'ss':
                this.addDateFilter(event, 'stockDate', 'date', filter);
                if (!this.ds.isTrans()) {
                    this.addDropDownFilter(event, 'transportTemp', 'dropDownTrans');
                }
                break;
        }
    }

    addSelectFilter(event, dataSource, template, field, filter, valueExpr = 'id') {
        event.toolbarOptions.items.unshift({
            widget: 'dxSelectBox',
            options: {
                width: '130px',
                showClearButton: true,
                dataSource: dataSource,
                valueExpr: valueExpr,
                displayExpr: 'name',
                onValueChanged: (e: any) => { filter[field] = e.value; this.initPage(); }
            },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            template: template,
            location: 'after'
        });
    }

    addDropDownFilter(event, template, templateDropDown) {
        event.toolbarOptions.items.unshift({
            template: templateDropDown,
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            template: template,
            location: 'after'
        });
    }

    onListDDB(e, field) {
        const filter = this.filters[this.current];
        if (e.addedItems.length > 0) {
            filter[field].push(e.addedItems[0]);
        }
        if (e.removedItems.length > 0) {
            filter[field] = filter[field].filter(x => x !== e.removedItems[0]);
        }
        if (field === 'depotId' && filter[field].length > 0) {
            this.tempDDBox = filter[field];
        }
        if (field === 'transporterId' && filter[field].length > 0) {
            this.tempTTBox = filter[field];
        }
        this.initPage();
    }

    onInitDxList(e: any, field: any) {
        this.dxListInstDDBox = e.component;
        this.filters[this.current][field] = [];
    }

    addDateFilter(event, template, field, filter) {
        event.toolbarOptions.items.unshift({
            widget: 'dxDateBox',
            options: {
                width: '110px',
                value: filter[field],
                displayFormat: 'dd.MM.yyyy',
                onValueChanged: (e: any) => {
                    filter[field] = e.value;
                    this.initPage();
                }
            },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            template: template,
            location: 'after'
        });
    }

    onDDBoxValChanged(e, field) {
        if (e.value == null) {
            this.filters[this.current][field] = [];
            this.dxListInstDDBox.unselectAll();
            this.initPage();
        }
    }

    orderInit(e, c) {
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getOrdersCustomer(c.data.customerCategory, this.filters['ra']));
        }
    }

    getSubnavs() {
        if (this.ds.isSales()) {
            return [{
                id: 0,
                text: 'Delivery Schedule',
                activeRoute: 'ds',
                active: false,
                icon: ''
            }, {
                id: 1,
                text: 'Loading Schedule',
                activeRoute: 'ls',
                active: false,
                icon: ''
            }];
        }
        if (this.ds.isTrans()) {
            const r = [{
                id: 0,
                text: 'Delivery Schedule',
                activeRoute: 'ds',
                active: false,
                icon: ''
            }, {
                id: 1,
                text: 'Loading Schedule',
                activeRoute: 'ls',
                active: false,
                icon: ''
            }, {
                id: 3,
                text: 'Stock Station',
                activeRoute: 'ss',
                active: false,
                icon: ''
            }, {
                id: 4,
                text: 'Truck Utilization',
                activeRoute: 'tu',
                active: false,
                icon: ''
            }, {
                id: 6,
                text: 'Rescheduled Orders',
                activeRoute: 'ro',
                active: false,
                icon: ''
            }, {
                id: 7,
                text: 'Rescheduled Analysis',
                activeRoute: 'ra',
                active: false,
                icon: ''
            }];
            if (this.ds.userType === 'transAdmin' || this.ds.userType === 'transRep') {
                r.push({
                    id: 8,
                    text: 'Volumes',
                    activeRoute: 'vol',
                    active: false,
                    icon: ''
                });
            }
            r.push({
                id: 9,
                text: 'Losses',
                activeRoute: 'lo',
                active: false,
                icon: ''
            });
            r.push({
                id: 10,
                text: 'ETA',
                activeRoute: 'eta',
                active: false,
                icon: ''
            });
            return r;
        }

        return [
            {
                id: 0,
                text: 'Delivery Schedule',
                activeRoute: 'ds',
                active: false,
                icon: ''
            },
            {
                id: 1,
                text: 'Loading Schedule',
                activeRoute: 'ls',
                active: false,
                icon: ''
            },
            {
                id: 2,
                text: 'Deliveries Report',
                activeRoute: 'dr',
                active: false,
                icon: ''
            },
            {
                id: 3,
                text: 'Stock Station',
                activeRoute: 'ss',
                active: false,
                icon: ''
            },
            {
                id: 4,
                text: 'Truck Utilization',
                activeRoute: 'tu',
                active: false,
                icon: ''
            }, {
                id: 5,
                text: 'Drop size',
                activeRoute: 'dz',
                active: false,
                icon: ''
            },
            {
                id: 6,
                text: 'Rescheduled Orders',
                activeRoute: 'ro',
                active: false,
                icon: ''
            },
            {
                id: 7,
                text: 'Rescheduled Analysis',
                activeRoute: 'ra',
                active: false,
                icon: ''
            }, {
                id: 8,
                text: 'Volumes',
                activeRoute: 'vol',
                active: false,
                icon: ''
            }, {
                id: 9,
                text: 'Losses',
                activeRoute: 'lo',
                active: false,
                icon: ''
            }, {
                id: 10,
                text: 'Rejected trips',
                activeRoute: 'rt',
                active: false,
                icon: ''
            }, {
                id: 11,
                text: 'Cost center',
                activeRoute: 'cc',
                active: false,
                icon: ''
            }, {
                id: 12,
                text: 'Active clients',
                activeRoute: 'ac',
                active: false,
                icon: ''
            }, {
                id: 13,
                text: 'Transfer',
                activeRoute: 'tr',
                active: false,
                icon: ''
            }, {
                id: 14,
                text: 'Tax',
                activeRoute: 'tax',
                active: false,
                icon: ''
            }, {
                id: 15,
                text: 'ETA',
                activeRoute: 'eta',
                active: false,
                icon: ''
            }];
    }
}
