import { DataService } from '../service/data.service';
import { Component, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DxDataGridComponent, DxTabsComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
import { confirm, alert } from 'devextreme/ui/dialog';

@Component({
    templateUrl: 'trip.component.html',
})
export class TripComponent implements OnDestroy {
    dsTrips: CustomStore;
    dsTruck: CustomStore;
    dsTank: CustomStore;
    dsDrivers: CustomStore;
    dsTransporter: CustomStore;
    dsDepot: CustomStore;
    dsMessages: any;

    current: string;
    customizeColumns: any;
    btAddTripInst: any;
    onTransporterCh: any;
    displayDriver: any;

    isRejectVisible: boolean;
    isTripVisible: boolean;

    pt: any = {}; t: any = {};
    orderGrids: any[] = [];
    statusBadge = { new: 0, pn: 0, ap: 0, sts: 0, pg: 0, fin: 0 };
    timerInterval: any;
    subnavTabs: any;
    currentTripDetails: any;
    calculateSummary: any;
    customLoadedQuant: any;
    @ViewChild('gridTrips') gridTrips: DxDataGridComponent;
    @ViewChild('navbarTabs') navbarTabs: DxTabsComponent;
    btSendSAPInst: any;
    constructor(public ds: DataService, private router: Router) {
        if (!ds.checkForUnauthorized('trip')) { return; }

        this.dsMessages = ds.getMessages();
        this.subnavTabs = this.getSubnavs();
        this.reloadData(sessionStorage.getItem('currentTripSubTab') || 'new');
        this.dsTransporter = ds.getTransporter();
        this.dsDepot = ds.getDepot();
        this.dsTrips = ds.getTrip(this.current);
        this.customizeColumns = (columns) => { this.customizeColumnsEv(columns, this); };
        this.onTransporterCh = (item) => { this.onTransporterChF(item, this); };
        this.displayDriver = this.displayDriverEvent;
        this.calculateSummary = (options) => { this.calculateSummaryEvent(options, this); };

        if (this.gridTrips) { this.gridTrips.instance.refresh(); }
        this.isRejectVisible = false;
        this.isTripVisible = false;
        this.updateBadge(true);

        this.timerInterval = setInterval(() => {
            this.updateBadge(false);
        }, 60000);

        this.customLoadedQuant = (itemInfo) => {
            let proc = Math.abs((this.currentTripDetails.tk_capacity - itemInfo.value) * 100 / this.currentTripDetails.tk_capacity);
            proc = proc === Infinity ? 0 : 100 - proc;
            return 'Loaded Quantity: ' + itemInfo.value + ' (' + Math.round(proc) + '%)';
        };
    }
    ngOnDestroy() {
        clearInterval(this.timerInterval);
        this.timerInterval = undefined;
    }
    // statusBadge = { new: 0, pn: 0, ap: 0, sts: 0, pg: 0 };
    updateBadge(reset) {
        this.ds.getTripsCount().then(data => {
            this.subnavTabs[0].badge = data.new.toString();
            this.subnavTabs[1].badge = data.pn.toString();
            this.subnavTabs[2].badge = data.ap.toString();
            this.subnavTabs[3].badge = data.sts.toString();
            if (!this.ds.isTrans() && !this.ds.allowCalor) {
                this.subnavTabs[4].badge = data.pg.toString();
                this.subnavTabs[5].badge = data.fin.toString();
            }
            this.checkBadges(data, reset);
        });
    }

    checkBadges(currentBadge, reset) {
        const badgesClone = JSON.parse(sessionStorage.getItem('currentBadgesTrip'));
        if (!badgesClone) { return; }
        this.subnavTabs.forEach((val, index) => {
            if (parseInt(val.badge, 0) !== parseInt(badgesClone[val.activeRoute], 0) && val.activeRoute !== 'rej') {
                val.icon = 'refresh';
            }
            if (reset && val.activeRoute === this.current) {
                val.icon = '';
                badgesClone[this.current] = parseInt(val.badge, 0);
                sessionStorage.setItem('currentBadgesTrip', JSON.stringify(badgesClone));
            }
        });
        if (this.navbarTabs) { this.navbarTabs.instance.repaint(); }
    }

    changeTripOrder(data, up: boolean) {
        // tslint:disable-next-line:max-line-length
        const condition = (data.t_orderIndex === 10 && up) ? data.t_orderIndex : ((data.t_orderIndex) ? data.t_orderIndex : 1) + (up ? 1 : -1);
        const newIndex = Math.max(1, condition);
        EntityStore.fromQuery(new EntityQuery('Trip')).update(data.t_id, { orderIndex: newIndex }).then((result) => {
            data.t_orderIndex = newIndex;
        });
    }

    changeOrder(data, up: boolean) {
        const dataToSend = {
            id: data.ol_id,
            index: data.ol_orderIndex == null ? 0 : data.ol_orderIndex,
            up: up,
            tripId: data.ol_tripId,
            userName: this.ds.userName
        };

        EntityStore.execute('ChangeOrderIndex', dataToSend).then((result) => {
            if (result === 'OK') {
                this.orderGrids[data.ol_tripId].refresh();
            }
        }, () => {
            notify('Error while changing position', 'error', 1500);
        });
    }

    private deleteOrder(data) {
        const result = confirm('Do you want to remove this order from trip?', 'Order remove');
        result.then((cfm) => {
            if (!cfm) { return; }

            EntityStore.execute('RemoveFromTrip', { id: data.ol_id, userName: this.ds.userName }).then((r) => {
                if (r !== 'ERROR') {
                    notify('Order has been deleted from trip', 'success', 1500);
                    this.orderGrids[data.ol_tripId].refresh();
                }
            });
        });
    }

    onInitTabs(e) {
        const selInd = this.getSubnavs().find(x => x.activeRoute === status).id;
        e.component.option('selectedIndex', selInd);
    }
    reloadData(e) {
        if (typeof e === 'string') {
            status = e;
        } else {
            status = this.subnavTabs[e.itemIndex].activeRoute;
        }
        this.current = status;
        this.dsTrips = this.ds.getTrip(status);
        if (this.gridTrips) { this.gridTrips.instance.clearFilter(); }
        if (this.btAddTripInst) { this.btAddTripInst.option('visible', status === 'new'); }
        if (this.btSendSAPInst) { this.btSendSAPInst.option('visible', status === 'ap'); }
        this.updateBadge(true);
        sessionStorage.setItem('currentTripSubTab', status);
    }

    orderInit(e, c) {
        this.currentTripDetails = c.data;
        this.orderGrids[c.data.t_id] = e.component;
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getOrdersForTrip(c.data.t_id));
        }
    }

    editTrip(data) {
        this.router.navigate(['/tripEdit'], { queryParams: { tripId: data.data.t_id } });
    }

    changeTripStatus(status, item) {
        this.pt.refNumber = item.data.t_refNumber;
        this.pt.id = item.data.t_id;
        this.pt.comments = '';
        this.pt.userName = this.ds.userName;

        if (status === 0 && !item.data.t_comments && item.data.t_statusId !== 3) {
            this.isRejectVisible = true;
            return;
        }
        if (status === -2) {
            confirm('Are you sure you whant to delete this trip ?', 'Confirm').then((val) => {
                if (val) {
                    this.sendTripStatus(status, item);
                }
            });
        } else {
            this.sendTripStatus(status, item);
        }
    }

    sendTripStatus(status, item) {
        EntityStore.execute('ChangeTripStatus', {
            ids: [item.data.t_id], status: status,
            userName: this.ds.userName, comments: this.pt.comments
        }).then((result) => {
            switch (result) {
                case 'OK':
                    notify('Success', 'success', 2000);
                    this.gridTrips.instance.refresh();
                    break;
                case 'NO_ORDERS':
                    notify('No orders on this trip. Can\'t be send to SAP.', 'error', 3000);
                    break;
                case 'ERROR':
                    notify('Data cannot be processed.Please try again later.', 'error', 3000);
                    break;
            }
        });
    }

    onSaveTrip(event) {
        const validation = event.validationGroup.validate();
        if (validation.isValid) {
            EntityStore.fromQuery(new EntityQuery('Trip')).insert(this.t).then((result) => {
                this.router.navigate(['tripEdit'], { queryParams: { tripId: result.id } });
            });
        }
    }

    cancelClick() {
        this.isRejectVisible = false;
        this.isTripVisible = false;
    }

    onSelectionChTrip(e) {
        this.btSendSAPInst.option('disabled', e.selectedRowsData.length === 0);
    }

    onRowTripPrepared(rowInfo) {
        if (rowInfo.rowType !== 'header' && rowInfo.data &&
            (rowInfo.data.t_statusId === 4 || (rowInfo.data.t_statusId === 2 && rowInfo.data.t_comments)) &&
            (rowInfo.rowElement.className.indexOf('orderSent') < 0)) {
            rowInfo.rowElement.className += ' orderSent';
        }
    }

    onRowOrderPrepared(rowInfo) {
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-7' &&
            (rowInfo.rowElement.className.indexOf('orderTransPen') < 0)) {
            rowInfo.rowElement.className += ' orderTransPen';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-8' &&
            (rowInfo.rowElement.className.indexOf('orderTransCan') < 0)) {
            rowInfo.rowElement.className += ' orderTransCan';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data &&
            (rowInfo.data.ol_deliveryDate) && (rowInfo.rowElement.className.indexOf('orderDeliv') < 0)) {
            rowInfo.rowElement.className += ' orderDeliv';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-6' &&
            (rowInfo.rowElement.className.indexOf('strike') < 0)) {
            rowInfo.rowElement.className += ' strike';
        }
        this.ds.alternateSoNumberColor(rowInfo, 'ol_refNumber');
    }

    displayDriverEvent(data) {
        return !data ? '' : (data.lastName + ' ' + data.firstName);
    }
    rejectClick() {
        if (this.pt.comments.length > 10) {
            EntityStore.execute('ChangeTripStatus',
                { ids: [this.pt.id], status: 0, userName: this.ds.userName, comments: this.pt.comments }).then((result) => {
                    notify('Success', 'success', 2000);
                    this.gridTrips.instance.refresh();
                    this.isRejectVisible = false;
                });
        } else {
            notify('Comments should be at least 10 letters', 'error', 1500);
        }
    }

    toolbarPreparing(event, showCheck: boolean) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Clear filters', icon: 'filter', onClick: (e: any) => event.component.clearFilter() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => { event.component.refresh(); this.updateBadge(true); } },
            location: 'after'
        });
        if (!this.ds.isSales()) {
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    text: 'Send to SAP', icon: 'runner', disabled: true, visible: this.current === 'ap', onClick: (e: any) => {
                        const trips = this.gridTrips.instance.getSelectedRowsData();
                        for (const tr of trips) {
                            this.changeTripStatus(3, { data: tr });
                        }
                    },
                    onInitialized: (e: any) => { this.btSendSAPInst = e.component; }
                },
                location: 'after'
            });
            if (showCheck) {
                event.toolbarOptions.items.unshift({
                    widget: 'dxButton',
                    options: {
                        icon: 'car', text: 'New trip', onClick: (e: any) => this.showNewTrip(),
                        visible: (this.current === 'new'),
                        onInitialized: (e: any) => { this.btAddTripInst = e.component; }
                    },
                    location: 'after'
                });
            }
        }
    }

    calculateSummaryEvent(options, that) {
        if (options.name === 'availableQuantSummary' && options.summaryProcess === 'start') {
            options.totalValue = that.currentTripDetails.tk_capacity;
        }
        if (options.name === 'availableQuantSummary' && options.summaryProcess === 'calculate') {
            options.totalValue -= options.value;
        }
    }
    customAvailableQuant(itemInfo) {
        return 'Available Quantity: ' + itemInfo.value;
    }
    onCellPrepared(e: any) {
        if (e.rowType === 'data' && e.column.dataField === 'ol_loadedQuantity' && e.data.ol_loadedQuantity) {
            if (Math.abs((e.data.ol_quantity - e.data.ol_loadedQuantity) * 100 / e.data.ol_quantity) > 10) {
                e.cellElement.classList.add('redbk');
            }
        }
        if (e.rowType === 'data' && e.column.dataField === 'ol_requestDate' && e.data.ol_oldRequestDate) {
            e.cellElement.classList.add('redbk');
        }
    }
    showNewTrip() {
        this.t = {
            plannedDate: new Date(),
            transporterId: this.ds.isTrans() ? this.ds.userId : null,
            disbleTransp: this.ds.isTrans() ? true : false,
            depotId: null,
            carId: null, tankId: null, driverId: null,
            statusId: 0,
            orderIndex: 1
        };
        if (this.ds.isTrans()) {
            this.dsTruck = this.ds.getCar(this.t.transporterId, true);
            this.dsTank = this.ds.getTank(this.t.transporterId, true);
            this.dsDrivers = this.ds.getDriver(this.t.transporterId, true);
        }
        this.isTripVisible = true;
    }
    onTransporterChF(data, that) {
        if (data.value && that.t.transporterId) {
            that.dsTruck = that.ds.getCar(that.t.transporterId, true);
            that.dsTank = that.ds.getTank(that.t.transporterId, true);
            that.dsDrivers = that.ds.getDriver(that.t.transporterId, true);
        }
    }

    customizeColumnsEv(columns: any[], that) {
        for (const c of columns) {
            if (c.cellTemplate === 'actions') {
                switch (that.current) {
                    case 'new':
                    case 'sts':
                        c.width = that.ds.isMob ? '70' : '50';
                        break;
                    case 'ap':
                        if (that.ds.isMob) {
                            c.width = that.ds.isTrans() ? '40' : '90';
                        } else {
                            c.width = that.ds.isTrans() ? '30' : '70';
                        }
                        break;
                    case 'pn':
                        c.width = that.ds.isMob ? '90' : '70';
                        break;
                }
            }
            if (c.cellTemplate === 'position') {
                const keys = ['new', 'pn'];
                if (!that.ds.isTrans()) {
                    keys.push('ap', 'sts');
                }
                c.visible = keys.indexOf(that.current) > -1;
            }

            if (c.dataField === 't_startDate') {
                c.visible = this.current === 'pg' || this.current === 'fin';
            }

            if (c.dataField === 't_plannedDate') {
                c.visible = this.current !== 'pg' && this.current !== 'fin';
            }
        }
    }

    getSubnavs() {
        const arr = [
            {
                id: 0,
                text: 'New',
                activeRoute: 'new',
                active: 'false',
                badge: this.statusBadge.new,
                icon: ''
            },
            {
                id: 1,
                text: 'Pending',
                activeRoute: 'pn',
                active: 'false',
                badge: this.statusBadge.pn,
                icon: ''
            },
            {
                id: 2,
                text: 'Approved',
                activeRoute: 'ap',
                active: 'false',
                badge: this.statusBadge.ap,
                icon: ''
            },
            {
                id: 3,
                text: 'Sent to SAP',
                activeRoute: 'sts',
                active: 'false',
                badge: this.statusBadge.sts,
                icon: ''
            },
            {
                id: 4,
                text: 'In progress',
                activeRoute: 'pg',
                active: 'false',
                badge: this.statusBadge.pg,
                icon: ''
            },
            {
                id: 5,
                text: 'Finished',
                activeRoute: 'fin',
                active: 'false',
                badge: this.statusBadge.fin,
                icon: ''
            }
        ];
        if (this.ds.isTrans() && this.ds.allowCalor) {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i].activeRoute === 'pg' || arr[i].activeRoute === 'fin') {
                    arr.splice(i, 1);
                    i--;
                }
            }
        }
        return arr;
    }
}
