import { DataService } from '../service/data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { DxButtonComponent, DxTextBoxComponent, DxLoadPanelComponent, DxValidatorComponent } from 'devextreme-angular';
import { EntityStoreOptions, EntityStore } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
import { not } from '@angular/compiler/src/output/output_ast';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.css']
})
export class LoginComponent {

    isLoadPanelVisible: boolean;
    userName: string; password: string;
    @ViewChild('txtPassword') txtPassword: DxTextBoxComponent;
    constructor(public ds: DataService, private http: Http) {
        this.ds.logout();
        this.isLoadPanelVisible = false;
    }

    onKeyPress(e, type?) {
        if (e.event.charCode === 13) {
            if (type === 'us') {
                this.txtPassword.instance.focus();
            } else {
                document.getElementById('btLogin').focus();
                document.getElementById('btLogin').click();
            }
        }
    }

    loginClick($event) {
        if (!this.userName || !this.password) {
            notify('Invalid data fields', 'error', 3000);
            return;
        }
        const json = JSON.stringify({ userName: this.userName, password: this.password });
        const headers = new Headers();
        const options = new RequestOptions({ headers: headers });
        this.http.post(EntityStoreOptions.DefaultServer + 'Login', json, options).subscribe(
            data => {
                const response: string = data.json();
                if (response.startsWith('driver')) {
                    notify('Drivers are not allowed to login!', 'error', 1000);
                    return;
                }

                if (data.status === 200 && response !== 'error' && response !== 'invalid') {
                    notify('Login success!', 'success', 1000);
                    this.ds.login(response);
                } else if (response === 'invalid') {
                    // tslint:disable-next-line:max-line-length
                    EntityStore.execute('ChangePassword', { userName: this.userName, currentPass: '123456', newPass: this.password }).then((result) => {
                        if (result === 'OK') {
                            this.loginClick($event);
                        } else {
                            notify('User or password are invalid!', 'error', 1000);
                        }
                    });
                } else {
                    notify('User or password are invalid!', 'error', 1000);
                }
            },
            error => {
                console.log(error);
                notify('Login error!', 'error', 1000);
            }
        );
    }
}
