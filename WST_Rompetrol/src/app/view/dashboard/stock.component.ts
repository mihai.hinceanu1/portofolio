import { Component, ViewChild, OnInit } from "@angular/core";
import { DxDataGridComponent } from "devextreme-angular";
import { DataService } from "../../service/data.service";
import CustomStore from 'devextreme/data/custom_store';

@Component({
    selector: 'app-stock',
    templateUrl: './stock.component.html',
    styleUrls: ['dashboard.component.css']
})

export class StockStationComponent implements OnInit {

    @ViewChild('gridST') gridST: DxDataGridComponent;
    dsStock: CustomStore;
    currentRow: any;
    checkStock: (options: any) => boolean;
    gridHeight: number;
    constructor(public ds: DataService) { }

    ngOnInit() {
        this.gridHeight = window.innerHeight - 113;
        const yes = new Date();
        yes.setDate(yes.getDate() - 1);
        this.dsStock = this.ds.getStock(this.ds.isStManager() ? this.ds.userId : null, yes);

        this.checkStock = (options) => {
            if (options.value < 0 || this.currentRow.st_capacity < options.value) {
                options.rule.message = 'The stock can\'t be greater then tank capacity or negative.';
                return false;
            } else {
                return true;
            }
        };
    }

    toolbarPreparingStation(event) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => event.component.refresh() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxDateBox',
            options: {
                width: '110px',
                value: new Date(new Date().setDate(new Date().getDate() - 1)),
                max: new Date(new Date().setDate(new Date().getDate() - 1)),
                displayFormat: 'dd/MM/yyyy',
                onValueChanged: (e: any) => { this.onStockDateChanged(e, this); }
            },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            template: 'stockDate',
            location: 'after'
        });
    }

    onStockUpdate(e) {
        if (e.row && e.row.data) {
            this.currentRow = e.row.data;
        }
    }

    onCellPrepared(e) {
        if (e.rowType === 'data' && (e.column.dataField === 'ts_quantity' || e.column.dataField === 'ts_variation')) {
            if (!e.value) {
                e.cellElement.classList.add('redbk');
            }
        }
    }

    onStockDateChanged(e, that) {
        that.dsStock = that.ds.getStock(that.ds.isStManager() ? that.ds.userId : null, e.value);
    }
}