import { Component } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
    selector: 'app-station-overview',
    templateUrl: './station-overview.component.html',
    styleUrls: ['dashboard.component.css']
})

export class StationOverviewComponent {
    dsProduct: any;
    dsTransporter: any;
    dsDepot: any;
    gridDs: any[];
    dsGridData: any;
    dsCarDelivery: { stockDate: string; stockDays: number; }[];
    filterData: any;
    dsSite: any;
    dsTanks: any;
    onFilterChanged: any;
    localFilterSave: any;
    dsStocks: any;

    constructor(public ds: DataService) {
        this.filterData = { };
        this.ds.checkForUnauthorized('site');
        this.dsProduct = this.ds.getProduct();
        this.dsTransporter = this.ds.getTransporter();
        this.dsDepot = this.ds.getDepot();
        this.gridDs = this.getgridDs();
        this.dsSite = this.ds.getStation();
        this.dsTanks = this.ds.getTankName();

        this.onFilterChanged = this.onFilterChangedEv.bind(this);
        this.localFilterSave = this.localFilterSaveEv.bind(this);
        this.onFilterChangedEv();
    }

    localFilterSaveEv() {
        localStorage.setItem('searchFilter', JSON.stringify(this.filterData));
    }

    onFilterChangedEv() {
        this.dsStocks = [];

        this.ds.getStationFilteredData(this.filterData, 'StockStation').then((result: any) => {
            this.dsGridData = result.data;
            for (const item of result.data) {
                const m = item.p_name;
                const u = this.dsStocks.find(x => x.arg === m);

                if (u) {
                    u.val += item.stockDays;
                } else {
                    this.dsStocks.push({ arg: item.p_name, val: item.stockDays});
                }
            }
        });
    }

    stocksTooltip = (args: any) => {
        console.log(args);
        return {
            html: '<h4 style="margin: 0">' + 'Product: ' + args.argument + '</h4><br>' +
                '<h4 style="margin: 0">' + 'Stock: ' + args.value + ' [l]' + '</h4>'
        };
    }

    getgridDs() {
        return [{
            id: 1,
            stockDays: '< 7 days',
            sites: 8,
            color: 'orange'
        }, {
            id: 2,
            stockDays: '< 3 days',
            sites: 15,
            color: 'orange'
        }, {
            id: 3,
            stockDays: '< 1 day',
            sites: 30,
            color: 'red'
        }];
    }
    getDelivered() {
        return [{
            arg: 1950,
            val: 2525778669
        }, {
            arg: 1960,
            val: 3026002942
        }, {
            arg: 1970,
            val: 3691172616
        }, {
            arg: 1980,
            val: 4449048798
        }, {
            arg: 1990,
            val: 5320816667
        }, {
            arg: 2000,
            val: 6127700428
        }, {
            arg: 2010,
            val: 6916183482
        }];
    }

    getUnitCost() {
        return [{
            arg: 'USA',
            val: 59.8,
        }, {
            arg: 'China',
            val: 74.2,
        }, {
            arg: 'Russia',
            val: 40,
        }, {
            arg: 'Japan',
            val: 22.6,
        }, {
            arg: 'India',
            val: 19,
        }, {
            arg: 'Germany',
            val: 6.1,
        }, {
            arg: 'USA',
            val: 59.8,
        }, {
            arg: 'China',
            val: 74.2,
        }, {
            arg: 'Russia',
            val: 40,
        }, {
            arg: 'Japan',
            val: 22.6,
        }, {
            arg: 'India',
            val: 19,
        }, {
            arg: 'Germany',
            val: 6.1,
        }];
    }

    getReschedule() {
        return [{
            arg: 'USA',
            val: 582,
        }, {
            arg: 'China',
            val: 35.1,
        }, {
            arg: 'Russia',
            val: 361.8,
        }, {
            arg: 'Japan',
            val: 64.9,
        }, {
            arg: 'India',
            val: 28.9,
        }, {
            arg: 'Germany',
            val: 77.3,
        }, {
            arg: 'USA',
            val: 582,
        }, {
            arg: 'China',
            val: 35.1,
        }, {
            arg: 'Russia',
            val: 361.8,
        }, {
            arg: 'Japan',
            val: 64.9,
        }, {
            arg: 'India',
            val: 28.9,
        }, {
            arg: 'Germany',
            val: 77.3,
        }];
    }

    getStockDays() {
        return [{
            arg: 21,
            val: 937.6
        }, {
            arg: 22,
            val: 308.6
        }, {
            arg: 23,
            val: 128.5
        }, {
            arg: 24,
            val: 241.5
        }, {
            arg: 25,
            val: 119.3
        }, {
            arg: 26,
            val: 123.6
        }, {
            arg: 21,
            val: 937.6
        }, {
            arg: 22,
            val: 308.6
        }, {
            arg: 23,
            val: 128.5
        }, {
            arg: 24,
            val: 241.5
        }, {
            arg: 25,
            val: 119.3
        }, {
            arg: 26,
            val: 123.6
        }];
    }
}
