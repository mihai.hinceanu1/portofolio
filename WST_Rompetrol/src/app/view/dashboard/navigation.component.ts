import { Component } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-dashboard-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['dashboard.component.css']
})

export class DashboardNavigationComponent {
    menuItems: { text: string; icon: string; path: string; }[];

    constructor(public ds: DataService, public router: Router) {
        this.menuItems = this.getMenuItems();
    }

    getMenuItems() {
        if (this.ds.isStManager() || this.ds.isTrans()) {
            return [{
                text: 'Stock',
                icon: 'home',
                path: '/dashboard/stock'
            }];
        }
        return [{
            text: 'Overview',
            icon: 'globe',
            path: '/dashboard/overview',
        }, {
            text: 'Metrics',
            icon: 'columnfield',
            path: '/dashboard/metrics'
        }, {
            text: 'Station',
            icon: 'home',
            path: '/dashboard/station'
        }, {
            text: 'Stock',
            icon: 'fields',
            path: '/dashboard/stock'
        }];
    }

    onItemClick(e: any) {
        const path = e.itemData.path;
        if (path) {
            this.router.navigateByUrl(path);
        }
    }

    updateSelection(e: any) {
        const nodeClass = 'dx-treeview-node';
        const selectedClass = 'dx-state-selected';
        const leafNodeClass = 'dx-treeview-node-is-leaf';
        const element: HTMLElement = e.element;

        const rootNodes = element.querySelectorAll(`.${nodeClass}:not(.${leafNodeClass})`);
        Array.from(rootNodes).forEach(node => {
            node.classList.remove(selectedClass);
        });

        let selectedNode = element.querySelector(`.${nodeClass}.${selectedClass}`);
        while (selectedNode && selectedNode.parentElement) {
            if (selectedNode.classList.contains(nodeClass)) {
                selectedNode.classList.add(selectedClass);
            }

            selectedNode = selectedNode.parentElement;
        }
    }
}
