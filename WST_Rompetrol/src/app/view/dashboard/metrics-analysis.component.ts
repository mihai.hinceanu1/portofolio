import { Component } from "@angular/core";
import { DataService } from "../../service/data.service";

@Component({
    selector: 'app-metrics-analysis',
    templateUrl: './metrics-analysis.component.html',
    styleUrls: ['dashboard.component.css']
})

export class MetricsAnalysisComponent {
    dsCompared: any;
    compareToPast: boolean;
    dsGraphData: any;
    dsCompanyDescription: any;
    gridDs: any[];
    dsGridData: any[];
    dsProduct: any;
    dsTransporter: any;
    dsDepot: any;
    filterData: { dateRange: Date; };
    dsMeasure: string[];
    dsAnalyze: string[];

    constructor(public ds: DataService) {
        this.ds.checkForUnauthorized('metrics');
        this.dsProduct = this.ds.getProduct();
        this.dsTransporter = this.ds.getTransporter();
        this.dsDepot = this.ds.getDepot();
        this.dsCompared = this.getCompareToLy();
        this.compareToPast = false;
        this.dsGraphData = this.getGraphData();
        this.dsCompanyDescription = this.getCompanyDescription();
        this.gridDs = this.getgridDs();
        this.dsGridData = this.getMetricsData();
        this.dsMeasure = ["Unit Cost", "Loss (truck balance/type of truck)", 'Total cost', 'Cost comparasion', 'Reschedule', 'No of orders',
            'No of transfers', 'No of orders received after 17:00', 'No of orders <3mc & 5mc', 'No of trucks used', 'Volumes deliv (clients/stations)',
            'Estimated km', 'Vol evolution per range'];
        this.dsAnalyze = ["Transporter", "Depot"];
        this.filterData = { dateRange: new Date() }
    }

    getMetricsData() {
        return [{
            id: 1,
            truck: 'B82FVZ/CT92HZD',
            driver: 'Marian Marius',
            lossProc: -0.92,
            lossLit: -42,
            deliveries: 22,
            quantity: 35091,
            trailerType: 'Flowmeter'
        }, {
            id: 2,
            truck: 'B12FKZ/BV92HZD',
            driver: 'Vasile Costel',
            lossProc: -0.92,
            lossLit: -42,
            deliveries: 22,
            quantity: 35091,
            trailerType: 'Flowmeter'
        }, {
            id: 3,
            truck: 'B02FKZ/BC92HZD',
            driver: 'Ion Marius',
            lossProc: 0.32,
            lossLit: -42,
            deliveries: 8,
            quantity: 35091,
            trailerType: 'Dipstick'
        }, {
            id: 4,
            truck: 'B32FKZ/GL92HZD',
            driver: 'Marcel Ionel',
            lossProc: 1.92,
            lossLit: -142,
            deliveries: 6,
            quantity: 35091,
            trailerType: 'Flowmeter'
        }, {
            id: 5,
            truck: 'B42FKZ/B92HZD',
            driver: 'Emil Marian',
            lossProc: 0.12,
            lossLit: -12,
            deliveries: 4,
            quantity: 35091,
            trailerType: 'Dipstick'
        }, {
            id: 6,
            truck: 'B62FKZ/B92HZD',
            driver: 'Holescu Mihai',
            lossProc: 3,
            lossLit: -200,
            deliveries: 9,
            quantity: 35091,
            trailerType: 'Flowmeter'
        }]
    }

    getgridDs() {
        return [{
            id: 1,
            loss: "<0.7%",
            cases: 18,
            progress: 72,
            color: 'red'
        }, {
            id: 2,
            loss: "0.7%-0.3%",
            cases: 15,
            progress: 60,
            color: 'orange'
        }, {
            id: 3,
            loss: "0.3%-0.7%",
            cases: 30,
            progress: 120,
            color: 'orange'
        }, {
            id: 4,
            loss: ">0.7%",
            cases: 9,
            progress: 36,
            color: 'red'
        }]
    }

    getCompanyDescription() {
        return [
            { value: "company1", name: "LCT" },
            { value: "company2", name: "TRANSOLUT" },
            { value: "company3", name: "TRANSPECO" },
            { value: "company4", name: "TREIRO" },
        ];
    }

    getGraphData() {
        return [{
            week: "w5",
            company1: 25000,
            company2: 25300,
            company3: 24700,
            company4: 24900,
        }, {
            week: "w6",
            company1: 25300,
            company2: 24800,
            company3: 24200,
            company4: 25000,
        }, {
            week: "w7",
            company1: 25600,
            company2: 24400,
            company3: 24900,
            company4: 24400,

        }, {
            week: "w8",
            company1: 26000,
            company2: 24000,
            company3: 24400,
            company4: 24900,

        }, {
            week: "w9",
            company1: 25400,
            company2: 24600,
            company3: 24800,
            company4: 24600,
        }, {
            week: "w10",
            company1: 24300,
            company2: 25100,
            company3: 24500,
            company4: 24300,

        }];
    }
    getCompareToLy() {
        return [{
            company: "LCT",
            present: 130000,
            past: 160000,
        }, {
            company: "TRANSOLUT",
            present: 250000,
            past: 210000,
        }, {
            company: "TRANSPECO",
            present: 398000,
            past: 300000,
        }, {
            company: "TREIRO",
            present: 80000,
            past: 95000,
        }];
    }
}