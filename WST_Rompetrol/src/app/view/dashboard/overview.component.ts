import { Component, ViewChild } from '@angular/core';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import { DataService } from '../../service/data.service';
import { DxChartComponent } from 'devextreme-angular';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['dashboard.component.css']
})

export class OverviewComponent {
    dsProduct: any;
    dsTransporter: any;
    dsDepot: any;
    centerMap: any;
    markersArray: any[];
    filterData: any;
    graphNo: any;
    onFilterChanged: any;
    localFilterSave: any;

    @ViewChild('qChart') qChart: DxChartComponent;

    dsDelivered: { arg: any; year: any; val: any; }[];
    dsUnitCost: { arg: any; year: any; val: any; tones: number }[];
    dsReschedule: { arg: any; year: any; val: any; }[];
    dsStockDays: { arg: number; val: number; }[];
    invalidDate: any;

    constructor(public ds: DataService, public http: HttpClient) {
        this.initFilterData();

        this.graphNo = [];
        this.ds.checkForUnauthorized('overview');
        this.dsProduct = this.ds.getProduct(true);
        this.dsTransporter = this.ds.getTransporter(true);
        this.dsDepot = this.ds.getDepot(true);
        this.markersArray = [];
        this.dsStockDays = this.getStockDays();
        this.centerMap = { lat: 46.032897, lng: 24.808611 };

        this.onFilterChanged = this.onFilterDataChangedEv.bind(this);
        this.localFilterSave = this.localFilterSaveEv.bind(this);
        this.invalidDate = this.invalidDateEv.bind(this);
        this.onFilterDataChangedEv();
    }

    initFilterData() {
        this.filterData = {};
        const date = new Date();
        if (localStorage.getItem('searchFilter')) {
            this.filterData = JSON.parse(localStorage.getItem('searchFilter'));
            if (!this.filterData.thruDate) {
                this.filterData.thruDate = date;
            }
        } else {
            this.filterData.thruDate = date;
        }
    }

    invalidDateEv(event: any) {
        console.log(event);
    }

    localFilterSaveEv() {
            localStorage.setItem('searchFilter', JSON.stringify(this.filterData));
    }

    onFilterDataChangedEv() {
        this.dsDelivered = [];
        this.dsUnitCost = [];
        this.dsReschedule = [];

        this.graphNo.qDelivered = 0;
        this.graphNo.uCost = 0;
        this.graphNo.tCost = 0;
        this.graphNo.resOrders = 0;
        this.graphNo.reschedule = 0;
        this.graphNo.totResOrd = 0;

        this.ds.getOverviewHeaderData(this.filterData, 'unitCost').then((result) => {
            for (const item of result.data) {
                this.graphNo.qDelivered += item.tones;
                const cpd = DataService.formatDate(item.t_plannedDate);
                const d = this.dsDelivered.find(x => x.arg === cpd);

                this.graphNo.tCost += (item.transpCost * 1.12); // 1970.17
                this.graphNo.uCost = this.graphNo.tCost / this.graphNo.qDelivered;
            }

            this.graphNo.qDelivered = Math.round(this.graphNo.qDelivered * 100) / 100;
            this.graphNo.uCost = Math.round(this.graphNo.uCost * 100) / 100;
            this.graphNo.tCost = Math.round(this.graphNo.tCost * 100) / 100;
        });

        // functia aduce de pe OrderLineView toate comenzile, dupa care le selectez doar pe cele cu statusId = -3 (reschedule)
            this.ds.getOverviewHeaderData(this.filterData, 'reschedule').then((result) => {
            this.graphNo.totResOrd = result.totalCount;
            if (result.totalCount !== 0) {
                const arr = [];
                for (const item of result.data) {
                    if (item.ol_statusId === '-3') {
                        arr.push(item);
                    }
                }
                this.graphNo.reschedule = (arr.length / result.totalCount) * 100;
                this.graphNo.reschedule = Math.round(this.graphNo.reschedule * 100) / 100;
                this.graphNo.resOrders = arr.length;
            } else {
                this.graphNo.reschedule = 0;
            }
        });

        this.ds.getOverviewChartData(this.filterData.thruDate, 'unitCost').then((result) => {
            for (const item of result.data) {
                const m = new Date(item.t_plannedDate).getMonth();
                this.graphNo.year = new Date(item.t_plannedDate).getFullYear();
                const u = this.dsDelivered.find(x => x.arg === m);
                const v = this.dsUnitCost.find(x => x.arg === m);

                if (u || v) {
                    v.val += item.transpCost * 1.12;
                    u.val += item.tones;
                    u.val = Math.round(u.val * 100) / 100;
                    v.val = Math.round(v.val * 100) / 100;
                    v.tones = u.val;
                } else {
                    if (item.tones === null) { item.tones = 0; }
                    this.dsDelivered.push({ arg: m, year: this.graphNo.year, val: Math.round(item.tones * 100) / 100 });
                    this.dsUnitCost.push({
                        arg: m, year: this.graphNo.year, val: Math.round((item.transpCost * 1.12) * 100) / 100,
                        tones: Math.round(item.tones * 100) / 100
                    });
                }
            }

            for (const data of this.dsUnitCost) {
                if (data.tones !== 0) {
                    data.val = data.val / data.tones;
                    data.val = Math.round(data.val * 100) / 100;
                }
            }
        });

            this.ds.getOverviewChartData(this.filterData.thruDate, 'reschedule').then((result) => {
            for (const item of result.data) {
                const m = new Date(item.ol_requestDate).getMonth();
                this.graphNo.resyear = new Date(item.ol_requestDate).getFullYear();
                const u = this.dsReschedule.find(x => x.arg === m);

                if (u) {
                    this.graphNo.totalCount ++;
                    u.val = this.graphNo.totalCount;
                } else {
                    this.graphNo.totalCount = 1;
                    this.dsReschedule.push({ arg: m, year: this.graphNo.resyear, val: this.graphNo.totalCount });
                }
            }
        });
    }

    qDeliveryTooltip = (args: any) => {
        return {
            html: '<h4 style="margin: 0">' + 'Month: ' + args.argument + '.' + this.graphNo.year + '</h4><br>' +
                '<h4 style="margin: 0">' + 'Tones: ' + args.value + '</h4>'
        };
    }

    unitCostTooltip = (args: any) => {
        return {
            html: '<h4 style="margin: 0">' + 'Month: ' + args.argument + '.' + this.graphNo.year + '</h4><br>' +
                '<h4 style="margin: 0">' + '[$/to]: ' + args.value + '</h4>'
        };
    }

    rescheduleTooltip = (args: any) => {
        return {
            html: '<h4 style="margin: 0">' + 'Month: ' + args.argument + '.' + this.graphNo.year + '</h4><br>' +
                '<h4 style="margin: 0">' + 'Reschedule: ' + args.value + '</h4>'
        };
    }

    mapInit(e: any) {
        const trs = ['TREIRO', 'TRANSPECO', 'LCT', 'TRANSOLUT', 'Depots', 'Stations'];
        const icon = ['pin_blue', 'pin_yellow', 'pin_green', 'pin_red', 'pin_rafinarie', 'pin_logo'];
        for (let i = 0; i < trs.length; i++) {
            this.setMarkersFor(trs[i], i, icon);
        }
        setTimeout(() => {
            const mk = this.markersArray[0];
            for (const mt of this.markersArray[4]) {
                mk.push(mt);
            }
            for (const mt of this.markersArray[5]) {
                mk.push(mt);
            }
            for (const mt of this.markersArray[1]) {
                mk.push(mt);
            }
            for (const mt of this.markersArray[2]) {
                mk.push(mt);
            }
            for (const mt of this.markersArray[3]) {
                mk.push(mt);
            }
            e.component.option('markers', mk);
        }, 3000);
    }

    setMarkersFor(transporter: string, index: number, icons: string[]) {
        let q = new EntityQuery('CustomerAddress').neq('coordX', null).neq('coordY', null);
        q.distinct = true;
        if (transporter === 'Depots') {
            q = new EntityQuery('Depot');
        } else if (transporter === 'Stations') {
            q.link('id', 'addressId', new EntityQuery('Station'));
        } else {
            q.link('id', 'addressId', new EntityQuery('OrderLine').linkEq('transporterId', 'id', 'Transporter', 'name', transporter));
        }
        EntityStore.fromQuery(q).load().then(result => {
            if (result && result.data && result.data.length > 0) {
                this.markersArray[index] = [];
                for (const client of result.data) {
                    const tt = '<p>' + client.code + '</p>' + (client.description ? ('<p>' + client.description + '</p>') : '');
                    this.markersArray[index].push({
                        location: client.coordX + ', ' + client.coordY,
                        iconSrc: 'assets/img/' + icons[index] + '.png',
                        tooltip: { text: tt, isShown: false }
                    });
                }
            }
        });
    }

    getStockDays() {
        return [{
            arg: 21,
            val: 937.6
        }, {
            arg: 22,
            val: 308.6
        }, {
            arg: 23,
            val: 128.5
        }, {
            arg: 24,
            val: 241.5
        }, {
            arg: 25,
            val: 119.3
        }, {
            arg: 26,
            val: 123.6
        }, {
            arg: 21,
            val: 937.6
        }, {
            arg: 22,
            val: 308.6
        }, {
            arg: 23,
            val: 128.5
        }, {
            arg: 24,
            val: 241.5
        }, {
            arg: 25,
            val: 119.3
        }, {
            arg: 26,
            val: 123.6
        }];
    }
}
