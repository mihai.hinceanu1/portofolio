import { DataService } from '../service/data.service';
import { Component, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DxDataGridComponent, DxTabsComponent, DxMapComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
import { alert } from 'devextreme/ui/dialog';

@Component({
    templateUrl: 'order.component.html',
})
export class OrderComponent implements OnDestroy {
    dsOrders: CustomStore;
    dsCustomer: CustomStore;
    dsTransporter: CustomStore;
    dsProduct: CustomStore;
    dsOrderType: CustomStore;
    dsDepot: CustomStore;
    dsTruck: CustomStore;
    dsTank: CustomStore;
    dsDrivers: CustomStore;
    dsTripTransfer: CustomStore;
    dsMessages: any;
    isOrderVisible: boolean; isTripVisible: boolean; isMapVisible: boolean;
    isStateVisible: boolean; isAssTranspVisible: boolean; isGasStationVisible: boolean;
    isApproveVisible: boolean; isRejectVisible: boolean; isRescheduleVisible: boolean;
    isCommentsReadOnly: boolean;

    selectedQuantity: number;
    rows: Array<any>;

    ol: OrderLineForm; t: any; po: any = { height: '300px' }; at: any; gs: any = {}; gsTank: any = {};
    statusBadge = { tr: 0, bo: 0, rs: 0, pn: 0, ap: 0, pg: 0, fin: 0 };
    customerName: string;
    current: string;

    frmOrderInst: any;
    btAddOrderInst: any;
    btAddTripInst: any;
    btAddGasInst: any;
    btAssTransportInst: any;
    typeEditorOpt: any;
    typeUrlParam: any;
    shipToParty: any;
    displayDriver: any;
    onSelectTruck: any; onAssignNewCar: any;
    customizeColumns: any;
    mapLocation: any;
    markers: any = [];
    isReschedule: boolean;
    timerInterval: any;
    subnavTabs: Array<any>;

    @ViewChild('gridOrder') gridOrder: DxDataGridComponent;
    @ViewChild('gridTransfer') gridTransfer: DxDataGridComponent;
    @ViewChild('subNavbarTabs') subNavbarTabs: DxTabsComponent;
    @ViewChild('map') orderMap: DxMapComponent;
    routes: { color: string; weight: number; locations: any; }[];
    dsOrderStatus: { id: string; name: string; }[];
    constructor(public ds: DataService, private router: Router, private activatedRoute: ActivatedRoute) {
        this.subnavTabs = this.getSubnavs();
        if (!ds.checkForUnauthorized('orders')) { return; }

        this.dsMessages = ds.getMessages();
        this.dsCustomer = ds.getCustomer();
        this.dsTransporter = ds.getTransporter();
        this.dsProduct = ds.getProduct();
        this.dsOrderType = ds.getOrderType();
        this.typeEditorOpt = {
            dataSource: this.dsOrderType, valueExpr: 'id',
            displayExpr: 'name', onSelectionChanged: ($event) => { this.onTypeChanged($event); }
        };
        this.displayDriver = this.displayDriverEvent;
        this.onSelectTruck = (e) => { this.onSelectTruckEvent(e, this); };
        this.onAssignNewCar = (e) => { this.onAssignNewCarEvent(e, this); };
        this.dsDepot = ds.getDepot();
        this.dsTruck = ds.getCar(null);
        this.dsTank = ds.getTank(null);

        this.isOrderVisible = false;
        this.isTripVisible = false;
        this.isMapVisible = false;
        this.isStateVisible = false;
        this.isGasStationVisible = false;
        this.isCommentsReadOnly = false;

        this.customizeColumns = (columns) => { this.customizeColumnsEv(columns, this); };

        this.dsTripTransfer = this.ds.getTransferOrder();
        this.dsOrderStatus = this.ds.getOrderStatus();
        this.selectedQuantity = 0;
        this.isAssTranspVisible = false;

        this.po = { height: '300px' };
        this.at = { carId: '', tankId: '' };
        this.ol = new OrderLineForm();

        this.activatedRoute.queryParams.subscribe(params => {
            this.typeUrlParam = params['type'] || 'sap';
            this.current = sessionStorage.getItem('currentOrderSubTab');
            if (this.current === null) {
                this.current = 'pn';
            }
            this.reloadData(this.current);
        });
        this.timerInterval = setInterval(() => {
            this.updateBadge(this.typeUrlParam === 'sap', false);
        }, 60000);
    }

    ngOnDestroy() {
        clearInterval(this.timerInterval);
        this.timerInterval = undefined;
    }

    loadState($event) {
        console.log($event);
        return '';
    }

    updateBadge(sap, reset) {
        this.ds.getOrdersCount(sap).then(data => {
            if (this.ds.isTrans() && this.ds.allowCalor) {
                this.subnavTabs[0].badge = data.tr.toString();
                this.subnavTabs[1].badge = data.rs.toString();
                this.subnavTabs[2].badge = data.pn.toString();
                this.subnavTabs[3].badge = data.ap.toString();
            } else {
                this.subnavTabs[0].badge = data.tr.toString();
                this.subnavTabs[1].badge = data.bo.toString();
                this.subnavTabs[2].badge = data.rs.toString();
                this.subnavTabs[3].badge = data.pn.toString();
                this.subnavTabs[4].badge = data.ap.toString();
                this.subnavTabs[5].badge = data.pg.toString();
                this.subnavTabs[6].badge = data.fin.toString();
            }
            // verificare
            this.checkBadges(data, reset);
        });
    }

    checkBadges(currentBadge, reset) {
        // tslint:disable-next-line:max-line-length
        const badgesClone = this.typeUrlParam === 'sap' ? JSON.parse(sessionStorage.getItem('currentBadgesOrdSap')) : JSON.parse(sessionStorage.getItem('currentBadgesOrdInt'));
        if (!badgesClone) { return; }
        this.subnavTabs.forEach((val, index) => {
            if (parseInt(val.badge, 0) !== parseInt(badgesClone[val.activeRoute], 0) && val.activeRoute !== 'rej') {
                val.icon = 'refresh';
            }
            if (reset && val.activeRoute === this.current && parseInt(val.badge, 0) !== parseInt(badgesClone[val.activeRoute], 0)) {
                val.icon = '';
                badgesClone[this.current] = parseInt(val.badge, 0);
                // tslint:disable-next-line:max-line-length
                sessionStorage.setItem(this.typeUrlParam === 'sap' ? 'currentBadgesOrdSap' : 'currentBadgesOrdInt', JSON.stringify(badgesClone));
            }
        });
        if (this.subNavbarTabs) { this.subNavbarTabs.instance.repaint(); }
    }

    editTrip(data) {
        this.router.navigate(['/tripEdit'], { queryParams: { tripId: data.data.ol_tripId } });
    }

    onInitTabs(e) {
        const selIdx = this.getSubnavs().find(x => x.activeRoute === this.current).id;
        e.component.option('selectedIndex', selIdx);
    }

    reloadData(e) {
        if (typeof e === 'string') {
            if (e === 'tr' && this.typeUrlParam !== 'sap') { e = 'pn'; }
            status = e;
            const nv = this.subnavTabs.find(n => n.activeRoute === status || n.route === status);
            if (nv != null) {
                nv.active = true;
                setTimeout(() => {
                    if (this.subNavbarTabs) {
                        this.subNavbarTabs.instance.option('selectedItem', nv);
                        this.subNavbarTabs.instance.repaint();
                    }
                }, 0);
            }
        } else {
            status = this.subnavTabs[e.itemIndex].activeRoute;
        }
        // hide transfer tab
        const rd = this.subnavTabs.find(n => n.activeRoute === 'tr');
        if (rd && this.subNavbarTabs) {
            rd.visible = this.typeUrlParam === 'sap';
            this.subNavbarTabs.instance.repaint();
        }

        this.current = status;
        if (this.current === 'tr') {
            if (this.gridTransfer) { this.gridTransfer.instance.refresh(); }
        } else {
            this.dsOrders = this.ds.getOrder(status, this.typeUrlParam === 'internal');
            if (this.btAddOrderInst) { this.btAddOrderInst.option('visible', this.typeUrlParam === 'internal' && status === 'pn'); }
            if (this.btAddGasInst) { this.btAddGasInst.option('visible', this.typeUrlParam === 'sap' && status === 'ap'); }
            if (this.btAddTripInst) { this.btAddTripInst.option('visible', status === 'ap'); }
            if (this.btAssTransportInst) { this.btAssTransportInst.option('visible', status === 'ap'); }
            if (this.gridOrder) {
                if (status === 'ap') {
                    this.gridOrder.instance.option('selection', { mode: 'multiple', showCheckBoxesMode: 'always', allowSelectAll: false });
                } else {
                    this.gridOrder.instance.option('selection', { mode: 'none', showCheckBoxesMode: 'none' });
                }
                this.gridOrder.instance.refresh();
                this.gridOrder.instance.clearFilter();
                // this.gridOrder.instance.repaint();
            }
            this.isApproveVisible = this.current === 'pn' || (!this.ds.isTrans() && this.current === 'rs');
            this.isRejectVisible = !this.ds.isTrans() && (this.current === 'pn' || this.current === 'ap' || this.current === 'rs');
            this.isRescheduleVisible = this.current === 'pn' || this.current === 'ap';
        }
        this.updateBadge(this.typeUrlParam === 'sap', true);
        sessionStorage.setItem('currentOrderSubTab', status);
    }
    customizeColumnsEv(columns: any[], that) {
        for (const c of columns) {
            if (c.cellTemplate === 'actions') {
                if (that.ds.isTrans()) {
                    c.visible = ['pn', 'ap'].indexOf(that.current) > -1;
                    c.width = that.ds.isMob ? (that.current === 'pn' ? 70 : 45) : (that.current === 'pn' ? 50 : 35);
                } else {
                    c.visible = ['pn', 'ap', 'rs'].indexOf(that.current) > -1;
                    c.width = that.ds.isMob ? (that.current === 'pn' ? 90 : 70) : (that.current === 'pn' ? 70 : 50);
                }
            }
            if (c.cellTemplate === 'map') {
                const prs = ['pn', 'ap', 'rs'].indexOf(that.current) > -1;
                c.width = that.ds.isMob ? (prs ? 35 : 45) : (prs ? 30 : 40);
            }
            if (c.dataField === 'ol_loadedQuantity') {
                c.visible = that.current === 'pg' || that.current === 'fin';
            }

            if (c.dataField === 'ol_deliveredQuantity') {
                c.visible = that.current === 'fin';
            }
        }
    }

    openMap(item) {
        this.isMapVisible = true;
        this.shipToParty = item.data.ca_code;
        this.mapLocation = item.data.ca_coordX + ', ' + item.data.ca_coordY;
        const deliveryDate = item.data.ol_deliveryDate ? this.ds.formatDateTime(item.data.ol_deliveryDate) : '-';
        const tt = '<h3 style="margin-bottom: 5px"> ' + item.data.c_name +
            '</b> / KM ' + (item.data.ol_estimatedKm || 0) +
            '</h3> <div style="text-align: center"><span> Order No.: <b>' + item.data.ol_orderIndex +
            '</b></span><br><span> Quantity: ' + item.data.ol_quantity +
            'L15</span><br><span> Delivery date: ' + deliveryDate +
            '</span> <br><span> Region: ' + item.data.ca_city + '</span> </div>';
        this.markers = [
            { location: this.mapLocation, tooltip: { text: tt, isShown: true } }];
        if (item.data.d_coordX && item.data.d_coordY) {
            const depotLoc = item.data.d_coordX + ', ' + item.data.d_coordY;
            this.markers.push({ location: depotLoc, tooltip: { text: '<h4>' + item.data.d_name + '</h4>', isShown: true } });
            this.routes = [{
                color: 'red',
                weight: 2,
                locations: [this.mapLocation, depotLoc]
            }];
            if (this.orderMap) {
                this.orderMap.instance.repaint();
            }
        }
    }

    onFormInit(e) {
        this.frmOrderInst = e.component;
    }

    changeState(type: number, item) {
        this.po.isReschedule = false;
        this.po.refNumber = item.data.ol_refNumber;
        this.po.id = item.data.ol_id;
        this.po.rescheduleDate = new Date();
        this.po.comments = null;
        this.po.reason = null;
        // 1: approve, 2: reject, 3: reschedule,
        this.po.type = this.ds.isTrans() && type === 3 ? -4 : type;
        this.po.item = item;
        this.po.carId = null;

        if (this.current === 'rs') {
            this.po.rescheduleDate = new Date(item.data.ol_requestDate);
            this.po.comments = item.data.ol_comments;
            this.po.type = type === 1 || type === 3 ? 3 : 0;
            this.sendChangeStatus();
            return;
        }

        switch (type) {
            case 1:
                this.po.height = '220px';
                this.po.oktext = 'Approve';
                this.po.question = 'Do you want to approve the order ?';
                break;
            case 2:
                this.po.height = '220px';
                this.po.oktext = 'Reject';
                this.po.question = 'Do you want to reject the order ?';
                break;
            case 3:
                this.po.isReschedule = true;
                this.po.height = '250px';
                this.po.oktext = 'Reschedule';
                this.po.question = 'Do you want to reschedule the order ?';
                break;
        }
        this.isStateVisible = true;
    }
    changeTransferState(type: number, item) {
        const obj = {
            transId: item.tt_id,
            fromId: item.tt_fromId,
            toId: item.tt_toId,
            fromRefNo: item.fo_refNumber,
            toRefNo: item.to_refNumber,
            quantity: item.tt_quantity,
            userName: this.ds.userName
        };
        EntityStore.execute(type === 1 ? 'ApproveTransfer' : 'RejectTransfer', obj).then((result) => {
            if (result === 'OK') {
                notify('Success', 'success', 3000);
                this.gridTransfer.instance.refresh();
                this.updateBadge(this.typeUrlParam === 'sap', false);
            }
        });
    }

    addGasStation() {
        this.gs = {
            stationId: '',
            shipId: '',
            addressId: '',
            comments: '',
            depotId: '',
            rangeId: '-',
            requestDate: new Date(),
            selTranspVisible: true,
            transporterId: this.ds.isTrans() ? this.ds.userId : null,
            disableDelivDate: false,
            loadedQuantity: null
        };
        this.isGasStationVisible = true;
    }

    cancelGS() {
        this.gs = {};
        this.gsTank = [];
        this.isGasStationVisible = false;
    }

    saveGS(gsTank) {
        setTimeout(() => {
            for (const g of gsTank) {
                if (g.order) {
                    const obj = {
                        addressId: this.gs.addressId,
                        depotId: this.gs.depotId,
                        transporterId: this.gs.transporterId,
                        requestDate: this.gs.requestDate,
                        typeId: '00000000-0000-0000-0000-000000000000',
                        statusId: 1,
                        quantity: g.order,
                        productId: g.st_productId
                    };
                    EntityStore.fromQuery(new EntityQuery('OrderLine')).insert(obj);
                }
            }
            this.isGasStationVisible = false;
            this.gridOrder.instance.refresh();
            notify('The orders has successfully been placed', 'success', 2000);
        }, 150);
    }

    acceptClick(po) {
        this.po = po;

        this.sendChangeStatus();
    }

    sendChangeStatus() {
        const info = {
            'id': this.po.id,
            'status': this.po.type,
            'date': EntityStore.toDateFilter(this.po.rescheduleDate),
            'comments': this.po.comments,
            'userName': this.ds.userName
        };
        EntityStore.execute('ChangeOrderStatus', info).then((data) => {
            if (data === 'OK') {
                notify('Order status has been changed!', 'success', 1500);
                this.gridOrder.instance.refresh();
                this.isStateVisible = false;
            } else {
                notify('Error while saving data', 'error', 1500);
            }
        }, () => {
            notify('Error while saving data', 'error', 1500);
        });
    }

    onCellEditingStart(e) {
        if (e.data.ol_typeId !== '00000000-0000-0000-0000-000000000000') {
            e.cancel = true;
        }
    }

    cancelClick() {
        this.isStateVisible = false;
        this.isMapVisible = false;
        this.isOrderVisible = false;
        this.isTripVisible = false;
        this.isAssTranspVisible = false;
    }

    toolbarPreparingTransfer(event) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Clear filters', icon: 'filter', onClick: (e: any) => event.component.clearFilter() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            // tslint:disable-next-line:max-line-length
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => { event.component.refresh(); this.updateBadge(this.typeUrlParam === 'sap', true); } },
            location: 'after'
        });
    }

    toolbarPreparing(event) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Clear filters', icon: 'filter', onClick: (e: any) => event.component.clearFilter() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            // tslint:disable-next-line:max-line-length
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => { event.component.refresh(); this.updateBadge(this.typeUrlParam === 'sap', true); } },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'plus', text: this.ds.isMob ? '' : 'New order', onClick: (e: any) => this.addNewOrder(),
                visible: (this.current === 'pn' && this.typeUrlParam === 'internal'),
                onInitialized: (e: any) => { this.btAddOrderInst = e.component; }
            },
            location: 'after',
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'plus', text: this.ds.isMob ? '' : 'Add Gas Station Order', onClick: (e: any) => this.addGasStation(),
                visible: (this.current === 'ap' && this.typeUrlParam === 'sap'),
                onInitialized: (e: any) => { this.btAddGasInst = e.component; }
            },
            location: 'after',
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'tags', text: this.ds.isMob ? '' : 'Assign Transport', onClick: (e: any) => this.showAssignTransportPopup(),
                visible: (this.current === 'ap'),
                onInitialized: (e: any) => { this.btAssTransportInst = e.component; }
            },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'car', text: this.ds.isMob ? '' : 'New trip', onClick: (e: any) => this.showNewTrip(),
                visible: (this.current === 'ap'),
                onInitialized: (e: any) => { this.btAddTripInst = e.component; }
            },
            location: 'after'
        });
    }

    selectionChanged(e) {
        if (this.current !== 'ap') {
            return;
        }
        let quantity = 0;
        for (const data of e.selectedRowsData) {
            quantity += data.ol_quantity;
        }
        this.selectedQuantity = quantity;

        if (e.currentSelectedRowKeys.length > 0) {
            this.changeSelection(e.currentSelectedRowKeys, true);
        }

        if (e.currentDeselectedRowKeys.length > 0) {
            this.changeSelection(e.currentDeselectedRowKeys, false);
        }
        this.checkRefNumber(e);
    }

    checkRefNumber(e) {
        const items = this.gridOrder.instance.getVisibleRows();
        const disabledKeys = e.currentSelectedRowKeys.filter(i => {
            const x = items.find((d) => d.data.ol_id === i);
            if (x && !x.data.ol_tripId) {
                return false;
            } else { return true; }
        });
        if (disabledKeys.length > 0) {
            e.component.deselectRows(disabledKeys);
        }
    }

    changeSelection(keys, select: boolean) {
        const items = this.gridOrder.instance.getVisibleRows();
        const refNo: string[] = [];
        for (const key of keys) {
            const row = items.find((d) => d.data.ol_id === key);
            if (row && refNo.indexOf(row.data.ol_refNumber) === -1) {
                refNo.push(row.data.ol_refNumber);
            }
        }

        const toChange = [];
        for (const no of refNo) {
            const rows = items.filter((d) => d.data.ol_refNumber === no);
            for (const r of rows) {
                if (r.isSelected !== select) {
                    toChange.push(r.data.ol_id);
                }
            }
        }
        if (toChange.length > 0) {
            if (select) {
                this.gridOrder.instance.selectRows(toChange, true);
            } else {
                this.gridOrder.instance.deselectRows(toChange);
            }
        }
    }

    onContentReady(e) {
        if (this.current === 'ap' && this.gridOrder) {
            const sel = this.gridOrder.instance.option('selection');
            if (sel.mode === 'none') {
                this.gridOrder.instance.option('selection', { mode: 'multiple', showCheckBoxesMode: 'always', allowSelectAll: false });
            }
        }
        e.component.option('height', this.ds.customHeight());
    }
    onContentReadyTransfer(e) {
        e.component.option('height', this.ds.customHeight());
    }

    onTypeChanged(e) {
        if (!e.selectedItem) { return; }
        const q = new EntityQuery('Customer').linkEq('id', 'customerId', 'OrderType', 'id', e.selectedItem.id);
        q.fields.push('id', 'name');
        if (e.selectedItem.name.indexOf('RATB') > -1) {
            this.dsProduct = this.ds.getProductDiesel();
        }
        EntityStore.single(q).then((result) => {
            if (!result) { return; }
            this.customerName = result.name;
            const slLocation = this.frmOrderInst.getEditor('addressId');
            slLocation.option('dataSource', this.ds.getAddr(result.id));
        });
    }

    addNewOrder() {
        this.customerName = '';
        this.ol = new OrderLineForm();
        this.isOrderVisible = true;
    }

    onCellPrepared(e: any) {
        if (e.rowType === 'data' && e.column.command === 'select' && this.current === 'ap' && e.data.ol_tripId !== null) {
            e.cellElement.children[0].classList.add('hideCheckbox');
        }

        if (e.rowType === 'data' && e.column.dataField === 'ol_loadedQuantity' && e.data.ol_loadedQuantity) {
            if (Math.abs((e.data.ol_quantity - e.data.ol_loadedQuantity) * 100 / e.data.ol_quantity) > 10) {
                e.cellElement.classList.add('redbk');
            }
        }
        if (e.rowType === 'data' && e.column.dataField === 'ol_requestDate' && e.data.ol_oldRequestDate) {
            e.cellElement.classList.add('redbk');
        }
        if (e.rowType !== 'header' && e.data && this.ds.isTrans() && e.column.cellTemplate === 'actions' &&
            e.data.ol_typeId !== null && e.data.ol_typeId !== '00000000-0000-0000-0000-000000000000') {
            e.cellElement.classList.add('hideResq');
        }
    }

    onSaveOrder($event) {
        const validation = $event.validationGroup.validate();
        if (validation.isValid) {
            EntityStore.fromQuery(new EntityQuery('OrderLine')).insert(this.ol).then((result) => {
                if (typeof result === 'string') {
                    $event.cancel = true;
                    alert(result, 'Station Range Error');
                    return;
                }
                this.isOrderVisible = false;
                this.ol = new OrderLineForm();
                this.gridOrder.instance.refresh();
            });
        }
    }


    showAssignTransportPopup() {
        this.rows = this.gridOrder.instance.getSelectedRowsData();
        if (this.rows.length === 0) {
            alert('Please select at least one line.', 'Change status');
            return;
        } else {
            const txt: any = this.checkOrderValidity(this.rows);
            if (txt) {
                alert(txt.message, txt.title);
                return;
            }
        }
        this.at = { transporter: this.rows[0].tr_name, cartId: '', tankId: '' };
        this.dsTruck = this.ds.getCar(this.rows[0].ol_transporterId);
        this.dsTank = this.ds.getTank(this.rows[0].ol_transporterId);
        this.isAssTranspVisible = true;
    }

    onAssTranspTrip($event) {
        const validation = $event.validationGroup.validate();
        if (validation.isValid) {
            const store = EntityStore.store(new EntityQuery('OrderLine'));
            this.rows.forEach(ent => {
                store.update(ent.ol_id, { carId: this.at.carId, tankId: this.at.tankId });
                ent.ol_tankId = this.at.tankId;
                ent.ol_carId = this.at.carId;
            });
            this.isAssTranspVisible = false;
            this.gridOrder.instance.refresh();
        }
    }

    showNewTrip() {
        this.rows = this.gridOrder.instance.getSelectedRowsData();
        if (this.rows.length === 0) {
            alert('Please select at least one line.', 'Change status');
            return;
        } else {
            const txt: any = this.checkOrderValidity(this.rows, true);
            if (txt) {
                alert(txt.message, txt.title);
                return;
            }
        }
        // tslint:disable-next-line:max-line-length
        const pd = new Date(new Date(this.rows[0].ol_requestDate).getFullYear(), new Date(this.rows[0].ol_requestDate).getMonth(), new Date(this.rows[0].ol_requestDate).getDate(), new Date().getHours(), new Date().getMinutes(), new Date().getSeconds());
        const min = new Date(new Date(this.rows[0].ol_requestDate).getFullYear(), new Date(this.rows[0].ol_requestDate).getMonth(), new Date(this.rows[0].ol_requestDate).getDate(), 0, 0, 1);
        // tslint:disable-next-line:max-line-length
        const max = new Date(new Date(this.rows[0].ol_requestDate).getFullYear(), new Date(this.rows[0].ol_requestDate).getMonth(), new Date(this.rows[0].ol_requestDate).getDate(), 23, 58, 0);
        this.t = {
            reqMinDate: min,
            reqMaxDate: max,
            plannedDate: pd,
            transporterId: this.rows[0].ol_transporterId,
            depotId: this.rows[0].ol_depotId,
            tankId: null,
            carId: null,
            driverId: null,
            statusId: 0,
            orderIndex: 1
        };
        this.dsTruck = this.ds.getCar(this.t.transporterId, true);
        this.dsTank = this.ds.getTank(this.t.transporterId, true);
        this.dsDrivers = this.ds.getDriver(this.t.transporterId, true);
        if (this.checkForCarTrailer(this.rows)) {
            this.t.tankId = this.rows[0].ol_tankId;
            this.t.carId = this.rows[0].ol_carId;
            if (this.t.carId) {
                this.ds.getCarById(this.t.carId).then((data) => {
                    this.t.driverId = data.driverId || null;
                    this.isTripVisible = true;
                });
            } else {
                this.isTripVisible = true;
            }
        } else {
            this.isTripVisible = true;
        }
    }

    onSaveTrip($event) {
        const validation = $event.validationGroup.validate();
        if (validation.isValid) {
            EntityStore.fromQuery(new EntityQuery('Trip')).insert(this.t).then((result) => {
                const ids = [];
                for (const r of this.rows) {
                    ids.push(r['ol_id']);
                }
                EntityStore.execute('AddToTrip', { tripId: result.id, ids: ids }).then((ord) => {
                    this.router.navigate(['tripEdit'], { queryParams: { tripId: result.id } });
                });
            });
        }
    }

    checkOrderValidity(rows, depot?) {
        for (const r of rows) {
            if (r['ol_transporterId'] !== rows[0].ol_transporterId) {
                return { message: 'Transporter name is different.', title: 'Transporter name' };
            }
            if (r['ol_requestDate'] !== rows[0].ol_requestDate) {
                return { message: 'Request date is different.', title: 'Request date' };
            }

            if (r['d_name'] !== rows[0].d_name) {
                return { message: 'Depot location is different.', title: 'Depot' };
            }
        }
        return false;
    }
    checkForCarTrailer(rows) {
        const truck = rows[0].ol_carId;
        const trailer = rows[0].ol_tankId;
        for (const r of rows) {
            if (r['ol_carId'] !== truck) {
                return false;
            }
            if (r['ol_tankId'] !== trailer) {
                return false;
            }
        }
        return true;
    }

    onSelectTruckEvent(e, that) {
        if (e.selectedItem) {
            that.t.driverId = e.selectedItem.driverId || null;
            if (!that.t.tankId) {
                that.t.tankId = e.selectedItem.tankId || null;
            }
        }
    }

    onAssignNewCarEvent(e, that) {
        if (e.selectedItem) {
            that.at.tankId = e.selectedItem.tankId || null;
        }
    }

    displayDriverEvent(data) {
        return !data ? null : (data.lastName + ' ' + data.firstName);
    }

    getSubnavs() {
        const arr = [{
            id: 0,
            text: 'Transfer',
            activeRoute: 'tr',
            active: false,
            badge: this.statusBadge.tr,
            icon: ''
        }, {
            id: 1,
            text: 'BO Validated ',
            activeRoute: 'bo',
            active: false,
            badge: this.statusBadge.bo,
            icon: ''
        },
        {
            id: 2,
            text: 'Reschedule',
            activeRoute: 'rs',
            active: false,
            badge: this.statusBadge.rs,
            icon: ''
        },
        {
            id: 3,
            text: 'Pending',
            activeRoute: 'pn',
            active: false,
            badge: this.statusBadge.pn,
            icon: ''
        },
        {
            id: 4,
            text: 'Approved',
            activeRoute: 'ap',
            active: false,
            badge: this.statusBadge.ap,
            icon: ''
        },
        {
            id: 5,
            text: 'In progress',
            activeRoute: 'pg',
            active: false,
            badge: this.statusBadge.pg,
            icon: ''
        },
        {
            id: 6,
            text: 'Finished',
            activeRoute: 'fin',
            active: false,
            badge: this.statusBadge.fin,
            icon: ''
        },
        {
            id: 7,
            text: 'Rejected',
            activeRoute: 'rej',
            active: false,
            badge: 0,
            icon: ''
        }, {
            id: 8,
            text: 'All',
            activeRoute: 'all',
            active: false,
            badge: 0,
            icon: ''
        }
        ];
        if (this.ds.isTrans()) {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i].activeRoute === 'tr' || arr[i].activeRoute === 'all' ||
                    (this.ds.allowCalor && (arr[i].activeRoute === 'bo' || arr[i].activeRoute === 'pg' || arr[i].activeRoute === 'fin'))) {
                    arr.splice(i, 1);
                    i--;
                }
            }
        }
        return arr;
    }
}

class OrderLineForm {
    statusId: number;
    typeId: string;
    addressId: string;
    transporterId: string;
    productId: string;
    depotId: string;
    requestDate: Date;
    quantity: number;
    comments: string;
    constructor() {
        this.statusId = 0;
        this.typeId = null;
        this.addressId = null;
        this.transporterId = null;
        this.productId = null;
        this.depotId = null;
        this.requestDate = new Date();
        this.quantity = null;
        this.comments = null;
    }
}
