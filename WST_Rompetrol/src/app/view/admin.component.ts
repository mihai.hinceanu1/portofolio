import { DxDataGridComponent, DxTabPanelComponent } from 'devextreme-angular';
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { EntityStore } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
@Component({
    templateUrl: 'admin.component.html'
})
export class AdminComponent {
    current: string;
    dsCustomer: CustomStore;
    dsTransporter: CustomStore;
    dsProduct: CustomStore;
    dsDepot: CustomStore;
    dsUser: CustomStore;
    dsStationUser: CustomStore;
    dsRange: CustomStore;
    dsDriver: CustomStore;
    dsTank: CustomStore;
    dsOrderType: CustomStore;
    dsStation: CustomStore;
    dsCustomerAddr: CustomStore;
    dsDrivers: CustomStore;
    dsAudit: CustomStore;
    dsConfig: CustomStore;
    dsOrders: CustomStore;
    dsUserType: any;
    dsStoreAddress: CustomStore;
    dsMeterType: Array<{ id, name }>;

    transTabs: any;
    customerId: any;
    transporterId: any;
    gasStationId: any;
    getDisplayExpr: any;
    stationId: any;
    subnavTabs: any;
    @ViewChild('gridCS') gridCS: DxDataGridComponent;
    @ViewChild('gridTR') gridTR: DxDataGridComponent;
    @ViewChild('gridDep') gridDep: DxDataGridComponent;
    @ViewChild('gridST') gridST: DxDataGridComponent;
    @ViewChild('gridProd') gridProd: DxDataGridComponent;
    @ViewChild('gridOT') gridOT: DxDataGridComponent;
    @ViewChild('gridAD') gridAD: DxDataGridComponent;
    @ViewChild('gridUS') gridUS: DxDataGridComponent;
    @ViewChild('gridCF') gridCF: DxDataGridComponent;
    dsAlertType: { id: number; val: string; }[];
    dsAlert: any;
    constructor(public ds: DataService, private router: Router) {
        this.subnavTabs = this.getSubnavs();
        if (!ds.checkForUnauthorized('admin')) { return; }

        this.reloadData(sessionStorage.getItem('currentAdmin') || 'st');
        this.dsStation = ds.getStation();
        this.dsCustomer = ds.getCustomer();
        this.dsTransporter = ds.getTransporter();
        this.dsProduct = ds.getProduct();
        this.dsDepot = ds.getDepot();
        this.dsUser = ds.getUser();
        this.dsAlert = ds.getAlert();
        this.dsStationUser = ds.getStationUser();
        this.dsOrderType = ds.getOrderType();
        this.dsDrivers = ds.getDrivers();
        this.dsAudit = ds.getAuditTrail();
        this.dsRange = ds.getRange();
        this.dsConfig = ds.getConfig();
        this.dsUserType = [
            { id: 0, type: 'Logistics' },
            { id: 10, type: 'Transporter' },
            { id: 11, type: 'Driver' },
            { id: 12, type: 'Station manager' },
            { id: 13, type: 'Transporter report' },
            { id: 14, type: 'Transporter admin' },
            { id: 15, type: 'Sales' }];
        this.dsCustomerAddr = ds.getCustomerAddr();
        this.transTabs =
            [{ id: 1, title: 'Truck', template: 'tabCar' },
            { id: 2, title: 'Trailer', template: 'tabTank' },
            { id: 3, title: 'Driver', template: 'tabDriver' },
            { id: 4, title: 'Cost', template: 'tabCost' }];
        this.dsAlertType = [{ id: 1, val: 'Aprobare cursa' }, { id: 2, val: 'Reprogramare comanda' },
        { id: 3, val: 'Fara stock' }, { id: 4, val: 'Transfer comanda' }, { id: 5, val: 'Anulare comanda' }];
        this.dsStoreAddress = ds.getAddressLikeRC();
        this.dsMeterType = ds.getMeterType();

        this.getDisplayExpr = this.getDisplayExprFunc;
    }

    onInitTabs(e) {
        const selIdx = this.getSubnavs().find(x => x.activeRoute === this.current).id;
        e.component.option('selectedIndex', selIdx);
    }

    toolbarPreparing(event) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => event.component.refresh() },
            location: 'after'
        });
    }

    toolbarPreparingStation(event) {
        this.toolbarPreparing(event);

        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { icon: 'download', onClick: (e: any) => this.updateStock() },
            location: 'after'
        });
    }

    onUserChanged(e: any) {
        if (e.data) {
            switch (e.data.typeId) {
                case 11: // driver
                    e.data.transporterId = null;
                    e.data.stationId = null;
                    if (!e.data.driverId) {
                        notify('Trebuie asociat soferul.', 'error', 3000);
                        e.cancel = true;
                    }
                    break;
                case 13:
                case 14:
                case 10: // transporter
                    e.data.stationId = null;
                    e.data.driverId = null;
                    if (!e.data.transporterId) {
                        notify('Trebuie asociat transportatorul.', 'error', 3000);
                        e.cancel = true;
                    }
                    break;
                case 12: // station manager
                    e.data.transporterId = null;
                    e.data.driverId = null;
                    if (!e.data.stationId) {
                        notify('Trebuie asociat statia.', 'error', 3000);
                        e.cancel = true;
                    }
                    break;
            }
        }
    }

    updateStock() {
        const rows = this.gridST.instance.getSelectedRowsData();
        if (rows.length === 0) {
            return;
        }
        const codes = [];
        for (const r of rows) {
            codes.push(r['code']);
        }

        EntityStore.execute('UpdateStock', { codes: codes }).then((data) => {
            if (data === 'OK') {
                notify('Stock has been updated!', 'success', 1500);
            } else {
                notify('Error while updating stock', 'error', 1500);
            }
        }, () => {
            notify('Error while updating stock', 'error', 1500);
        });
    }

    onContentReady(e) {
        e.component.option('height', this.ds.customHeight());
    }

    reloadData(e) {
        if (typeof e === 'string') {
            status = e;
        } else {
            status = this.subnavTabs[e.itemIndex].activeRoute;
        }
        this.current = status;
        sessionStorage.setItem('currentAdmin', status);

        switch (this.current) {
            case 'cs':
                if (this.gridCS) { this.gridCS.instance.refresh(); }
                break;
            case 'tr':
                if (this.gridTR) { this.gridTR.instance.refresh(); }
                break;
            case 'dep':
                if (this.gridDep) { this.gridDep.instance.refresh(); }
                break;
            case 'st':
                if (this.gridST) { this.gridST.instance.refresh(); }
                break;
            case 'prod':
                if (this.gridProd) { this.gridProd.instance.refresh(); }
                break;
            case 'ot':
                if (this.gridOT) { this.gridOT.instance.refresh(); }
                break;
            case 'ad':
                if (this.gridAD) { this.gridAD.instance.refresh(); }
                break;
            case 'us':
                if (this.gridUS) { this.gridUS.instance.refresh(); }
                break;
            case 'cf':
                if (this.gridCF) { this.gridCF.instance.refresh(); }
                break;
        }
    }

    showAddressDetails(rowInfo: any) {
        return rowInfo.code + ' - ' + rowInfo.description;
    }

    addrNewRow(e: any) {
        e.data.customerId = this.customerId;
        e.data.stationId = this.stationId;
    }
    costNewRow(e: any) {
        e.data.transporterId = this.transporterId;
    }

    addNewStationRange(e: any) {
        e.data.stationId = this.stationId;
    }

    trNewRow(e: any) {
        e.data.transporterId = this.transporterId;
        this.dsDriver = this.ds.getDriver(this.transporterId);
        // console.log(this.dsDriver);
    }

    driverNewRow(e: any) {
        e.data.transporterId = this.transporterId;
    }

    userNewRow(e: any) {
        e.data.isActiv = true;
        e.data.allowCalor = false;
    }

    addrInit(e, c) {
        this.customerId = c.data.id;
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getAddr(c.data.id));
        }
    }

    onPasswordChange(e, data) {
        data.setValue(e.value);
    }

    stationInit(e, c, tab) {
        this.stationId = c.data.id;
        switch (tab) {
            case 'tank':
                if (e.component.option('dataSource') == null) {
                    e.component.option('dataSource', this.ds.getStationTank(c.data.id));
                }
                break;
            case 'range':
                if (e.component.option('dataSource') == null) {
                    e.component.option('dataSource', this.ds.getStationRange(c.data.id));
                }
                break;
        }
    }

    editorPreparing(e) {
        if (e.dataField === 'correctPassword') {
            e.editorOptions.mode = 'password';
        }
    }

    carInit(e, c) {
        this.transporterId = c.data.id;
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getCar(c.data.id));
            this.dsDriver = this.ds.getDriver(this.transporterId);
            this.dsTank = this.ds.getTank(this.transporterId);
        }
    }

    tankInit(e, c) {
        this.transporterId = c.data.id;
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getTank(c.data.id));
        }
    }

    driverInit(e, c) {
        this.transporterId = c.data.id;
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getDriver(c.data.id));
        }
    }

    costInit(e, c) {
        this.transporterId = c.data.id;
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getCost(c.data.id));
        }
    }

    getDisplayExprFunc(item) {
        if (!item || !item.description) {
            return;
        }
        return item.description + ', ' + item.region + ', ' + item.city;
    }
    onDriverSelCh(e, data) {
        data.setValue(e.value);
    }

    displayDriver(e) {
        return e === null ? '' : e.firstName + ' ' + e.lastName;
    }

    stationRangeDisplayed(e) {
        return e === null ? '' : e.code + ' [' + e.value + ']';
    }

    getSubnavs() {
        const arr = [
            {
                id: 0,
                text: 'Customers',
                activeRoute: 'cs',
                active: 'false',
                icon: ''
            },
            {
                id: 1,
                text: 'Transporters',
                activeRoute: 'tr',
                active: 'false',
                icon: ''
            },
            {
                id: 2,
                text: 'Depots',
                activeRoute: 'dep',
                active: 'false',
                icon: ''
            },
            {
                id: 3,
                text: 'Station',
                activeRoute: 'st',
                active: 'false',
                icon: ''
            },
            {
                id: 4,
                text: 'Products',
                activeRoute: 'prod',
                active: 'false',
                icon: ''
            },
            {
                id: 5,
                text: 'Order Types',
                activeRoute: 'ot',
                active: 'false',
                icon: ''
            },
            {
                id: 6,
                text: 'Audit',
                activeRoute: 'ad',
                active: 'false',
                icon: ''
            },
            {
                id: 7,
                text: 'Alert',
                activeRoute: 'at',
                active: 'false',
                icon: ''
            },
            {
                id: 8,
                text: 'Config',
                activeRoute: 'cf',
                active: 'false',
                icon: ''
            },
            {
                id: 9,
                text: 'Users',
                activeRoute: 'us',
                active: 'false',
                icon: ''
            }];
        return arr;
    }
}
