import { DxDataGridComponent } from 'devextreme-angular';
import { DataService } from '../service/data.service';
import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
import { confirm, alert } from 'devextreme/ui/dialog';
import { environment } from '../../environments/environment';
import { not } from '@angular/compiler/src/output/output_ast';
import { elementStyleProp } from '@angular/core/src/render3';

@Component({
    templateUrl: 'tripEdit.component.html'
})

export class TripEditComponent {

    dsOrders: CustomStore;
    dsDipstickLoaded: CustomStore;
    dsOrderHistory: CustomStore;
    dsTransporter: CustomStore;
    dsDepot: CustomStore;
    dsProduct: CustomStore;
    dsTank: CustomStore;
    dsCar: CustomStore;
    dsDriver: CustomStore;
    dsAddOrders: CustomStore;
    dsMeterType: Array<{ id, name }>;
    imagePath: any;

    teTabs: any;
    tripId: any;
    trip: any = {};
    gs: any = {};
    gsTank: any;
    // tslint:disable-next-line:max-line-length
    tripHeaderState = 2; // 0: logistica(modifica tot)   1: transportator change planned date hour    2: read only tot    3: modifica tot except loading date
    customizeColumns: any;
    isGasStationVisible: boolean;
    isTransferVisible: boolean;
    isAddOrderVisible: boolean;
    onTankChanged: any;
    getDisplayExpr: any;
    selectedRows: number[];
    orderMaxCap: any = 99999;
    orderGrids: any[] = [];
    dsStatus: any[];
    btAddOrderOnTripInst: any;
    btAddGasOrderOnTripInst: any;
    btAddGasOrderOnTripCond: any;
    btAddOrdersBtnInst: any;
    selected = { quantity: 0, remains: 0, availableQuantity: 0 };
    calculateSummary: any; customLoadedQuant: any;

    @ViewChild('gridOrder') gridOrder: DxDataGridComponent;
    @ViewChild('gridGasStation') gridGasStation: DxDataGridComponent;
    @ViewChild('gridAddOrder') gridAddOrder: DxDataGridComponent;
    btAddDipstickTicketInst: any;
    isDipstickPopupVisible: boolean;
    currentDate: Date;
    constructor(public ds: DataService, private router: Router, private activatedRoute: ActivatedRoute) {
        if (!ds.checkForUnauthorized('tripEdit')) { return; }
        this.dsStatus = this.ds.getOrderStatusDescription();
        this.isGasStationVisible = false;
        this.isTransferVisible = false;
        this.isAddOrderVisible = false;
        this.isDipstickPopupVisible = false;
        this.btAddGasOrderOnTripCond = false;
        this.currentDate = new Date();
        this.dsProduct = ds.getProduct();
        this.dsMeterType = ds.getMeterType();
        this.customizeColumns = (columns) => { this.customizeColumnsEv(columns, this); };
        this.onTankChanged = (item) => { this.onTankChangedEvent(item, this); };
        this.calculateSummary = (options) => { this.calculateSummaryEvent(options, this); };
        this.customLoadedQuant = (itemInfo) => {
            let proc = Math.abs((this.trip.capacity - itemInfo.value) * 100 / this.trip.capacity);
            proc = proc === Infinity ? 0 : 100 - proc;
            return 'Loaded Quantity: ' + itemInfo.value + ' (' + Math.round(proc) + '%)';
        };
        this.getDisplayExpr = this.getDisplayExprEvent;
        this.imagePath = environment.ImagePath;

        this.activatedRoute.queryParams.subscribe(params => {
            this.tripId = params['tripId'];
            if (!this.tripId) {
                router.navigateByUrl('/trips');
                return;
            }
            this.initTrip();
        });
    }

    initTrip() {
        EntityStore.fromQuery(new EntityQuery('Trip').eq('id', this.tripId)).single().then(
            (result) => {
                this.trip = result;
                this.ds.getCapacityForTank(result.tankId).then((data: any) => {
                    this.trip.capacity = data.capacity;
                    this.trip.typeId = data.typeId;
                    const vs = (this.trip.statusId === 5 || ((this.ds.isAdmin() || this.trip.allowEdit) && this.trip.statusId === 6)) && this.trip.typeId === 1;
                    if (this.btAddDipstickTicketInst) {
                        this.btAddDipstickTicketInst.option('visible', vs);
                    }
                });
                this.trip.plannedDate = new Date(result.plannedDate);
                this.trip.sendFinish = this.trip.statusId === 5;
                this.dsOrders = this.ds.getOrderByTrip(this.tripId);
                this.dsOrderHistory = this.ds.getOrderHistory(this.tripId);
                this.dsDipstickLoaded = this.ds.getTripLoading(this.tripId);
                this.dsTransporter = this.ds.getTransporter();
                this.dsDepot = this.ds.getDepot();
                this.dsTank = this.ds.getTank(result.transporterId, true);
                this.dsCar = this.ds.getCar(result.transporterId, true);
                this.dsDriver = this.ds.getDriver(result.transporterId, true);
                this.btAddOrderOnTripInst.option('visible', (this.ds.isTrans() ? this.trip.statusId === 0 : this.trip.statusId < 3));
                this.btAddGasOrderOnTripCond = () => {
                    if (this.ds.isTrans()) {
                        return this.trip.statusId === 0 && !this.ds.allowCalor;
                    } else {
                        return this.trip.statusId < 3 && !this.ds.allowCalor;
                    }
                };
                this.isTransferVisible = this.trip.statusId === 5 && !this.ds.allowCalor;
                this.btAddGasOrderOnTripInst.option('visible', this.btAddGasOrderOnTripCond());
                EntityStore.fromQuery(new EntityQuery('OrderLineView').eq('ol.tripId', this.tripId)).totalCount().then(tc => {
                    this.setHeaderState(tc, result);
                });
            });
    }
    toolbarPreparing(event) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { icon: 'refresh', onClick: (e: any) => event.component.refresh() },
            location: 'after'
        });
        if (!this.ds.isSales()) {
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    icon: 'doc',
                    text: 'Dipstick',
                    visible: false,
                    onClick: (e: any) => {
                        this.isDipstickPopupVisible = true;
                    },
                    onInitialized: (e: any) => { this.btAddDipstickTicketInst = e.component; }
                },
                location: 'after',
            });
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    icon: 'plus', text: 'Add Orders', onClick: (e: any) => this.addNewOrder(),
                    onInitialized: (e: any) => { this.btAddOrderOnTripInst = e.component; }
                },
                location: 'after',
            });
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    icon: 'plus', text: 'Add Gas Station Order',
                    onClick: (e: any) => this.addGasStationOrder(null),
                    onInitialized: (e: any) => { this.btAddGasOrderOnTripInst = e.component; }
                },
                location: 'after',
            });
        }
    }

    onToolbarHistory(event: any) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => event.component.refresh() },
            location: 'after'
        });
    }

    onOrderToolbarPreparing(event) {
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Clear filters', icon: 'filter', onClick: (e: any) => event.component.clearFilter() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reset layout', icon: 'toolbox', onClick: (e: any) => event.component.state(null) },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Refresh data', icon: 'refresh', onClick: (e: any) => event.component.refresh() },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'plus', text: 'Add Orders', disabled: true,
                onClick: (e: any) => this.addOrdersToTrip(),
                onInitialized: (e: any) => { this.btAddOrdersBtnInst = e.component; }
            },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            location: 'before',
            template: 'selectedQuantity'
        });
    }
    customizeColumnsEv(columns, that) {
        for (const c of columns) {
            if (c.cellTemplate === 'actions') {
                c.visible = (that.trip.statusId < 2 || that.trip.statusId >= 5);
            }
        }
    }

    schedulerTrip() {
        EntityStore.execute('ScheduleTrip', { tripId: this.tripId }).then((result) => {
            if (result !== 'OK') {
                notify(result, 'error', 3000);
            } else {
                this.initTrip();
                notify('Trip schedule finished', 'success', 3000);
            }
            this.gridOrder.instance.refresh();
        }, (error) => {
            this.ds.serverError(error);
        });
    }

    addNewOrder() {
        this.dsAddOrders = this.ds.getOrdersToAddTrip(this.trip.depotId, this.trip.transporterId, this.trip.plannedDate);
        this.initAddOrdersGrid();
        this.isAddOrderVisible = true;
    }

    uploadTicket(id, dipstickLoaded) {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', '.png, .jpg, .jpeg');
        input.addEventListener('change', (e: any) => {
            const fc: any = e.target;
            if (dipstickLoaded) {
                this.ds.loadTrip(this.tripId, dipstickLoaded).then(() => {
                    this.uploadFile(fc.files[0], id);
                });
            } else {
                this.uploadFile(fc.files[0], id);
            }
        }, false);
        input.click();
    }
    onPanelTitleClick(e: any) {
        if (e.itemData.title == 'History') {
            this.gridOrder.instance.refresh();
        }
    }

    uploadDipstickTicket(dipstickLoaded, id) {
        if (dipstickLoaded && dipstickLoaded > 0) {
            this.uploadTicket(id, dipstickLoaded);
        } else {
            notify('Loaded quantity is invalid', 'warning', 3000);
        }
    }
    onUpdateQuantity(dipstickLoaded: number, id: string) {
        if (dipstickLoaded && dipstickLoaded > 0) {
            this.ds.updateLoadedTripQuantity(id, dipstickLoaded).then(() => {
                notify('Quantity updated', 'success', 3000);
                this.isDipstickPopupVisible = false;
                this.initTrip();
            })
        }
    }
    uploadFile(file, id) {
        this.ds.uploadFile(file, id).then(resolve => {
            notify('Success', 'success', 3000);
            this.isDipstickPopupVisible = false;
            this.initTrip();
        }, error => {
            this.ds.serverError(error);
        });
    }

    addOrdersToTrip() {
        const rows = this.gridAddOrder.instance.getSelectedRowsData();
        const ids = [];
        for (const r of rows) {
            ids.push(r['ol_id']);
        }
        EntityStore.execute('AddToTrip', { tripId: this.trip.id, ids: ids }).then((result) => {
            if (rows.length > 0) { this.tripHeaderState = 1; }
            notify('Success', 'sucess', 2000);
            this.isAddOrderVisible = false;
            this.gridOrder.instance.refresh();
        });
    }

    onTankChangedEvent(data, that) {
        this.ds.getCapacityForTank(data.value).then((tank: any) => {
            this.trip.capacity = tank.capacity;
        });
    }

    getDisplayExprEvent(item) {
        if (!item || !item.description) {
            return;
        }
        return item.description + ', ' + item.region + ', ' + item.city;
    }

    addGasStationOrder(rowInfo: any) {
        this.gs = {
            requestDate: this.trip.plannedDate,
            stationId: '',
            shipId: '',
            addressId: '',
            comments: '',
            depotId: this.trip.depotId,
            selTranspVisible: false,
            rangeId: '-',
            disableDelivDate: true,
        };
        if (rowInfo) {
            this.gs.unloadedId = rowInfo.ol_id;
            this.gs.product = rowInfo.p_name;
            this.gs.loadedQuantity = rowInfo.ol_loadedQuantity;
        }
        this.isGasStationVisible = true;
    }

    deleteOrder(data) {
        const result = confirm('Do you want to remove this order?', 'Order Remove');
        result.then((cfm) => {
            if (!cfm) { return; }

            EntityStore.execute('RemoveFromTrip', { id: data.ol_id, userName: this.ds.userName }).then((qr) => {
                if (qr !== 'ERROR') {
                    notify('Order has been deleted from trip', 'success', 1500);
                    this.gridOrder.instance.refresh();
                }
            });
        });
    }

    sendToApproved(data, statusId) {
        const result = confirm('Do you want to remove from trip and send it to approve?', 'Order Remove');
        result.then((cfm) => {
            if (!cfm) { return; }

            EntityStore.execute('RemoveFromTripInProgress', {
                id: data.ol_id, userName: this.ds.userName,
                status: statusId, orderNo: data.ol_refNumber, tripNo: this.trip.refNumber
            }).then((response: any) => {
                if (response === 'OK') {
                    notify('Order has been move to approved state!', 'success', 3000);
                    this.gridOrder.instance.refresh();
                }
            });
        });
    }

    changeOrder(data, up: boolean) {
        const dataToSend = {
            id: data.ol_id,
            index: data.ol_orderIndex == null ? 0 : data.ol_orderIndex,
            up: up,
            tripId: data.ol_tripId,
            userName: this.ds.userName
        };

        EntityStore.execute('ChangeOrderIndex', dataToSend).then((result) => {
            if (result === 'OK') {
                this.gridOrder.instance.refresh();
            }
        }, () => {
            notify('Error while changing position', 'error', 1500);
        });
    }

    channgeTripStatus(status) {
        this.gridOrder.instance.endUpdate();
        if (status === -2 || status === 6) {
            const action = status === -2 ? 'reject' : 'send to finish';
            if (status === 6) {
                const it = this.gridOrder.instance.getDataSource().items();
                const valid = it.find(a => a.ol_statusId < 3 && a.ol_statusId > 0);
                if (valid) {
                    alert('Need to add delivery quantity and ticket to each order!', 'Trip delivery');
                    return;
                } else {
                    const ord = []; const sts = [];
                    it.forEach(item => { ord.push(item.ol_id); });
                    if (ord.length > 0) {
                        this.ds.checkForTickets(ord).then(result => {
                            if (result !== 'OK') {
                                alert('Need to upload ticket for order: ' + result, 'No Ticket');
                                return;
                            } else {
                                this.sendTripStatus(status);
                                this.updateOrders();
                            }
                        });
                    }
                }
            } else {
                this.confirmPopupFinish(action, status);
            }
        } else {
            this.sendTripStatus(status);
        }
    }
    confirmPopupFinish(action, status) {
        confirm('Are you sure you whant to ' + action + ' this trip ?', 'Confirm').then((val) => {
            if (val) {
                this.sendTripStatus(status);
                if (status === 6) {
                    this.updateOrders();
                }
            }
        });
    }

    updateOrders() {
        const es = EntityStore.store(new EntityQuery('OrderLine'));
        const gr = this.gridOrder.instance.getDataSource().items();
        gr.forEach(v => {
            if (v.ol_statusId > 0) {
                es.update(v.ol_id, { statusId: 3 });
            }
        });
    }

    sendTripStatus(status) {
        EntityStore.execute('ChangeTripStatus',
            { ids: [this.trip.id], status: status, userName: this.ds.userName, comments: '' }).then((result) => {
                switch (result) {
                    case 'OK':
                        notify('Success', 'success', 2000);
                        this.initTrip();
                        if (this.gridOrder) { this.gridOrder.instance.refresh(); }
                        break;
                    case 'NO_ORDERS':
                        notify('No orders on this trip. Can\'t be send to SAP.', 'error', 3000);
                        break;
                    case 'ERROR':
                        notify('Data cannot be processed.Please try again later.', 'error', 3000);
                        break;
                }
            });
    }

    onSaveTrip(trip) {
        const sendToSave = {
            id: trip.id,
            transporterId: trip.transporterId,
            carId: trip.carId,
            tankId: trip.tankId,
            depotId: trip.depotId,
            driverId: trip.driverId,
            quantity: trip.quantity,
            orderIndex: trip.orderIndex ? trip.orderIndex : 1,
            statusId: trip.statusId,
            plannedDate: trip.plannedDate
        };
        EntityStore.fromQuery(new EntityQuery('Trip')).update(this.trip.id, sendToSave).then((data) => {
            if (data && (data.key || data === 'OK')) {
                notify('Success', 'success', 2000);
                this.router.navigate(['trips']);
            }
        }, () => {
            notify('Error while saving trip', 'error', 1500);
        });
    }

    cancelGS() {
        this.gs = {};
        this.gsTank = [];
        this.isGasStationVisible = false;
    }

    cancelClick() {
        this.isAddOrderVisible = false;
        this.isDipstickPopupVisible = false;
    }

    onIsLockSwtich(e: any) {
        EntityStore.execute("AllowEditTrip", { "allowEdit": e.value, "refNumber": this.trip.refNumber, userName: this.ds.userName }).then(() => {
            this.gridOrder.instance.state(null);
        })
    }

    isApproveVisible(data: any) {
        if (this.ds.isSales()) {
            return false;
        }
        return this.trip.statusId > 4 && !data.ol_loadedQuantity && data.ol_statusId > 0 && this.ds.isSuperAdmin();
    }

    isUploadVisible(data: any) {
        if (this.ds.isSales()) {
            return false;
        }
        if (this.ds.isAdmin()) {
            return this.trip.statusId >= 5;
        } else if (this.ds.isTrans()) {
            if (data.ol_statusId >= 2) {
                return this.trip.statusId === 5 || (this.trip.statusId === 6 && this.trip.allowEdit);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    isTransferVisibleEv(data: any) {
        if (this.ds.isSales()) {
            return false;
        }
        if (data.ol_typeId == '00000000-0000-0000-0000-000000000000' && data.ol_quantity && data.ol_loadedQuantity) {
            // statusId = 5
            if (this.isTransferVisible && !data.ol_deliveryDate) {
                return true;
            } else if (this.trip.statusId === 6 && !this.ds.allowCalor) {
                return this.trip.allowEdit;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    saveGS(gsTank: any) {
        if (this.gs.unloadedId) {
            EntityStore.execute('CreateTransfer', {
                oldId: this.gs.unloadedId, quantity: gsTank[0].order,
                addressId: this.gs.addressId, userName: this.ds.userName, comments: this.gs.comments
            }).then((response) => {
                if (response === 'OK') {
                    notify('The orders has successfully been placed', 'success', 2000);
                    this.isGasStationVisible = false;
                    this.gridOrder.instance.refresh();
                } else if (response === 'trip_has_locked') {
                    notify('Trip is already locked.', 'error', 3000);
                    setTimeout(() => {
                        window.location.reload();
                    }, 3000);
                } else {
                    notify('Data can\'t be processed complete. Please check and try again.', 'error', 4000);
                }
            });
        } else {
            let index = this.gridOrder.instance.getTotalSummaryValue('orderCount');
            for (let i = 0; i < gsTank.length; i++) {
                if (gsTank[i].order) {
                    index++;
                    const obj = {
                        requestDate: this.trip.plannedDate,
                        orderIndex: index,
                        addressId: this.gs.addressId,
                        tripId: this.trip.id,
                        depotId: this.trip.depotId,
                        transporterId: this.trip.transporterId,
                        statusId: 1,
                        quantity: gsTank[i].order,
                        typeId: '00000000-0000-0000-0000-000000000000',
                        productId: gsTank[i].st_productId,
                        comments: this.gs.comments
                    };

                    this.tripHeaderState = 1;
                    EntityStore.fromQuery(new EntityQuery('OrderLine')).insert(obj).then(() => {
                        notify('Data can\'t be processed complete. Please check and try again.', 'error', 4000);
                    });
                }
            }
            setTimeout(() => {
                this.isGasStationVisible = false;
                this.gridOrder.instance.refresh();
            }, 300);
        }
    }

    displayDriver(e) {
        return !e ? '' : (e.lastName + ' ' + e.firstName);
    }

    routeInitialize(e) {
        const routesArray = [];
        const markersArray = [];
        const gridItems = this.gridOrder.instance.getDataSource().items();
        this.ds.getDepotById(this.trip.depotId).then((data: any) => {
            let depotCoordX = data.coordX;
            let depotCoordY = data.coordY;

            if (!depotCoordX) {
                depotCoordX = 45.5665823;
                depotCoordY = 25.3084744;
            }
            const depotLocation = depotCoordX + ', ' + depotCoordY;
            let localIndex = 1;
            for (const ord of gridItems) {
                if (!ord.totalQuant) {
                    const flt = gridItems.filter(x => x.ol_refNumber === ord.ol_refNumber);
                    ord.totalQuant = 0;
                    ord.customIndex = localIndex++;
                    flt.forEach(val => {
                        ord.totalQuant += val.ol_quantity;
                    });
                    if (ord.ca_coordX) {
                        const location = ord.ca_coordX + ', ' + ord.ca_coordY;
                        const tt = '<h3 style="margin-bottom:5px"> ' + ord.c_name +
                            '</h3> <div style="text-align: center"><span> Order No.: <b>' + ord.customIndex +
                            '</b> / KM ' + (ord.ol_estimatedKm || 0) +
                            '</span><br><span> Quantity: ' + ord.totalQuant + ' L15</span><br>' +
                            (ord.ol_deliveryDate ? '<span> Delivery date: ' + this.ds.formatDateTime(ord.ol_deliveryDate) + '</span>' :
                                // tslint:disable-next-line:max-line-length
                                '<span> ETA: ' + (ord.ol_scheduledDate === null ? '' : this.ds.formatDateTime(ord.ol_scheduledDate)) + '</span>')
                            + '<br><span> City: ' + ord.ca_city + '</span></div>';
                        routesArray.push(location);
                        markersArray.push({ location: location, tooltip: { text: tt, isShown: true } });
                    }
                }
            }
            routesArray.unshift(depotLocation);
            routesArray.push(depotLocation);
            // tslint:disable-next-line:max-line-length
            const depotDate = this.trip.startDate ? ('Start Date: ' + this.ds.formatDateTime(this.trip.startDate)) : ('Loading Date: ' + this.ds.formatDateTime(this.trip.plannedDate));
            const tDepot = '<h3> Depot </h3> <div style="text-align: center"><span>' + depotDate + ' </span><br><span>';
            markersArray.push({ location: depotLocation, tooltip: { text: tDepot, isShown: true } });

            e.component.option({
                routes: [{
                    color: 'red',
                    weight: 2,
                    locations: routesArray
                }],
                markers: markersArray
            });
        });
    }

    selectionChangedHandler(event) {
        const ds = this.gridAddOrder.instance.getDataSource().items();
        const currentId = event.currentSelectedRowKeys[0];
        let refNumber;
        for (const el of ds) {
            if (el.EntityID === event.currentSelectedRowKeys[0]) {
                refNumber = el.ol_refNumber;
                break;
            }
        }
        const temp = [];
        for (const data of ds) {
            if (data.ol_refNumber === refNumber && currentId !== data.EntityID) {
                temp.push(data.EntityID);
            }
        }
        this.gridAddOrder.instance.selectRows(temp, true);

        // Clasa I By Mihai
        let selectButton = 0;
        for (const data of event.selectedRowsData) {
            selectButton += data.ol_quantity;
        }
        this.selected.quantity = selectButton;
        // tslint:disable-next-line:radix
        this.selected.remains = parseInt((this.selected.availableQuantity - selectButton).toFixed(0));
        if (selectButton !== 0) {
            this.btAddOrdersBtnInst.option('disabled', false);
        } else {
            this.btAddOrdersBtnInst.option('disabled', true);
        }
    }

    initAddOrdersGrid() {
        const it = this.gridOrder.instance.getDataSource().items();
        this.selected.availableQuantity = this.trip.capacity;
        for (const or of it) {
            this.selected.availableQuantity -= or.ol_quantity;
        }
        this.selected.availableQuantity.toFixed(0);
        this.selected.remains = this.selected.availableQuantity;
    }

    goBack() {
        window.history.back();
    }

    onCellEditingStart(e: any) {
        if (e.column.dataField === 'ol_loadedQuantity' && (!this.ds.isAdmin() || this.trip.stationId < 5)) {
            e.cancel = true;
            return;
        }

        //SERVER: Pentru statusul != 6 nu se pot edita comenzi fara allowEdit
        if (this.trip.statusId >= 5 || e.data.ol_typeId === '00000000-0000-0000-0000-000000000000') {
            if (!this.ds.isAdmin() && !this.trip.allowEdit && this.trip.statusId == 6 && e.column.dataField === 'ol_deliveredQuantity') {
                e.cancel = true;
            }
            if (e.data.ol_loadedQuantity == null && e.column.dataField === 'ol_deliveredQuantity') {
                e.cancel = true;
                notify('Loaded quantity missing. Delivery quantity can not be edited.', 'error', 3000);
                return;
            }
            if (e.data.ol_statusId < 1) {
                e.cancel = true;
                notify('Order status invalid to edit delivery quantity.', 'error', 3000);
                return;
            }
        }
    }
    onRowUpdating(e) {
        if (e.newData && e.newData.ol_deliveredQuantity) {
            const dq = e.newData.ol_deliveredQuantity;
            if (dq > (dq * 1.2) || dq < (dq * 0.8)) {
                e.cancel = true;
                notify('Delivery quantity can not have a higher tolerance of +/- 20%', 'warning', 3000);
                return;
            }
            if (!e.oldData.ol_deliveryDate) {
                e.newData.ol_deliveryDate = new Date();
            }
            e.newData.ol_statusId = 3;
        }
    }

    onContentGridOrder(e) {
        // this.trip.sendFinish = this.checkStatusAndDeliveryQuantity(e.component.getDataSource().items());
    }

    checkStatusAndDeliveryQuantity(items) {
        for (const it of items) {
            if (it.ol_statusId !== 6 || !it.ol_deliveredQuantity) {
                return false;
            }
        }
        return true;
    }

    calculateSummaryEvent(options, that) {
        if (options.name === 'availableQuantSummary' && options.summaryProcess === 'start') {
            options.totalValue = that.trip.capacity;
        }
        if (options.name === 'availableQuantSummary' && options.summaryProcess === 'calculate') {
            options.totalValue -= options.value;
        }
    }

    customAvailableQuant(itemInfo) {
        return 'Available Quantity: ' + itemInfo.value;
    }

    setHeaderState(tc: number, result: any) {
        if (tc > 0) {
            if (this.ds.isTrans()) {
                if (this.trip.statusId === 0) {
                    this.tripHeaderState = 3; // modifica tot except loading date
                } else {
                    this.tripHeaderState = 2; // read only
                }
            } else {
                this.tripHeaderState = 3; // modifica tot except loading date
            }
        } else {
            this.tripHeaderState = 0; // modifica tot
        }
        if (this.tripHeaderState === 1 || this.tripHeaderState === 3) {
            // tslint:disable-next-line:max-line-length
            this.trip.minplannedDate = new Date(new Date(result.plannedDate).getFullYear(), new Date(result.plannedDate).getMonth(), new Date(result.plannedDate).getDate(), 0, 0, 0);
            this.trip.maxplannedDate = new Date(new Date(result.plannedDate).getFullYear(), new Date(result.plannedDate).getMonth(), new Date(result.plannedDate).getDate(), 23, 58, 0);
        } else {
            this.trip.minplannedDate = undefined;
            this.trip.maxplannedDate = undefined;
        }
    }
    onRowPrepared(rowInfo: any) {
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-6') {
            rowInfo.rowElement.className += ' strike';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-7' &&
            (rowInfo.rowElement.className.indexOf('orderTransPen') < 0)) {
            rowInfo.rowElement.className += ' orderTransPen';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data.ol_statusId === '-8' &&
            (rowInfo.rowElement.className.indexOf('orderTransCan') < 0)) {
            rowInfo.rowElement.className += ' orderTransCan';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data &&
            // tslint:disable-next-line:triple-equals
            (rowInfo.data.ol_deliveryDate || rowInfo.data.ol_statusId == 3) && (rowInfo.rowElement.className.indexOf('orderTransPen') < 0)
            && (rowInfo.rowElement.className.indexOf('orderDeliv') < 0)) {
            rowInfo.rowElement.className += ' orderDeliv';
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data &&
            (rowInfo.data.t_statusId >= 5 || (rowInfo.data.t_statusId === 2 && rowInfo.data.t_comments)) &&
            (rowInfo.rowElement.className.indexOf('orderSent') < 0) && (rowInfo.rowElement.className.indexOf('orderTransPen') < 0)
            && (rowInfo.rowElement.className.indexOf('orderDeliv') < 0)) {
            rowInfo.rowElement.className += ' orderSent';
        }
        this.ds.alternateSoNumberColor(rowInfo, 'ol_refNumber');
    }

    onCellPrepared(e) {
        if (e.rowType === 'data' && e.column.dataField === 'ol_loadedQuantity' && e.data.ol_loadedQuantity) {
            if (Math.abs((e.data.ol_quantity - e.data.ol_loadedQuantity) * 100 / e.data.ol_quantity) > 10) {
                e.cellElement.classList.add('redbk');
            }
        }
        if (e.rowType === 'data' && e.column.dataField === 'ol_requestDate' && e.data.ol_oldRequestDate) {
            e.cellElement.classList.add('redbk');
        }
    }
}
