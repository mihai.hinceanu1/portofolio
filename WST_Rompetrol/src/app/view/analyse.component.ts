import { Component, ViewChild } from '@angular/core';
import { DataService } from '../service/data.service';
import { Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';

@Component({
    selector: 'app-analyse',
    templateUrl: 'analyse.component.html'
})

export class AnalyseComponent {
    dashFromDate: any;
    dashThruDate: any;
    dashTransporter: string;
    current: string;
    checkStock: any;
    currentRow: any;
    dsTransNoTrip: any;
    subnavTabs: Array<any>;
    dsDashboardInfo: any;
    newDate = new Date();

    @ViewChild('gridST') gridST: DxDataGridComponent;
    dsTransporter: CustomStore;
    constructor(public ds: DataService, router: Router) {
        if (!ds.checkForUnauthorized('dashboard')) { return; }
        this.current = sessionStorage.getItem('currentDashboard');
        if (this.current == null) {
            this.ds.isAdmin() ? this.current = 'al' : this.current = 'st';
        }
        this.checkStock = (options) => {
            if (options.value < 0 || this.currentRow.st_capacity < options.value) {
                options.rule.message = 'The stock can\'t be greater then tank capacity or negative.';
                return false;
            } else { return true; }
        };
        this.subnavTabs = this.getSubnavs();
        this.ds.getTransNoTrip('aa').then((result) => {
            this.dsTransNoTrip = result;
        });
        this.dsTransporter = this.ds.getTransporter();

        // dashboard init
        this.dashFromDate = new Date();
        this.dashThruDate = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);
        this.dashTransporter = null;
        this.dsDashboardInfo = {};
        this.onDashValChanged(null);
    }

    indicatorCustomize(indi) {
        return indi + '%';
    }

    onDashValChanged(e) {
        this.ds.getDashboardInfo({ fromDate: this.dashFromDate, thruDate: this.dashThruDate, transporter: this.dashTransporter })
            .then((result) => {
                this.dsDashboardInfo = result;
            });
    }

    getSubnavs() {
        const snav = [{
            id: 1,
            text: 'Stock',
            activeRoute: 'st',
            active: 'true',
            icon: ''
        }];
        if (this.ds.isAdmin()) {
            snav.unshift({
                id: 0,
                text: 'Analytics',
                activeRoute: 'al',
                active: 'true',
                icon: ''
            });
        }
        return snav;
    }

    reloadData(e) {
        if (typeof e === 'string') {
            status = e;
        } else {
            status = this.subnavTabs[e.itemIndex].activeRoute;
        }
        this.current = status;
    }

    onStockUpdate(e) {
        if (e.row && e.row.data) {
            this.currentRow = e.row.data;
        }
    }

    onStockDateChanged(e, that) {
        that.dsStock = that.ds.getStock(that.ds.isStManager() ? that.ds.userId : null, e.value);
    }

    onCellPrepared(e) {
        if (e.rowType === 'data' && (e.column.dataField === 'ts_quantity' || e.column.dataField === 'ts_variation')) {
            if (!e.value) {
                e.cellElement.classList.add('redbk');
            }
        }
    }

    customizeTooltip = (info: any) => {
        return {
            html: '<div><div class="tooltip-header">' +
                info.argumentText + '</div>' +
                '<div class="tooltip-body"><div class="series-name">' +
                info.points[0].seriesName +
                ': </div><div class="value-text">' +
                info.points[0].valueText +
                // '</div><div class="series-name">' +
                // info.points[1].seriesName +
                // ': </div><div class="value-text">' +
                // info.points[1].valueText + '% '
                '</div></div></div>'
        };
    }

    customizeLabelText = (info: any) => {
        return info.valueText + '%';
    }
}
