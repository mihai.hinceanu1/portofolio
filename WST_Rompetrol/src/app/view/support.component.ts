import { Component } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { DataService } from '../service/data.service';
import { Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';

@Component({
    selector: 'app-support',
    templateUrl: './support.component.html'
})

export class SupportComponent {
    cwFormData: any;
    dsOrderStatus: any;
    moveOrder: any;
    removeOrderFromTrip: any;
    changeStatus: any;


    dsTypes: { id: number; name: string; }[];
    dsTripStatusDesc: { id: any; name: any; }[];
    dsOrderStatusDesc: { id: any; name: any; }[];
    dsOrdersTrip: CustomStore;
    constructor(public ds: DataService, private router: Router) {
        if (!ds.checkForUnauthorized('support')) { return; }
        this.cwFormData = { isTrip: false };
        this.dsTripStatusDesc = this.ds.getTripStatusDescription();
        this.dsOrderStatusDesc = this.ds.getOrderStatusDescription();
        this.dsTypes = this.ds.getOrderStatusType();

        this.dsOrderStatus = null;
        this.moveOrder = this.moveOrderEv.bind(this);
        this.removeOrderFromTrip = this.moveOrderEv.bind(this, true);
        this.changeStatus = this.changeStatusEv.bind(this);
    }

    moveOrderEv(ev: any, remove: boolean) {
        this.ds.moveOrderToTrip(this.cwFormData.o_refNumber, this.cwFormData.t_refNumber, remove).then((res) => {
            if (res === 'OK') {
                notify('Order moved successfully', 'success', 3000);
            } else if (res === 'Invalid numbers') {
                notify('Invalid numbers', 'warning', 3000);
            } else if (res === 'Trip not found') {
                notify('Trip not found', 'warning', 3000);
            }
        });
    }

    changeStatusEv(ev: any) {
        const statusId = this.cwFormData.t_statusId === null ? this.cwFormData.o_statusId : this.cwFormData.t_statusId;
        this.ds.changeStatusId(this.cwFormData.soNumber, this.cwFormData.tripNo, statusId).then((res) => {
            if (res === 'OK') {
                notify('Status changed successfully', 'success', 3000);
            } else if (res === 'Invalid numbers') {
                notify('Invalid numbers', 'warning', 3000);
            }
        });
    }

    onSearchTrip(e: any) {
        this.cwFormData.tripDetails = {};
        this.cwFormData.orderDetails = {};
        this.dsOrderStatus = null;
        this.dsOrdersTrip = null;
        if (this.cwFormData.isTrip) {
            this.getTripInfo(this.cwFormData);
            (<any>this.ds.getValuesByTable('Trip', ['id'], ['refNumber'], [this.cwFormData.valueToFind])).single().then(data => {
                this.dsOrdersTrip = this.ds.getOrderByTrip(data.id);
            });
        } else {
            (<any>this.ds.getValuesByTable('OrderLineView', null, ['ol_refNumber'], [this.cwFormData.valueToFind])).single().then(data => {
                if (data) {
                    this.cwFormData.orderDetails = data;
                    this.dsOrderStatus = this.ds.getValuesByTable('OrderLineStatus', null,
                        ['orderId'], [data.ol_id], 'rescheduleDate', true, true);
                    if (this.cwFormData.orderDetails.ol_tripId) {
                        this.cwFormData.tripId = this.cwFormData.orderDetails.ol_tripId;
                        this.getTripInfo(this.cwFormData);
                    }
                }
            });
        }
    }

    getTripInfo(cfd: any) {
        (<any>this.ds.getValuesByTable('TripView', null, [cfd.isTrip ? 't_refNumber' : 't_id'],
            [cfd.isTrip ? cfd.valueToFind : cfd.tripId])).single().then(data => {
                if (data) {
                    cfd.tripDetails = data;
                }
            });
    }

    onFieldDataChanged(item: any) {
        const field = item.dataField;

        switch (field) {
            case 'soNumber':
                if (item.value) {
                    this.cwFormData.tripNo = null;
                }
                break;
            case 'tripNo':
                if (item.value) {
                    this.cwFormData.soNumber = null;
                }
                break;
            case 't_statusId':
                if (item.value) {
                    this.cwFormData.o_statusId = null;
                }
                break;
            case 'o_statusId':
                if (item.value) {
                    this.cwFormData.t_statusId = null;
                }
                break;
        }

        let editor;
        if (item.dataField === 'soNumber' && item.value) {
            editor = item.component.getEditor('tripNo');
            editor.option('value', null);
        } else if (item.dataField === 'tripNo' && item.value) {
            editor = item.component.getEditor('soNumber');
            editor.option('value', null);
        }
    }
}
