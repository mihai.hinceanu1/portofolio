import { Component, ViewChild } from '@angular/core';
import { environment } from '../environments/environment';
import { DataService } from './service/data.service';
import { DxSelectBoxComponent, DxFormComponent } from 'devextreme-angular';
import DataSource from 'devextreme/data/data_source';
import themes from 'devextreme/ui/themes';
import devices from 'devextreme/core/devices';
import { EntityStoreOptions, EntityStore } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
import { HttpClient } from '@angular/common/http';

declare var require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'WST';
  public appVersion;
  dsThemes: DataSource;
  onValueChanged: any;
  currentTheme: any;
  isChangePassVisible: boolean;
  newPass: any;
  passwordComparison: any;
  @ViewChild(DxFormComponent) form: DxFormComponent;

  themesArray = [
    {
      'value': 'generic.light.compact',
      'name': 'Light compact',
      'category': 'Generic compact'
    },
    {
      'value': 'generic.dark.compact',
      'name': 'Dark compact',
      'category': 'Generic compact'
    },
    {
      'value': 'generic.contrast.compact',
      'name': 'Contrast compact',
      'category': 'Generic compact'
    },
    {
      'value': 'generic.carmine.compact',
      'name': 'Carmine compact',
      'category': 'Colors compact'
    },
    {
      'value': 'generic.darkmoon.compact',
      'name': 'Dark moon compact',
      'category': 'Colors compact'
    },
    {
      'value': 'generic.darkviolet.compact',
      'name': 'Dark violet compact',
      'category': 'Colors compact'
    },
    {
      'value': 'generic.greenmist.compact',
      'name': 'Green mist compact',
      'category': 'Colors compact'
    },
    {
      'value': 'generic.softblue.compact',
      'name': 'Soft blue compact',
      'category': 'Colors compact'
    },
    {
      'value': 'ios7.default',
      'name': 'IOs 7',
      'category': 'IOs7'
    },
    {
      'value': 'android5.light',
      'name': 'Android Light',
      'category': 'Android'
    }];

  constructor(public ds: DataService, http: HttpClient) {
    const href = window.location.href;
    if (href.startsWith('http://wst.rompetrol.org')) {
      EntityStoreOptions.DefaultServer = 'http://wst.rompetrol.org:8070/wst/';
    } else {
      EntityStoreOptions.DefaultServer = environment.DefaultUrlServer;
    }
    EntityStoreOptions.Http = http;

    const { version: appVersion } = require('../../package.json');
    this.appVersion = appVersion;

    this.dsThemes = new DataSource({
      store: this.themesArray,
      group: 'category'
    });
    this.isChangePassVisible = false;
    this.passwordComparison = () => {
      return this.form.instance.option('formData').newPass;
    };
    this.newPass = {};

    const dev = devices.current();
    switch (dev.deviceType) {
      case 'phone':
      case 'tablet':
        if (dev.platform === 'ios') {
          themes.current('ios7.default');
        } else {
          themes.current('android5.light');
        }
        break;
      default:
        this.currentTheme = localStorage.getItem('localTheme');
          if (!this.currentTheme || this.themesArray.findIndex((d) => d.value === this.currentTheme) === -1 ) {
            localStorage.setItem('localTheme', 'generic.light.compact');
            this.currentTheme = 'generic.light.compact';
          }
          themes.current(this.currentTheme);
        break;
    }

    this.onValueChanged = (e) => { this.onValueChangedEv(e, this); };

  }

  onValueChangedEv(e, that) {
    themes.current(e.value);
    localStorage.setItem('localTheme', e.value);
  }

  cancelClick() {
    this.isChangePassVisible = false;
    this.newPass = {};
  }

  onSavePass(e) {
    const validation = e.validationGroup.validate();
    if (validation.isValid) {
      this.newPass.userName = this.ds.userName || sessionStorage.getItem('userName');
      EntityStore.execute('ChangePassword', this.newPass).then(result => {
        if (result === 'invalid_currentPass') {
          notify('Current password is incorrect!', 'error', 3000);
        } else if (result === 'OK') {
          this.newPass = {};
          notify('Your passowrd has been changed', 'success', 3000);
          this.isChangePassVisible = false;
        } else {
          notify('The operation can\'t be completed', 'error', 3000);
        }
      }, error => {
        notify('Error from server. New password can\'t be saved', 'error', 3000);
      });
    }
  }
}
