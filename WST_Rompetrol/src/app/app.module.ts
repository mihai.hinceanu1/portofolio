import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {
  DxDataGridModule, DxButtonModule, DxTextBoxModule, DxDateBoxModule, DxSelectBoxModule, DxPopupModule, DxTextAreaModule,
  DxFileUploaderModule, DxLoadPanelModule, DxValidatorModule, DxTabPanelModule, DxMapModule, DxToolbarModule, DxFormModule,
  DxNumberBoxModule, DxTabsModule, DxTagBoxModule, DxDropDownBoxModule, DxListModule, DxChartModule, DxBoxModule,
  DxCircularGaugeModule, DxCheckBoxModule, DxScrollViewModule, DxTreeViewModule, DxLookupModule, DxSwitchModule
} from 'devextreme-angular';
import { AppComponent } from './app.component';
import { NavBarComponent } from './navigation/navbar.component';
import { OrderComponent } from './view/order.component';
import { TripComponent } from './view/trip.component';
import { TripEditComponent } from './view/tripEdit.component';
import { AdminComponent } from './view/admin.component';
import { LoginComponent } from './view/login.component';
import { ReportComponent } from './view/report.component';

import { GasStationComponent } from './common/gasStation.component';

import { DataService } from './service/data.service';
import { AuthGuard } from './service/auth.guard';
import { ChangeOrderComponent } from './common/changeOrder.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SupportComponent } from './view/support.component';
import { AnalyseComponent } from './view/analyse.component';
import { DashboardComponent } from './view/dashboard/dashboard.component';
import { OverviewComponent } from './view/dashboard/overview.component';
import { MetricsAnalysisComponent } from './view/dashboard/metrics-analysis.component';
import { StationOverviewComponent } from './view/dashboard/station-overview.component';
import { DashboardNavigationComponent } from './view/dashboard/navigation.component';
import { StockStationComponent } from './view/dashboard/stock.component';

const routes: Routes = [
  { path: '', redirectTo: 'orders', pathMatch: 'full' },
  {
    path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard],
    children: [
      { path: 'overview', component: OverviewComponent },
      { path: 'metrics', component: MetricsAnalysisComponent },
      { path: 'station', component: StationOverviewComponent },
      { path: 'stock', component: StockStationComponent }
    ]
  },
  { path: 'orders', component: OrderComponent, canActivate: [AuthGuard] },
  { path: 'trips', component: TripComponent, canActivate: [AuthGuard] },
  { path: 'tripEdit', component: TripEditComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'report', component: ReportComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard] },
  { path: 'support', component: SupportComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    AnalyseComponent,
    NavBarComponent,
    OrderComponent,
    TripComponent,
    TripEditComponent,
    LoginComponent,
    AdminComponent,
    ReportComponent,
    GasStationComponent,
    ChangeOrderComponent,
    SupportComponent,
    OverviewComponent,
    MetricsAnalysisComponent,
    StationOverviewComponent,
    DashboardComponent,
    DashboardNavigationComponent,
    StockStationComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpModule, HttpClientModule,
    DxButtonModule,
    DxNumberBoxModule,
    DxTextBoxModule,
    DxTextAreaModule,
    DxDataGridModule,
    DxDateBoxModule,
    DxScrollViewModule,
    DxSelectBoxModule,
    DxSwitchModule,
    DxPopupModule,
    DxLookupModule,
    DxFileUploaderModule,
    DxLoadPanelModule,
    DxValidatorModule,
    DxTabPanelModule,
    DxTreeViewModule,
    DxMapModule,
    DxChartModule,
    DxCheckBoxModule,
    DxBoxModule,
    DxCircularGaugeModule,
    DxFormModule,
    DxToolbarModule,
    DxListModule,
    DxTagBoxModule,
    DxDropDownBoxModule,
    DxTabsModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false })
  ],
  providers: [DataService, AuthGuard, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
