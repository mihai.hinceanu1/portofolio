import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import {
    EntityStore, EntityQuery, EntityConditionGroup,
    EntityConditionOperator, EntityStoreOptions, SimpleFunctionType
} from '@dohu/ibis-entity';
import devices from 'devextreme/core/devices';
import notify from 'devextreme/ui/notify';
import { Http, Headers } from '@angular/http';

@Injectable()
export class DataService {

    public rowAlternationEnabled: boolean;
    public showColumnLines: boolean;
    public showRowLines: boolean;
    public userName: string; public userType: string; public userId: string; allowCalor: boolean;
    public isAuth: boolean;
    public navTabs: Array<any>;
    public isMob: boolean;
    public popupPosition: string;
    public tabsHeight: any;
    public tabsWidth: any;
    public btnHeight: any;
    lastOrderTrip: any;
    alternateClass: boolean;
    headerPost: { headers: Headers; };
    constructor(private router: Router, private activatedRoute: ActivatedRoute, private http: Http) {
        this.headerPost = { headers: new Headers({}) };
        this.showRowLines = true;
        this.showColumnLines = false;
        this.alternateClass = true;
        this.rowAlternationEnabled = true;
        this.isMob = devices.current().deviceType === 'phone' || devices.current().deviceType === 'tablet';
        this.tabsHeight = this.isMob ? 50 : 46;
        this.tabsWidth = window.innerWidth;
        this.btnHeight = this.isMob ? 12 : 14;
        this.popupPosition = this.isMob ? 'top' : 'center';
        this.userType = sessionStorage.getItem('userType');
        this.userName = sessionStorage.getItem('userName');
        this.userId = sessionStorage.getItem('userId');
        this.allowCalor = sessionStorage.getItem('allowCalor') === '1';
        if (this.userType != null && this.userName != null && this.userId != null) {
            this.isAuth = true;
            this.setNavbar();
        } else {
            this.isAuth = false;
            this.navTabs = [];
        }
        this.activatedRoute.queryParams.subscribe(params => {
            if (!EntityStoreOptions.DefaultServer) { return; }
            if (!sessionStorage.getItem('currentBadgesOrdSap')) {
                this.getOrdersCount(params['type'] === 'sap').then(result => {
                    sessionStorage.setItem('currentBadgesOrdSap', JSON.stringify(result));
                });
            }
            if (!sessionStorage.getItem('currentBadgesOrdInt')) {
                this.getOrdersCount(params['type'] !== 'sap').then(result => {
                    sessionStorage.setItem('currentBadgesOrdInt', JSON.stringify(result));
                });
            }
            if (!sessionStorage.getItem('currentBadgesTrip')) {
                this.getTripsCount().then(result => {
                    sessionStorage.setItem('currentBadgesTrip', JSON.stringify(result));
                });
            }
        });

        EntityStoreOptions.OnError = (jqXHR) => {
            if (jqXHR.status === 401) {
                // this.logout();
                // notify('Authorization has expired. Retry to login.', 'error', 3000);
                // this.router.navigateByUrl('/login');
                //   this.isLoginModalVisible = true;
            }
        };

        setInterval(() => {
            this.tabsWidth = window.innerWidth;
        }, 1000);
    }

    static formatDate(date: Date) {
        if (date == null || date === undefined) {
            return '';
        }
        date = new Date(date);
        return EntityStore.toD2(date.getDate()) + '.' +
            EntityStore.toD2((date.getMonth() + 1)) + '.' + date.getFullYear();
    }

    static formatDateMonth(date: Date) {
        if (date == null || date === undefined) {
            return '';
        }
        date = new Date(date);
        return EntityStore.toD2((date.getMonth() + 1)) + '.' + date.getFullYear();
    }

    static formatDateTime(date: Date) {
        if (date == null || date === undefined) {
            return '';
        }
        date = new Date(date);
        return EntityStore.toD2(date.getDate()) + '.' +
            EntityStore.toD2((date.getMonth() + 1)) + '.' +
            date.getFullYear() + ' ' + date.getHours() + ':' + EntityStore.toD2(date.getMinutes());
    }

    public gridHeight() {
        return window.innerHeight - 170;
    }

    public logout() {
        this.isAuth = false;
        this.userType = null;
        this.userName = null;
        this.userId = null;
        this.allowCalor = null;
        sessionStorage.removeItem('userType');
        sessionStorage.removeItem('userName');
        sessionStorage.removeItem('userId');
        sessionStorage.removeItem('allowCalor');
    }

    public login(ut: string) {
        this.isAuth = true;
        const u = ut.split(';');
        this.userType = u[0];
        this.userName = u[1];
        this.userId = u[2];
        this.allowCalor = u[3] === '1';
        sessionStorage.setItem('userType', this.userType);
        sessionStorage.setItem('userName', this.userName);
        sessionStorage.setItem('userId', this.userId);
        sessionStorage.setItem('allowCalor', u[3]);
        this.setNavbar();
        if (this.userType === 'transRep') {
            this.router.navigateByUrl('report');
        } else if (this.userName === 'support') {
            this.router.navigateByUrl('/support');
        } else {
            this.router.navigate(['orders'], { queryParams: { type: 'sap' } });
        }
    }

    public customHeight() {
        const mob = devices.current().deviceType === 'phone' || devices.current().deviceType === 'tablet';
        return mob ? 'calc(100vh - 10px)' : window.innerHeight - 170;
    }

    serverError(error?: string) {
        let msg = '';
        if (error) {
            const start = error.indexOf('The exception message is');
            const end = error.indexOf('The exception stack trace is:');
            msg = error.substring(start + 24, end - 34);
        }
        notify('Server error ' + (error ? ('[' + msg + ']') : ''), 'error', 3000);
    }

    checkForUnauthorized(page: string) {
        if (!this.isAuth) {
            this.router.navigateByUrl('/login');
            return false;
        }
        switch (page) {
            case 'orders':
                if (this.isStManager()) {
                    this.router.navigateByUrl('/dashboard/stock');
                    return false;
                }
                if (this.userType === 'transRep' || this.isSales()) {
                    this.router.navigateByUrl('/report');
                    return false;
                }
                break;
            case 'overview':
            case 'site':
            case 'metrics':
            case 'admin':
                if (!this.isAdmin()) {
                    this.router.navigateByUrl('/login');
                    return false;
                }
                break;
            case 'support':
                if (!this.isSuperAdmin() && !this.isSupport()) {
                    this.router.navigateByUrl('/login');
                    return false;
                }
                break;
            default:
                return true;
        }
        return true;
    }


    public isAdmin(): boolean {
        return this.userType === 'admin';
    }

    public isSuperAdmin(): boolean {
        return this.userName === 'huzzy' || this.userName === 'george' || this.userName === 'amalia';
    }

    public isSupport(): boolean {
        return this.userName === 'support';
    }

    public isTrans(): boolean {
        return this.userType === 'trans' || this.userType === 'transRep' || this.userType === 'transAdmin';
    }

    public isStManager(): boolean {
        return this.userType === 'stManager';
    }

    public isSales(): boolean {
        return this.userType === 'sales';
    }

    public getDate(rowData: any) {
        const col = this as any;
        return DataService.formatDate(rowData[col.dataField]);
    }

    public getDateMonth(rowData: any) {
        const col = this as any;
        return DataService.formatDateMonth(rowData[col.dataField]);
    }

    getDateTime(rowData: any) {
        const col = this as any;
        return DataService.formatDateTime(rowData[col.dataField]);
    }

    public getMessages() {
        return [
            'Clientul doreste reprogramarea',
            'Nu sunt clienti in zona pentru a incarca o cisterna',
            'Comanda venita tarziu (>17:00)',
            'Comanda zi pe zi',
            'Motive tehnice (cisterna defecta)',
            'Lipsa carburant depozit',
            'Alt motiv'
        ];
    }

    gridLoadState() {
        const that = this as any;
        return new Promise((resolve, reject) => {
            const state = localStorage.getItem(that.storageKey);
            let st = null;
            if (state !== null) {
                try {
                    st = JSON.parse(state);
                } catch {
                }
            }
            resolve(st);
        });
    }

    gridSaveState(state) {
        if (state === null) {
            return;
        }
        const that = this as any;
        state.searchText = null;
        state.filterValue = null;
        if (state.columns) {
            for (const c of state.columns) {
                c.filterValues = undefined;
            }
        }
        localStorage.setItem(that.storageKey, JSON.stringify(state));
    }

    convertToDate(date: any) {
        const d = new Date(date);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    // functia care trimite datele de calculat pentru headerul graficelor qDelivery, unitCost si Reschedule
    getOverviewHeaderData(obj: any, chart: string) {
        if (chart === 'unitCost') {
            const q = new EntityQuery('Volumes').eq('ol_statusId', 3).addOrderByDesc(['t.plannedDate']);
            if (obj.fromDate) { q.gte('t_plannedDate', EntityStore.toDateTimeFilter(this.convertToDate(obj.fromDate))); }
            if (obj.thruDate) { q.lte('t_plannedDate', EntityStore.toDateTimeFilter(this.convertToDate(obj.thruDate))); }
            if (obj.productName) { q.eq('p_name', obj.productName); }
            if (obj.transporterName) { q.eq('tr_name', obj.transporterName); }
            if (obj.depotName) { q.eq('d_name', obj.depotName); }
            return new CustomStore(EntityStore.store(q)).load();
        }

        if (chart === 'reschedule') {
            const q = new EntityQuery('OrderLineView').addOrderByDesc(['ol.requestDate']);
            if (obj.fromDate) { q.gte('ol_requestDate', EntityStore.toDateTimeFilter(this.convertToDate(obj.fromDate))); }
            if (obj.thruDate) { q.lte('ol_requestDate', EntityStore.toDateTimeFilter(this.convertToDate(obj.thruDate))); }
            if (obj.productName) { q.eq('p_name', obj.productName); }
            if (obj.transporterName) { q.eq('tr_name', obj.transporterName); }
            if (obj.depotName) { q.eq('d_name', obj.depotName); }
            return new CustomStore(EntityStore.store(q)).load();
        }
    }

    // functia care trimite datele de facut dataSource pentru graficele qDelivery, unitCost si Reschedule
    getOverviewChartData(thruDate: any, chart: string) {
        if (chart === 'unitCost') {
            // sunt aduse doar comenzile cu status Finished (ol_statusId 3)
            const q = new EntityQuery('Volumes').eq('ol_statusId', 3).addOrderByDesc(['t.plannedDate']);
            const fDate = new Date(new Date(thruDate).setMonth(new Date(thruDate).getMonth() - 12));
            if (thruDate) {
                q.gte('t_plannedDate', EntityStore.toDateTimeFilter(this.convertToDate(fDate)));
                q.lte('t_plannedDate', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
            }
            return new CustomStore(EntityStore.store(q)).load();
        }

        if (chart === 'reschedule') {
            // ol_statusId -3 sunt comenzile cu status reschedule
            const q = new EntityQuery('OrderLineView').eq('ol_statusId', -3).addOrderByDesc(['ol.requestDate']);
            const fDate = new Date(new Date(thruDate).setMonth(new Date(thruDate).getMonth() - 12));
            if (thruDate) {
                q.gte('ol_requestDate', EntityStore.toDateTimeFilter(this.convertToDate(fDate)));
                q.lte('ol_requestDate', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
            }
            return new CustomStore(EntityStore.store(q)).load();
        }
    }

    getOverviewChartFilteredData(thruDate: any) {
        // sunt aduse doar comenzile cu status Finished (ol_statusId 3)
        const q = new EntityQuery('Volumes').eq('ol_statusId', 3).addOrderByDesc(['t.plannedDate']);
        const fDate = new Date(new Date(thruDate).setMonth(new Date(thruDate).getMonth() - 12));
        if (thruDate) {
            q.gte('t_plannedDate', EntityStore.toDateTimeFilter(this.convertToDate(fDate)));
            q.lte('t_plannedDate', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
        }
        return new CustomStore(EntityStore.store(q)).load();
    }

    getOverviewChartRescheduleOrder(thruDate: any) {
        const q = new EntityQuery('OrderLineView').eq('ol_statusId', -3).addOrderByDesc(['ol.requestDate']);
        const fDate = new Date(new Date(thruDate).setMonth(new Date(thruDate).getMonth() - 12));
        if (thruDate) {
            q.gte('ol_requestDate', EntityStore.toDateTimeFilter(this.convertToDate(fDate)));
            q.lte('ol_requestDate', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
        }
        return new CustomStore(EntityStore.store(q)).load();
    }

    getStationFilteredData(obj: any, table: string) {
        const q = new EntityQuery(table).addOrderByDesc(['st.stockDate']);
        if (obj.fromDate) { q.gte('st_stockDate', EntityStore.toDateTimeFilter(this.convertToDate(obj.fromDate))); }
        if (obj.thruDate) { q.lte('st_stockDate', EntityStore.toDateTimeFilter(this.convertToDate(obj.thruDate))); }
        if (obj.productName) { q.eq('p_name', obj.productName); }
        if (obj.transporterName) { q.eq('tr_name', obj.transporterName); }
        // if (obj.depotName) { q.eq('', obj.depotName); }
        if (obj.siteName) { q.eq('s_name', obj.siteName); }
        // if (obj.tankName) { q.eq('', obj.tankName); }
        return new CustomStore(EntityStore.store(q)).load();
    }

    getConfig(): CustomStore {
        return new CustomStore(EntityStore.store(new EntityQuery('Config').addOrderBy(['key'])));
    }

    getAuditTrail() {
        return new CustomStore(EntityStore.store(new EntityQuery('AuditTrail').addOrderByDesc(['eventDate'])));
    }

    addEditComment(obj: any) {
        return EntityStore.fromQuery(new EntityQuery('Comment')).insert({ id: obj.ol_id, description: obj.description });
    }

    getOrdersForTrip(tripId: string) {
        const cso = EntityStore.store(new EntityQuery('OrderLineView').eq('ol.tripId', tripId).addOrderBy(['ol.orderIndex']),
            true, 'ol_id');
        return new CustomStore(cso);
    }

    updateModel(model: any, id: string, obj: any) {
        return EntityStore.fromQuery(new EntityQuery(model)).update(id, obj);
    }

    checkForTickets(ords) {
        return EntityStore.execute('CheckOrderTicket', { orderId: ords });
    }

    getOrderRangeDetails(refNumber: string, rangeVal: string) {
        const rq = new EntityQuery('Range').eq('value', rangeVal);
        rq.fields.push('id');
        const orq = new EntityQuery('OrderLine').eq('refNumber', refNumber);
        orq.fields.push('id');
        return EntityStore.fromQuery(orq.link('rangeId', 'id', rq)).query();
    }

    alternateSoNumberColor(rowInfo, field) {
        // added row alternate by soNumber
        if (!this.lastOrderTrip && rowInfo.data && rowInfo.rowType !== 'header') {
            this.lastOrderTrip = rowInfo.data[field];
            this.alternateClass = true;
        }
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data[field] !== this.lastOrderTrip) {
            this.lastOrderTrip = rowInfo.data[field];
            this.alternateClass = !this.alternateClass;
        }
        const cls = this.alternateClass === true ? 'hgTrip' : 'lwTrip';
        if (rowInfo.rowType !== 'header' && rowInfo.data && rowInfo.data[field] === this.lastOrderTrip &&
            (rowInfo.rowElement.className.indexOf(cls) < 0)) {
            rowInfo.rowElement.className += ' ' + cls;
        }
    }

    getOrdersToAddTrip(depotId, transporterId, requestDate) {
        const q = new EntityQuery('OrderLineView').eq('ol.statusId', 1).eq('ol.tripId', null).
            eq('ol.requestDate', EntityStore.toDateFilter(requestDate)).eq('ol.transporterId', transporterId).addOrderBy(['ol.refNumber']).
            link('ol.depotId', 'id', new EntityQuery('Depot').linkEq('name', 'name', 'Depot', 'id', depotId));
        if (this.allowCalor) {
            q.like('p.name', 'Calor%');
        } else {
            q.addCondition('p.name', EntityConditionOperator.NotLike, 'Calor%', true, true);
        }
        return new CustomStore(EntityStore.store(q, false, 'ol_id'));
    }

    getTrip(status): CustomStore {
        const q = new EntityQuery('TripView').addOrderByDesc(['t.refNumber']);
        switch (status) {
            case 'new':
                q.eq('t.statusId', 0);
                break;
            case 'pn':
                q.eq('t.statusId', 1);
                break;
            case 'ap':
                q.eq('t.statusId', 2);
                break;
            case 'sts':
                q.in('t.statusId', [3, 4]);
                break;
            case 'pg':
                q.eq('t.statusId', 5);
                break;
            case 'fin':
                q.eq('t.statusId', 6);
                break;
        }
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
            q.eq('hasCalor', this.allowCalor);
        }
        return new CustomStore(EntityStore.store(q, false, 't_id'));
    }

    getTransferOrder(): CustomStore {
        const q = new EntityQuery('TripTransferView').eq('tt.approvedDate', null).addOrderBy(['t.refNumber']);
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        }
        const cso = EntityStore.store(q, true, 'tt_id');
        return new CustomStore(cso);
    }

    getOrderType(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('OrderType').addOrderBy(['name']), false, 'id');
        return new CustomStore(cso);
    }

    getDepot(filterByName = false): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Depot').addOrderBy(['name']), false, filterByName ? 'name' : 'id');
        return new CustomStore(cso);
    }

    getPlainDepot() {
        const q = new EntityQuery('Depot').addOrderBy(['name']);
        q.distinct = true;
        q.fields.push('name');
        return EntityStore.fromQuery(q, true, 'name').function(null);
    }

    getPlainTransporter() {
        const q = new EntityQuery('Transporter').addOrderBy(['name']);
        q.distinct = true;
        q.fields.push('name');
        return EntityStore.fromQuery(q, true, 'name').function(null);
    }

    getDepotName(): CustomStore {
        const q = new EntityQuery('Depot').addOrderBy(['name']);
        q.distinct = true;
        q.fields.push('name');
        return new CustomStore(EntityStore.store(q, true, 'name'));
    }

    getDepotById(depotId) {
        const pr = new Promise((resolve, reject) => {
            const q = new EntityQuery('Depot').eq('id', depotId);
            EntityStore.fromQuery(q).single().then((result) => {
                resolve(result);
            }, (err) => {
                reject(err);
            });
        });
        return pr;
    }

    getDriver(transporterId, isActiv?): CustomStore {
        const q = new EntityQuery('Driver').addOrderBy(['lastName', 'firstName']);
        if (transporterId) {
            q.eq('transporterId', transporterId);
        }
        if (isActiv) {
            q.eq('isActiv', true);
        }
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getCost(transporterId: string): CustomStore {
        const q = new EntityQuery('TransportCost').addOrderBy(['value']);
        if (transporterId) {
            q.eq('transporterId', transporterId);
        }
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getTaxes(): CustomStore {
        const q = new EntityQuery('TransportTax').addOrderBy(['monthYear']);
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getEta(param: any): CustomStore {
        const q = new EntityQuery('Eta').eq('ol.typeId', '00000000-0000-0000-0000-000000000000').addOrderByDesc(['t.plannedDate']);
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('t.plannedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('t.plannedDate', EntityStore.toDateTimeFilter(param.fromDate));

        if (param.depotId && param.depotId.length > 0) { q.in('d.name', param.depotId); }
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        } else if (param.transporterId && param.transporterId.length > 0) {
            q.in('tr.name', param.transporterId);
        }
        const cso = EntityStore.store(q, false, 'ol_id');
        return new CustomStore(cso);
    }

    getDrivers() {
        const cso = EntityStore.store(new EntityQuery('Driver').eq('isActiv', true).addOrderBy(['lastName', 'firstName']), false, 'id');
        return new CustomStore(cso);
    }

    getProduct(filterByName = false): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Product').addOrderBy(['name']), false, filterByName ? 'name' : 'id');
        return new CustomStore(cso);
    }

    getTankName() {
        const cso = EntityStore.store(new EntityQuery('Tank'));
        return new CustomStore(cso);
    }

    getProductDiesel(): CustomStore {
        const q = new EntityQuery('Product').addOrderBy(['name']);
        const group = new EntityConditionGroup();
        group.useOr = true;
        group.like('name', '%diesel%').like('name', '%motorina%');
        q.conditionGroups.groups.push(group);
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getUser(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('UserLogin').addOrderBy(['loginName']), false, 'id');
        return new CustomStore(cso);
    }

    getAlert(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Alert').addOrderByDesc(['eventDate']));
        return new CustomStore(cso);
    }

    getStationUser(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('UserLogin').eq('typeId', 12).addOrderBy(['loginName']));
        return new CustomStore(cso);
    }

    getTransporter(filterByName = false): CustomStore {
        const q = new EntityQuery('Transporter').addOrderBy(['name']);
        return new CustomStore(EntityStore.store(q, false, filterByName ? 'name' : 'id'));
    }

    getCustomer(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Customer').addOrderBy(['name']), false, 'id');
        return new CustomStore(cso);
    }

    getStation(): CustomStore {
        const q = new EntityQuery('Station').addOrderBy(['Name']);
        if (this.isTrans()) {
            q.eq('transporterId', this.userId);
        }
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getDashboardInfo(param: any) {
        param.fromDate = DataService.formatDate(param.fromDate);
        param.thruDate = DataService.formatDate(param.thruDate);
        if (param.transporter == null) {
            param.transporter = '';
        }
        return EntityStore.execute('GetDashboardInfo', param);
    }

    getDSReport(param: any): CustomStore {
        const q = new EntityQuery('DeliverySchedule').neq('t.id', null).addOrderBy(['car.registrationNo', 't.orderIndex', 'ol.orderIndex']);
        param.date.setHours(0);
        param.date.setMinutes(0);
        param.date.setSeconds(0);
        param.date.setMilliseconds(0);
        const nextDay = new Date(param.date);
        nextDay.setDate(param.date.getDate() + 1);
        q.lt('t.plannedDate', EntityStore.toDateTimeFilter(nextDay));
        q.gte('t.plannedDate', EntityStore.toDateTimeFilter(param.date));
        if (param.depotId && param.depotId.length > 0) { q.in('d.name', param.depotId); }
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        } else if (param.transporterId && param.transporterId.length > 0) {
            q.in('tr.name', param.transporterId);
        }
        const cso = EntityStore.store(q);
        return new CustomStore(cso);
    }

    getLSReport(param: any) {
        const q = new EntityQuery('TripView').gt('t.statusId', 1).addOrderBy(['t.refNumber', 'c.registrationNo', 't.orderIndex']);
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('t.estimatedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('t.estimatedDate', EntityStore.toDateTimeFilter(param.fromDate));
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        } else if (param.transporterId && param.transporterId.length > 0) {
            q.in('tr.name', param.transporterId);
        }
        if (param.depotId && param.depotId.length > 0) { q.in('d.name', param.depotId); }

        const cso = EntityStore.store(q, false, 't_id');
        return new CustomStore(cso);
    }

    formatDateTime(d: string): string {
        // tslint:disable-next-line:max-line-length
        const dd = new Date(d);
        return dd.getDate() + '.' + (dd.getMonth() + 1) + '.' + dd.getFullYear() + ' ' + dd.getHours() + ':' + dd.getMinutes();
    }

    getDRReport(eta: Date): CustomStore {
        const q = new EntityQuery('DeliveriesReport').addOrderBy(['depot']);
        q.eq('requestDate', EntityStore.toDateFilter(eta));
        const cso = EntityStore.store(q);
        return new CustomStore(cso);
    }

    getSSReport(transporterId: any, stockDate: Date): CustomStore {
        const q = new EntityQuery('StockStation').addOrderBy(['s.code']);
        if (this.isTrans()) {
            q.eq('s.transporterId', this.userId);
        } else if (transporterId && transporterId.length > 0) {
            q.in('tr.name', transporterId);
        }
        const yes = new Date(stockDate.getFullYear(), stockDate.getMonth(), stockDate.getDate() - 1);
        q.eq('st.stockDate', EntityStore.toDateFilter(yes));
        return new CustomStore(EntityStore.store(q));
    }

    getROReport(fromDate: Date, thruDate: Date): CustomStore {
        if (!fromDate || !thruDate) {
            notify('Need to select the period!', 'warning', 3000);
            return;
        }
        thruDate.setDate(thruDate.getDate() + 1);
        const q = new EntityQuery('RescheduledOrder').addOrderBy(['ol.requestDate']);
        q.lt('ol.requestDate', EntityStore.toDateFilter(thruDate));
        q.gte('ol.requestDate', EntityStore.toDateFilter(fromDate));
        if (this.isTrans()) {
            q.eq('ol.transporterId', this.userId);
        }
        const cso = EntityStore.store(q);
        return new CustomStore(cso);
    }

    getTUReport(param: any) {
        const q = new EntityQuery('TankUtilization').addOrderBy(['t.estimatedDate']);
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('t.estimatedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('t.estimatedDate', EntityStore.toDateTimeFilter(param.fromDate));
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        } else if (param.transporterId && param.transporterId.length > 0) {
            q.in('tr.name', param.transporterId);
        }
        if (param.depotId && param.depotId.length > 0) { q.in('d.name', param.depotId); }
        const cso = EntityStore.store(q, false, 't_id');
        return new CustomStore(cso);
    }

    getVOLReport(param: any) {
        const q = new EntityQuery('Volumes').addOrderBy(['t.plannedDate']);
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('t.plannedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('t.plannedDate', EntityStore.toDateTimeFilter(param.fromDate));
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        }
        const cso = EntityStore.store(q);
        return new CustomStore(cso);
    }

    getLOReport(param: any) {
        const q = new EntityQuery('Losses').addOrderBy(['t.plannedDate']);
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('t.plannedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('t.plannedDate', EntityStore.toDateTimeFilter(param.fromDate));
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        }
        const cso = EntityStore.store(q);
        return new CustomStore(cso);
    }

    getRTReport(param: any) {
        const q = new EntityQuery('RejectedTrips');
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('t.plannedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('t.plannedDate', EntityStore.toDateTimeFilter(param.fromDate));
        q.fields.push('transporter', 'depot', 'userName', 'type');
        return EntityStore.fromQuery(q).function(<any>[
            { field: 'ts.id', ft: SimpleFunctionType.Count },
        ]);
    }

    getCCReport() {
        const q = new EntityQuery('CostCenter').addOrderBy(['c.name']);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getTRReport(param: any) {
        const q = new EntityQuery('TripTransferView').addOrderByDesc(['tt.approvedDate']);
        param.fromDate.setHours(0);
        param.fromDate.setMinutes(0);
        param.fromDate.setSeconds(0);
        param.fromDate.setMilliseconds(0);
        param.thruDate.setDate(param.thruDate.getDate() + 1);

        q.lt('tt.approvedDate', EntityStore.toDateTimeFilter(param.thruDate));
        q.gte('tt.approvedDate', EntityStore.toDateTimeFilter(param.fromDate));
        if (this.isTrans()) {
            q.eq('t.transporterId', this.userId);
        } else if (param.transporterId && param.transporterId.length > 0) {
            q.in('tr.name', param.transporterId);
        }
        return new CustomStore(EntityStore.store(q, false, 'tt_id'));
    }

    getACReport(param: any) {
        return EntityStore.execute('GetAC', {
            fromDate: DataService.formatDate(param.fromDate),
            thruDate: DataService.formatDate(param.thruDate)
        });
    }

    getDZReport(param: any) {
        const obj: any = {};
        obj.depotId = (param.depotId && param.depotId.length > 0) ? param.depotId.join(';') : '';
        obj.transporterId = (param.transporterId && param.transporterId.length > 0) ? param.transporterId.join(';') : '';
        obj.fromDate = DataService.formatDate(param.fromDate);
        obj.thruDate = DataService.formatDate(param.thruDate);
        return EntityStore.execute('GetDZ', obj);
    }

    getRAReport(param) {
        const q = this.createRAQuery(param);
        q.fields.push('customerCategory');

        return EntityStore.fromQuery(q).function(<any>[
            { field: 'noOrders', ft: SimpleFunctionType.Sum },
            { field: 'deliveredOnTime', ft: SimpleFunctionType.Sum },
            { field: 'deliveredBefore', ft: SimpleFunctionType.Sum },
            { field: 'rescheduled', ft: SimpleFunctionType.Sum },
            { field: 'quantity3', ft: SimpleFunctionType.Sum },
            { field: 'quantity5', ft: SimpleFunctionType.Sum },
            { field: 'recBt45', ft: SimpleFunctionType.Sum },
            { field: 'recAf5', ft: SimpleFunctionType.Sum },
            { field: 'tft', ft: SimpleFunctionType.Sum }
        ]);
    }

    createRAQuery(param): EntityQuery {
        const qd = new EntityQuery('Depot');
        const qt = new EntityQuery('Transporter');
        const q = new EntityQuery('RescheduledAnalyze').addOrderBy(['customerCategory']);
        param.thruDate.setDate(param.thruDate.getDate() + 1);
        q.lt('ol.requestDate', EntityStore.toDateFilter(param.thruDate));
        q.gte('ol.requestDate', EntityStore.toDateFilter(param.fromDate));
        if (this.isTrans()) {
            q.eq('ol.transporterId', this.userId);
        } else if (param.transporterId && param.transporterId.length > 0) {
            qt.in('name', param.transporterId);
            q.link('ol.transporterId', 'id', qt);
        }
        if (param.depotId && param.depotId.length > 0) {
            qd.in('name', param.depotId);
            q.link('ol.depotId', 'id', qd);
        }
        return q;
    }

    getOrdersCustomer(customerCategory: string, param) {
        const raQ = this.createRAQuery(param);
        raQ.eq('customerCategory', customerCategory);
        const q = new EntityQuery('OrderLineView').eq('reschedule', true)
            .link('ol.refNumber', 'ol.refNumber', raQ).addOrderBy(['ol.refNumber']);
        const cso = EntityStore.store(q, false, 'ol_id');
        return new CustomStore(cso);
    }

    getStock(stationId, stockDate?: Date): CustomStore {
        const q = new EntityQuery('StationTankStockView').addOrderBy(['s.code', 'st.code']);
        if (stockDate) {
            stockDate.setHours(-1, 0, 0, 0);
            const nextDay = new Date(stockDate);
            nextDay.setDate(stockDate.getDate() + 1);
            q.lt('ts.stockDate', EntityStore.toDateTimeFilter(nextDay));
            q.gte('ts.stockDate', EntityStore.toDateTimeFilter(stockDate));
        }
        if (stationId) {
            q.eq('s.id', stationId);
        }
        if (this.isTrans()) {
            if (this.allowCalor) {
                q.like('p.name', 'Calor%');
            }
            q.linkEq('s.id', 'id', 'Station', 'transporterId', this.userId);
        }
        const cso = EntityStore.store(q, true, 'ts_id');
        return new CustomStore(cso);
    }

    getTransNoTrip(param: any) {
        // param.fromDate.setHours(0);
        // param.fromDate.setMinutes(0);
        // param.fromDate.setSeconds(0);
        // param.fromDate.setMilliseconds(0);
        const q = new EntityQuery('Transporter'); // .gte('plannedDate', param.fromDate).lt('plannedDate', param.thruDate);
        q.link('id', 'transporterId', new EntityQuery('Trip'));
        q.fields.push('name');
        return EntityStore.fromQuery(q).function(<any>[
            { field: 'id', ft: SimpleFunctionType.Count },
        ]);
    }

    getStationRange(stationId): CustomStore {
        const cso = EntityStore.store(new EntityQuery('StationRange').eq('stationId', stationId), false, 'id');
        return new CustomStore(cso);
    }

    getRange(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Range').addOrderBy(['code']), false, 'id');
        return new CustomStore(cso);
    }

    getCustomerAddr(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('CustomerAddress').addOrderBy(['region']));
        return new CustomStore(cso);
    }

    getCostCenter(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('CostCenter').neq('ca.costCenter', null).addOrderBy(['ca.costCenter']));
        return new CustomStore(cso);
    }

    getStationTank(stationId) {
        const q = new EntityQuery('StationTank').addOrderBy(['code']);
        if (stationId != null) {
            q.eq('stationId', stationId);
        }
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getAddr(customerId): CustomStore {
        const cso = EntityStore.store(new EntityQuery('CustomerAddress')
            .eq('customerId', customerId).addOrderBy(['region', 'city']), false, 'id');
        return new CustomStore(cso);
    }

    getAddressLikeRC(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('CustomerAddress').like('code', 'R%').addOrderBy(['description']), false, 'id');
        return new CustomStore(cso);
    }

    getCar(transporterId, isActiv?): CustomStore {
        const q = new EntityQuery('Car').addOrderBy(['registrationNo']);
        if (transporterId != null) {
            q.eq('transporterId', transporterId);
        } if (isActiv) {
            q.eq('isActiv', true);
        }
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getCarById(carId) {
        const q = new EntityQuery('Car').eq('id', carId);
        q.fields.push('id', 'driverId');
        return EntityStore.fromQuery(q, false, 'id').single();
    }

    getTank(transporterId, isActiv?): CustomStore {
        const q = new EntityQuery('Tank').addOrderBy([]);
        if (transporterId != null) {
            q.eq('transporterId', transporterId);
        }
        if (isActiv) {
            q.eq('isActiv', true);
        }
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    updateLoadedTripQuantity(tripId: string, quant: number) {
        return EntityStore.fromQuery(new EntityQuery('Trip')).update(tripId, { manuallyLoadedQuantity: quant });
    }

    loadTrip(tripId: string, quant: number) {
        return EntityStore.execute('LoadTrip', { tripId: tripId, quant: quant });
    }

    uploadFile(fileData: File, id: string) {
        return new Promise((resolve, reject) => {
            const file: File = fileData;
            const formData: FormData = new FormData();

            // const blob = this.b64toBlob(fileData, 'image');
            formData.append('uploadFile', file, id + '_img.png');
            this.http.post(EntityStoreOptions.DefaultServer + 'UploadFile', formData, this.headerPost).subscribe(
                () => {
                    resolve();
                }, error => {
                    reject();
                });
        });
    }

    b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        const sliceSize = 512;

        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    moveOrderToTrip(orderNo: string, tripNo: string, remove: boolean) {
        const obj: any = { tripNo: tripNo, orderNo: orderNo };
        if (remove) {
            obj.remove = 'null';
        }
        return EntityStore.execute('MoveOrderToTrip', obj);
    }

    changeStatusId(orderNo: string, tripNo: string, statusId: string) {
        return EntityStore.execute('ChangeStatus', { orderNo: orderNo, tripNo: tripNo, statusId: statusId });
    }

    getCapacityForTank(tankId): any {
        const q = new EntityQuery('Tank').eq('id', tankId);
        q.fields.push('capacity', 'typeId');
        return EntityStore.fromQuery(q, false, 'id').single();
    }

    getOrderByTrip(tripId): CustomStore {
        const q = new EntityQuery('OrderLineView').eq('ol.tripId', tripId).addOrderBy(['ol.orderIndex']);
        return new CustomStore(EntityStore.store(q, true, 'ol_id'));
    }

    getOrderHistory(tripId: string): CustomStore {
        const q = new EntityQuery('OrderLineView').eq('ol.tripId', tripId).eq('ol.statusId', 3).addOrderBy(['ol.orderIndex']);
        q.fields.push('ol.id', 'ol.refNumber', 'c.name', 'ol.deliveryDate', 'ol.deliveredQuantity', 'hasTicket');
        return new CustomStore(EntityStore.store(q, false, 'ol_id'));
    }

    getTripLoading(tripId): CustomStore {
        const q = new EntityQuery('TripLoading').eq('tripId', tripId).addOrderBy(['receivedDate']);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getOrderStatus() {
        return [{
            id: '-8', name: 'Canceled'
        }, {
            id: '-7', name: 'Pending transfer'
        }, {
            id: '-6', name: 'Storno'
        }, {
            id: '-5', name: 'BO Validated'
        }, {
            id: '-4', name: 'Request reschedule'
        }, {
            id: '-3', name: 'Rescheduled'
        }, {
            id: '-2', name: 'Deleted',
        }, {
            id: '-1', name: 'Rejected'
        }, {
            id: '0', name: 'Pending'
        }, {
            id: '1', name: 'Approved'
        }, {
            id: '2', name: 'In progress'
        }, {
            id: '3', name: 'Finished'
        }];
    }

    getValuesByTable(table: string, fields?: string[], eqFields?: string[], eqValues?: string[],
        orderBy?: string, isDesc = false, isStore = false) {
        const q = new EntityQuery(table);
        if (fields && fields.length > 0) {
            for (const f of fields) {
                q.fields.push(f);
            }
        }
        if (eqFields && eqFields.length > 0 && eqValues && eqValues.length > 0) {
            for (let i = 0; i < eqFields.length; i++) {
                q.eq(eqFields[i], eqValues[i]);
            }
        }
        if (orderBy) {
            if (isDesc) {
                q.addOrderByDesc([orderBy]);
            } else {
                q.addOrderBy([orderBy]);
            }
        }
        if (isStore === true) {
            return new CustomStore(EntityStore.store(q));
        } else {
            return EntityStore.fromQuery(q);
        }
    }

    getOrder(status: string, notNull: boolean = true): CustomStore {
        const q = new EntityQuery('OrderLineView').addOrderByDesc(['ol.requestDate', 'ol.refNumber']);

        if (!this.isTrans()) {
            if (notNull) {
                q.neq('ol.typeId', null).neq('ol.typeId', '00000000-0000-0000-0000-000000000000');
            } else {
                const group = new EntityConditionGroup();
                group.useOr = true;
                group.eq('ol.typeId', null).eq('ol.typeId', '00000000-0000-0000-0000-000000000000');
                q.conditionGroups.groups.push(group);
            }
        } else {
            if (this.allowCalor) {
                q.like('p.name', 'Calor%');
            } else {
                q.addCondition('p.name', EntityConditionOperator.NotLike, 'Calor%', true, true);
            }
            q.eq('ol.transporterId', this.userId);
        }

        switch (status) {
            case 'bo':
                q.eq('ol.statusId', -5);
                const yes = new Date(new Date().setDate(new Date().getDate() - 1));
                q.gt('ol.requestDate', EntityStore.toDateFilter(yes));
                break;
            case 'rs':
                q.eq('ol.statusId', -4);
                break;
            case 'rej':
                q.eq('ol.statusId', -1);
                break;
            case 'pn':
                q.eq('ol.statusId', 0);
                break;
            case 'ap':
                q.eq('ol.statusId', 1);
                break;
            case 'pg':
                q.eq('ol.statusId', 2);
                break;
            case 'fin':
                q.eq('ol.statusId', 3);
                break;
        }
        return new CustomStore(EntityStore.store(q, false, 'ol_id'));
    }

    getMeterType(): Array<{ id, name }> {
        return [{ id: 0, name: 'Flowmeter' }, { id: 1, name: 'Dipstick' }];
    }

    getTripStatusDescription(): Array<{ id, name }> {
        return [
            { id: 0, name: 'New' },
            { id: 1, name: 'Pending' },
            { id: 2, name: 'Approved' },
            { id: 3, name: 'Send to SAP' },
            { id: 4, name: 'Send to SAP' },
            { id: 5, name: 'In Progress' },
            { id: 6, name: 'Finished' }
        ];
    }

    getOrderStatusDescription(): Array<{ id, name }> {
        return [
            { id: '-8', name: 'Canceled' },
            { id: '-7', name: 'Transfering' },
            { id: '-6', name: 'Storno' },
            { id: '-5', name: 'BO Val' },
            { id: '-4', name: 'Req scheduled' },
            { id: '-3', name: 'Rescheduled' },
            { id: '-2', name: 'Deleted' },
            { id: '-1', name: 'Rejected' },
            { id: '0', name: 'Pending' },
            { id: '1', name: 'Approved' },
            { id: '2', name: 'In progress' },
            { id: '3', name: 'Finished' }
        ];
    }

    getOrderStatusType(): Array<{ id, name }> {
        return [{ id: 0, name: 'Created' },
        { id: 1, name: 'Approved' },
        { id: 2, name: 'Rejected' },
        { id: 3, name: 'Reschedule' }];
    }

    getOrdersCount(sap: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            EntityStore.execute('GetOrdersCount',
                { userId: this.isTrans() ? this.userId : null, allowCalor: this.allowCalor ? 1 : 0, sap: sap }).then((result) => {
                    resolve({
                        tr: result['-7'] || 0,
                        bo: result['-5'] || 0,
                        rs: result['-4'] || 0,
                        pn: result['0'] || 0,
                        ap: result['1'] || 0,
                        pg: result['2'] || 0,
                        fin: result['3'] || 0
                    });
                });
        });
    }

    getTripsCount(): Promise<any> {
        return new Promise((resolve, reject) => {
            EntityStore.execute('GetTripsCount',
                { userId: this.isTrans() ? this.userId : null, allowCalor: this.allowCalor ? 1 : 0 }).then((result) => {
                    resolve({
                        new: result['0'] || 0,
                        pn: result['1'] || 0,
                        ap: result['2'] || 0,
                        sts: (result['3'] || 0) + (result['4'] || 0),
                        pg: result['5'] || 0,
                        fin: result['6'] || 0
                    });
                });
        });
    }

    setNavbar() {
        this.navTabs = [];
        this.navTabs.push({
            id: -1,
            route: '',
            icon: this.isMob ? '/assets/img/Logo1.png' : '/assets/img/rompetrol.svg'
        });
        if (this.userType === 'transRep') {
            this.navTabs.push({
                id: 4,
                text: 'Reports',
                route: '/report',
                queryParam: '',
                active: false,
                icon: ''
            }, {
                    id: 6,
                    text: '',
                    route: '/login',
                    queryParam: '',
                    active: 'false',
                    icon: 'assets/img/logout.png'
                });
            return;
        }
        if (this.isStManager() || this.isTrans() || this.isAdmin()) {
            this.navTabs.push({
                id: 0,
                text: 'Dashboard',
                route: (this.isStManager() || this.isTrans()) ? '/dashboard/stock' : '/dashboard/overview',
                queryParam: 'sap',
                activeRoute: '/dashboard',
                active: false,
                icon: ''
            });
        }
        if (!this.isStManager()) {
            if (!this.isSales()) {
                this.navTabs.push({
                    id: 1,
                    text: 'Orders',
                    route: '/orders',
                    queryParam: 'sap',
                    activeRoute: '/orders?type=sap',
                    active: false,
                    icon: ''
                });
            }
            if (!this.isTrans() && !this.isSales()) {
                this.navTabs.push({
                    id: 2,
                    text: 'WST Orders',
                    route: '/orders',
                    queryParam: 'internal',
                    activeRoute: '/orders?type=internal',
                    active: 'false',
                    icon: ''
                });
            }
            this.navTabs.push({
                id: 3,
                text: 'Trips',
                route: '/trips',
                queryParam: '',
                active: false,
                icon: ''
            });
            this.navTabs.push({
                id: 4,
                text: 'Reports',
                route: '/report',
                queryParam: '',
                active: false,
                icon: ''
            });
            if (!this.isTrans() && !this.isSales()) {
                this.navTabs.push({
                    id: 5,
                    text: 'Admin',
                    route: '/admin',
                    queryParam: '',
                    active: false,
                    icon: ''
                });
            }
        }
        if (this.isSupport()) {
            this.navTabs.push({
                id: 5,
                text: 'Support',
                route: '/support',
                active: true
            });
        }
        this.navTabs.push({
            id: 7,
            text: '',
            route: '/login',
            queryParam: '',
            active: 'false',
            icon: 'assets/img/logout.png'
        });
    }

    allThemes() {
        const themesArray = [
            {
                'value': 'light.compact',
                'name': 'Light compact',
                'category': 'Generic compact'
            },
            {
                'value': 'dark.compact',
                'name': 'Dark compact',
                'category': 'Generic compact'
            },
            {
                'value': 'contrast.compact',
                'name': 'Contrast compact',
                'category': 'Generic compact'
            },
            {
                'value': 'carmine.compact',
                'name': 'Carmine compact',
                'category': 'Colors compact'
            },
            {
                'value': 'darkmoon.compact',
                'name': 'Dark moon compact',
                'category': 'Colors compact'
            },
            {
                'value': 'darkviolet.compact',
                'name': 'Dark violet compact',
                'category': 'Colors compact'
            },
            {
                'value': 'greenmist.compact',
                'name': 'Green mist compact',
                'category': 'Colors compact'
            },
            {
                'value': 'softblue.compact',
                'name': 'Soft blue compact',
                'category': 'Colors compact'
            },
            {
                'value': 'ios7.default',
                'name': 'IOs 7',
                'category': 'IOs7'
            },
            {
                'value': 'android5.light',
                'name': 'Android Light',
                'category': 'Android'
            }];
        return themesArray;
    }
}
