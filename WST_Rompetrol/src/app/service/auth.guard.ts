import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(public location: Location) { }
    canActivate() {
        const ut = sessionStorage.getItem('userType');
        const path = this.location.path();
        if (ut && ut === 'transRep' && path.indexOf('/report') < 0 && path.indexOf('/login') < 0 && path === '') {
            return false;
        }
        const us = sessionStorage.getItem('userName');
        if (us && us === 'support' && path.indexOf('/support') < 0 && path === '') {
            return false;
        }
        return true;
    }
}
