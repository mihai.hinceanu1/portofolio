import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../service/data.service';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'app-change-order',
    templateUrl: 'changeOrder.component.html'
})
export class ChangeOrderComponent {
    @Input() isStateVisible: boolean;
    @Input() po: any;

    @Output() save = new EventEmitter<any>();
    @Output() cancel = new EventEmitter<any>();
    @Input() dsMessages: string[];

    isCommentsReadOnly: boolean;
    onCommentsValChanged: any;
    dsDepot: any;

    constructor(public ds: DataService) {
        this.onCommentsValChanged = (e) => { this.onCommentsValChangedEv(e, this); };
        this.ds.getPlainDepot().then((result) => {
            this.dsDepot = [];
            for (const it of result) {
                this.dsDepot.push(it.name);
            }
        });
    }

    onCommentsValChangedEv(e, th) {
        if (th.po.type !== -6) {
            this.isCommentsReadOnly = e.value !== 'Alt motiv';
            th.po.comments = e.value === 'Alt motiv' ? '' : e.value;
        }
    }

    acceptClick() {
        if (this.po.comments) { this.po.comments = this.po.comments.trim(); }
        if ((this.po.type === 2 || this.po.type === 3) && (!this.po.comments || this.po.comments.length < 10)) {
            notify('Comments should be at least 10 letters', 'error', 1500);
            return;
        }
        if (this.po.type === 3 || this.po.type === -4) {
            if (this.po.rescheduleDate == null) {
                notify('The reschedule date field is empty!', 'error', 1500);
                return;
            }
            const yesterday = new Date().setDate(new Date().getDate() - 1);
            if (!this.ds.isAdmin() && this.po.rescheduleDate <= yesterday) {
                notify('You cannot choose a date before today', 'error', 1500);
                return;
            }
        }
        this.save.emit(this.po);
    }
    cancelClick() {
        this.cancel.emit();
    }
}
