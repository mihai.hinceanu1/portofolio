import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { DataService } from '../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { DxDataGridComponent } from 'devextreme-angular';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import DevExpress from 'devextreme/bundles/dx.all';
import { alert } from 'devextreme/ui/dialog';

@Component({
    selector: 'app-gas-station',
    templateUrl: 'gasStation.component.html'
})
export class GasStationComponent {
    dsDepot: CustomStore;
    dsStation: CustomStore;
    dsTransporter: CustomStore;

    onStationChanged: any;
    onDepotChanged: any;

    @Input() gsTank: any;
    @Input() gasVisible: boolean;
    @Input() gs: any;
    @Output() save = new EventEmitter<any>();
    @Output() cancel = new EventEmitter<any>();
    @Output() popupHidden = new EventEmitter<any>();

    @ViewChild('gridGasStation') gridGasStation: DxDataGridComponent;
    constructor(public ds: DataService) {
        this.dsStation = ds.getStation();
        this.dsDepot = ds.getDepot();
        this.dsTransporter = ds.getTransporter();

        this.onStationChanged = (item) => { this.onStationChangedEvent(item, this); };
        this.onDepotChanged = (item) => { this.onDepotChangedEvent(item, this); };
    }

    saveGS(event) {
        if (this.gs.rangeId === '-') {
            alert('The station and depot selected have no set range distance! \n Please contact Logistic!', 'Station Range Error');
            return;
        }
        this.gridGasStation.instance.saveEditData();
        const validation = event.validationGroup.validate();
        if (validation.isValid && this.checkEmptyField() && this.validateGasQuantity()) {
            this.gridGasStation.instance.focus();
            this.save.emit(this.gsTank);
            setTimeout(() => {
                this.gsTank = [];
                this.gridGasStation.instance.option('dataSource', this.gsTank);
            }, 200);
        }
    }

    checkEmptyField() {
        let haveOrder = false;
        for (const a of this.gsTank) {
            if (a.order) {
                haveOrder = true;
                break;
            }
        }
        return haveOrder;
    }

    validateGasQuantity() {
        const gridItems = this.gridGasStation.instance.getDataSource().items();
        for (const data of gridItems) {
            if (data.order <= 0) {
                return false;
            }
        }
        return true;
    }

    getMaxQuantity(e) {
        return e.capacity - e.stock;
    }

    cancelGS(event) {
        this.cancel.emit(event);
        this.gasVisible = false;
    }

    onStationChangedEvent(data, that) {
        if (data.value && data.value.id) {
            that.gs.stationId = data.value.id;
            that.gs.addressId = data.value.addressId;

            if (that.gs.depotId) { that.getRange(data.value.id, that.gs.depotId); }
            const q = new EntityQuery('StationTankView').eq('st.stationId', data.value.id).addOrderBy(['codes']);
            if (that.gs.product) { q.eq('p.name', that.gs.product); }
            EntityStore.fromQuery(q).load().then((result: any) => {
                that.gsTank = result.data;
                that.gridGasStation.instance.option('dataSource', that.gsTank);
            });
            const qe = new EntityQuery('CustomerAddress').eq('id', that.gs.addressId);
            qe.fields.push('description');
            EntityStore.fromQuery(qe).single().then((result) => {
                that.gs.addressDesc = result.description;
            });
        }
    }
    onDepotChangedEvent(data, that) {
        if (data.value && that.gs.stationId) {
            that.getRange(that.gs.stationId, data.value);
        }
    }

    getRange(stationId, depotId) {
        // tslint:disable-next-line:max-line-length
        const q = new EntityQuery('Range').link('id', 'rangeId', new EntityQuery('StationRange').eq('stationId', stationId).eq('depotId', depotId));
        EntityStore.fromQuery(q).single().then((result) => {
            if (result) {
                this.gs.rangeId = result.value;
            } else {
                this.gs.rangeId = '-';
            }
        });
    }

    onPopupHidden(event) {
        this.popupHidden.emit(event);
        this.gsTank = [];
        this.gridGasStation.instance.option('dataSource', this.gsTank);
    }

    // onValidationCallback(options) {
    //     if (options.value > 0) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
