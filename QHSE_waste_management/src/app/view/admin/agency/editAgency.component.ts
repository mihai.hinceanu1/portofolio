import { Component, Input } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import DataSource from 'devextreme/data/data_source';

@Component({
    selector: 'app-edit-agency',
    templateUrl: 'editAgency.component.html'
})
export class EditAgencyComponent {
    @Input() agencyData: any;
    dsCounty: CustomStore;
    dsLocality: any;

    onCountyChange: any;
    onLocalityInit: any;
    localityComponent: any;

    constructor(private ds: DataService) {
        this.dsCounty = this.ds.geographicAreas(2, 'id');
        this.onCountyChange = (data) => { this.onCountyChangeEv(data, this); };
        this.onLocalityInit = (data) => { this.onLocalityInitEv(data, this); };
        setTimeout(() => {
            if (this.agencyData.countyId) {
                this.dsLocality = this.ds.getLocalitiesByCountyId(this.agencyData.countyId);
            } else {
                this.setLocalityDataSource();
            }
        }, 0);
    }

    onCountyChangeEv(event, that) {
        if (event.event) {
            that.ds.resetValidation(that.localityComponent.component);
        }
        if (event.value) {
            that.localityComponent.component.option('dataSource', that.ds.getLocalitiesByCountyId(event.value));
        } else if (event.value === null) {  // vine null doar la a doua deschidere de popup
            that.setLocalityDataSource();
        }
    }

    setLocalityDataSource() {
        this.dsLocality = new DataSource({
            store: this.ds.geographicAreas(1, 'id'),
            paginate: true,
            pageSize: 50
        });
    }

    onLocalityInitEv(event, that) {
        that.localityComponent = event;
    }
}
