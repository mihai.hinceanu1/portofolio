import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { confirm } from 'devextreme/ui/dialog';
import { DxDataGridComponent } from 'devextreme-angular';
import { UIService } from '../../../service/ui.service';

@Component({
    templateUrl: 'agency.component.html'
})
export class AgencyComponent implements OnInit {
    dsCounty: CustomStore;
    dsLocality: any;
    dsAgency: CustomStore;

    isAgencyVisible: boolean;

    currentAgency: any;
    onEditAgencyClick: any;
    onDeleteAgencyClick: any;

    @ViewChild('gridAgency') gridAgency: DxDataGridComponent;
    onSaveAgency: any;

    constructor(public ds: DataService, public ui: UIService) {
        this.onEditAgencyClick = this.onEditAgency.bind(this);
        this.onDeleteAgencyClick = this.onDeleteAgency.bind(this);
        this.onSaveAgency = this.onSaveAgencyEv.bind(this);
    }

    ngOnInit() {

        this.dsAgency = this.ds.getAgencies();
        this.dsLocality = this.ds.geographicAreas(1, 'id');
        this.dsCounty = this.ds.geographicAreas(2, 'id');
        this.currentAgency = new Agency();

        this.isAgencyVisible = false;

    }

    onEditAgency(e: any) {
        e.event.preventDefault();
        this.ds.getAgencyById(e.row.data.a_id).then((result) => {
            this.currentAgency = result;
            this.ds.getLocalityById(result.localityId).then((locality) => {
                this.currentAgency.countyId = locality.parentId;
                this.isAgencyVisible = true;
            });
        }, (error) => {
            this.ds.serverError(error);
        });
    }

    onDeleteAgency(e: any) {
        e.event.preventDefault();
        confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').then(val => {
            if (val) {
                this.ds.removeAgency(e.row.data.a_id).then(() => {
                    notify('Success', 'success', 2000);
                    this.gridAgency.instance.refresh();
                }, err => {
                    this.ds.serverError(err);
                });
            }
        });
    }

    onSaveAgencyEv(event: any) {
        event.event.preventDefault();
        const validation = event.validationGroup.validate();
        if (!validation.isValid) {
            notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
            return;
        }
        const obj = {
            name: this.currentAgency.name,
            contactInfo: this.currentAgency.contactInfo,
            localityId: this.currentAgency.localityId,
            address: this.currentAgency.address
        };

        this.ds.addEditAgency(obj, this.currentAgency.id).then((result: any) => {
            this.whenDone();
        }, error => {
            this.ds.serverError(error);
        });
    }

    whenDone() {
        this.isAgencyVisible = false;
        this.currentAgency = new Agency();
        notify('Success', 'success', 2000);
        this.gridAgency.instance.refresh();
    }

    toolbarPreparing(event: any) {
        this.ui.prepareToolbar(event, true);
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                hint: 'Adaugă', icon: 'assets/img/grid_add.svg', onClick: (e: any) => {
                    this.currentAgency = {};
                    this.isAgencyVisible = true;
                }
            },
            location: 'after'
        });
    }
}

class Agency {
    EntityID: string;
    id: string;
    name: string;
    localityId: string;
    countyId: string;
    address: string;
    contactInfo: string;
    constructor() {
        this.name = null;
        this.localityId = null;
        this.address = null;
        this.contactInfo = null;
        this.countyId = null;
    }
}
