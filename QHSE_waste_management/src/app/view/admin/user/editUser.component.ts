import { Component, Input } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';


@Component({
    selector: 'app-edit-user',
    templateUrl: 'editUser.component.html'
})
export class EditUserComponent {
    @Input() userData: any;
    dsGaRegions: CustomStore;
    dsRoles: CustomStore;

    constructor(private ds: DataService) {
        this.dsGaRegions = this.ds.geographicAreas(3, 'id');
        this.dsRoles = this.ds.getRoles();
    }
}
