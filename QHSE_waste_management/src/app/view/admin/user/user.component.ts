import { Component, OnInit, ViewChild, NgModule } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { confirm } from 'devextreme/ui/dialog';
import { DxDataGridComponent, DxPopupModule, DxDataGridModule, DxButtonModule, DxTabPanelModule, DxSelectBoxModule } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { UIService } from '../../../service/ui.service';
import { iBisAuthService } from '@dohu/ibis-auth';
import { CommonModule } from '@angular/common';
import { InviteUserModule } from '../../../common/invite-user/invite-user.component';

@Component({
    templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {
    dsUsers: CustomStore;
    dsEnt: CustomStore;
    // dsWorkCenter: CustomStore;
    dsGaRegions: CustomStore;
    dsRoles: CustomStore;
    dsAllRoles: any = [];

    changeUserRole: any;
    openChangeRole: any;
    onRoleChanged: any;

    getFilteredWc: any;

    changeCurrentRole: any;
    currentRoleId: any;
    isChangeRoleVisible: boolean;
    isInviteUserVisible: boolean;

    @ViewChild('gridUsers') gridUsers: DxDataGridComponent;
    @ViewChild('gridWorkstation') gridWorkstation: DxDataGridComponent;


    constructor(public ds: DataService, public ui: UIService, public auth: iBisAuthService) {
        this.changeUserRole = this.changeUserRoleEv.bind(this);
        this.openChangeRole = this.openChangeRoleEv.bind(this);
        this.onRoleChanged = this.onRoleChangedEv.bind(this);
        this.getFilteredWc = this.getFilteredWcEv.bind(this);
    }

    ngOnInit() {
        this.dsUsers = this.ds.securityUsers();
        this.dsEnt = this.ds.getParty('pc');
        // this.dsWorkCenter = this.ds.getParty('wc');
        this.dsGaRegions = this.ds.geographicAreas(3, 'id');
        this.dsRoles = this.ds.getRoles();
        this.currentRoleId = 0;

        this.dsAllRoles.push({ id: 0, name: 'Toate rolurile' });
        this.ds.getRoles().load().then((res) => {
            res.data.forEach(element => {
                this.dsAllRoles.push(element);
            });
        });

    }

    getFilteredWcEv(options) {
        return {
            store: this.ds.getParty('wc'),
            filter: options.data ? ['parentId', '=', options.data.entityId] : null
        }
    }

    openChangeRoleEv(e: any) {
        e.event.preventDefault();
        this.ds.getRoleByUserId(e.row.data.id).then((result: any) => {
            if (result && result.length > 0) {
                this.changeCurrentRole = { id: result[0]['f0'], name: result[0]['f1'], arId: result[0]['f2'] };
                this.isChangeRoleVisible = true;
            } else {
                notify('Schimbarea de rol nu este posibila', 'warning', 3000);
            }
        });
    }

    onUserChangeRole(e: any, newValue: string) {
        if (newValue === this.changeCurrentRole.id) {
            notify('Invalid role', 'error', 3000);
            return;
        }
        this.ds.updateUserRole(newValue, this.changeCurrentRole.arId).then(result => {
            notify('Rol schimbat cu succes', 'success', 3000);
            this.isChangeRoleVisible = false;
            e.component.option('value', null);
            this.gridUsers.instance.refresh();
        }, err => {
            this.ds.serverError(err);
        });
    }

    changeUserRoleEv(e: any) {
        return e.row.data.id !== this.auth.user.id;
    }

    onRoleChangedEv(e: any) {
        this.currentRoleId = e.value;
        if (e.value) {
            this.gridUsers.instance.option('dataSource', this.ds.getUsersByRoleId(e.value));
        } else if (e.value === 0) {
            this.gridUsers.instance.option('dataSource', this.ds.getUsersByRoleId(null));
        }
    }

    // master detail grid

    onWcContentReady(event: any, data: any) {
        if (event.component.option('dataSource') == null) {
            event.component.option('dataSource', this.ds.userLoginParty(data.id));
        }
    }

    onZoneContentReady(event: any, data: any) {
        if (event.component.option('dataSource') == null) {
            event.component.option('dataSource', this.ds.userLoginZone(data.id));
        }
    }

    onSetCellValueEntity(rowData: any, value: any): void {
        rowData.workCenterId = null;
        (<any>this).defaultSetCellValue(rowData, value);
    }

    onRowInserting(event: any, user) {
        event.data.userId = user.id;
    }

    onInviteClose(event: any) {
        this.isInviteUserVisible = false;
    }

    toolbarPreparing(event: any, userGrid = false) {

        if (userGrid) {
            this.ui.prepareToolbar(event, true);
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    hint: 'Adaugă', icon: 'assets/img/grid_add.svg', onClick: (e: any) => {
                        // adaugat invite
                        if (this.currentRoleId === 0) {
                            notify('Selectează un rol', 'warning', 3000);
                            return;
                        }
                        this.isInviteUserVisible = true;
                    }
                },
                location: 'after'
            });

            event.toolbarOptions.items.unshift({
                widget: 'dxSelectBox',
                options: {
                    dataSource: this.dsAllRoles,
                    displayExpr: 'name',
                    valueExpr: 'id',
                    onValueChanged: this.onRoleChanged,
                    value: this.dsAllRoles[0].id
                },
                location: 'after'
            })
        } else {
            this.ui.prepareToolbar(event, false);
        }
    }
}


@NgModule({
    imports: [
        CommonModule,
        DxButtonModule,
        DxButtonModule,
        DxPopupModule,
        DxDataGridModule,
        DxSelectBoxModule,
        DxTabPanelModule,
        InviteUserModule,
    ],
    declarations: [
        UserComponent
    ],
    exports: [UserComponent]
})
export class UserModule { }

