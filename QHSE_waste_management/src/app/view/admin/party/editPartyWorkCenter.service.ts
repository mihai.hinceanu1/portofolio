import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import { EditUnitService } from '../unit';

@Injectable()
export class EditPartyWorkCenterService extends EditUnitService {

    constructor() {
        super();
        this.validation = 'wcValidation';
    }

    createDefault(): any {
        return { isActiv: true, isSubcontractor: false, typeId: 4 };
    }
}
