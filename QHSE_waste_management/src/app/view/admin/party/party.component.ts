import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { DataService } from '../../../service/data.service';
import { UIService } from '../../../service/ui.service';
import { EditPartyService } from './editParty.service';
import { EditPartyWorkCenterService } from './editPartyWorkCenter.service';

@Component({
    templateUrl: 'party.component.html',
    providers: [EditPartyWorkCenterService]
})
export class PartyComponent implements OnInit {
    dsBoolean: { id: any; name: any; }[];
    dsActivity: { id: string; name: string; }[];
    dsParty: CustomStore;

    allowEditContractor: boolean;

    parentId: any;
    onDeletePartyClick: any;
    onEditPartyClick: any;
    onEditDetailsPartyClick: any;

    @ViewChild('gridParty') gridParty: DxDataGridComponent;
    @ViewChild('gridWC') gridWC: DxDataGridComponent;

    constructor(public ds: DataService, public ui: UIService, public edit: EditPartyService,
        public editWC: EditPartyWorkCenterService) {

            this.onEditPartyClick = this.onEditParty.bind(this);
            this.onDeletePartyClick = this.onDeleteParty.bind(this);
            this.onEditDetailsPartyClick = this.onEditDetailsParty.bind(this);
    }

    ngOnInit() {
        this.dsParty = this.ds.getPartyView('trt', false);

        this.dsActivity = this.ds.getActivityType();
        this.dsBoolean = this.ds.boolean();
        this.allowEditContractor = true;
    }

    onEditParty(e: any) {
        e.event.preventDefault();
        this.edit.showPopup(e.row.data.p_id, true).then(() => this.gridParty.instance.refresh());
    }

    onDeleteParty(e: any) {
        e.event.preventDefault();
        this.edit.remove(e.row.data.p_id).then(() => {
            notify('Success', 'success', 2000);
            this.gridParty.instance.refresh();
        });
    }

    partyToolbarPreparing(event: any) {
        this.ui.prepareToolbar(event, true);
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                hint: 'Adaugă', icon: 'assets/img/grid_add.svg', onClick: () =>
                    this.edit.showPopup(null).then(() => this.gridParty.instance.refresh())
            },
            location: 'after'
        });
    }

    // master detail
    onWCInit(e: any, id: string) {
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getWorkCenterByParty(id, true));
            this.parentId = id;
        }
    }

    onEditDetailsParty(e: any) {
        e.event.preventDefault();
        this.editWC.showPopup(e.row.data.p_id, true).then(() => this.gridWC.instance.refresh());
    }

    wcToolbarPreparing(event: any) {
        this.ui.prepareToolbar(event, false);
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                hint: 'Adaugă', icon: 'assets/img/grid_add.svg', onClick: () => {
                    const newWC = this.editWC.createDefault();
                    newWC.parentId = this.parentId;
                    this.editWC.showPopup(newWC).then(() => event.component.refresh());
                }
            },
            location: 'after'
        });
        event.toolbarOptions.items.unshift({
            location: 'before',
            text: 'Puncte de lucru'
        });
    }
}
