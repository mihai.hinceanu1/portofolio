import { Injectable } from '@angular/core';
import { EditUnitService } from '../unit';

@Injectable()
export class EditPartyService extends EditUnitService {

    validation: string;

    constructor() {
        super();
        this.validation = 'partyValidation';
    }

    createDefault() {
        return { isActiv: true, isSubcontractor: false, typeId: 3 };
    }

    getById(id: string): Promise<any> {
        return new Promise<any>( (resolve, reject) => {
            super.getById(id).then((obj) => {
                if (obj.activitiesType) {
                    obj.activitiesTypeSplit = obj.activitiesType.split(', ');
                }
                resolve(obj);
            }, (e) => reject(e));
        });
    }

    onSaveEv(): Promise<any> {
        this.model.activitiesType = (this.model.activitiesTypeSplit || []).join(', ');
        return super.onSaveEv();
    }
}
