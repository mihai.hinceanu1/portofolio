import { Component, OnInit, ViewChild } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { confirm } from 'devextreme/ui/dialog';
import { DxDataGridComponent } from 'devextreme-angular';
import { DataService } from '../../../service/data.service';
import { UIService } from '../../../service/ui.service';

@Component({
  selector: 'app-nomenclature',
  templateUrl: './nomenclature.component.html'
})
export class NomenclatureComponent implements OnInit {

  dsNomenclatureTypes: CustomStore;
  isNomenPopupVisible: boolean;
  nomen: any;
  onDeleteNomenclaturesClick: any;
  onEditNomenclaturesClick: any;

  @ViewChild('gridNomenclatureType') gridNomenclatureType: DxDataGridComponent;
  @ViewChild('gridNomenclatureVal') gridNomenclatureVal: DxDataGridComponent;
  onSaveNomen: any;

  constructor(public ds: DataService, public ui: UIService) {
    this.onEditNomenclaturesClick = this.onEditNomenclatures.bind(this);
    this.onDeleteNomenclaturesClick = this.onDeleteNomenclatures.bind(this);

    this.onSaveNomen = this.onSaveNomenEv.bind(this);
  }

  ngOnInit() {
    this.dsNomenclatureTypes = this.ds.nomenclaturesType();
    this.isNomenPopupVisible = false;
    this.nomen = {};
  }

  onContentReady(e: any, type: any) {
    if (e.component.option('dataSource') == null) {
      e.component.option('dataSource', this.ds.nomenclaturesVal(type.id));
    }
  }

  onEditNomenclatures(e: any) {
    e.event.preventDefault();
    this.isNomenPopupVisible = true;
    this.nomen = e.row.data;
  }

  onDeleteNomenclatures(e: any) {
    e.event.preventDefault();
    confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').then(val => {
      if (val) {
        this.ds.removeNomen(e.row.data.id).then(() => {
          notify('Succes', 'success', 2000);
          this.gridNomenclatureType.instance.refresh();
        }, err => {
          this.ds.serverError(err);
        });
      }
    });
  }

  onSaveNomenEv(event: any) {
    event.event.preventDefault();
    const validation = event.validationGroup.validate();
    if (!validation.isValid) {
      notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
      return;
    }
    let obj = {};
    if (this.nomen.typeId) {
      obj = { code: this.nomen.code, description: this.nomen.description, typeId: this.nomen.typeId };
      this.ds.addEditNomenclatures(obj, this.nomen.id, 'EnumValue').then((result: any) => {
        this.whenDone();
      }, error => {
        this.ds.serverError(error);
      });
    } else {
      obj = { code: this.nomen.code, description: this.nomen.description };
      this.ds.addEditNomenclatures(obj, this.nomen.id, 'EnumType').then((result: any) => {
        this.whenDone();
      }, error => {
        this.ds.serverError(error);
      });
    }


  }

  whenDone() {
    this.isNomenPopupVisible = false;
    this.nomen = {};
    this.nomen.typeId ? this.gridNomenclatureVal.instance.refresh() : this.gridNomenclatureType.instance.refresh();
    notify('Success', 'success', 2000);

  }

  toolbarPreparing(event: any, nomenType?: any) {
    if (nomenType) {
      this.ui.prepareToolbar(event, false);
    } else {
      this.ui.prepareToolbar(event, true);
    }

    if (this.ds.isSuperAdmin) {
      event.toolbarOptions.items.unshift({
        widget: 'dxButton',
        options: {
          hint: 'Adaugă', icon: 'assets/img/grid_add.svg', onClick: (e: any) => {
            this.nomen = {};
            if (nomenType) {
              this.nomen.typeId = nomenType.id;
            }
            this.isNomenPopupVisible = true;
          }
        },
        location: 'after'
      });
    }
  }
}
