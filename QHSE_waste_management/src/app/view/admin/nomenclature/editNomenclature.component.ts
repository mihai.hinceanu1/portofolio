import { Component, Input } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';


@Component({
    selector: 'app-edit-nomenclatures',
    template: `
    <dx-scroll-view height='100%'>
        <dx-form [colCount]="1" [formData]='nomenData' [showValidationSummary]="false" validationGroup="nomenValidation">
            <dxi-item itemType="group" [colSpan]="1" [colCount]='1'>
                <dxi-item [label]="{text: 'Cod'}" dataField="code" editorType="dxTextBox" [editorOptions]="{ }">
                    <dxi-validation-rule type="required"></dxi-validation-rule>
                </dxi-item>
            </dxi-item>
            <dxi-item itemType="group" [colSpan]="1" [colCount]='1'>
                <dxi-item [label]="{text: 'Descriere'}" dataField="description" editorType="dxTextBox" [editorOptions]="{}">
                    <dxi-validation-rule type="required"></dxi-validation-rule>
                </dxi-item>
            </dxi-item>
        </dx-form>
    </dx-scroll-view>`
})

export class EditNomenclatureComponent {

    @Input() nomenData: any;


    constructor(private ds: DataService) {
    }
}
