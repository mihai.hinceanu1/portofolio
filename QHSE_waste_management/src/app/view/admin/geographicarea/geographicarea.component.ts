import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { confirm } from 'devextreme/ui/dialog';
import { DxDataGridComponent } from 'devextreme-angular';
import { UIService } from '../../../service/ui.service';

@Component({
    templateUrl: 'geographicarea.component.html'
})
export class GeographicareaComponent implements OnInit {
    dsGa: CustomStore;

    isGeoAreasVisible: boolean;
    currentGeo: any;
    onEditGareasClick: any;
    onDeleteGareasClick: any;

    @ViewChild('gridGa') gridGa: DxDataGridComponent;
    onSaveGeo: any;

    constructor(public ds: DataService, public ui: UIService) {
        this.onEditGareasClick = this.onEditGareas.bind(this);
        this.onDeleteGareasClick = this.onDeleteGareas.bind(this);

        this.onSaveGeo = this.onSaveGeoEv.bind(this);
    }

    ngOnInit() {
        this.dsGa = this.ds.geographicAreas(null);
        this.isGeoAreasVisible = false;
        this.currentGeo = new GeoAreas();
    }

    onEditGareas(e: any) {
        e.event.preventDefault();
        this.isGeoAreasVisible = true;
        this.currentGeo = e.row.data;
    }

    onDeleteGareas(e: any) {
        e.event.preventDefault();
        confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').then(val => {
            if (val) {
                this.ds.removeGeographicArea(e.row.data.id).then(() => {
                    notify('Success', 'success', 2000);
                    this.gridGa.instance.refresh();
                }, err => {
                    this.ds.serverError(err);
                });
            }
        });
    }

    onSaveGeoEv(event: any) {
        event.event.preventDefault();
        const validation = event.validationGroup.validate();
        if (!validation.isValid) {
            notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
            return;
        }
        const obj = {
            name: this.currentGeo.name,
            typeId: this.currentGeo.typeId,
            parentId: this.currentGeo.parentId
        };

        this.ds.addEditGeographicArea(obj, this.currentGeo.id).then((result: any) => {
            this.whenDone();
        }, error => {
            this.ds.serverError(error);
        });
    }

    whenDone() {
        this.isGeoAreasVisible = false;
        this.currentGeo = new GeoAreas();
        notify('Success', 'success', 2000);
        this.gridGa.instance.refresh();
    }

    toolbarPreparing(event: any) {
        this.ui.prepareToolbar(event, true);
        if (this.ds.isSuperAdmin) {
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    hint: 'Adaugă', icon: 'assets/img/grid_add.svg', onClick: (e: any) => {
                        this.currentGeo = {};
                        this.isGeoAreasVisible = true;
                    }
                },
                location: 'after'
            });
        }
    }

}

class GeoAreas {
    id: string;
    name: string;
    typeId: string;
    parentId: string;
    constructor() {
        this.name = null;
        this.typeId = null;
        this.parentId = null;
    }
}

