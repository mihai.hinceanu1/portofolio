import { Component, Input } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';


@Component({
    selector: 'app-edit-garea',
    templateUrl: 'editGarea.component.html'
})
export class EditGareasComponent {

    @Input() GareasData: any;
    dsParent: CustomStore;
    onParentInit: any;
    onTypeChange: any;

    constructor(public ds: DataService) {
        this.dsParent = this.ds.geographicAreas(null, 'id');
        this.onParentInit = (ev) => { this.onParentInitEv(ev, this); };
        this.onTypeChange = (ev) => { this.onTypeChangeEv(ev, this); };
    }

    onTypeChangeEv(event, that) {
        if (event.event) {
            switch (event.value) {
                case 1:
                    that.GareasData.parentId = null;
                    that.parentComponent.component.option('dataSource', this.ds.geographicAreas(2));
                    break;
                case 2:
                    that.GareasData.parentId = null;
                    that.parentComponent.component.option('dataSource', this.ds.geographicAreas(3));
                    break;
                case 3:
                    that.parentComponent.component.option('dataSource', []);

                    break;
            }
        }
    }

    onParentInitEv(event, that) {
        that.parentComponent = event;
    }
}
