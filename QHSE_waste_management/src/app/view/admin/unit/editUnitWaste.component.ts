import { Component } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { EditUnitWasteService } from './editUnitWaste.service';

@Component({
    selector: 'app-edit-unit-waste',
    templateUrl: './editUnitWaste.component.html'
})

export class EditUnitWasteComponent {
    onWasteTypeChange: any;
    dsWasteType: CustomStore;
    dsStorageType: CustomStore;

    constructor(public ds: DataService, public edit: EditUnitWasteService) {
        this.dsWasteType = this.ds.getUnitWasteTypes();
        this.dsStorageType = this.ds.getEnumValue('Stocare');
        this.onWasteTypeChange = (e) => { this.onWasteTypeChangeEv(e, this); };
    }

    onWasteTypeChangeEv(ev, that) {
        that.edit.updateWasteInfo(ev.value);
    }
}
