import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { DataService } from '../../../service/data.service';


@Injectable()
export class EditPermitService {
    isPermitVisible: boolean;
    permitModel: any;
    permitValidation: string;
    onSaveDataNew: (e: any) => void;
    onSaveData: (e: any) => void;
    popupTitle = 'Acte reglementare';

    constructor(private ds: DataService) {
        this.permitModel = {};
        this.permitValidation = 'permitValidation';
        this.isPermitVisible = false;

        this.onSaveDataNew = (e: any) => this.onSavePermitEv(e, 1);
        this.onSaveData = (e: any) => this.onSavePermitEv(e, 0);
    }

    onSavePermitEv(event: any, type: number, permitGrid?: any) {
        event.event.preventDefault();
        // const validation = event.validationGroup.validate();
        const validators = event.validationGroup.validators.filter((x: any) => x._isHidden === false);
        const validation = event.validationGroup;
        validation.validators = validators;
        if (!validation.validate().isValid) {
            notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
            return;
        }
        const obj = {
            no: this.permitModel.no,
            typeId: this.permitModel.typeId,
            validity: this.permitModel.validity,
            revision: this.permitModel.revision,
            entityId: this.permitModel.entityId,
            description: this.permitModel.description,
            isValid: this.permitModel.isValid
        };
        this.ds.addEditPermit(obj, this.permitModel.id).then((result: any) => {
            this.isPermitVisible = type === 0 ? false : true;
            this.initPermitPopup(this.isPermitVisible);
            notify('Success', 'success', 2000);
            if (permitGrid) {
                permitGrid.instance.refresh();
            }
        }, error => {
            this.ds.serverError(error);
        });
    }

    initPermitPopup(visible = true) {
        this.permitModel = { };
        this.isPermitVisible = visible;
    }

}
