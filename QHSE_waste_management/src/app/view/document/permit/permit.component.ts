import { Component, OnInit, ViewChild } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';
import { UIService } from '../../../service/ui.service';
import { confirm } from 'devextreme/ui/dialog';
import { DataService } from '../../../service/data.service';
import { EditPermitService } from './editPermit.service';

@Component({
    templateUrl: 'permit.component.html',
    providers: [EditPermitService]
})
export class PermitComponent implements OnInit {
    dsPermit: CustomStore;
    pWithActiv: CustomStore;
    dsPermitsType: any;
    dsBoolean: any;

    dsReportType: any;
    onEditPermitsClick: any;
    onDeletePermitsClick: any;
    popupTitle: string;

    @ViewChild('filesGrid') filesGrid: DxDataGridComponent;
    @ViewChild('permitGrid') permitGrid: DxDataGridComponent;
    onSavePermit: any;

    constructor(public ds: DataService, public ui: UIService, public edit: EditPermitService) {
        this.onEditPermitsClick = this.onEditPermits.bind(this);
        this.onDeletePermitsClick = this.onDeletePermits.bind(this);

        setTimeout(() => {
            this.onSavePermit = (e: any) => this.edit.onSavePermitEv(e, 0, this.permitGrid);
        }, 0);
    }

    ngOnInit() {
        this.dsPermit = this.ds.getPermit();
        this.pWithActiv = this.ds.getParty('wa');
        this.dsPermitsType = this.ds.getPermitsType();
        this.dsBoolean = this.ds.boolean('bool');
        this.edit.isPermitVisible = false;
    }

    onFilesGridInit(e: any, data: any) {
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getFileInfo(data.data.id));
        }
    }

    addNewRow(e: any) {
        this.edit.isPermitVisible = true;
        this.edit.permitModel = {};
        this.popupTitle = 'Adaugă înregistrare';
    }

    onEditPermits(e: any) {
        e.event.preventDefault();
        this.edit.isPermitVisible = true;
        this.popupTitle = 'Editează înregistrare';
        this.edit.permitModel = e.row.data;
    }

    onDeletePermits(e: any) {
        e.event.preventDefault();
        confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').then(val => {
            if (val) {
                this.ds.removeContract(e.row.data.id).then(() => {
                    notify('Success', 'success', 2000);
                    this.permitGrid.instance.refresh();
                }, err => {
                    this.ds.serverError(err);
                });
            }
        });
    }

    toolbarPreparing(event: any) {
        this.ui.prepareToolbar(event, true);
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'assets/img/grid_add.svg', hint: 'Adaugă rând', onClick: (e: any) => {
                    this.addNewRow(null);
                }
            },
            location: 'after'
        });
    }
}
