import { Component, Input, NgModule } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { DxScrollViewModule, DxFormModule } from 'devextreme-angular';

@Component({
    selector: 'app-edit-permit',
    templateUrl: 'editPermit.component.html'
})
export class EditPermitComponent {
    @Input() permitData: any;
    dsPermitsType: any;
    dsBoolean: Array<{ id, name }>;

    pWithActiv: CustomStore;

    constructor(private ds: DataService) {
        this.dsPermitsType = this.ds.getPermitsType();
        this.pWithActiv = this.ds.getParty('wa');
        this.dsBoolean = this.ds.boolean('bool');
    }
}


@NgModule({
    imports: [DxScrollViewModule, DxFormModule],
    declarations: [EditPermitComponent],
    exports: [EditPermitComponent]
})
export class EditPermitModule{}
