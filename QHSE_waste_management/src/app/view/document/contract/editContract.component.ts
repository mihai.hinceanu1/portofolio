import { Component, Input, NgModule } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { DxScrollViewModule, DxFormModule } from 'devextreme-angular';

@Component({
    selector: 'app-edit-contract',
    templateUrl: 'editContract.component.html'
})
export class EditContractComponent {
    @Input() contractData: any;
    dspCtr: CustomStore;
    dsBoolean: Array<{ id, name }>;

    constructor(private ds: DataService) {
        this.dspCtr = this.ds.getParty('cr');
        this.dsBoolean = this.ds.boolean('bool');
    }
}

@NgModule({
    imports: [DxScrollViewModule, DxFormModule],
    declarations: [EditContractComponent],
    exports: [EditContractComponent]
})
export class EditContractModule {
}
