import { Component, OnInit, ViewChild } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';
import { UIService } from '../../../service/ui.service';
import { confirm } from 'devextreme/ui/dialog';
import { DataService } from '../../../service/data.service';
import { EditContractService } from './editContract.service';

@Component({
    templateUrl: 'contract.component.html',
    providers: [EditContractService]
})
export class ContractComponent implements OnInit {
    dsContracts: CustomStore;
    dspCtr: CustomStore;
    dsBoolean: Array<{ id, name }>;
    onDeleteContractsClick: any;
    onEditContractsClick: any;

    @ViewChild('filesGrid') filesGrid: DxDataGridComponent;
    @ViewChild('gridContract') gridContract: DxDataGridComponent;
    onSaveContract: any;

    constructor(public ds: DataService, public ui: UIService, public edit: EditContractService) {
        this.onEditContractsClick = this.onEditContracts.bind(this);
        this.onDeleteContractsClick = this.onDeleteContracts.bind(this);

        setTimeout(() => {
            this.onSaveContract = (e: any) => this.edit.onSaveContractEv(e, 0, this.gridContract);
        }, 0);
    }

    ngOnInit() {
        this.dsContracts = this.ds.contracts();
        this.dspCtr = this.ds.getParty('cr');
        this.dsBoolean = this.ds.boolean('bool');
    }

    onFilesGridInit(e: any, data: any) {
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getFileInfo(data.data.id));
        }
    }

    addNewRow(e: any) {
        this.edit.initContractPopup();
    }

    onEditContracts(e: any) {
        e.event.preventDefault();
        this.edit.contractModel = e.row.data;
        this.edit.isContractVisible = true;
    }

    onDeleteContracts(e: any) {
        e.event.preventDefault();
        confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').then(val => {
            if (val) {
                this.ds.removeContract(e.row.data.id).then(() => {
                    notify('Success', 'success', 2000);
                    this.gridContract.instance.refresh();
                }, err => {
                    this.ds.serverError(err);
                });
            }
        });
    }

    toolbarPreparing(event: any) {
        this.ui.prepareToolbar(event, true);
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'assets/img/grid_add.svg', hint: 'Adaugă rând', onClick: (e: any) => {
                    this.addNewRow(null);
                }
            },
            location: 'after'
        });
    }
}
