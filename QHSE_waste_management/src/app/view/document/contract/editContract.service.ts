import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { DataService } from '../../../service/data.service';

@Injectable()
export class EditContractService {
    isContractVisible: boolean;
    contractModel: any;
    contractValidation: string;
    onSaveDataNew: (e: any) => any;
    onSaveData: (e: any) => any;
    popupTitle = 'Adaugă contract';

    constructor(private ds: DataService) {
        this.contractModel = {};
        this.contractValidation = 'contractValidation';
        this.isContractVisible = false;
        this.onSaveDataNew = (e: any) => this.onSaveContractEv(e, 1);
        this.onSaveData = (e: any) => this.onSaveContractEv(e, 0);
    }

    onSaveContractEv(event: any, type: number, gridContract?: any) {
        // event.event.preventDefault();
        // const validation = event.validationGroup.validate();
        const validators = event.validationGroup.validators.filter((x: any) => x._isHidden === false);
        const validation = event.validationGroup;
        validation.validators = validators;
        if (!validation.validate().isValid) {
            notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
            return;
        }

        const obj = {
            no: this.contractModel.no,
            fromId: this.contractModel.fromId,
            toId: this.contractModel.toId,
            contractObjective: this.contractModel.contractObjective,
            startDate: this.contractModel.startDate,
            endDate: this.contractModel.endDate,
            isValid: this.contractModel.isValid,
            contractValue: this.contractModel.contractValue
        };

        this.ds.addEditContract(obj, this.contractModel.id).then((result: any) => {
            this.isContractVisible = type === 0 ? false : true;
            this.initContractPopup(this.isContractVisible);
            notify('Success', 'success', 2000);
            if (gridContract) {
                gridContract.instance.refresh();
            }
        }, error => {
            this.ds.serverError(error);
        });
    }

    initContractPopup(visible = true) {
        this.contractModel = { isValid: true };
        this.isContractVisible = visible;
    }
}
