import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { AccountComponent } from './account.component';
import { DataService } from '../../service/data.service';
import { iBisAuthService } from '@dohu/ibis-auth';
import { DxFormModule, DevExtremeModule } from 'devextreme-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import themes from 'devextreme/ui/themes';

describe('AccountComponent', () => {
    let component: AccountComponent;
    let fixture: ComponentFixture<AccountComponent>;
    let testBedDataService: any;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AccountComponent],
            providers: [
                DataService,
                iBisAuthService
            ],
            imports: [
                DevExtremeModule,
                RouterTestingModule,
                HttpClientTestingModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(async(() => {
        fixture = TestBed.createComponent(AccountComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should be initialized', inject([iBisAuthService], (authService: iBisAuthService) => {
        expect(authService).toBeTruthy();
      }));

    it('should create', async(() => {
        expect(component).toBeTruthy();
    }));
});
