import { Component, OnInit, ViewChild } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { DataService } from '../../../service/data.service';
import { UIService } from '../../../service/ui.service';
import { EditUnitWasteService } from '../../admin/unit/editUnitWaste.service';
import { EditGenerateService } from './editGenerate.service';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'app-edit-generate',
    templateUrl: 'editGenerate.component.html'
})
export class EditGenerateComponent implements OnInit {

    @ViewChild('generateForm') generateForm: DxFormComponent;

    // wcDisable = true;
    // wtDisable = true;
    // wtAddDisable = true;
    dsStorageType: CustomStore;
    dspWorkCenter: CustomStore;
    dspEntities: CustomStore;
    dsWasteType: CustomStore;
    wasteCodeInst: any;
    workCenterInst: any;
    onEntityChange: any;
    onWorkCenterInit: any;
    onWorkCenterChange: any;

    constructor(public ds: DataService, public edit: EditGenerateService, public editU: EditUnitWasteService, public ui: UIService) {
        this.dspEntities = this.ds.getParty('pc');
        this.dsStorageType = this.ds.getEnumValue('Stocare');

        this.onEntityChange = this.onEntityChangeEv.bind(this);
        this.onWorkCenterInit = this.onWorkCenterInitEv.bind(this);
        this.onWorkCenterChange = this.onWorkCenterChangeEv.bind(this);
    }

    ngOnInit() {
        if (this.edit.model.entityId && this.edit.model.entityId.length) {
            this.dspWorkCenter = this.ds.getPartyMemberById(this.edit.model.entityId);
            // this.wcDisable = false;
        }

        if (this.edit.model.workCenterId && this.edit.model.workCenterId.length) {
            this.dsWasteType = this.ds.getWasteTypeForWorkCenter(this.edit.model.workCenterId);
            // this.wtDisable = false;
            // this.wtAddDisable = false;
        }
    }

    onWasteCodeInitEv(event: any) {
        this.wasteCodeInst = event.component;
    }
    onWorkCenterInitEv(event: any) {
        this.workCenterInst = event.component;
    }

    openWasteTypeAddEv(event: any) {
        const newWT = this.editU.createDefault();
        newWT.workCenterId = this.edit.model.workCenterId;
        this.editU.showPopup(newWT).then(() => this.wasteCodeInst.getDataSource().reload());
        event.event.preventDefault();
        event.event.stopPropagation();
    }

    onEntityChangeEv(event: any) {
        if (event.event) {
            this.ds.resetValidation(this.workCenterInst);
            this.ds.resetValidation(this.wasteCodeInst);
            this.setWasteInfo();
            // this.wtDisable = true;
            // this.wtAddDisable = true;
        }

        if (event.value) {
            // this.wcDisable = false;
            this.dspWorkCenter = this.ds.getPartyMemberById(event.value);
        }
    }

    onWorkCenterChangeEv(event: any) {
        if (event.event) {
            this.ds.resetValidation(this.wasteCodeInst);
            this.setWasteInfo();
        }

        if (event.value) {
            // this.wtDisable = false;
            // this.wtAddDisable = false;
            this.dsWasteType = this.ds.getWasteTypeForWorkCenter(event.value);
        }
    }

    setWasteInfo(hazardName = null, wt_name = null, uom = null, storageTypeId = null) {
        this.edit.model.wt_hazardName = hazardName;
        this.edit.model.wt_name = wt_name;
        this.edit.model.wt_uom = uom;
        this.edit.model.wt_storageTypeId = storageTypeId;
    }

    onWasteChangeEv(event: any) {
        if (event.event) {
            const items = this.wasteCodeInst.getDataSource().items();
            const item = items.filter((x: any) => x.id === event.value);
            this.ds.getWasteTypeParty(event.value).then((result) => {
                const hazard = DataService.getHarzardName(item[0].hazard);
                this.setWasteInfo(hazard, item[0].name, item[0].uom, result.storageTypeId);
            }, (error) => { this.ds.serverError(error); });
        }
    }

    onAddHidden(e: any) {
        this.ui.onPopupHidden(e);
    }
}
