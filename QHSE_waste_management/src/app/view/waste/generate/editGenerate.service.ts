import { Injectable } from '@angular/core';
import { EntityQuery, EntityStore } from '@dohu/ibis-entity';
import { DataService } from '../../../service/data.service';
import { EditService } from '../../../service/edit.service';

@Injectable()
export class EditGenerateService extends EditService {

    
    customValidation: string;
    constructor() {
        super();
        this.validation = 'gdValidation';
        this.customValidation = 'customTValidation';
    }

    getViewById(id: string) {
        return EntityStore.fromQuery(new EntityQuery('GenerateView').eq('g_id', id)).single();
    }

    createDefault() {
        return { source: 1, estimated: true, date: new Date() };
    }

    getById(id: string): Promise<any> {
        return new Promise<any>( (resolve, reject) => {
            this.getViewById(id).then((obj) => {
                if (obj === null) {
                    reject();
                } else {
                    resolve({
                        id: obj.g_id, entityId: obj.wc_parentId, workCenterId: obj.g_workCenterId,
                        wasteTypeId: obj.g_wasteTypeId, quantity: obj.g_quantity, date: obj.g_date,
                        source: obj.g_source, estimated: obj.g_estimated ? 1 : 0, wt_name: obj.wt_name,
                        wt_hazardName: DataService.getHarzardName(obj.wt_hazard), wt_storageTypeId: obj.wtp_storageTypeId
                    });
                }
            }, (e) => reject(e));
        });
    }

    onRemove(id: string): Promise<void> {
        return EntityStore.fromQuery(new EntityQuery('Generate')).remove(id);
    }

    onSaveEv(): Promise<any> {
        const obj = {
            wasteTypeId: this.model.wasteTypeId,
            workCenterId: this.model.workCenterId,
            quantity: this.model.quantity,
            source: this.model.source,
            estimated: this.model.estimated === 1 ? true : false,
            date: this.model.date,
        };

        const s = EntityStore.fromQuery(new EntityQuery('Generate'));
        return (this.model.id ? s.update(this.model.id, obj) : s.insert(obj));
    }

    reset(): void {
        this.model.id = null;
        this.model.quantity = 0;
    }
}
