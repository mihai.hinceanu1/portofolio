
import { Component } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { UIService } from '../../../service/ui.service';

@Component({
    templateUrl: 'capitalization.component.html'
})
export class CapitalizationComponent {
    dsCaps: CustomStore;
    dsValorificare: CustomStore;

    constructor(public ds: DataService, public ui: UIService) {
        this.dsCaps = ds.capitalizationInfo();
        this.dsValorificare = this.ds.getEnumValue('Valorificare');
    }
}
