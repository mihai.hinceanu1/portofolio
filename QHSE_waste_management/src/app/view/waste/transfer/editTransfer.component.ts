import { Component, OnInit, Input } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { DataService } from '../../../service/data.service';
import { EditTransferService } from './editTransfer.service';

@Component({
    selector: 'app-edit-transfer',
    templateUrl: './editTransfer.component.html'
})
export class EditTransferComponent implements OnInit {
    @Input() transferData: any;

    dspWorkCenter: CustomStore;
    dsWasteType: CustomStore;
    onWasteChange: any;
    onWasteCodeInit: any;
    wasteCodeInst: any;
    onWorkCenterSource: any;

    constructor(private ds: DataService, public edit: EditTransferService) {
        this.onWasteChange = this.onWasteChangeEv.bind(this);
        this.onWasteCodeInit = this.onWasteCodeInitEv.bind(this);
        this.onWorkCenterSource = this.onWorkCenterSourceEv.bind(this);
    }

    ngOnInit() {
        this.dspWorkCenter = this.ds.getParty('wc');
        this.dsWasteType = this.ds.getWasteTypeForWorkCenter(null);
    }

    onWasteCodeInitEv(event: any) {
        this.wasteCodeInst = event.component;
    }

    onWasteChangeEv(event: any) {
        if (event.event) {
            const items = this.wasteCodeInst.getDataSource().items();
            const item = items.filter((x: any) => x.id === event.value);
            this.edit.model.wt_uom = item[0].uom;
        }
    }

    onWorkCenterSourceEv(event: any) {
        this.dsWasteType = this.ds.getWasteTypeForWorkCenter(event.value);
    }

}
