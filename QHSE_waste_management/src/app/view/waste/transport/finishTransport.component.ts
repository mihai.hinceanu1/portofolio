import { Component, Input } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { FinishTransportService } from './finishTransport.service';

@Component({
    selector: 'app-finish-transport',
    templateUrl: 'finishTransport.component.html'
})
export class FinishTransportComponent {

    dsTreatmentTypes: CustomStore;
    dsCapitalization: CustomStore;
    dsElimination: CustomStore;

    carTransport: string;
    formInstance: any;
    constructor(public ds: DataService, public edit: FinishTransportService) {
        this.dsTreatmentTypes = this.ds.getEnumValue('Tratament');
        this.dsCapitalization = this.ds.getEnumValue('Valorificare');
        this.dsElimination = this.ds.getEnumValue('Eliminare');
    }

    public get nrDate(): string {
        return this.edit ? this.edit.model.t_no + ' / ' + DataService.formatDate(this.edit.model.t_sendDate) : '';
    }

    public get entityWorkCenter(): string {
        return this.edit ? this.edit.model.ent_name + ' / ' + this.edit.model.wc_name : '';
    }

    public get carTransporter(): string {
        return this.edit ? this.edit.model.t_carNumber + ' / ' + this.edit.model.tr_name : '';
    }

    onFormInit(event) {
        this.formInstance = event.component;
    }

    checkValue(val) {
        if (val || val === 0) {
            return true;
        }
        return false;
    }

    eQuantityValidationCallback = (options) => {
        const vQuantity = this.formInstance.getEditor('t_valuedQuantity').option('value');
        if (vQuantity === 0 && options.value === 0) { return false; }
        if (vQuantity === 0 || options.value === 0) { return true; }
        return vQuantity || options.value;
    }

    eCodeValidationCallback = (options) => {
        const eQuantity = this.formInstance.getEditor('t_eliminatedQuantity').option('value');
        const vQuantity = this.formInstance.getEditor('t_valuedQuantity').option('value');
        const condition = (this.checkValue(eQuantity) && !vQuantity) || (this.checkValue(eQuantity) && this.checkValue(vQuantity));
        if (condition) {
            if (eQuantity === 0) { return true; }
            return options.value;
        } else { return true; }
    }

    vQuantityValidationCallback = (options) => {
        const eQuantity = this.formInstance.getEditor('t_eliminatedQuantity').option('value');
        if (eQuantity === 0 && options.value === 0) { return false; }
        if (eQuantity === 0 || options.value === 0) { return true; }
        return eQuantity || options.value;
    }

    vCodeValidationCallback = (options) => {
        const vQuantity = this.formInstance.getEditor('t_valuedQuantity').option('value');
        const eQuantity = this.formInstance.getEditor('t_eliminatedQuantity').option('value');
        const condition = (this.checkValue(vQuantity) && !eQuantity) || (this.checkValue(eQuantity) && this.checkValue(vQuantity));
        if (condition) {
            if (vQuantity === 0) { return true; }
            return options.value;
        } else { return true; }
    }
}
