import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { confirm } from 'devextreme/ui/dialog';
import { UIService } from '../../../service/ui.service';
import { EditTransportService } from './editTransport.service';
import { EntityStoreOptions } from '@dohu/ibis-entity';
import { HttpClient } from '@angular/common/http';
import { FinishTransportService } from './finishTransport.service';

@Component({
    templateUrl: 'transport.component.html',
})
export class TransportComponent implements OnInit {
    dsTransportEntry: CustomStore;
    dsTransportFinish: CustomStore;

    @ViewChild('gridEntryTransport') gridEntryTransport: DxDataGridComponent;
    @ViewChild('gridFinishTransport') gridFinishTransport: DxDataGridComponent;
    @ViewChild('filesGrid') filesGrid: DxDataGridComponent;
    onFinishTransportClick: any;
    onEditTransportClick: any;
    onDeleteTransportClick: any;
    onEditTransFinishClick: any;
    onDeleteTransFinishClick: any;

    constructor(public ds: DataService, public ui: UIService, public edit: EditTransportService, public finish: FinishTransportService,
         public http: HttpClient) {

        this.onEditTransportClick = (e: any) => this.onEdit(this, e, false, true);
        this.onFinishTransportClick = (e: any) => this.onEdit(this, e, true, true);
        this.onEditTransFinishClick = (e: any) => this.onEdit(this, e, true, false);

        this.onDeleteTransFinishClick = (e: any) => this.onDelete(this, e, true);
        this.onDeleteTransportClick = (e: any) => this.onDelete(this, e, false);
    }

    ngOnInit() {
        this.dsTransportEntry = this.ds.transportEntry();
        this.dsTransportFinish = this.ds.getTransportFinish();
    }


    onRaportTransport(e: any) {
        const url = EntityStoreOptions.DefaultServer + 'DownloadAnex/' + e.row.data.t_id;
        this.ds.getFile(url);
    }

    onEdit(that: any, e: any, isFinish: boolean, refreshEntry: boolean) {
        e.event.preventDefault();
        (isFinish ? that.finish : that.edit).showPopup(e.row.data.t_id, true).then(() => {
            (refreshEntry ? that.gridEntryTransport : that.gridFinishTransport).instance.refresh();
        });
    }

    onDelete(that: any, e: any, isFinish: boolean) {
        e.event.preventDefault();
        confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').then(val => {
            if (val) {
                that.edit.remove(e.row.data.t_id).then(() => {
                    notify('Success', 'success', 2000);
                    (isFinish ? that.gridFinishTransport : that.gridEntryTransport).instance.refresh();
                }, err => {
                    that.ds.serverError(err);
                });
            }
        });
    }

    // for file uploading
    toolbarPreparing(event: any, type: string, data?: any) {
        this.ui.prepareToolbar(event, true);
        if (type === 'add') {
            event.toolbarOptions.items.unshift({
                widget: 'dxButton',
                options: {
                    icon: 'assets/img/grid_add.svg', hint: 'Adaugă rând',
                        onClick: () => this.edit.showPopup(null).then(() => event.component.refresh())
                },
                location: 'after'
            });
        }
    }

    onFileUploaderReady(e: any, data: any) {
        const buttonInstance = e.component._selectButton;
        buttonInstance.option('icon', 'assets/img/grid_add.svg');
        buttonInstance.option('width', '130');
        buttonInstance.option('height', '26');
    }
}
