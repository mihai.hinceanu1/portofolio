import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { EditService } from '../../../service/edit.service';
import { EntityStore, EntityQuery } from '@dohu/ibis-entity';
import { DataService } from '../../../service/data.service';

@Injectable()
export class EditTransportService extends EditService {

    public validation: string;
    public customValidation: string;
    public transportActions = [ { id: 1, text: 'Colectare' },
        { id: 2, text: 'Stocare temporara' },
        { id: 4, text: 'Tratare' },
        { id: 8, text: 'Eliminare' },
        { id: 16, text: 'Valorificare' }];

    constructor() {
        super();

        this.validation = 'editValidation';
        this.customValidation = 'customTValidation';
    }

    protected setTransportDataSource(actionNo: number) {
        if (!actionNo) { return []; }
        const result = [];
        for (const val of this.transportActions) {
            // tslint:disable-next-line:no-bitwise
            if (actionNo & val.id) {
                result.push(val.id);
            }
        }
        return result;
    }

    createDefault(): any {
        return { t_weighing: 1, t_sendDate: new Date(), selectedActions: [] };
    }

    getById(id: string): Promise<any> {
        return new Promise<any>( (resolve, reject) => {
            EntityStore.fromQuery(new EntityQuery('TransportView').eq('t_id', id)).single().
                then((obj) => {
                    if (obj === null) {
                        reject();
                    } else {
                        obj.selectedActions = this.setTransportDataSource(obj.t_actions);
                        obj.wt_hazard = DataService.getHarzardName(obj.wt_hazard);
                        resolve(obj);
                    }
                }, (e) => reject(e));
        });
    }

    onRemove(id: string): Promise<void> {
        return EntityStore.fromQuery(new EntityQuery('Transport')).remove(id);
    }

    onSaveEv(): Promise<any> {
        let obj = {};
        if (this.model.t_finishDate) {
            // finalizare transport
            obj = {
                valuedQuantity: this.model.t_valuedQuantity,
                eliminatedQuantity: this.model.t_eliminatedQuantity,
                tempStockDate: this.model.t_tempStockDate,
                shippingCost: this.model.t_shippingCost,
                receptionDate: this.model.t_receptionDate,
                finishDate: this.model.t_finishDate,
                capitalizationId: this.model.t_capitalizationId,
                eliminationId: this.model.t_eliminationId,
                treatmentTypeId: this.model.t_treatmentTypeId
            };
        } else {
            obj = {
                wasteTypeId: this.model.t_wasteTypeId,
                workCenterId: this.model.t_workCenterId,
                no: this.model.t_no,
                quantity: this.model.t_quantity,
                weighing: this.model.t_weighing,
                sendDate: this.model.t_sendDate,
                carNumber: this.model.t_carNumber,
                driverName: this.model.t_driverName,
                driverId: this.model.t_driverId,
                transportTypeId: this.model.t_transportTypeId,
                transporterId: this.model.t_transporterId,
                destinationTypeId: this.model.t_destinationTypeId,
                recipientId: this.model.t_recipientId,
                actions: this.getTransportVal(this.model.selectedActions)
            };
        }

        const s = EntityStore.fromQuery(new EntityQuery('Transport'));
        return this.model.t_id ? s.update(this.model.t_id, obj) : s.insert(obj);
    }

    reset(): void {
        this.model.t_id = null;
        this.model.t_quantity = 0;
    }

    private getTransportVal(actionsArr: number[]) {
        if (actionsArr && actionsArr.length > 0) {
            let result = 0;
            for (const act of actionsArr) {
                result += act;
            }
            return result;
        }
        return 0;
    }
}
