import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { DxSelectBoxComponent } from 'devextreme-angular';
import { EditPartyService } from '../../admin/party/editParty.service';
import { UIService } from '../../../service/ui.service';
import { EditTransportService } from './editTransport.service';

@Component({
    selector: 'app-edit-transport',
    templateUrl: 'editTransport.component.html'
})
export class EditTransportComponent implements OnInit {
    dsEntity: CustomStore;
    dsWorkCenter: CustomStore;
    dsTransportTypes: CustomStore;
    dsValidTransports: CustomStore;
    dsDestinationTypes: CustomStore;
    dsValidDest: CustomStore;
    dsWasteType: CustomStore;
    onWasteChange: any;
    onEntityChange: any;
    onWorkCenterChange: any;
    wasteCodeInst: any;
    workCenterInst: any;
    onWorkCenterInit: any;
    onWasteCodeInit: any;
    onDestinationInit: any;

    wtDisable = true; wcDisable = true;
    transHelpText: string;
    wasteCodeHelpText: string;
    openPartyDestAdd: any;
    transportSelInstance: any;
    destinationInstance: any;

    constructor(public ds: DataService, public edit: EditTransportService, public editParty: EditPartyService, public ui: UIService) {
        this.dsEntity = this.ds.getParty('pc');
        this.dsTransportTypes = this.ds.getEnumValue('Transport');
        this.dsValidTransports = this.ds.validTransports();
        this.dsDestinationTypes = this.ds.getEnumValue('Destinatie');
        this.dsValidDest = this.ds.validDest();

        this.onWorkCenterInit = this.onWorkCenterInitEv.bind(this);
        this.onWasteCodeInit = this.onWasteCodeInitEv.bind(this);
        this.onDestinationInit = this.onDestinationInitEv.bind(this);

        this.onWasteChange = this.onWasteChangeEv.bind(this);
        this.onEntityChange = this.onEntityChangeEv.bind(this);
        this.onWorkCenterChange = this.onWorkCenterChangeEv.bind(this);
        this.openPartyDestAdd = this.openPartyAddEv.bind(this, 1);
        this.transHelpText = '**Trebuie să adăugați un partener cu tip de activitate "Transport"';
    }

    ngOnInit() {
        if (this.edit.model.wc_parentId && this.edit.model.wc_parentId.length) {
            this.dsWorkCenter = this.ds.getPartyMemberById(this.edit.model.wc_parentId);
            this.wtDisable = false;
        } else {
            this.dsWorkCenter = this.ds.getPartyMemberById(null);
        }

        if (this.edit.model.t_workCenterId && this.edit.model.t_workCenterId.length) {
            this.dsWasteType = this.ds.getWasteTypeForWorkCenter(this.edit.model.t_workCenterId);
            this.wcDisable = false;
        }
    }

    onWorkCenterInitEv(event: any) {
        this.workCenterInst = event.component;
    }

    onWasteCodeInitEv(event: any) {
        this.wasteCodeInst = event.component;
    }

    openPartyAddEv(event: any, type: number) {
        switch (type) {
            case 0:
                this.editParty.showPopup(null).then(() => {
                    if (this.transportSelInstance) {
                        this.transportSelInstance.getDataSource().reload();
                    }
                });
                break;
            case 1:
                this.editParty.showPopup(null).then(() => {
                    if (this.destinationInstance) {
                        this.destinationInstance.getDataSource().reload();
                    }
                });
                break;
        }
        event.event.preventDefault();
        event.event.stopPropagation();
    }

    onTransporterInitEv(event: any) {
        this.transportSelInstance = event.component;
    }

    onDestinationInitEv(event: any) {
        this.destinationInstance = event.component;
    }

    onWasteChangeEv(event: any) {
        if (event.event) {
            const items = this.wasteCodeInst.getDataSource().items();
            const item = items.filter(x => x.id === event.value);
            const hazard = DataService.getHarzardName(item[0].hazard);
            this.setWasteInfo(hazard, item[0].name, item[0].uom);
        }
    }

    onEntityChangeEv(event: any) {
        if (event.event) {
            this.ds.resetValidation(this.workCenterInst);
            this.ds.resetValidation(this.wasteCodeInst);
            this.setWasteInfo();
            this.wtDisable = true;
        }
        if (event.value) {
            this.wcDisable = false;
            this.dsWorkCenter = this.ds.getPartyMemberById(event.value);
        }
    }

    setWasteInfo(hazardName = null, wt_name = null, uom = null) {
        this.edit.model.wt_hazard = hazardName;
        this.edit.model.wt_name = wt_name;
        this.edit.model.wt_uom = uom;
    }

    onWorkCenterChangeEv(event: any) {
        if (event.event) {
            if (this.wasteCodeInst.getDataSource().items() === 0) {
                this.wasteCodeHelpText = '**Tipurile de deșeu se asociază în Administrare/Organizatie';
            }
            this.ds.resetValidation(this.wasteCodeInst);
            this.setWasteInfo();
        }
        if (event.value) {
            this.wtDisable = false;
            this.dsWasteType = this.ds.getWasteTypeForWorkCenter(event.value);
        }
    }
}
