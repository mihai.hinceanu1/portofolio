import { Injectable } from '@angular/core';
import { EditTransportService } from './editTransport.service';

@Injectable()
export class FinishTransportService extends EditTransportService {

    constructor() {
        super();
        this.validation = 'finishValidation';
    }
}
