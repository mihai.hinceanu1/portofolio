
import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import { UIService } from '../../../service/ui.service';

@Component({
    templateUrl: 'eliminate.component.html'
})
export class EliminateComponent implements OnInit {

    dsEliminate: CustomStore;
    dsElimination: any;

    constructor(public ds: DataService, public ui: UIService) { }

    ngOnInit() {
        this.dsEliminate = this.ds.getEliminateInfo();
        this.dsElimination = this.ds.getEnumValue('Eliminare');
    }
}
