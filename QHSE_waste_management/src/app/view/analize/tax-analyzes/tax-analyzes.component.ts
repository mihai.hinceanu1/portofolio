import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../service/data.service';
import { UIService } from '../../../service/ui.service';
import { AnalizeService } from '../analize.service';

@Component({
  selector: 'app-tax-analyzes',
  templateUrl: './tax-analyzes.component.html'
})
export class TaxAnalyzesComponent implements OnInit {

  constructor(public ds: DataService, public ui: UIService, public analyze: AnalizeService) { }

  ngOnInit() { }

  toolbarPreparing(event: any) {
    this.ui.prepareToolbar(event, true);
  }
}
