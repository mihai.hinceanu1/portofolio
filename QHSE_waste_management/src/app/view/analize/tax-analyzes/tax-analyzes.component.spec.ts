import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAnalyzesComponent } from './tax-analyzes.component';

describe('TaxAnalyzesComponent', () => {
  let component: TaxAnalyzesComponent;
  let fixture: ComponentFixture<TaxAnalyzesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxAnalyzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAnalyzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
