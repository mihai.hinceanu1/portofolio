import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlAnalyzesComponent } from './control-analyzes.component';

describe('ControlAnalyzesComponent', () => {
  let component: ControlAnalyzesComponent;
  let fixture: ComponentFixture<ControlAnalyzesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlAnalyzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlAnalyzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
