import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapitalizationAnalyzesComponent } from './capitalization-analyzes.component';

describe('CapitalizationAnalyzesComponent', () => {
  let component: CapitalizationAnalyzesComponent;
  let fixture: ComponentFixture<CapitalizationAnalyzesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapitalizationAnalyzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapitalizationAnalyzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
