import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostAnalyzesComponent } from './cost-analyzes.component';

describe('CostAnalyzesComponent', () => {
  let component: CostAnalyzesComponent;
  let fixture: ComponentFixture<CostAnalyzesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostAnalyzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostAnalyzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
