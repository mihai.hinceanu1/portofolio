import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeAnalyzesComponent } from './income-analyzes.component';

describe('IncomeAnalyzesComponent', () => {
  let component: IncomeAnalyzesComponent;
  let fixture: ComponentFixture<IncomeAnalyzesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeAnalyzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeAnalyzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
