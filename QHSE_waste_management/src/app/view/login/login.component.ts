import { Component } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { iBisAuthService } from '@dohu/ibis-auth';
import { DataService } from '../../service/data.service';
import { Router } from '@angular/router';
declare var require: any;

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    userLogin: any;
    appVersion: string;

    constructor(public auth: iBisAuthService, public ds: DataService, public router: Router) {
        this.userLogin = {};
        if (this.auth.isAuth) {
            router.navigateByUrl('/analize/dashboard');
        }
        if (this.router.url == '/login') {
            this.ds.isLoginModalVisible = false;
        }
        const { version: appVersion } = require('../../../../package.json');
        this.appVersion = appVersion;
    }


    onLogin(e: any) {
        const validation = e.validationGroup.validate();
        if (!validation.isValid) {
            // notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
            return;
        }
        this.auth.login(this.userLogin).then(() => {
            this.ds.checkForTheme();
            notify('Autentificare cu success.', 'success', 3000);
            if (this.router.url === '/login') {
                this.router.navigateByUrl('/analize/dashboard');
            } else {
                this.ds.isLoginModalVisible = false;
                this.ds.initStatistics();
            }
        }, (error) => {
            if (typeof error === 'object') {
                notify(error.err, 'error', 3000);
            } else if (typeof error === 'string') {
                this.ds.serverError(error);
            }
        });
    }

    onEditorEnterKey(e: any) {
        document.getElementById('loginBtn').focus();
        document.getElementById('loginBtn').click();
    }
    onContentFormReady(e: any) {
        e.component.getEditor('userName').focus();
    }

    goToRegister(fp: boolean) {
        this.router.navigate(['/register'], { queryParams: { forgotPassword: fp } });
        this.ds.isLoginModalVisible = false;
        return false;
    }
}
