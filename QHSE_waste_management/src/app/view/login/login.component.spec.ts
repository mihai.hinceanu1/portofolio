import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { DataService } from '../../service/data.service';
import { iBisAuthService } from '@dohu/ibis-auth';
import { DevExtremeModule } from 'devextreme-angular';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            providers: [
                DataService,
                iBisAuthService
            ],
            imports: [
                DevExtremeModule,
                RouterTestingModule,
                HttpClientTestingModule,
            ]
        });


    }));

    beforeEach(async(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('Login - isAuth', async(() => {
        expect(component).toBeTruthy();
        // spyOn(authService, 'isAuth').and.returnValue(false);
    }));
});