import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../service/data.service';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { EntityStoreOptions } from '@dohu/ibis-entity';
import { Router, NavigationStart } from '@angular/router';
import { DxFormModule, DxFormComponent } from 'devextreme-angular';

@Component({
    templateUrl: 'reportExt.component.html'
})
export class ReportExtComponent implements OnInit {

    now: Date;
    dsMonth: any;
    dsSemester: any;

    dsBu: CustomStore;
    dsEntities: CustomStore;
    reportMonth: number;
    reportYear: number;
    dsWorkCenter: CustomStore;
    reportObj: any;

    wcVisible: boolean;
    groupVisible: boolean;
    yearVisible: boolean;
    monthVisible: boolean;
    semVisible: boolean;
    thruDateVisible: boolean;
    fromDateVisible: boolean;

    disableEntity: boolean;
    disableWc: boolean;
    dsReportType: { id: number; value: string; }[];

    @ViewChild('reportExt') reportExt: DxFormComponent;
    constructor(public ds: DataService, public router: Router) { }

    ngOnInit() {
        this.reportMonth = new Date().getMonth() == 0 ? 12 : new Date().getMonth();
        this.reportYear = this.reportMonth == 12 ? new Date().getFullYear() - 1 : new Date().getFullYear();
        this.dsMonth = this.ds.getMonth();
        this.dsSemester = this.ds.getSemester();
        this.dsBu = this.ds.getParty('bu');
        this.dsEntities = this.ds.getParty('ent');
        this.reportObj = {
            bu: null, entity: null, workCenter: null,
            year: this.reportYear, sem: null, month: this.reportMonth,
            fromDate: this.ds.firstMonthDay, thruDate: this.ds.lastMonthDay
        };
        this.setFieldsVisible(true, true, true, true, false, false, false);
        this.setFieldsDisable(!this.reportObj.bu, !this.reportObj.entity);

        const report = this.router.url.split('/');
        if (report[3]) {
            this.setReportType(report[3]);
        }

        this.router.events.forEach((event) => {
            if (event instanceof (NavigationStart)) {
                const report = event.url.split('/');
                if (report[3]) {
                    this.setReportType(report[3]);
                }
            }
        })
    }

    setReportType(param: string) {
        switch (param) {
            case 'generic':
                this.dsReportType = [{ id: 1, value: 'Anexa1' }]
                // { id: 2, value: 'Trat' },
                // { id: 3, value: 'Proddes' },
                // { id: 4, value: 'Namol' },
                // { id: 5, value: 'Mun' }];
                break;
            case 'electrics':
                this.dsReportType = [{ id: 1, value: 'Anexa 4' },
                { id: 2, value: 'Anexa 5' },
                { id: 3, value: 'Anexa 6' }]
                break;
            case 'batery':
                this.dsReportType = [{ id: 1, value: 'Anexa 2' },
                { id: 2, value: 'Anexa 3' }]
                break;
            // case 'extractive':
            //     break;
            case 'vehicles':
                this.dsReportType = [{ id: 1, value: 'Raportare VSU-anual' }]
                break;
            // case 'pcbpct':
            //     break;
            case 'oils':
                this.dsReportType = [{ id: 1, value: 'Raportare semestriala uleiuri' }]
                break;
            // case 'mud':
            //     break;
        }
        if(this.reportExt.instance){
            this.reportExt.instance.option('dataSource', this.dsReportType);
        }
    }

    onReportFieldChange(event) {
        const val = event.value;
        if (event.dataField === 'reportType' && val) {
            switch (val) {
                case 1:
                    this.setFieldsVisible(true, true, true, true, false, false);
                    this.setFieldsDisable(!this.reportObj.bu, !this.reportObj.entity);
                    break;
                case 2:
                    this.setFieldsVisible(false, false, true, false, false, false, true);
                    this.setFieldsDisable();
                    break;
                case 3:
                    this.setFieldsVisible(false, true, true, false, false, false);
                    this.setFieldsDisable(false, !this.reportObj.entity);
                    break;
                case 4:
                    this.setFieldsVisible(true, false, false, false, true, true);
                    this.setFieldsDisable(!this.reportObj.bu);
                    break;
            }
        } else if (event.dataField === 'reportType' && !val) {
            this.reportObj.reportType = 1;
        }

        if (event.dataField === 'bu') {
            this.reportObj.entity = null;
            this.reportObj.workCenter = null;
            this.dsEntities = this.ds.getEntityByParentId(val);
            this.setFieldsDisable(!this.reportObj.bu, !this.reportObj.entity);
        }

        if (event.dataField === 'entity') {
            this.reportObj.workCenter = null;
            this.dsWorkCenter = this.ds.getPartyMemberById(val);
            this.disableWc = !this.reportObj.entity;
        }
    }

    setFieldsVisible(group: boolean, wc: boolean, year: boolean, month: boolean, thruDate: boolean, fromDate: boolean, sem: boolean = false) {
        this.groupVisible = group;
        this.wcVisible = wc;
        this.yearVisible = year;
        this.monthVisible = month;
        this.thruDateVisible = thruDate;
        this.fromDateVisible = fromDate;
        this.semVisible = sem;
    }

    setFieldsDisable(entity: boolean = false, wc: boolean = false) {
        this.disableEntity = entity;
        this.disableWc = wc;
    }

    onClickArchive(event, reportNo: number) {
        notify('Această funcționalitate nu este implementată încă.', 'warning', 3000);
        let obj = {};
        let method;
        switch (reportNo) {
            case 1:
                obj = {
                    year: this.reportObj.year, month: this.reportObj.month, bu: this.reportObj.bu,
                    entity: this.reportObj.entity, workCenter: this.reportObj.workCenter
                };
                method = 'ArchiveWasteManagement';
                break;
            case 2:
                obj = { year: this.reportObj.year, bu: this.reportObj.sem, entity: this.reportObj.entity };
                method = 'ArchiveOilManagement';
                break;
            case 3:
                break;
            case 4:
                break;
        }
        this.ds.archiveReport(method, obj);
    }


    onClickGenerateReport(e: any, reportNo: number) {
        const validation = e.validationGroup.validate();
        if (!validation.isValid) {
            notify('Câmpurile de mai sus nu sunt valide.', 'error', 3000);
            return;
        }
        let url;
        switch (reportNo) {
            case 1:
                url = EntityStoreOptions.DefaultServer + '/DownloadAnex1/' +
                    this.reportObj.year + '_' + this.reportObj.month + '_' +
                    this.reportObj.bu + '_' + this.reportObj.entity + '_' + this.reportObj.workCenter;
                break;
            case 2:
                url = EntityStoreOptions.DefaultServer + '/DownloadOilManagement/' +
                    this.reportObj.year + '_' + this.reportObj.sem + '_' + this.reportObj.entity;
            default:
                break;
        }
        if (url) {
            this.ds.getFile(url);
        } else {
            notify('Raportul nu a fost găsit.', 'warning', 3000);
        }
    }

    onClickOil(e: any, obj: any) {
        console.log(obj);
    }
}
