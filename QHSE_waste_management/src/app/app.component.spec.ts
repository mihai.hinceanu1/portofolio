import { TestBed, async, inject, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {
  DevExtremeModule
} from 'devextreme-angular';
import { RouterTestingModule } from '@angular/router/testing';
import { DataService } from './service/data.service';
import { HttpClientModule } from '@angular/common/http';

import { LoginComponent } from './view/login/login.component';
import { UIService } from './service/ui.service';
import { iBisAuthService } from '@dohu/ibis-auth';
import { FooterComponent } from './common/footer/footer.component';
import { NavToolbarComponent } from './common/nav-toolbar/nav-toolbar.component';
import { SingleCardComponent } from './common/single-card/single-card.component';
import { iBisCommonModule } from '@dohu/ibis-common';
import { ProxyService } from './service/proxy.service';
import { EditGenerateComponent, EditGenerateService } from './view/waste/generate';
import { EditTransferComponent } from './view/waste/transfer/editTransfer.component';
import { EditUnitWasteComponent, EditUnitWasteService } from './view/admin/unit';
import { EditPartyComponent } from './view/admin/party/editParty.component';
import { EditTransportComponent } from './view/waste/transport/editTransport.component';
import { HeaderComponent } from './common/header/header.component';
import { NavigationMenuComponent } from './common/navigation-menu/navigation-menu.component';
import { BreadcrumbComponent } from './common/breadcrumb/breadcrumb.component';

import themes from 'devextreme/ui/themes';
import { InviteUserComponent } from './common/invite-user/invite-user.component';
import { AddCompanyComponent } from './common/add-company/add-company.component';
import { CreateTicketComponent } from './common/create-ticket/create-ticket.component';
import { EditContractComponent } from './view/document/contract/editContract.component';
import { EditPermitComponent } from './view/document/permit/editPermit.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EditPartyService } from './view/admin/party/editParty.service';
import { EditPermitService } from './view/document/permit/editPermit.service';
import { EditTransferService } from './view/waste/transfer/editTransfer.service';
import { EditTransportService } from './view/waste/transport/editTransport.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let authService: iBisAuthService;
  let dataService: DataService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FooterComponent,
        NavToolbarComponent,
        SingleCardComponent,
        LoginComponent,
        EditGenerateComponent,
        EditTransferComponent,
        EditUnitWasteComponent,
        EditPartyComponent,
        EditTransportComponent,
        HeaderComponent,
        NavigationMenuComponent,
        BreadcrumbComponent,
        InviteUserComponent,
        CreateTicketComponent,
        AddCompanyComponent,
        EditContractComponent,
        EditPermitComponent
      ],
      providers: [
        DataService,
        iBisAuthService,
        EditGenerateService,
        UIService,
        ProxyService,
        EditPartyService,
        EditPermitService,
        EditUnitWasteService,
        EditTransferService,
        EditTransportService
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        iBisCommonModule,
        DevExtremeModule
      ]
    }).compileComponents();
  }));


  beforeEach(async(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it(`should have as title 'QHSE'`, async(() => {
    console.log(themes.current());
    inject([iBisAuthService], (auth) => {
      expect(auth).toBeDefined();
      expect(auth).toBeTruthy();
      expect(component).toBeTruthy();
      expect(auth.appName).toEqual('QHSE');
    });
  }));
});
