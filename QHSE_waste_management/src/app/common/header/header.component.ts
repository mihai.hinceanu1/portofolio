import { Component, NgModule, Input, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { iBisAuthService } from '@dohu/ibis-auth';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxToolbarModule } from 'devextreme-angular/ui/toolbar';
import { DxPopoverModule, DxListModule, DxFormModule, DxPopupModule, DxScrollViewModule } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { Router, RouterModule } from '@angular/router';
import { DataService } from '../../service/data.service';
import { InviteUserModule } from '../invite-user/invite-user.component';
import { CreateTicketModule } from '../create-ticket/create-ticket.component';
import { EditContractService } from '../../view/document/contract/editContract.service';
import { EditPermitService } from '../../view/document/permit/editPermit.service';
import { EditContractModule } from '../../view/document/contract/editContract.component';
import { EditPartyModule } from '../../view/admin/party/editParty.component';
import { EditPermitModule } from '../../view/document/permit/editPermit.component';
import { UIService } from '../../service/ui.service';
import { environment } from '../../../environments/environment';
import { AddCompanyComponent } from '../add-company/add-company.component';
import { ProxyService } from '../../service/proxy.service';
// import { InviteUserModule } from '../invite-user/invite-user.component';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
    @Output() menuToggle = new EventEmitter<boolean>();

    @Input() menuToggleEnabled = false;
    @Input() companyName: string;

    currentTarget: string;
    menuItems: any[];
    isSubMenuVisible: boolean;
    isAddCompanyVisible: boolean;
    isInviteUserVisible: boolean;
    isCreateTicketVisible: boolean;
    addDataValidation: string;

    onSaveDataNew: (e: any) => any;
    onSaveData: (e: any) => any;
    popupTitle: any;
    manualUrl: string;

    constructor(private auth: iBisAuthService, private ds: DataService, public router: Router, public proxy: ProxyService,
        public editC: EditContractService, public editP: EditPermitService, public ui: UIService) {
        this.isSubMenuVisible = false;
        this.isAddCompanyVisible = false;
        this.isInviteUserVisible = false;
        this.manualUrl = environment.manualUrl;
    }

    toggleMenu = () => {
        this.menuToggle.emit();
    }

    onShowMenuEv(target: string, e: any) {
        if (e) {
            e.event.preventDefault();
        }
        this.currentTarget = target;
        switch (target) {
            case '#user-options':
                this.menuItems = [
                    { icon: 'user', text: 'Profil', route: '/profile' },
                    { icon: 'message', text: 'Invită utilizator', route: '/invite' },
                    { iconUrl: 'assets/img/support.svg', text: 'Suport', route: '/createTicket' },
                    { icon: 'runner', text: 'Deconectare', route: '/login' }
                ];
                break;
            case '#company-options':
                this.menuItems = JSON.parse(JSON.stringify(this.auth.dsCompanies));
                for (let i = 0; i < this.menuItems.length - 1; i++) {
                    this.menuItems[i].icon = 'product';
                }
                this.menuItems.push({ icon: 'preferences', text: 'Setări' });
                break;
            case '#plus-wizard':
                this.menuItems = [
                    { text: 'Generare', popup: 'isGenerateVisible' },
                    { text: 'Transport', popup: 'isAddTransProgVisible' },
                    { text: 'Contract', popup: 'isContractVisible' },
                    { text: 'Acte reglementare', popup: 'isPermitVisible' },
                    { text: 'Parteneri', popup: 'isPartyVisible' }
                ];
                break;
        }
        this.isSubMenuVisible = true;
    }

    onInviteClose(e: any) {
        this.isInviteUserVisible = false;
    }
    onCreateTicketClose(e: any) {
        this.isCreateTicketVisible = false;
    }
    onAddCompanyClose(e: any) {
        this.isAddCompanyVisible = false;
    }

    onMenuLeave() {
        this.isSubMenuVisible = false;
    }

    onIconMenuClick(e: any) {
        switch (this.currentTarget) {
            case '#user-options':
                if (e.itemData.route === '/login') {
                    this.auth.logout().then(() => {
                        sessionStorage.removeItem('dashboardQHSE');
                        this.router.navigateByUrl('/login');
                    });
                } else if (e.itemData.route === '/invite') {
                    this.isInviteUserVisible = true;
                } else if (e.itemData.route === '/createTicket') {
                    this.isCreateTicketVisible = true;
                } else if (e.itemData.route) {
                    this.router.navigateByUrl(e.itemData.route);
                }
                break;
            case '#company-options':
                if (e.itemData.type === 'add') {
                    this.isAddCompanyVisible = true;
                } else if (e.itemData.id && e.itemData.text) {
                    this.auth.changeCompany(e.itemData).then(result => {
                    }, err => {
                        if (typeof err === 'string') {
                            this.ds.serverError();
                        } else {
                            notify('Cererea nu a putut fi procesată.', 'error', 3000);
                        }
                    });
                }
                break;
            case '#plus-wizard':
                this.initService(e.itemData);
                break;
        }
        this.onMenuLeave();
    }

    onAddEditHidden(e: any) {
        this.ui.onPopupHidden(e);
    }

    initService(itemData: any) {
        switch (itemData.popup) {
            case 'isGenerateVisible':
                this.proxy.editGenerate.showPopup(null);
                break;
            case 'isAddTransProgVisible':
                this.proxy.editTransport.showPopup(null);
                break;
            case 'isContractVisible':
                this.editC.initContractPopup();
                break;
            case 'isPermitVisible':
                this.editP.initPermitPopup();
                break;
            case 'isPartyVisible':
                this.proxy.editParty.showPopup(null);
                break;
        }
    }
}

@NgModule({
    imports: [
        CommonModule,
        DxButtonModule,
        DxToolbarModule,
        DxPopoverModule,
        DxListModule,
        DxScrollViewModule,
        DxPopupModule,
        DxFormModule,
        InviteUserModule,
        CreateTicketModule,
        RouterModule,

        EditContractModule,
        EditPartyModule,
        EditPermitModule
    ],
    declarations: [
        HeaderComponent,
        AddCompanyComponent
    ],
    exports: [HeaderComponent,
        AddCompanyComponent,
        EditContractModule,
        EditPartyModule,
        EditPermitModule],
    providers: [EditContractService, EditPermitService]
})
export class HeaderModule { }
