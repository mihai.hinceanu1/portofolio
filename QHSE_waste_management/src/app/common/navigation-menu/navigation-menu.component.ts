import { Component, NgModule, Output, Input, EventEmitter, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { DxTreeViewModule, DxTreeViewComponent } from 'devextreme-angular/ui/tree-view';

import * as events from 'devextreme/events';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements AfterViewInit, OnDestroy {
  @ViewChild(DxTreeViewComponent)
  menu: DxTreeViewComponent;

  @Output()
  selectedItemChanged = new EventEmitter<string>();

  @Output()
  openMenu = new EventEmitter<any>();

  @Input()
  items: any[];

  @Input()
  set selectedItem(value: String) {
    if (this.menu.instance) {
      this.menu.instance.selectItem(value);
    }
  }

  private _compactMode = false;
  @Input()
  get compactMode() {
    return this._compactMode;
  }
  set compactMode(val) {
    this._compactMode = val;
    if (val && this.menu.instance) {
      this.menu.instance.collapseAll();
    }
  }

  constructor(private elementRef: ElementRef) { }

  updateSelection(event) {
    const nodeClass = 'dx-treeview-node';
    const selectedClass = 'dx-state-selected';
    const leafNodeClass = 'dx-treeview-node-is-leaf';
    const element: HTMLElement = event.element;

    const rootNodes = element.querySelectorAll(`.${nodeClass}:not(.${leafNodeClass})`);
    Array.from(rootNodes).forEach(node => {
      node.classList.remove(selectedClass);
    });

    let selectedNode = element.querySelector(`.${nodeClass}.${selectedClass}`);
    while (selectedNode && selectedNode.parentElement) {
      if (selectedNode.classList.contains(nodeClass)) {
        selectedNode.classList.add(selectedClass);
      }

      selectedNode = selectedNode.parentElement;
    }

    const ds = event.component.option('items');
    const url = event.component.getSelectedNodesKeys();
    if (ds && url && url.length > 0 && ds.length > 0) {
      for (const item of ds) {
        if (item.items) {
          const fd = item.items.find(x => x.path == url[0]);
          if (fd && item.icon.indexOf('_tomato') < 0) {
            item.icon = item.icon.split('.svg')[0] + "_tomato.svg";
          }
        } else if (!item.items && item.path === url[0] && item.icon.indexOf('_tomato') < 0) {
          item.icon = item.icon.split('.svg')[0] + '_tomato.svg';
        }
      }
    }
  }

  onItemClick(event) {
    // manareala (PORNOOO)
    const ds = event.component.option('items');
    if (ds) {
      for (const item of ds) {
        if (item.icon.indexOf('_tomato') > -1 && (event.node.parent || event.node.children.length === 0)) {
          if (item.icon) {
            item.icon = item.icon.split('_tomato')[0] + '.svg';
          }
        }
      }
    }
    if (event.node.parent && event.node.parent.itemData.icon) {
      event.node.parent.itemData.icon = event.node.parent.itemData.icon.split('.svg')[0] + '_tomato.svg';
    } else if (event.node.children && event.node.children.length === 0 && event.itemData && event.itemData.icon) {
      event.itemData.icon = event.itemData.icon.split('.svg')[0] + '_tomato.svg';
    }
    this.selectedItemChanged.emit(event);
  }

  onMenuInitialized(event) {
    event.component.option('deferRendering', false);
  }

  ngAfterViewInit() {
    events.on(this.elementRef.nativeElement, 'dxclick', (e) => {
      this.openMenu.next(e);
    });
  }

  ngOnDestroy() {
    events.off(this.elementRef.nativeElement, 'dxclick');
  }
}

@NgModule({
  imports: [DxTreeViewModule],
  declarations: [NavigationMenuComponent],
  exports: [NavigationMenuComponent]
})
export class NavigationMenuModule { }
