import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { DataService } from '../../service/data.service';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent {
    breadObj: any[];
    constructor(private router: Router, private ds: DataService) {
        const menuItems = ds.getMenuItems();
        this.breadObj = [];
        this.router.events.forEach((event) => {
            if (event instanceof (NavigationStart)) {
                this.breadObj = [];
                if (event.url) {
                    if (event.url === '/profile') {
                        this.breadObj = [{ icon: 'user', text: 'Profil' }];
                    }

                    const urlItems = event.url.split('/');
                    const mainItem = menuItems.find(x => x.infoPath === ('/' + urlItems[1]));
                    let secondItem: any;
                    if (mainItem && mainItem.items) {
                        secondItem = mainItem.items.find(x => x.path === event.url);
                    }
                    if (mainItem) {
                        const ic = mainItem.icon.split('.svg');
                        this.breadObj = [{ icon: ic[0] + '_tomato.svg', text: mainItem.text }];
                    }
                    if (secondItem) {
                        this.breadObj.push({ text: secondItem.text });
                    }
                }
            }
        });
    }
}
