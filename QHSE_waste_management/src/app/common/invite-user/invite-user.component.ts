import { Component, OnInit, Input, Output, EventEmitter, ViewChild, NgModule } from '@angular/core';
import { DataService } from '../../service/data.service';
import notify from 'devextreme/ui/notify';
import { DxFormComponent, DxPopupModule, DxScrollViewModule, DxFormModule } from 'devextreme-angular';
import { UIService } from '../../service/ui.service';

@Component({
    selector: 'app-invite-user',
    templateUrl: './invite-user.component.html'
})
export class InviteUserComponent implements OnInit {
    @Input() popupVisible: boolean;
    @Input () roleId: string;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onClosePopup = new EventEmitter<any>();
    onSendInvite: any;
    inviteDetails: any;

    @ViewChild('formInvite') formInvite: DxFormComponent;
    constructor(public ds: DataService, public ui: UIService) { }

    ngOnInit() {
        this.onSendInvite = this.onSendInviteEv.bind(this);
    }

    onSendInviteEv(e: any) {
        this.ds.inviteUser(this.inviteDetails, this.roleId).then(data => {
            if (data === 'OK') {
                notify('Invitație trimisă cu succes.', 'success', 3000);
                this.inviteDetails = {};
                this.formInvite.instance.resetValues();
                this.onClosePopup.emit(true);
            } else {
                notify('Datele introduse nu au putut fi procesate', 'error', 3000);
            }
        }, err => {
            this.ds.serverError(err);
        });
    }

    onHiddenPopup(e: any) {
        this.inviteDetails = {};
        this.formInvite.instance.resetValues();
        this.onClosePopup.emit(true);
    }

}
@NgModule({
    imports: [DxPopupModule, DxScrollViewModule, DxFormModule],
    declarations: [InviteUserComponent],
    exports: [InviteUserComponent]
})
export class InviteUserModule { }
