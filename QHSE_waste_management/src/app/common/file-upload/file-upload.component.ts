import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { iBisAuthService } from '@dohu/ibis-auth';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { environment } from '../../../environments/environment';
import { DataService } from '../../service/data.service';
import { UIService } from '../../service/ui.service';

@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html'
})
export class FileUploadComponent implements OnInit {
    @Input() gridData: any;

    @ViewChild('filesGrid') filesGrid: DxDataGridComponent;
    // serverFilePath: string;
    onDownloadClick: any;
    onUploadNewFileClick: any;
    isCreateTicketVisible: boolean;
    constructor(public ui: UIService, public ds: DataService, public auth: iBisAuthService, private http: Http) { }

    ngOnInit() {
        // this.serverFilePath = environment.saasServer + 'Download/';
        this.isCreateTicketVisible = false;
        this.onUploadNewFileClick = this.onUploadNewFileClickEv.bind(this);
        this.onDownloadClick = this.onDownloadClickEv.bind(this);
    }

    onCreateTicketClose(e: any) {
        this.isCreateTicketVisible = false;
    }

    onUploadNewFileClickEv() {
        this.createUploadEvent(this.gridData.data, this.filesGrid);
    }

    onDownloadClickEv(e: any) {
        const apiUrl = environment.saasServer + 'Download/' + e.row.data.id;
        this.ds.getFile(apiUrl);
    }

    onFilesGridInit(e: any) {
        if (e.component.option('dataSource') == null) {
            e.component.option('dataSource', this.ds.getFileInfo(this.gridData.data.id));
        }
    }

    toolbarPreparing(event: any) {
        this.ui.prepareToolbar(event);

        // event.toolbarOptions.items.unshift({
        //     widget: 'dxButton',
        //     options: {
        //         icon: 'assets/img/support.svg', hint: 'Cere asistență', onClick: (e: any) => {
        //             this.isCreateTicketVisible = true;
        //         }
        //     },
        //     location: 'after'
        // });

        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: {
                icon: 'assets/img/grid_add.svg', hint: 'Adaugă un fișier', onClick: (e: any) => {
                    this.createUploadEvent(this.gridData.data, this.filesGrid);
                }
            },
            location: 'after'
        });
    }

    createUploadEvent(data: any, gridComponent) {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', '.*');
        input.addEventListener('change', (e) => {
            this.uploadFile(input.files[0], data, gridComponent);
        }, false);

        input.click();
    }

    uploadFile(fileData: any, entity: any, gridComponent: any) {
        const file: File = fileData;
        const formData: FormData = new FormData();
        formData.append('uploadFile', file, entity.id + '_' + file.name);

        const headers = new Headers();
        const currentUser = JSON.parse(sessionStorage.getItem('currentUserQHSE'));
        headers.set('Authorization', 'Bearer ' + currentUser.token);

        const options = new RequestOptions({ headers: headers });
        const apiUrl1 = environment.saasServer + 'UploadFile';
        this.http.post(apiUrl1, formData, options).subscribe(
            () => {
                gridComponent.instance.refresh();
                notify('Fisierele au fost uploadate cu success', 'success', 2000);
            },
            error => {
                this.ds.serverError(error);
            }
        );
    }
}
