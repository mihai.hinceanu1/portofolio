import { Component, Input } from '@angular/core';


@Component({
    selector: 'app-graph-line',
    templateUrl: 'graph.component.html'
})
export class GraphComponent {
    @Input() txt: any;
    @Input() typeValue: any[];
    @Input() typeOp: any[];
    @Input() uom: number;
    @Input() intervalDate: string;
    constructor() {
    }
}
