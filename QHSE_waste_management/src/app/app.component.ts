import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import { DataService } from './service/data.service';
import { EntityStoreOptions } from '@dohu/ibis-entity';
import { UIService } from './service/ui.service';
import { Router } from '@angular/router';
import { locale, loadMessages } from 'devextreme/localization';
import 'devextreme-intl';
import * as  roMessages from 'assets/localization/messages/ro.json';
import { HttpClient } from '@angular/common/http';
import { iBisAuthService } from '@dohu/ibis-auth';
import { ProxyService } from './service/proxy.service';
import themes from 'devextreme/ui/themes';
import { currentTheme } from 'devextreme/viz/themes';
import { iBisGridConfigService, iBisLanguageService } from '@dohu/ibis-common';

declare var require: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    appVersion: string;
    globalVar: any;

    constructor(public ds: DataService, public ui: UIService, public proxy: ProxyService,
        public auth: iBisAuthService, http: HttpClient, private router: Router, public localization: iBisLanguageService) {
        EntityStoreOptions.DefaultServer = environment.defaultUrlServer;
        auth.deployId = environment.deployId;
        auth.authServer = environment.authServer;
        auth.saasServer = environment.saasServer;
        auth.appName = 'QHSE';

        EntityStoreOptions.Http = http;
        localization.server = 'https://api-test.sistemis.ro/saas/';

        // this.globalVar = JSON.parse(localStorage.getItem('globalVarQHSE')) || {};
        localization.locale('en');

        const { version: appVersion } = require('../../package.json');
        this.appVersion = appVersion;
        locale('ro');
        loadMessages(roMessages.default);
        this.ds.checkForTheme();
    }

    checkUrl() {
        return (this.router.url.indexOf('/login') > -1) || (this.router.url.indexOf('/register') > -1)
            || (this.router.url.indexOf('/confirmInvite') > -1) || (this.router.url.indexOf('/resetPassword') > -1);
    }
}

