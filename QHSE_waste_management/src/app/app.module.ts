import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { routes } from './app.route';

import {
  DxDataGridModule, DxMenuModule, DxButtonModule, DxTextBoxModule, DxDateBoxModule, DxSelectBoxModule, DxPopupModule, DxTextAreaModule,
  DxFileUploaderModule, DxLoadPanelModule, DxValidatorModule, DxTabPanelModule, DxFormModule, DxToolbarModule, DxAutocompleteModule,
  DxTagBoxModule, DxLookupModule, DxCheckBoxModule, DxPieChartModule, DxChartModule, DxRangeSelectorModule, DxListModule, DxBoxModule,
  DxTabsModule, DxMultiViewModule, DxPopoverModule, DxValidationGroupModule, DxScrollViewModule, DxTreeListModule,
  DxCalendarModule, DxValidationSummaryModule, DxNumberBoxModule, DxDropDownBoxModule, DxSwitchModule
} from 'devextreme-angular';

import { AppComponent } from './app.component';
import { LoginComponent } from './view/login/login.component';
import { EliminateComponent } from './view/waste/eliminate/eliminate.component';
import { TransportComponent } from './view/waste/transport/transport.component';
import { TaxComponent } from './view/document/tax/tax.component';
import { ControlComponent } from './view/waste/control/control.component';
import { AlertComponent } from './view/alert/alert.component';
import { ReportExtComponent } from './view/report/reportExt.component';
import { ReportIntComponent } from './view/report/reportInt.component';
import { AgencyComponent } from './view/admin/agency/agency.component';
import { PartyComponent } from './view/admin/party/party.component';
import { WastetypeComponent } from './view/admin/wastetype/wastetype.component';
import { CapitalizationComponent } from './view/waste/capitalization/capitalization.component';
import { DataService } from './service/data.service';
import { MonthlyComponent } from './view/waste/monthly/monthly.component';
import { UserModule } from './view/admin/user/user.component';
import { GeographicareaComponent } from './view/admin/geographicarea/geographicarea.component';
import { WastefileComponent } from './view/document/wastefile/wastefile.component';
import { UIService } from './service/ui.service';
import { EditControlComponent } from './view/waste/control/editControl.component';
import { EditActionComponent } from './view/waste/control/editAction.component';
import { EditSanctionComponent } from './view/waste/control/editSanction.component';
import { EditMonthlyComponent } from './view/waste/monthly/editMonthly.component';
import { EditTaxComponent } from './view/document/tax/editTax.component';
// import { FinishTransportComponent } from './view/waste/transport/finishTransport.component';
import { ArchiveComponent } from './view/document/archive/archive.component';
import { EditArchiveComponent } from './view/document/archive/editArchive.component';
import { ContractComponent } from './view/document/contract/contract.component';
import { PermitComponent } from './view/document/permit/permit.component';
import { InvoiceComponent } from './view/document/invoice/invoice.component';
import { EditInvoiceComponent } from './view/document/invoice/editInvoice.component';
import { EditAgencyComponent } from './view/admin/agency/editAgency.component';
import { EditGareasComponent } from './view/admin/geographicarea/editGarea.component';
import { EditUserComponent } from './view/admin/user/editUser.component';
import { EditWasteTypeComponent } from './view/admin/wastetype/editWastetype.component';
import { EditWastetypeDetailComponent } from './view/admin/wastetype/editWastetypeDetail.component';
import { NomenclatureComponent } from './view/admin/nomenclature/nomenclature.component';
import { EditNomenclatureComponent } from './view/admin/nomenclature/editNomenclature.component';
import { EditPartyWorkCenterComponent } from './view/admin/party/editPartyWorkCenter.component';
// import { GenerareTransportComponent } from './wizard/generareTransport.component';
import { RegisterComponent } from './view/register/register.component';
// import { GraphComponent } from './common/graph/graph.component';
import { FileUploadComponent } from './common/file-upload/file-upload.component';
import { AccountComponent } from './view/account/account.component';
import { iBisAuthService, iBisAuthModule } from '@dohu/ibis-auth';
import { iBisCommonModule, iBisGridConfigService, iBisLanguageService } from '@dohu/ibis-common';
// import { InitAppComponent } from './wizard/init-app.component';
import { HttpModule } from '@angular/http';
import { NavToolbarModule } from './common/nav-toolbar/nav-toolbar.component';
import { HeaderModule } from './common/header/header.component';
import { NavigationMenuModule } from './common/navigation-menu/navigation-menu.component';
import { ScreenService } from './service/screen.service';
import { SingleCardModule } from './common/single-card/single-card.component';
import { FooterModule } from './common/footer/footer.component';
import { TransferComponent } from './view/waste/transfer/transfer.component';
import { EditTransferComponent } from './view/waste/transfer/editTransfer.component';
import { CreateTicketModule } from './common/create-ticket/create-ticket.component';
import { EditGenerateService } from './view/waste/generate/editGenerate.service';
// import { EditGenerateComponent } from './view/waste/generate/editGenerate.component';
import { GenerateComponent } from './view/waste/generate/generate.component';
import { UnitComponent } from './view/admin/unit/unit.component';
// import { EditUnitComponent } from './view/admin/unit/editUnit.component';
// import { EditUnitWasteComponent } from './view/admin/unit/editUnitWaste.component';
import { EditUnitService } from './view/admin/unit/editUnit.service';
import { EditUnitWasteService } from './view/admin/unit/editUnitWaste.service';
import { EditPartyService } from './view/admin/party/editParty.service';
// import { EditTransportComponent } from './view/waste/transport/editTransport.component';
import { EditTransportService } from './view/waste/transport/editTransport.service';
import { FinishTransportService } from './view/waste/transport/finishTransport.service';
import { EditTransferService } from './view/waste/transfer/editTransfer.service';
import { ProxyService } from './service/proxy.service';
import { AnalizeModule } from './view/analize/analize.module';
import { AnalizeService } from './view/analize/analize.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GenerateComponent,
    EliminateComponent,
    TransportComponent,
    TaxComponent,
    ControlComponent,
    PermitComponent,
    WastefileComponent,
    InvoiceComponent,
    AlertComponent,
    ReportExtComponent,
    ReportIntComponent,
    AgencyComponent,
    PartyComponent,
    UnitComponent,
    WastetypeComponent,
    CapitalizationComponent,
    MonthlyComponent,
    ArchiveComponent,
    GeographicareaComponent,
    NomenclatureComponent,
    ContractComponent,
    // GraphComponent,
    // InitAppComponent,
    FileUploadComponent,

    // EditGenerateComponent,
    // EditUnitComponent,
    // EditUnitWasteComponent,

    EditControlComponent,
    EditActionComponent,
    EditSanctionComponent,
    EditMonthlyComponent,
    EditTaxComponent,
    EditArchiveComponent,
    EditInvoiceComponent,
    EditAgencyComponent,
    EditGareasComponent,
    EditUserComponent,
    EditWasteTypeComponent,
    EditWastetypeDetailComponent,
    EditNomenclatureComponent,
    EditPartyWorkCenterComponent,
    // EditTransportComponent,
    // FinishTransportComponent,
    // GenerareTransportComponent,
    RegisterComponent,
    FileUploadComponent,
    AccountComponent,
    TransferComponent,
    EditTransferComponent
  ],
  imports: [
    BrowserModule,
    NavToolbarModule,
    HeaderModule,
    NavigationMenuModule,
    SingleCardModule,
    FooterModule,
    UserModule,
    AnalizeModule,

    HttpClientModule,
    DxMenuModule,
    DxButtonModule,
    DxDataGridModule,
    DxMenuModule,
    DxTagBoxModule,
    DxButtonModule,
    DxTextBoxModule,
    DxNumberBoxModule,
    DxDateBoxModule,
    DxSelectBoxModule,
    DxPopupModule,
    DxTextAreaModule,
    DxFileUploaderModule,
    DxValidationGroupModule,
    DxValidationSummaryModule,
    DxLoadPanelModule,
    DxListModule,
    DxPopoverModule,
    DxBoxModule,
    DxValidatorModule,
    DxCalendarModule,
    DxTabPanelModule,
    DxFormModule,
    DxTabsModule,
    DxToolbarModule,
    DxAutocompleteModule,
    DxLookupModule,
    DxCheckBoxModule,
    DxPieChartModule,
    DxChartModule,
    DxRangeSelectorModule,
    DxMultiViewModule,
    DxScrollViewModule,
    DxDropDownBoxModule,
    DxSwitchModule,
    DxTreeListModule,
    HttpModule,
    iBisAuthModule,
    iBisCommonModule,
    CreateTicketModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false })
  ],
  providers: [
    DataService,
    ProxyService,
    EditGenerateService,
    EditUnitService,
    EditUnitWasteService,
    EditPartyService,
    EditTransportService,
    FinishTransportService,
    EditTransferService,
    AnalizeService,
    UIService,
    ScreenService,
    iBisAuthService,
    iBisGridConfigService,
    HttpClient,
    iBisLanguageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
