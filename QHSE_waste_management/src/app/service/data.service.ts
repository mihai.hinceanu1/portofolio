import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { EntityStore, EntityQuery, EntityStoreOptions, SimpleFunctionType } from '@dohu/ibis-entity';
import notify from 'devextreme/ui/notify';
import { environment } from '../../environments/environment';
import { iBisAuthService } from '@dohu/ibis-auth';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import themes from 'devextreme/ui/themes';
import { currentTheme } from 'devextreme/viz/themes';
import { iBisLanguageService } from '@dohu/ibis-common';

@Injectable()
export class DataService {

    public userName: string;
    public userId: string;
    public monthTwo = new Date();
    public remoteOperations: boolean;
    public isButtonDisable: boolean;
    isLoginModalVisible: any;
    statistic: { trUnf: number; wtNo: number; wkNo: number; trNo: number; avNo: number; masNo: number; };
    firstMonthDay: Date;
    lastMonthDay: Date;

    uom = [{ id: 1, value: 'tone' }, { id: 2, value: 'mc' }];
    zoneType = [{ 'id': 1, 'name': 'Localitate' }, { 'id': 2, 'name': 'Judet' }, { 'id': 3, 'name': 'Regiune' }];
    unitType = [{ id: 0, value: 'Grup' }, { id: 1, value: 'Entitate' }, { id: 2, value: 'Punct de lucru' }];
    physicalCondition = [{ id: 0, value: 'Solid' }, { id: 1, value: 'Lichid' }];
    evaluationBool = [{ id: false, value: 'Cântărită' }, { id: true, value: 'Estimată' }];
    dsInvoiceType = [{ id: false, value: 'Factură cumpărare' }, { id: true, value: 'Factură vânzare' }];
    evaluation = [{ id: 0, value: 'Cântărită' }, { id: 1, value: 'Estimată' }];
    source = [{ id: 1, value: 'Generată' }, { id: 2, value: 'Preluată' }];
    hazard = [{ id: 0, value: 'Nepericulos' }, { id: 1, value: 'Periculos' }];
    weighing = [{ id: 1, value: 'Cântărită' }, { id: 2, value: 'Estimată' }];
    controlType = [{ id: 1, value: 'Intern' }, { id: 2, value: 'Extern' }];
    actionStatus = [{ id: 0, value: 'În lucru' }, { id: 1, value: 'Realizat' }];
    sanctionStatus = [{ id: 0, value: 'Primită' }, { id: 1, value: 'Contestată' }, { id: 2, value: 'Platită' }];
    defaultParty: { bu: string, ent: string, wc: string };

    constructor(public router: Router, public http: HttpClient, public auth: iBisAuthService, public lg: iBisLanguageService) {

        EntityStoreOptions.OnError = (jqXHR) => {
            if (jqXHR.status === 401) {
                // auth.logout();
                notify('Authorization has expired. Retry to login.', 'error', 3000);
                // this.router.navigateByUrl('/login');
                this.auth.user.isAuth = false;
                this.isLoginModalVisible = true;
            }
        };

        this.firstMonthDay = new Date();
        this.firstMonthDay.setDate(1);
        this.lastMonthDay = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);
        this.isButtonDisable = false;
        setTimeout(() => {
            if (this.auth.isAuth) {
                this.initDefaultParty();
            }
        }, 0);
    }

    static getHarzardName(hazard: number): string {
        return hazard === 1 ? 'Periculos' : 'Nepericulos';
    }

    static formatDate(date: Date) {
        if (date == null || date === undefined) {
            return '';
        }
        date = new Date(date);
        return EntityStore.toD2(date.getDate()) + '.' +
            EntityStore.toD2((date.getMonth() + 1)) + '.' + date.getFullYear();
    }
    static formatDateTime(date: Date) {
        if (date == null || date === undefined) {
            return '';
        }
        date = new Date(date);
        return EntityStore.toD2(date.getDate()) + '.' +
            EntityStore.toD2((date.getMonth() + 1)) + '.' +
            date.getFullYear() + ' ' + date.getHours() + ':' + EntityStore.toD2(date.getMinutes());
    }

    customHeightSubGrid() {
        return window.innerHeight - 173;
    }

    public getDate(rowData: any) {
        const col = this as any;
        return DataService.formatDate(rowData[col.dataField]);
    }

    getDateTime(rowData: any) {
        const col = this as any;
        return DataService.formatDateTime(rowData[col.dataField]);
    }

    public getFile(url: string) {
        let headers = new HttpHeaders();
        for (const key in EntityStoreOptions.Headers) {
            if (EntityStoreOptions.Headers.hasOwnProperty(key)) {
                headers = headers.append(key, EntityStoreOptions.Headers[key]);
            }
        }
        this.http.get(url, { responseType: 'blob', observe: 'response' as 'body', headers: headers }).subscribe((res: any) => {
            const header = res.headers.get('Content-Disposition');
            const startIndex = header.indexOf('filename=') + 9;
            const endIndex = header.length;
            const fileName = header.substring(startIndex, endIndex);

            const blob = new Blob([res.body], { type: res.type });
            const fileUrl = window.URL.createObjectURL(blob);
            const fileLink = document.createElement('a');
            fileLink.href = fileUrl;
            fileLink.download = fileName;
            // fileLink.click();
            fileLink.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window })); // for mozilla
            // const pwa = window.open(fileUrl);
            // if (!pwa || pwa.closed || typeof pwa.closed === 'undefined') {
            //     alert( 'Please disable your Pop-up blocker and try again.');
            // }
        }, err => {
            console.log(err);
        }
        );
    }

    initDefaultParty() {
        this.defaultParty = { bu: null, ent: null, wc: null };
        this.lookingForSingleParty().then(data => {
            this.defaultParty = data;
        });
    }

    lookingForSingleParty() {
        return EntityStore.execute('GetSingleFilterParty', {});
    }

    getSymbolOf(name: any) {
        const us = name.split(' ');
        return (us[0][0].toUpperCase() || '') + (us[1] ? us[1][0].toUpperCase() : (us[0][1].toUpperCase() || 'AA'));
    }

    validationCallbackTrueFalse(e: any) {
        return e.value == null ? false : true;
    }

    serverError(error?: any) {
        let msg = '';
        if (error && typeof error === 'string') {
            const start = error.indexOf('The exception message is');
            const end = error.indexOf('The exception stack trace is:');
            msg = error.substring(start + 24, end - 34);
        } else if (error && typeof error === 'object') {
            const start = error._body.indexOf('The exception message is');
            const end = error._body.indexOf('The exception stack trace is:');
            msg = error._body.substring(start + 24, end - 34);
        }
        if (msg.toLocaleLowerCase().indexOf('unauthorized') > -1) {
            this.isLoginModalVisible = true;
        }
        notify('Server error ' + (error ? ('[' + msg + ']') : ''), 'error', 3000);
    }

    public get isSuperAdmin() {
        const user = JSON.parse(sessionStorage.getItem('currentUserQHSE'));
        if (user.id === '00000000-0000-0000-0000-000000000000') {
            return true;
        } else {
            return false;
        }
    }

    /* Deșeuri tab */
    getWasteGenerate(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('GenerateView').addOrderByDesc(['g.date']), false, 'g_id');
        return new CustomStore(cso);
    }

    transportEntry(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('TransportView').eq('t_finishDate', null).addOrderBy(['t.no']), false, 't_id');
        return new CustomStore(cso);
    }

    getTransportFinish(workCenterId?: string, entityId?: string): CustomStore {
        const q = new EntityQuery('TransportView').neq('t_finishDate', null).addOrderBy(['t.no']);
        if (workCenterId) {
            q.eq('t.workCenterId', workCenterId);
        } else if (entityId) {
            q.linkEq('t.workCenterId', 'id', 'Party', 'parentId', entityId);
        }
        const cso = EntityStore.store(q, false, 't_id');
        return new CustomStore(cso);
    }

    getEliminateInfo(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('TransportView').neq('t_finishDate', null)
            .neq('t_eliminationId', null).addOrderBy(['t.no']), false, 't_id');
        return new CustomStore(cso);
    }

    addEditTransport(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Transport'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    getTransportViewById(id: string) {
        return EntityStore.fromQuery(new EntityQuery('TransportView').eq('t_id', id)).single();
    }

    removeTransport(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Transport')).remove(id);
    }

    capitalizationInfo(): CustomStore {
        // tslint:disable-next-line:max-line-length
        const cso = EntityStore.store(new EntityQuery('TransportView').neq('t_finishDate', null).neq('t_capitalizationId', null).addOrderBy(['t.no']), false, 't_id');
        return new CustomStore(cso);
    }

    getCapsByDate(wasteType: string, fromDate: Date, thruDate: Date) {
        const q = new EntityQuery('TransportView').eq('t_wasteTypeId', wasteType).neq('t_capitalizationId', null).addOrderBy(['t.no']);
        q.gte('t.sendDate', EntityStore.toDateTimeFilter(this.convertToDate(fromDate)));
        q.lte('t.sendDate', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
        q.fields.push('t.id', 't.sendDate', 't.quantity', 't.valuedQuantity', 'wt.code');
        return EntityStore.fromQuery(q, false, 't_id').load();
    }

    getTaxByDate(wc: string, fromDate: Date, thruDate: Date) {
        const q = new EntityQuery('TaxView').eq('t_workCenterId', wc).neq('t_payDate', null).addOrderBy(['t.no']);
        q.gte('t.payDate', EntityStore.toDateTimeFilter(this.convertToDate(fromDate)));
        q.lte('t.payDate', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
        q.fields.push('t.id', 't.payDate', 't.value', 'wc.name');
        return EntityStore.fromQuery(q, false, 't_id').load();
    }

    getInvoicesByDate(obj: any, isSales: boolean = false) {
        const q = new EntityQuery('Invoice').eq('entityId', obj.entity).eq('isSales', isSales).addOrderByDesc(['date']);
        if (obj.supplier) {
            q.eq('supplierId', obj.supplier);
        }
        q.gte('date', EntityStore.toDateTimeFilter(this.convertToDate(obj.fromDate)));
        q.lte('date', EntityStore.toDateTimeFilter(this.convertToDate(obj.thruDate)));
        q.fields.push('id', 'entityId', 'amount', 'supplierId', 'date');
        return EntityStore.fromQuery(q, false, 't_id').load();
    }

    getControlsByDate(entity: string, wc: string, fromDate: Date, thruDate: Date) {
        const q = new EntityQuery('ControlView').addOrderBy(['c.authority']);
        if (wc) {
            q.eq('c_workCenterId', wc);
        } else if (entity) {
            q.eq('c_entityId', entity);
        }
        q.gte('c.date', EntityStore.toDateTimeFilter(this.convertToDate(fromDate)));
        q.lte('c.date', EntityStore.toDateTimeFilter(this.convertToDate(thruDate)));
        q.fields.push('c.id', 'c.date', 'c.authority', 'wc.name', 'c.cost', 'ent.name');
        return EntityStore.fromQuery(q, false, 't_id').load();
    }

    convertToDate(date: any) {
        const d = new Date(date);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    getTransferView(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('TransferView').addOrderByDesc(['t.sendDate']));
        return new CustomStore(cso);
    }

    getTransferViewById(id: string) {
        return EntityStore.fromQuery(new EntityQuery('TransferView').eq('t_id', id)).single();
    }

    removeTrasfer(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Transfer')).remove(id);
    }

    // PORNO
    getWasteGenerateCode(): any {
        const q = new EntityQuery('GenerateView');
        q.fields.push('wt.code');
        return EntityStore.fromQuery(q).function(<any>[{ field: 'wt.code', ft: SimpleFunctionType.Sum }]);
    }

    addEditGenerate(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Generate'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    getMonthly(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('MonthlyStock').addOrderByDesc(['year', 'month']));
        return new CustomStore(cso);
    }
    getMonthlyById(id: string) {
        return EntityStore.fromQuery(new EntityQuery('MonthlyStock').eq('id', id)).single();
    }

    addEditAdjustment(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('GenerateAdjustment'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    recheckMonthlyStock(month, year) {
        return EntityStore.execute('RecheckMonthlyStock', { month: month, year: year }, environment.authServer);
    }

    archiveReport(method: string, obj: any) {
        return EntityStore.execute(method, obj);
    }

    checkForAdjustment(obj: any) {
        return EntityStore.fromQuery(new EntityQuery('GenerateAdjustment')
            .eq('month', obj.month).eq('year', obj.year).eq('wasteTypeId', obj.wasteTypeId)).single();
    }

    removeAdjustment(id: string) {
        return EntityStore.fromQuery(new EntityQuery('MonthlyStock')).remove(id);
    }

    getTax(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('TaxView').addOrderBy(['t.no']));
        return new CustomStore(cso);
    }

    addEditTax(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Tax'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeTax(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Tax')).remove(id);
    }

    getControlView(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('ControlView').addOrderBy(['c.date']));
        return new CustomStore(cso);
    }

    addEditControl(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Control'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    deleteControl(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Control')).remove(id);
    }

    getControlMeasures(controlId): CustomStore {
        const cso = EntityStore.store(new EntityQuery('ControlMeasure').eq('controlId', controlId));
        return new CustomStore(cso);
    }
    addEditMeasure(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('ControlMeasure'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    deleteMeasure(id: string) {
        return EntityStore.fromQuery(new EntityQuery('ControlMeasure')).remove(id);
    }

    getControlSanctions(controlId: string): CustomStore {
        const cso = EntityStore.store(new EntityQuery('ControlSanction').eq('controlId', controlId));
        return new CustomStore(cso);
    }
    addEditSanction(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('ControlSanction'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }
    deleteSanction(id: string) {
        return EntityStore.fromQuery(new EntityQuery('ControlSanction')).remove(id);
    }

    /* Documente tab */
    getArchive(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Archive'));
        return new CustomStore(cso);
    }

    addEditArchive(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Archive'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeArchive(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Archive')).remove(id);
    }

    getPartyMemberById(id: string, view?: boolean): CustomStore {
        if (view) {
            const q = new EntityQuery('PartyView').eq('p.parentId', id).eq('p.typeId', 2);
            return new CustomStore(EntityStore.store(q, false, 'p_id'));
        } else {
            const q = new EntityQuery('Party').eq('parentId', id).eq('typeId', 2);
            return new CustomStore(EntityStore.store(q, false, 'id'));
        }
    }

    getWorkCenterByParty(id: string, isView?: boolean): CustomStore {
        if (isView) {
            const q = new EntityQuery('PartyView').eq('p.parentId', id).eq('p.typeId', 4);
            return new CustomStore(EntityStore.store(q, false, 'p_id'));
        } else {
            const q = new EntityQuery('Party').eq('parentId', id).eq('typeId', 4);
            return new CustomStore(EntityStore.store(q, false, 'id'));
        }
    }
    getEntityByParentId(id: string): CustomStore {
        const q = new EntityQuery('Party').eq('parentId', id).eq('typeId', 1);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    checkInitApp() {
        return EntityStore.execute('CheckInitApp', {});
    }

    public getParentParty(typeId: number): CustomStore {
        const q = new EntityQuery('Party').eq('isActiv', true).eq('typeId', typeId - 1).addOrderBy(['name']);
        q.fields.push('id', 'name');
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getParty(type: string, isActiv = true, exceptId?: string): CustomStore {
        const q = new EntityQuery('Party').eq('isActiv', isActiv).addOrderBy(['name']);
        switch (type) {
            case 'pc': q.eq('typeId', 1); break;
            case 'bu': q.eq('typeId', 0); break;
            case 'ent': q.eq('typeId', 1); break;
            case 'un': q.lt('typeId', 3); break;
            case 'wc': q.eq('typeId', 2); break;
            case 'wa': q.eq('typeId', 1).eq('isActiv', true); break;
            case 'cr':
                q.conditionGroups.useOr = true;
                q.eq('typeId', 0).eq('typeId', 1).eq('typeId', 3);
                break;
            case 'ot': q.gt('typeId', 2); break;
            case 'ct': q.gt('typeId', 2).eq('isSubcontractor', false); break;
            case 'unPar': q.neq('id', exceptId).lt('typeId', 3); break;
        }
        q.fields.push('id', 'parentId', 'name');
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getPartyView(type: string, isActiv = true): CustomStore {
        const q = new EntityQuery('PartyView').addOrderBy(['p.name']);
        if (isActiv) {
            q.eq('p.isActiv', isActiv);
        }
        switch (type) {
            case 'pc': q.eq('p.typeId', 1); break;
            case 'bu': q.eq('p.typeId', 0); break;
            case 'ent': q.eq('p.typeId', 1); break;
            case 'un': q.lt('p.typeId', 3); break;
            case 'wc': q.eq('p.typeId', 2); break;
            case 'trt': q.eq('p.typeId', 3); break;
            case 'ct': q.gt('p.typeId', 2).eq('p.isSubcontractor', false); break;
        }
        return new CustomStore(EntityStore.store(q, false, 'p_id'));
    }

    getPartById(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Party').eq('id', id)).single();
    }

    getWorkingCenter(parentId): CustomStore {
        const q = new EntityQuery('Party').eq('typeId', 2).eq('parentId', parentId).addOrderBy(['name']);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getPermit(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Permit'), false, 'id');
        return new CustomStore(cso);
    }

    addEditPermit(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Permit'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removePermit(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Permit')).remove(id);
    }

    contracts(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Contract'), false, 'id');
        return new CustomStore(cso);
    }

    addEditContract(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Contract').addOrderByDesc(['endDate']));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeContract(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Contract')).remove(id);
    }

    invoices(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('Invoice').addOrderByDesc(['date']), false, 'id');
        return new CustomStore(cso);
    }

    addEditInvoice(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Invoice'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    inviteUser(userDetails: any, roleId: string) {
        if (roleId) {
            userDetails.roleId = roleId;
        }
        userDetails.role = 'user';
        return EntityStore.execute('Invite', userDetails, environment.authServer);
    }

    removeInvoice(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Invoice')).remove(id);
    }

    wasteType(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('WasteType').addOrderBy(['code']), false, 'id');
        return new CustomStore(cso);
    }

    getWasteTypeById(wasteTypeId: string) {
        const q = new EntityQuery('WasteType').eq('id', wasteTypeId);
        return EntityStore.fromQuery(q).single();
    }

    partyWaste(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('WasteTypePartyView').addOrderBy(['wt.code']), false, 'wtp_id');
        return new CustomStore(cso);
    }

    getPartyWasteViewByWc(wc: string): CustomStore {
        const cso = EntityStore.store(new EntityQuery('WasteTypePartyView').eq('wtp_workCenterId', wc).
            addOrderBy(['wt.code']), false, 'wtp_id');
        return new CustomStore(cso);
    }

    getWasteTypePartyView(id: string, type: string): CustomStore {
        const cso = EntityStore.store(new EntityQuery('WasteTypePartyView').eq(type, id).
            addOrderBy(['wt.code']), false, 'wtp_id');
        return new CustomStore(cso);
    }

    getWasteTypeForWorkCenter(workCenterId: string): CustomStore {
        const q = new EntityQuery('WasteType').neq('parentId', '00000000-0000-0000-0000-000000000000')
            .addOrderBy(['code']);
        q.distinct = true;
        if (workCenterId) {
            q.linkEq('id', 'wasteTypeId', 'WasteTypeParty', 'workCenterId', workCenterId);
        }
        q.fields.push('id', 'code', 'name', 'hazard', 'uom');
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getActiveWasteTypes(): CustomStore {
        const q = new EntityQuery('WasteType').neq('parentId', '00000000-0000-0000-0000-000000000000')
            .addOrderBy(['code']);
        q.distinct = true;
        q.link('id', 'wasteTypeId', new EntityQuery('WasteTypeParty'));
        return new CustomStore(EntityStore.store(q, false, 'id'));

    }

    getWasteTypeByEntity(entityId?: string, buId?: string) {
        const q = new EntityQuery('WasteType').addOrderBy(['code']);
        let qp = new EntityQuery('Party');
        if (buId) {
            qp.linkEq('parentId', 'id', 'Party', 'parentId', buId);
        }
        if (entityId) {
            qp.eq('typeId', 2).eq('parentId', entityId);
        }
        q.link('id', 'wasteTypeId', new EntityQuery('WasteTypeParty').link('workCenterId', 'id', qp));
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    getWasteTypeParty(wasteTypeId: string): Promise<any> & JQueryPromise<any> {
        const q = new EntityQuery('WasteTypeParty').eq('wasteTypeId', wasteTypeId);
        return EntityStore.fromQuery(q).single();
    }

    resetValidation(component) {
        component.reset();
        component.option('isValid', true);
    }

    addEditWasteParty(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('WasteTypeParty'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    getPartyWasteById(workcenterId: string) {
        return EntityStore.fromQuery(new EntityQuery('WasteTypeParty').eq('workCenterId', workcenterId)).single();
    }

    initStatistics() {
        this.statistic = { trUnf: 0, wtNo: 0, wkNo: 0, trNo: 0, avNo: 0, masNo: 0 };
        EntityStore.execute('GlobalStatistic', {}).then(data => {
            this.statistic = data;
        }, err => {
            this.serverError(err);
        });
    }

    getGraphData(filter: any) {
        const obj: any = {};
        Object.assign(obj, filter);
        obj.fromDate = DataService.formatDate(obj.fromDate);
        obj.thruDate = DataService.formatDate(obj.thruDate);
        return EntityStore.execute('GetGraphData', obj);
    }

    removeWasteParty(id: string) {
        return EntityStore.fromQuery(new EntityQuery('WasteTypeParty')).remove(id);
    }

    aPermits(): CustomStore {
        this.monthTwo.setMonth(new Date().getMonth() + 2);
        const q = new EntityQuery('Permit').eq('typeId', 2).lt('validity', this.monthTwo).eq('isValid', true);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    aPermitsInt(): CustomStore {
        this.monthTwo.setMonth(this.monthTwo.getMonth() + 4);
        const q = new EntityQuery('Permit').eq('typeId', 3).lt('validity', this.monthTwo).eq('isValid', true);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    aContracts(): CustomStore {
        this.monthTwo.setMonth(new Date().getMonth() + 3);
        const q = new EntityQuery('Contract').eq('isValid', true).lt('endDate', this.monthTwo);
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    aTransports(): CustomStore {
        const days = new Date();
        days.setDate(days.getDate() - 3);
        const q = new EntityQuery('TransportView').eq('t_finishDate', null).lt('t_sendDate', days);
        return new CustomStore(EntityStore.store(q, false, 't_id'));
    }

    aControlMeasures(): CustomStore {
        const q = new EntityQuery('ControlMeasure').neq('actionStatus', 1);
        return new CustomStore(EntityStore.store(q));
    }

    getAgencies(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('AgencyView').addOrderBy(['a.name']), false, 'a_id');
        return new CustomStore(cso);
    }

    addEditAgency(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Agency'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeAgency(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Agency')).remove(id);
    }

    getAgencyById(id: string): Promise<any> & JQueryPromise<any> {
        return EntityStore.fromQuery(new EntityQuery('Agency').eq('id', id)).single();
    }

    geographicAreas(typeId: number, field?: string): CustomStore {
        const q = new EntityQuery('GeographicArea').addOrderBy(['name']);
        if (typeId) {
            q.eq('typeId', typeId);
            q.fields.push('id', 'name');
        }
        const cso = EntityStore.store(q, false, field);
        return new CustomStore(cso);
    }

    addEditGeographicArea(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('GeographicArea'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeGeographicArea(id: string) {
        return EntityStore.fromQuery(new EntityQuery('GeographicArea')).remove(id);
    }

    getLocalitiesByCountyId(countyId: string) {
        const q = new EntityQuery('GeographicArea').eq('typeId', 1).eq('parentId', countyId).addOrderBy(['name']);
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getLocalityById(id: string): Promise<any> & JQueryPromise<any> {
        return EntityStore.fromQuery(new EntityQuery('GeographicArea').eq('id', id)).single();
    }

    nomenclaturesVal(typeId): CustomStore {
        const cso = EntityStore.store(new EntityQuery('EnumValue').eq('typeId', typeId).addOrderBy(['code']), false, 'id');
        return new CustomStore(cso);
    }

    getEnumValue(code: string): CustomStore {
        const q = new EntityQuery('EnumValue').addOrderBy(['code']).
            linkEq('typeId', 'id', 'EnumType', 'code', code);
        q.fields.push('id', 'code', 'description');
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    enumDisplay(info: any) {
        return info ? info.code + ' - ' + (info.description || '') : '';
    }

    nomenclaturesType(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('EnumType').addOrderBy(['code']), false, 'id');
        return new CustomStore(cso);
    }

    addEditNomenclatures(obj: any, id: string, model: string) {
        const s = EntityStore.fromQuery(new EntityQuery(model));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeNomen(id: string) {
        return EntityStore.fromQuery(new EntityQuery('EnumType')).remove(id);
    }

    validTransports(): CustomStore {
        const q = new EntityQuery('Party').like('activitiesType', '%Transport%').addOrderBy(['name']);
        // .linkEq('id', 'toId', 'Contract', 'isValid', true);
        q.distinct = true;
        q.fields.push('id', 'name');
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    validDest(): CustomStore {
        const q = new EntityQuery('Party').addOrderBy(['name']).
            like('activitiesType', '%Eliminare%').like('activitiesType', '%Valorificare%');
        // .linkEq('id', 'toId', 'Contract', 'isValid', true);
        q.conditionGroups.useOr = true;
        q.distinct = true;
        q.fields.push('id', 'name');
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getRoles() {
        const q = new EntityQuery('UserLoginRole');
        q.link('groupId', 'rolegroupid', new EntityQuery('ApplicationConfig')
            .linkEq('id', 'configId', 'Application', 'id', this.auth.currentCompany.id));
        return new CustomStore(EntityStore.store(q, false, 'id', environment.saasServer));
    }

    getUsersByRoleId(roleId: string) {
        const q = new EntityQuery('ApplicationRole').eq('appId', this.auth.currentCompany.id);
        if (roleId) {
            q.eq('roleId', roleId);
        }
        return new CustomStore(EntityStore.store(new EntityQuery('UserLogin').link('id', 'userId', q), false, 'id', environment.saasServer));
    }

    updateUserRole(newRoleId: string, arId: string) {
        return EntityStore.fromQuery(new EntityQuery('ApplicationRole'), false, 'id', environment.saasServer)
            .update(arId, { roleId: newRoleId });
    }

    getRoleByUserId(userId: string) {
        const q = new EntityQuery('UserLoginRole');
        q.fields.push('id', 'name');
        const arQ = new EntityQuery('ApplicationRole').eq('appId', this.auth.currentCompany.id).eq('userId', userId);
        arQ.fields.push('id');
        return EntityStore.fromQuery(q.link('id', 'roleId', arQ), false, 'id', environment.saasServer).query();
    }

    securityUsers(): CustomStore {
        const q = new EntityQuery('UserLogin');
        q.link('id', 'userId', new EntityQuery('ApplicationRole').eq('appId', this.auth.currentCompany.id));
        const cso = EntityStore.store(q, false, 'id', environment.saasServer);
        return new CustomStore(cso);
    }

    updateUserLogin(obj: any) {
        return EntityStore.fromQuery(new EntityQuery('UserLogin'), false, 'id', environment.saasServer).update(this.auth.user.id, obj);
    }

    userLoginParty(userId: string): CustomStore {
        const q = new EntityQuery('UserLoginParty').eq('userId', userId);
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    userLoginZone(userId: string): CustomStore {
        const q = new EntityQuery('UserLoginZone').eq('userId', userId);
        const cso = EntityStore.store(q, false, 'id');
        return new CustomStore(cso);
    }

    getFileInfo(entityId: string): CustomStore {
        const cso = EntityStore.store(new EntityQuery('FileInfo')
            .eq('entityId', entityId).addOrderBy(['name']), false, 'id', environment.saasServer);
        return new CustomStore(cso);
    }

    getFiles() {
        const cso = EntityStore.store(new EntityQuery('FileInfo').addOrderBy(['name']), false, 'id');
        return new CustomStore(cso);
    }

    wasteTypes(): CustomStore {
        const cso = EntityStore.store(new EntityQuery('WasteType').addOrderBy(['code']), false, 'id');
        return new CustomStore(cso);
    }

    getUnitWasteTypes(): CustomStore {
        const q = new EntityQuery('WasteType').neq('parentId', '00000000-0000-0000-0000-000000000000')
            .addOrderBy(['code']);
        q.fields.push('id', 'code');
        return new CustomStore(EntityStore.store(q, false, 'id'));
    }

    addEditWastetype(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('WasteType'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    removeWasteType(id: string) {
        return EntityStore.fromQuery(new EntityQuery('WasteType')).remove(id);
    }

    getPartyById(id: string): Promise<any> & JQueryPromise<any> {
        return EntityStore.fromQuery(new EntityQuery('Party').eq('id', id)).single();
    }

    getCountId(localityId: string) {
        const q = new EntityQuery('GeographicArea').eq('id', localityId);
        q.fields.push('parentId');
        return EntityStore.fromQuery(q).single();
    }

    getPartyViewById(id: string) {
        return EntityStore.fromQuery(new EntityQuery('PartyView').eq('p_id', id)).single();
    }

    addEditParty(obj: any, id: string) {
        const s = EntityStore.fromQuery(new EntityQuery('Party'));
        if (id) {
            return s.update(id, obj);
        } else {
            return s.insert(obj);
        }
    }

    addDefaultGroup(entity: any) {
        const group = {
            name: 'Grup',
            cif: entity.cif,
            typeId: 0,
            countyId: entity.countyId,
            localityId: entity.localityId,
            address: entity.address,
            isActiv: true,
            isSubcontractor: false,
        };
        EntityStore.fromQuery(new EntityQuery('Party')).insert(group).then(result => {
            EntityStore.fromQuery(new EntityQuery('Party')).update(entity.id, { parentId: result.id });
        });
    }

    removeParty(id: string) {
        return EntityStore.fromQuery(new EntityQuery('Party')).remove(id);
    }

    checkForTheme() {
        // currentTheme('material.light');
        let isCompact, theme;
        theme = localStorage.getItem('localTheme');
        const windowScreen = window.screen.width + 'x' + window.screen.height;
        if (!theme) {
            if (window.screen.width >= 1024) {
                theme = 'material.light';
            } else {
                theme = 'generic.light.compact';
            }
        }
        isCompact = theme === 'generic.light.compact' ? 1 : 0;
        document.documentElement.style.setProperty('--isCompact', isCompact);
        localStorage.setItem('localTheme', theme);
        themes.current(theme);
    }


    // getReportExtType() {
    //     return [{ type: 1, value: 'Fișă de gestiune deșeuri' },
    //     { type: 2, value: 'Fișă uleiuri' },
    //     { type: 3, value: 'Fișă ambalaje' },
    //     { type: 4, value: 'Situație deșeuri' }];
    // }

    boolean(type?): Array<{ id, name }> {
        return type === 'bool' ? [{ id: false, name: 'NU' }, { id: true, name: 'DA' }] : [{ id: 0, name: 'NU' }, { id: 1, name: 'DA' }];
    }

    getActivityType() {
        return [{ id: 'Colectare', name: 'Colectare' },
        { id: 'Transport', name: 'Transport' },
        { id: 'Valorificare', name: 'Valorificare' },
        { id: 'Eliminare', name: 'Eliminare' },
        { id: 'Stocare temporara', name: 'Stocare temporara' }];
    }

    getReportType() {
        return [{ id: 0, value: 'Fisa de gestiune deseuri' },
        { id: 1, value: 'Fisa uleiuri' },
        { id: 2, value: 'Situatie deseuri' },
        { id: 3, value: 'Rapoarte interne' }];
    }

    getPermitsType() {
        return [{ id: 0, value: 'Aviz' },
        { id: 1, value: 'Acord' },
        { id: 2, value: 'Autorizatie de mediu' },
        { id: 3, value: 'Autorizatie integrata de mediu' }];
    }

    getMonth() {
        return [{ id: 1, name: 'Ianuarie' }, { id: 2, name: 'Februarie' }, { id: 3, name: 'Martie' },
        { id: 4, name: 'Aprilie' }, { id: 5, name: 'Mai' }, { id: 6, name: 'Iunie' },
        { id: 7, name: 'Iulie' }, { id: 8, name: 'August' }, { id: 9, name: 'Septembrie' },
        { id: 10, name: 'Octombrie' }, { id: 11, name: 'Noiembrie' }, { id: 12, name: 'Decembrie' }];
    }

    getSemester() {
        return [{ id: 1, name: 'Semestrul 1' }, { id: 2, name: 'Semestrul 2' }];
    }

    changeEditButtons(e) {
        if (e.rowType === 'data' && e.column.command === 'edit') {
            e.column.fixed = true;
            e.column.fixedPosition = 'right';
            if (e.cellElement.children[0]) {
                e.cellElement.children[0].text = '';
                e.cellElement.children[0].classList.add('dx-icon-edit');
                e.cellElement.children[0].classList.add('dx-success');
                e.cellElement.children[0].classList.add('custom');
            }
            if (e.cellElement.children[1]) {
                e.cellElement.children[1].text = '';
                e.cellElement.children[1].classList.add('dx-icon-trash');
                e.cellElement.children[1].classList.add('dx-danger');
                e.cellElement.children[1].classList.add('custom');
            }
        }
    }

    getMenuItems() {
        return [{
            text: 'Analize',
            icon: 'assets/img/analize.svg',
            infoPath: '/analize',
            items: [{
                text: 'Panou de control',
                path: '/analize/dashboard'
            }, {
                text: 'Taxe',
                path: '/analize/taxe'
            }, {
                text: 'Costuri',
                path: '/analize/costuri'
            }, {
                text: 'Venituri',
                path: '/analize/venituri'
            }, {
                text: 'Controale',
                path: '/analize/controale'
            }, {
                text: 'Valorificare',
                path: '/analize/valorificare'
            }]
        },
        {
            text: 'Deșeuri',
            icon: 'assets/img/deseuri.svg',
            infoPath: '/waste',
            items: [{
                text: 'Generare',
                path: '/waste/generate'
            }, {
                text: 'Transport',
                path: '/waste/transport'
            }, {
                text: 'Transfer',
                path: '/waste/transfer'
            }, {
                text: 'Eliminare',
                path: '/waste/eliminare'
            }, {
                text: 'Valorificare',
                path: '/waste/valorificare'
            }, {
                text: 'Situații',
                path: '/waste/monthly'
            }, {
                text: 'Controale',
                path: '/waste/controls'
            }]
        }, {
            text: 'Documente',
            icon: 'assets/img/documente.svg',
            infoPath: '/document',
            items: [{
                text: 'Arhivă',
                path: '/document/archive'
            }, {
                text: 'Contracte',
                path: '/document/contract'
            }, {
                text: 'Acte reglementare',
                path: '/document/permit'
            }, {
                text: 'Fișe caracterizare',
                path: '/document/wastefile'
            }, {
                text: 'Facturi',
                path: '/document/invoice'
            }, {
                text: 'Taxe',
                path: '/document/tax'
            }]
        }, {
            text: 'Rapoarte',
            icon: 'assets/img/rapoarte.svg',
            infoPath: '/report',
            items: [{
                text: 'A.N.P.M. - Generice',
                path: '/report/reportExt/generic'
            }, {
                text: 'A.N.P.M. - Electronice',
                path: '/report/reportExt/electrics'
            }, {
                text: 'A.N.P.M. - Baterii',
                path: '/report/reportExt/batery'
            }, {
                //     text: 'A.N.P.M. - Extractive',
                //     path: '/report/reportExt/extractive'
                // },{
                text: 'A.N.P.M. - Vehicule',
                path: '/report/reportExt/vehicles'
            }, {
                //     text: 'A.N.P.M. - PCB&PCT',
                //     path: '/report/reportExt/pcbpct'
                // },{
                text: 'A.N.P.M. - Uleiuri',
                path: '/report/reportExt/oils'
                // }, {
                //     text: 'A.N.P.M. - Namoluri',
                //     path: '/report/reportExt/mud'
                // }, {
                // text: 'Interne',
                // path: '/report/reportInt'
            }]
        }, {
            text: 'Administrare',
            icon: 'assets/img/administrare.svg',
            infoPath: '/admin',
            items: [
                {
                    text: 'Tipuri deșeuri',
                    path: '/admin/wastetype'
                }, {
                    text: 'Nomenclatoare',
                    path: '/admin/nomenclatures'
                }, {
                    text: 'Localități',
                    path: '/admin/geographicareas'
                }, {
                    text: 'Organizație',
                    path: '/admin/units'
                }, {
                    text: 'Parteneri',
                    path: '/admin/party'
                }, {
                    text: 'Agenții',
                    path: '/admin/agency'
                }, {
                    text: 'Utilizatori',
                    path: '/admin/users'
                }]
        }];
    }

    getNavBar() {
        return [
            {
                id: -1,
                route: '',
                text: this.auth.currentCompany ? this.auth.currentCompany.text : '',
                itemId: 'companyName'
                // icon: '/assets/img/logo.svg'
            }, {
                id: 0,
                text: 'Analize',
                route: '/analize/dashboard',
                itemId: 'analize',
                active: true,
                icon: ''
            }, {
                id: 1,
                text: 'Deșeuri',
                route: '/waste/generate',
                itemId: 'waste',
                active: false,
                icon: ''
            }, {
                id: 2,
                text: 'Documente',
                route: '/document/archive',
                itemId: 'document',
                active: false,
                icon: ''
            }, {
                id: 3,
                text: 'Rapoarte',
                route: '/report/reportInt',
                itemId: 'report',
                active: false,
                icon: ''
            }, {
                id: 4,
                text: 'Administrare',
                route: '/admin/agency',
                itemId: 'admin',
                active: false,
                icon: ''
            },
            {
                id: 5,
                text: '',
                route: '',
                itemId: '',
                queryParam: '',
                active: 'false',
                template: 'shortC'
            },
            {
                id: 6,
                text: '',
                route: '',
                itemId: '',
                queryParam: '',
                active: 'false',
                template: 'qhseDoc'
            }, {
                id: 7,
                text: 'Alerte',
                route: '/alert',
                itemId: '',
                queryParam: '',
                active: 'false',
                template: 'notify'
            }, {
                id: 8,
                text: '',
                route: '',
                itemId: '',
                queryParam: '',
                active: 'false',
                template: 'userOpt'
            }];
    }
}
