import { Injectable } from '@angular/core';
import { iBisGridConfigService } from '@dohu/ibis-common';

@Injectable()
export class UIService {
    currentGridInst: any;
    tabsWidth: any;
    rowAlternationEnabled: boolean;
    showColumnLines: boolean;
    showRowLines: boolean;
    columnHidingEnabled: boolean;
    fullHeight: any;
    gridTabsHeight: any;
    scrollHeight: any;
    minWidthPopup: any;

    constructor(public gridService: iBisGridConfigService) {
        this.showRowLines = true;
        this.showColumnLines = false;
        this.rowAlternationEnabled = false;
        this.columnHidingEnabled = false;
        this.fullHeight = 'calc(calc(100vh - 142px) * var(--isCompact) + calc(100vh - 158px) * (1 - var(--isCompact)))';
        this.gridTabsHeight = 'calc(calc(100vh - 194px) * var(--isCompact) + calc(100vh - 226px) * (1 - var(--isCompact)))';

        this.tabsWidth = '100vw';
        this.scrollHeight = '100%';
        this.minWidthPopup = '600';
    }

    onContentReady(e: any) {
        e.component.option('height', 'calc(100vh - 150px)');
    }

    onPopupShown(ev: any) {
        const theme = localStorage.getItem('localTheme');
        let minusHeight;
        theme === 'generic.light.compact' ? minusHeight = 20 : minusHeight = 40;
        this.scrollHeight = ev.component.content().clientHeight - minusHeight;
    }

    onPopupHidden(ev: any) {
        this.scrollHeight = '100%';
    }

    prepareToolbar(event: any, customStore: boolean = false) {
        const ss = event.component.option('stateStoring');
        if (customStore) {
            ss.type = 'custom';
            ss.customLoad = () => this.gridService.loadState(event.component, ss.storageKey);
            ss.customSave = (state) => this.gridService.saveState(ss.storageKey, state);
            event.component.option('stateStoring', ss);
        }
        event.toolbarOptions.items.unshift({
            widget: 'dxButton',
            options: { hint: 'Reîncarcă datele', icon: 'assets/img/grid_refresh.svg', onClick: (e: any) => event.component.refresh() },
            location: 'after'
        });
        if (customStore) {
            this.gridService.addButton(event);
        }
    }
}
