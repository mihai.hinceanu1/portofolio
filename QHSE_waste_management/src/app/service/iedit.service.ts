export interface IEditService {

    popupInit(event: any): void;
    showPopup(data: any, byId?: boolean): Promise<any>;
    edit(data: any, byId?: boolean): Promise<any>;
    view(data: any, byId?: boolean): void;

    remove(id: string): Promise<void>;
}
