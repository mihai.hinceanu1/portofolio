import { Injectable } from '@angular/core';
import { EditGenerateService } from '../view/waste/generate/editGenerate.service';
import { EditUnitWasteService } from '../view/admin/unit';
import { EditPartyService } from '../view/admin/party/editParty.service';
import { EditTransportService } from '../view/waste/transport/editTransport.service';
import { EditTransferService } from '../view/waste/transfer/editTransfer.service';

@Injectable()
export class ProxyService {

    constructor(public editGenerate: EditGenerateService, public editParty: EditPartyService,
        public editUnitWaste: EditUnitWasteService, public editTransport: EditTransportService,
        public editTransfer: EditTransferService) {
    }
}
