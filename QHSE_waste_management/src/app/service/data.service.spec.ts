import { TestBed, inject, async } from '@angular/core/testing';
import { DataService } from './data.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import themes from 'devextreme/ui/themes';
import { DevExtremeModule } from 'devextreme-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ServiceService', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [DataService],
            imports: [
                DevExtremeModule,
                RouterTestingModule,
                HttpClientTestingModule
            ]
        });
    }));

    it('should be created', async(() => {
        expect(true).toBe(true);
    }));
});