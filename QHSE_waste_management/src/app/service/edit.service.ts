import { confirm } from 'devextreme/ui/dialog';
import notify from 'devextreme/ui/notify';
import { IEditService } from './iedit.service';
import validationEngine from 'devextreme/ui/validation_engine';

export abstract class EditService implements IEditService {

    public model: any;
    public validation: string;
    public customValidation: string;
    public onSaveNew: (e: any) => void;
    public onSave: (e: any) => void;
    protected popup: any;
    protected success: (d: any) => void;
    protected error: (d?: any) => void;

    constructor() {
        this.model = this.createDefault();
        this.onSaveNew = (e: any) => this.save(e, 1);
        this.onSave = (e: any) => this.save(e, 0);
    }

    public popupInit(event: any) {
        this.popup = event.component;
        this.popup.on('hidden', () => {
            if (this.customValidation) {
                validationEngine.resetGroup(this.customValidation);
            }
            if (this.success) {
                this.success(this.model);
            }
        });
    }

    public showPopup(data: any, byId?: boolean): Promise<any> {
        const result = new Promise<any>((resolve, reject) => { this.success = resolve; this.error = reject; });
        if (byId) {
            this.getById(data).then((entity) => {
                this.model = entity;
                this.popup.show();
            }, (e) => this.error(e));
        } else {
            this.model = data || this.createDefault();
            this.popup.show();
        }
        return result;
    }

    public edit(data: any, byId?: boolean): Promise<any> {
        const result = new Promise<any>((resolve, reject) => { this.success = resolve; this.error = reject; });
        if (byId) {
            this.getById(data).then((entity) => this.model = entity, (e) => this.error(e));
        } else {
            this.model = data || this.createDefault();
        }
        return result;
    }

    public view(data: any, byId?: boolean): void {
        if (byId) {
            this.getById(data).then((entity) => this.model = entity, (e) => this.model = this.createDefault());
        } else {
            this.model = data || this.createDefault();
        }
    }

    public remove(id: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            confirm('Ești sigur că vrei să ștergi această înregistrare ?', 'Confirmare').
                then((val: boolean) => {
                    if (val) {
                        this.onRemove(id).then(() => resolve(), () => reject);
                    } else {
                        reject();
                    }
                });
        });
    }

    private save(event: any, type: number): void {
        let cValidation;
        if (this.customValidation) {
            try{
                cValidation = validationEngine.validateGroup(this.customValidation).isValid;
            }
            catch(e){
                console.log(e);
                cValidation = true;
            }
        } else {
            cValidation = true;
        }
        const validators = event.validationGroup.validators.filter((x: any) => x._isHidden === false);
        const validation = event.validationGroup;
        validation.validators = validators;
        if (!validation.validate().isValid || !cValidation) {
            notify('Trebuie să completați toate câmpurile obligatorii.', 'error', 3000);
            return;
        }

        this.onSaveEv().then(() => {
            notify('Succes!', 'success', 2000);
            if (type === 0) {
                if (this.popup) {
                    this.popup.hide();
                } else {
                    this.success(this.model);
                }
            } else {
                this.reset();
            }
        }, (e: any) => {
            this.error(e);
        });
    }

    abstract createDefault(): any;
    abstract getById(id: string): Promise<any>;
    abstract onRemove(id: string): Promise<void>;
    abstract onSaveEv(): Promise<any>;
    abstract reset(): void;
}
