import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './view/login/login.component';
import { GenerateComponent } from './view/waste/generate/generate.component';
import { EliminateComponent } from './view/waste/eliminate/eliminate.component';
import { TransportComponent } from './view/waste/transport/transport.component';
import { TaxComponent } from './view/document/tax/tax.component';
import { ControlComponent } from './view/waste/control/control.component';
import { AlertComponent } from './view/alert/alert.component';
import { ReportExtComponent } from './view/report/reportExt.component';
import { ReportIntComponent } from './view/report/reportInt.component';
import { AgencyComponent } from './view/admin/agency/agency.component';
import { PartyComponent } from './view/admin/party/party.component';
import { WastetypeComponent } from './view/admin/wastetype/wastetype.component';
import { UnitComponent } from './view/admin/unit/unit.component';
import { CapitalizationComponent } from './view/waste/capitalization/capitalization.component';
import { iBisAuthService, iBisConfirmInviteComponent, iBisResetPasswordComponent } from '@dohu/ibis-auth';
import { MonthlyComponent } from './view/waste/monthly/monthly.component';
import { UserComponent } from './view/admin/user/user.component';
import { GeographicareaComponent } from './view/admin/geographicarea/geographicarea.component';
import { ArchiveComponent } from './view/document/archive/archive.component';
import { ContractComponent } from './view/document/contract/contract.component';
import { PermitComponent } from './view/document/permit/permit.component';
import { InvoiceComponent } from './view/document/invoice/invoice.component';
import { WastefileComponent } from './view/document/wastefile/wastefile.component';
import { NomenclatureComponent } from './view/admin/nomenclature/nomenclature.component';
import { RegisterComponent } from './view/register/register.component';

import { AccountComponent } from './view/account/account.component';
import { TransferComponent } from './view/waste/transfer/transfer.component';
import { DashboardComponent } from './view/analize/dashboard/dashboard.component';
import { TaxAnalyzesComponent } from './view/analize/tax-analyzes/tax-analyzes.component';
import { ControlAnalyzesComponent } from './view/analize/control-analyzes/control-analyzes.component';
import { IncomeAnalyzesComponent } from './view/analize/income-analyzes/income-analyzes.component';
import { CostAnalyzesComponent } from './view/analize/cost-analyzes/cost-analyzes.component';
import { CapitalizationAnalyzesComponent } from './view/analize/capitalization-analyzes/capitalization-analyzes.component';
import { AnalizeComponent } from './view/analize/analize.component';

export const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {
        path: 'analize', canActivate: [iBisAuthService], component: AnalizeComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'taxe', component: TaxAnalyzesComponent },
            { path: 'controale', component: ControlAnalyzesComponent },
            { path: 'venituri', component: IncomeAnalyzesComponent },
            { path: 'costuri', component: CostAnalyzesComponent },
            { path: 'valorificare', component: CapitalizationAnalyzesComponent }
        ]
    },
    {
        path: 'waste', canActivate: [iBisAuthService],
        children: [
            { path: 'generate', component: GenerateComponent },
            { path: 'transport', component: TransportComponent },
            { path: 'transfer', component: TransferComponent },
            { path: 'eliminare', component: EliminateComponent },
            { path: 'valorificare', component: CapitalizationComponent },
            { path: 'monthly', component: MonthlyComponent },
            { path: 'controls', component: ControlComponent }
        ]
    },
    {
        path: 'document', canActivate: [iBisAuthService],
        children: [
            { path: 'archive', component: ArchiveComponent },
            { path: 'contract', component: ContractComponent },
            { path: 'permit', component: PermitComponent },
            { path: 'wastefile', component: WastefileComponent },
            { path: 'invoice', component: InvoiceComponent },
            { path: 'tax', component: TaxComponent }
        ]
    },
    {
        path: 'report', canActivate: [iBisAuthService],
        children: [
            {
                path: 'reportExt',
                children: [{
                    path: 'generic', component: ReportExtComponent
                }, {
                    path: 'electrics', component: ReportExtComponent
                }, {
                    path: 'batery', component: ReportExtComponent
                }, {
                    //     path: 'extractive', component: ReportExtComponent
                    // }, {
                    path: 'vehicles', component: ReportExtComponent
                }, {
                    //     path: 'pcbpct', component: ReportExtComponent
                    // }, {
                    path: 'oils', component: ReportExtComponent
                    //}, {
                    // path: 'mud', component: ReportExtComponent
                }]
            },
            { path: 'reportInt', component: ReportIntComponent }
        ]
    },
    {
        path: 'admin', canActivate: [iBisAuthService],
        children: [
            { path: 'agency', component: AgencyComponent },
            { path: 'nomenclatures', component: NomenclatureComponent },
            { path: 'party', component: PartyComponent },
            { path: 'wastetype', component: WastetypeComponent },
            { path: 'units', component: UnitComponent },
            { path: 'users', component: UserComponent },
            { path: 'geographicareas', component: GeographicareaComponent }

        ]
    },
    { path: 'profile', component: AccountComponent },
    { path: 'alert', component: AlertComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    {
        path: 'confirmInvite',
        component: iBisConfirmInviteComponent,
        data: [{ returnUrl: '/login' }]
    },
    {
        path: 'resetPassword',
        component: iBisResetPasswordComponent,
        data: [{ title: 'Resetează parola', isReset: true, returnUrl: '/login' }]
    }
];
