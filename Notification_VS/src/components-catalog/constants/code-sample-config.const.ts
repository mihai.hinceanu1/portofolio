/** The props for a custom code editor for code chunks demos in components catalog test page */
export const codeSampleConfig = {
    showGutter: false,
    width: 350,
    height: 300,
    mode: 'javascript',
    editable: false,
    theme: 'iplastic',
    highlightActiveLine: false,
};