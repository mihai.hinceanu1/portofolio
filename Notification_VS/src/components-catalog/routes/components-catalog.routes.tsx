import { LoginResponse } from '../../auth/interfaces/login';
import { IconPickerPage } from '../../icons/components/icon-picker/docs/icon-picker.page';
import { IconPage } from '../../icons/components/icon/docs/icon.page';
import { LessonCardDetailsPage } from '../../lessons-catalog/components/lesson-card-details/_docs/lesson-card-details.page';
import { LessonCardPage } from '../../lessons-catalog/components/lesson-card/_docs/lesson-card.page';
import { BranchesMenuPage } from '../../shared/components/branches-menu/_docs/branches-menu.page';
import { CheckboxPage } from '../../shared/components/checkbox/_doc/checkbox.page';
import { DatePickerPage } from '../../shared/components/date-picker/_doc/date-picker.page';
import { DotsMenuPage } from '../../shared/components/dots-menu/_docs/dots-menu.page';
import { DropdownPage } from '../../shared/components/dropdown/_docs/dropdown.page';
import { IndicatorsCardPage } from '../../shared/components/indicators-card/_docs/indicators-card.page';
import { InputPage } from '../../shared/components/input/_docs/input.page';
import { LoadingAnimationPage } from '../../shared/components/loading-animation/_docs/loading-animation.page';
import { ModalPage } from '../../shared/components/modal/_doc/modal.page';
import { PageHeaderPage } from '../../shared/components/page-header/_docs/page-header.page';
import { RadioButtonsPage } from '../../shared/components/radio-button/_docs/radio-buttons.page';
import { SearchBarPage } from '../../shared/components/search-bar/_docs/search-bar.page';
import { SliderPage } from '../../shared/components/slider/_docs/slider.page';
import { SwitchButtonPage } from '../../shared/components/switch-button/_docs/switch-button.page';
import { TabsPage } from '../../shared/components/tabs/_docs/tabs.page';
import { TextareaPage } from '../../shared/components/textarea/_docs/textarea.page';
import { ToastrPage } from '../../shared/components/toastr/_docs/toastr.page';
import { VscButtonPage } from '../../shared/components/vsc-button/docs/button.page';
import { CodePackagePage } from '../../source-code/components/code-package/_docs/code-pacakge.page';
import { FloatingMenuPage } from '../../source-code/components/floating-menu/_docs/floating-menu.page';
import { FramedImagePage } from '../../source-code/components/framed-image/_docs/framed-image.page';
import { StageCardPage } from '../../source-code/components/stage-card/_docs/stage-card.page';
import { StatsBarPage } from '../../source-code/components/stats-bar/_docs/stats-bar.page';
import { TimelineBarPage } from '../../source-code/components/timeline-bar/_docs/timeline-bar.page';
import { VersionNavbarPage } from '../../source-code/components/version-navbar/_docs/version-nav-bar.page';
import { VersionPreviewImagePage } from '../../source-code/components/version-preview-image/_docs/version-preview-image.page';
import { VersionTitlePage } from '../../source-code/components/version-title/_docs/version-title.page';
import { TaxonomyTreePage } from '../../taxonomy/components/taxonomy-tree/docs/taxonomy-tree.page';
import { ComponentsCatalogHomePage } from '../pages/components-catalog-home-page/components-catalog-home.page';
import * as React from 'react';
import { Redirect, Route } from 'react-router';

/** Components Catalog Routes */
export const componentsCatalogRoutes = (_loginResponse: LoginResponse) => {
    return [

        // Home
        <Route exact={true} path='/components-catalog' key='components-catalog'>
            <Redirect to='/components-catalog/home' />
        </Route>,
        <Route exact={true} path='/components-catalog/home' key='components-catalog-home' component={ComponentsCatalogHomePage} />,

        // Icons
        <Route exact={true} path='/components-catalog/icons/icon-picker' key='icon-picker' component={IconPickerPage} />,
        <Route exact={true} path='/components-catalog/icons/icon' key='icon' component={IconPage} />,

        // Shared Components
        <Route exact={true} path='/components-catalog/shared/branches-menu' key='branches-menu' component={BranchesMenuPage} />,
        <Route exact={true} path='/components-catalog/shared/button' key='button' component={VscButtonPage} />,
        <Route exact={true} path='/components-catalog/shared/checkbox' key='checkbox' component={CheckboxPage} />,
        <Route exact={true} path='/components-catalog/shared/date-picker' key='date-picker' component={DatePickerPage} />,
        <Route exact={true} path='/components-catalog/shared/dots-menu' key='dots-menu' component={DotsMenuPage} />,
        <Route exact={true} path='/components-catalog/shared/dropdown' key='dropdown' component={DropdownPage} />,
        <Route exact={true} path='/components-catalog/shared/indicators-card' key='indicators-card' component={IndicatorsCardPage} />,
        <Route exact={true} path='/components-catalog/shared/input' key='input' component={InputPage} />,
        <Route exact={true} path='/components-catalog/shared/loader' key='loading-animation' component={LoadingAnimationPage} />,
        <Route exact={true} path='/components-catalog/shared/modal' key='modal' component={ModalPage} />,
        <Route exact={true} path='/components-catalog/shared/radio-button' key='radio-button' component={RadioButtonsPage} />,
        <Route exact={true} path='/components-catalog/shared/search-bar' key='search-bar' component={SearchBarPage} />,
        <Route exact={true} path='/components-catalog/shared/slider' key='slider' component={SliderPage} />,
        <Route exact={true} path='/components-catalog/shared/switch-button' key='switch-button' component={SwitchButtonPage} />,
        <Route exact={true} path='/components-catalog/shared/tabs' key='tabs' component={TabsPage} />,
        <Route exact={true} path='/components-catalog/shared/textarea' key='textarea' component={TextareaPage} />,
        <Route exact={true} path='/components-catalog/shared/page-header' key='catalog-header' component={PageHeaderPage} />,
        <Route exact={true} path='/components-catalog/shared/framed-image' key='catalog-header' component={FramedImagePage} />,
        <Route exact={true} path='/components-catalog/shared/toastr' key='toastr' component={ToastrPage} />,

        // Topics Components

        // Lesson Catalog Components
        <Route exact={true} path='/components-catalog/lesson-catalog/card' key='lesson-card' component={LessonCardPage} />,
        <Route exact={true} path='/components-catalog/lesson-catalog/card-details' key='lesson-card-details' component={LessonCardDetailsPage} />,

        // Taxonomy Components
        <Route exact={true} path='/components-catalog/taxonomy/tree' key='taxonomy-tree' component={TaxonomyTreePage} />,

        // Source Code Components
        <Route exact={true} path='/components-catalog/source-code/version-navbar' key='version-navbar' component={VersionNavbarPage} />,
        <Route exact={true} path='/components-catalog/source-code/version-preview-image' key='version-preview-image' component={VersionPreviewImagePage} />,
        <Route exact={true} path='/components-catalog/source-code/stage-card' key='stage-card' component={StageCardPage} />,
        <Route exact={true} path='/components-catalog/source-code/stats-bar' key='stats-bar' component={StatsBarPage} />,
        <Route exact={true} path='/components-catalog/source-code/version-title' key='version-title' component={VersionTitlePage} />,
        <Route exact={true} path='/components-catalog/source-code/timeline-bar' key='timeline-bar' component={TimelineBarPage} />,
        <Route exact={true} path='/components-catalog/source-code/floating-menu' key='floating-menu' component={FloatingMenuPage} />,
        <Route exact={true} path='/components-catalog/source-code/code-package' key='code-package' component={CodePackagePage} />
    ];
};