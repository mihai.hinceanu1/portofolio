import * as scroll from '../../../shared/style/scroll.style';
import { ComponentsCatalogHeader } from '../../components/components-catalog-header/components-catalog-header';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';

export interface ScrollableDocPageProps { }

export interface ScrollableDocPageState<TState> {
    width: number;

    /**
     * Custom object that is specific to each Doc Page State
     * Extends current state with given object. Handling state in "child" component is one level deeper that usual
     */
    overrides: TState;
}

/**
 * All documentation pages use a common header and access to the width of the page.
 * This base class abstracts away this common behavior.
 */
export abstract class ScrollableDocPage<TState> extends React.Component<ScrollableDocPageProps, ScrollableDocPageState<TState>> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            overrides: null,
        };
    }

    public render() {
        let { width } = this.state;

        return (
            <scroll.ScrollView data-cy='modal-page'
                contentContainerStyle={scroll.inner.content}
                onLayout={e => this.updateWidth(e)}>

                {/* Header */}
                <ComponentsCatalogHeader />

                {/* Component Documentation */}
                <scroll.Center data-cy='center' width={width}>
                    {this.renderDemoPage()}
                </scroll.Center>

            </scroll.ScrollView>
        );
    }

    /** Provide the template of the extended class to be rendered inside a scroll view. */
    abstract renderDemoPage(): JSX.Element;

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

}