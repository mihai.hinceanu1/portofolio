import * as div from './components-catalog-home.page.style';
import * as scroll from '../../../shared/style/scroll.style';
import { ComponentsCatalogHeader } from '../../components/components-catalog-header/components-catalog-header';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps { }

interface State {
    width: number;
}

class _ComponentsCatalogHomePage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        let { width } = this.state;

        return (
            <scroll.ScrollView data-cy='components-catalog-home-page'
                contentContainerStyle={scroll.inner.content}
                onLayout={e => this.updateWidth(e)}>

                <ComponentsCatalogHeader />

                <scroll.Center data-cy='center' width={width}>

                    {/* Header */}
                    <div.Header>
                        <div.HeaderTitle>
                            Components Catalog
                        </div.HeaderTitle>

                        <div.HeaderDescription>
                            Components catalog was created in order to describe every component that takes
                            part in our app, and also a secondary motivation to create this page was the fact
                            that we need to create strong tests for each component to increase our safeness
                        </div.HeaderDescription>
                    </div.Header>

                    {/* Motivation */}
                    <div.Section>
                        <div.Title>
                            Why Shared ?
                        </div.Title>

                        <div.Description>
                            We use shared components for a better code maintainability and also we don't want
                            to repeat the code. For example we won't create a button each time we need one, the shared
                            button will be used instead of creating one from scratch.
                        </div.Description>
                    </div.Section>

                    {/* Cypress */}
                    <div.Section>
                        <div.Title>
                            Cypress Tests
                        </div.Title>

                        <div.Description>
                            One of the reasons , beside the presentation reason , is to use this page in order to
                            create strong Cypress tests which will detect any weakness of our components. We will
                            have full coverage on our unit tests for every component.
                        </div.Description>
                    </div.Section>

                    {/* How To Cypress */}
                    <div.Section>
                        <div.Title>
                            How to use Cypress ?
                        </div.Title>

                        <div.Description>
                            First step is to get your playground ready for action so just type this
                            in your console : "npm install cypress --save-dev", now you can open your
                            Cypress interface with the CLI command : "./node_modules/.bin/cypress open"
                        </div.Description>
                    </div.Section>

                    {/* Writing Tests */}
                    <div.Section>
                        <div.Title>
                            Writing Tests
                        </div.Title>

                        <div.Description>
                            To start with the first test you need to checkout the Cypress documentation
                            which you can find on google. First step for writing a good test is to
                            appropriate the testing theory and skills very well.
                        </div.Description>
                    </div.Section>

                </scroll.Center>
            </scroll.ScrollView>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }
}

export const ComponentsCatalogHomePage = withRouter<Props, typeof _ComponentsCatalogHomePage>(_ComponentsCatalogHomePage);
