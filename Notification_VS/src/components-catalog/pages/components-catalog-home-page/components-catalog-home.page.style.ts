import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Header = styled.View`
    margin-top: 30px;
    width: 100%;
    justify-content: flex-start;
    margin-bottom: 15px;
`;

export const HeaderTitle = styled.Text`
    font-size: ${font.$size40}px;
    font-weight: 700;
`;

export const HeaderDescription = styled.Text`
    font-size: ${font.$size18}px;
    font-weight: 400;
    margin-bottom: 20px;
`;

export const Section = styled.View`
    width: 100%;
    justify-content: center;
    margin-top: 30px;
`;

export const Title = styled.Text`
    font-size: ${font.$size22}px;
    font-weight: 600;
    color: ${colors.$blue};
`;

export const Description = styled.Text`
    font-size:  ${font.$size16}px;
    font-weight: 400;
    margin-bottom: 30px;
`;