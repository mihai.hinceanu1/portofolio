import { PAGE_WIDTH } from '../../shared/constants/adaptive.const';
import { getBoxPaddings, getPagePaddings } from '../../shared/style/adaptive.style';
import { colors } from '../../shared/style/colors';
import { font } from '../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface CenterProps {
    width: number;
}

interface DemoProps {
    width: number;
    /**
     * Some components might be pretty long on X axis
     * If wide property is provided (not mandatory), put CodeEditor at the bottom of Demo Section
     */
    wide?: boolean;
}

// ====== OVERVIEW ======

export const Overview = styled.View<CenterProps>`
    margin-top: 30px;
    margin-bottom: 15px;
    padding: 0 ${props => getPagePaddings(props)}px;
`;

export const OverviewTitle = styled.Text`
    font-size: ${font.$size40}px;
    font-weight: 700;
`;

export const OverviewDescription = styled.Text`
    font-size:  ${font.$size18}px;
    font-weight: 400;
    margin-bottom: 20px;
`;

// ====== DEMO AREA ======

export const Demo = styled.View<DemoProps>`
    flex-direction: ${props => getDemoLayout(props)};
    margin: 0 ${props => getPagePaddings(props)}px 60px ${props => getPagePaddings(props)}px;
    padding: 20px ${props => getBoxPaddings(props)}px;
    background: white;
    min-height: 100px;
    min-width: 380px;
    justify-content: space-between;
    flex-wrap: wrap;
`;

export const DemoArea = styled.View`
    align-self: flex-start;
    min-height: 200px;
    margin-bottom: 10px;
`;

export const DemoTitle = styled.Text`
    font-size: ${font.$size18}px;
    font-weight: 600;
    color: ${colors.$blue};
    margin: 20px 0px;
    align-self: flex-start;
`;

export const CallbackResult = styled.Text`
    margin-bottom: 10px;
    font-size: ${font.$size14}px;
`;

export const DemoDescription = styled.Text`
    font-size: ${font.$size16}px;
    margin-bottom: 10px;
    max-width: ${800 - 350 - 60}px;
`;

// ====== UTILS ======

const getDemoLayout = (props: DemoProps): string => {
    if (props.wide) {
        return 'column';
    }

    return props.width >= PAGE_WIDTH ? 'row' : 'column';
};
