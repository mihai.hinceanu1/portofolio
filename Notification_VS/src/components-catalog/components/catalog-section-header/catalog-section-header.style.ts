import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const PageHeader = styled.View`
    width: 100%;
    justify-content: center;
    margin-top: 30px;
    padding: 0 60px;
`;

export const PageTitle = styled.Text`
    font-size: ${font.$size40}px;
    font-weight: 700;
    color: ${colors.$blue};
`;

export const PageDescription = styled.Text`
    font-size:  ${font.$size18}px;
    font-weight: 400;
    margin-bottom: 30px;
`;