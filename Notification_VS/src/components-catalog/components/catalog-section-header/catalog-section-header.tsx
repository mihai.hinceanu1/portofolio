import * as div from './catalog-section-header.style';
import * as React from 'react';

/** All pages of a section catalog have a common header. */
export const CatalogSectionHeader = () =>
    <div.PageHeader>

        <div.PageTitle>
            Shared Components
        </div.PageTitle>
        <div.PageDescription>
            General purpose reusable components.
        </div.PageDescription>

    </div.PageHeader>;