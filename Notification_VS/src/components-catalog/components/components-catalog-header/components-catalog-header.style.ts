import { PAGE_WIDTH } from '../../../shared/constants/adaptive.const';
import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { getPagePaddings } from '../../../shared/style/adaptive.style';
import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

interface CenterProps {
    width: number;
}

export const ComponentsCatalogHeader = styled.View`
    height: 72px;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: ${colors.$blue};
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);
    align-items: center;
    z-index: 1;
    elevation: 1;
    overflow: visible;
`;

export const Center = styled.View<CenterProps>`
    display: flex;
    padding: 14px ${props => getPagePaddings(props)}px;
    flex-direction: row;
    width: 100%;
    max-width: ${PAGE_WIDTH}px;
`;

export const Logo = styled.View<ReactWebAttributes>``;

export const Section = styled.View`
    display: flex;
    flex-direction: row;
    z-index: 10;
`;