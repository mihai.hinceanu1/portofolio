import * as div from './components-catalog-header.style';
import * as configs from './components-catalog-header.utils';
import { Dropdown } from '../../../shared/components/dropdown/dropdown';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps<never> { }

interface State {
    width: number;
}

/**
 * Shared header between all tests pages.
 * TODO Header padding seems to jump sooner to MOBILE width.
 */
class _ComponentsCatalogHeader extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { width } = this.state;
        const { history } = this.props;

        return (
            <div.ComponentsCatalogHeader data-cy='components-catalog-header'
                onLayout={e => this.updateWidth(e)}>
                <div.Center data-cy='center' width={width}>

                    {/* Logo */}
                    <div.Logo data-cy='logo'>
                        {/* <Icon width={100}
                            height={50} icon='/header/logo-white.png'
                            onPress={() => this.goToCatalogHome()} /> */}
                    </div.Logo>

                    {/* Catalog Sections */}
                    <div.Section data-cy='section'>
                        {/** Shared */}
                        <Dropdown config={configs.getSharedDropdownConfig(history)} />

                        {/** Topics */}
                        <Dropdown config={configs.getTopicsDropdownConfig(history)} />

                        {/** Lesson Catalog */}
                        <Dropdown config={configs.getLessonCatalogDropdownConfig(history)} />

                        {/** Source Code */}
                        <Dropdown config={configs.getSourceCodeCatalogDropdownConfig(history)} />

                        {/** Taxonomy */}
                        <Dropdown config={configs.getTaxonomyCatalogDropdownConfig(history)} />

                        {/** Icons */}
                        <Dropdown config={configs.getIconsCatalogDropdownConfig(history)} />
                    </div.Section>

                </div.Center>
            </div.ComponentsCatalogHeader>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        const { layout } = event.nativeEvent;

        this.setState({
            width: layout.width,
        });
    }

    // private goToCatalogHome() {
    //     const { history } = this.props;

    //     history.push('/components-catalog/home');
    // }
}

export const ComponentsCatalogHeader = withRouter(_ComponentsCatalogHeader);