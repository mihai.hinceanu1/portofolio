import { DROPDOWN_TYPE } from '../../../shared/constants/dropdown.const';
import { DropdownConfig, IDropdownOption } from '../../../shared/interfaces/dropdown';
import { History } from 'history';

/** Links to shared components doc pages */
export const getSharedDropdownConfig = (history: History<{}>): DropdownConfig => ({
    options: [
        {
            _id: 'branches-menu',
            label: 'Branches Menu'
        },
        {
            _id: 'button',
            label: 'Button'
        },
        {
            _id: 'checkbox',
            label: 'Checkbox'
        },
        {
            _id: 'date-picker',
            label: 'Date Picker'
        },
        {
            _id: 'dots-menu',
            label: 'Dots Menu'
        },
        {
            _id: 'dropdown',
            label: 'Dropdown'
        },
        {
            _id: 'icon',
            label: 'Icon'
        },
        {
            _id: 'indicators-card',
            label: 'Indicators Card'
        },
        {
            _id: 'input',
            label: 'Input'
        },
        {
            _id: 'loader',
            label: 'Loader'
        },
        {
            _id: 'modal',
            label: 'Modal'
        },
        {
            _id: 'radio-button',
            label: 'Radio Button'
        },
        {
            _id: 'search-bar',
            label: 'Search Bar'
        },
        {
            _id: 'slider',
            label: 'Slider'
        },
        {
            _id: 'switch-button',
            label: 'Switch Button'
        },
        {
            _id: 'tabs',
            label: 'Tabs'
        },
        {
            _id: 'textarea',
            label: 'Textarea'
        },
        {
            _id: 'toastr',
            label: 'Toastr',
        },
        {
            _id: 'page-header',
            label: 'Page Header'
        },
        {
            _id: 'framed-image',
            label: 'Framed Image',
        }
    ],
    placeholder: 'Shared',
    type: DROPDOWN_TYPE.OnlyInput,
    callback: option => goToDocPage(history, option, 'shared'),
});

/** Links to topics components doc pages */
export const getTopicsDropdownConfig = (history: History): DropdownConfig => ({
    options: [],
    placeholder: 'Topics',
    callback: option => goToDocPage(history, option, 'topics'),
});

export const getLessonCatalogDropdownConfig = (history: History): DropdownConfig => ({
    options: [
        {
            _id: 'card',
            label: 'Lesson Card'
        },
        {
            _id: 'card-details',
            label: 'Lesson Card Details'
        },
    ],
    placeholder: 'Lesson Catalog',
    callback: option => goToDocPage(history, option, 'lesson-catalog')
});

export const getSourceCodeCatalogDropdownConfig = (history: History): DropdownConfig => ({
    options: [
        {
            _id: 'stage-card',
            label: 'Stage Card'
        },
        {
            _id: 'stats-bar',
            label: 'Stats Bar'
        },
        {
            _id: 'version-title',
            label: 'Version Title'
        },
        {
            _id: 'version-preview-image',
            label: 'Version Preview Image'
        },
        {
            _id: 'timeline-bar',
            label: 'Timeline Bar'
        },
        {
            _id: 'floating-menu',
            label: 'Floating Menu',
        },
        {
            _id: 'code-package',
            label: 'Code Package',
        }
    ],
    placeholder: 'Source Code',
    callback: option => goToDocPage(history, option, 'source-code')
});

export const getTaxonomyCatalogDropdownConfig = (history: History): DropdownConfig => ({
    options: [
        {
            _id: 'tree',
            label: 'Tree',
        },
    ],
    placeholder: 'Taxonomy',
    callback: option => goToDocPage(history, option, 'taxonomy')
});

export const getIconsCatalogDropdownConfig = (history: History): DropdownConfig => ({
    options: [
        {
            _id: 'icon-picker',
            label: 'Icon Picker',
        },
        {
            _id: 'icon',
            label: 'Icon',
        }
    ],
    placeholder: 'Icons',
    callback: option => goToDocPage(history, option, 'icons'),
});

// ====== PRIVATE ======

let goToDocPage = (history: History<{}>, option: IDropdownOption, section: string) => {
    const pathName = option._id;

    history.push(`/components-catalog/${section}/${pathName}`);
};