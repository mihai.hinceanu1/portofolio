import * as samples from './taxonomy-tree.samples';
import * as utils from './taxonomy-tree.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import * as icons from '../../../../shared/assets/icons';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { TaxonomyLevel } from '../../../interfaces/taxonomy-level';
import { TaxonomyTree } from '../taxonomy-tree';
import * as React from 'react';

interface TaxonomyTreeState {
    currentExpandedLevel: TaxonomyLevel;
    currentCollapsedLevel: TaxonomyLevel;
    currentClickedLevel: TaxonomyLevel;
}

export class TaxonomyTreePage extends ScrollableDocPage<TaxonomyTreeState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                currentExpandedLevel: null,
                currentCollapsedLevel: null,
                currentClickedLevel: null,
            }
        };
    }

    public renderDemoPage() {
        const { width } = this.state;
        const {
            currentExpandedLevel,
            currentCollapsedLevel,
            currentClickedLevel,
        } = this.state.overrides;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Taxonomy Tree
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Used to render relations between levels.
                        Each level might have a parent and children.
                        It's represented as a tree, one node being one level
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Tree
                        </div.DemoTitle>

                        <TaxonomyTree levels={utils.DEFAULT} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** OnExpandLevel */}
                <div.Demo data-cy='on-expand' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            OnExpand Callback
                        </div.DemoTitle>

                        <TaxonomyTree levels={utils.DEFAULT}
                            onLevelExpand={(level: TaxonomyLevel) => this.onLevelActionClick(level, 'currentExpandedLevel')} />

                        <div.CallbackResult data-cy='callback-result'>
                            {currentExpandedLevel && currentExpandedLevel.name}
                        </div.CallbackResult>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ON_EXPAND} />
                </div.Demo>

                {/** On Collapse */}
                <div.Demo data-cy='on-collapse' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            On Collapse Callback
                        </div.DemoTitle>

                        <TaxonomyTree levels={utils.DEFAULT}
                            onLevelCollapse={(level: TaxonomyLevel) => this.onLevelActionClick(level, 'currentCollapsedLevel')} />

                        <div.CallbackResult data-cy='callback-result'>
                            {currentCollapsedLevel && currentCollapsedLevel.name}
                        </div.CallbackResult>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ON_COLLAPSE} />
                </div.Demo>

                {/** On Click */}
                <div.Demo data-cy='on-click' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            On Click Callback
                        </div.DemoTitle>

                        <TaxonomyTree levels={utils.DEFAULT}
                            onLevelClick={(level: TaxonomyLevel) => this.onLevelActionClick(level, 'currentClickedLevel')} />

                        <div.CallbackResult data-cy='callback-result'>
                            {currentClickedLevel && currentClickedLevel.name}
                        </div.CallbackResult>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ON_CLICK} />
                </div.Demo>

                {/** Custom icons */}
                <div.Demo data-cy='custom-icons' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom icons
                        </div.DemoTitle>

                        <TaxonomyTree levels={utils.DEFAULT}
                            collapseIcon={icons.ANGULAR}
                            expandIcon={icons.ANGULAR} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CUSTOM_ICONS} />
                </div.Demo>
            </>
        );
    }

    private onLevelActionClick(level: TaxonomyLevel, key: string) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                [key]: level,
            },
        });
    }
}
