export const DEFAULT = `
    <TaxonomyTree levels={tree} />

    const tree: TaxonomyLevel[] = [{
        _id: '1',
        authorId: '1',
        created: 123456642,
        icon: null,
        isActive: true,
        isRoot: true,
        name: 'Coding',
        parent: null,
        updated: 12346642,
        pathName: 'coding',
        children: [{
            _id: '2',
            authorId: '1',
            created: 123456642,
            icon: null,
            isActive: true,
            isRoot: true,
            name: 'Frontend',
            parent: '1',
            updated: 12346642,
            pathName: 'coding',
            children: [],
        }],
    }];
`;

export const ON_EXPAND = `
    <TaxonomyTree onLevelExpand={(level: TaxonomyLevel) => onLevelExpand(level)} />

    function onLevelExpand(level: TaxonomyLevel) {
        this.setState({ latestLevelExpanded: level.name });
    }
`;

export const ON_COLLAPSE = `
    <TaxonomyTree onLevelCollapse={(level: TaxonomyLevel) => onLevelCollapse(level)} />

    function onLevelCollapse(level: TaxonomyLevel) {
        this.setState({ currentCollapsedLevel: level });
    }
`;

export const ON_CLICK = `
    <TaxonomyTree onLevelClick={(level: TaxonomyLevel) => onLevelClick(level)} />

    function onLevelClick(level: TaxonomyLevel) {
        this.setState({ currentClickedLevel: level });
    }
`;

export const CUSTOM_ICONS = `
    <TaxonomyTree levels={utils.DEFAULT}
        collapseIcon={icons.ANGULAR}
        expandIcon={icons.ANGULAR} />
`;