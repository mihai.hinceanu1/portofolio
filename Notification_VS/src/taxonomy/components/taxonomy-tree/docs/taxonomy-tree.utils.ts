import { TaxonomyLevel } from '../../../interfaces/taxonomy-level';

/**
 * Because of the complexity of the dataset given to this component,
 * the following array of objects is gonna be used in the entire
 * test page. If necessary, some additional informations will be added
 * in the future, but this will be the main array used to demonstrate
 * how TaxonomyTree is working.
 */
export const DEFAULT: TaxonomyLevel[] = [{
    _id: '1',
    authorId: '1',
    created: 123456642,
    icon: null,
    isActive: true,
    isRoot: true,
    name: 'Coding',
    parent: null,
    updated: 12346642,
    pathName: 'coding',
    children: [{
        _id: '2',
        authorId: '1',
        created: 123456642,
        icon: null,
        isActive: true,
        isRoot: true,
        name: 'Frontend',
        parent: '1',
        updated: 12346642,
        pathName: 'coding',
        children: [],
    }],
}];
