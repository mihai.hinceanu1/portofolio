import { SEARCH } from '../../../shared/assets/icons';
import { TIcon } from '../../../shared/interfaces/icon';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { BOX_SHADOW_CSS, getShadow } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

interface TaxonomyTreeProps {
    override: string;
}

export const TaxonomyTree = styled.View<TaxonomyTreeProps>`
    max-width: 230px;
    align-items: center;
    justify-content: flex-start;
    ${props => props.override};
`;

export const Frame = styled.View`
    width: 100%;
    background-color: ${colors.$white};
    padding-bottom: 50px;
    ${getShadow(BOX_SHADOW_CSS)};
`;

export const Title = styled.Text`
    /* width: 85px; */
    text-align: center;
    padding: 10px 15px;
    font-size: ${font.$size12}px;
    background-color: ${colors.$white};
    align-self: flex-start;
    z-index: 100;
    /* ${getShadow(BOX_SHADOW_CSS)}; */
`;

// ===== OVERRIDES =====

export const Input = {
    root: `
        width: 100%;
        margin-top: 10px;
    `,
    inputField: `
        border-radius: 20px;
        height: 30px;
        font-size: ${font.$size12}px;
        font-weight: 600;
    `,
};

// ===== ICONS =====

export const SearchIcon: TIcon = {
    type: 'svg',
    svgPath: SEARCH,
    fill: colors.$darkGrey,
};
