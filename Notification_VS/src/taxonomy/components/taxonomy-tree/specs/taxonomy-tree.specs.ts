/// <reference types='cypress' />

describe('Taxonomy Tree Tests', () => {
    it('Loads the page', () => {
        cy.visit('http://localhost:3000/components-catalog/taxonomy/tree');
    });

    it('Renders default tree', () => {
        getEl('default', 'taxonomy-tree').should('exist');
    });

    it('Renders default elements (title, searchbar, parent tree-node', () => {
        getEl('default', 'title').should('exist');
        getEl('default', 'input-box').should('exist');
        getEl('default', 'taxonomy-tree-node').should('exist');
    });

    it('Initially renders only root levels', () => {
        getEl('default', 'taxonomy-tree-node').should('have.length', 1);
    });

    it('When clicking root level, it renders children', () => {
        getEl('default', 'taxonomy-tree-node').click();

        getEl('default', 'taxonomy-tree-node').should('have.length', 2);
    });

    it('After clicking root level, child level has expands icons and root has collapse icons', () => {
        getEl('default', 'taxonomy-tree-node').eq(0)
            .within(() => {
                cy.get('[data-cy=collapse-icons]').should('exist');
            });

        getEl('default', 'taxonomy-tree-node').eq(1)
            .within(() => {
                cy.get('[data-cy=expand-icons]').should('exist');
            });
    });

    it('Execute attached on expand callback', () => {
        getEl('on-expand', 'expand-icons').click();
        getEl('on-expand', 'callback-result').should('have.text', 'Coding');
    });

    it('Execute attached on collapse callback', () => {
        getEl('on-collapse', 'expand-icons').eq(0).click();
        getEl('on-collapse', 'collapse-icons').eq(0).click();
        getEl('on-collapse', 'callback-result').should('have.text', 'Coding');
    });

    it('Execute attached on click callback', () => {
        getEl('on-click', 'taxonomy-tree-node').eq(0).click();
        getEl('on-click', 'callback-result').should('have.text', 'Coding');
    });

    it('Given custom icons, it overrides default icons', () => {
        getEl('custom-icons', 'expand-collapse-icon').within(() => {
            cy.get('path').should('have.attr', 'd', customIcon);
        });

        // expand to check the expanded icon
        getEl('custom-icons', 'expand-collapse-icon').click();

        getEl('custom-icons', 'expand-collapse-icon').within(() => {
            cy.get('path').should('have.attr', 'd', customIcon);
        });
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}

// Angular icon used for custom collapse/expand icon
const customIcon = 'M9.46981 0L0 3.31167L1.49612 15.64L9.47987 20L17.5039 15.5808L19 3.2525L9.46981 0ZM9.44717 1.04833L18.0557 3.965L16.6611 14.9917L9.44717 18.9583L2.34481 15.0442L1.06254 4.01833L9.44717 1.04833ZM9.44717 1.69833L3.59353 14.6417L5.77732 14.6017L6.9514 11.6842H12.1987L13.4835 14.6425L15.5734 14.6808L9.44717 1.69833ZM9.46394 5.84833L11.4389 9.95667H7.72043L9.46394 5.84833Z';
