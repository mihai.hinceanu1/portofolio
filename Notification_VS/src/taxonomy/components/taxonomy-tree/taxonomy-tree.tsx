import * as div from './taxonomy-tree.style';
import { Input } from '../../../shared/components/input/input';
import { TaxonomyLevel } from '../../interfaces/taxonomy-level';
import { ITaxonomyTree } from '../../interfaces/taxonomy-tree';
import { TaxonomyTreeNode } from '../taxonomy-tree-node/taxonomy-tree-node';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps, ITaxonomyTree {
    overrides?: {
        root?: string;
    };
}
interface State {
    searchTerm: string;
}

class _TaxonomyTree extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            searchTerm: '',
        };
    }

    public render() {
        const { levels, collapseIcon, expandIcon, onLevelCollapse, onLevelExpand, onLevelClick, overrides } = this.props;
        const { searchTerm } = this.state;

        return (
            <div.TaxonomyTree data-cy='taxonomy-tree'
                override={!!overrides && overrides.root}>

                {/** Title */}
                <div.Title data-cy='title'>Taxonomy</div.Title>

                {/** Frame body */}
                <div.Frame data-cy='frame'>

                    {/** Search bar */}
                    <Input type='text'
                        value={searchTerm}
                        onChangeText={(text: string) => this.onSearchChange(text)}
                        placeholder='Search'
                        overrides={div.Input}
                        icon={div.SearchIcon} />

                    {/** Tree Listing */}
                    {
                        !!levels.length &&
                        levels.map(node =>
                            <TaxonomyTreeNode key={node._id}
                                level={node}
                                collapseIcon={collapseIcon}
                                expandIcon={expandIcon}
                                onLevelCollapse={(level: TaxonomyLevel) => onLevelCollapse && onLevelCollapse(level)}
                                onLevelExpand={(level: TaxonomyLevel) => onLevelExpand && onLevelExpand(level)}
                                onLevelClick={(level: TaxonomyLevel) => onLevelClick && onLevelClick(level)} />
                        )
                    }
                </div.Frame>
            </div.TaxonomyTree>
        );
    }

    private onSearchChange(text: string) {
        this.setState({ searchTerm: text });
    }
}

export const TaxonomyTree = withRouter<Props, any>(_TaxonomyTree);
