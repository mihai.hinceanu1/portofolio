import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

export const TaxonomyTreeNode = styled.View`
    padding-left: 10px;
`;

export const Level = styled.View`
    width: 100%;
    flex-direction: row;
    align-items: center;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 10px;
`;

export const Name = styled.TouchableOpacity`

`;

export const Label = styled.Text`
    font-weight: 600;
`;

export const CollapseIcons = styled.TouchableOpacity`
    width: 50px;
    height: 20px;
    align-items: center;
    justify-content: flex-start;
    flex-direction: row;
`;

export const ExpandIcons = styled.TouchableOpacity`
    width: 50px;
    height: 20px;
    align-items: center;
    justify-content: flex-start;
    flex-direction: row;
`;

// ====== SVGs =====

export const iconSvg = (svgPath: string, dataCy?: string, fill?: string, rotation?: number): JSX.Element => {
    let { width, height } = pathBoundingBox(svgPath);

    return (
        <Svg fill='none' width={width}
            data-cy={dataCy ? dataCy : 'svg'}
            height={height}
            transform={`rotate(${rotation || 0})`}
            style={{ marginRight: 10 }}>

            <Path d={svgPath} fill={fill || colors.$grey} />
        </Svg>
    );
};
