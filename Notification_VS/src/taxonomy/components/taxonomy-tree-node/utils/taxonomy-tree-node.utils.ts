import { TaxonomyQueryString } from '../../../interfaces/taxonomy-query-string';
import * as queryString from 'query-string';

/**
 *
 * @param search current search terms taken from url (i.e. ?domain=coding&category=frontend)
 * @param depth level that was clicked
 * @param pathname current clicked level
 */
export function getSearchQueryString(search: string, depth: number, pathname: string): string {
    let searchObject = queryString.parse(search);
    let newSearchObject: TaxonomyQueryString = {
        domain: '',
        category: '',
        tlt: '',
    };

    if (depth === 0) {
        newSearchObject.domain = pathname;

        delete newSearchObject.category;
        delete newSearchObject.tlt;
    }

    if (depth === 1) {
        newSearchObject = {
            ...searchObject,
            category: pathname,
        };

        delete newSearchObject.tlt;
    }

    if (depth === 2) {
        newSearchObject = {
            ...searchObject,
            tlt: pathname,
        };
    }

    return queryString.stringify(newSearchObject);
}