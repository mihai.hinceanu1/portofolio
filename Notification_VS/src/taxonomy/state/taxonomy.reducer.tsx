import * as actions from './taxonomy.actions';
import { taxonomyInitialState } from './taxonomy.init-state';
import { TaxonomyState } from '../interfaces/taxonomy.state';

export const taxonomyReducer = (state: TaxonomyState = taxonomyInitialState, action: any): TaxonomyState => {

    switch (action.type) {
        case actions.GET_TAXONOMY_TREE_OK: {
            return {
                ...state,
                tree: action.payload,
            };
        }

        default:
            return state;
    }

};
