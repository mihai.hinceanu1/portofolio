import { TaxonomyState } from '../interfaces/taxonomy.state';

export const taxonomyInitialState: TaxonomyState = {
    tree: null,
};
