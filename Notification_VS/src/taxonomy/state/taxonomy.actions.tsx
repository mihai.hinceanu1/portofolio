import { Action } from '../../shared/interfaces/shared';
import { TaxonomyLevel } from '../interfaces/taxonomy-level';

export const GET_TAXONOMY_TREE_REQ = 'GET_TAXONOMY_TREE_REQ';
export const getTaxonomyTreeReq = (): Action<null> => ({
    type: GET_TAXONOMY_TREE_REQ,
    payload: null,
});

export const GET_TAXONOMY_TREE_OK = 'GET_TAXONOMY_TREE_OK';
export const getTaxonomyTreeOk = (tree: TaxonomyLevel[]): Action<TaxonomyLevel[]> => ({
    type: GET_TAXONOMY_TREE_OK,
    payload: tree,
});

export const GET_TAXONOMY_TREE_FAIL = 'GET_TAXONOMY_TREE_FAIL';
export const getTaxonomyTreeFail = (error: string): Action<string> => ({
    type: GET_TAXONOMY_TREE_FAIL,
    payload: error,
});
