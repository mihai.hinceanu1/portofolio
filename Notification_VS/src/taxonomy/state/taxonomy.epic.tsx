import * as taxonomyActions from './taxonomy.actions';
import { Action } from '../../shared/interfaces/shared';
import * as taxonomyWebapi from '../webapis/taxonomy.webapi';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getTaxonomyTree$ = (action$: ActionsObservable<Action<null>>) =>
    action$.ofType(taxonomyActions.GET_TAXONOMY_TREE_REQ).pipe(
        map(action => action.payload),
        concatMap(() => taxonomyWebapi.getTaxonomyTree().pipe(
            map(tree => taxonomyActions.getTaxonomyTreeOk(tree)),
            catchError(err => err.data && of(taxonomyActions.getTaxonomyTreeFail(err.data.response))),
        )),
    );
