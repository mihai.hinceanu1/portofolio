import { AppState } from '../../shared/interfaces/app.state';
import { TaxonomyLevel } from '../interfaces/taxonomy-level';
import { TaxonomyState } from '../interfaces/taxonomy.state';
import { createSelector, Selector } from 'reselect';

const TAXONOMY_MODULE: Selector<AppState, TaxonomyState> = (state: AppState) => state.taxonomy;

export const TREE = createSelector<AppState, TaxonomyState, TaxonomyLevel[]>(
    TAXONOMY_MODULE,
    (state: TaxonomyState) => state.tree,
);
