import { store, store$ } from '../../shared/services/state.service';
import { TaxonomyLevel } from '../interfaces/taxonomy-level';
import * as actions from '../state/taxonomy.actions';
import * as sel from '../state/taxonomy.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export const tree$ = (): Observable<TaxonomyLevel[]> => store$.pipe(
    map(state => sel.TREE(state)),
    skipWhile(tree => tree === null),
    distinctUntilChanged(),
);

export function getTaxonomyTree() {
    store.dispatch(
        actions.getTaxonomyTreeReq(),
    );
}
