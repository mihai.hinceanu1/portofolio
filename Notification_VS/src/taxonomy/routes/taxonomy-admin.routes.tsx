import { LoginResponse } from '../../auth/interfaces/login';
import * as React from 'react';
import { Text } from 'react-native';
import { Route } from 'react-router';
// import { renderPage } from '../../shared/services/utils.service';
// import { TaxonomyPage } from '../pages/taxonomy.page';

export const taxonomyAdminRoutes = (_loginResponse: LoginResponse, key: string) => {
    return [
        <Route path='/admin/taxonomy' key={key} render={() => <Text>Taxonomy</Text>} />,
        // <Route path='/admin/taxonomy' key={key}
        //     render={props => renderPage(props, true, '/auth/login', TaxonomyPage, loginResponse)} />,
    ];
};
