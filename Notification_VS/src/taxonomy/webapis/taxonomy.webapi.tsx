import axios from '../../shared/services/axios.service';
import { TaxonomyLevel } from '../interfaces/taxonomy-level';
import { from, Observable } from 'rxjs';

export const getTaxonomyTree = (): Observable<TaxonomyLevel[]> =>
    from<Promise<TaxonomyLevel[]>>(
        axios.get<TaxonomyLevel[]>(`/taxonomy`)
            .then(res => res.data),
    );
