export interface TaxonomyQueryString {
    domain?: string;

    category?: string;

    tlt?: string;
}
