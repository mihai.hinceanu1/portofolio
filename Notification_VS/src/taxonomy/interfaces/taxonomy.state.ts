import { TaxonomyLevel } from './taxonomy-level';

export interface TaxonomyState {
    tree: TaxonomyLevel[];
}
