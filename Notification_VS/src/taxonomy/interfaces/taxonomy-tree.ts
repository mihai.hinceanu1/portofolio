import { TaxonomyLevel } from './taxonomy-level';

export interface ITaxonomyTree {
    /** levels under Tree Data Structure */
    levels: TaxonomyLevel[];

    /** Attached function to search input */
    searchCallback?: (term: string) => void;

    /** Icon for collapsing levels */
    collapseIcon?: string;

    /** Icon for expanding levels */
    expandIcon?: string;

    /** Additional functionality when any level expands */
    onLevelExpand?: (level?: TaxonomyLevel) => void;

    /** Additional functionality when any level collapses */
    onLevelCollapse?: (level?: TaxonomyLevel) => void;

    /** Custom trigger when level click */
    onLevelClick?: (level: TaxonomyLevel) => void;
}
