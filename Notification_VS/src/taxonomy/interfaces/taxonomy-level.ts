import { CommonMetadata } from '../../shared/interfaces/common-metadata';

export interface TaxonomyLevel extends CommonMetadata {
    pathName: string;

    name: string;

    isRoot: boolean;

    icon: string;

    parent: string;

    children: TaxonomyLevel[];
}
