import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const AdminNavigator = styled.View`
    background-color: ${colors.$white};
    position: absolute;
    top: 0;
    left: 0;
    width: 300px;
    height: 100%;
    z-index: 100;
`;

export const AdminNavigatorList = styled.View`
    margin-top: 40px;
    margin-left: 50px;
`;

export const NavigatorLabel = styled.Text`
    font-weight: 600;
    color: ${colors.$blue};
    margin-left: 17px;
`;

// ===== OVERRIDES =====

export const BRANCHES_MENU_OVERRIDES = {
    root: `
        top: 20px;
        left: 36px;
        width: auto;
    `,
};
