import * as div from './admin-navigator.style';
import * as utils from './admin-navigator.utils';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { colors } from '../../../shared/style/colors';
import * as hover from '../../../shared/style/hover-menus.style';
import { AdminNavigatorBullet } from '../admin-navigator-bullet/admin-navigator-bullet';
import { AdminNavigatorItem } from '../admin-navigator-item/admin-navigator-item';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    width: number;
}
interface State {}

class _AdminNavigator extends React.Component<Props, State> {
    // key for mapping through nav items
    private itemId = 0;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { history, width } = this.props;

        return (
            <div.AdminNavigator data-cy='admin-navigator'>

                {/** Branches Menu */}
                <BranchesMenu config={utils.branchesMenuConfig}
                    overrides={div.BRANCHES_MENU_OVERRIDES} />

                {/** Logo */}
                <hover.AppLogo width={width}>
                    <Icon path={icons.VS_LOGO} width={83} height={25} color={colors.$blue} />
                </hover.AppLogo>

                {/** Navigator Bullet */}
                <hover.AdminNavigator>

                    {/** Bullet */}
                    <AdminNavigatorBullet />

                    {/** Label */}
                    <div.NavigatorLabel data-cy='navigator-label'>
                        Admin Navigator
                    </div.NavigatorLabel>

                </hover.AdminNavigator>

                {/** Navigator Items */}
                <div.AdminNavigatorList data-cy='admin-navigator-list'>
                    {
                        utils.getNavigatorItems(history).map(item =>
                            <AdminNavigatorItem key={`navigator-item-${this.itemId++}`}
                                navItem={item} />
                        )
                    }
                </div.AdminNavigatorList>
            </div.AdminNavigator>
        );
    }

    public componentWillUnmount() {
        this.itemId = 0;
    }
}

export const AdminNavigator = withRouter<Props, any>(_AdminNavigator);
