import * as icons from '../../../shared/assets/icons';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../../shared/constants/branches-menu.const';
import { BranchesMenuConfig } from '../../../shared/interfaces/branches-menu';
import { colors } from '../../../shared/style/colors';
import { IAdminNavigatorItem } from '../../interfaces/admin-navigator-item';
import { History } from 'history';

export function getNavigatorItems(history: History): IAdminNavigatorItem[] {
    return [{
        icon: icons.LESSON,
        text: 'My Lessons',
        onPress: () => history.push('/'),
        isAvailable: false,
    }, {
        icon: icons.APP,
        text: 'My Projects',
        onPress: () => history.push('/'),
        isAvailable: false,
    }, {
        icon: icons.APP,
        text: 'Source Code',
        onPress: () => history.push('/admin/packages-list'),
        isAvailable: true,
    }, {
        icon: icons.COMMENTS,
        text: 'Dialog Manager',
        onPress: () => history.push('/'),
        isAvailable: false,
    }, {
        icon: icons.APP,
        text: 'Media Hub',
        onPress: () => history.push('/'),
        isAvailable: false,
    }, {
        icon: icons.APP,
        text: 'My Teams',
        onPress: () => history.push('/'),
        isAvailable: false,
    }, {
        icon: icons.APP,
        text: 'Reports',
        onPress: () => history.push('/'),
        isAvailable: false,
    }, {
        icon: icons.APP,
        text: 'Help',
        onPress: () => history.push('/'),
        isAvailable: false,
    }];
}

export const branchesMenuConfig: BranchesMenuConfig = {
    branches: [],
    icon: {
        svgPath: icons.LEARNING_MAP,
        color: colors.$blue,
    },
    color: colors.$white,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.bottom,
};
