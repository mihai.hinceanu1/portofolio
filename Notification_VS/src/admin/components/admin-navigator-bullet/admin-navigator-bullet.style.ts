import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const AdminNavigatorBullet = styled.TouchableOpacity`
    align-items: center;
    justify-content: center;
    flex-direction: row;
    width: 50px;
    height: 50px;
    background-color: ${colors.$blue};
    border-radius: 25px;
`;

export const Label = styled.Text`
    color: ${colors.$blue};
`;