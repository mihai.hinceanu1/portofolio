import * as div from './admin-navigator-bullet.style';
import { Icon } from '../../../icons/components/icon/icon';
import { ADMIN_NAVIGATOR } from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import { setShowAdminNavigator } from '../../../source-code/services/source-code-common.service';
import * as React from 'react';

interface Props {}

export const AdminNavigatorBullet: React.FunctionComponent<Props> = (_props: Props) => {
    return (
        <div.AdminNavigatorBullet data-cy='admin-navigator-bullet'
            onPress={() => setShowAdminNavigator()}>

                {/** Icon */}
                <Icon path={ADMIN_NAVIGATOR} width={30} height={30} color={colors.$white} />

        </div.AdminNavigatorBullet>
    );
};
