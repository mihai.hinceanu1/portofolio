import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

// ===== INTERFACES =====

interface LabelProps {
    isAvailable: boolean;
}

export const AdminNavigatorItem = styled.TouchableOpacity`
    justify-content: flex-start;
    align-items: center;
    flex-direction: row;
    margin-top: 20px;
    margin-bottom: 20px;
`;

export const Label = styled.Text<LabelProps>`
    margin-left: 20px;
    color: ${props => getColor(props.isAvailable)};
`;

// ===== UTILS =====

export function getColor(isAvailable: boolean) {
    if (isAvailable) {
        return colors.$darkBlue;
    }

    return colors.$grey;
}
