import * as div from './admin-navigator-item.style';
import { Icon } from '../../../icons/components/icon/icon';
import { IAdminNavigatorItem } from '../../interfaces/admin-navigator-item';
import * as React from 'react';

interface Props {
    navItem: IAdminNavigatorItem;
}

export const AdminNavigatorItem: React.FunctionComponent<Props> = (props: Props) => {
    const { navItem } = props;
    const { text, onPress, icon, isAvailable } = navItem;

    return (
        <div.AdminNavigatorItem data-cy='admin-navigator-item'
            onPress={() => onPress()}
            disabled={!isAvailable}>

            {/** Icon */}
            <Icon path={icon} width={40} height={40} color={div.getColor(isAvailable)} />

            {/** Label */}
            <div.Label data-cy='label' isAvailable={isAvailable}>
                {text}
            </div.Label>

        </div.AdminNavigatorItem>
    );
};
