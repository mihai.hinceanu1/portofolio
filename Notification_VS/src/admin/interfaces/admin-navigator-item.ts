export interface IAdminNavigatorItem {
    onPress: () => void;
    text: string;
    icon: string;
    isAvailable: boolean;
}
