import { LoginResponse } from '../../auth/interfaces/login';
import { lessonEditorAdminRoutes } from '../../lesson-editor/routes/lesson-editor-admin.routes';
import { nanolessonsAdminRoutes } from '../../nano-lessons/routes/nanolessons-admin.routes';
import { sourceCodeAdminRoutes } from '../../source-code/routes/source-code-admin.routes';
// import { taxonomyAdminRoutes } from '../../taxonomy/routes/taxonomy-admin.routes';

/**
 * Admin Routes
 *
 * Passing keys is necessary when returning all in array.
 * Returning all in array is needed for the <Switch> to work
 */
export const adminRoutes = (loginResponse: LoginResponse) => {
    return [
        lessonEditorAdminRoutes(loginResponse, 'lesson-editor'),
        sourceCodeAdminRoutes(loginResponse, 'source-code'),
        // taxonomyAdminRoutes(loginResponse, 'taxonomy'),
        nanolessonsAdminRoutes(loginResponse, 'nanolessons'),
    ];
};