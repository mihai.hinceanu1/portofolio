import * as div from './not-found.page.style';
import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
// import { FitImage } from '../../components/fit-image/fit-image';

interface Params { }

interface Props extends RouteComponentProps<Params> {

}
interface State { }

export class NotFoundPage extends React.Component<Props, State> {
    public render() {
        return (
            <div.NotFoundPage data-cy='not-found-page'>
                {/* <FitImage name={`/shared/page_not_found.gif`} /> */}
                <TouchableOpacity onPress={() => this.props.history.push('/components-catalog')}>
                    <Text>
                        Click here for test page
                    </Text>
                </TouchableOpacity>
            </div.NotFoundPage>
        );
    }
}

export const NotFoundPageClass = withRouter(NotFoundPage);