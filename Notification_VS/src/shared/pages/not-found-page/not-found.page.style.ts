import styled from 'styled-components/native';

export const NotFoundPage = styled.View`
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
`;
