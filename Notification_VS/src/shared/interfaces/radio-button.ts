/** Identify and describe a option for radio button. */
export interface IRadioOption {
    _id: string;
    label: string;

    onPress?: () => void;
}
