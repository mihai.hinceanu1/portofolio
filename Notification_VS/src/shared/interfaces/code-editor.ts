/**
 * Code editor components configuration.
 * Depending on the platform and situation we might have different components rendering the same code
 * This interface serves the purpose of creating a common contract for all these components.
 */
export interface ICodeEditor {
    /** Code snippet or some other arbitrary string */
    srcCode: string;

    /** Allow the user to edit the source code */
    editable?: boolean;

    /** These markers highlight code either for code annotations or for tooltips */
    markers?: any[]; // IMarker[]; // RESTORE for Monaco editor

    width?: number;
    height?: number;

    /** Programming language used for the code snippet (Javascript, C++, etc)  */
    language?: string;

    /** Shorter code samples are better render without line numbers */
    showLineNumbers?: boolean;

    /** Color theme used for rendering the code. Usually in sync with the rest of the app. */
    theme?: string;

    /** Should the current line under the caret be highlighted */
    highlightActiveLine?: boolean;

    onChangeValue?: (value: string) => void;

}
