/**
 * For some actions like creating, editing or deleting a resource
 * a confirmation message must be communicated to the user so he's
 * aware of the result. These actions can have 2 results for now,
 * they can either be successful or show that an error ocurred.
 */
export enum TOASTR_TYPE {
    ERROR = 'ERROR',
    SUCCESS = 'SUCCESS',
}

export interface IToastr {
    message: string;

    type: TOASTR_TYPE;

    isNarrow?: boolean;

    /** Custom icon */
    icon?: string;

    close?: () => void;

    /**
     * Used for "key" prop in ActionConfirmationsOverlay.
     * Increment each time an action is created
     */
    id?: string;

    override?: {
        root?: string;
    };
}