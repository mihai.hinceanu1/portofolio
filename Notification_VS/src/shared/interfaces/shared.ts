/**
 * Generic type of action.
 * This is somewhat against the general recomandation of creating unique interfaces fore each action.
 */
export interface Action<P> {
    type: string;
    payload?: P;
}

/** Check DB is alive. */
export interface DbCheck {
    message: string;
    connected: boolean;
}

/** Pagination params for endpoints that return lists. */
export interface Paginated {
    page?: number;
    perPage?: number;
}

// Move to search ?
/** Search query results (paginated). */
export interface PaginatedSearchReq {
    query: string;
    page?: number;
    perPage?: number;
}

// RESTORE to all enpoints that need it
/** Retrieve children elements by UUID. */
export interface PaginatedChildrenReq {
    uuid: string;
    page?: number;
    perPage?: number;
}

export interface MongoError {
    message: string;
}
