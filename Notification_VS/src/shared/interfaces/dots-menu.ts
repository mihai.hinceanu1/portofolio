import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../constants/dots-menu.const';

export interface DotsMenuConfig {
    textDirection: TEXT_DIRECTION;
    menuExpandDirection: MENU_EXPAND_DIRECTION;

    /** Dots Background Color */
    bgrColor?: string;

    txtColor?: string;

    /** Open the menu programmatically */
    isExpanded?: boolean;

    /** Items to be listed in the menu */
    dots: IDot[];

    /** Anchor element from which the menu stems */
    trigger: IDot;
}

export interface IDot {

    /**
     * Unique identifier from db or hardcoded for static lists.
     * Helps React preserve maximum performance when rendering lists.
     */
    _id: string;

    /** User defined callback */
    onClick?: () => void;

    icon: DotIcon;
    label?: string;
}

export interface DotIcon {
    assetsSrc: 'image' | 'svg' ;
    imagePath?: string;

    /** Svg data string */
    svgData?: string;
    fill?: string;
}