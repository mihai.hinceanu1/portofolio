import { TopicDto } from '../../topics/interfaces/topic';

// REVIEW
/** Generic definition of a Elasticsearch object */
export interface EsObject {
    took: number;
    timed_out: boolean;
    _shards: Shard;
}

// REVIEW
export interface Shard {
    total: number;
    successful: number;
    failed: number;
}

// REVIEW
export interface TopicHits extends EsObject {
    total: number;
    max_score: number;
    hits: {
        hits: TopicDto[];
    };
}

// REVIEW
export interface EsError {
    message: string;
}
