export interface ITabs {

}

/**
 * Represents the instance of single Tab.
 * Each tab can have different behaviour when active or closing
 * The parent component MUST implement those functionalities for triggering
 */
export interface ITab {
    /** Unique key used in .map() to improve performance. */
    key: string;

    /** Suggestive string that is displayed inside */
    label: string;

    /**
     * Executes when clicking one tab
     * It's defined by the user, it doesn't overlap active tab
     */
    onClick?: () => void;

    /** Executes when closing one tab */
    onClose?: () => void;
}
