/**
 * All objects defined in database store these properties.
 * DTOs defined at runtime via data aggregation do not have any of these properties.
 * <!> Even DTOs that are created by scheduled jobs should not extend this interface.
 */
export interface CommonMetadata {

    _id: any; // Because mongoose uses any :(

    /** Deletion not allowed, only disabled. */
    isActive: boolean;

    /** Date of creation */
    created: number;

    /** TOTO REVIEW Reconsider if we are going to allow EDIT and DELETE operations */
    updated: number;

    /** Points to the user that created this object */
    authorId?: string;

    /** Mongo is adding this automatically. */
    __v?: number;

}