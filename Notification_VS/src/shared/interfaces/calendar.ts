export interface Month {
    month: number;
    year: number;
}

export interface CalendarDay {
    year: number;
    month: string;
    day: string;
}