import { Icon } from './button';
import { IDropdownOption } from './dropdown';

export interface HeaderConfig {
    title: string;
    description?: string;

    textColor?: string;
    highlightColor?: string;

    /**
     * Config for header navigation buttons
     * We are using IDropdownOption because when on mobile, it renders as a dropdown.
     * On web, it's just like a basic navigation
     */
    options?: IDropdownOption[];

    /**
     * Handle active option from the parent
     * It underlines the current selected options
     * The active option might influence the way data is showing
     * For example, it might filter some data by it's type (all handled by the parent)
     * Also, different option than the first one might be selected as "active"
     */
    activeOption?: IDropdownOption;

    /** Config for header button */
    button?: PageHeaderButton;

    /**
     * Config for filter bar, contains filters and a general callback for selecting
     * a filter to allow the parent to control the functionality.
     *
     * <!>Maybe a clear filters functionality should be provided.
     */
    filterBar?: IHeaderFilter;

    /**
     * Search bar config for header.
     * Parent must provide alongside the value and onChange
     * method, submit method and clear method, used when
     * the screen width is narrow.
     *
     * <!>Maybe the clear method can be used in both cases(wide and narrow).
     */
    searchBar?: PageHeaderSearchBar;
}

export interface IHeaderFilter {
    filters: Filter[];
    selectFilter: (filter: Filter) => void;
}

export interface Filter {
    _id: string;
    label: string;
    callback?: (filter: Filter) => void;
}

export interface PageHeaderButton {
    onPress: () => void;
    text: string;
    color?: string;
    icon?: Icon;
}

export interface PageHeaderSearchBar {
    value: string;
    onChange: (text: string) => void;
    onSubmit: () => void;
    clearSearch: () => void;
}
