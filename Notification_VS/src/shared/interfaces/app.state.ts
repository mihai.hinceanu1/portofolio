import { SharedState } from './shared.state';
import { AuthState } from '../../auth/interfaces/auth.state';
import { CommentsState } from '../../comments/interfaces/comments.state';
import { FeedbackState } from '../../feedback/interfaces/feedback.state';
import { IconsState } from '../../icons/interfaces/icons-state';
import { ImagesState } from '../../images/interfaces/images.state';
import { LessonEditorState } from '../../lesson-editor/interfaces/lesson-editor.state';
import { LessonNavigatorState } from '../../lesson-navigator/interfaces/lesson-navigator.state';
import { LessonsCatalogState } from '../../lessons-catalog/interfaces/lessons-catalog.state';
import { LessonsState } from '../../lessons/interfaces/lessons.state';
import { MapState } from '../../maps/interfaces/maps.state';
import { NanolessonsState } from '../../nano-lessons/interfaces/nano-lessons.state';
import { NotificationState } from '../../notifications/interfaces/notification-state.interface';
import { SearchState } from '../../search/interfaces/search.state';
import { SourceCodeState } from '../../source-code/interfaces/state/source-code.state';
import { TaxonomyState } from '../../taxonomy/interfaces/taxonomy.state';
import { TopicsState } from '../../topics/interfaces/topics.state';
import { UsersState } from '../../users/interfaces/users.state';

export interface AppState {
    shared: SharedState;
    auth: AuthState;
    comments: CommentsState;
    feedback: FeedbackState;
    icons: IconsState;
    images: ImagesState;
    lessonEditor: LessonEditorState;
    lessonNavigator: LessonNavigatorState;
    lessonsCatalog: LessonsCatalogState;
    lessons: LessonsState;
    map: MapState;
    nanolessons: NanolessonsState;
    notifications: NotificationState;
    search: SearchState;
    sourceCode: SourceCodeState;
    taxonomy: TaxonomyState;
    topics: TopicsState;
    users: UsersState;
}
