import { GestureResponderEvent } from 'react-native';

/**
 * Until we figure out a better way to reconcile React Native and React Native Web interfaces in Styled Components
 * In certain cases we need to use web specific attributes for styled views
 * We can use this interface to get the right type of events.
 * The benefit is that we get to preserve all the type coverage for styled views, meaning autocomplete works
 *
 * Ex:
 * export const Heart = styled.View<ReactWebAttributes>``;
 *
 * TODO This is a workaround. We need to find the proper solution eventually.
 */
export interface ReactWebAttributes {
    onMouseEnter?: (e: React.MouseEvent) => void;
    onMouseLeave?: (e: React.MouseEvent) => void;
    onClick?: (e: React.MouseEvent) => void;
    onPress?: (e: GestureResponderEvent) => void;
}