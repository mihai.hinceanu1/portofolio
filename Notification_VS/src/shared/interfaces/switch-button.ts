
export interface SwitchButtonConfig {
    /** Generic text color of the button text */
    default: boolean;

    /** Associated function, executed on button press */
    onPress?: (e: boolean) => void;
}

export interface Overrides {
    sliderColor: string;
    switchColor: string;
}