export interface FileInput {
    type: string;
    name: string;
    size: number;
}