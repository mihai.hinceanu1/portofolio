/**
 * Measurements of the bounding box obtained from Raphael
 * Available also in React Native
 */
export interface PathBoundingBox {
    x: number;
    y: number;
    x2: number;
    y2: number;
    width: number;
    height: number;
    cx?: number;
    cy?: number;
}