export interface InputOverrides {
    /** Component that encapsulated the others */
    root?: string;

    /** Override properties of the core components (containing input and icon) */
    inputFrame?: string;

    /** Override input properties */
    input?: string;
}