export interface SvgIcon {
    type: 'svg';
    svgPath: string;
    fill: string;
}

export interface ImageIcon {
    type: 'image';
    imageUrl: string;
}

export type TIcon = SvgIcon | ImageIcon;
