/**
 * Represents an indicator item that has
 * _id, to improve render performance
 * label, a short text that's placed under the value,
 * value, a counter that represents the number of counted values
 * and color of the indicator
 */
export interface IIndicatorsCardItem {
    /** Unique identifier to improve render performance */
    _id: string;

    /** Specific text shown below value */
    label: string;

    /** Value of the specific indicator */
    value: string;

    /**
     * Color of the value and label
     * The default value is black (#000000)
     */
    color?: string;
}
