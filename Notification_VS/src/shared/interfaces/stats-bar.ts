import { MILESTONES } from '../../source-code/services/packages-list.utils';

export interface IStatsBar {
    /**
     * Number of awards given by users
     * Same functionality as Reddit's Golden Award
     */
    awards: number;

    /**
     * Total number of reactions
     * Sum of every sentiment (laugh, love, party etc...)
     */
    reactions: number;

    /** Editor's Choice, Highest Rated, Bestseller, etc. */
    milestone: MILESTONES;

    /** Number of comments */
    comments: number;

    /**
     * Line of codes
     * Sum of all files in the entire package
     */
    codeLines: number;

    /**
     * Technologies that the lesson is written with
     * Array of strings that contains the exact name of the technology
     * To any technology sent, there will be an icon that maps the value in the UI
     * Ex: 'angular' will have the angular icon
     */
    techStack: string[];

    /** If there are any new comments, a red circle appears near CommentIcon */
    newComments?: number;

    /** Display special flag */
    isEditorChoice?: boolean;

    /** Color of icons, texts, editor's choice highlight */
    color?: string;

    /**
     * Greyed out if disabled.
     * For packages that are no longer used, unavailable, deprecated or corrupt
     */
    disabled?: boolean;

    /**
     * Width and Height is necessary
     */
    width?: number;

    height?: number;
}
