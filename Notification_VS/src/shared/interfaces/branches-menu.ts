import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../constants/branches-menu.const';

export interface BranchesMenuConfig {
    /** Items listed in the branches menu */
    branches: IBranch[];

    /** Menu can expand from the toggle button towards the top or the bottom */
    stubDirection: STUB_DIRECTIONS;

    /** Branches can face either left side or right side */
    branchesDirection: BRANCHES_DIRECTIONS;

    /** Optional color for anchor, stub and branches */
    color?: string;

    /** Optional color for text, in case we have a dark background */
    textColor?: string;

    /** Optional function called when toggle is pressed */
    onTogglePress?: () => void;

    icon?: BranchesMenuIcon;

    /** Open the menu programmatically */
    isExpanded?: boolean;
}

/** A branch can have children options listed bellow the name */
export interface IBranch extends BranchConfig {
    children?: BranchConfig[];
}

export interface BranchConfig {

    /**
     * Unique identifier from db or hardcoded for static lists.
     * Helps React preserve maximum performance when rendering lists.
     */
    _id: string;

    name: string;
    onPress?: () => void;
}

export interface DirectionsProps {
    branchesDirection: BRANCHES_DIRECTIONS;
    stubDirection: STUB_DIRECTIONS;
}

export interface BranchesMenuIcon {
    /** Optional toggle icon color */
    color?: string;

    /** Svg Path for the icon inside the toggle button */
    svgPath?: string;
}

/** Configs used to test callbacks in the doc page. */
export interface BranchesCallbackConfigs {
    toggleConfig: BranchesMenuConfig;
    branchesCallbacksConfig: BranchesMenuConfig;
    childrenCallbacksConfig: BranchesMenuConfig;
}