import { GestureResponderEvent } from 'react-native';

export interface ButtonConfig {
    /** Associated function, executed on button press */
    onPress: (e: GestureResponderEvent) => void;

    /** Generic text color of the button text */
    color?: string;

    /** Button can be filled */
    filled?: boolean;

    /** Greys out the button and disables onPress function */
    disabled?: boolean;

    /** If provided will set the button width to this (in px) */
    width?: number;

    /** Text rendered inside the button */
    text?: string;

    /** Font size if default doesn't work for a certain use case (example login button) */
    fontSize?: number;

    /** Font weight of the text rendered in button */
    fontWeight?: number;

    /** It can have an icon next to the name */
    icon?: Icon;

    /** The button can have rounded borders */
    rounded?: boolean;
}

/**
 * When we have a button with text and icon we can
 * choose on which side we want to have the icon.
 */
export enum ICON_SIDES {
    'left',
    'right'
}

export interface ButtonIcon {
    iconSide?: ICON_SIDES;
}

/**
 * This interface is used so that we know how to render the icon.
 *
 * The @type property is our reference point, it can be either image or svg,
 * and with that information we know if we have the path or url of an image or
 * a svg path, goes the same way for hover icon.
 *
 * TODO future improvement: the hover can be either svg or image, for now
 * this forces us to have the hover icon in the same format.
 */
export interface ButtonImage extends ButtonIcon {
    type: 'image';
    imagePath: string;
    imageHover?: string;
}

/** Icon is provided in the form of a svg path. */
export interface ButtonSvg extends ButtonIcon {
    type: 'svg';
    svgPath: string;
    svgHover?: string;
}

/**
 * This is basically the XOR between ButtonImage and ButtonSvg,
 * it can be either one or the other, can't be both.
 *
 * This type is useful when creating the config for a button because
 * it requires you to provide a type property and the other properties
 * depend on it.
 *
 * If the type is not provided and someone tries to combine the properties
 * the linter will highlight that, therefore there can't be any mismatches.
 */
export type Icon = ButtonImage | ButtonSvg;

export enum IconPosition {
    LEFT = 'LEFT',
    RIGHT = 'RIGHT',
}

export enum ButtonType {
    FLAT = 'FLAT',
    GHOST = 'GHOST',
    FILL = 'FILL',
    FAB = 'FAB',
    ROUNDED = 'ROUNDED',
}

export enum ButtonSize {
    SMALL = 'SMALL',
    MEDIUM = 'MEDIUM',
    LARGE = 'LARGE'
}

export interface IButton {
    type: ButtonType;

    callback?: () => void;

    text?: string;

    icon?: {
        position: IconPosition;
        path: string;
    };

    color?: string;

    /**
     * Different styling, depending on the size
     * @TODO implement
     */
    size?: ButtonSize;

    disabled?: boolean;

    /** Invert colors of the button () */
    inverted?: boolean;
}