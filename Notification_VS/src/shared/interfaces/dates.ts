
/** Used to render graps indicating a time serie (evolution of a value in time). */
export interface ValueOnDate {

    /** Mongo UUID forcefully added. TODO Find a way to remove it. */
    _id: string;

    /** Unix time */
    date: string;

    /** Value */
    value: number;
}