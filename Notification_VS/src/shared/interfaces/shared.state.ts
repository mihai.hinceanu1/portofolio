import { DbCheck } from './shared';
import { IToastr } from './toastr';

export interface SharedState {

    /** <!> All pages trigger this loader */
    isPageLoading: boolean;

    dbCheck: DbCheck;

    isStoreLoading: boolean;

    toastrs: IToastr[];
}
