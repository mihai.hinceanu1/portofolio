import { DROPDOWN_OPTION_SIDE, DROPDOWN_TYPE } from '../constants/dropdown.const';

export interface DropdownConfig {

    /** Selected option */
    selected?: IDropdownOption;

    /** List of options */
    options: IDropdownOption[];

    /** Callback for the dropdown button */
    callback?: (item: IDropdownOption) => void;

    /** Icon rendered in dropdown button. Defaults to arrow. */
    icon?: DropdownButtonIcon;

    /** Placeholder when no value is selected */
    placeholder?: string;

    /** Specifies the type of dropdown. Defaults to standard */
    type?: DROPDOWN_TYPE;

    /**
     * Specify on which side should the options appear
     * Example: if the dropdown is near the right edge
     * and options extend to the right they won't be visible,
     * therefore we set this props as side: DROPDOWN_OPTION_SIDE.LEFT
     * so the options are visible.
     */
    side?: DROPDOWN_OPTION_SIDE;
}

export interface IDropdownOption {
    _id: string;
    label: string;

    icon?: DropdownButtonIcon; // TO IMPLEMENT

    callback?: (option?: IDropdownOption) => void; // TO IMPLEMENT
}

export interface DropdownButtonIcon {
    svgData?: string;
    fill?: string;
}