import { ITopic } from '../../topics/interfaces/topic';
import { APP_CFG } from '../config/app.config';
import { DbCheck } from '../interfaces/shared';
import axios from 'axios';
import { from, Observable } from 'rxjs';

export const getDbConnection = (): Observable<DbCheck> =>
    from<Promise<DbCheck>>(
        axios.get<DbCheck>(`${APP_CFG.api}/shared/db-check`)
            .then(res => res.data),
    );

export const getTopicOverlay = (topicId: string): Observable<ITopic> =>
    from<Promise<ITopic>>(
        axios.get<ITopic>(`${APP_CFG.api}/topics/topics/${topicId}`)
            .then(res => res.data),
    );

export const contact = (): Observable<any> =>
    from<Promise<any>>(
        axios.post<any>(`${APP_CFG.api}/shared/contact`)
            .then(res => res.data),
    );