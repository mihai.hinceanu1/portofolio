import { isPageLoading$ } from '../../services/shared.service';
import { LoadingAnimation } from '../loading-animation/loading-animation';
import * as React from 'react';

interface Props { }

interface State {
    isPageLoading: boolean;
}

/**
 * <!> Used only int the root of the app. Always mounted.
 * It will display a loader animation when content is loading.
 * All state store paths include a signal sent to this component.
 */
export class PageLoadingOverlay extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isPageLoading: false,
        };
    }

    public render() {
        let { isPageLoading } = this.state;

        // Hide loader
        if (!isPageLoading) { return null; }

        return <LoadingAnimation color={'#1d90f1'} size={'small'} />;
    }

    public componentDidMount() {
        this.subscribeToIsPageLoading();
    }

    private subscribeToIsPageLoading() {
        isPageLoading$().subscribe(isPageLoading => {
            this.setState({ isPageLoading });
        });
    }
}
