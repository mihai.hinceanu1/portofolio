import { TOOLTIP_CONNECTOR_SIZE } from '../../constants/tooltips.const';
import { colors } from '../../style/colors';
import styled from 'styled-components/native';

interface Props {
    isReadyForDisplay: boolean;
    isDisplayed?: boolean;
    isExtended?: boolean;
    xCoord: number;
    yCoord: number;
}

export const Tooltip = styled.View``;

export const Label = styled.TouchableOpacity`
    background-color: ${colors.$highlightBgr};
`;

export const Connector = styled.View<Props>`
    opacity: ${props => props.isReadyForDisplay ? '1' : '0'};
    border-style: solid;
    border-left-width: ${TOOLTIP_CONNECTOR_SIZE}px;
    border-right-width: ${TOOLTIP_CONNECTOR_SIZE}px;
    border-top-width: ${TOOLTIP_CONNECTOR_SIZE}px;
    border-left-color: transparent;
    border-right-color: transparent;
    border-top-color: ${colors.$tooltipPanelBgr};
    display: ${props => props.isDisplayed ? 'flex' : 'none'};
    left: ${props => props.xCoord - TOOLTIP_CONNECTOR_SIZE}px;
    top: ${props => props.yCoord - TOOLTIP_CONNECTOR_SIZE}px;
    position: absolute;
    z-index: 10;
`;

export const ExtendedContent = styled.View<Props>`
    opacity: ${props => props.isReadyForDisplay ? '1' : '0'};
    display: ${props => props.isExtended ? 'flex' : 'none'};
    position: absolute;
    z-index: 10;
    left: ${props => props.xCoord}px;
    top: ${props => props.yCoord - TOOLTIP_CONNECTOR_SIZE}px;
    width: 200px;
    padding: 10px;
    background-color: ${colors.$tooltipPanelBgr};
    border-radius: 5px;
    /* top: 25px; */
`;

export const TooltipText = styled.Text`
    color: ${colors.$tooltipText};
`;