import {
    Connector,
    ExtendedContent,
    Label,
    TooltipText
    } from './tooltip.style';
import { GraphPosition } from '../../../graphs/interfaces/graphs';
import { font } from '../../style/font-sizes';
import * as React from 'react';
import { Text, View } from 'react-native';
import { Subject } from 'rxjs';
import { isNullOrUndefined } from 'util';

interface Props {
    targetedAt?: string; // Image / Text / Code

    // Was for text
    label?: string;
    tooltip?: any;

    role?: string;
    isVisible?: boolean;
    isAnimated?: boolean;
    goToReadMore?: () => void;

    // Image
    visible?: boolean;
    tooltipImg?: any;
}

interface State {

    isExtended: boolean;
    xCoord: number;
    yCoord: number;
    extendedTooltipId: string;
    tooltipHeight: number;
    tooltipWidth: number;

    // image
    contentPosition: GraphPosition;

    // TODO investigate a better solution
    // small hack to not duble render the tooltip on two separate positions
    isReadyForDisplay: boolean;
}

/**
 * Tooltips render small bits of information when the user needs some extra clarification.
 * Tooltips can be triggered by hovering the label text but also remotely via a boolean.
 * Animations can put extra emphasis, but they are seldom used.
 * TODO Bouncing animation.
 * TODO Not all tooltips have a button
 * TODO Make sure that hidden divs don't impact DOM performance (hiding only via opacity is not ok)
 *
 * @REFACTORING: same component for both images and text
 */
export class Tooltip extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            isExtended: false,

            // Text tooltip coords
            xCoord: -1,
            yCoord: -1,

            extendedTooltipId: '',
            tooltipHeight: -1,
            tooltipWidth: -1,

            // Image Tooltip coords
            contentPosition: {
                x: -1,
                y: -1,
            },

            isReadyForDisplay: false,
        };
    }

    public render() {

        const { xCoord, extendedTooltipId, isExtended,
            yCoord, tooltipHeight, tooltipWidth, contentPosition, isReadyForDisplay } = this.state;
        const { tooltip, targetedAt, tooltipImg } = this.props;

        // Based on targetAt: for iamge / text
        let customTooltip: any;
        let customTooltipPosition: GraphPosition;

        // TODO make a setter
        if (targetedAt === 'text') {
            customTooltip = tooltip;
            customTooltipPosition = {
                x: xCoord,
                y: yCoord,
            };
        } else if (targetedAt === 'image') {
            customTooltip = tooltipImg;
            customTooltipPosition = { ...contentPosition };
        }

        return (
            <Text data-cy='tooltip'>

                {/** Failsafe 'text' by default if no target is provided */}
                {this.getTooltipTriggerArea(targetedAt ? targetedAt : 'text')}

                {
                    !isNullOrUndefined(customTooltipPosition) &&
                    <>
                        <Connector isDisplayed={isExtended && extendedTooltipId === customTooltip._id}
                            isReadyForDisplay={isReadyForDisplay}
                            xCoord={customTooltipPosition.x}
                            yCoord={customTooltipPosition.y} />

                        <ExtendedContent onLayout={(ev: any) => this.measureTooltip(ev)}
                            isReadyForDisplay={isReadyForDisplay}
                            isExtended={isExtended && extendedTooltipId === customTooltip._id}
                            xCoord={customTooltipPosition.x - tooltipWidth / 2}
                            yCoord={customTooltipPosition.y - tooltipHeight}>

                            <TooltipText>{this.getTooltipContent(targetedAt)}</TooltipText>

                        </ExtendedContent>
                    </>
                }

            </Text>
        );
    }

    public componentDidMount() {
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private handleLabelClick(event: any) {
        const { tooltip } = this.props;
        const { extendedTooltipId, tooltipHeight, tooltipWidth } = this.state;
        let parentWidth = event.nativeEvent.target.offsetWidth;
        // let parentHeight = event.nativeEvent.target.offsetHeight;
        let parentXCoord = event.nativeEvent.target.offsetParent.offsetLeft;
        let parentYCoord = event.nativeEvent.target.offsetParent.offsetTop;

        let middleX = parentXCoord + (parentWidth / 2) - (tooltipWidth / 2);
        let middleY = parentYCoord - tooltipHeight;

        let keepExtended = !(extendedTooltipId === tooltip._id);
        if (!keepExtended) {
            this.setState({
                extendedTooltipId: '',
            });
        } else {
        }

        this.setState({
            isExtended: keepExtended,
            xCoord: middleX,
            yCoord: middleY,
        });
    }

    private measureTooltip(event: any) {
        let { isReadyForDisplay } = this.state;

        this.setState({
            tooltipHeight: event.nativeEvent.layout.height,
            tooltipWidth: event.nativeEvent.layout.width,
            isReadyForDisplay: !isReadyForDisplay,
        });
    }

    private getTooltipTriggerArea(targetedAt: string): JSX.Element {

        let resultTooltipArea: JSX.Element;

        switch (targetedAt) {
            /** Text Tooltip */
            case 'text':
                let { label } = this.props;

                resultTooltipArea =
                    <Label onPress={(event) => this.handleLabelClick(event)}>
                        <Text style={{ fontSize: font.$size12 }}>{label}</Text>
                    </Label>;
                break;
            /** Image Tooltip */
            case 'image':
                resultTooltipArea =
                   <Text>Hello</Text>;
                break;
            default:
                resultTooltipArea = <View style={{ backgroundColor: 'red' }} />;

        }

        return resultTooltipArea;
    }

    private getTooltipContent(targetedAt: string): string {

        let resultContent = '';

        switch (targetedAt) {
            case 'text':
                let { tooltip } = this.props;
                resultContent = tooltip.content;
                break;
            case 'image':
                let { tooltipImg } = this.props;
                resultContent = tooltipImg.content;
                break;
            default:

        }

        return resultContent;
    }
}

        // require('./tooltip.scss');
// export const Tooltip: React.StatelessComponent<Props> = (props: Props) => {

    // let { label, content/*, isVisible, role, goToReadMore*/ } = props;
    // let cssRoleClass = TOPIC_TOOLTIPS_ROLES[role] ? TOPIC_TOOLTIPS_ROLES[role].toLowerCase() : '';

    // return (
    //     <Tooltip>
    //         <Text>{label}</Text>
    //         <View>
    //             <Text>{label}</Text>
    //             <Text>{content}</Text>
    //             <Text>Read More</Text>
    //             {/* <ArrowDown /> */}
    //             {/* <Target /> */}
    //         </View>
    //     </Tooltip>
    // );
    // return (
    //     <div className={`tooltip ${cssRoleClass} ${isVisible ? `active` : ``}`} onClick={goToReadMore}>
    //         {label}
    //         <div className='panel'>
    //             <div className='title'>{label}</div>
    //             <div className='content'>{content}</div>
    //             <div className='link button on-dark'>Read More</div>
    //             <div className='arrow-down'/>
    //             <div className='target'/>
    //         </div>
    //     </div>
    // );

// };