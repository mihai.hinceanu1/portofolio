import * as samples from './switch-button.sample';
// import * as utils from './icon.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { SwitchButton } from '../switch-button';
import { CodeEditor } from '../../code-editor/code-editor';
import * as React from 'react';

interface State {
    switchState: boolean;
}

export class SwitchButtonPage extends ScrollableDocPage<State> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                switchState: false,
            }
        }
    }

    public renderDemoPage() {
        const { width, overrides } = this.state;
        const { switchState } = overrides;

        return (
            <>
                <div.Overview data-cy='button-page' width={width}>
                    <div.OverviewTitle>
                        Switch Button
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        The Switch Button is usually used to send a signal to another switching device to change it's
                        state from "off" to "on" or the reverse.
                    </div.OverviewDescription>
                </div.Overview>

                {/* Demo with switch button off */}
                <div.Demo data-cy='switch-button-off' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Switch Button Off
                        </div.DemoTitle>

                        <SwitchButton config={{ default: false }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.disabledButton} />
                </div.Demo>

                {/* Demo with switch button on */}
                <div.Demo data-cy='switch-button-on' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Switch Button On
                        </div.DemoTitle>

                        <SwitchButton config={{ default: true }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.enabledButton} />
                </div.Demo>

                {/* Demo with styled switch button */}
                <div.Demo data-cy='switch-button-style' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Switch Button with overrides style
                        </div.DemoTitle>

                        <SwitchButton
                            config={{ default: false }}
                            overrides={{
                                sliderColor: 'red',
                                switchColor: 'yellow'
                            }} />
                    </div.DemoArea>

                    <div.DemoDescription>Press the button and say wooooaa ... </div.DemoDescription>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.styledButton} />
                </div.Demo>

                {/* Demo with switch button callback */}
                <div.Demo data-cy='switch-button-test' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Switch Button Callback
                        </div.DemoTitle>

                        <SwitchButton
                            config={{
                                default: switchState,
                                onPress: (e: boolean) => this.toggleSwitch(e)
                            }} />

                        <div.DemoDescription>Press the button.</div.DemoDescription>
                    </div.DemoArea>

                    {
                        (switchState) &&

                        <div.OverviewDescription>
                            The Switch is ON
                            </div.OverviewDescription>
                    }

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.callbackButton} />
                </div.Demo>
            </>
        )
    }

    private toggleSwitch(currentState: boolean) {
        this.setState({ overrides: { switchState: !currentState } });
    }
}