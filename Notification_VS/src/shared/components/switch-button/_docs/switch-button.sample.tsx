export const disabledButton = `
let disabledSwitchButton = {
    default: false,
    // onPress is not mandatory
    onPress?: () => console.log('Switch is off')
};

<SwitchButton config={disabledSwitchButton} />
`;

export const enabledButton = `
let enabledSwitchButton = {
    default: true,
    // onPress is not mandatory
    onPress?: () => console.log('Switch is on')
};

<SwitchButton config={enabledSwitchButton} />
`;

export const styledButton = `
let configSwitchButton = {
    default: false,
    onPress?: () => console.log('Switch is on')
}

let override = {
    sliderColor: 'red',
    switchColor: 'yellow'
}

<SwitchButton config={configSwitchButton} overrides={override}/>
`;

export const callbackButton = `
let enabledSwitchButton = {
    default: true,
    // onPress is not mandatory
    onPress?: (currentState) => 
    this.setState({ overrides: {switchState: !currentState }});
};

<SwitchButton config={callbackButton} />
`;