import React from 'react';
import * as div from './switch-button.style';
import { SwitchButtonConfig, Overrides } from '../../interfaces/switch-button';
import { colors } from '../../style/colors';

interface Props {
    config: SwitchButtonConfig;
    overrides?: Overrides;
}

interface State {
    enabled: boolean;
    sliderColor: string;
    switchColor: string;
}

export class SwitchButton extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            enabled: this.props.config.default,
            sliderColor: null,
            switchColor: null
        }

    }

    render() {
        const { enabled, sliderColor, switchColor } = this.state;

        return (
            <div.Switch
                enabled={enabled}
                sliderColor={sliderColor}
                onPress={() => this.toggleSwitch()}
                data-cy='switch'>

                <div.Point 
                    onPress={() => this.toggleSwitch()}
                    switchColor={switchColor} 
                    data-cy='point'></div.Point>
            </div.Switch>
        );
    }

    componentDidMount() {
        this.setDefault();
    }

    // Beacause overrides is not mandatory, here is a methode to set style by default,
    // or overrides with given styles
    private setDefault() {
        if (!this.props.overrides) {
            this.setState({
                sliderColor: colors.$blue, 
                switchColor: colors.$white
            });
        } else {
            this.setState({
                sliderColor: this.props.overrides.sliderColor,
                switchColor: this.props.overrides.switchColor
            })
        }
    }

    private toggleSwitch() {
        const { enabled } = this.state;
        const { config } = this.props;
        this.setState({ enabled: !enabled });

        // onPress is not mandatory, so this condition bypass undefined error
        if (!config.onPress) return;

        config.onPress(enabled);

    }
};