import styled from 'styled-components/native';
import { colors } from '../../style/colors';

interface SwitchProps {
    enabled: boolean;
    sliderColor: string;
}

interface PointProps {
    switchColor: string;
}

export const Switch = styled.TouchableOpacity<SwitchProps>`
    width: 40px;
    height: 26px;
    background-color: ${props => props.enabled ? props.sliderColor : colors.$darkGrey};
    border-radius: 13px;
    padding: 0 2px;
    display: flex;
    justify-content: center;
    align-items: ${props => props.enabled ? 'flex-end' : 'flex-start'};
`;

export const Point = styled.TouchableOpacity<PointProps>`
    width: 21px;
    height: 21px;
    border-radius: 10px;
    background-color: ${props => props.switchColor ? props.switchColor : colors.$white};
`;