import styled from 'styled-components/native';

export const Slider = styled.View`
    margin-left: 10px;
    margin-right: 10px;
    width: 300px;
    align-items: stretch;
    justify-content: center;
`;

export const SliderHead = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

export const MinValue = styled.Text`
    align-self: flex-start;
`;

export const MaxValue = styled.Text`
    align-self: flex-end;
`;

export const UnitValue = styled.View`
    flex-direction: row;
    align-self: center;
`;