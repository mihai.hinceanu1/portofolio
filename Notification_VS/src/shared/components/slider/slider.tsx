import * as div from './slider.style';
import * as React from 'react';
import { Text } from 'react-native';

interface Props {
    /** min value of the slider */
    min: number;

    /** max value of the slider */
    max: number;

    /** how many steps it makes between min and max value at a time */
    step: number;

    /** function which executes when you drag the slider */
    onSliderChange: (value: number) => void;

    /** the selected value of the slider */
    value: number;

    /** unit of measure written below the slider */
    unit: string;
}

interface State {
    value: number;
}

/*
* Used to choose a value between a min and a max(value)
* unit-type of the value($, %, etc)
* step-value growth(0.5 -> 1, 1.5, 2, 2.5 ...)
*/
export class Slider extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            value: null,
        };
    }

    public render() {
        const { min, max, /* step, onSliderChange, */ unit, value } = this.props;

        return (
            <div.Slider data-cy='slider'>

                <div.SliderHead>
                    <div.MinValue data-cy='min-value'>
                        {this.swap(min, max, 'min')}
                    </div.MinValue>

                    <div.MaxValue data-cy='max-value'>
                        {this.swap(min, max, 'max')}
                    </div.MaxValue>
                </div.SliderHead>

                {/* <Slider data-cy='slide'
                    value={value || this.swap(min, max, 'min')}
                    step={step}
                    maximumValue={this.swap(min, max, 'max')}
                    minimumValue={this.swap(min, max, 'min')}
                    thumbTintColor='#6699CC'
                    minimumTrackTintColor='#6699CC'
                    onValueChange={(value: any) => onSliderChange(value)} /> */}

                <div.UnitValue>
                    <Text data-cy='value'>{value || this.swap(min, max, 'min')}</Text>
                    <Text data-cy='unit'>{unit}</Text>
                </div.UnitValue>
            </div.Slider>
        );
    }

    private swap(min: number, max: number, type: string) {
        let maxValue = 0;
        let minValue = 0;

        if (min > max) {
            maxValue = min;
            minValue = max;
        } else {
            maxValue = max;
            minValue = min;
        }

        return type === 'min' ? minValue : maxValue;
    }
}
