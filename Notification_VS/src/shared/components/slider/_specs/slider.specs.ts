/// <reference types="cypress" />

describe('Slider Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/slider');
    });

    it('Check If Slider Loader', () => {
        cy.get('[data-cy=slider]').eq(0).should('exist');
    });

    it('Check Min And Max Value', () => {
        cy.get('[data-cy=slider]').eq(0);
        cy.get('[data-cy=min-value]').eq(0).should(div => {
            const text = div.text();
            expect(text).to.eq('0');
        });

        cy.get('[data-cy=max-value]').eq(0).should(div => {
            const text = div.text();
            expect(text).to.eq('100');
        });
    });

    it('Checks the given unit', () => {
        cy.get('[data-cy=unit]').eq(1).should(div => {
            const text = div.text();
            expect(text).to.eq('$');
        });
    });

    it('Range is correct', () => {
        cy.get('[data-cy=min-value]').eq(2).contains(0);
        cy.get('[data-cy=max-value]').eq(2).contains(1000);
    });

    it('Min and max values are inverted', () => {
        cy.get('[data-cy=min-value]').eq(3).contains(-30);
        cy.get('[data-cy=max-value]').eq(3).contains(200);
    });
});