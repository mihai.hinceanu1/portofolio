export const DEFAULT = `<GenericSlider
    min={0}
    max={100}
    step={1}
    value={firstSlider}
    unit={'$'}
    onSliderChange={(value: number) => this.onSliderChange(value)} />
`;

export const INTERVAL = `<GenericSlider
    // Choose the range
    min={0}
    max={1000}
    step={1}
    value={secondSlider}
    unit={'$'}
    onSliderChange={(value: number) => this.onSliderChange(value)} />
`;

export const STEP_UNIT = `<GenericSlider
    // Choose the range
    min={0}
    max={1000}
    // How many steps on one drag
    step={1}
    value={thirdSlider}
    // Measurement unit
    unit={'%'}
    onSliderChange={(value: number) => this.onSliderChange(value)} />
`;

export const INVERTED_VALUES = `<GenericSlider
    // Min range is greater than max range
    min={200}
    max={-30}
    // Positive step
    step={1}
    value={fourthSlider}
    unit={'pinguini'}
    onSliderChange={(value: number) => this.onSliderChange(value, 'fourthSlider')} />
`;
