import * as samples from './slider.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Slider } from '../slider';
import * as React from 'react';

interface SliderState {
    firstSlider: number;
    secondSlider: number;
    thirdSlider: number;
    fourthSlider: number;
}

export class SliderPage extends ScrollableDocPage<SliderState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                firstSlider: 0,
                secondSlider: 0,
                thirdSlider: 0,
                fourthSlider: 0,
            },
        };
    }

    public renderDemoPage() {
        const { width } = this.state;
        const {
            firstSlider,
            secondSlider,
            thirdSlider,
            fourthSlider,
        } = this.state.overrides;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Slider
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Slider is used to select a particular value from a given range.
                        You can choose each step value , the type of unit you want to select, and the range.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Slider
                        </div.DemoTitle>

                        <Slider min={0}
                            max={100}
                            step={1}
                            value={firstSlider}
                            unit={'$'}
                            onSliderChange={(value: number) => this.onSliderChange(value, 'firstSlider')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Interval Changes */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Generic Slider - Interval Changes
                        </div.DemoTitle>

                        <Slider min={0}
                            max={1000}
                            step={1}
                            value={secondSlider}
                            unit={'$'}
                            onSliderChange={(value: number) => this.onSliderChange(value, 'secondSlider')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.INTERVAL} />
                </div.Demo>

                {/** Step and unit */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Generic Slider - step and unit
                        </div.DemoTitle>

                        <Slider min={0}
                            max={1000}
                            step={5}
                            value={thirdSlider}
                            unit={'%'}
                            onSliderChange={(value: number) => this.onSliderChange(value, 'thirdSlider')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.STEP_UNIT} />
                </div.Demo>

                {/** Inverted Values */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Generic Slider - values are inverted
                        </div.DemoTitle>

                        <Slider min={200}
                            max={-30}
                            step={1}
                            value={fourthSlider}
                            unit={'pinguini'}
                            onSliderChange={(value: number) => this.onSliderChange(value, 'fourthSlider')}
                        />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.INVERTED_VALUES} />
                </div.Demo>
            </>
        );
    }

    private onSliderChange(value: number, order: string) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                [order]: value,
            },
        });
    }
}