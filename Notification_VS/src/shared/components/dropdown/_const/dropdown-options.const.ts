import { IDropdownOption } from '../../../interfaces/dropdown';

export const DROPDOWN_WIDTH = 150;

/** Sample options */
export const dropdownOptions: IDropdownOption[] = [
    {
        _id: 'alien',
        label: 'Alien'
    },
    {
        _id: 'fork',
        label: 'Fork'
    },
    {
        _id: 'cup',
        label: 'Cup'
    },
    {
        _id: 'dog',
        label: 'Dog'
    },
    {
        _id: 'human',
        label: 'Human'
    },
];