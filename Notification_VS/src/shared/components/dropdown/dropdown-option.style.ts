import { DROPDOWN_TYPE } from '../../constants/dropdown.const';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

interface DropdownProps {
    type?: DROPDOWN_TYPE;
    color?: string;
    isSelected?: boolean;
}

export const Label = styled.Text<DropdownProps>`
    text-align: left;
    font-size: ${font.$size16}px;
    font-weight: 600;
    color: ${props => getLabelColor(props.color, props.isSelected)};
`;

export const DropdownOption = styled.TouchableOpacity`
    z-index: 100;
    padding: 5px;
    display: flex;
    flex-direction: column;
`;

// ====== UTILS ======

const getLabelColor = (fill: string, isSelected: boolean) => {
    if (fill && isSelected) {
        return fill;
    } else if (!fill && isSelected) {
        return '#0D8EF1';
    } else {
        return 'rgb(140,140,140);';
    }
};