import * as div from './dropdown.style';
import { getDropdownByType, getOptionsList } from './dropdown.utils';
import { DropdownConfig, IDropdownOption } from '../../interfaces/dropdown';
import { addAppOverlay, removeAppOverlay } from '../../services/app-overlays.service';
import * as React from 'react';
import { NativeComponent } from 'react-native';

interface Props {
    config: DropdownConfig;
    overrides?: {
        root?: string;
    };
}

interface State {
    expanded: boolean;
}

/**
 * Renders a button which expands a menu.
 * Callbacks can be defined for the dropdown button and each option.
 * Has three types of looks - only icon - input - classic.
 * Custom icon for stem can be defined.
 *
 * <!> Options list is rendered dynamically on top of the UI.
 * - Android ignored taps on children outside of parent area
 * - Collision detection needs freedom to place the list arbitrary on screen
 *
 * TODO - Update options position on page scroll
 * TODO - Show selected value on dropdown
 */
export class Dropdown extends React.Component<Props, State> {

    private optionsList: JSX.Element;
    private dropdown: NativeComponent;

    constructor(props: Props) {
        super(props);
        this.state = {
            expanded: false,
        };
    }

    public render() {
        const { config, overrides } = this.props;

        return (
            <div.Dropdown data-cy='dropdown'
                collapsable={false}
                type={config.type}
                overrides={overrides && overrides.root}
                ref={view => { this.dropdown = view; }}>

                {/* Button */}
                {getDropdownByType(config, () => this.toggleExpandMenu())}

            </div.Dropdown>
        );
    }

    public componentWillUnmount() {
        this.hideOptionsList();
    }

    private toggleExpandMenu() {
        const { expanded } = this.state;
        const _expanded = !expanded;

        this.toggleOptionsList(_expanded);
        this.setState({ expanded: _expanded });
    }

    /** Render the options list programmatically on top of the app */
    private toggleOptionsList(expanded: boolean) {
        const { config } = this.props;

        if (expanded) {
            this.showOptionsList(config);
        } else {
            this.hideOptionsList();
        }
    }

    private showOptionsList(config: DropdownConfig) {
        this.dropdown.measure((_fx, _fy, width, _height, px, py) => {
            const { side } = this.props.config;
            const callback = (option: IDropdownOption) => this.onPressDropdownItem(option);
            this.optionsList = getOptionsList(config, callback, side, width, px, py);
            addAppOverlay(this.optionsList);
        });
    }

    private hideOptionsList() {
        removeAppOverlay(this.optionsList);
        delete this.optionsList;
    }

    private onPressDropdownItem(option: IDropdownOption) {
        const { callback } = this.props.config;

        /** If the option has a callback set, call it. */
        if (!!option && !!option.callback) {
            option.callback();
        }

        callback && callback(option);
        this.toggleExpandMenu();
    }
}
