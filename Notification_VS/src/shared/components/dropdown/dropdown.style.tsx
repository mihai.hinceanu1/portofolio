import { DEFAULT_ICON } from './_assets/dropdown.assets';
import { DROPDOWN_WIDTH } from './_const/dropdown-options.const';
import { DROPDOWN_OPTION_SIDE, DROPDOWN_TYPE } from '../../constants/dropdown.const';
import { pathBoundingBox } from '../../services/raphael.utils';
import { font } from '../../style/font-sizes';
import { BOX_SHADOW_CSS, BOX_SHADOW_NATIVE } from '../../style/shadow.style';
import * as React from 'react';
import { Platform } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

const isMobile = Platform.OS !== 'web';

// ====== PROPS ======

interface DropdownProps {
    type?: DROPDOWN_TYPE;
    color?: string;
    isSelected?: boolean;
    side?: DROPDOWN_OPTION_SIDE;
    width?: number;
    xOffset?: number;
    yOffset?: number;
}

interface Overrides {
    overrides: string;
}

// ====== STYLES ======

export const Dropdown = styled.View<DropdownProps & Overrides>`
    width: ${DROPDOWN_WIDTH}px;
    flex-direction: column;
    justify-content: space-between;
    align-self: flex-start;
    background-color: white;
    width: auto;
    overflow: visible;
    ${props => props.overrides}
`;

export const DropdownOptions = styled.View<DropdownProps>`
    position: absolute;
    background-color: white;
    width: ${DROPDOWN_WIDTH}px;
    top: ${props => getMarginTop(props.type) + (props.yOffset || 0)}px;
    left: ${props => getOptionsLeftPosition(props)}px;
    ${isMobile ? BOX_SHADOW_NATIVE : BOX_SHADOW_CSS};
    overflow: visible;
`;

export const DropdownIcon = styled.View<DropdownProps>`
    height: 100%;
    width: 30px;
    align-items: center;
    justify-content: center;
    ${props => props.type === DROPDOWN_TYPE.OnlyIcon && getBoxShadow(props.type)};
`;

export const SelectedValue = styled.Text<DropdownProps>`
    /* <!>Stop combining percentages with fixed dimensions! */
    /* width: 85%; */
    flex-grow: 1;
    font-size: ${font.$size16}px;
    font-weight: 500;
    color: ${props => getPathFill(props.color)};
    text-align: left;
`;

export const DropdownButton = styled.TouchableOpacity<DropdownProps>`
    width: 100%;
    height: 45px;
    display: flex;
    flex-direction: row;
    padding: 5px;
    align-items: center;
    ${props => getBoxShadow(props.type)};
    ${props => getBorder(props.type)}
    ${props => getJustifyStyle(props.type)};
`;

// ====== UTILS ======

const getJustifyStyle = (type: DROPDOWN_TYPE) => {
    return DROPDOWN_TYPE.OnlyIcon === type ? 'justify-content: flex-end;' : 'justify-content: space-between;';
};

const getMarginTop = (type: DROPDOWN_TYPE): number => {
    return type === DROPDOWN_TYPE.OnlyIcon ? 60 : 45;
};

function getOptionsLeftPosition(props: DropdownProps): number {
    if (props.side === DROPDOWN_OPTION_SIDE.LEFT && props.xOffset && props.width) {
        return props.xOffset - DROPDOWN_WIDTH + props.width;
    }

    if (props.xOffset) {
        return props.xOffset;
    }

    return 0;
}

var getPath = (svgPath: string) => {
    if (svgPath) {
        return svgPath;
    } else {
        return DEFAULT_ICON;
    }
};

const getPathFill = (fill: string) => {
    if (fill) {
        return fill;
    } else {
        return '#0D8EF1';
    }
};

const getBoxShadow = (type: DROPDOWN_TYPE) => {
    if (![DROPDOWN_TYPE.OnlyInput, DROPDOWN_TYPE.OnlyIcon].includes(type)) {
        if (isMobile) {
            return BOX_SHADOW_NATIVE;
        } else {
            return BOX_SHADOW_CSS;
        }
    }
};

const getBorder = (type: DROPDOWN_TYPE) => {
    return DROPDOWN_TYPE.OnlyInput === type && 'border-style: solid; border-width: 0.1px; border-color: rgba(0,0,0, 0.2);';
};

// ====== SVG ======

export const dropdownIcon = (svgPath: string, fill: string): JSX.Element => {
    const path = getPath(svgPath);
    let boundingBox = pathBoundingBox(path);
    const { height, width } = boundingBox;

    return (
        <Svg data-cy='dropdown-svg'
            width={width + 1}
            height={height + 1}
            fill='none' style={{ backgroundColor: 'none', justifyContent: 'center', alignItems: 'center' }} >
            <Path data-cy='dropdown-svg-path'
                d={path} fill={getPathFill(fill)} />
        </Svg>
    );
};