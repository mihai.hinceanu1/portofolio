import { CUSTOM_ICON, DROPDOWN_ARROW } from '../_assets/dropdown.assets';
import { dropdownOptions } from '../_const/dropdown-options.const';
import { DROPDOWN_OPTION_SIDE, DROPDOWN_TYPE } from '../../../constants/dropdown.const';
import { DropdownConfig } from '../../../interfaces/dropdown';
import { colors } from '../../../style/colors';

export const standard: DropdownConfig = {
    options: dropdownOptions,
    icon: {
        svgData: DROPDOWN_ARROW,
        fill: colors.$blue,
    },
};

export const options: DropdownConfig = {
    options: dropdownOptions,
};

export const preselectedOption: DropdownConfig = {
    options: dropdownOptions,
    selected: {
        _id: 'alien',
        label: 'Alien',
    }
};

export const onlyIconConfig: DropdownConfig = {
    options: dropdownOptions,
    type: DROPDOWN_TYPE.OnlyIcon,
};

export const inputConfig: DropdownConfig = {
    options: dropdownOptions,
    type: DROPDOWN_TYPE.OnlyInput,
};

export const customIcon: DropdownConfig = {
    options: dropdownOptions,
    icon: {
        svgData: CUSTOM_ICON,
        fill: '#000000',
    },
};

export const getCallbackConfig = (callback: () => void): DropdownConfig => ({
    options: dropdownOptions,
    callback: callback,
});

export const placeholderConfig: DropdownConfig = {
    options: dropdownOptions,
    placeholder: 'Select Preferred Object',
};

export const OPTIONS_SIDE: DropdownConfig = {
    options: dropdownOptions,
    side: DROPDOWN_OPTION_SIDE.LEFT,
};

export const OVERRIDE = {
    config: standard,
    overrides: {
        root: 'background-color: green;'
    }
};