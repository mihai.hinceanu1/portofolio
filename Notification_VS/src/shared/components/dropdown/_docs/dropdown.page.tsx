import * as config from './dropdown.page.utils';
import * as samples from '../_docs/dropdown.page.const';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Dropdown } from '../dropdown';
import * as React from 'react';

interface DropdownState {
    numOfCallbacks: number;
}

export class DropdownPage extends ScrollableDocPage<DropdownState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                numOfCallbacks: 0,
            },
        };
    }

    public renderDemoPage() {
        const { width } = this.state;
        const { numOfCallbacks } = this.state.overrides;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Dropdown
                        </div.OverviewTitle>

                    <div.OverviewDescription>
                        Dropdown menu is made in order to ease the creation
                        of a basic dropdown list, and has some extra features
                        than a classic one.
                        </div.OverviewDescription>
                </div.Overview>

                {/** Standard Dropdown */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='standard'>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <Dropdown config={config.standard} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.STANDARD} />
                </div.Demo>

                {/* Options */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='options'>
                        <div.DemoTitle>
                            Options
                        </div.DemoTitle>

                        <Dropdown config={config.options} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.OPTIONS} />
                </div.Demo>

                {/* Preselected Option */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='preselected-option'>
                        <div.DemoTitle>
                            Preselect option
                        </div.DemoTitle>

                        <Dropdown config={config.preselectedOption} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.PRESELECTED_OPTION} />
                </div.Demo>

                {/** Only Icon */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='only-icon'>
                        <div.DemoTitle>
                            Only Icon
                        </div.DemoTitle>

                        <Dropdown config={config.onlyIconConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.ONLY_ICON} />
                </div.Demo>

                {/** Only Input */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='input'>
                        <div.DemoTitle>
                            Only Input
                        </div.DemoTitle>

                        <Dropdown config={config.inputConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.ONLY_INPUT} />
                </div.Demo>

                {/** Customizable Icon dropdown */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='customizable-icon'>
                        <div.DemoTitle>
                            Customizable icon
                        </div.DemoTitle>

                        <Dropdown config={config.customIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_ICON} />
                </div.Demo>

                {/** Callback */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='callback'>
                        <div.DemoTitle>
                            Callback function
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='callback-result'>
                            You selected an item: {numOfCallbacks} times
                        </div.CallbackResult>

                        <Dropdown config={config.getCallbackConfig(this.incrementNumOfCallbacks.bind(this))} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CALLBACK} />
                </div.Demo>

                {/* Placeholder */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='placeholder'>
                        <div.DemoTitle>
                            Placeholder
                        </div.DemoTitle>

                        <Dropdown config={config.placeholderConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.PLACEHOLDER} />
                </div.Demo>

                {/** Options side */}
                <div.Demo width={width}>
                    <div.DemoArea data-cy='options-side'>
                        <div.DemoTitle>
                            Options side
                        </div.DemoTitle>

                        <Dropdown config={config.OPTIONS_SIDE} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OPTIONS_SIDE} />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea data-cy='override'>
                        <div.DemoTitle>
                            Override
                        </div.DemoTitle>

                        <Dropdown {...config.OVERRIDE} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDE} />
                </div.Demo>
            </>
        );
    }

    public incrementNumOfCallbacks() {
        const { numOfCallbacks } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                numOfCallbacks: numOfCallbacks + 1,
            },
        });
    }
}