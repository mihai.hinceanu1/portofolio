export const STANDARD = `
<Dropdown config={config}/>

const config: DropdownConfig = {
    options: options
};`;

export const OPTIONS = `
const config: DropdownConfig = {
    options: [
        {
            _id: 'car',
            title: 'Car'
        },
        {
            _id: 'alien',
            title: 'Alien'
        },
        {
            _id: 'fork',
            title: 'Fork'
        },
        {
            _id: 'cup',
            title: 'Cup'
        },
        {
            _id: 'dog',
            title: 'Dog'
        },
        {
            _id: 'human',
            title: 'Human'
        },
    ],
};`;

export const PRESELECTED_OPTION = `
const config: DropdownConfig = {
    selected: {
        _id: 'dog',
        title: 'Dog',
    },
};`;

export const ONLY_ICON = `
const config: DropdownConfig = {
    type: TYPE.OnlyIcon,
};`;

export const ONLY_INPUT = `
const config: DropdownConfig = {
    type: TYPE.Input,
};`;

export const CUSTOM_ICON = `
const config: DropdownConfig = {
    icon: {
        svgData: CUSTOM_SVG,
        fill: '#000000',
    },
};`;

export const CALLBACK = `
const config: DropdownConfig = {
    onPress: () => customFunction(),
};`;

export const PLACEHOLDER = `
const config: DropdownConfig = {
    placeholder: 'Placeholder',
};`;

export const OPTIONS_SIDE = `
const config: DropdownConfig = {
    side: DROPDOWN_OPTIONS_SIDE.LEFT,
};`;

export const OVERRIDE = `
const dropdownProps = {
    config: {
        options: options,
    },
    overrides: {
        root: 'background-color: green;'
    }
}
`;