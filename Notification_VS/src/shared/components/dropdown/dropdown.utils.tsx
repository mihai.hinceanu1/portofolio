import { DropdownOption } from './dropdown-option';
import * as div from './dropdown.style';
import { DROPDOWN_OPTION_SIDE, DROPDOWN_TYPE } from '../../constants/dropdown.const';
import { DropdownConfig, IDropdownOption } from '../../interfaces/dropdown';
import * as React from 'react';

/**
 * The options list is rendered programmatically on top of the app.
 * Fixes issue with android not clicking on children outside of parent boundaries.
 */
export const getOptionsList = (
    config: DropdownConfig,
    callback: (item: IDropdownOption) => void,
    side: DROPDOWN_OPTION_SIDE,
    width: number,
    xOffset: number,
    yOffset: number
): JSX.Element => {
    const { options, icon, selected, type } = config;
    const { fill } = icon || {};

    return (
        <div.DropdownOptions data-cy='dropdown-options'
            type={type}
            side={side}
            width={width}
            yOffset={yOffset}
            xOffset={xOffset}>
            {
                options && options.map((option: IDropdownOption) =>
                    <DropdownOption
                        key={option._id}
                        option={option}
                        callback={() => callback(option)}
                        fill={fill}
                        selected={selected} />)
            }
        </div.DropdownOptions>
    );
};

export const getDropdownByType = (config: DropdownConfig, callback: () => void) => {
    let type: DROPDOWN_TYPE = config.type || DROPDOWN_TYPE.Standard as DROPDOWN_TYPE;

    switch (type) {

        case DROPDOWN_TYPE.OnlyInput:
            return getInputButton(config, callback);

        case DROPDOWN_TYPE.Standard:
            return getStandardDropdown(config, callback);

        case DROPDOWN_TYPE.OnlyIcon:
            return getOnlyIcon(config, callback);

        default:
            break;
    }
};

// ====== PRIVATE ======

const getInputButton = (config: DropdownConfig, callback: () => void) => {
    const { fill, svgData } = config.icon || {};

    return (
        <div.DropdownButton data-cy='dropdown-button' onPress={() => callback()} type={config.type}>
            <div.SelectedValue data-cy='selected-value' color={fill}>
                {
                    getSelectedItemLabel(config.selected, config.placeholder)
                }
            </div.SelectedValue>
            <div.DropdownIcon data-cy='dropdown-icon' type={config.type} >
                {
                    div.dropdownIcon(svgData, fill)
                }
            </div.DropdownIcon>
        </div.DropdownButton>
    );
};

const getStandardDropdown = (config: DropdownConfig, callback: () => void) => {
    const { fill, svgData } = config.icon || {};

    return (
        <div.DropdownButton data-cy='dropdown-button' onPress={() => callback()} type={config.type}>
            <div.SelectedValue data-cy='selected-value' color={fill}>
                {
                    getSelectedItemLabel(config.selected, config.placeholder)
                }
            </div.SelectedValue>
            <div.DropdownIcon data-cy='dropdown-icon' type={config.type} >
                {
                    div.dropdownIcon(svgData, fill)
                }
            </div.DropdownIcon>
        </div.DropdownButton>
    );
};

const getOnlyIcon = (config: DropdownConfig, callback: () => void) => {
    const { fill, svgData } = config.icon || {};

    return (
        <div.DropdownButton data-cy='dropdown-button' onPress={() => callback()} type={config.type}>
            <div.DropdownIcon data-cy='dropdown-icon' type={config.type} >
                {
                    div.dropdownIcon(svgData, fill)
                }
            </div.DropdownIcon>
        </div.DropdownButton>
    );
};

const getSelectedItemLabel = (selectedItem: IDropdownOption, placeholder: string): string => {
    return selectedItem && selectedItem.label !== null ? selectedItem.label : placeholder;
};