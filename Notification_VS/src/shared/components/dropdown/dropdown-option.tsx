import * as div from './dropdown-option.style';
import { IDropdownOption } from '../../interfaces/dropdown';
import * as React from 'react';

interface Props {
    option: IDropdownOption;
    callback: (item: IDropdownOption) => void;
    fill: string;
    selected: IDropdownOption;
}

export const DropdownOption: React.StatelessComponent<Props> = (props: Props) => {
    const { option, callback, fill, selected } = props;

    return (
        <div.DropdownOption data-cy='option' onPress={() => callback && callback(option)}>

            {/* Label */}
            <div.Label data-cy='label' color={fill} isSelected={isSelected(option, selected)}>
                {option.label}
            </div.Label>

        </div.DropdownOption>
    );
};

function isSelected(option: IDropdownOption, selected: IDropdownOption):boolean {
    return option.label=== (selected && selected.label);
}