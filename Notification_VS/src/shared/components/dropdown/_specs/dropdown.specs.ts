/// <reference types="cypress" />

describe('Dropdown Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/dropdown');
    });

    it('Expand button is rendered', () => {
        getElem('standard', 'dropdown-button').its('length').should('eq', 1);
    });

    it('Renders Icon', () => {
        getElem('standard', 'dropdown-icon').should('exist');
    });

    it('Expands dropdown options', () => {
        getElem('standard', 'dropdown-button').click();
        cy.get('[data-cy=dropdown-options]').should('exist');
        /** Close the dropdown */
        getElem('standard', 'dropdown-button').click();
    });

    it('Renders placeholder if no value selected', () => {
        getElem('placeholder', 'dropdown-button').children().eq(0).then(div => {
            expect(div.text()).to.eq('Select Preferred Object');
        });
    });

    it('Icon Dropdown Has No Text Area', () => {
        getElem('only-icon', 'dropdown-button').children().should('have.length', 1);
    });

    it('Callback function', () => {
        getElem('callback', 'dropdown-button').click();
        getElem('dropdown-options', 'label').eq(0).click();

        cy.get('[data-cy=callback]').scrollIntoView();
        getElem('callback', 'callback-result').then(div => {
            expect(div.text()).to.eq('You selected an item: 1 times');
        });
    });

    it('Dropdown options can appear on the left side', () => {
        /** Toggle the options */
        getElem('options-side', 'dropdown-button').click();

        cy.get('[data-cy=dropdown-options]')
            .first()
            .then($option => {
                getElem('options-side', 'dropdown-button').then($button => {
                    expect($option.position().left).to.be.lt($button.position().left);
                });
            });

        /** Toggle the options */
        getElem('options-side', 'dropdown-button').click();
    });

    it('Can have overrides', () => {
        getElem('override', 'dropdown').should('have.css', 'background-color', 'rgb(0, 128, 0)');
    });
});

// ====== SELECTORS ======

function getElem(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}
