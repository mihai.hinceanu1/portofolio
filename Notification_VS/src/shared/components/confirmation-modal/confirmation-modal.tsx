import { UserConfirmStyles as styles } from './confirmation-modal.style';
import { Button } from '../button/button';
import { FitImage } from '../fit-image/fit-image';
import * as React from 'react';
import { Text, View } from 'react-native';

interface Props {
    image?: string;
    message: string;
    onNoPressed: () => void;
    onYesPressed: () => void;
}

interface State { }

// TODO Convert to stylesheet
/**
 * Requires user confirmation for each attempt
 * of deletion on any element
 * @param message question for user
 * TODO Rename to ConfirmModal
 */
export class ConfirmModal extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {

        };
    }

    public render() {
        let { message, onNoPressed, onYesPressed, image } = this.props;
        return (
            <View data-cy='confirmation-modal'
                style={styles.container}>

                {/** Image */}
                {
                    image &&
                    <View style={{ width: 50, height: 50 }}>
                        <FitImage imgPath={image} />
                    </View>
                }
                <Text data-cy='messageText' style={styles.message}>
                    {message}
                </Text>
                <View data-cy='buttons-container'
                    style={styles.buttonsContainer}>

                    {/** Yes button */}
                    <View data-cy='confirm-button'
                        style={styles.button}>
                        <Button config={{ text: 'Yes', onPress: () => onYesPressed() }} />
                    </View>

                    {/** No button */}
                    <View data-cy='noButton' style={styles.button}>
                        <Button config={{ text: 'No', onPress: () => onNoPressed() }} />
                    </View>
                </View>

            </View>
        );
    }
}
