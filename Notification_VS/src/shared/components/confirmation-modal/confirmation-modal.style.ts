import EStyleSheet from 'react-native-extended-stylesheet';

export const UserConfirmStyles = EStyleSheet.create({
    container: {
        width: '100%',
        height: 100,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    message: {
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: '600',
        color: 'white',
    },
    buttonsContainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
    },
});
