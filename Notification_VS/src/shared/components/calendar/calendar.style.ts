import styled from 'styled-components/native';

export const Calendar = styled.View`
    width: 300px;
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2);
`;

export const CalendarMonthHeader = styled.View`
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
`;

export const CalendarMonthContent = styled.View`
    flex-direction: row;
    flex-grow: 4;
    justify-content: center;
`;

export const CalendarMonthText = styled.Text`
    text-align: center;
`;

export const CalendarHeaderIcons = styled.View`
    flex-grow: 1;
`;

export const WeekdaysHeader = styled.View`
    width: 100%;
    display: flex;
    flex-flow: row nowrap;
`;

export const WeekdaysCell = styled.View`
    flex-basis: calc((100% — 2px) / 7);
    height: 44px;
    justify-content: center;
    align-items: center;
`;

export const DatesTable = styled.View`
    display: flex;
    flex-flow: row wrap;
    border-right-color: rgba(0, 0, 0, 0.1);
    border-right-style: solid;
    border-right-width: 0.5px;
    border-bottom-color: rgba(0, 0, 0, 0.1);
    border-bottom-style: solid;
    border-bottom-width: 0.5px;
    border-left-color: rgba(0, 0, 0, 0.1);
    border-left-style: solid;
    border-left-width: 0.5px;
`;

export const InactiveCell = styled.View`
    border-top-color: rgba(0, 0, 0, 0.1);
    border-top-style: solid;
    border-top-width: 0.5px;
    border-left-color: rgba(0, 0, 0, 0.1);
    border-left-style: solid;
    border-left-width: 0.5px;
    flex-basis: calc((100% — 2px) / 7);
    height: 44px;
    justify-content: center;
    align-items: center;
`;

export const InactiveCellContent = styled.Text`
    color: rgba(0, 0, 0, 0.1);
`;

export const ActiveCell = styled.TouchableOpacity`
    border-top-color: rgba(0, 0, 0, 0.1);
    border-top-style: solid;
    border-top-width: 0.5px;
    border-left-color: rgba(0, 0, 0, 0.1);
    border-left-style: solid;
    border-left-width: 0.5px;
    flex-basis: calc((100% — 2px) / 7);
    height: 44px;
    justify-content: center;
    align-items: center;
`;

export const HighlightedCell = styled.View`
    border-color: rgba(0, 0, 0, 0.6);
    border-style: solid;
    border-width: 1px;
    border-radius: 50%;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
`;

export const ActiveCellContent = styled.Text`
    color: rgba(0, 0, 0, 0.6);
    font-weight: 600;
`;