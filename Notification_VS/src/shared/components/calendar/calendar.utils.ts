export function getNextMonth(month: number, year: number) {
    let nextMonth = {
        month: month === 11 ? 0 : month + 1,
        year: month === 11 ? year + 1 : year,
    };
    return nextMonth;
}

export function getPreviousMonth(month: number, year: number) {
    let prevMonth = {
        month: month === 0 ? 11 : month - 1,
        year: month === 0 ? year - 1 : year,
    };
    return prevMonth;
}

export function getMonthFirstDay(month: number, year: number) {
    return new Date(`${year}-${padWithZero(month + 1)}-01`).getDay();
}

export function getMonthDays(month: number, year: number) {
    const months30 = [3, 5, 8, 10];
    const leapYear = (year % 400 === 0) || ((year % 4 === 0) && (year % 100 !== 0));

    return month === 1
        ? leapYear
            ? 29
            : 28
        : months30.includes(month)
            ? 30
            : 31;
}

export function padWithZero(value: number) {
    if (value < 10) {
        return '0' + value.toString();
    } else {
        return value.toString();
    }
}