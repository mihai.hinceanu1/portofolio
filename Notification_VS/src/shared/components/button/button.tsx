import * as div from './button.style';
import { getDarkerColor, getHoverIcon, getLighterColor } from './button.utils';
import { ButtonConfig } from '../../interfaces/button';
import { FitImage } from '../fit-image/fit-image';
import * as React from 'react';
import { GestureResponderEvent, LayoutChangeEvent } from 'react-native';

interface Props {
    config: ButtonConfig;
    overrides?: {
        root?: string;
        text?: string;
    };
}

interface State {
    isHovered: boolean;

    /** When tapped, the button gets a darker color derived from the button color. */
    btnColor: string;

    /** Used in styling rounded buttons to get the right border radius */
    height: number;

    /**
     * Useful for styling when holding click and cursor leaves the area of the button
     * so that elements inside the button keep the same properties and are still visible
     */
    longPress: boolean;
}

/**
 * We need a generic button component in order to maintain a consistent visual style throughout the app.
 * Several parameters can be configured:
 * - Color, border, icon, callback
 */
export class Button extends React.Component<Props, State> {
    private _isMounted: boolean = false;

    constructor(props: Props) {
        super(props);

        this.state = {
            isHovered: false,
            btnColor: null,
            height: 0,
            longPress: false,
        };
    }

    public render() {
        const { config, overrides } = this.props;
        let { isHovered, height, longPress } = this.state;
        let {
            disabled,
            color,
            width,
            text,
            icon,
            fontSize,
            fontWeight,
            filled,
            rounded,
        } = config;

        return (
            <div.Button data-cy='button'
                onPressIn={() => this.onButtonPress()}
                onPressOut={(e) => this.onButtonRelease(e)}
                onLayout={(e) => this.getButtonHeight(e)}
                onMouseEnter={() => this.onMouseEnter()}
                onMouseLeave={() => this.onMouseLeave()}
                width={width}
                height={height}
                filled={filled}
                rounded={rounded}
                iconSide={icon && icon.iconSide}
                hasIcon={icon ? true : false}
                hasText={text ? true : false}
                isHovered={isHovered}
                longPress={longPress}
                color={this.getButtonColor()}
                disabled={this.getDisabledStatus()}
                overrides={overrides && overrides.root}
                underlayColor={getDarkerColor(color)}>

                {/** Label */}
                <div.Label data-cy='label' iconSide={icon && icon.iconSide}>
                    {
                        icon &&
                        <div.Icon data-cy='icon'>
                            {this.getIcon()}
                        </div.Icon>
                    }

                    {
                        text &&
                        <div.Text data-cy='text'
                            color={color}
                            filled={filled}
                            disabled={disabled}
                            isHovered={isHovered}
                            longPress={longPress}
                            fontWeight={fontWeight}
                            overrides={overrides && overrides.text}
                            fontSize={fontSize}>
                            {text}
                        </div.Text>
                    }
                </div.Label>

            </div.Button>
        );
    }

    public componentDidMount() {
        this._isMounted = true;
    }

    public componentWillUnmount() {
        this._isMounted = false;
    }

    private onButtonPress() {
        const { color } = this.props.config;

        if (this._isMounted) {
            this.setState({
                btnColor: getDarkerColor(color),
                longPress: true,
            });
        }
    }

    private onButtonRelease(e: GestureResponderEvent) {
        const { onPress, color } = this.props.config;

        onPress(e);
        this.resetColor(color);
        this.resetPressStatus();
    }

    private getButtonHeight(e: LayoutChangeEvent) {
        this.setState({
            height: e.nativeEvent.layout.height,
        });
    }

    private onMouseEnter() {
        const { filled, color } = this.props.config;

        if (this._isMounted) {
            if (filled) {
                this.setState({
                    btnColor: getLighterColor(color),
                    isHovered: true,
                });
            }

            this.setState({ isHovered: true });
        }
    }

    private onMouseLeave() {
        const { filled, color } = this.props.config;

        if (this._isMounted) {
            if (filled) {
                this.resetColor(color);
            }

            this.setState({ isHovered: false });
        }
    }

    private resetColor(color: string) {
        if (this._isMounted) {
            this.setState({
                btnColor: color,
            });
        }
    }

    private resetPressStatus() {
        if (this._isMounted) {
            this.setState({
                longPress: false,
            });
        }
    }

    private getDisabledStatus() {
        const { disabled } = this.props.config;

        return disabled ? disabled : false;
    }

    private getButtonColor() {
        const { btnColor } = this.state;
        const { color } = this.props.config;

        return btnColor ? btnColor : color;
    }

    private getIcon(): JSX.Element {
        const { color, filled, disabled, icon } = this.props.config;
        const { isHovered, longPress } = this.state;

        switch (icon.type) {
            case 'image': {
                const { imageHover, imagePath } = icon;
                return isHovered ? <FitImage imgPath={getHoverIcon(imageHover, imagePath)} /> : <FitImage imgPath={imagePath} />;
            }
            case 'svg': {
                let { svgPath, svgHover } = icon;

                return isHovered ?
                    div.getIconSvg(getHoverIcon(svgHover, svgPath), color, isHovered, filled, disabled, longPress) :
                    div.getIconSvg(svgPath, color, isHovered, filled, disabled, longPress);
            }
            default: return;
        }
    }
}
