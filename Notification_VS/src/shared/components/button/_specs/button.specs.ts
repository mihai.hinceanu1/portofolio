/// <reference types="cypress" />

describe('Button Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/button');
        cy.get('[data-cy=button-page]').should('exist');
    });

    it('Button renders', () => {
        getEl('simple-button', 'button').should('exist');
    });

    it('Button callback is called', () => {
        getEl('callback', 'button').click();
        cy.get('[data-cy=callback-count]').should('have.text', '1');
    });

    it('Line button has a distinctive border', () => {
        getEl('simple-button', 'button').should('have.css', 'border-width', '2px');
        getEl('simple-button', 'button').should('have.css', 'border-color', 'rgb(13, 142, 241)');
    });

    it('Line button has a filled background color when hovered', () => {
        getEl('simple-button', 'button')
            .trigger('mouseover')
            .should('have.css', 'background-color', 'rgb(13, 142, 241)');
    });

    it('Can render text inside', () => {
        getEl('simple-button', 'text').should('exist');
    });

    it('Has a color effect on press', () => {
        getEl('simple-button', 'button')
            .trigger('mousedown')
            .should('have.css', 'background-color', 'rgb(10, 113, 193)');
    });

    it('Can render icon along with text', () => {
        getEl('image-icon', 'text').should('exist');
        getEl('image-icon', 'icon').should('exist');
    });

    it('Renders the correct image', () => {
        getEl('image-icon', 'icon')
            .find('img')
            .should('have.attr', 'src', 'http://192.168.0.105:8080/assets/shared/topicFavesGreen.png');
    });

    it('On hover, if no hover icon is provided, the same icon will be rendered', () => {
        getEl('image-icon', 'button')
            .trigger('mouseover')
            .find('img')
            .should('have.attr', 'src', 'http://192.168.0.105:8080/assets/shared/topicFavesGreen.png');
    });

    it('Can have different image on hover', () => {
        compareAttributeBeforeAndAfterHover('image-icon-hover', 'img', 'src', imageUrl);
    });

    it('Can render svg as icon', () => {
        getEl('svg-icon', 'icon')
            .find('path')
            .should('exist');
    });

    it('Renders the right svg', () => {
        getEl('svg-icon', 'icon')
            .find('path')
            .should('have.attr', 'd', 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z');
    });

    it('Can have different svg on hover', () => {
        compareAttributeBeforeAndAfterHover('svg-icon-hover', 'path', 'd', svgPath);
    });

    it('Can render only icon', () => {
        getEl('icon-only', 'icon').should('exist');
        getEl('icon-only', 'text').should('not.exist');
    });

    it('Can have disabled status', () => {
        getEl('icon-disabled-line-button', 'button')
            .should('have.attr', 'disabled');
    });

    it('Can have different color', () => {
        getEl('color-button', 'button')
            .should('have.css', 'border-color', 'rgb(234, 67, 53)');

        getEl('color-button', 'button')
            .trigger('mouseover')
            .should('have.css', 'background-color', 'rgb(234, 67, 53)');
    });

    it('Can have a different text size', () => {
        getEl('font-size', 'text')
            .should('have.css', 'font-size', '20px');
    });

    it('Can have a preset width', () => {
        getEl('width', 'button').should('have.css', 'width', '200px');
    });

    it('Can have icon on the right side of text', () => {
        getEl('icon-right', 'icon')
            .then($btnIcon => {
                getEl('icon-right', 'text')
                    .should($btnText => {
                        expect($btnText.position().left).to.be.lte($btnIcon.position().left);
                    });
            });
    });

    it('When no color is provided but filled flag is true, a default color fills the button', () => {
        getEl('filled-no-color-provided', 'button')
            .should('have.css', 'background-color', 'rgb(13, 142, 241)');
    });

    it('Can have rounded corners', () => {
        getEl('rounded-borders', 'button')
            .should('have.css', 'border-radius', '20px');
    });

    it('Some styles for root can be overwritten', () => {
        getEl('overrides', 'button')
            .should('have.css', 'margin', '50px');
    });
});

// ====== ASSETS ======

var svgPath = 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z';
var imageUrl = 'http://192.168.0.105:8080/assets/shared/topicFavesGreen.png';

// ====== SELECTORS ======

function getEl(useCase, element) {
    return cy.get(`[data-cy=${useCase}] [data-cy = ${element}]`);
}

// ====== COMMANDS ======

function compareAttributeBeforeAndAfterHover(useCase, element, attribute, defaultValue) {
    let previousValue;
    getEl(useCase, 'button')
        .find(element)
        .should('have.attr', attribute).and($src => {
            previousValue = $src;
            expect(previousValue).to.eq(defaultValue);
        });

    getEl(useCase, 'button')
        .trigger('mouseover')
        .find(element)
        .should('have.attr', attribute).and($hoveredSrc => {
            expect($hoveredSrc).to.not.eq(previousValue);
        });
}