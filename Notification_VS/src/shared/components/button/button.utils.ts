import { colors } from '../../style/colors';
import Color from 'color-js';

/**
 * Darkens a color by 10%
 * @param color string representing the color (TODO test if it can be
 * simply the color name, rgb, rgba or hex)
 * The color defaults to blue.
 */
export const getDarkerColor = (color: string): string => {
    return Color(color ? color : colors.$blue).darkenByAmount(0.1).toString();
};

/**
 * Lightens a color by 10%
 * @param color string representing the color
 * The color defaults to blue.
 */
export const getLighterColor = (color: string): string => {
    return Color(color ? color : colors.$blue).lightenByAmount(0.1).toString();
};

/** If no hover icon is provided, the default icon should appear */
export const getHoverIcon = (hoverIcon: string, defaultIcon: string): string =>
    hoverIcon ? hoverIcon : defaultIcon;
