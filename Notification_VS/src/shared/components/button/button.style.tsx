import { BORDER_RADIUS } from '../../../users/components/forms/_constants/forms.const';
import { ICON_SIDES } from '../../interfaces/button';
import { ReactWebAttributes } from '../../interfaces/workarounds';
import { pathBoundingBox } from '../../services/raphael.utils';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import { BOX_SHADOW_NATIVE } from '../../style/shadow.style';
import React from 'react';
import { Platform } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface Override {
    overrides: {};
}

interface ButtonProps extends ReactWebAttributes {
    color: string;
    isHovered: boolean;
    disabled: boolean;
    width: number;
    height: number;
    hasIcon: boolean;
    iconSide: ICON_SIDES;
    hasText: boolean;
    filled: boolean;
    rounded: boolean;
    longPress: boolean;
}

interface LabelProps {
    iconSide: ICON_SIDES;
}

interface TextProps {
    color: string;
    isHovered: boolean;
    disabled: boolean;
    fontSize: number;
    fontWeight: number;
    filled: boolean;
    longPress: boolean;
}

const DEFAULT_COLOR = colors.$blue;
const DISABLED_COLOR = colors.$grey;

// ====== STYLES ======

/** <!> Regarding align-self: flex-start
 * This is the equivalent of display: inline-block and
 * will make the container extend just enough to fill the content
 * if not specified otherwise.
 */
export const Button = styled.TouchableHighlight<ButtonProps & Override>`
    align-self: flex-start;
    min-height: 40px;
    margin: 10px;
    align-items: center;
    justify-content: center;
    border-radius: ${props => getBorderRadius(props)}px;
    background-color: ${props => getBackgroundColor(props)};
    ${props => getBoxShadow(props)}
    ${props => getCustomWidth(props)}
    ${props => getBorderStyle(props)}
    ${props => getButtonPadding(props)};
    ${props => props.overrides};
`;

export const Label = styled.View<LabelProps>`
    align-items: center;
    flex-direction: ${props => getFlexRowOrRowReverse(props)};
`;
// For the moment the size for the icons is hardcoded,
// maybe in the future it will adapt according to icon size
export const Icon = styled.View`
    display: flex;
    justify-content: center;
    align-items: center;
    /* width: 18px; */
    /* height: 18px; */
    margin: 0px 5px;
`;

export const Text = styled.Text<TextProps & Override>`
    text-align: center;
    margin: 0px 5px;
    color: ${props => getTextColor(props)};
    font-size: ${props => getFontSize(props)}px;
    font-weight: ${props => getFontWeight(props)};
    ${props => props.overrides}
`;

// ====== UTILS ======

let getButtonPadding = (props: ButtonProps): string => {
    const { hasText, hasIcon } = props;

    if (!hasText) {
        return 'padding: 0';
    } else {
        return hasIcon ? 'padding: 0px 10px' : 'padding: 0px 15px';
    }
};

/**
 * Rounded buttons have a thinner border than line buttons
 * and filled buttons have no border at all.
 * This method handles these cases
 */
let getBorderStyle = (props: ButtonProps): string => {
    const { rounded, filled } = props;

    if (!filled) {
        let borderColor = getBorderColor(props);
        let borderWidth = rounded ? 1 : 2;

        return `
            border-width: ${borderWidth}px;
            border-color: ${borderColor};
        `;
    }

    return '';
};

/**
 * TODO Consult with designer: buttons having only icon seem to have a bigger
 * border radius, therefore this handles that case.
 * When rounded flag is provided, the border radius is half of the button height.
 */
let getBorderRadius = (props: ButtonProps): number => {
    let { hasText, hasIcon, rounded, height } = props;

    if (rounded) {
        return height / 2;
    }

    return !hasText && hasIcon ? 5 : BORDER_RADIUS;
};

/**
 * Line buttons' borders have the color provided/default color
 * Rounded buttons that aren't filled have a grey border
 */
let getBorderColor = (props: ButtonProps): string => {
    const { color, disabled, rounded, filled, isHovered, longPress } = props;

    if (rounded && !filled) {
        return isHovered || longPress ? color : colors.$grey;
    }

    if (color) {
        return getColorByDisabledStatus(color, disabled);
    }

    // Return a default color (blue)
    return getColorByDisabledStatus(colors.$blue, disabled);
};

/**
 * Buttons shrink down to the minimal possible size.
 * A custom width can be defined
 */
function getCustomWidth(props: ButtonProps): string {
    let { width, rounded, hasText, hasIcon } = props;
    let resultWidth: string;
    let hasOnlyText = hasText && !hasIcon;
    let hasOnlyIcon = hasIcon && !hasText;

    if (rounded && hasOnlyText) {
        resultWidth = 'min-width: 140px';
    }

    if (hasOnlyIcon) {
        resultWidth = 'min-width: 40px';
    }

    if (width) {
        resultWidth = `min-width: ${width}px`;
    }

    return resultWidth;
}

/**
 * Android has a different method to describe shadows
 * Get shadow styling depending on the platform
 * The shadow appears only on filled buttons
 */
function getBoxShadow(props: ButtonProps) {
    const { filled } = props;
    let isAndroid = Platform.OS === 'android';

    if (filled) {
        return isAndroid ? BOX_SHADOW_NATIVE : `box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.4);`;
    }
}

function getBackgroundColor(props: ButtonProps): string {
    let { color, isHovered, disabled, filled } = props;

    if (disabled) {
        return DISABLED_COLOR;
    }

    return getBgrColorByHoveredOrFilledStatus(color, filled || isHovered);
}

let getFlexRowOrRowReverse = (props: LabelProps): string =>
    props.iconSide === ICON_SIDES.right ? 'row-reverse' : 'row';

function getTextColor(props: TextProps): string {
    let { disabled, isHovered, color, filled, longPress } = props;

    if (filled || disabled || longPress) {
        return colors.$white;
    }

    return getContrastColor(color, isHovered);
}

let getFontSize = (props: TextProps): number =>
    props.fontSize ? props.fontSize : font.$size14;

let getFontWeight = (props: TextProps): number =>
    props.fontWeight ? props.fontWeight : 500;

/**
 * Handles whether should return a color (provided or default)
 * or white depending on a flag
 */
let getBgrColorByHoveredOrFilledStatus = (color: string, isFilledOrHovered: boolean): string => {
    if (color) {
        return isFilledOrHovered ? color : colors.$white;
    } else {
        return isFilledOrHovered ? DEFAULT_COLOR : colors.$white;
    }
};

/** Returns the opposite color to getBgrColorByHoverOrFilledStatus */
let getContrastColor = (color: string, isFilledOrHovered: boolean): string =>
    getBgrColorByHoveredOrFilledStatus(color, !isFilledOrHovered);

let getColorByDisabledStatus = (color: string, disabled: boolean): string =>
    disabled ? DISABLED_COLOR : color;

// ====== SVG ======

export const getIconSvg = (icon: string, color: string, isHovered: boolean, filled: boolean, disabled: boolean, longPress: boolean): JSX.Element => {
    let svgColor = filled || disabled || longPress ? colors.$white : getContrastColor(color, isHovered);
    let { width, height } = pathBoundingBox(icon);

    return (
        <Svg width={width} height={height}>
            <Path d={icon} fill={svgColor} />
        </Svg>
    );
};