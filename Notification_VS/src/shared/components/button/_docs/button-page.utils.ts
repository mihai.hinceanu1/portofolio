import { ButtonConfig, ICON_SIDES } from '../../../interfaces/button';
import { colors } from '../../../style/colors';

const plus = 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z';
const minus = 'M0 8H18V2H-18V-2Z';

// ====== LINE BUTTON ======

export const simpleButton: ButtonConfig = {
    text: 'Add',
    onPress: () => console.log('Button Pressed')
};

export const imageIcon: ButtonConfig = {
    icon: {
        type: 'image',
        imagePath: '/shared/topicFavesGreen.png'
    },
    text: 'Add',
    onPress: () => console.log('Button Pressed')
};

export const imageIconHover: ButtonConfig = {
    icon: {
        type: 'image',
        imagePath: '/shared/topicFavesGreen.png',
        imageHover: '/shared/topicFavesFilledBlack.png'
    },
    text: 'Add',
    onPress: () => console.log('Button Pressed')
};

export const svgIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    text: 'Add',
    onPress: () => console.log('Button Pressed')
};

export const svgIconHover: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus,
        svgHover: minus,
    },
    text: 'Add',
    onPress: () => console.log('Button Pressed')
};

export const onlyIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    onPress: () => console.log('Button Pressed')
};

export const onlyIconDisabled: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    disabled: true,
    onPress: () => console.log('Button Pressed')
};

export const callbackPressTest: ButtonConfig = {
    text: '+1',
    onPress: () => console.log('Button Pressed')
};

export const backgroundColor: ButtonConfig = {
    text: 'Add',
    color: colors.$red,
    onPress: () => console.log('Button Pressed'),
};

export const fontSize: ButtonConfig = {
    text: 'Add',
    fontSize: 20,
    color: colors.$blue,
    onPress: () => console.log('Button Pressed'),
};

export const disabledButton: ButtonConfig = {
    text: 'Add',
    disabled: true,
    color: colors.$blue,
    onPress: () => console.log('Button Pressed'),
};

export const fixedWidth: ButtonConfig = {
    width: 200,
    text: 'Add',
    onPress: () => console.log('Button Pressed'),
};

export const iconOnRight: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus,
        iconSide: ICON_SIDES.right,
    },
    text: 'Next',
    onPress: () => console.log('Button Pressed')
};

// ====== FILLED BUTTON ======

export const filledButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const noColorFilledButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    onPress: () => console.log('Button Pressed')
};

export const filledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    text: 'Add',
    filled: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const filledDisabledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    text: 'Add',
    filled: true,
    disabled: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

// ====== ROUNDED BUTTON ======

export const roundedButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    rounded: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const roundedDisabledButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    rounded: true,
    disabled: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const roundedFilledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    filled: true,
    rounded: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const roundedFilledDisabledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    filled: true,
    rounded: true,
    disabled: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const roundedButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    rounded: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};

export const roundedDisabledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: plus
    },
    rounded: true,
    disabled: true,
    color: colors.$darkBlue,
    onPress: () => console.log('Button Pressed')
};
