// ====== LINE BUTTON ======

export const simpleButton = `
let simpleButtonConfig: ButtonConfig = {
    text: 'Add',
    // onPress is mandatory but it won't appear
    // in following samples because it's repetitive
    onPress: () => console.log('Button Pressed')
};

<Button config={simpleButtonConfig} />
`;

export const imageIcon = `
let buttonImageIcon: ButtonConfig = {
    icon: {
        // The type of the icon (can be image or svg)
        // The other properties depend on this.
        type: 'image',
        // The path toward the image
        imagePath: '/shared/topicFavesGreen.png'
    },
    text: 'Add',
};
`;

export const imageIconHover = `
let imageIconHover: ButtonConfig = {
    icon: {
        type: 'image',
        imagePath: '/shared/topicFavesGreen.png'
        // The path toward replacement image on hover.
        imageHover: '/shared/topicFavesFilledBlack.png'
    },
    text: 'Add',
};
`;

export const svgIcon = `
let onlyIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
    text: 'Add',
};
`;

export const svgIconHover = `
let onlyIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
        svgHover: 'M0 8H18V2H-18V-2Z',
    },
    text: 'Add',
};
`;

export const onlyIcon = `
let onlyIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
};
`;

export const onlyIconDisabled = `
let onlyIconDisabled: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
    // Disabled flag
    disabled: true,
};
`;

export const callbackPressTest = `
let callbackPressTest: ButtonConfig = {
    text: '+1',
    onPress: () => {
        const { callbackCount } = this.state;
        this.setState({
            callbackCount: callbackCount + 1,
        });
    }
`;

export const backgroundColor = `
let backgroundColor: ButtonConfig = {
    // The desired color for the elements inside the button.
    color: '#0D8EF1',
    text: 'Add',
};
`;

export const fontSize = `
let fontSize: ButtonConfig = {
    text: 'Add',
    // The size for the text inside button
    fontSize: 20,
}
`;

export const disabledButton = `
let disabledButton: ButtonConfig = {
    text: 'Add',
    // Disabled flag
    disabled: true,
    // You can notice that the color doesn't have any effect
    // but once the button is enabled it will have the proper color.
    color: '#0D8EF1',
};
`;

export const fixedWidth = `
let fixedWidth: ButtonConfig = {
    // The width in px
    width: 200,
    text: 'Add',
};
`;

export const iconOnRight = `
let iconOnRight: ButtonConfig = {
    text: 'Next',
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
        iconSide: ICON_SIDES.right,
    },
};
`;

// ====== FILLED BUTTON ======

export const filledButton = `
let filledButton: ButtonConfig = {
    text: 'Add',
    // This flag controls whether the button background has
    // color by default or is white and colored only when hovered
    filled: true,
    color: '#001e40',
};
`;

export const noColorFilledButton = `
let filledButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    // No color provided, so it will have the default color (blue)
};
`;

export const filledButtonIcon = `
let filledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
    text: 'Add',
    filled: true,
    color: '#001e40',
};
`;

export const filledDisabledButtonIcon = `
let filledDisabledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
    text: 'Add',
    filled: true,
    disabled: true,
    color: '#001e40',
};
`;

// ====== ROUNDED BUTTON ======

export const roundedButton = `
let roundedButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    rounded: true,
    color: '#001e40',
};
`;

export const roundedDisabledButton = `
let roundedButton: ButtonConfig = {
    text: 'Add',
    filled: true,
    rounded: true,
    disabled: true,
    color: '#001e40',
};
`;

export const roundedFilledButtonIcon = `
let roundedButtonIconFilled: ButtonConfig = {
    filled: true,
    rounded: true,
    color: '#001e40',
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
};
`;

export const roundedFilledDisabledButtonIcon = `
let roundedFilledDisabledButtonIcon: ButtonConfig = {
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
    filled: true,
    rounded: true,
    disabled: true,
    color: '#001e40',
};
`;

export const roundedButtonIcon = `
let roundedButtonIcon: ButtonConfig = {
    rounded: true,
    icon: {
        type: 'svg',
        svgPath: 'M10 0H8V8L0 8V10H8V18H10V10H18V8L10 8V0Z',
    },
};
`;

export const overrides = `
<Button config={simpleButton}
    overrides={{ root: 'margin: 50px;' }} />
`;
