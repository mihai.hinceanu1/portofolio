import * as samples from './button-page.sample';
import * as configs from './button-page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Button } from '../button';
import * as React from 'react';
import { Text } from 'react-native';

interface ButtonState {
    callbackCount: number;
}

/** Catalog with all the variation of the <Button/> component used for testing */
export class ButtonPage extends ScrollableDocPage<ButtonState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                callbackCount: 0,
            },
        };
    }

    public renderDemoPage() {
        const { width, overrides } = this.state;
        const { callbackCount } = overrides;

        return (
            <>
                <div.Overview data-cy='button-page' width={width}>
                    <div.OverviewTitle>
                        Button
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        The shared button will stretch as much as the parent component is. It is fully customizable
                        from its background color to its text color. It has special hover properties and also can
                        be disabled. It can have any custom icon you want.
                    </div.OverviewDescription>
                </div.Overview>

                {/* ====== LINE BUTTONS ====== */}
                {/** Button */}
                <div.Demo data-cy='simple-button' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Button
                        </div.DemoTitle>

                        <Button config={configs.simpleButton} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.simpleButton} />
                </div.Demo>

                {/** Callback */}
                <div.Demo data-cy='callback' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callback
                        </div.DemoTitle>

                        <Button config={{ ...configs.callbackPressTest, onPress: () => this.incrementCallbackCount() }} />

                        <Text data-cy='callback-count'>{callbackCount.toString()}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.callbackPressTest} />
                </div.Demo>

                {/** Image Icon */}
                <div.Demo data-cy='image-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Image Icon
                        </div.DemoTitle>

                        <Button config={configs.imageIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.imageIcon} />
                </div.Demo>

                {/** Image Icon Hover */}
                <div.Demo data-cy='image-icon-hover' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Image Icon Hover
                        </div.DemoTitle>

                        <Button config={configs.imageIconHover} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.imageIconHover} />
                </div.Demo>

                {/** SVG Icon */}
                <div.Demo data-cy='svg-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            SVG Icon
                        </div.DemoTitle>

                        <Button config={configs.svgIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.svgIcon} />
                </div.Demo>

                {/** SVG Icon Hover */}
                <div.Demo data-cy='svg-icon-hover' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            SVG Icon Hover
                        </div.DemoTitle>

                        <Button config={configs.svgIconHover} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.svgIconHover} />
                </div.Demo>

                {/** Only Icon */}
                <div.Demo data-cy='icon-only' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Only Icon
                        </div.DemoTitle>

                        <Button config={configs.onlyIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.onlyIcon} />
                </div.Demo>

                {/* Only Icon Disabled */}
                <div.Demo data-cy='icon-disabled-line-button' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Only Icon Disabled
                        </div.DemoTitle>

                        <Button config={configs.onlyIconDisabled} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.onlyIconDisabled} />
                </div.Demo>

                {/** Color */}
                <div.Demo data-cy='color-button' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Color
                        </div.DemoTitle>

                        <Button config={configs.backgroundColor} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.backgroundColor} />
                </div.Demo>

                {/** Font Size */}
                <div.Demo data-cy='font-size' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Font Size
                        </div.DemoTitle>

                        <Button config={configs.fontSize} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.fontSize} />
                </div.Demo>

                {/** Disabled */}
                <div.Demo data-cy='disabled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Disabled
                        </div.DemoTitle>

                        <Button config={configs.disabledButton} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.disabledButton} />
                </div.Demo>

                {/** Width */}
                <div.Demo data-cy='width' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Width
                        </div.DemoTitle>

                        <Button config={configs.fixedWidth} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.fixedWidth} />
                </div.Demo>

                {/* Icon on right */}
                <div.Demo data-cy='icon-right' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Icon on right
                        </div.DemoTitle>

                        <Button config={configs.iconOnRight} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.iconOnRight} />
                </div.Demo>

                {/* ====== FILLED BUTTONS ====== */}
                {/* Filled button */}
                <div.Demo data-cy='filled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Filled
                        </div.DemoTitle>

                        <Button config={configs.filledButton} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.filledButton} />
                </div.Demo>

                {/* Filled button but no color provided */}
                <div.Demo data-cy='filled-no-color-provided' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Filled without color
                        </div.DemoTitle>

                        <Button config={configs.noColorFilledButton} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.noColorFilledButton} />
                </div.Demo>

                {/* Filled button with icon */}
                <div.Demo data-cy='filled-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Filled button with icon
                        </div.DemoTitle>

                        <Button config={configs.filledButtonIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.filledButtonIcon} />
                </div.Demo>

                {/* Filled disabled button with icon */}
                <div.Demo data-cy='filled-disabled-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Filled disabled button with icon
                        </div.DemoTitle>

                        <Button config={configs.filledDisabledButtonIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.filledDisabledButtonIcon} />
                </div.Demo>

                {/* ====== ROUNDED BUTTONS ====== */}
                {/** Rounded button */}
                <div.Demo data-cy='rounded-borders' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Rounded borders
                        </div.DemoTitle>

                        <Button config={configs.roundedButton} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.roundedButton} />
                </div.Demo>

                {/** Rounded disabled button */}
                <div.Demo data-cy='rounded-disabled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Rounded disabled button
                        </div.DemoTitle>

                        <Button config={configs.roundedDisabledButton} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.roundedDisabledButton} />
                </div.Demo>

                {/** Rounded filled button with icon */}
                <div.Demo data-cy='rounded-filled-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Rounded filled button with icon
                        </div.DemoTitle>

                        <Button config={configs.roundedFilledButtonIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.roundedFilledButtonIcon} />
                </div.Demo>

                {/** Rounded filled disabled button with icon */}
                <div.Demo data-cy='rounded-filled-disabled-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Rounded filled disabled button with icon
                        </div.DemoTitle>

                        <Button config={configs.roundedFilledDisabledButtonIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.roundedFilledDisabledButtonIcon} />
                </div.Demo>

                {/** Rounded button with icon */}
                <div.Demo data-cy='rounded-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Rounded button with icon
                        </div.DemoTitle>

                        <Button config={configs.roundedButtonIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.roundedButtonIcon} />
                </div.Demo>

                {/** Overrides */}
                <div.Demo data-cy='overrides' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides
                        </div.DemoTitle>

                        <Button config={configs.simpleButton}
                            overrides={{ root: 'margin: 50px;' }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.overrides} />
                </div.Demo>
            </>
        );
    }

    private incrementCallbackCount() {
        const { callbackCount } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                callbackCount: callbackCount + 1,
            }
        });
    }
}