import { ReactWebAttributes } from '../../interfaces/workarounds';
import styled from 'styled-components/native';

export const Icon = styled.TouchableOpacity<ReactWebAttributes>`
    align-items: center;
    justify-content: center;
`;

interface Props {
    width: number;
    height: number;
}

export const Icon2 = styled.View<Props>`
    margin: 3px;
    width: ${props => props.width ? props.width : '20'}px;
    height: ${props => props.height ? props.height : '20'}px;
`;
