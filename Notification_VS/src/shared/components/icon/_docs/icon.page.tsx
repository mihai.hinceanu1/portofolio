import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { colors } from '../../../style/colors';
import { CodeEditor } from '../../code-editor/code-editor';
import { Icon } from '../icon';
import * as React from 'react';

interface IconState { }

export class IconPage extends ScrollableDocPage<IconState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Icon
                        </div.OverviewTitle>

                    <div.OverviewDescription>
                        Custom icon, with hover effect, click function and customizable
                        dimensions and common style properties.
                        </div.OverviewDescription>
                </div.Overview>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Simple Icon
                        </div.DemoTitle>

                        <Icon icon='/sentiments/partyDarkGrey.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // path to the desired icon from the assets folder.
    icon='/sentiments/partyDarkGrey.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Empty Icon
                        </div.DemoTitle>

                        <Icon icon='' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // path to the desired icon from the assets folder.
    icon='' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Icon with width set
                        </div.DemoTitle>

                        <Icon
                            width={40}
                            icon='/sentiments/partyDarkGrey.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // width prop with the desired value
    width={40}
    // path to the desired icon from the assets folder
    icon='/sentiments/partyDarkGrey.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Icon with height set
                        </div.DemoTitle>

                        <Icon
                            height={40}
                            icon='/sentiments/partyDarkGrey.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // height prop with the desired value
    height={40}
    // path to the desired icon from the assets folder.
    icon='/sentiments/partyDarkGrey.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Icon with both dimensions set
                        </div.DemoTitle>

                        <Icon
                            width={30}
                            height={40}
                            icon='/sentiments/partyDarkGrey.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // both dimensions are set
    width={30}
    height={40}
    // path to the desired icon from the assets folder.
    icon='/sentiments/partyDarkGrey.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Round Icon
                        </div.DemoTitle>

                        <Icon
                            round={true}
                            icon='/shared/checksign.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // set round flag
    round={true}
    // path to the desired icon from the assets folder.
    icon='/shared/checksign.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Icon disabled prop
                        </div.DemoTitle>

                        <Icon
                            disabled={true}
                            icon='/shared/checksign.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // set disabled flag
    disabled={true}
    // path to the desired icon from the assets folder.
    icon='/shared/checksign.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Hover overlay message
                        </div.DemoTitle>

                        <Icon
                            onHoverOverlayMessage='test message'
                            icon='/shared/checksign.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // set the hover message
    onHoverOverlayMessage='test message'
    // path to the desired icon from the assets folder.
    icon='/shared/checksign.png' />`}
                    />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Hover overlay message with different color
                        </div.DemoTitle>

                        <Icon
                            onHoverOverlayMessage='test message'
                            overlayMessageBoxColor={colors.$lightBlue}
                            icon='/shared/checksign.png' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<Icon
    // set the hover message
    onHoverOverlayMessage='test message'
    // set the box color
    overlayMessageBoxColor={colors.$lightBlue}
    // path to the desired icon from the assets folder.
    icon='/shared/checksign.png' />`}
                    />
                </div.Demo>
            </>
        );
    }
}