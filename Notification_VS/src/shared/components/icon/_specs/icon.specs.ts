/// <reference types="cypress" />

describe('Icon test', () => {
    it('Successfully loads the page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/icon');
    });

    it('Renders icon having only its path provided', () => {
        // !!!
        // First icon appears in the header so the counter starts from 1
        // !!!
        let wrapper = cy.get('[data-cy=icon]').eq(1);
        // Following commands are scoped within this wrapper.
        wrapper.within(() => {
            cy.get('img').should('have.attr', 'src').should('include', 'partyDarkGrey');
        });
    });

    it('When icon is empty page we should have an empty container of 20px when no width is provided', () => {
        let wrapper = cy.get('[data-cy=icon]').eq(2);
        wrapper.within(() => {
            cy.get('[data-cy=icon-container]').should('have.css', 'width', '20px');
        });
    });

    it('Icon with width set to 40 should have the given value if it is valid', () => {
        cy.get('[data-cy=icon-container]').eq(3).should('have.css', 'width', '40px');
    });

    it('Icon with height set to 40 should have the given value if it is valid', () => {
        cy.get('[data-cy=icon-container]').eq(4).should('have.css', 'height', '40px');
    });

    it('Icon with both dimensions set should have the given values if they are valid', () => {
        let icon = cy.get('[data-cy=icon-container]').eq(5);
        icon.should('have.css', 'width', '30px').and('have.css', 'height', '40px');
    });

    it('Icon with round prop set true must be round', () => {
         cy.get('[data-cy=icon]').eq(6).within(() => {
            cy.get('[data-testid=fit-img]').should('have.css', 'border-radius', '10px');
        });
    });

    it('Icon with disabled prop can\'t be clicked', () => {
        let wrapper = cy.get('[data-cy=icon]').eq(7);
        wrapper.should('have.attr', 'disabled');
    });

    xit('When testID prop is set (to "test") it should be displayed as a custom attribute', () => {
        cy.get('[data-cy=icon]').eq(8).should('have.attr', 'data-testid', 'test');
    });

    xit('When nativeID prop is set (to "test") it should have it as id', () => {
        cy.get('[data-cy=icon]').eq(9).should('have.id', 'test');
    });

    it('Hover overlay message renders on hover', () => {
        cy.get('[data-cy=icon]').eq(8).trigger('mouseover');
        cy.get('[data-cy=icon-hover-message]').should('be.visible');
    });

    it('Hover overlay message with different color is rendered correctly', () => {
        cy.get('[data-cy=icon]').eq(8).trigger('mouseover');
        cy.get('[data-cy=icon-hover-message]').should('be.visible')
            .and('have.css', 'background-color', 'rgb(13, 142, 241)');
    });
});