import * as div from './icon.style';
import { FitImage } from '../fit-image/fit-image';
import { MessageOverlay } from '../message-overlay/message-overlay';
import * as React from 'react';
import { GestureResponderEvent } from 'react-native';

interface Props {
    /** pathname of the icon rendered */
    icon: string;

    /** executes when icon is pressed */
    onPress?: (e: GestureResponderEvent) => void;

    /** pathname of the icon when it is hovered */
    iconHovered?: string;

    /** overlaying message for icon */
    onHoverOverlayMessage?: string;

    /** color for the box which contains the overlay message */
    overlayMessageBoxColor?: string;

    /** fixed width */
    width?: number;

    /** fixed height */
    height?: number;

    /** Rounded corners existence */
    round?: boolean;

    /** onPress works or not */
    disabled?: boolean;
}

interface State {
    /** Change the image src when hovered (simulated icon color change) and add a small message box (tooltip) when hovered */
    hovered: boolean;
}

/** Custom Icon, can have secondary image (will switch img src) when hovered contains message on hover if provided */
export class Icon extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            hovered: false,
        };
    }

    public render() {
        const {
            onPress,
            icon,
            iconHovered,
            onHoverOverlayMessage,
            round,
            width,
            height,
            disabled,
            overlayMessageBoxColor,
        } = this.props;
        const { hovered } = this.state;

        return (
            <div.Icon data-cy='icon'
                onPress={onPress}
                disabled={disabled ? disabled : false}
                onMouseEnter={(e: any) => this.onMouseEnterButton(e)}
                onMouseLeave={(e: any) => this.onMouseLeaveButton(e)}>

                {/** Switch between image src for icon(base) and iconHovered when hover */}
                {
                    iconHovered &&
                    <div.Icon2 width={width} height={height} data-cy='icon-container'>
                        <FitImage imgPath={hovered ? iconHovered : icon} round={round} />
                    </div.Icon2>
                }

                {/** Overlay message at hover */}
                {
                    hovered && onHoverOverlayMessage &&
                    <MessageOverlay
                        text={onHoverOverlayMessage}
                        iconHeight={height ? height : -9.5}
                        overlayMessageBoxColor={overlayMessageBoxColor} />
                }
                {/** Just default icon with no hovered icon provided */}
                {
                    !iconHovered &&
                    <div.Icon2 width={width} height={height} data-cy='icon-container'>
                        <FitImage imgPath={icon} round={round} />
                    </div.Icon2>
                }

            </div.Icon>
        );
    }

    private onMouseEnterButton(_event: any) {
        this.setState({ hovered: true });
    }

    private onMouseLeaveButton(_event: any) {
        this.setState({ hovered: false });
    }
}
