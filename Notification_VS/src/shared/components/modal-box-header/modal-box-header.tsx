import * as div from './modal-box-header.style';
import { Icon } from '../../../icons/components/icon/icon';
import { CROSS } from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import React from 'react';

interface Props {
    icon?: string;
    /** The fill color of the svg, defaults to white */
    iconColor?: string;
    /** The background color, defaults to blue */
    bgrColor?: string;
    title: string;
    closeModal: () => void;
}

/**
 * Presentation component, represents the header area that
 * appears usually inside the content area of a modal.
 */
export const ModalBoxHeader: React.FunctionComponent<Props> = (props: Props) => {
    const { icon, iconColor, bgrColor, title, closeModal } = props;

    return (
        <div.ModalBoxHeader data-cy='header-section' bgrColor={bgrColor}>
            {/** Icon */}
            <Icon path={icon} color={div.getIconColor(iconColor)} width={30} height={30} />

            {/** Title */}
            <div.Title data-cy='title'>
                {title}
            </div.Title>

            {/** Close button */}
            <div.Close data-cy='close'
                onPress={closeModal}
                underlayColor='none'>
                <Icon path={CROSS} color={colors.$white} />
            </div.Close>
        </div.ModalBoxHeader>
    );
};