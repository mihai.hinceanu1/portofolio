import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface Props {
    bgrColor: string;
}

export const ModalBoxHeader = styled.View<Props>`
    width: 100%;
    height: 100px;
    align-items: center;
    justify-content: center;
    background-color: ${props => getBgrColor(props)};
`;

export const Title = styled.Text`
    font-weight: 600;
    font-size: ${font.$size18}px;
    color: ${colors.$white};
    text-align: center;
`;

export const Close = styled.TouchableHighlight`
    position: absolute;
    top: 5px;
    right: 5px;
    width: 20px;
    height: 20px;
    align-items: center;
    justify-content: center;
`;

// ====== UTILS ======

function getBgrColor(props: Props): string {
    return !!props.bgrColor ? props.bgrColor : colors.$blue;
}

export function getIconColor(color: string): string {
    return !!color ? color : colors.$white;
}