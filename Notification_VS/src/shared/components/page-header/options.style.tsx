import { CARET_DOWN } from '../../assets/icons';
import { pathBoundingBox } from '../../services/raphael.utils';
import { colors } from '../../style/colors';
import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

// ====== DIMENSIONS ======

export const OPTION_WIDTH = 115;

// ====== PROPS ======

interface OptionNameProps {
    isSelected: boolean;
    textColor: string;
    highlightColor: string;
}

// ====== STYLE COMPRESSED ======

export const OptionsCompressed = styled.View`
    flex-basis: ${OPTION_WIDTH}px;
`;

// ====== STYLE WEB ======

export const Options = styled.View`
    flex-direction: row;
    align-items: center;
`;

export const Option = styled.TouchableHighlight`
    margin: 10px 10px 10px 0px;
`;

export const OptionName = styled.Text<OptionNameProps>`
    padding-bottom: 2px;
    font-weight: 600;
    color: ${props => getTextColor(props)};
    ${props => highlightOption(props)};
`;

// ====== UTILS ======

let highlightOption = (props: OptionNameProps): string =>
    props.isSelected ?
    `
        border-bottom-width: 1px;
        border-bottom-color: ${getTextColor(props)};
    ` : ``;

let getTextColor = (props: OptionNameProps): string => {
    const { isSelected, highlightColor, textColor } = props;

    if (isSelected) {
        return highlightColor ? highlightColor : colors.$blue;
    }

    return textColor ? textColor : colors.$black;
};


// ====== SVG ======

export const getArrowDownSvg = (): JSX.Element => {
    const { width, height } = pathBoundingBox(CARET_DOWN);

    return (
        <Svg width={width + 1}
            height={height + 1}
            style={centerSvgStyle}>
            <Path d={CARET_DOWN} fill={colors.$grey} />
        </Svg>
    );
};

// ====== CONST ======

var centerSvgStyle: StyleProp<ViewStyle> = {
    justifyContent: 'center',
    alignItems: 'center',
};

// ===== OVERRIDES =====
export const dropdownOverrides = {
    root: `
        background-color: transparent;
        width: 100%;
    `,
}
