/**
 * <!>For better accuracy this should be computed dynamically inside the
 * page header component, depending on the width of the page title and
 * other tools present on the header, but for now we'll just use this constant.
 *
 * When the width of the header is smaller than this it's contents (search bar,
 * filter bar and buttons, if present) will shrink for some extra space.
 */
export const NARROW_THRESHOLD = 768;

export const FILTER_BAR_WIDTH = 120;
