import { FilterBar } from './filter-bar';
import { Options } from './options';
import { SearchBar } from './search-bar';
import * as div from './tools.style';
import { WIDTH_S, WIDTH_XS } from '../../constants/adaptive.const';
import { IDropdownOption } from '../../interfaces/dropdown';
import { IHeaderFilter, PageHeaderButton, PageHeaderSearchBar } from '../../interfaces/page-header';
import { Button } from '../button/button';
import React from 'react';

interface Props {
    options: IDropdownOption[];
    activeOption: IDropdownOption;
    searchBar: PageHeaderSearchBar;
    filterBar: IHeaderFilter;
    button: PageHeaderButton;
    width: number;
    textColor: string;
    highlightColor: string;
}
interface State {
    /** Signals to compress options in a dropdown when the search bar is extended */
    searchBarExtended: boolean;
    selectedOption: IDropdownOption;
}

/**
 * The utilities available on the page header
 * Contains navigation buttons, search bar, filters and a button
 */
export class Tools extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            searchBarExtended: false,
            selectedOption: null,
        };
    }

    /**
     * Part of this may be better of in the parent component (PageHeader)
     * as some parent should have control over all the variables (search term,
     * filter).
     *
     * For the sake of presentability I'll just leave them here and as a real use
     * case pops up and it must be moved, I'll make sure to move it.
     */
    static getDerivedStateFromProps(props: Props, state: State): State {
        const {options, width, searchBar} = props;
        const { selectedOption } = state;

        let newState: State = {...state};

        /**
         * Reset the flag for search bar so the options
         * menu resets from its compressed state.
         */
        if (width > WIDTH_XS) {
            newState.searchBarExtended = false;
        }

        /**
         * If the screen changes size and we have a
         * value inside the search bar, then extend it.
         */
        if (!!searchBar && !!searchBar.value && (width <= WIDTH_XS)) {
            newState.searchBarExtended = true;
        }

        /**
         * Initialize selected option
         * If it doesn't receive any "activeOption" prop from parent
         * set first value from "options" array
         */
        if (!!options && !selectedOption) {
            newState.selectedOption = props.activeOption || props.options[0];
        }

        return newState;
    }

    public render() {
        const { options, searchBar, filterBar, button, width, textColor, highlightColor } = this.props;
        const { searchBarExtended, selectedOption } = this.state;
        let shouldRenderButton = !!button && width >= WIDTH_S;

        return (
            <div.Tools data-cy='tools' hasOptions={!!options} width={width}>
                {/** Options */}
                {
                    !!options &&
                    <Options options={options}
                        selectedOption={selectedOption}
                        textColor={textColor}
                        highlightColor={highlightColor}
                        compressed={searchBarExtended}
                        setSelectedOption={(option) => this.setSelectedOption(option)} />
                }

                {/** Group search bar, filter bar and button together */}
                <div.Utilities data-cy='utilities'>
                    {/** Search Bar */}
                    {
                        !!searchBar &&
                        <SearchBar {...searchBar}
                            width={width}
                            textColor={textColor}
                            hasOptions={!!options}
                            isExtended={searchBarExtended}
                            highlightColor={highlightColor}
                            extendSearchBarSignal={() => this.extendSearchBarSignal()} />
                    }

                    {/** Filters */}
                    {
                        !!filterBar &&
                        <FilterBar {...filterBar}
                            width={width}
                            textColor={textColor}
                            highlightColor={highlightColor} />
                    }

                    {/** Button */}
                    {
                        shouldRenderButton &&
                        <Button config={{
                            ...button,
                            filled: true,
                            rounded: true,
                        }}
                            overrides={div.BUTTON_OVERRIDES} />
                    }
                </div.Utilities>
            </div.Tools>
        );
    }

    /**
     * The signal that extends the search bar must be sent from
     * this component, as the options need this signal as well
     * to compress when the search bar extends.
     */
    private extendSearchBarSignal() {
        const { searchBarExtended } = this.state;
        const { searchBar } = this.props;

        /** If the value inside the search bar is not empty keep it extended */
        if (!!searchBar && !searchBar.value) {
            this.setState({
                searchBarExtended: !searchBarExtended,
            });
        }
    }

    private setSelectedOption(option: IDropdownOption) {
        this.setState({
            selectedOption: option,
        });
    }
}