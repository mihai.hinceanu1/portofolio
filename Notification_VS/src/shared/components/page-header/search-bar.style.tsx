import { InputOverrides } from '../../interfaces/input';
import { colors } from '../../style/colors';
import styled from 'styled-components/native';
const BORDER_WIDTH = 2;

// ====== PROPS ======

interface SearchBarMobileProps {
    hasOptions: boolean;
    isExtended: boolean;
}

interface ThemeProps {
    highlightColor: string;
}

// ====== STYLE MOBILE ======

export const SearchBarMobile = styled.View<SearchBarMobileProps & ThemeProps>`
    flex-direction: row;
    align-items: center;
    ${props => setMaxWidth(props)};
    ${props => getBorders(props)};
`;

export const SearchIconMobile = styled.TouchableHighlight``;

export const ClearIcon = styled.TouchableHighlight`
    margin: 5px 10px;
`;

// ====== STYLE WEB ======

export const SearchBar = styled.View<ThemeProps>`
    flex-direction: row;
    align-items: center;
    border-width: ${BORDER_WIDTH}px;
    border-color: ${props => getHighlightColor(props)};
    height: 40px;
    max-width: 250px;
    width: auto;
    flex-grow: 1;
    border-radius: 20px;
    padding-left: 5px;
    padding-right: 10px;
`;

export const SearchIcon = styled.View``;

// ====== UTILS ======

let getBorders = (props: SearchBarMobileProps & ThemeProps): string => {
    if (!props.hasOptions) {
        return `
            border-width: ${BORDER_WIDTH}px;
            border-color: ${getHighlightColor(props)};
            height: 40px;
            width: 100%;
            border-radius: 20px;
            padding-left: 5px;
            padding-right: 10px;
        `;
    }

    if (props.isExtended) {
        return `
            border-width: ${BORDER_WIDTH}px;
            border-color: ${getHighlightColor(props)};
            height: 40px;
            flex-grow: 1;
            border-radius: 20px;
            padding-left: 5px;
            padding-right: 10px;
        `;
    }
};

let setMaxWidth = (props: SearchBarMobileProps): string =>
    props.hasOptions ? 'max-width: 250px;' : '';

export const getHighlightColor = (props: ThemeProps): string =>
    !!props.highlightColor ? props.highlightColor : colors.$blue;

// ====== CONST ======

export const getInputOverrides = (textColor: string): InputOverrides => {
    let inputOverrides: InputOverrides = {
        root: `
        margin: 0px;
        width: auto;
        flex-grow: 1;
    `,
        inputFrame: 'margin: 0px;',
        input: `
        height: 35px;
        border-width: 0px;
        width: auto;
        min-width: 150px;
        flex-grow: 1;
        color: ${textColor};
    `
    };

    return inputOverrides;
};