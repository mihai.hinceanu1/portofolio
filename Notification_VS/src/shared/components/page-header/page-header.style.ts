import { WIDTH_S } from '../../constants/adaptive.const';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

// ====== PROPS ======

interface Override {
    override: string;
}

interface PageHeaderProps extends Override {
    width: number;
}

interface TextProps {
    color: string;
}

// ====== STYLE ======

export const PageHeader = styled.View<PageHeaderProps>`
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    ${props => getPageHeaderPadding(props)};
    ${props => props.override};
`;

export const Details = styled.View`
    margin-bottom: 10px;
`;

export const Title = styled.Text<TextProps>`
    font-size: ${font.$size22}px;
    font-weight: 700;
    color: ${props => getTextColor(props)};
`;

export const Description = styled.Text<TextProps>`
    color: ${props => getTextColor(props)};
`;

// ====== UTILS ======

let getTextColor = (props: TextProps): string =>
    !!props.color ? props.color : colors.$black;

function getPageHeaderPadding(props: PageHeaderProps): string {
    if (props.width <= WIDTH_S) {
        return 'padding: 0px 10px;';
    }
}
