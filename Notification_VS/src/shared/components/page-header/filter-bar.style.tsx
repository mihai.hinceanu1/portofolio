import { SORT } from '../../assets/icons';
import { pathBoundingBox } from '../../services/raphael.utils';
import { colors } from '../../style/colors';
import styled from 'styled-components/native';

// ====== CONST ======

export const FILTER_ICON = SORT;

const { width: ICON_WIDTH, height: ICON_HEIGHT } = pathBoundingBox(FILTER_ICON);
const FILTER_WIDTH = 100;

// ====== PROPS ======

interface FilterBarProps {
    borderColor: string;
}

interface ThemeProps {
    textColor: string;
}

// ====== STYLE MOBILE ======

export const FilterBarMobile = styled.View`
    margin-left: 10px;
    flex-direction: row;
    align-items: center;
    flex-basis: ${ICON_WIDTH + 1}px;
`;

export const FiltersDropdownMobile = styled.View`
    position: absolute;
    top: ${ICON_HEIGHT}px;
    right: 0px;
`;

export const FilterMobile = styled.TouchableHighlight`
    width: ${FILTER_WIDTH}px;
    align-items: flex-end;
    padding: 3px;
`;

// ====== COMMON ======

export const ToggleDropdown = styled.TouchableHighlight``;

export const Label = styled.Text<ThemeProps>`
    color: ${props => getTextColor(props)};
`;

// ====== STYLE WEB ======

export const FilterBar = styled.View<FilterBarProps>`
    height: 40px;
    margin-left: 10px;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    flex-basis: ${FILTER_WIDTH}px;
    border-width: 2px;
    border-color: ${props => getBorderColor(props)};
    border-radius: 20px;
    padding: 0px 10px 0px 5px;
`;

export const SelectedFilter = styled.Text<ThemeProps>`
    color: ${props => getTextColor(props)};
`;

export const FiltersDropdown = styled.View`
    position: absolute;
    top:  40px;
    left: 0px;
`;

export const Filter = styled.TouchableHighlight`
    width: ${FILTER_WIDTH}px;
    padding: 3px 10px 3px 5px;
`;

// ====== UTILS ======

let getTextColor = (props: ThemeProps) =>
    !!props.textColor ? props.textColor : colors.$blue;

let getBorderColor = (props: FilterBarProps) =>
    !!props.borderColor ? props.borderColor : colors.$blue;