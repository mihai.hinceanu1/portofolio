/// <reference types="cypress" />

describe('Page Header Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/page-header');
    });

    it('Page Header Is Rendered', () => {
        getEl('default', 'page-header').should('exist');
    });

    it('Title Is Rendered', () => {
        getEl('default', 'title').should('exist');
    });

    it('Title Value', () => {
        getEl('default', 'title').then(title => {
            expect(title.text()).to.eq('FrontEnd');
        });
    });

    it('Text color can be changed', () => {
        getEl('text-color', 'title').should('have.css', 'color', 'rgb(0, 195, 167)');
        getEl('text-color', 'description').should('have.css', 'color', 'rgb(0, 195, 167)');
        /** <!> Navigation items that aren't selected. */
        getEl('text-color', 'option-name').last().should('have.css', 'color', 'rgb(0, 195, 167)');
        getEl('text-color', 'input').should('have.css', 'color', 'rgb(0, 195, 167)');
        getEl('text-color', 'selected-filter').should('have.css', 'color', 'rgb(0, 195, 167)');
    });

    it('Highlighted elements can have different color', () => {
        /** Selected Navigation item */
        getEl('highlight-color', 'option-name').first()
            .should('have.css', 'color', 'rgb(0, 195, 167)');

        /** Search bar borders */
        getEl('highlight-color', 'search-bar')
            .should('have.css', 'border-color', 'rgb(0, 195, 167)');

        /** Icons */
        getElByTag('highlight-color', 'path')
            .should('have.attr', 'fill', 'rgb(0,195,167)');

        /** Filter bar borders */
        getEl('highlight-color', 'filter-bar')
            .should('have.css', 'border-color', 'rgb(0, 195, 167)');
    });

    it('Links Are Rendered', () => {
        getEl('links', 'option').its('length').should('gte', 1);
    });

    it('Selected Link Changes Color', () => {
        getEl('links', 'option').eq(0).click();
        getEl('links', 'option-name').eq(0).should('have.css', 'color', 'rgb(13, 142, 241)');
    });

    it('Calls Options Callback', () => {
        getEl('callbacks', 'option').eq(0).click();
        getEl('callbacks', 'callback-result').then(callback => {
            expect(callback.text()).to.eq('The Callback was called: 1 times');
        });
    });

    it('Can have CSS properties for outer container overwritten', () => {
        getEl('overrides', 'page-header').should('have.css', 'background-color', 'rgba(0, 195, 167, 0.8)');
    });

    it('Search Bar is rendered', () => {
        getEl('header-search-bar', 'search-bar').should('exist');
    });

    it('Search Bar input works as instructed', () => {
        getEl('header-search-bar', 'input').clear().type('JavaScript');
        getEl('header-search-bar', 'search-value').should('have.text', 'JavaScript');
        getEl('header-search-bar', 'input').clear();
    });

    it('Mobile Search Bar appears on narrow screens.', () => {
        getEl('search-bar-narrow', 'input').should('exist');
        getEl('search-bar-narrow', 'search-icon-mobile').should('exist');
        getEl('search-bar-narrow', 'search-bar-mobile').should('exist');
    });

    it('On narrower screens (<512px) tools bar will move under the title', () => {
        getEl('search-bar-narrow', 'tools').then(
            ($toolsBar) => {
                getEl('search-bar-narrow', 'title').then(
                    ($header) => {
                        expect($header.position().top).to.be.lt($toolsBar.position().top);
                    });
            });
    });

    it('Narrow Search Bar works as instructed', () => {
        // Type inside the input and check the output
        getEl('search-bar-narrow', 'input').clear().type('JavaScript');
        getEl('search-bar-narrow', 'search-value').should('have.text', 'JavaScript');
    });

    it('A button can also be rendered with the right props', () => {
        // Test for button existence
        getEl('header-button', 'button').should('exist');
        // Test for the text inside the button
        getEl('header-button', 'text').should('have.text', 'NEW PACKAGE');
        // Test for the Icon inside the button
        getEl('header-button', 'icon').should('have.length', 1);
        getElByTag('header-button', 'path').should('have.attr', 'd', NEW_PACKAGE);
        // Test if the callback is called
        getEl('header-button', 'button-callback-calls').should('have.text', '0');
        getEl('header-button', 'button').click();
        getEl('header-button', 'button-callback-calls').should('have.text', '1');
    });

    it('Filters are shown on click', () => {
        getEl('header-filter', 'toggle-dropdown').click();
        getEl('header-filter', 'filters-dropdown').should('exist');
    });

    it('Filter callback is called', () => {
        getEl('header-filter', 'filter').eq(0).click();
        getEl('header-filter', 'header-selected-filter').should('have.text', 'Projects');
    });

    it('On narrow screens the filter is represented only through an icon', () => {
        getEl('header-filter-narrow', 'toggle-dropdown-mobile').should('exist');
        getEl('header-filter-narrow', 'toggle-dropdown-mobile').click();
        getEl('header-filter-narrow', 'filters-dropdown-mobile').should('exist');
        getEl('header-filter-narrow', 'filter-mobile').eq(0).click();
        getEl('header-filter-narrow', 'header-narrow-selected-filter').should('have.text', 'Projects');
    });

    it('When fully stacked, everything is visible', () => {
        /**
         * <!>Removing this will result in cypress saying that
         * the elements are not visible, although they clearly are,
         * may be something related to the fact that they're wrapped
         * in a ScrollView or something.
         */
        cy.get('[data-cy=stacked-header]').scrollIntoView();
        getEl('stacked-header', 'title').should('be.visible');
        getEl('stacked-header', 'description').should('be.visible');
        getEl('stacked-header', 'options').should('be.visible');
        getEl('stacked-header', 'search-bar').should('be.visible');
        getEl('stacked-header', 'filter-bar').should('be.visible');
        getEl('stacked-header', 'button').should('be.visible');
    });

    /** Testing adaptive behavior */
    it('On narrow screens the button disappears', () => {
        getEl('stacked-header-narrower', 'button').should('not.exist');
    });

    it('On narrower screens, the header is wrapped in 2 rows', () => {
        getEl('stacked-header-narrower', 'title').then(($title) => {
            getEl('stacked-header-narrower', 'tools').then(($tools) => {
                expect($title.position().top).to.be.lt($tools.position().top);
            });
        });

        /** The button isn't rendered */
        getEl('stacked-header-narrower', 'button').should('not.exist');
    });

    it('When search bar is present alongside navigation buttons and the screen is narrow, when extended, the navigation buttons compress into a dropdown', () => {
        getEl('stacked-header-narrower', 'search-bar-mobile').should('exist');
        getEl('stacked-header-narrower', 'options').should('exist');

        /** Extend the search bar */
        getEl('stacked-header-narrower', 'search-icon-mobile').should('exist');
        getEl('stacked-header-narrower', 'search-icon-mobile').click();

        /** The navigation buttons compress in a dropdown */
        getEl('stacked-header-narrower', 'options-compressed').should('exist');
        getEl('stacked-header-narrower', 'dropdown').should('exist');
    });
});

// ====== CONST ======

var NEW_PACKAGE = 'M31.358,7.892 L31.358,11.370 L29.312,11.370 L29.312,7.892 L25.826,7.892 L25.826,5.805 L29.312,5.805 L29.312,2.327 L31.358,2.327 L31.358,5.805 L34.844,5.805 L34.844,7.892 L31.358,7.892 ZM26.129,20.723 L25.826,19.796 L26.811,19.332 L27.190,20.259 L26.129,20.723 ZM28.099,17.786 L22.719,20.105 L22.264,19.177 L27.645,16.781 L28.099,17.786 ZM20.825,16.085 L25.068,14.385 L25.371,15.312 L21.204,17.013 L20.825,16.085 ZM21.204,18.868 L20.825,17.940 L24.917,16.240 L25.371,17.167 L21.204,18.868 ZM21.734,21.496 L24.917,20.105 L25.371,21.110 L22.189,22.501 L21.734,21.496 ZM20.749,23.969 L24.386,22.424 L24.917,23.428 L21.279,25.052 L20.749,23.969 ZM24.765,6.346 C24.765,6.346 18.139,3.243 17.490,2.945 C16.842,2.648 16.278,2.945 16.278,2.945 L3.774,8.510 L16.884,14.308 L28.327,9.206 L28.327,9.979 L17.339,14.849 L17.339,28.762 C17.339,28.762 28.595,23.798 29.463,23.506 C30.332,23.214 30.221,22.578 30.221,22.578 L30.221,12.452 L32.722,12.452 C32.722,12.452 32.646,20.541 32.646,22.501 C32.646,24.460 30.979,25.438 30.979,25.438 C30.979,25.438 19.695,30.391 18.703,30.771 C17.711,31.151 17.319,31.313 16.581,31.313 C15.843,31.313 15.066,30.926 15.066,30.926 C15.066,30.926 5.193,26.549 3.017,25.670 C0.841,24.791 0.971,21.805 0.971,21.805 C0.971,21.805 0.971,11.784 0.971,9.438 C0.971,7.092 2.714,6.346 2.714,6.346 C2.714,6.346 14.604,0.962 15.520,0.549 C16.436,0.136 17.945,0.472 17.945,0.472 L27.645,4.800 L24.765,4.800 L24.765,6.346 ZM3.396,21.728 C3.396,23.531 4.153,23.428 4.153,23.428 L16.505,28.839 L16.505,14.926 L3.320,9.129 C3.320,9.129 3.396,19.924 3.396,21.728 Z';

// ====== SELECTORS ======

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

function getElByTag(wrapper, tag) {
    return cy.get(`[data-cy=${wrapper}] ${tag}`);
}