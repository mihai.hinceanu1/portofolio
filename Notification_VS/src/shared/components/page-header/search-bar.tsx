import * as div from './search-bar.style';
import { Icon } from '../../../icons/components/icon/icon';
import { CROSS, SEARCH } from '../../assets/icons';
import { WIDTH_XS } from '../../constants/adaptive.const';
import { PageHeaderSearchBar } from '../../interfaces/page-header';
import { colors } from '../../style/colors';
import { Input } from '../input/input';
import React from 'react';

interface Props extends PageHeaderSearchBar {
    width: number;
    hasOptions: boolean;
    extendSearchBarSignal: () => void;
    isExtended: boolean;
    textColor: string;
    highlightColor: string;
}

/**
 * Custom search bar for header.
 * On narrow screens, if navigation buttons are also present
 * on the header it shrinks into an icon and expands on click.
 */
export const SearchBar: React.FunctionComponent<Props> = (props: Props) => {
    const {
        width,
        hasOptions,
        value,
        onChange,
        onSubmit,
        clearSearch,
        extendSearchBarSignal,
        isExtended,
        textColor,
        highlightColor
    } = props;

    let shouldRenderInput = isExtended || !hasOptions;

    /** Mobile */
    if (width <= WIDTH_XS) {
        return (
            <div.SearchBarMobile data-cy='search-bar-mobile'
                hasOptions={hasOptions}
                isExtended={isExtended}
                highlightColor={highlightColor}>
                {
                    shouldRenderInput &&
                    <>
                        {/** Input */}
                        <Input value={value}
                            onChangeText={onChange}
                            /** Autofocus only if the header has options */
                            autoFocus={hasOptions}
                            placeholderColor={getColor(textColor)}
                            onBlur={() => onLoseFocus(value, onSubmit)}
                            overrides={div.getInputOverrides(getColor(textColor))} />

                        {/** Clear Icon */}
                        <div.ClearIcon data-cy='clear-icon'
                            onPress={clearSearch}
                            underlayColor='none'>
                            <Icon path={CROSS} color={div.getHighlightColor({ highlightColor })} />
                        </div.ClearIcon>
                    </>
                }

                {/** Search Icon */}
                <div.SearchIconMobile data-cy='search-icon-mobile'
                    onPress={extendSearchBarSignal}
                    underlayColor='none'>
                    <Icon path={SEARCH} color={div.getHighlightColor({ highlightColor })} />
                </div.SearchIconMobile>
            </div.SearchBarMobile>
        );
    }

    /** Web */
    return (
        <div.SearchBar data-cy='search-bar'
            highlightColor={highlightColor}>
            {/** Input */}
            <Input value={value}
                onChangeText={onChange}
                placeholderColor={getColor(textColor)}
                onBlur={() => onLoseFocus(value, onSubmit)}
                overrides={div.getInputOverrides(getColor(textColor))} />

            {/** Search Icon */}
            <div.SearchIcon data-cy='search-icon'>
                <Icon path={SEARCH} color={div.getHighlightColor({ highlightColor })} />
            </div.SearchIcon>
        </div.SearchBar>
    );
};

function onLoseFocus(value: string, onSubmit: () => void) {
    if (!!value) {
        onSubmit();
    }
}

function getColor(textColor: string): string {
    return !!textColor ? textColor : colors.$blue;
}