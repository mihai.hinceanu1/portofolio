import styled from 'styled-components/native';

// ====== PROPS ======

interface ToolsProps {
    hasOptions: boolean;
    width: number;
}

// ====== STYLE ======

export const Tools = styled.View<ToolsProps>`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    flex-grow: 1;
    ${props => getWidth(props)};
`;

export const Utilities = styled.View`
    flex-direction: row;
    flex-grow: 1;
    justify-content: flex-end;
`;

// ====== UTILS ======

let getWidth = (props: ToolsProps): string => {
    if (props.hasOptions) {
        return `flex-basis: ${props.width}px`;
    }

    return 'width: auto';
};

// ====== CONST ======

export const BUTTON_OVERRIDES = {
    root: `
        margin: 0px 10px;
        align-self: center;
        padding: 0px 30px;
        height: 34px;
    `,
};