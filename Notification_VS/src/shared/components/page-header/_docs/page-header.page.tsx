import * as samples from './page-header.page.samples';
import * as configs from './page-header.page.utils';
import { NARROW_THRESHOLD } from '../_const/page-header.const';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { WIDTH_XS } from '../../../constants/adaptive.const';
import { Filter } from '../../../interfaces/page-header';
import { colors } from '../../../style/colors';
import { CodeEditor } from '../../code-editor/code-editor';
import { PageHeader } from '../page-header';
import * as React from 'react';

interface PageHeaderState {
    callbackPresses: number;
    searchBar: string;
    searchBarNarrow: string;
    buttonCallbackCalls: number;
    selectedFilter: Filter;
    selectedFilterNarrow: Filter;
    stackedHeaderSearchTerm: string;
    stackedHeaderFilter: Filter;
    stackedButtonCallCount: number;
    stackedOptionsCallCount: number;
}

export class PageHeaderPage extends ScrollableDocPage<PageHeaderState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                callbackPresses: 0,
                searchBar: '',
                searchBarNarrow: '',
                buttonCallbackCalls: 0,
                selectedFilter: null,
                selectedFilterNarrow: null,
                stackedHeaderSearchTerm: '',
                stackedHeaderFilter: null,
                stackedButtonCallCount: 0,
                stackedOptionsCallCount: 0,
            },
        };
    }

    public renderDemoPage() {
        let { width, overrides } = this.state;
        const {
            callbackPresses,
            searchBar,
            searchBarNarrow,
            buttonCallbackCalls,
            selectedFilter,
            selectedFilterNarrow,
            stackedHeaderSearchTerm,
            stackedHeaderFilter,
            stackedButtonCallCount,
            stackedOptionsCallCount,
        } = overrides;

        return (
            <>
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Page header
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        {samples.PAGE_HEADER_DESCRIPTION}
                    </div.OverviewDescription>
                </div.Overview>

                {/* DEFAULT */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default
                        </div.DemoTitle>

                        <PageHeader config={configs.defaultConfig} />

                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.defaultConfig} />
                </div.Demo>

                {/* TEXT COLOR */}
                <div.Demo data-cy='text-color' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Text Color
                        </div.DemoTitle>

                        <PageHeader config={configs.textColorConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.textColorConfig} />
                </div.Demo>

                {/** HIGHLIGHT COLOR */}
                <div.Demo data-cy='highlight-color' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Highlight Color
                        </div.DemoTitle>

                        <PageHeader config={configs.highlightColorConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.highlightColorConfig} />
                </div.Demo>

                {/* LINKS */}
                <div.Demo data-cy='links' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Links
                        </div.DemoTitle>

                        <PageHeader config={configs.linksConfig} />

                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.linksConfig} />
                </div.Demo>

                {/* NAV BUTTONS CALLBACKS */}
                <div.Demo data-cy='callbacks' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callbacks
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='callback-result'>
                            The Callback was called: {callbackPresses} times
                        </div.CallbackResult>

                        <PageHeader config={configs.getCallbackCfg(this.incrementNumberOfPress.bind(this))} />

                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.callbacksSample} />
                </div.Demo>

                {/** OVERRIDES */}
                <div.Demo data-cy='overrides' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides
                        </div.DemoTitle>

                        <PageHeader config={configs.defaultConfig}
                            overrides={{ root: `background-color: ${colors.$lightGreen};` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.overridesConfig} />
                </div.Demo>

                {/** SEARCH BAR */}
                <div.Demo data-cy='header-search-bar' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Search Bar
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='search-value'>
                            {searchBar}
                        </div.CallbackResult>

                        <PageHeader config={configs.getSearchBarConfig(
                            searchBar,
                            (text) => this.handleSearchBarChange(text, 'searchBar'),
                            this.handleSearchBarSubmit.bind(this))
                        }
                            overrides={{ root: `width: ${NARROW_THRESHOLD + 10}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.searchBarConfig} />
                </div.Demo>

                {/** SEARCH BAR NARROW */}
                <div.Demo data-cy='search-bar-narrow' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Search Bar Narrow(&le; {WIDTH_XS}px)
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='search-value'>
                            {searchBarNarrow}
                        </div.CallbackResult>

                        <PageHeader config={configs.getSearchBarConfig(
                            searchBarNarrow,
                            (text) => this.handleSearchBarChange(text, 'searchBarNarrow'),
                            this.handleSearchBarNarrowSubmit.bind(this))
                        }
                            overrides={{ root: `width: ${WIDTH_XS - 200}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.searchBarConfig} />
                </div.Demo>

                {/** BUTTON */}
                <div.Demo data-cy='header-button' width={width} wide={true}>
                    <div.DemoArea data-cy='demo-area'>
                        <div.DemoTitle>
                            Header with Button
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='button-callback-calls'>
                            {buttonCallbackCalls}
                        </div.CallbackResult>

                        <PageHeader config={configs.getButtonConfig(this.incrementButtonCallbackCalls.bind(this))}
                            overrides={{ root: `width: ${NARROW_THRESHOLD + 10}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.buttonConfig} />

                </div.Demo>

                {/** FILTER */}
                <div.Demo data-cy='header-filter' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Header with Filter
                        </div.DemoTitle>

                        {
                            !!selectedFilter &&
                            <div.CallbackResult data-cy='header-selected-filter'>
                                {selectedFilter.label}
                            </div.CallbackResult>
                        }

                        <PageHeader config={configs.getFilterConfig(this.setFilter.bind(this))}
                            overrides={{ root: `width: ${NARROW_THRESHOLD + 10}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.filtersConfig} />
                </div.Demo>

                {/** FILTER NARROW */}
                <div.Demo data-cy='header-filter-narrow' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Header with Filter Narrow(&le; {WIDTH_XS}px)
                        </div.DemoTitle>

                        {
                            !!selectedFilterNarrow &&
                            <div.CallbackResult data-cy='header-narrow-selected-filter'>
                                {selectedFilterNarrow.label}
                            </div.CallbackResult>
                        }

                        <PageHeader config={configs.getFilterConfig(this.setFilterNarrow.bind(this))}
                            overrides={{ root: `width: ${WIDTH_XS - 200}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.filtersConfig} />
                </div.Demo>

                {/** FULLY STACKED HEADER */}
                <div.Demo data-cy='stacked-header' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Stacked header
                        </div.DemoTitle>

                        <div.DemoDescription>
                            A header containing all element (links, search bar, filters and button)
                        </div.DemoDescription>

                        {/** Search term result */}
                        <div.CallbackResult data-cy='search-term'>
                            {stackedHeaderSearchTerm}
                        </div.CallbackResult>

                        {/** Filter callback result */}
                        {
                            !!stackedHeaderFilter &&
                            <div.CallbackResult data-cy='filter'>
                                {stackedHeaderFilter.label}
                            </div.CallbackResult>
                        }

                        {/** BUTTON callback result */}
                        <div.DemoDescription>
                            Count for BUTTON callback calls:
                        </div.DemoDescription>

                        <div.CallbackResult data-cy='button-call-count'>
                            {stackedButtonCallCount}
                        </div.CallbackResult>

                        {/** OPTION callback result */}
                        <div.DemoDescription>
                            Count for OPTION callback calls:
                        </div.DemoDescription>

                        <div.CallbackResult data-cy='options-call-count'>
                            {stackedOptionsCallCount}
                        </div.CallbackResult>

                        <PageHeader config={configs.getStackedHeaderConfig(
                            this.setFilterStacked.bind(this),
                            this.incrementStackedButtonCallCount.bind(this),
                            stackedHeaderSearchTerm,
                            (text: string) => this.handleSearchBarChange(text, 'stackedHeaderSearchTerm'),
                            this.handleStackedHeaderSubmit.bind(this),
                            this.onStackedOptionPress.bind(this),
                        )}
                            overrides={{ root: `width: ${NARROW_THRESHOLD + 10}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.stackedHeaderConfig} />
                </div.Demo>

                {/** FULLY STACKED HEADER NARROW */}
                <div.Demo data-cy='stacked-header-narrow' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Stacked header narrow (&le; {NARROW_THRESHOLD}px)
                        </div.DemoTitle>

                        <div.DemoDescription>
                            A header containing all element (links, search bar, filters and button), but
                            having a narrower width to see how it behaves.
                        </div.DemoDescription>

                        {/** <!>Note the callbacks are same as above so make sure to not mix things up in testing. */}
                        <PageHeader config={configs.getStackedHeaderConfig(
                            this.setFilterStacked.bind(this),
                            this.incrementStackedButtonCallCount.bind(this),
                            stackedHeaderSearchTerm,
                            (text) => this.handleSearchBarChange(text, 'stackedHeaderSearchTerm'),
                            this.handleStackedHeaderSubmit.bind(this),
                            this.onStackedOptionPress.bind(this),
                        )}
                            overrides={{ root: `width: ${NARROW_THRESHOLD - 10}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.stackedHeaderConfig} />
                </div.Demo>

                {/** FULLY STACKED HEADER NARROWER */}
                <div.Demo data-cy='stacked-header-narrower' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Stacked header narrower (&le; {WIDTH_XS}px)
                        </div.DemoTitle>

                        <div.DemoDescription>
                            A header containing all element (links, search bar, filters and button), but
                            having a narrower width to see how it behaves.
                        </div.DemoDescription>

                        {/** <!>Note the callbacks are same as above so make sure to not mix things up in testing. */}
                        <PageHeader config={configs.getStackedHeaderConfig(
                            this.setFilterStacked.bind(this),
                            this.incrementStackedButtonCallCount.bind(this),
                            stackedHeaderSearchTerm,
                            (text) => this.handleSearchBarChange(text, 'stackedHeaderSearchTerm'),
                            this.handleStackedHeaderSubmit.bind(this),
                            this.onStackedOptionPress.bind(this),
                        )}
                            overrides={{ root: `width: ${WIDTH_XS - 10}px;` }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.stackedHeaderConfig} />
                </div.Demo>
            </>
        );
    }

    /** Callback example for options, increments calls count */
    private incrementNumberOfPress() {
        const { callbackPresses } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                callbackPresses: callbackPresses + 1,
            },
        });
    }

    /** Update state with input value */
    private handleSearchBarChange(searchTerm: string, field: string) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                [field]: searchTerm,
            },
        });
    }

    /** Example callback for submit */
    private handleSearchBarSubmit() {
        const { searchBar } = this.state.overrides;
        console.log(searchBar);
    }

    /** Example callback for submit used on narrow example */
    private handleSearchBarNarrowSubmit() {
        const { searchBarNarrow } = this.state.overrides;
        console.log(searchBarNarrow);
    }

    /** Callback example for button, increments calls count */
    private incrementButtonCallbackCalls() {
        const { buttonCallbackCalls } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                buttonCallbackCalls: buttonCallbackCalls + 1,
            },
        });
    }

    /** Filter callback for wide header example */
    private setFilter(filter: Filter) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                selectedFilter: { ...filter },
            },
        });
    }

    /** Filter callback for narrow header example */
    private setFilterNarrow(filter: Filter) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                selectedFilterNarrow: filter,
            },
        });
    }

    /** Filter callback for stacked header example */
    private setFilterStacked(filter: Filter) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                stackedHeaderFilter: filter,
            },
        });
    }

    /** Button callback for stacked header example, increments call count */
    private incrementStackedButtonCallCount() {
        const { stackedButtonCallCount } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                stackedButtonCallCount: stackedButtonCallCount + 1,
            },
        });
    }

    /** Example submit method for stacked header */
    private handleStackedHeaderSubmit() {
        const { stackedHeaderFilter } = this.state.overrides;
        console.log(stackedHeaderFilter);
    }

    /** Example callback for options, increments a count when clicking on any option */
    private onStackedOptionPress() {
        const { stackedOptionsCallCount } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                stackedOptionsCallCount: stackedOptionsCallCount + 1,
            }
        });
    }
}