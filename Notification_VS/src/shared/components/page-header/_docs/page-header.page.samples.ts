import { colors } from '../../../style/colors';

export const PAGE_HEADER_DESCRIPTION = `As the application grows, it will get harder and harder to keep track of your whereabouts, therefore every page should have a header indicating that out.
Other than that, the page header offers a few additional functionalities like:
    - a search bar to find things of interest more quickly;
    - possibility to filter content;
    - an additional button open to any functionality;
    - can have a list of links for quick access;
`;

export const defaultConfig = `
let defaultConfig: HeaderConfig = {
    title: 'FrontEnd',
};

// How to use it
<PageHeader config={defaultConfig} />
`;

export const textColorConfig = `
let config: HeaderConfig = {
    title: 'FrontEnd',
    // The color of text elements.
    textColor: ${colors.$blue},
    description: 'Learn to create awesome looking websites with our FrontEnd guide',
    filterBar: {
        filters: [
            {
                _id: 'highest_rated',
                label: 'Highest Rated'
            },
            {
                _id: 'bestseller',
                label: 'Bestseller'
            }
        ],
        selectFilter: (_filter) => {},
    },
    searchBar: {
        value: 'CSS guides',
        onChange: (_text) => {},
        onSubmit: () => {},
        clearSearch: () => {}
    },
    options: [
        {
            _id: 'projects',
            label: 'Projects',
        },
        {
            _id: 'lessons',
            label: 'Lessons',
        }
    ],
};
`;

export const highlightColorConfig = `
let config: HeaderConfig = {
    title: 'FrontEnd',
    // The color of text elements.
    highlightColor: ${colors.$green},
    description: 'Learn to create awesome looking websites with our FrontEnd guide',
    filterBar: {
        filters: [
            {
                _id: 'highest_rated',
                label: 'Highest Rated'
            },
            {
                _id: 'bestseller',
                label: 'Bestseller'
            }
        ],
        selectFilter: (_filter) => {},
    },
    searchBar: {
        value: 'CSS guides',
        onChange: (_text) => {},
        onSubmit: () => {},
        clearSearch: () => {}
    },
    options: [
        {
            _id: 'projects',
            label: 'Projects',
        },
        {
            _id: 'lessons',
            label: 'Lessons',
        }
    ],
};
`;

export const linksConfig = `
const linksConfig: HeaderConfig = {
    title: 'FrontEnd',
    options: [
        {
            _id: 'projects',
            label: 'Projects',
            callback: () => console.log('pressed')
        },
        {
            _id: 'lessons',
            label: 'Lessons',
            callback: () => console.log('pressed')
        }
    ]
};`;

export const callbacksSample = `
const customOptions: HeaderConfig = {
    options: [
        {
            _id: 'CSS',
            label: 'CSS',
            callback: () => customCallback(),
        },
        {
            _id: 'JS',
            label: 'JS',
            callback: () => customCallback(),
        },
    ]
}`;

export const overridesConfig = `
<PageHeader config={configs.defaultConfig}
    overrides={{ root: 'background-color: ${colors.$lightGreen};'}}/>
`;

export const searchBarConfig = `
let headerConfig: HeaderConfig = {
    title: 'FrontEnd',
    searchBar: {
        // The value of the search bar is stored in the parent page of
        // the header as presumably that is where you'd want to filter
        // the content based on the search term.
        value: context.state.overrides.searchBar,
        onChange: (text) => this.setState({
            searchTerm: text
        }),
        // Submit method must be provided, here the functionality for
        // search will be described.
        onSubmit: () => console.log(this.state.searchTerm),
        // Method called only on smaller screens, where, when extending the
        // search bar, a clear button appears inside of it.
        clearSearch: () => this.setState({ searchTerm: '' }),
    },
});

<PageHeader {...headerConfig} />
`;

export const buttonConfig = `
let headerConfig: HeaderConfig = {
    title: 'FrontEnd',
    button: {
        onPress: () => this.setState({
            count: this.state.count + 1,
        }),
        text: 'NEW PACKAGE',
        icon: {
            type: 'svg',
            svgPath: NEW_PACKAGE
        }
    },
});

<PageHeader {...headerConfig} />
`;

export const filtersConfig = `
let headerConfig: HeaderConfig = {
    title: 'FrontEnd',
    filters: [{
        _id: 'projects',
        label: 'Projects',
        callback: (filter) => this.setState({ selectedFilter: filter }),
    }, {
        _id: 'lessons',
        label: 'Lessons',
        callback: (filter) => this.setState({ selectedFilter: filter }),
    }]
}

<PageHeader {...headerConfig} />
`;

export const stackedHeaderConfig = `
/** Filters */
const filterCategories: IHeaderFilter[] = [
    {
        _id: 'projects',
        label: 'Projects',
        callback: (filter) => filterCallback(filter),
    },
    {
        _id: 'lessons',
        label: 'Lessons',
        callback: (filter) => filterCallback(filter),
    }
];

/** Button */
const buttonConfig: PageHeaderButton = {
    onPress: buttonCallback,
    text: 'NEW PACKAGE',
    icon: {
        type: 'svg',
        svgPath: NEW_PACKAGE
    }
};

/** Search bar */
const searchBarConfig: PageHeaderSearchBar = {
    value: searchBarValue,
    onChange: onChange,
    onSubmit: onSubmit,
    clearSearch: () => this.setState({ searchBarValue: '' }),
};

/** Options */
const options: IHeaderOption[] = [
    {
        _id: 'css',
        label: 'CSS',,
        callback: optionCallback,
    },
    {
        _id: 'js',
        label: 'JS',
        callback: optionCallback,
    }
];

/** Assemble config */
let config: HeaderConfig = {
    title: 'FrontEnd',
    filterBar: {
        filters: filterCategories,
        selectFilter: (filter) => setFilter(filter),
    }
    button: buttonConfig,
    searchBar: searchBarConfig,
    options,
};

<PageHeader {...config} />
`;