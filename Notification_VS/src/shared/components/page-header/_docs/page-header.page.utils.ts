import { NEW_PACKAGE } from '../../../assets/icons';
import { IDropdownOption } from '../../../interfaces/dropdown';
import {
    Filter,
    HeaderConfig,
    PageHeaderButton,
    PageHeaderSearchBar
    } from '../../../interfaces/page-header';
import { colors } from '../../../style/colors';

export const defaultConfig: HeaderConfig = {
    title: 'FrontEnd',
};

export const textColorConfig: HeaderConfig = {
    title: 'FrontEnd',
    textColor: colors.$green,
    description: 'Learn to create awesome looking websites with our FrontEnd guide',
    filterBar: {
        filters: [
            {
                _id: 'highest_rated',
                label: 'Highest Rated'
            },
            {
                _id: 'bestseller',
                label: 'Bestseller'
            }
        ],
        selectFilter: (_filter) => {},
    },
    searchBar: {
        value: 'CSS guides',
        onChange: (_text) => {},
        onSubmit: () => {},
        clearSearch: () => {}
    },
    options: [
        {
            _id: 'projects',
            label: 'Projects',
        },
        {
            _id: 'lessons',
            label: 'Lessons',
        }
    ],
};

export const highlightColorConfig: HeaderConfig = {
    title: 'FrontEnd',
    highlightColor: colors.$green,
    description: 'Learn to create awesome looking websites with our FrontEnd guide',
    filterBar: {
        filters: [
            {
                _id: 'highest_rated',
                label: 'Highest Rated'
            },
            {
                _id: 'bestseller',
                label: 'Bestseller'
            }
        ],
        selectFilter: (_filter) => {},
    },
    searchBar: {
        value: 'CSS guides',
        onChange: (_text) => {},
        onSubmit: () => {},
        clearSearch: () => {}
    },
    options: [
        {
            _id: 'projects',
            label: 'Projects',
        },
        {
            _id: 'lessons',
            label: 'Lessons',
        }
    ],
};

export const linksConfig: HeaderConfig = {
    title: 'FrontEnd',
    options: [
        {
            _id: 'projects',
            label: 'Projects',
            callback: () => console.log('pressed')
        },
        {
            _id: 'lessons',
            label: 'Lessons',
            callback: () => console.log('pressed')
        }
    ]
};

export const getCallbackCfg = (callback: () => void): HeaderConfig => {
    return ({
        title: 'FrontEnd',
        options: [
            {
                _id: 'CSS',
                label: 'CSS',
                callback: callback,
            },
            {
                _id: 'JS',
                label: 'JS',
                callback: callback,
            }
        ]
    });
};

export const getSearchBarConfig = (searchBarValue: string, onChange: (text: string) => void, onSubmit: () => void): HeaderConfig => {
    return ({
        title: 'FrontEnd',
        searchBar: {
            value: searchBarValue,
            onChange: onChange,
            onSubmit: onSubmit,
            clearSearch: () => console.log('Clear'),
        },
    });
};

export const getButtonConfig = (onPress: () => void): HeaderConfig => {
    return ({
        title: 'FrontEnd',
        button: {
            onPress: onPress,
            text: 'NEW PACKAGE',
            icon: {
                type: 'svg',
                svgPath: NEW_PACKAGE
            }
        },
    });
};

export const getFilterConfig = (callback: (filter: Filter) => void): HeaderConfig => {
    const filterCategories: Filter[] = [
        {
            _id: 'projects',
            label: 'Projects',
            callback: (filter) => callback(filter),
        },
        {
            _id: 'lessons',
            label: 'Lessons',
            callback: (filter) => callback(filter),
        }
    ];

    return ({
        title: 'FrontEnd',
        filterBar: {
            filters: filterCategories,
            selectFilter: (filter) => callback(filter),
        }
    });
};

export const getStackedHeaderConfig = (
    filterCallback: (filter: Filter) => void,
    buttonCallback: () => void,
    searchBarValue: string,
    onChange: (text: string) => void,
    onSubmit: () => void,
    optionCallback: () => void,
): HeaderConfig => {
    /** Filters */
    const filterCategories: Filter[] = [
        {
            _id: 'projects',
            label: 'Projects',
            callback: (filter) => filterCallback(filter),
        },
        {
            _id: 'lessons',
            label: 'Lessons',
            callback: (filter) => filterCallback(filter),
        }
    ];

    /** Button */
    const buttonConfig: PageHeaderButton = {
        onPress: buttonCallback,
        text: 'NEW PACKAGE',
        icon: {
            type: 'svg',
            svgPath: NEW_PACKAGE
        }
    };

    /** Search bar */
    const searchBarConfig: PageHeaderSearchBar = {
        value: searchBarValue,
        onChange: onChange,
        onSubmit: onSubmit,
        clearSearch: () => console.log('Clear'),
    };

    /** Options */
    const options: IDropdownOption[] = [
        {
            _id: 'css',
            label: 'CSS',
            callback: optionCallback,
        },
        {
            _id: 'js',
            label: 'JS',
            callback: optionCallback,
        }
    ];

    /** Assemble config */
    let config: HeaderConfig = {
        title: 'FrontEnd',
        description: 'Give life to your application using our FrontEnd lessons.',
        filterBar: {
            filters: filterCategories,
            selectFilter: (filter) => filterCallback(filter),
        },
        button: buttonConfig,
        searchBar: searchBarConfig,
        options,
    };

    return config;
};