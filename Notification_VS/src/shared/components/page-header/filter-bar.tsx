import * as div from './filter-bar.style';
import { Icon } from '../../../icons/components/icon/icon';
import { WIDTH_XS } from '../../constants/adaptive.const';
import { Filter } from '../../interfaces/page-header';
import { colors } from '../../style/colors';
import React from 'react';

interface Props {
    width: number;
    filters: Filter[];
    textColor: string;
    highlightColor: string;
    selectFilter: (filter: Filter) => void;
}

/**
 * Custom filter bar with adaptive layout.
 * Collapses to show only the icon on narrow screens.
 * The filters are displayed in a dropdown that is triggered by clicking on the
 * filter icon.
 *
 * <!>Look into the age old problem with Android where touchable elements outside
 * the parent bounds can't be clicked as this is affected by it.
 */
export const FilterBar: React.FunctionComponent<Props> = (props: Props) => {
    const { width, filters, textColor, highlightColor } = props;
    let [isExtended, setIsExtended] = React.useState(false);
    let [selectedFilter, setSelectedFilter] = React.useState(props.filters[0]);

    /** Adaptive layout */
    if (width <= WIDTH_XS) {
        return (
            <div.FilterBarMobile data-cy='filter-bar-mobile'>
                <div.ToggleDropdown data-cy='toggle-dropdown-mobile'
                    onPress={() => setIsExtended(!isExtended)}
                    underlayColor='none'>

                    <Icon path={div.FILTER_ICON} color={getHighlightColor(highlightColor)} />
                </div.ToggleDropdown>

                {/** Filters */}
                {
                    isExtended &&
                    <div.FiltersDropdownMobile data-cy='filters-dropdown-mobile'>
                        {
                            filters.map(filter =>
                                <div.FilterMobile data-cy='filter-mobile'
                                    key={filter._id}
                                    onPress={() => onSelectFilter(filter)}
                                    underlayColor='none'>

                                    {/** Label */}
                                    <div.Label data-cy='label'
                                        textColor={textColor}>
                                        {filter.label}
                                    </div.Label>
                                </div.FilterMobile>
                            )
                        }
                    </div.FiltersDropdownMobile>
                }
            </div.FilterBarMobile>
        );
    }

    return (
        <div.FilterBar data-cy='filter-bar'
            borderColor={highlightColor}>
            {/** Selected filter */}
            <div.SelectedFilter data-cy='selected-filter'
                textColor={textColor}>
                {!!selectedFilter ? selectedFilter.label : 'Filter'}
            </div.SelectedFilter>

            {/** Toggle dropdown */}
            <div.ToggleDropdown data-cy='toggle-dropdown'
                onPress={() => setIsExtended(!isExtended)}
                underlayColor='none'>

                {/** Icon */}
                <Icon path={div.FILTER_ICON} color={getHighlightColor(highlightColor)} />
            </div.ToggleDropdown>

            {/** Filters */}
            {
                isExtended &&
                <div.FiltersDropdown data-cy='filters-dropdown'>
                    {
                        filters.map(filter =>
                            <div.Filter data-cy='filter'
                                key={filter._id}
                                onPress={() => onSelectFilter(filter)}
                                underlayColor='none'>

                                {/** Label */}
                                <div.Label data-cy='label'
                                    textColor={textColor}>
                                    {filter.label}
                                </div.Label>
                            </div.Filter>
                        )
                    }
                </div.FiltersDropdown>
            }
        </div.FilterBar>
    );

    function onSelectFilter(filter: Filter) {
        const { selectFilter } = props;
        setSelectedFilter(filter);
        selectFilter(filter);
    }
};

function getHighlightColor(highlightColor: string): string {
    return !!highlightColor ? highlightColor : colors.$blue;
}