import * as div from './page-header.style';
import { Tools } from './tools';
import { HeaderConfig } from '../../interfaces/page-header';
import * as React from 'react';
import { LayoutChangeEvent } from 'react-native';

interface Props {
    config: HeaderConfig;
    overrides?: {
        root: string;
    };
}
interface State {
    width: number;
}

/**
 * Custom header with utilities like search bar, filters, a button
 * and some navigation buttons.
 *
 * When navigation buttons are present the header splits in 2 rows,
 * one containing the title and description and the other having the
 * utilities.
 */
export class PageHeader extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: 0,
        };
    }

    public render() {
        const { config, overrides } = this.props;
        const { width } = this.state;
        const { title, textColor, highlightColor, description, options, searchBar, filterBar, button, activeOption } = config;
        let shouldRenderTools = !!options || !!searchBar || !!filterBar || !!button;

        return (
            <div.PageHeader data-cy='page-header'
                width={width}
                onLayout={(ev) => this.updateWidth(ev)}
                override={!!overrides && overrides.root}>

                <div.Details data-cy='details'>

                    {/** Title */}
                    <div.Title data-cy='title' color={textColor}>
                        {title}
                    </div.Title>

                    {/** Description */}
                    {
                        !!description &&
                        <div.Description data-cy='description' color={textColor}>
                            {description}
                        </div.Description>
                    }
                </div.Details>

                {/** Utilities */}
                {
                    shouldRenderTools &&
                    <Tools width={width}
                        options={options}
                        activeOption={activeOption}
                        searchBar={searchBar}
                        filterBar={filterBar}
                        button={button}
                        textColor={textColor}
                        highlightColor={highlightColor}/>
                }
            </div.PageHeader>
        );
    }

    private updateWidth(e: LayoutChangeEvent) {
        const { width } = e.nativeEvent.layout;

        this.setState({
            width,
        });
    }
}