import * as div from './options.style';
import { DropdownConfig, IDropdownOption } from '../../interfaces/dropdown';
import { Dropdown } from '../dropdown/dropdown';
import React from 'react';

interface Props {
    options: IDropdownOption[];
    selectedOption: IDropdownOption;
    /** Compress the options in a dropdown */
    compressed: boolean;
    textColor: string;
    highlightColor: string;
    setSelectedOption: (option: IDropdownOption) => void;
}

/**
 * Header navigation buttons.
 * They compress in a dropdown when screen
 * width is narrow and the search bar extends.
 */
export const Options: React.FunctionComponent<Props> = (props: Props) => {

    const { options, selectedOption, compressed, setSelectedOption, textColor, highlightColor } = props;

    let dropdownConfig: DropdownConfig = {
        options: options,
        selected: selectedOption,
        callback: (item) => selectOption(item, (item) => setSelectedOption(item)),
    };

    if (compressed) {
        return (
            <div.OptionsCompressed data-cy='options-compressed'>
                <Dropdown config={dropdownConfig} overrides={div.dropdownOverrides} />
            </div.OptionsCompressed>
        );
    }

    return (
        <div.Options data-cy='options'>
            {
                options.map(option =>
                    <div.Option data-cy='option' key={option._id}
                        onPress={() => selectOption(option, (option) => setSelectedOption(option))}
                        underlayColor='none'>

                        <div.OptionName data-cy='option-name'
                            textColor={textColor}
                            highlightColor={highlightColor}
                            isSelected={selectedOption._id === option._id}>
                            {option.label}
                        </div.OptionName>
                    </div.Option>
                )
            }
        </div.Options>
    );
};

function selectOption(option: IDropdownOption, setSelectedOption: (option: IDropdownOption) => void) {
    setSelectedOption(option);

    /** Failsafe */
    if (!!option.callback) {
        option.callback(option);
    }
}