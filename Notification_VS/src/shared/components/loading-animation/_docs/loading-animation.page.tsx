import * as samples from './loading-animation.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { LoadingAnimation } from '../loading-animation';
import * as React from 'react';

interface LoadingAnimationState { }

export class LoadingAnimationPage extends ScrollableDocPage<LoadingAnimationState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Loading Animation
                        </div.OverviewTitle>
                    <div.OverviewDescription>
                        Loading Animation is a component which is used
                        to display the loading state on any page that
                        takes part of the app. It can have a custom size
                        and also a custom color.
                        </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Classic Loader
                        </div.DemoTitle>

                        <LoadingAnimation color={'black'} size={40} />
                    </div.DemoArea>
                    <CodeEditor
                        height={150}
                        {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Editable */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Editable loader size
                        </div.DemoTitle>

                        <LoadingAnimation
                            color={'black'}
                            size={50}
                        />
                    </div.DemoArea>
                    <CodeEditor
                        height={150}
                        {...codeSampleConfig}
                        srcCode={samples.EDITABLE} />
                </div.Demo>

                {/** Colored */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Editable loader color
                        </div.DemoTitle>

                        <LoadingAnimation
                            color={'blue'}
                            size={50}
                        />
                    </div.DemoArea>
                    <CodeEditor
                        height={150}
                        {...codeSampleConfig}
                        srcCode={samples.COLORED} />
                </div.Demo>
            </>
        );
    }
}