export const DEFAULT = `<LoadingAnimation
    // Changes the color of the Loader
    color={'black'}
    size={40} />
`;

export const EDITABLE = `<LoadingAnimation
    // Changes the color of the loader
    color={'black'}
    // Changes the size of the loader
    size={50} />
`;

export const COLORED = `<LoadingAnimation
    // Changes the color of the loader
    color={'blue'}
    // Changes the size of the loader
    size={50} />
`;