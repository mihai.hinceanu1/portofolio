import styled from 'styled-components/native';

export const LoadingAnimation = styled.View`
    justify-content: center;
    align-items: center;
    align-self: center;
    width: 100%;
    height: 100%;
`;