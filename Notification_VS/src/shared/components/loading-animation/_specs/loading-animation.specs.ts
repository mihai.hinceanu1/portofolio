/// <reference types="cypress" />

describe('Loader Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/loader');
    });

    it('Loads the loading animation', () => {
        cy.get('[data-cy=loading-animation]').eq(0);
    });

    it('Checks the size of the loader', () => {
        cy.get('[data-cy=activity]').eq(0).should('have.css', 'width' , '40px');
        cy.get('[data-cy=activity]').eq(0).should('have.css', 'height' , '40px');
    });

    it('Checks the color of the loader', () => {
        cy.get('[data-cy=activity]').eq(2).should('have.css', 'background-color' , 'rgba(0, 0, 0, 0)');
    });
});