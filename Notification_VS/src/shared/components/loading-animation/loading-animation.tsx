import * as div from './loading-animation.style';
import { colors } from '../../style/colors';
import * as React from 'react';
import { ActivityIndicator, Text } from 'react-native';

// TODO When changing the loader animation, remove these inputs.
// Convert to stateless component
interface Props {
    /** size of the loading animation */
    size: number | 'large' | 'small';
    /** color of the loading animation */
    color: string;
}

interface State { }

export class LoadingAnimation extends React.Component<Props, State> {

    public constructor(props: Props) {
        super(props);
    }

    public render() {
        const { size, color } = this.props;

        return (
            <div.LoadingAnimation data-cy='loading-animation'>

                <ActivityIndicator data-cy='activity' size={size} color={color} />
                {/* MOVE Styles outside */}
                <Text style={{
                    marginTop: 20,
                    color: colors.$menuBtnTextColor,
                    fontWeight: '600',
                    fontSize: 16
                }}>
                    Loading
                </Text>

            </div.LoadingAnimation>
        );

    }
}