import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

interface Props {
    width: number;
    height: number;
}

export const Image = styled.View<Props>`
    width: ${props => (props.width !== -1) ? props.width : 200}px;
    height: ${props => (props.height !== -1) ? props.height : 200}px;
    align-self: center;
`;

// TODO height: 100% is just a workaround --> further investigation is needed for the size of the tooltipLayer
export const TooltipLayer = styled(Image)`
    position: absolute;
    z-index: 10;
`;

export const ImageDescription = styled.Text`
    font-size: ${font.$size12}px;
    font-weight: 300;
    color: ${colors.$subheaderSmallColor};
    margin-bottom: 10px;
    align-self: center;
`;