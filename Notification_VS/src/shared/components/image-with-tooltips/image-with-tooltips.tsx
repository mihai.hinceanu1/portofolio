import * as div from './image-with-tooltips.style';
import { APP_CFG } from '../../config/app.config';
import { isNotUndefined } from '../../services/failsafe.utils';
import { FitImage } from '../fit-image/fit-image';
import * as React from 'react';
import { Image, Text, View } from 'react-native';

interface Props {
    imagePath: string;
    tooltips: any[];
    imageDescription?: string;
    /** TODO handle admin - always true maybe */
    isExpertOn?: boolean;
}

interface State {
    width: number;
    height: number;
}

/**
 * Image component which can render tooltips.
 * Requires path to an image and it's tooltips as Props.
 */
export class ImageWithTooltips extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: -1,
            height: -1,
        };
    }

    public render() {
        const { imagePath, tooltips, imageDescription } = this.props;
        const { width, height } = this.state;

        return (
            <View data-cy='image-with-tooltips'
                style={{ marginBottom: 25, position: 'relative' }}
            >

                <div.Image data-cy='image-container'
                    width={width} height={height}>
                    {
                        !!imagePath &&
                        <FitImage imgPath={imagePath} />
                    }
                </div.Image>

                {
                    !!tooltips.length &&
                    <div.TooltipLayer data-cy='tooltip-layer'
                        width={width}
                        height={height}>
                        <Text>Hello</Text>
                    </div.TooltipLayer>
                }

                {
                    isNotUndefined(imageDescription) &&
                    <div.ImageDescription data-cy='image-description'>
                        {imageDescription}
                    </div.ImageDescription>
                }

            </View>
        );
    }

    public componentDidMount() {
        const { imagePath } = this.props;
        if (imagePath.length) {
            this.initState();
        }
    }

    private initState() {
        const { imagePath } = this.props;
        Image.getSize(`${APP_CFG.assets}${imagePath}`,
            // Success
            (width, height) => {
                if (width > 600 || height > 600) {
                    let widthToHeightRatio = width / height;

                    if (width > height) {
                        width = 600;
                        height = width / widthToHeightRatio;
                    } else {
                        if (width < height) {
                            height = 600;
                            width = height * widthToHeightRatio;
                        } else {
                            height = 600;
                            width = 600;
                        }
                    }
                }
                this.setState({
                    width,
                    height,
                });
            },
            // Failure
            () => {
                this.setState({
                    width: 300,
                    height: 300,
                });
            },
        );
    }
}