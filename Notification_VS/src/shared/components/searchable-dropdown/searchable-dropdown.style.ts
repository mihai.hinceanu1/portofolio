import EStyleSheet from 'react-native-extended-stylesheet';

export const SearchableDropdownStyles = EStyleSheet.create({
    scrollContainer: {
        height: 300,
         width: 200,
    },
});