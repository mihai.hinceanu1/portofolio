import { SearchableDropdownStyles as styles } from './searchable-dropdown.style';
import { ITopic } from '../../../topics/interfaces/topic';
import { Input } from '../input/input';
import * as React from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { Subject } from 'rxjs';

interface Props {
    onItemSelect?: (topic: ITopic) => void;
    items: any[];
}

interface State {
    nameSearched: string;
    filteredItems: any[];
}

// TODO Convert to stylesheet
/**
 * WARNING! items need to be an array of Objects that have a name field
 * each item.name will be rendered inside the dropdown
 * TODO make it more generic (send the field that needs to be listed via params)
 */
export class SearchableDropdown extends React.Component<Props, State> {

    public static getDerivedStateFromProps(props: Props, state: State) {
        if (!state.filteredItems.length) {
            return {
                ...state,
                filteredItems: props.items,
            };
        } else {
            return null;
        }
    }

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            nameSearched: '',
            filteredItems: [],
        };
    }

    public render() {
        let { items, onItemSelect } = this.props;
        let { nameSearched, filteredItems } = this.state;

        return (
            <View data-cy='searchable-dropdown'>

                <View style={[styles.scrollContainer]}>
                    <Input value={nameSearched}
                        placeholder='Search...'
                        onChangeText={(text: string) => this.onChangeSearchInput(text)} />
                    <ScrollView>
                        {/** TODO option to attach new topic if none is attached */}

                        {
                            !!items.length &&
                            filteredItems.map(item =>
                                <TouchableOpacity key={item._id}
                                    onPress={() => onItemSelect(item)}>

                                    <Text style={{ color: 'white' }}>
                                        {item.name}
                                    </Text>

                                </TouchableOpacity>,
                            )
                        }
                        {
                            !items.length && <Text>Loading...</Text>
                        }
                    </ScrollView>
                </View>

            </View>
        );
    }

    public componentDidMount() { }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private onChangeSearchInput(text: string) {
        this.setState({
            nameSearched: text,
        });
        this.filterItemsBySearch(text);
    }

    private filterItemsBySearch(text: string) {
        let result = this.props.items;
        if (text) {
            result = result.filter(item => {
                return (item.name.toLowerCase().includes(text.toLowerCase()));
            });
        }
        this.setState({ filteredItems: result });
    }
}
