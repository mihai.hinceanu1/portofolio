/** Checked mark appearing inside the checkbox */
export const CHECK_MARK = 'M13.6656 1.19469e-06L6.33437 9.66563L2 5.33438L-6.41191e-07 7.33438L6.66562 14L16 2L13.6656 1.19469e-06Z';