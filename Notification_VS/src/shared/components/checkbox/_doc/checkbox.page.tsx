import * as utils from './checkbox.page.utils';
import * as samples from './checkbox.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Checkbox } from '../checkbox';
import * as React from 'react';
import { Text } from 'react-native';

interface CheckboxState {
    subscriptions: utils.Newsletter[];
}

export class CheckboxPage extends ScrollableDocPage<CheckboxState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                subscriptions: [utils.newsletters[1]],
            },
        };
    }

    public renderDemoPage() {
        let { width, overrides } = this.state;
        let { subscriptions } = overrides;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width} nativeID='checkbox-page'>
                    <div.OverviewTitle>
                        Checkbox
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Checkbox is used in various cases when user needs to make any kind of selection.
                    </div.OverviewDescription>
                </div.Overview>

                {/* Simple checkbox */}
                <div.Demo width={width} data-cy='simple-checkbox'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Classic Checkbox
                        </div.DemoTitle>

                        <Checkbox isChecked={subscriptions.includes(utils.newsletters[0])}
                            label='Subscribe to newsletter'
                            onPress={() => this.changeCheckboxValue(utils.newsletters[0])} />

                        {
                            subscriptions.includes(utils.newsletters[0]) &&
                            <Text data-cy='result'>Subscribed to {utils.newsletters[0].name}</Text>
                        }
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.SIMPLE_CHECKBOX} />
                </div.Demo>

                {/* Checkbox description */}
                <div.Demo width={width} data-cy='description'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Additional description
                        </div.DemoTitle>

                        <Checkbox isChecked={subscriptions.includes(utils.newsletters[1])}
                            onPress={() => this.changeCheckboxValue(utils.newsletters[1])}
                            label='Subscribe to newsletter'
                            description='Stay up to date with new courses, receive offers and updates information.' />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DESCRIPTION_CHECKBOX} />
                </div.Demo>

                {/* Override */}
                <div.Demo width={width} data-cy='overrides'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides
                        </div.DemoTitle>

                        <Checkbox isChecked={subscriptions.includes(utils.newsletters[2])}
                            onPress={() => this.changeCheckboxValue(utils.newsletters[2])}
                            label='Subscribe to newsletter'
                            overrides={{ root: 'padding: 30px' }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDES_CHECKBOX} />
                </div.Demo>
            </>
        );
    }

    /**
     * Because we have 3 test cases, each case is "subscribed" to one value from "utils.Newsletter",
     * even if the value itself doesn't render in the GUI.
     * This functionality is just for TESTING PURPOSE.
     */
    private changeCheckboxValue(subscribedNewsletter: utils.Newsletter) {
        const { subscriptions } = this.state.overrides;
        let subscriptionsCopy: utils.Newsletter[];

        if (subscriptions.includes(subscribedNewsletter)) {
            // remove
            subscriptionsCopy = subscriptions.filter(subscription => subscription !== subscribedNewsletter);
        } else {
            // add
            subscriptionsCopy = [...subscriptions];
            subscriptionsCopy.push(subscribedNewsletter);
        }

        this.setState({
            overrides: {
                subscriptions: subscriptionsCopy,
            }
        });
    }
}
