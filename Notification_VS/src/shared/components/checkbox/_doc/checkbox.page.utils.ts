export interface Newsletter {
    name: string;
}

export const newsletters: Newsletter[] = [
    { name: 'Medium newsletter' },
    { name: 'Duck accessories store newsletter' },
    { name: 'Manual cow impregnation blog newsletter' },
];