export const SIMPLE_CHECKBOX = `
let checkboxConfig = {
    // The parent controls if the checkbox is ticked or not.
    isChecked: this.state.isSubscribed,
    // Specifies what the user has to express his accord to
    label: 'Subscribe to newsletter',
    // Callback with instructions about what happens when checkbox is marked/unmarked
    onPress: () => {
        this.setState({
            isSubscribed: !this.state.isSubscribed
        });
    }
};

<Checkbox {...checkboxConfig} />
`;

export const DESCRIPTION_CHECKBOX = `
<Checkbox {...checkboxConfig}
    // Additional information about what happens when user ticks the checkbox
    description='Stay up to date with new courses, receive offers and updates information.'
/>
`;

export const OVERRIDES_CHECKBOX = `
<Checkbox {...checkboxConfig}
    // Override root styles
    overrides={{ root: 'padding: 30px' }} />
`;