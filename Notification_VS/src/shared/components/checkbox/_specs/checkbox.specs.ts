/// <reference types="cypress"/>

describe('Checkbox Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/checkbox');
        cy.get('#checkbox-page').should('exist');
    });

    it('Checkbox renders', () => {
        cy.get('[data-cy=checkbox]').should('exist');
    });

    // Added this before the click even if it's not major functionality
    // because for some reason the hover effect didn't reset.
    it('When checkbox is hovered, box borders have highlight effect', () => {
        let previousValue;
        getEl('simple-checkbox', 'custom-checkbox')
            .should('have.css', 'border-color').and($borderColor => {
                previousValue = $borderColor;
                expect(previousValue).to.eq(defaultBorderColor);
            });

        getEl('simple-checkbox', 'custom-checkbox')
            .trigger('mouseover')
            .should('have.css', 'border-color').and($hoveredBorderColor => {
                expect($hoveredBorderColor).to.not.eq(previousValue);
            });
    });

    it('When checkbox is clicked it toggles tick mark', () => {
        getEl('simple-checkbox', 'custom-checkbox').click();
        cy.get('[data-cy=tick]').should('exist');

        // Reset so it doesn't interfere
        getEl('simple-checkbox', 'custom-checkbox').click();
    });

    it('Callback is called', () => {
        getEl('simple-checkbox', 'result').should('not.exist');
        getEl('simple-checkbox', 'custom-checkbox').click();
        getEl('simple-checkbox', 'result').should('exist');
    });

    it('Checked status is handled in parent', () => {
        getEl('description', 'tick').should('exist');
    });

    it('Can have label', () => {
        getEl('simple-checkbox', 'label').should('exist');
    });

    it('Can have description', () => {
        getEl('description', 'description').should('exist');
    });

    it('Root styling can be overwritten', () => {
        getEl('overrides', 'checkbox').should('have.css', 'padding', '30px');
    });
});

// ====== ASSETS ======

var defaultBorderColor = 'rgb(127, 127, 127)';

// ====== SELECTORS ======

function getEl(useCase, element) {
    return cy.get(`[data-cy=${useCase}] [data-cy=${element}]`);
}