import * as div from './checkbox.style';
import * as React from 'react';

interface Props {
    label: string;

    /** Flag received from the parent, as it controls whether or not the checkbox is marked */
    isChecked: boolean;

    onPress: () => void;

    /** Additional explanations/instructions provided for the user if the checkbox label is not enough */
    description?: string;

    overrides?: {
        root?: string;
    };
}

interface State {
    isHovered: boolean;
}

/**
 * A GUI widget that permits the user to make a binary choice.
 * The functionality is handled by the parent through a callback method
 * given as a prop.
 */
export class Checkbox extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            isHovered: false,
        };
    }

    public render() {
        const { label, overrides, description, isChecked, onPress } = this.props;
        const { isHovered } = this.state;

        return (
            <div.Checkbox data-cy='checkbox' overrides={overrides && overrides.root}>

                {/** Always visible */}
                <div.CheckAndLabel data-cy='check-and-label'>

                    {/** Checkbox */}
                    <div.CustomCheckBox data-cy='custom-checkbox'
                        isHovered={isHovered}
                        checkBoxActive={isChecked}
                        onMouseEnter={() => this.setHoveredState()}
                        onMouseLeave={() => this.setHoveredState()}
                        onPress={() => onPress()}>

                        {/** Check mark */}
                        {
                            isChecked &&
                            div.checkMarkSvg()
                        }
                    </div.CustomCheckBox>

                    {/** Label */}
                    <div.Label data-cy='label'>
                        {label}
                    </div.Label>

                </div.CheckAndLabel>

                {
                    // Description
                    description &&
                    <div.Description data-cy='description'>
                        {description}
                    </div.Description>
                }

            </div.Checkbox >
        );
    }

    private setHoveredState() {
        const { isHovered } = this.state;
        this.setState({ isHovered: !isHovered });
    }
}
