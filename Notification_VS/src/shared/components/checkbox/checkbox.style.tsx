import { CHECK_MARK } from './_assets/checkbox.assets';
import { ReactWebAttributes } from '../../interfaces/workarounds';
import { pathBoundingBox } from '../../services/raphael.utils';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface Overrides {
    overrides: {};
}

interface CustomCheckboxProps extends ReactWebAttributes {
    checkBoxActive: boolean;
    isHovered: boolean;
}

export const Checkbox = styled.View<Overrides>`
    min-height: 30px;
    ${props => props.overrides}
`;

export const CheckAndLabel = styled.View`
    flex-direction: row;
    align-self: flex-start;
    align-items: center;
`;

export const CustomCheckBox = styled.TouchableOpacity<CustomCheckboxProps>`
    width: 24px;
    height: 24px;
    margin-right: 10px;
    border-width: 2px;
    border-radius: 2px;
    border-color: ${props => getBorderColor(props)};
    align-items: center;
    justify-content: center;
    background-color: ${props => getBackgroundColor(props)};
`;

export const Label = styled.Text`
    font-size: ${font.$size16}px;
`;

export const Description = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
    margin-top: 5px;
    margin-left: 34px;
`;

// ====== UTILS ======

let getBorderColor = (props: CustomCheckboxProps) => {
    const { checkBoxActive, isHovered } = props;

    if (checkBoxActive || isHovered) {
        return colors.$blue;
    } else {
        return colors.$darkGrey;
    }
};

let getBackgroundColor = (props: CustomCheckboxProps) =>
    props.checkBoxActive ? colors.$blue : colors.$white;

// ====== SVG ======

export const checkMarkSvg = (): JSX.Element => {
    const { width, height } = pathBoundingBox(CHECK_MARK);

    return (
        <Svg width={width} height={height} data-cy='tick'>
            <Path d={CHECK_MARK} fill={colors.$white} />
        </Svg>
    );
};
