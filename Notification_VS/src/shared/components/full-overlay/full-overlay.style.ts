import { colors } from '../../style/colors';
import styled from 'styled-components/native';

export const FullOverlay = styled.View`
    height: 100%;
    width: 100%;
    z-index: 3; // On top of navigator and quick menu
    top: 0;
    left: 0;
    background-color: ${colors.$overlayBgr};
    transition: opacity 0.2s ease-in;
    opacity: 1;
    color: ${colors.$overlayText};
`;
// position: fixed;

export const CloseButton = styled.View`
    position: absolute;
    top: 55px;
    right: 55px;
    width: 50px;
    height: 50px;
    cursor: pointer;
    transition: background 0.2s;
`;

// &:hover {
//     // background: url(/images/icons/close-hover.png) no-repeat center center; // Parked, still pending on the design. Mabye delete.
//     background-color: $overlay-bgr-vivid;
// }

export const Content = styled.View`
    position: relative;
    padding: 50px 0 0 0;
    width: calc(100 % - 150px);
    height: 100 %;
    margin: 0 auto;
    box - sizing: border - box;
`;

//     // <!> Renamed from "hidden" to "closed"
//     //     There was a collision with bootstrab-grid-only.
//     &.closed {
//     opacity: 0;
//     pointer - events: none;
// }