import * as div from './full-overlay.style';
import { toggleSearchOverlay } from '../../../search/services/search.service';
import { Icon } from '../icon/icon';
import * as React from 'react';

interface Props {
    isVisible: boolean;
    closeOverlay?: () => void;
    cssClass?: string; // Better targeting of css styles (without extra divs)
}

interface State { }

// TODO remove if no longer used -> replaced by <Modal />
export class FullOverlay extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {

        return (
            <div.FullOverlay data-cy={`full-overlay`}>

                {/* Content */}
                <div.Content data-cy='content'>
                    {/* Project content (transclusion) */}
                    {this.props.children}
                </div.Content>

                {/* Close */}
                <div.CloseButton data-cy='close'>
                    <Icon icon={'/icons/close.png'} onPress={() => toggleSearchOverlay(false)} />
                </div.CloseButton>

            </div.FullOverlay>
        );
    }
}