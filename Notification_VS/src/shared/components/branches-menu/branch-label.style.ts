import { BRANCHES_DIRECTIONS } from '../../constants/branches-menu.const';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';


interface BranchLabelProps {
    branchesDirection: BRANCHES_DIRECTIONS;
}

interface TextProps {
    textColor: string;
}

export const BranchLabel = styled.TouchableHighlight<BranchLabelProps>`
    ${props => branchLabelTextAlign(props)}
`;

export const Text = styled.Text<TextProps>`
    font-size: ${font.$size16}px;
    font-weight: 600;
    color: ${props => getTextColor(props)};
`;

// ===== UTILS =====

let branchLabelTextAlign = (props: BranchLabelProps) => {
    if (props.branchesDirection === BRANCHES_DIRECTIONS.left) {
        return 'align-self: flex-end;';
    }
};

let getTextColor = (props: TextProps) =>
    props.textColor ? props.textColor : 'black';