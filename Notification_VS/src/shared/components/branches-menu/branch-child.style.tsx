import { EXTENSION_LINE } from './_assets/branches-menu.icons';
import {
    BRANCH_TOPIC_DISTANCE,
    COMPLETING_LINE_LENGTH,
    CURVED_BRANCH_WIDTH,
    STROKE
    } from './_constants/branches-menu.const';
import { getColor } from './branches-menu.utils';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import { DirectionsProps } from '../../interfaces/branches-menu';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface ChildProps {
    branchesDirection: BRANCHES_DIRECTIONS;
    textColor: string;
}

export const BranchChild = styled.View`
    height: 28px;
`;

export const ChildName = styled.Text<ChildProps>`
    ${props => getLeftOrRightPadding(props)};
    font-size: ${font.$size12}px;
    color: ${props => getTextColor(props)};
    font-weight: 500;
    text-align: ${props => getLeftOrRightAlign(props)};
`;

export const StubExtension = styled.View<DirectionsProps>`
    position: absolute;
    ${props => isStubExtLeftOrRight(props)}
    top: ${props => isStubExtTopOrBottom(props)}px;
`;

// ====== SVG ======

export const extensionLineSvg = (color: string) => (
    <Svg height={COMPLETING_LINE_LENGTH} width={9} >
        <Path d={EXTENSION_LINE} strokeWidth={STROKE} stroke={getColor(color)} />
    </Svg>
);

// ====== UTILS ======

let getLeftOrRightPadding = (props: ChildProps) =>
    props.branchesDirection === BRANCHES_DIRECTIONS.left ?
        `padding-right: ${BRANCH_TOPIC_DISTANCE}px` :
        `padding-left: ${BRANCH_TOPIC_DISTANCE}px`;

let getTextColor = (props: ChildProps) =>
    props.textColor ? props.textColor : colors.$black;

let getLeftOrRightAlign = (props: ChildProps) =>
    props.branchesDirection === BRANCHES_DIRECTIONS.left ? 'right' : 'auto';

let isStubExtLeftOrRight = (props: DirectionsProps) =>
    props.branchesDirection === BRANCHES_DIRECTIONS.right ?
        `left: -${CURVED_BRANCH_WIDTH + BRANCH_TOPIC_DISTANCE}px;` :
        `right: -${CURVED_BRANCH_WIDTH + BRANCH_TOPIC_DISTANCE + 8 / 2}px;`;

let isStubExtTopOrBottom = (props: DirectionsProps) =>
    props.stubDirection === STUB_DIRECTIONS.top ? 30 : -30;