import * as div from './branch-label.style';
import { BRANCHES_DIRECTIONS } from '../../constants/branches-menu.const';
import { IBranch } from '../../interfaces/branches-menu';
import * as React from 'react';

interface Props {
    branchesDirection: BRANCHES_DIRECTIONS;
    expandBranch: () => void;
    branch: IBranch;
    textColor: string;
}

export function BranchLabel(props: Props) {
    const { branchesDirection, expandBranch, branch, textColor } = props;

    return (
        <div.BranchLabel data-cy='branch-label'
            branchesDirection={branchesDirection}
            onPress={() => handleBranchPress()}
            underlayColor='transparent'>

            <div.Text data-cy='label-text'
                textColor={textColor} >
                {branch.name}
            </div.Text>
        </div.BranchLabel>
    );

    function handleBranchPress() {
        if (branch.onPress) {
            branch.onPress();
        }

        expandBranch();
    }
}