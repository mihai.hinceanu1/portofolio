import { testColor } from './branches-menu.page.utils';

export const branches = `let branches: IBranch[] = [
    {
        _id: 'web-apps',
        name: 'Web Apps',
        children: [
            {
                _id: 'css',
                name: 'Css',
            },
            {
                _id: 'javascript',
                name: 'Javascript'
            },
        ]
    }
];`;

export const emptyBranches = `let config: BranchesMenuConfig = {
    branches: [],

    // The content is displayed on the right side
    branchesDirection: BRANCHES_DIRECTIONS.right,

    // The menu will extend up
    stubDirection: STUB_DIRECTIONS.top,
}

<BranchesMenu config={config} />`;

export const defaultMenu = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
}

<BranchesMenu config={config} />`;

export const expandUpLeft = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.left,
    stubDirection: STUB_DIRECTIONS.top,
}`;

export const expandDownLeft = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.left,
    stubDirection: STUB_DIRECTIONS.bottom,
}`;

export const expandDownRight = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.bottom,
}`;

export const colorConfig = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,

    // Desired color
    color: '#00C3A7',
}`;

export const menuToggle = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
    // Assign a callback method (in this case just toggles a flag which will render a text)
    onTogglePress={() => handleTogglePress()}
}`;

export const customIcon = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
    icon: {
        svgPath: mockedIconPath,
    }
}`;

export const iconColor = `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
    icon: {
        color: '${testColor}',
    }
}`;

export const textColor= `let config: BranchesMenuConfig = {
    branches: branches,
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
    textColor: '${testColor}',
};`;