import * as samples from './branches-menu.page.samples';
import * as utils from './branches-menu.page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { BranchesCallbackConfigs, BranchesMenuConfig } from '../../../interfaces/branches-menu';
import { colors } from '../../../style/colors';
import { CodeEditor } from '../../code-editor/code-editor';
import { BranchesMenu } from '../branches-menu';
import * as React from 'react';
import { Text } from 'react-native';

interface BranchesMenuState {
    toggleCallbackCount: number;
    branchCallbackCount: number;
    childCallbackCount: number;
    [name: string]: number;
}

export class BranchesMenuPage extends ScrollableDocPage<BranchesMenuState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                toggleCallbackCount: 0,
                branchCallbackCount: 0,
                childCallbackCount: 0,
            }
        };
    }

    public renderDemoPage() {
        const { width, overrides } = this.state;
        const { toggleCallbackCount, branchCallbackCount, childCallbackCount } = overrides;
        const { toggleConfig, branchesCallbacksConfig, childrenCallbacksConfig } = this.getCallbackConfigs();

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle data-cy='branches-menu-page'>
                        Branches Menu
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        A menu where the items appear linked by branches which expands in 4 directions
                    </div.OverviewDescription>

                    <Text>- When no items are given the button will have a circle shape</Text>
                    <Text>- Can get callbacks for all items and sub-items</Text>
                    <Text>- Can expand upward or downward and can face either left or right</Text>
                    <Text>- Color can be changed, by default it is blue</Text>
                    <Text>- Can take a custom icon inside the toggle button, by default has a book symbol</Text>
                </div.Overview>

                {/* Branches */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>Branches</div.DemoTitle>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.branches} />
                </div.Demo>

                {/* Empty menu */}
                <div.Demo width={width} data-cy='empty-menu'>
                    <div.DemoArea>
                        <div.DemoTitle>Empty branches</div.DemoTitle>
                        <div.DemoDescription>Branches menu with empty data (empty items array)</div.DemoDescription>

                        <BranchesMenu config={utils.emptyConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.emptyBranches} />
                </div.Demo>

                {/* Up right */}
                <div.Demo width={width} data-cy='upward-right-side'>
                    <div.DemoArea>
                        <div.DemoTitle> Up Right </div.DemoTitle>
                        <div.DemoDescription>Presumably the most used case will be when the menu expands top and the items are on its right</div.DemoDescription>
                        <BranchesMenu config={utils.upRightConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.defaultMenu} />
                </div.Demo>

                {/* Up left */}
                <div.Demo width={width} data-cy='upward-left-side'>
                    <div.DemoArea>
                        <div.DemoTitle>Branches menu with content on the left</div.DemoTitle>

                        <BranchesMenu config={utils.upLeftConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.expandUpLeft} />
                </div.Demo>

                {/* Down left */}
                <div.Demo width={width} data-cy='downward-left-side'>
                    <div.DemoArea>
                        <div.DemoTitle>Down Left</div.DemoTitle>

                        <BranchesMenu config={utils.downLeftConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.expandDownLeft} />
                </div.Demo>

                {/* Down right */}
                <div.Demo width={width} data-cy='downward-right-side'>
                    <div.DemoArea>
                        <div.DemoTitle>Down Right</div.DemoTitle>

                        <BranchesMenu config={utils.downRightConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.expandDownRight} />
                </div.Demo>

                {/* Different toggle & branch color */}
                <div.Demo width={width} data-cy='different-color'>
                    <div.DemoArea>
                        <div.DemoTitle>Set a different color for the menu elements</div.DemoTitle>

                        <BranchesMenu config={utils.differentColorConfig} />

                        <Text data-cy='toggle-color'>{colors.$green}</Text>

                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.colorConfig} />
                </div.Demo>

                {/* Callback on toggle button */}
                <div.Demo width={width} data-cy='toggle-callback'>
                    <div.DemoArea>
                        <div.DemoTitle>Toggle callback</div.DemoTitle>
                        <div.DemoDescription>Toggle button can have a callback set so we can trigger other actions along with opening/collapsing the menu</div.DemoDescription>

                        <BranchesMenu config={toggleConfig} />

                        <Text data-cy='toggle-callback-count'>{toggleCallbackCount}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.menuToggle} />
                </div.Demo>

                {/* Callback on branch button */}
                <div.Demo width={width} data-cy='branch-callback'>
                    <div.DemoArea>
                        <div.DemoTitle>Branch callback</div.DemoTitle>
                        <div.DemoDescription>Branch can have callback set.</div.DemoDescription>

                        <BranchesMenu config={branchesCallbacksConfig} />

                        <Text data-cy='branch-callback-count'>{branchCallbackCount}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.defaultMenu} />
                </div.Demo>

                {/* Callback on branch children */}
                <div.Demo width={width} data-cy='branch-child-callback'>
                    <div.DemoArea>
                        <div.DemoTitle>Children callback</div.DemoTitle>
                        <div.DemoDescription>Children can also have callback set.</div.DemoDescription>

                        <BranchesMenu config={childrenCallbacksConfig} />

                        <Text data-cy='child-callback-count'>{childCallbackCount}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.defaultMenu} />
                </div.Demo>

                {/* Different icon in toggle button */}
                <div.Demo width={width} data-cy='different-icon'>
                    <div.DemoArea>
                        <div.DemoTitle>Toggle icon</div.DemoTitle>
                        <div.DemoDescription>Toggle button can have a custom icon, it will be given as a svg path.</div.DemoDescription>

                        <BranchesMenu config={utils.differentIconConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.customIcon} />
                </div.Demo>

                {/* Different icon color */}
                <div.Demo width={width} data-cy='different-icon-color'>
                    <div.DemoArea>
                        <div.DemoTitle>Icon color</div.DemoTitle>
                        <div.DemoDescription>The icon inside the toggle button can have custom color.</div.DemoDescription>

                        <BranchesMenu config={utils.differentIconColorConfig} />
                        <Text data-cy='icon-color'>{utils.testColor}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.iconColor} />
                </div.Demo>

                {/* Different text color */}
                <div.Demo width={width} data-cy='different-text-color'>
                    <div.DemoArea>
                        <div.DemoTitle>Text color</div.DemoTitle>
                        <div.DemoDescription>The branch label and its children can have custom color.</div.DemoDescription>

                        <BranchesMenu config={utils.textColor} />
                        <Text data-cy='text-color'>{utils.testColor}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.textColor} />
                </div.Demo>
            </>
        );
    }

    private incrementCallbackCount(count: string) {
        const { overrides } = this.state;

        this.setState({
            ...this.state,
            overrides: {
                ...overrides,
                [count]: overrides[count] + 1,
            }
        });
    }

    private getCallbackConfigs(): BranchesCallbackConfigs {
        let toggleConfig: BranchesMenuConfig = {
            ...utils.togglePressConfig,
            onTogglePress: () => this.incrementCallbackCount('toggleCallbackCount'),
        };

        let mockedBranchesWithCallbacks = utils.attachOnPressToBranches(utils.branches, () => this.incrementCallbackCount('branchCallbackCount'));
        let branchesCallbacksConfig: BranchesMenuConfig = {
            ...utils.upRightConfig,
            branches: mockedBranchesWithCallbacks,
        };

        let mockedBranchesChildrenWithCallbacks = utils.attachOnPressToChildren(utils.branches, () => this.incrementCallbackCount('childCallbackCount'));
        let childrenCallbacksConfig: BranchesMenuConfig = {
            ...utils.upRightConfig,
            branches: mockedBranchesChildrenWithCallbacks,
        };

        return {
            toggleConfig,
            branchesCallbacksConfig,
            childrenCallbacksConfig
        };
    }
}