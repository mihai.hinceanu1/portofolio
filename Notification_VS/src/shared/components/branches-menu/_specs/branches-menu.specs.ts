/// <reference types="cypress" />

describe('Branches menu tests', () => {
    it('Go to test page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/branches-menu');
        cy.get('[data-cy=branches-menu-page]').should('exist');
    });

    it('Menu without any items still renders', () => {
        getEl('empty-menu', 'branches-menu').should('exist');
    });

    it('Menu expands on toggle click', () => {
        toggleMenu(1);
        cy.get('[data-cy=branch]').should('exist');
        toggleMenu(1); // Reset for next test
    });

    it('Menu extends upward.', () => {
        getEl('upward-right-side', 'toggle-button').click().then($toggleButton => {
            cy.get('[data-cy=branch]').eq(0).should($item => {
                expect($item.position().top).be.below($toggleButton.position().top);
            });
        });
        getEl('upward-right-side', 'toggle-button').click(); // Reset for next test
    });

    it('Menu extends downward', () => {
        getEl('downward-right-side', 'toggle-button').click().then($toggleButton => {
            cy.get('[data-cy=branch]').eq(0).should($item => {
                expect($item.position().top).be.above($toggleButton.position().top);
            });
        });
        getEl('downward-right-side', 'toggle-button').click(); // Reset for next test
    });

    it('Menu is rendered on the left', () => {
        // Check when menu is rendered upward
        getUseCase('upward-left-side', () =>
            checkIfItemIsOnLeft('toggle-button', 'branches')
        );

        // Check when menu is rendered downward
        getUseCase('downward-left-side', () =>
            checkIfItemIsOnLeft('toggle-button', 'branches')
        );

    });

    it('Menu is rendered on the right', () => {
        // Check when menu extends upward
        getUseCase('upward-right-side', () =>
            checkIfItemIsOnRight('toggle-button', 'branches')
        );

        // Check when menu extends downward
        getUseCase('downward-right-side', () =>
            checkIfItemIsOnRight('toggle-button', 'branches')
        );
    });

    it('When given a different color, all items will have that color', () => {
        getUseCase('different-color', () => {
            // Toggle button has the desired color
            toggleMenu().within(() => {
                // Get the path inside the toggle button and
                // check that fill matches the desired color
                cy.get('path')
                    .should('have.attr', 'fill', '#00C3A7');
            });

            // Curved branches have the desired color
            cy.get('[data-cy=curve-svg]')
                .within(() => {
                    // The branch matches the desired color
                    cy.get('path')
                        .should('have.attr', 'stroke', 'rgb(0,195,167)');
                });
        });
    });

    it('If a callback is given to toggle button, the callback is called when toggle button is clicked', () => {
        getUseCase('toggle-callback', () => {
            toggleMenu();

            cy.get('[data-cy=toggle-callback-count]')
                .should($incrementedCount => {
                    expect(parseInt($incrementedCount.text())).to.be.greaterThan(0);
                });
        });
    });

    it('If a callback is given to a branch, it is called when the branch is clicked', () => {
        getUseCase('branch-callback', () => {
            toggleMenu();

            cy.get('[data-cy=branch-label]').first().click();

            cy.get('[data-cy=branch-callback-count]').should($incrementedCount => {
                expect(parseInt($incrementedCount.text())).to.be.greaterThan(0);
            });
        });
    });

    it('If a callback is given to a child, it is called when the child is clicked', () => {
        getUseCase('branch-child-callback', () => {
            // Open menu
            toggleMenu();

            // Expand branch
            cy.get('[data-cy=branch-label]').first().click();

            // Click on a child
            cy.get('[data-cy=child-button]').first().click();

            cy.get('[data-cy=child-callback-count]').should($incrementedCount => {
                expect(parseInt($incrementedCount.text())).to.be.greaterThan(0);
            });
        });
    });

    it('When a different icon is given it is rendered inside the toggle button', () => {
        getUseCase('different-icon', () => {
            cy.get('[data-cy=toggle-icon-path]').should('have.attr', 'd', mockedIcon);
        });
    });

    it('When giving a different icon color it changes correctly', () => {
        getUseCase('different-icon-color', () =>
            cy.get(`[data-cy=toggle-icon-path]`).should('have.attr', 'fill', 'orange')
        );
    });

    it('Can have a different text color', () => {
        // Open the menu
        getEl('different-text-color', 'toggle-button').click();
        getEl('different-text-color', 'branch-label').first().click();
        getEl('different-text-color', 'label-text').should('have.css', 'color', 'rgb(255, 165, 0)');
        getEl('different-text-color', 'child-name').should('have.css', 'color', 'rgb(255, 165, 0)');

        // Close for future tests
        getEl('different-text-color', 'toggle-button').click();
    });
});

// ====== ASSETS ======

var mockedIcon = `M10.306,0.000 C15.430,-0.037 18.866,3.195 20.291,6.867 C22.274,11.976 19.793,17.301 16.610,19.461C16.610,21.237 16.610,23.014 16.610,24.790 C16.610,25.515 16.708,26.479 16.485,27.011 C16.257,27.555 15.817,27.907 15.124,27.991 C14.546,28.062 14.114,27.689 13.721,27.513 C12.894,27.125 12.067,26.738 11.241,26.351 C10.997,26.235 10.754,26.120 10.511,26.004 C10.285,26.115 10.059,26.226 9.832,26.337 C9.104,26.693 8.375,27.050 7.647,27.407 C7.174,27.619 6.639,28.067 5.926,27.963 C5.314,27.874 4.900,27.466 4.706,26.956 C4.506,26.432 4.607,25.503 4.607,24.819 C4.607,23.083 4.607,21.347 4.607,19.612 C4.018,19.274 3.422,18.726 2.989,18.221 C2.291,17.405 1.739,16.660 1.187,15.675 C0.332,14.148 -0.279,11.521 0.130,9.125 C0.379,7.667 0.795,6.449 1.413,5.370 C2.802,2.946 4.901,1.174 7.876,0.337 C8.387,0.194 8.932,0.120 9.517,0.045 C9.780,0.030 10.043,0.015 10.306,0.000 ZM10.341,2.518 C10.095,2.534 9.848,2.551 9.602,2.567 C9.097,2.639 8.634,2.725 8.198,2.859 C5.674,3.635 3.666,5.611 2.885,8.128 C1.334,13.130 4.313,17.169 7.826,18.480 C8.489,18.728 9.703,19.057 10.666,18.949 C11.331,18.875 11.911,18.859 12.463,18.704 C15.118,17.959 16.960,16.259 17.938,13.839 C18.389,12.723 18.690,11.107 18.447,9.556 C17.839,5.675 14.822,2.513 10.341,2.518 ZM10.468,5.235 C10.726,5.235 10.904,5.327 11.022,5.466 C11.187,5.662 11.266,5.975 11.370,6.225 C11.590,6.764 11.810,7.302 12.030,7.840 C12.146,8.118 12.221,8.416 12.463,8.568 C12.661,8.693 12.971,8.677 13.266,8.712 C13.850,8.761 14.434,8.810 15.018,8.858 C15.266,8.889 15.558,8.869 15.739,8.967 C15.948,9.080 16.086,9.269 16.120,9.558 C16.144,9.758 16.067,9.922 15.991,10.030 C15.385,10.570 14.779,11.110 14.173,11.649 C13.967,11.823 13.696,11.993 13.607,12.281 C13.520,12.567 13.644,12.876 13.704,13.127 C13.835,13.694 13.966,14.260 14.097,14.827 C14.155,15.067 14.286,15.351 14.236,15.640 C14.186,15.934 13.994,16.142 13.728,16.220 C13.604,16.256 13.431,16.250 13.318,16.208 C12.633,15.787 11.949,15.366 11.264,14.945 C11.046,14.808 10.856,14.625 10.544,14.589 C10.211,14.550 9.982,14.793 9.785,14.916 C9.291,15.224 8.796,15.532 8.302,15.840 C8.084,15.977 7.863,16.184 7.579,16.236 C7.244,16.298 6.989,16.104 6.870,15.913 C6.610,15.497 7.174,13.656 7.303,13.115 C7.370,12.837 7.494,12.487 7.364,12.196 C7.261,11.964 7.019,11.806 6.835,11.652 C6.412,11.280 5.990,10.908 5.568,10.537 C5.369,10.369 5.104,10.194 4.972,9.964 C4.873,9.792 4.860,9.506 4.946,9.311 C5.022,9.137 5.160,9.003 5.342,8.934 C5.542,8.857 5.835,8.880 6.074,8.851 C6.638,8.804 7.203,8.757 7.767,8.710 C8.126,8.667 8.437,8.683 8.639,8.493 C8.809,8.331 8.880,8.068 8.978,7.833 C9.197,7.295 9.417,6.757 9.637,6.218 C9.746,5.958 9.828,5.648 10.000,5.448 C10.116,5.312 10.260,5.286 10.468,5.235 ZM10.501,8.352 C10.262,9.267 9.664,9.980 8.759,10.237 C8.488,10.314 8.109,10.257 7.817,10.353 C7.818,10.354 7.820,10.356 7.821,10.358 C8.383,10.794 9.154,11.541 9.027,12.632 C8.986,12.987 8.915,13.270 8.841,13.584 C9.530,13.166 10.328,12.685 11.375,13.125 C11.667,13.247 11.900,13.434 12.164,13.584 C11.715,11.979 12.231,11.115 13.188,10.353 C12.890,10.262 12.519,10.314 12.246,10.237 C11.566,10.046 11.116,9.628 10.803,9.068 C10.702,8.829 10.602,8.590 10.501,8.352 ZM14.592,20.620 C13.394,21.259 11.021,21.668 9.180,21.384 C8.239,21.239 7.416,21.031 6.651,20.722 C6.636,20.716 6.621,20.709 6.606,20.703 C6.607,22.358 6.608,24.013 6.609,25.667 C7.901,25.024 9.194,24.380 10.487,23.737 C11.857,24.387 13.227,25.038 14.597,25.688 C14.595,23.999 14.593,22.310 14.592,20.620 Z`;

// ====== SELECTORS ======

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

function getUseCase(useCase, instructions) {
    return cy.get(`[data-cy=${useCase}]`).within(instructions);
}

// ====== COMMANDS ======

function toggleMenu(index) {
    if (index) {
        return cy.get('[data-cy=toggle-button]').eq(index).click();
    }
    return cy.get('[data-cy=toggle-button]').click();
}

function checkIfItemIsOnLeft(root, item) {
    cy.get(`[data-cy=${root}]`)
        .click()
        .then($root => {
            cy.get(`[data-cy=${item}]`)
                .should($item => {
                    expect($item.position().left).be.lessThan($root.position().left);
                });
        });

    // Close the menu so it doesn't interfere with other tests.
    cy.get(`[data-cy=${root}]`)
        .click();
}

function checkIfItemIsOnRight(root, item) {
    cy.get(`[data-cy=${root}]`)
        .click()
        .then($root => {
            cy.get(`[data-cy=${item}]`)
                .should($item => {
                    expect($item.position().left).be.greaterThan($root.position().left);
                });
        });

    // Close the menu so it doesn't interfere with other tests.
    cy.get(`[data-cy=${root}]`)
        .click();
}
