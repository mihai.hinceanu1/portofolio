import { BranchChild } from './branch-child';
import { BranchLabel } from './branch-label';
import * as div from './branch.style';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import { IBranch } from '../../interfaces/branches-menu';
import * as React from 'react';

interface Props {
    branch: IBranch;
    branchesDirection: BRANCHES_DIRECTIONS;
    stubDirection: STUB_DIRECTIONS;
    isLast: boolean;
    color: string;
    textColor: string;
}

interface State {
    areChildrenExpanded: boolean;
}

/**
 * An item in branches menu.
 * Contains the curved svg.
 * Can expand to show sub-items.
 */
export class Branch extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            areChildrenExpanded: false,
        };
    }

    public render() {
        const { branch, branchesDirection, stubDirection, isLast, color, textColor } = this.props;
        const { areChildrenExpanded } = this.state;

        return (
            <div.Branch data-cy='branch'
                branchesDirection={branchesDirection}
                stubDirection={stubDirection}>

                {/** Curved Branch */}
                <div.CurveSvg data-cy='curve-svg' stubDirection={stubDirection}>
                    {div.branchSvg(stubDirection, branchesDirection, color)}
                </div.CurveSvg>

                <div.BranchContent data-cy='branch-content'
                    branchesDirection={branchesDirection}
                    stubDirection={stubDirection}>

                    {/* Branch Label */}
                    <BranchLabel branch={branch}
                        textColor={textColor}
                        branchesDirection={branchesDirection}
                        expandBranch={() => this.expandBranch()} />

                    {
                        // Branch Children
                        areChildrenExpanded && branch &&
                        <div.BranchChildren data-cy='branch-children'>
                            {
                                branch.children && branch.children.map(child =>
                                    <BranchChild key={child._id}
                                        child={child}
                                        branchesDirection={branchesDirection}
                                        stubDirection={stubDirection}
                                        isLast={isLast}
                                        color={color}
                                        textColor={textColor} />
                                )
                            }
                        </div.BranchChildren>
                    }
                </div.BranchContent>

            </div.Branch>
        );
    }

    private expandBranch() {
        this.setState({
            areChildrenExpanded: !this.state.areChildrenExpanded,
        });
    }
}