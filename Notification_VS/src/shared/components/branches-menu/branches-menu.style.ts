import { STROKE, TOGGLE_WIDTH } from './_constants/branches-menu.const';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import styled from 'styled-components/native';

interface Override {
    override: {};
}

interface BranchesMenu {
    stubDirection: STUB_DIRECTIONS;
}

interface BranchesProps {
    branchesDirection: BRANCHES_DIRECTIONS;
}

export const BranchesMenu = styled.View<BranchesMenu & Override>`
    ${props => paddingTopOrBottom(props)}
    ${props => expandTopOrBottom(props)}
    ${props => props.override}
`;

/** Position branches and stub relative to toggle button */
export const Branches = styled.View<BranchesProps>`
    ${props => directionLeftOrRight(props)}
`;

// ====== UTILS ======

function expandTopOrBottom(props: BranchesMenu) {
    if (props.stubDirection === STUB_DIRECTIONS.bottom) {
        return 'flex-direction: column-reverse;';
    }
}

let paddingTopOrBottom = (props: BranchesMenu) =>
    props.stubDirection === STUB_DIRECTIONS.bottom ?
        `padding-bottom: 30px;` :
        `padding-top: 30px;`;

function directionLeftOrRight(props: BranchesProps) {
    return props.branchesDirection === BRANCHES_DIRECTIONS.left ?
        `right: ${Math.floor(TOGGLE_WIDTH / 2) - STROKE + 1}px;` :
        `left: ${Math.floor(TOGGLE_WIDTH / 2) - STROKE + 1}px;`;
}