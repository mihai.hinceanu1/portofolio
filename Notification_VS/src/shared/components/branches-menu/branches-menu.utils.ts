import { LESSON_ICON } from './_assets/branches-menu.icons';
import { BranchesMenuIcon } from '../../interfaces/branches-menu';
import { colors } from '../../style/colors';

export function getColor(color: string, defaultColor?: string) {
    if (defaultColor) {
        return color ? color : defaultColor;
    }
    return color ? color : colors.$blue;
}

export function getIcon(icon: BranchesMenuIcon) {
    return icon && icon.svgPath ? icon.svgPath : LESSON_ICON;
}