import * as div from './branch-child.style';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import { BranchConfig } from '../../interfaces/branches-menu';
import * as React from 'react';
import { TouchableHighlight } from 'react-native';

interface Props {
    child: BranchConfig;
    branchesDirection: BRANCHES_DIRECTIONS;
    stubDirection: STUB_DIRECTIONS;
    isLast: boolean;
    color: string;
    textColor: string;
}

export function BranchChild(props: Props) {
    const { branchesDirection, child, stubDirection, isLast, color, textColor } = props;

    function handleChildPress() {
        if (child.onPress) {
            child.onPress();
        }
    }

    return (
        <div.BranchChild data-cy='branch-child'>

            {/* Child name */}
            <TouchableHighlight data-cy='child-button'
                onPress={() => handleChildPress()}
                underlayColor='transparent'>
                <div.ChildName data-cy='child-name'
                    textColor={textColor}
                    branchesDirection={branchesDirection}>
                    {child.name}
                </div.ChildName>
            </TouchableHighlight>

            {
                // Stub Extension
                !(isLast && stubDirection === STUB_DIRECTIONS.bottom) &&
                <div.StubExtension data-cy='stub-extension'
                    branchesDirection={branchesDirection}
                    stubDirection={stubDirection}>
                    {div.extensionLineSvg(color)}
                </div.StubExtension>
            }

        </div.BranchChild>
    );
}