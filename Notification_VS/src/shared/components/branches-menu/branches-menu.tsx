import { Branch } from './branch';
import * as div from './branches-menu.style';
import { ToggleButton } from './toggle-button';
import { BranchesMenuConfig } from '../../interfaces/branches-menu';
import { detectClickIsOutside, selfCloseEvents$ } from '../../services/self-close-signal.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

interface Props {
    config: BranchesMenuConfig;
    overrides?: {
        root?: string
    };
}

interface State {
    isExpanded: boolean;
}

/**
 * Renders a set of branches, diverging from a central stub
 * The stub start from an anchor point, the toggle button
 * Branches can have children items as well
 * For each clickable item a callback can be defined
 */
export class BranchesMenu extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            isExpanded: false,
        };
    }

    public render() {
        const { isExpanded } = this.state;
        const { overrides } = this.props;
        const { branches, stubDirection, branchesDirection, color, icon, textColor } = this.props.config;

        return (
            <div.BranchesMenu data-cy='branches-menu'
                stubDirection={stubDirection}
                override={overrides && overrides.root}
                pointerEvents='box-none'>

                {/* Branches */}
                <div.Branches data-cy='branches' branchesDirection={branchesDirection}>
                    {
                        isExpanded && branches &&
                        branches.map((branch, index) =>
                            <Branch key={branch._id}
                                branch={branch}
                                stubDirection={stubDirection}
                                branchesDirection={branchesDirection}
                                isLast={branches.length - 1 === index}
                                color={color}
                                textColor={textColor} />
                        )
                    }
                </div.Branches>

                {/** Toggle button */}
                <ToggleButton expandBranches={() => this.expandBranches()}
                    isExpanded={isExpanded}
                    stubDirection={stubDirection}
                    branchesDirection={branchesDirection}
                    branchesCount={branches.length}
                    icon={icon}
                    color={color} />

            </div.BranchesMenu>
        );
    }

    public componentDidMount() {
        this.subscribeToSelfClose();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToSelfClose() {
        selfCloseEvents$.pipe(
            takeUntil(this.destroyed$),
            filter(() => this.state.isExpanded)
        )
            .subscribe(event => {
                let clickOutside = detectClickIsOutside(event, this);

                if (clickOutside) {
                    this.expandBranches();
                }
            });
    }

    private expandBranches() {
        const { onTogglePress } = this.props.config;

        // Callback
        onTogglePress && onTogglePress();

        // Toggle
        if (!this.state.isExpanded) {
            this.displayMenu();
        } else {
            this.hideMenu();
        }
    }

    private displayMenu() {
        this.setState({
            isExpanded: true,
        });
    }

    private hideMenu() {
        this.setState({
            isExpanded: false,
        });
    }
}
