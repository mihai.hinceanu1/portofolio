import { getColor } from './branches-menu.utils';
import * as div from './toggle-button.style';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import { BranchesMenuIcon } from '../../interfaces/branches-menu';
import Color from 'color-js';
import * as React from 'react';

interface Props {
    expandBranches: () => void;
    stubDirection: STUB_DIRECTIONS;
    branchesDirection: BRANCHES_DIRECTIONS;
    branchesCount: number;
    color: string;
    icon: BranchesMenuIcon;
    isExpanded: boolean;
}

/** Used as a trigger to extend the branches menu. */
export function ToggleButton(props: Props) {
    const { expandBranches, stubDirection, branchesDirection, branchesCount, color, icon, isExpanded } = props;
    let [colorNuance, setColorNuance] = React.useState(Color(getColor(color)));

    return (
        /** Toggle Button  */
        <div.ToggleButton data-cy='toggle-button'
            underlayColor='transparent'
            onPressIn={() => handleExpandBranches(expandBranches)}
            onPressOut={() => handleColorChange()}
            branchesDirection={branchesDirection} >
            <>
                {/* Toggle Bgr */}
                {
                    (isExpanded && !!branchesCount) &&
                    <div.ToggleSvg data-cy='toggle-svg'>
                        {div.toggleButtonBgrSvg(stubDirection, colorNuance)}
                        {div.clickAreaSvg()}
                        {div.toggleButtonIconSvg(branchesCount, icon, isExpanded)}
                    </div.ToggleSvg>
                }

                {
                    (!isExpanded || !branchesCount) &&
                    <div.ToggleCircle data-cy='toggle-circle'
                        color={colorNuance}>
                        {div.clickAreaSvg()}
                        {div.toggleButtonIconSvg(branchesCount, icon, isExpanded)}
                    </div.ToggleCircle>
                }
            </>
        </div.ToggleButton>
    );

    function handleExpandBranches(expandBranches: () => void) {
        const { color } = props;
        expandBranches();

        // Darken only if it's the initial color.
        if (colorNuance === Color(getColor(color))) {
            setColorNuance(colorNuance.darkenByAmount(0.1));
        }
    }

    function handleColorChange() {
        const { color } = props;
        // Reset to the initial color;
        setColorNuance(Color(getColor(color)));
    }
}
