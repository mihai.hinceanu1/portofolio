import { WATER_DROP } from './_assets/branches-menu.icons';
import { TOGGLE_HEIGHT, TOGGLE_WIDTH } from './_constants/branches-menu.const';
import { getColor, getIcon } from './branches-menu.utils';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import { BranchesMenuIcon } from '../../interfaces/branches-menu';
import { pathBoundingBox } from '../../services/raphael.utils';
import { getShadow } from '../../style/shadow.style';
import Color from 'color-js';
import * as React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface ToggleButtonProps {
    branchesDirection: BRANCHES_DIRECTIONS;
}

interface ToggleCircleProps {
    color: Color;
}

export const ToggleButton = styled.TouchableHighlight<ToggleButtonProps>`
    width: ${TOGGLE_WIDTH}px;
    height: ${TOGGLE_HEIGHT}px;
    align-self: ${props => alignEndOrStart(props)};
    justify-content: center;
`;

/** TouchableHighlight will throw an error if a svg is a direct child therefore I added this View */
export const ToggleSvg = styled.View`
    justify-content: center;
    align-items: center;
`;

export const ToggleCircle = styled.View<ToggleCircleProps>`
    width: ${TOGGLE_WIDTH}px;
    height: ${TOGGLE_WIDTH}px;
    border-radius: ${TOGGLE_WIDTH / 2}px;
    background-color: ${props => props.color.toRGB().toString()};
    ${getShadow('box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);')}
`;

// ====== SVG ======

/**
 * We are using an unbalanced graphical element and we need to center
 * it such that the visual center is also matching the svg center.
 * This means that we can simply rotate the element without any further issues
 */
export const toggleButtonBgrSvg = (stubDirection: STUB_DIRECTIONS, color: Color) => {
    return (
        <Svg width={TOGGLE_WIDTH} height={TOGGLE_HEIGHT} style={getToggleBgrStyle(stubDirection)} >
            <Path d={WATER_DROP} fill={color.toRGB().toString()} />
        </Svg>
    );
};

/**
 * <!> Fixes issue with React Web not detecting clicks in the area of the toggleButtonBgrSvg().
 * REVIEW Not sure why the toggleButtonBgrSvg() does not detect clicks.
 * I noticed that clicks on toggleButtonIconSvg() were registered.
 * I created a bigger SVG to capture all clicks on the entire surface of the branches menu toggle.
 * If clicks on branches are not detected then the auto-close does not get triggered.
 */
export const clickAreaSvg = () => {
    return (
        <Svg width={65} height={65} style={{ position: 'absolute' }} />
    );
};

export const toggleButtonIconSvg = (branchesCount: number, icon: BranchesMenuIcon, isExpanded: boolean) => {
    let svgPath = getIcon(icon);
    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;

    return (
        <Svg width={width} height={height}
            style={getIconCenterPosition(branchesCount, width, height, isExpanded)}>
            <Path data-cy='toggle-icon-path' d={getIcon(icon)} fill={getIconColor(icon, 'white')} />
        </Svg >
    );
};

// ====== UTILS ======

const getToggleBgrStyle = (stubDirection: STUB_DIRECTIONS): StyleProp<ViewStyle> =>
    stubDirection === STUB_DIRECTIONS.bottom ? {
        transform: ([{ rotate: '180deg' }])
    } : {};

function getIconCenterPosition(branchesCount: number, width: number, height: number, isExpanded: boolean): StyleProp<ViewStyle> {
    let containerWidth: number = TOGGLE_WIDTH;
    let containerHeight: number;

    if (isExpanded && branchesCount) {
        containerHeight = TOGGLE_HEIGHT;
    } else {
        containerHeight = containerWidth;
    }

    let style: StyleProp<ViewStyle> = {
        top: containerHeight / 2 - height / 2,
        left: containerWidth / 2 - width / 2,
        position: 'absolute',
    };

    return style;
}

let alignEndOrStart = (props: ToggleButtonProps) =>
    props.branchesDirection === BRANCHES_DIRECTIONS.left ? 'flex-end' : 'flex-start';

function getIconColor(icon: BranchesMenuIcon, defaultColor: string): string {
    return icon ? getColor(icon.color, defaultColor) : getColor('', defaultColor);
}