/** Exact svg width for water drop */
export const TOGGLE_WIDTH = 65;

// Calculated like this so we can center icons correctly in the container
// because the sharp tip of the water drop would make it seem like the icon
// is misplaced.
/**
 * Exact svg height for water drop
 */
export const TOGGLE_HEIGHT = 2 * 81 - TOGGLE_WIDTH;

/** Default stroke width for all branches */
export const STROKE = 3;

/** Completing line/topic ratio */
export const COMPLETING_LINE_LENGTH = 32;

/** Exact svg width for curved branch */
export const CURVED_BRANCH_WIDTH = 43;

/** Exact svg height for curved branch */
export const CURVED_BRANCH_HEIGHT = 56;

/** The distance necessary for the branches to overlap */
export const BRANCHES_OVERLAP = 15;

/** The distance from the branch to the topic */
export const BRANCH_TOPIC_DISTANCE = 16;
