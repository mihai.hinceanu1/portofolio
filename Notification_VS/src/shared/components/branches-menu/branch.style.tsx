import { TOPIC_BRANCH } from './_assets/branches-menu.icons';
import {
    BRANCH_TOPIC_DISTANCE,
    BRANCHES_OVERLAP,
    CURVED_BRANCH_HEIGHT,
    CURVED_BRANCH_WIDTH,
    STROKE
    } from './_constants/branches-menu.const';
import { getColor } from './branches-menu.utils';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../constants/branches-menu.const';
import { DirectionsProps } from '../../interfaces/branches-menu';
import * as React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface CurveSvgProps {
    stubDirection: STUB_DIRECTIONS;
}

export const Branch = styled.View<DirectionsProps>`
    min-height: 30px;
    top: ${props => branchTopDistance(props)}px;
    flex-direction: ${props => expandTopOrBottom(props)};
`;

export const CurveSvg = styled.View<CurveSvgProps>`
    width: ${CURVED_BRANCH_WIDTH}px;
    height: ${CURVED_BRANCH_HEIGHT}px;
    position: absolute;
    ${props => getCurveTopDistance(props)};
`;

export const BranchContent = styled.View<DirectionsProps>`
    ${props => branchContentLeftOrRight(props)}
    ${props => branchContentTop(props)}
`;

export const BranchChildren = styled.View`
    padding-top: 5px;
`;

// ====== SVG ======

export const branchSvg = (stubDirection: STUB_DIRECTIONS, branchesDirection: BRANCHES_DIRECTIONS, color: string) => (
    <Svg style={getBranchStyle(stubDirection, branchesDirection)}
        width={CURVED_BRANCH_WIDTH}
        height={CURVED_BRANCH_HEIGHT}>

        <Path d={TOPIC_BRANCH.CURVED_BRANCH}
            stroke={getColor(color)}
            strokeWidth={STROKE}
            fill='none' />

        <Path d={TOPIC_BRANCH.SEPARATED_LINE}
            stroke={getColor(color)}
            strokeWidth={STROKE} />
    </Svg>
);

// ====== UTILS ======

function getBranchStyle(stubDirection: STUB_DIRECTIONS, branchesDirection: BRANCHES_DIRECTIONS): StyleProp<ViewStyle> {
    let style: StyleProp<ViewStyle> = {};

    if (stubDirection === STUB_DIRECTIONS.bottom) {
        branchesDirection === BRANCHES_DIRECTIONS.left ?
            style = {
                transform: ([{ scale: -1 }]),
            } :
            style = {
                transform: ([{ scaleY: -1 }]),
            };
    } else {
        if (branchesDirection === BRANCHES_DIRECTIONS.left) {
            style = {
                transform: ([{ scaleX: -1 }]),
            };
        }
    }

    return style;
}

let branchTopDistance = (props: DirectionsProps) => props.stubDirection === STUB_DIRECTIONS.bottom ? 30 : -9;

let expandTopOrBottom = (props: DirectionsProps) =>
    props.branchesDirection === BRANCHES_DIRECTIONS.left ? 'row-reverse' : 'row';

let getCurveTopDistance = (props: CurveSvgProps) => {
    if (props.stubDirection === STUB_DIRECTIONS.bottom) {
        return `top: -${CURVED_BRANCH_HEIGHT - BRANCHES_OVERLAP}px`;
    }
};

let branchContentLeftOrRight = (props: DirectionsProps) =>
    props.branchesDirection === BRANCHES_DIRECTIONS.left ?
        `right: ${CURVED_BRANCH_WIDTH + BRANCH_TOPIC_DISTANCE}px;` :
        `left: ${CURVED_BRANCH_WIDTH + BRANCH_TOPIC_DISTANCE}px;`;

let branchContentTop = (props: DirectionsProps) => {
    if (props.stubDirection === STUB_DIRECTIONS.top) {
        return 'top: -10px';
    }
};