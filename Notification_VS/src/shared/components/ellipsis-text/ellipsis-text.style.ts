import styled from 'styled-components/native';

interface Props {
    fontSize?: number;
    height?: number;
    width?: number;
}

export const EllipsisText = styled.View<Props>`
    min-width: ${props => (props.width > 0 || props.width !== undefined) ? props.width : 300}px;
    max-width: 50%;
    min-height: ${props => (props.height > 0 || props.height !== undefined) ? props.height : 50}px;
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    align-items: center;
`;

export const Content = styled.Text<Props>`
    font-size: ${props => props.fontSize}px;
    margin: 10px;
`;

export const Extend = styled.TouchableOpacity<Props>`
    width: 75px;
    /* height: ${props => props.fontSize}; */
    height: 20px;
    background: rgba(0, 0, 0, 0.2);
    margin-right: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
`;