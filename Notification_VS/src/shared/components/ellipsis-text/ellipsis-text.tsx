import * as div from './ellipsis-text.style';
import * as React from 'react';
import { Text } from 'react-native';

interface Props {
    maxLength: number;
    text: string;
    width: number;
    height: number;
}

interface State {
    extended: boolean;
}

/** EllipsisText component to display text in a limited space, with the possibility to expand. */
export class EllipsisText extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            extended: false,
        };
    }

    public render() {
        const { maxLength, text, width, height } = this.props;
        const { extended } = this.state;

        return (
            <div.EllipsisText data-cy='ellipsis-text' fontSize={14} width={width} height={height}>

                <div.Content fontSize={14}>
                    {extended ? text : this.manageTextByLength(maxLength, text)}
                </div.Content>
                <div.Extend data-cy='extend-btn' onPress={() => this.handleExtendClick()}>
                    <Text>{extended ? 'Show Less' : 'Show More'}</Text>
                </div.Extend>

            </div.EllipsisText>
        );
    }

    private handleExtendClick() {
        let extended = !this.state.extended;
        this.setState({
            extended,
        });
    }

    private manageTextByLength(maxLength: number, text: string) {
        if (text.length <= maxLength) {
            return text;
        } else {
            return text.slice(0, maxLength - 3) + '...';
        }
    }
}