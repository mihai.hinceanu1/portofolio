import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

export const UserDropdown = styled.View`
    flex-direction: column;
    height: 150px;
    justify-content: flex-start;
    align-items: center;
    width : 120px;
    background-color: rgb(250,250,250);
`;

export const ProfileSettingsTitle = styled.Text`
    font-size: ${font.$size16}px;
    margin-top: 10px;
    align-items: center;
    color: ${colors.$menuBtnTextColor};
`;

export const DelimitationBorder = styled.View`
    border-bottom-width: 1px;
    border-color: rgb(220,220,220);
    border-bottom-style: solid;
    width: 120px;
    margin-top: 10px;
`;

export const SignOut = styled.TouchableOpacity`
    align-self: center;
    justify-content: flex-end;
    margin-top: 5px;
`;

export const SignOutText = styled.Text`
    color: ${colors.$menuBtnTextColor};
    font-size: ${font.$size12}px;
`;

export const SeeProfileSettings = styled.Text`
    color: ${colors.$blue};
    font-size: ${font.$size12}px;
`;

export const SeeMyProfileSettings = styled.TouchableOpacity`
    margin-top: 5px;
`;