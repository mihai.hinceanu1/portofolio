import * as div from './user-dropdown.style';
import { signOut } from '../../../auth/services/auth.service';
import { User } from '../../../users/interfaces/user';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Params { }

interface Props extends RouteComponentProps<Params> {
    user: User;
    onMenuPress: () => void;
}

interface State { }

export class _UserDropdown extends React.Component<Props, State> {

    public constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { user } = this.props;

        return (
            <div.UserDropdown data-cy='user-dropdown'>

                <div.ProfileSettingsTitle>
                    {user.firstName + ' ' + user.lastName}
                </div.ProfileSettingsTitle>

                <div.SeeMyProfileSettings onPress={() => this.goToUserProfile()}>
                    <div.SeeProfileSettings>
                        My Profile
                    </div.SeeProfileSettings>
                </div.SeeMyProfileSettings>

                <div.SignOut onPress={() => this.signOut()}>
                    <div.SignOutText>
                        Sign Out
                    </div.SignOutText>
                </div.SignOut>
            </div.UserDropdown>
        );
    }

    private signOut() {
        const { history } = this.props;

        signOut();
        history.push('/auth/login');
    }

    private goToUserProfile() {
        const { onMenuPress, history, user } = this.props;

        onMenuPress();
        history.push(`/user/${user.pathName}`);
    }
}

export const UserDropdown = withRouter(_UserDropdown);
