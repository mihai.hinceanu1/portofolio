import * as div from './message-overlay.style';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    /** Text rendered in message overlay */
    text: string;

    /** triangle pointing to (some text / image) height */
    iconHeight: number;

    /** Not mandatory color of the box in which the message is rendered */
    overlayMessageBoxColor?: string;
}

/** Small overlay message, currently used for adding a message(tooltip) when hovering buttons */
export const MessageOverlay: React.StatelessComponent<Props> = (props: Props) => {

    let { text, overlayMessageBoxColor } = props;

    return (
        <div.MessageOverlay data-cy='message-overlay'>

            {/** Box container */}
            <div.OverlayText data-cy='icon-hover-message'
                overlayMessageBoxColor={overlayMessageBoxColor ? overlayMessageBoxColor : div.HOVERED_MESSAGE_BOX_COLOR}>

                <div.OverlayText2> {text} </div.OverlayText2>

            </div.OverlayText>

            {/** Arrow tip */}
            <View style={{
                backgroundColor: 'transparent',
                borderStyle: 'solid',
                borderLeftWidth: 7,
                borderRightWidth: 7,
                borderBottomWidth: 7,
                borderLeftColor: 'transparent',
                borderRightColor: 'transparent',
                borderBottomColor: overlayMessageBoxColor ? overlayMessageBoxColor : div.HOVERED_MESSAGE_BOX_COLOR,
                transform: [
                    { rotate: '180deg' },
                ],
            }} />
        </div.MessageOverlay>
    );
};