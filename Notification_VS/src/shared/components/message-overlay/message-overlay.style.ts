import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

/** Arrow tim and the box above */
export const HOVERED_MESSAGE_BOX_COLOR = colors.$blue;

export const MessageOverlay = styled.View`
    z-index:100;
    position:absolute;
    align-items: center;
    justify-content: center;
    top:${props => (props as any).iconHeight * (-1)};
`;

export const OverlayText = styled.View<{overlayMessageBoxColor: string}>`
    width: 75px;
    align-items: center;
    border-radius: 2px;
    background-color: ${props => props.overlayMessageBoxColor};
`;

export const OverlayText2 = styled.Text`
    font-size: ${font.$size10}px;
    color: ${colors.$white};
`;
