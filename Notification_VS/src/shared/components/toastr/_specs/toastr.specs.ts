/// <reference types="cypress" />

describe('Toastr tests', () => {
    it('Go to the specific doc page', () => {
        cy.login();
        cy.visit('components-catalog/shared/toastr');
        cy.url().should('include', 'components-catalog/shared/toastr');
        cy.get('[data-cy=toastr-page]').should('exist');
    });

    it('Default component has all its children', () => {
        getEl('default', 'toastr').should('exist');
        getEl('default', 'icon').should('exist');
        getEl('default', 'message').should('exist');
        getEl('default', 'close').should('exist');
    });

    it('The props are displayed correctly', () => {
        const CHECKMARK = 'M13.6656 1.19469e-06L6.33437 9.66563L2 5.33438L-6.41191e-07 7.33438L6.66562 14L16 2L13.6656 1.19469e-06Z';
        const green = 'rgb(0, 195, 167)';

        getEl('default', 'message').should('have.text', 'Image uploaded successfully');
        /** Check the icon */
        getNestedElByTag('default', 'action-icon-type-frame', 'path')
            .should('have.attr', 'd', CHECKMARK);

        /** Check the color */
        getEl('default', 'toastr').should('have.css', 'background-color', green);
    });

    it('In case of error the box will have the proper color and icon', () => {
        const CROSS = 'M13.002 2.91801L10.8352 0.751343L6.50193 5.08467L2.16861 0.751343L0.00195312 2.91801L4.33526 7.25134L0.00195312 11.5847L2.16861 13.7513L6.50193 9.41801L10.8352 13.7513L13.002 11.5847L8.66859 7.25134L13.002 2.91801Z';
        const red = 'rgb(234, 67, 53)';

        /** Check the icon */
        getNestedElByTag('error', 'action-icon-type-frame', 'path')
            .should('have.attr', 'd', CROSS);

        /** Check the color */
        getEl('error', 'toastr').should('have.css', 'background-color', red);
    });

    it('When custom icon is provided it is correctly displayed', () => {
        const PEN_SVG = `M11.2141 5.50827L3.2052 13.4732L0.418823 13.5102V10.7694L8.53474 2.7958L8.85262 2.45715L9.12286 2.15534L10.8397 0.438477L13.5262 3.14079L11.9047 4.8576L9.12286 2.15534L8.85262 2.45715L8.53474 2.7958L11.2141 5.50827Z`;

        /** Check the icon */
        getNestedElByTag('custom-icon', 'action-icon-type-frame', 'path')
            .should('have.attr', 'd', PEN_SVG);
    });

    it('The Close callback is called when clicking on the "X" button', () => {
        /** Initially the value is 0 */
        getEl('close-callback', 'callback-result').should('have.text', '0');
        getEl('close-callback', 'close').click();

        /** The value incremented after click */
        getEl('close-callback', 'callback-result').should('have.text', '1');
    });

    it('CSS properties can be overwritten', () => {
        const yellow = 'rgb(255, 213, 72)';

        getEl('override', 'toastr')
            .should('have.css', 'background-color', yellow);
    });

    it('When narrow flag is true, the component takes the entire width', () => {
        /**
         * For some reason cypress doesn't take the width
         * CSS property but the actual width of the container.
         */
        getEl('narrow', 'toastr')
            .invoke('outerWidth')
            .should('be.gt', 300);
    });
});

// ====== SELECTORS ======

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

function getNestedElByTag(useCase, wrapper, tag) {
    return cy.get(`[data-cy=${useCase}] [data-cy=${wrapper}] ${tag}`);
}