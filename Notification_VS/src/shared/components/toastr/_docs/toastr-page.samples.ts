import { DEFAUTL_OVERRIDE } from './toastr-page.utils';
import { colors } from '../../../style/colors';

export const DEFAULT = `
const config: ToastrProps = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
};

<Toastr {...config} />
`;

export const ERROR_TYPE = `
const config: ToastrProps = {
    message: 'Image upload failed',
    type: TOASTR_TYPE.ERROR,
    // This is needed because the component is absolutely positioned by default
    // and it may get out of the box bounds.
    override: { root: '${DEFAUTL_OVERRIDE.root}' },
};

<Toastr {...config} />
`;

export const CUSTOM_ICON = `
const config: ToastrProps = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    // The path of the desired icon, can also be an image url
    icon: PEN_SVG,
    // This is needed because the component is absolutely positioned by default
    // and it may get out of the box bounds.
    override: { root: '${DEFAUTL_OVERRIDE.root}' },
};

<Toastr {...config} />
`;

export const CLOSE_CALLBACK = `
const config: ToastrProps = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    // Pass the callback
    close: () => doSomething(),
    // This is needed because the component is absolutely positioned by default
    // and it may get out of the box bounds.
    override: { root: '${DEFAUTL_OVERRIDE.root}' },
};

<Toastr {...config} />
`;

export const OVERRIDE = `
const config: ToastrProps = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    // This is needed because the component is absolutely positioned by default
    // and it may get out of the box bounds.
    // The string represents the changed root properties.
    override: { root: 'background-color: ${colors.$yellow}; ${DEFAUTL_OVERRIDE.root}'},

};

<Toastr {...config} />
`;

export const NARROW = `
const config: ToastrProps = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    // When this flag is set to true the box extends 100% of the available width.
    isNarrow: true,
    override: { root: '${DEFAUTL_OVERRIDE.root}' },
};

<Toastr {...config} />
`;