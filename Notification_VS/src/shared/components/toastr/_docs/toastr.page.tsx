import * as samples from './toastr-page.samples';
import * as utils from './toastr-page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Toastr } from '../toastr';
import React from 'react';

interface ToastrState {
    callbackCount: number;
}

export class ToastrPage extends ScrollableDocPage<ToastrState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                callbackCount: 0,
            },
        };
    }

    public renderDemoPage() {
        let { width, overrides } = this.state;
        const { callbackCount } = overrides;

        return (
            <>
                {/** Overview */}
                <div.Overview data-cy='toastr-page' width={width}>
                    <div.OverviewTitle>
                        Toastr
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Notify the user about the results of his actions targeting resources (edit, create, delete).
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Basic config
                        </div.DemoTitle>

                        <Toastr {...utils.DEFAULT} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Error */}
                <div.Demo data-cy='error' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Error
                        </div.DemoTitle>

                        <Toastr {...utils.ERROR_TYPE} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ERROR_TYPE} />
                </div.Demo>

                {/** Custom icon */}
                <div.Demo data-cy='custom-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom icon
                        </div.DemoTitle>

                        <Toastr {...utils.CUSTOM_ICON} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CUSTOM_ICON} />
                </div.Demo>

                {/** Close callback */}
                <div.Demo data-cy='close-callback' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Close callback
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='callback-result'>
                            {callbackCount}
                        </div.CallbackResult>

                        <Toastr {...utils.getCloseCallbackConfig(() => this.incrementCallbackCount())} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CLOSE_CALLBACK} />
                </div.Demo>

                {/** Override */}
                <div.Demo data-cy='override' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Override
                        </div.DemoTitle>

                        <Toastr {...utils.OVERRIDE} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDE} />
                </div.Demo>

                {/** Narrow */}
                <div.Demo data-cy='narrow' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Narrow
                        </div.DemoTitle>

                        <Toastr {...utils.NARROW} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.NARROW} />
                </div.Demo>
            </>
        );
    }

    private incrementCallbackCount() {
        const { callbackCount } = this.state.overrides;
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                callbackCount: callbackCount + 1,
            },
        });
    }
}