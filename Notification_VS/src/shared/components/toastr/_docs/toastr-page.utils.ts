import { PEN_SVG } from '../../../assets/icons';
import { IToastr, TOASTR_TYPE } from '../../../interfaces/toastr';
import { colors } from '../../../style/colors';

export const DEFAUTL_OVERRIDE = { root: 'position: relative; bottom: 0px;' };

export const DEFAULT: IToastr = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    override: DEFAUTL_OVERRIDE,
};

export const ERROR_TYPE: IToastr = {
    message: 'Image upload failed',
    type: TOASTR_TYPE.ERROR,
    override: DEFAUTL_OVERRIDE,
};

export const CUSTOM_ICON: IToastr = {
    message: 'Lesson edited successfully',
    type: TOASTR_TYPE.SUCCESS,
    icon: PEN_SVG,
    override: DEFAUTL_OVERRIDE,
};

export function getCloseCallbackConfig(callback: () => void): IToastr {
    return ({
        message: 'Image uploaded successfully',
        type: TOASTR_TYPE.SUCCESS,
        close: callback,
        override: DEFAUTL_OVERRIDE,
    });
}

export const OVERRIDE: IToastr = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    override: { root: `${DEFAUTL_OVERRIDE.root} background-color: ${colors.$yellow}` },
};

export const NARROW: IToastr = {
    message: 'Image uploaded successfully',
    type: TOASTR_TYPE.SUCCESS,
    isNarrow: true,
    override: DEFAUTL_OVERRIDE,
};
