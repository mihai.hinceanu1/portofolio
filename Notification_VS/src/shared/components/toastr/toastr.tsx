import * as div from './toastr.style';
import { Icon } from '../../../icons/components/icon/icon';
import { CHECKMARK, CROSS } from '../../assets/icons';
import { IToastr, TOASTR_TYPE } from '../../interfaces/toastr';
import { colors } from '../../style/colors';
import React from 'react';

interface Props extends IToastr { }
interface State { }

/**
 * For some actions like creating, editing or deleting a resource,
 * a confirmation message must be communicated to the user so he's
 * aware of the result.
 *
 * <!> This should be used as an overlay, as it must be over all other
 * components.
 * <!> This should also self close after a short period of time
 */
export class Toastr extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public render() {
        const { message, type, isNarrow, icon, close, override } = this.props;

        return (
            <div.Toastr data-cy='toastr'
                isNarrow={isNarrow}
                type={type}
                override={!!override && override.root}>

                {/** Icon */}
                <div.IconSeparator data-cy='icon-separator'>
                    <div.ActionIconTypeFrame data-cy='action-icon-type-frame'>
                        <Icon path={this.getIconByType(type, icon)} color={colors.$white} />
                    </div.ActionIconTypeFrame>
                </div.IconSeparator>

                {/** Message */}
                <div.Message data-cy='message'>
                    {message}
                </div.Message>

                {/** Close icon */}
                <div.Close data-cy='close'
                    underlayColor='none'
                    onPress={!!close && close}>

                    <Icon path={CROSS} color='rgba(0, 0, 0, 0.1)' width={14} height={14} />

                </div.Close>
            </div.Toastr>
        );
    }

    private getIconByType(type: TOASTR_TYPE, icon: string) {
        if (icon) {
            return icon;
        }

        switch (type) {
            case TOASTR_TYPE.ERROR: return CROSS;

            case TOASTR_TYPE.SUCCESS: return CHECKMARK;

            default: return;
        }
    }
}
