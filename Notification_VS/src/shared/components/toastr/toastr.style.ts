import { TOASTR_TYPE } from '../../interfaces/toastr';
import { colors } from '../../style/colors';
import { getShadow } from '../../style/shadow.style';
import styled from 'styled-components/native';

// ====== PROPS ======

interface AdaptiveProps {
    isNarrow: boolean;
}

interface ActionConfirmationProps extends AdaptiveProps {
    type: TOASTR_TYPE;
    override: string;
}

// ====== STYLE ======

export const Toastr = styled.View<ActionConfirmationProps>`
    z-index: 100;
    width: 250px;
    padding: 15px;
    flex-direction: row;
    align-items: center;
    margin-bottom: 20px;
    margin-left: 20px;
    background-color: ${props => getBgrColor(props)};
    ${getShadow()}
    ${props => getAdaptiveStyles(props)}
    ${props => props.override}
`;

export const IconSeparator = styled.View`
    padding: 0px 15px;
    margin-right: 10px;
    align-items: center;
    justify-content: center;
    border-right-width: 1px;
    border-color: rgba(0, 0, 0, 0.1);
`;

export const ActionIconTypeFrame = styled.View`
    width: 25px;
    height: 25px;
    border-radius: 25px;
    align-items: center;
    justify-content: center;
    background-color: rgba(255, 255, 255, 0.1);
`;

export const Message = styled.Text`
    flex-grow: 1;
    font-weight: 500;
    text-align: center;
    color: ${colors.$white};
`;

export const Close = styled.TouchableHighlight`
    margin: 0px 10px;
`;

// ====== UTILS ======

function getAdaptiveStyles(props: AdaptiveProps): string {
    if (props.isNarrow) {
        return `
            width: 100%;
        `;
    }
}

function getBgrColor(props: ActionConfirmationProps): string {
    switch (props.type) {
        case TOASTR_TYPE.ERROR: return colors.$red;

        default:
            return colors.$green;
    }
}