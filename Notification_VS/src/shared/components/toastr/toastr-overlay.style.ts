import styled from 'styled-components/native';

export const ToastrOverlay = styled.View`
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column-reverse;
    width: 100%;
    height: 100%;
`;
