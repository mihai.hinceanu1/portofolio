import { Toastr } from './toastr';
import * as div from './toastr-overlay.style';
import { IToastr } from '../../interfaces/toastr';
import { toastrs$ } from '../../services/shared.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {}
interface State {
    toastrs: IToastr[];
}

export class ToastrOverlay extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            toastrs: [],
        };
    }

    public render() {
        const { toastrs } = this.state;

        return (
            <div.ToastrOverlay data-cy='toastr-overlay' pointerEvents='box-none'>
                {
                    (toastrs && !!toastrs.length) &&
                    toastrs.map(toastrs => <Toastr {...toastrs} key={toastrs.id} />)
                }
            </div.ToastrOverlay>
        );
    }

    public componentDidMount() {
        this.subscribeToToastrs();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToToastrs() {
        toastrs$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(toastrs => {
                this.setState({ toastrs });
            });
    }
}