import { warningMessageStyles as styles } from './warning-message.style';
import { FitImage } from '../fit-image/fit-image';
import * as React from 'react';
import { Text, View } from 'react-native';

interface Props {
    // displayed message when error occurs
    message: string;
    // alternativeIcon?: string;
}

// TODO COnvert to stylesheet
/** Warning message passed from props, default warning triangle logo */
export const WarningMessage: React.StatelessComponent<Props> = (props: Props) => {
    const { message } = props;

    return (
        <View data-cy='warning-message' style={[styles.container]}>

            <View style={[styles.icon]}>
                <FitImage imgPath='/shared/warning.png' />
            </View>
            <Text data-cy='message' style={[styles.message]}>
                {message}
            </Text>

        </View>
    );
};