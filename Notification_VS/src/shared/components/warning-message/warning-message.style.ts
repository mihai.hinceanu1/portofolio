import EStyleSheet from 'react-native-extended-stylesheet';

export const warningMessageStyles = EStyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    icon: {
        height: 20,
        width: 20,
        marginRight: 5,
    },
    message: {
        color: '$warningMessage',
    },
});
