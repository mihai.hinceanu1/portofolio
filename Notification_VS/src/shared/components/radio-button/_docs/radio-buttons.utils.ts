import { IRadioOption } from '../../../interfaces/radio-button';

export const reminderFrequencies: IRadioOption[] = [
    {
        _id: '5e60bd4f20266c9eefdac36b',
        label: 'Daily',
    },
    {
        _id: '5e60bd53360c810e1463f3b1',
        label: 'Weekly',
    },
    {
        _id: '5e60bd55f67536a0742fe2bd',
        label: 'Monthly'
    },
];