export const DEFAULT = `
const reminderFrequencies: IRadioOption[] = [
    {
        _id: '1',
        label: 'Daily',
    },
    {
        _id: '2',
        label: 'Weekly',
    },
    {
        _id: '3',
        label: 'Monthly'
    },
];

let simpleConfig = {
    options: reminderFrequencies
    onPress: (option) => this.selectOption(option)
};

<RadioButton {..simpleConfig} />
`;

export const RADIO_RIGHT = `
<RadioButton {...simpleConfig}
    // Specify on which side you want the circle to be
    radioSide={RADIO_SIDES.right} />
`;

export const OVERRIDES = `
    <RadioButton {...simpleConfig}
        // Styling changes for root level
        overrides={{root: 'margin: 50px;'}} />
`;