import * as samples from './radio-buttons.samples';
import * as utils from './radio-buttons.utils';
import { RADIO_SIDES } from '../_const/radio-button.const';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { IRadioOption } from '../../../interfaces/radio-button';
import { CodeEditor } from '../../code-editor/code-editor';
import { RadioButtons } from '../radio-buttons';
import * as React from 'react';
import { Text } from 'react-native';

interface RadioButtonState {
    selectedOption: IRadioOption;
    sideSelectedOption: IRadioOption;
    overridesSelectedOption: IRadioOption;
}

export class RadioButtonsPage extends ScrollableDocPage<RadioButtonState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                selectedOption: {
                    _id: '',
                    label: '',
                },
                sideSelectedOption: {
                    _id: '',
                    label: '',
                },
                overridesSelectedOption: {
                    _id: '',
                    label: '',
                },
            }
        };
    }

    public renderDemoPage() {
        const { width, overrides } = this.state;
        const { selectedOption, sideSelectedOption, overridesSelectedOption } = overrides;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width} data-cy='radio-buttons-page'>
                    <div.OverviewTitle>
                        Radio Buttons
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Radio Buttons are used in various cases when user needs to make any kind of
                        single choice selection.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default radio button */}
                <div.Demo width={width} data-cy='simple-radio-button'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard Radio Button
                        </div.DemoTitle>

                        <RadioButtons options={utils.reminderFrequencies}
                            selected={selectedOption}
                            onPress={(option) => this.selectOption(option, 'selectedOption')} />

                        {
                            !!selectedOption._id &&
                            <Text data-cy='selected-option'>{selectedOption.label}</Text>
                        }
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Change radio side */}
                <div.Demo width={width} data-cy='radio-right'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Radio on right
                        </div.DemoTitle>

                        <RadioButtons radioSide={RADIO_SIDES.right}
                            options={utils.reminderFrequencies}
                            selected={sideSelectedOption}
                            onPress={(option) => this.selectOption(option, 'sideSelectedOption')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.RADIO_RIGHT} />
                </div.Demo>

                {/** Overrides */}
                <div.Demo width={width} data-cy='overrides'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides
                        </div.DemoTitle>

                        <RadioButtons options={utils.reminderFrequencies}
                            overrides={{ root: 'margin: 50px;' }}
                            selected={overridesSelectedOption}
                            onPress={(option) => this.selectOption(option, 'overridesSelectedOption')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDES} />
                </div.Demo>
            </>
        );
    }

    /**
     * Update state value with the selected option from each radio button,
     * so that the functionality can be tested and the results don't overlap.
     */
    private selectOption(option: IRadioOption, identifier: string) {
        let selectedOption = utils.reminderFrequencies.find(freq => freq._id === option._id);

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                [identifier]: selectedOption,
            }
        });
    }
}