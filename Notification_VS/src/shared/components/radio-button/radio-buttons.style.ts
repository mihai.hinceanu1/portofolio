import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

interface Overrides {
    overrides: {};
}

export const RadioButton = styled.View<Overrides>`
    ${props => props.overrides};
`;

export const Label = styled.Text`
    font-size: ${font.$size14}px;
    text-align: center;
`;

export const Options = styled.View<Overrides>`
    justify-content: flex-start;
    flex-wrap: wrap;
    ${props => props.overrides};
`;
