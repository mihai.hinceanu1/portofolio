/** The diameter of the outer circle */
export const CIRCLE_DIAMETER = 24;

/** The diameter of the inner circle which appears when option is selected */
export const CHECKED_CIRCLE_DIAMETER = 16;

/** The radio circle can appear on either left or right side of the label */
export enum RADIO_SIDES {
    left = 'left',
    right = 'right',
}
