/// <reference types="cypress" />

describe('Radio Buttons Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/radio-button');
        cy.get('[data-cy=radio-buttons-page]').should('exist');
    });

    it('Renders label', () => {
        getEl('simple-radio-button', 'label').should('exist');
    });

    it('Callback function and is called on click', () => {
        getEl('simple-radio-button', 'selected-option').should('not.exist');
        getEl('simple-radio-button', 'radio-circle').first().click();
        getEl('simple-radio-button', 'selected-option')
            .should('exist')
            .and($selectedOption => {
                expect($selectedOption.text()).to.eq('Daily');
            });
    });

    it('When an option is selected the radio button is highlighted', () => {
        getEl('overrides', 'radio-checked').should('not.exist');
        getEl('overrides', 'radio-circle').first().click();
        getEl('overrides', 'radio-checked').should('exist');
    });

    it('Radio button can appear on left side', () => {
        getEl('radio-right', 'label').then($label => {
            getEl('radio-right', 'radio-circle').first().should($radio => {
                expect($radio.position().left).to.be.gt($label.position().left);
            });
        });
    });

    it('Root styles can be overwritten', () => {
        getEl('overrides', 'radio-button').should('have.css', 'margin', '50px');
    });
});

// ====== SELECTORS ======

function getEl(useCase, element) {
    return cy.get(`[data-cy=${useCase}] [data-cy=${element}]`);
}
