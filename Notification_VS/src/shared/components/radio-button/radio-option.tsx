import { RADIO_SIDES } from './_const/radio-button.const';
import * as div from './radio-option.style';
import { IRadioOption } from '../../interfaces/radio-button';
import React from 'react';
import { Text } from 'react-native';

interface Props {
    option: IRadioOption;
    isSelected: boolean;
    onPress: (_id: string) => void;
    radioSide: RADIO_SIDES;
}

/**
 * A single option for radio button.
 * Has a label and a basic radio input, which can appear on either
 * left or right side of the label.
 * When the option is selected the radio circle is highlighted.
 */
export const RadioOption: React.FunctionComponent<Props> = (props: Props) => {

    const { option, isSelected, radioSide } = props;
    const { label } = option;

    return (
        <div.RadioOption data-cy='radio-option' radioSide={radioSide}>

            {/** Label */}
            <Text data-cy='label'>
                {label}
            </Text>

            {/** Radio Circle */}
            <div.RadioCircle data-cy='radio-circle'
                isSelected={isSelected}
                underlayColor='none'
                onPress={() => onOptionSelect(option)}>

                {/** Checked circle */}
                {/** This fragment is added because TouchableHighlight requires to have exactly one child */}
                <>
                    {
                        isSelected &&
                        <div.RadioCheckedCircle data-cy='radio-checked' />
                    }
                </>
            </div.RadioCircle>
        </div.RadioOption>
    );

    function onOptionSelect(option: IRadioOption) {
        const { onPress, _id } = option;

        if (onPress) {
            onPress();
        }

        props.onPress(_id);
    }
};
