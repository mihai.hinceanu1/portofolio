import { RADIO_SIDES } from './_const/radio-button.const';
import * as div from './radio-buttons.style';
import { RadioOption } from './radio-option';
import { IRadioOption } from '../../interfaces/radio-button';
import * as React from 'react';

interface Props {
    /**
     * Consist of an id and a label which will also be the information the user sees.
     * The id is used to keep track of the selected option
     */
    options: IRadioOption[];

    /** Function executed when the radio button is pressed */
    onPress: (option: IRadioOption) => void;

    /** Current selected value */
    selected: IRadioOption;

    /** Short descriptive text */
    label?: string;

    /** The circle can appear on either left or right side of the label */
    radioSide?: RADIO_SIDES;

    /** Change some styling */
    overrides?: {
        root?: string;
        options?: string;
    };
}

interface State { }

/** Offer multiple options but it constraints you to select only one. */
export class RadioButtons extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { options, overrides, radioSide, label } = this.props;

        return (
            <div.RadioButton data-cy='radio-button'
                overrides={overrides && overrides.root}>

                {/** Label */}
                {
                    !!label && <div.Label data-cy='label'>{label}</div.Label>
                }

                {/** Options */}
                <div.Options data-cy='options' overrides={overrides && overrides.options}>
                    {
                        !!options.length && options.map(option =>
                            <RadioOption key={option._id}
                                option={option}
                                radioSide={radioSide}
                                isSelected={this.getSelectedStatus(option)}
                                onPress={() => this.handleSelectOption(option)} />
                        )
                    }
                </div.Options>
            </div.RadioButton>
        );
    }

    /** Updates the selected option */
    private handleSelectOption(option: IRadioOption) {
        const { onPress } = this.props;

        if (onPress) {
            onPress(option);
        }
    }

    /**
     * Returns true if the option given as parameter is the
     * selected option so that we know which one to highlight.
     */
    private getSelectedStatus(option: IRadioOption) {
        const { selected } = this.props;

        if (selected) {
            return option._id === selected._id ? true : false;
        }

        return false;
    }
}
