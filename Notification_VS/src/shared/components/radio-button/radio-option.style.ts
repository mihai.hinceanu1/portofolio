import { CHECKED_CIRCLE_DIAMETER, CIRCLE_DIAMETER, RADIO_SIDES } from './_const/radio-button.const';
import { colors } from '../../style/colors';
import styled from 'styled-components/native';

interface RadioOptionProps {
    radioSide: RADIO_SIDES;
}

interface CircleProps {
    isSelected: boolean;
}

export const RadioOption = styled.View<RadioOptionProps>`
    padding: 10px;
    width: 100%;
    align-items: center;
    align-self: flex-start;
    justify-content: flex-end;
    flex-direction: ${props => getFlexRowOrReverse(props)};
`;

export const RadioCircle = styled.TouchableHighlight<CircleProps>`
    margin: 0 10px;
    border-width: 1px;
    align-items: center;
    justify-content: center;
    height: ${CIRCLE_DIAMETER}px;
    width: ${CIRCLE_DIAMETER}px;
    border-radius: ${CIRCLE_DIAMETER / 2}px;
    border-color: ${props => getBorderColor(props)};
`;

export const RadioCheckedCircle = styled.View`
    height: ${CHECKED_CIRCLE_DIAMETER}px;
    width: ${CHECKED_CIRCLE_DIAMETER}px;
    background-color: ${colors.$blue};
    border-radius: ${CHECKED_CIRCLE_DIAMETER / 2}px;
`;

// ====== UTILS ======

let getFlexRowOrReverse = (props: RadioOptionProps) =>
    props.radioSide === RADIO_SIDES.right ? 'row' : 'row-reverse';

let getBorderColor = (props: CircleProps) =>
    props.isSelected ? colors.$blue : colors.$grey;
