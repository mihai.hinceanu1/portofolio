import * as div from './modal.style';
import { Icon } from '../../../icons/components/icon/icon';
import { CROSS } from '../../assets/icons';
import { colors } from '../../style/colors';
import { OVERLAY_GRADIENT, style } from '../../style/overlay.style';
import * as React from 'react';
import { Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
    callback?: () => void;
    hasBackgroundColor?: boolean;

    overrides?: {
        root?: string;
    };
}

interface State { }

/**
 * Generic Modal.
 * Renders children components.
 * Standard close button.
 * Esc dismisses it. // not really
 */
export class Modal extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        let { children, callback, hasBackgroundColor, overrides } = this.props;

        return (
            <div.Modal data-cy='modal'
                hasBackgroundColor={hasBackgroundColor}
                override={overrides && overrides.root}>

                {/** Linear Gradient */}
                <LinearGradient colors={OVERLAY_GRADIENT}
                    style={style.linearGradient}>

                        {/** Header */}
                        <div.Header data-cy='header'>

                            {/** Close */}
                            <div.Close data-cy='close' onPress={() => callback && callback()}>
                                <Icon path={CROSS} color={colors.$white} />
                            </div.Close>

                        </div.Header>

                        {/* Content */}
                        <div.Body data-cy='body'>
                            {children}
                        </div.Body>

                    </LinearGradient>
            </div.Modal>
        );
    }

    public componentDidMount() {
        this.addEventListener();
    }

    public componentWillUnmount() {
        this.removeEventListener();
    }

    private addEventListener() {
        if (Platform.OS === 'web') {
            document.addEventListener('keydown', this.closeModalOnButtonPress);
        }
    }

    private removeEventListener() {
        if (Platform.OS === 'web') {
            document.removeEventListener('keydown', this.closeModalOnButtonPress);
        }
    }

    private closeModalOnButtonPress(event: any) {
        if (event.keyCode === 27) {
            // Close Modal
        }
    }
}