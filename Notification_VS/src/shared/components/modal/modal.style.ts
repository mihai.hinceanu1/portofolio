import { colors } from '../../style/colors';
import styled from 'styled-components/native';

interface ModalProps {
    hasBackgroundColor: boolean;
    override: string;
}

export const Modal = styled.View<ModalProps>`
    ${props => props.hasBackgroundColor && `background-color: ${colors.$overlayBgr};`}
    z-index: 100;
    position: absolute;
    width: 100%;
    height: 100%;
    ${props => props.override};
`;

export const Header = styled.View`
    align-items: flex-end;
    width: 100%;
    position: absolute;
    z-index: 100;
    top: 0;
    left: 0;
`;

export const Close = styled.TouchableOpacity`
    margin-top: 10px;
    margin-right: 25px;
    z-index: 101;
`;

export const Body = styled.View`
    align-items: center;
    justify-content: center;
    position: relative;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    padding-top: 50px;
`;
