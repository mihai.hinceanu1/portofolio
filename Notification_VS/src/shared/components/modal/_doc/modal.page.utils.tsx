import { ButtonConfig } from '../../../interfaces/button';
import { Modal } from '../modal';
import * as React from 'react';
import { Text } from 'react-native';
import { Subject } from 'rxjs';

export const getShowModalBtnCfg = (callback: () => void): ButtonConfig => ({
    text: 'Show Modal',
    onPress: callback
});

export const getShowSubModalBtnCfg = (): ButtonConfig => ({
    text: 'Show Subscribed Modal',
    onPress: () => demoModalVisible$.next(true),
});

export const getModal = (callback: () => void): JSX.Element => (
    <Modal callback={callback}>
        <Text style={{color: 'white', fontSize:30}}>Basic Modal Overlay</Text>
        <Text style={{color: 'white'}}>This is a simple demo of a modal</Text>
    </Modal>
);

/** Used to demonstrate how a typical modal connected to state store would work */
export const demoModalVisible$ = new Subject<boolean>();