import { demoModalVisible$, getModal } from './modal.page.utils';
import { addAppOverlay, removeAppOverlay } from '../../../services/app-overlays.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { Modal } from '../modal';

interface Props { }

interface State { }

/**
 * Demonstration of a state store subscribed modal.
 * There is no template returned.
 * Only a virtual node to hookup the component somewhere in the parent template.
 * When the subscription triggers the modal is inserted and removed programmatically.
 * All modals will render on top of the app.
 * This approach is plugins compatible.
 * Any plugin can define any modal without overwriting core.
 */
export class DemoModal extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();
    private modal: JSX.Element;

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        return <></>;
    }

    public componentDidMount() {
        this.subDemoModalVisible();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subDemoModalVisible() {

        // <!> Fake state store subscription
        // <!> Use a real state store observable
        demoModalVisible$.pipe(
            takeUntil(this.destroyed$),
        ).subscribe(isVisible => {
            if (isVisible) {
                this.showModal();
            } else {
                this.hideModal();
            }
        });
    }

    /** CLoses the modal via state store */
    private closeDemoModal() {

        // <!> Fake state store dispatch
        // <!> Use a real state store dispatch command
        demoModalVisible$.next(false);
    }

    private showModal() {

        // <!> Demo modal with demo content
        // <!> Write your own modal template
        this.modal = getModal(() => this.closeDemoModal());
        addAppOverlay(this.modal);
    }

    private hideModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }
}