import { DemoModal } from './demo-modal';
import * as samples from './modal.page.samples';
import { getModal, getShowModalBtnCfg, getShowSubModalBtnCfg } from './modal.page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { addAppOverlay, removeAppOverlay } from '../../../services/app-overlays.service';
import { Button } from '../../button/button';
import { CodeEditor } from '../../code-editor/code-editor';
import * as React from 'react';
import { Text } from 'react-native';

interface ModalState {}

export class ModalPage extends ScrollableDocPage<ModalState> {

    private modal: JSX.Element;

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        let showModalBtnCfg = getShowModalBtnCfg(() => this.addModal());
        let showSubModalBtnCfg = getShowSubModalBtnCfg();

        return (
            <>

                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Modal
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Generic modal used by pages to display various screens and forms.
                    </div.OverviewDescription>
                </div.Overview>

                {/* Modal */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>Modal</div.DemoTitle>
                        <Button config={showModalBtnCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.defaultModal} />
                </div.Demo>

                {/* Subscribed Modal */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>Subscribed Modal</div.DemoTitle>
                        <Text>This modal is triggered via an observable subscription.</Text>
                        <Text>Usually these would be hooked to state store observables.</Text>
                        <Button config={showSubModalBtnCfg} />

                        <DemoModal />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.subscribedModal} />
                </div.Demo>

            </>
        );
    }

    private addModal() {
        this.modal = getModal(() => this.removeModal());
        addAppOverlay(this.modal);
    }

    private removeModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }
}