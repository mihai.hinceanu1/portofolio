import * as div from './panel.style';
import * as scroll from '../../style/scroll.style';
import { Input } from '../input/input';
import * as React from 'react';

interface Props {
    title: string;

    children?: React.ReactNodeArray | React.ReactNode;

    // toggle search bar
    search?: boolean;

    // callback when typing on search
    onSearchChange: (text: string) => void;

    searchValue?: string;

    overrides?: {
        root?: string;
        children?: string;
        height?: number;
    };
}

/**
 * Visual Component that encapsulates different other components
 * Useful on TaxonomyTree or IconPicker, it has only one tab.
 * SearchBar is not mandatory, due to some components will not
 * require any search bar at all.
 */
export const Panel: React.FunctionComponent<Props> = (props: Props) => {
    const { title, children, onSearchChange, searchValue, overrides } = props;

    return (
        <div.Panel data-cy='panel'
            override={overrides && overrides.root}>

            {/** Title */}
            <div.Title data-cy='title'>
                {title}
            </div.Title>

            <div.Frame data-cy='frame'>

                {/** Search Bar */}
                {
                    <Input type='text'
                        value={searchValue}
                        onChangeText={(text: string) => onSearchChange(text)}
                        placeholder='Search'
                        overrides={div.Input}
                        icon={div.SearchIcon} />
                }

                {/** Children */}
                <scroll.ScrollView data-cy='children'
                    contentContainerStyle={{
                        ...scroll.inner.content,
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        justifyContent: 'space-around',
                        alignItems: 'flex-start',
                        height: overrides && overrides.height,
                    }}>

                    {children}
                </scroll.ScrollView>

            </div.Frame>
        </div.Panel>
    );
};
