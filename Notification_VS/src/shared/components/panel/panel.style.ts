import { SEARCH } from '../../assets/icons';
import { TIcon } from '../../interfaces/icon';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import { BOX_SHADOW_CSS, getShadow } from '../../style/shadow.style';
import styled from 'styled-components/native';

interface PanelOverride {
    override?: string;
}

export const Panel = styled.View<PanelOverride>`
    width: 230px;
    max-height: 500px;
    height: 400px;
    ${props => props.override};
`;

export const Title = styled.Text`
    text-align: center;
    padding: 10px 15px;
    font-size: ${font.$size12}px;
    background-color: ${colors.$white};
    align-self: flex-start;
    z-index: 100;
`;

export const Frame = styled.View`
    width: 230px;
    background-color: ${colors.$white};
    padding-bottom: 50px;
    ${getShadow(BOX_SHADOW_CSS)};
`;

// export const Children = styled.ScrollView`
//     align-items: flex-start;
//     justify-content: space-between;
//     padding-left: 25px;
//     padding-right: 25px;
// `;

export const Input = {
    root: `
        width: 100%;
        margin-top: 10px;
    `,
    inputField: `
        border-radius: 20px;
        height: 30px;
        font-size: ${font.$size12}px;
        font-weight: 600;
    `,
};

export const SearchIcon: TIcon = {
    type: 'svg',
    svgPath: SEARCH,
    fill: colors.$darkGrey,
};
