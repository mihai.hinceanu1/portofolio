import { ReactWebAttributes } from '../../interfaces/workarounds';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import { BOX_SHADOW_CSS } from '../../style/shadow.style';
import { Platform } from 'react-native';
import styled from 'styled-components/native';

interface Props extends ReactWebAttributes {
    backgroundColor: string;
}

let isAndroid = Platform.OS === 'android';
export const PageButton = styled.TouchableOpacity<Props>`
    flex-direction: row;
    background-color: ${props => props.backgroundColor};
    width: 350px;
    ${isAndroid ? 'elevation: 10;' : BOX_SHADOW_CSS}
    margin-bottom: 15px;
`;

export const BigIcon = styled.View`
    height: 70px;
    width: 70px;
    align-items: center;
    justify-content: center;
`;

export const PageButtonText = styled.View`
    background-color: ${colors.$white};
    align-items: center;
    justify-content: center;
    width: 280px;
    height: 70px;
`;

export const PageButtonText2 = styled.Text<{ color: string }>`
    font-size: ${font.$size22}px;
    font-weight: 700;
    color:  ${props => props.color};
`;