import * as div from './page-button.style';
import { colors } from '../../style/colors';
import { Icon } from '../icon/icon';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';

interface Params { }

interface Props extends RouteComponentProps<Params> {
    icon: string;
    text: string;
    redirectTo: string;
}

interface State {
    hovered: boolean;
}

/** Big buttons currently use in beta for routing users to our pages receives link to where should redirect */
export class _PageButton extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            hovered: false,
        };
    }

    public render() {
        const { hovered } = this.state;
        const { icon, text, redirectTo } = this.props;

        return (
            <div.PageButton data-cy='page-button'
                onPress={() => this.redirect(redirectTo)}
                onMouseEnter={(e: any) => this.onMouseEnterButton(e)}
                onMouseLeave={(e: any) => this.onMouseLeaveButton(e)}
                backgroundColor={hovered ? colors.$blue : colors.$darkBlue}>

                <div.BigIcon>
                    <Icon icon={icon} width={40} height={40} />
                </div.BigIcon>

                <div.PageButtonText>
                    <div.PageButtonText2 color={hovered ? colors.$blue : colors.$darkBlue}>
                        {text}
                    </div.PageButtonText2>
                </div.PageButtonText>
            </div.PageButton>

        );
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private onMouseEnterButton(_event: any) {
        this.setState({ hovered: true });
    }

    private onMouseLeaveButton(_event: any) {
        this.setState({ hovered: false });
    }

    private redirect(url: string) {
        this.props.history.push(url);
    }
}

export const PageButton = withRouter(_PageButton);
