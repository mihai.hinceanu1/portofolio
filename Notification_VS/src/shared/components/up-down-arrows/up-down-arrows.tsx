import { Icon } from '../icon/icon';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    /** fixed width */
    width?: number;

    /** fixed height */
    height?: number;

    /** function executed when upper arrow is pressed */
    onClickUp: () => void;

    /** function executed when lower arrow is pressed */
    onClickDown: () => void;
}

interface State { }

// Component used to change the order of the rendered topics
export class UpDownArrows extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { width, height, onClickUp, onClickDown } = this.props;
        return (
            <View data-cy='up-down-arrows'>
                <Icon width={width} height={height} icon='/topics/arrow_up_black.png' onPress={onClickUp} />
                <Icon width={width} height={height} icon='/topics/arrow_down_black.png' onPress={onClickDown} />
            </View>
        );
    }
}