import { InputOverrides } from '../../interfaces/input';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import styledWeb from 'styled-components';
import styled from 'styled-components/native';

export const UploadImage = styled.View``;

export const Box = styled.View`
    width: 300px;
    min-height: 400px;
    background-color: ${colors.$white};
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

export const InputFile = styledWeb.input`
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1000;
    opacity: 0;
`;

export const UploadButton = styled.View`
    width: 200px;
    height: 40px;
    position: relative;
    overflow: hidden;
`;

export const FakeUploadButton = styled.View`
    width: 100%;
    height: 100%;
    background-color: ${colors.$white};
    border-width: 2px;
    border-color: ${colors.$blue};
    justify-content: center;
    align-items: center;
`;

export const FakeUploadButtonText = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$blue};
`;

export const Preview = styled.Image`
    width: 100px;
    height: 100px;
    margin-top: 20px;
`;

export const Divider = styled.View`
    margin-top: 20px;
    margin-bottom: 20px;
`;

export const Line = styled.View`
    width: 150px;
    align-self: center;
    height: 1px;
    background-color: ${colors.$grey};
`;

export const Text = styled.Text`
    align-self: center;
    width: 30px;
    background-color: ${colors.$white};
    text-align: center;
    z-index: 1;
    position: absolute;
    top: -8px;
    color: ${colors.$grey};
`;

export const Error = styled.Text`
    text-align: center;
    color: ${colors.$red};
    margin-top: 20px;
    font-weight: 400;
    font-size: ${font.$size14}px;
    padding-left: 20px;
    padding-right: 20px;
`;

// ===== OVERRIDES =====

export const BUTTON_OVERRIDE = {
    root: `
        align-self: center;
        margin-top: 20px;
    `,
};

export const INPUT_OVERRIDE: InputOverrides = {
    root: `
        padding: 0px;
        width: 200px;
    `,
    inputFrame: `
        margin-top: 0px;
    `,
    input: `
        text-align: center;
        border-width: 2px;
        border-color: ${colors.$blue};
    `,
};
