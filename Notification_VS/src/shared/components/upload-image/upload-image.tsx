import * as div from './upload-image.style';
import * as utils from './upload-image.utils';
import { FileInput } from '../../interfaces/file-input';
import { colors } from '../../style/colors';
import { Button } from '../button/button';
import { Input } from '../input/input';
import { Modal } from '../modal/modal';
import * as React from 'react';

interface Props {
    onClose?: () => void;
    onSavePress: (image: FileInput, inputUrl: string) => void;
}
interface State {
    imgUrl: string;
    error: string;
    image: FileInput;
    inputUrl: string;
}

/**
 * Let users upload different images
 * This component should be displayed inside a modal
 * It returns both image and inputUrl, either one or another are provided (never both)
 * It calls custom method onSavePress
 * Also, it displays a preview of the image in case of any value provided
 */
export class UploadImage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            imgUrl: null,
            error: null,
            image: null,
            inputUrl: '',
        };
    }

    public render() {
        const { onClose, onSavePress } = this.props;
        const { imgUrl, image, inputUrl, error } = this.state;

        return (
            <Modal callback={() => onClose()}>

                {/** White Box */}
                <div.Box data-cy='box'>

                    {/** Upload Button */}
                    <div.UploadButton data-cy='upload-button'>

                        {/** Input File - Web Only */}
                        <div.InputFile type='file'
                            data-cy='input-file'
                            name='file'
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.handleOnChange(event)} />

                        {/** Fake Upload Button */}
                        <div.FakeUploadButton data-cy='fake-upload-button'>

                            {/** Text */}
                            <div.FakeUploadButtonText data-cy='fake-upload-button-text'>
                                Choose your file
                            </div.FakeUploadButtonText>
                        </div.FakeUploadButton>

                    </div.UploadButton>

                    {/** Divider */}
                    <div.Divider data-cy='divider'>

                        {/** Line */}
                        <div.Line data-cy='line' />

                        {/** Text */}
                        <div.Text data-cy='text'>
                            or
                        </div.Text>
                    </div.Divider>

                    {/** Paste URL */}
                    <Input type='text'
                        value={inputUrl}
                        placeholder='Paste your URL here'
                        onChangeText={(text: string) => this.onPasteInput(text)}
                        placeholderColor={colors.$blue}
                        overrides={div.INPUT_OVERRIDE} />

                    {/** Preview of the uploaded image */}
                    {
                        (imgUrl && !inputUrl) &&
                        <div.Preview data-cy='preview' source={{ uri: imgUrl }} />
                    }

                    {
                        (!imgUrl && !!inputUrl) &&
                        <div.Preview data-cy='preview' source={{ uri: inputUrl }} />
                    }

                    {/** Save Button */}
                    <Button config={utils.SAVE(() => onSavePress(image, inputUrl), this.isButtonDisabled(image, inputUrl))}
                        overrides={div.BUTTON_OVERRIDE} />

                    {/** Error */}
                    {
                        !!error &&
                        <div.Error data-cy='error'>
                            {error}
                        </div.Error>
                    }

                </div.Box>

            </Modal>
        );
    }

    private isButtonDisabled(image: FileInput, url: string) {
        return !image && !url;
    }

    private handleOnChange(event: React.ChangeEvent<HTMLInputElement>) {
        const { files } = event.target;
        const file = files[0];

        // only update if type of file is image
        if (file.type.includes('image')) {
            this.setState({
                image: file,
                imgUrl: URL.createObjectURL(file),
                inputUrl: '',
                error: null,
            });
        } else {
            this.setState({ error: 'Your image format is not supported. Please choose between SVG, JPG, JPEG, PNG.' });
        }
    }

    private onPasteInput(text: string) {
        this.setState({ inputUrl: text, imgUrl: null, error: null });
    }
}
