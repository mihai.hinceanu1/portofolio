import { ButtonConfig } from '../../interfaces/button';

export const SAVE = (onPress: () => void, disabled: boolean): ButtonConfig => ({
    text: 'Save',
    onPress: () => onPress(),
    disabled,
});
