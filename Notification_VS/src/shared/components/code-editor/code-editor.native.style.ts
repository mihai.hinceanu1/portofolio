import { Platform } from 'react-native';
import styled from 'styled-components/native';

const isAndroid = Platform.OS === 'android';
const androidFont = 'monospace';
const iosFont = 'Courier';
const fontLabel = isAndroid ? androidFont : iosFont;

export const StyledCode = styled.Text`
    font-family: ${fontLabel};
    font-size: 14px;
    font-weight: 500;
`;

export const CodeEditor = styled.View`
    width: 100%;
    background-color: rgb(250, 250, 250);
`;