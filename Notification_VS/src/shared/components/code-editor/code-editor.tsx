import * as div from './code-editor.style';
import { ICodeEditor } from '../../interfaces/code-editor';
import * as React from 'react';
import MonacoEditor from 'react-monaco-editor';

interface Props extends ICodeEditor {

}

interface State { }

export class CodeEditor extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {

        const {
            srcCode,
            width,
            height,
            language
        } = this.props;

        const options = {
            selectOnLineNumbers: true,
            minimap: {
                enabled: false
            },
        };

        return (
            <div.CodeEditor data-cy='code-editor'>
                <MonacoEditor
                    width={width}
                    height={height}
                    language={language}
                    theme='vs'
                    value={srcCode}
                    options={options}
                    onChange={(value: string, e: any) => { this.onChange(value, e); !!this.props.onChangeValue && this.props.onChangeValue(value) }}
                    editorDidMount={this.editorDidMount}
                />
            </div.CodeEditor>
        );
    }

    private editorDidMount(_editor: any, _monaco: any) {
        // console.log('editorDidMount', editor);
        // editor.focus();
    }

    private onChange(newValue: any, e: any) {
        console.log('onChange', newValue, e);
    }
}
