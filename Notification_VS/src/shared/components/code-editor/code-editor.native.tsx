import * as div from './code-editor.native.style';
import { setupMonacoEditorConfig } from './code-editor.native.utils';
import { ICodeEditor } from '../../interfaces/code-editor';
import * as React from 'react';
import { ScrollView, View } from 'react-native';
import { WebView, WebViewMessageEvent } from 'react-native-webview';
import { WebViewErrorEvent } from 'react-native-webview/lib/WebViewTypes';

interface Props extends ICodeEditor { }

interface State { }

/**
 * Code editor rendered in the mobile builds (ios, android).
 * Currently we are using Monaco code editor, developed by the VS Code team.
 * Because Monaco editor was built for web only we are currently using a webview on mobiles to render the code.
 * The webview was configured not to scale, maintaining similar font ratio to the rest of the app.
 *
 * INFO
 * Sending message to the webview and back is possible by using:
 * IN - this.sendMessageToWebview()
 * OUT - window.ReactNativeWebView.postMessage("Hello!")
 *
 * WARNING
 * <!> Each webview instantiates the Monaco editor once again.
 * Memory sharing between web views is not possible.
 * This means you have to pay attention to how many instances you open.
 *
 * UPDATING
 * This webview is powered by a bundled version of monaco using webpack
 * In order to update the libs on this component we must run the build from:
 * vsc-web\android\app\src\main\assets\monaco-editor\webpack.config.js
 *
 * TODO
 * Share the same code between android and ios probably with symlinks between the builds
 * Cleanup CSS and Nested views
 * Restore Highlights, Annotations
 */
export class CodeEditor extends React.Component<Props, State> {

    webview: WebView;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { srcCode, width, height } = this.props;
        let language = 'javascript';

        return (
            <ScrollView data-cy='code-editor' style={{ width: '100%' }}>
                <div.CodeEditor>
                    <View style={{ height, width }}>
                        <WebView
                            ref={webview => { this.webview = webview; }}
                            onMessage={this.getMessageFromWebview}
                            onError={this.onError}
                            scalesPageToFit={false}
                            injectedJavaScript={setupMonacoEditorConfig({ width, height, srcCode, language })}
                            javaScriptEnabled={true}
                            source={{
                                uri: 'file:///android_asset/monaco-editor/dist/index.html',
                                baseUrl: 'file:///android_asset//monaco-editor/dist/'
                            }} />
                    </View>
                    {/* <div.StyledCode>{'ABC'}</div.StyledCode> DEPRECATED */}
                </div.CodeEditor>
            </ScrollView>
        );
    }

    sendMessageToWebview(message: string) {
        this.webview.injectJavaScript(message);
    }

    /** Messages sent from the webview */
    getMessageFromWebview(_event: WebViewMessageEvent) {
        // let data: string = event.nativeEvent.data;
    }

    onError(event: WebViewErrorEvent) {
        const { nativeEvent } = event;
        console.warn('WebView error: ', nativeEvent);
    }
}