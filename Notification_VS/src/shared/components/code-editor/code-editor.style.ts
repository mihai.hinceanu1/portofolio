import { colors } from '../../style/colors';
import styled from 'styled-components';

export const CodeEditor = styled.div`
    & .code-snippet {
        position: absolute;
        z-index: 10;
        background-color: ${colors.$highlightDefault};
    }
`;