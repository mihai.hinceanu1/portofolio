import { ICodeEditor } from '../../interfaces/code-editor';

/** Pass config variables to monaco code editor inside a web view */
export const setupMonacoEditorConfig = (config: ICodeEditor) => {
    let configJson = JSON.stringify(config);

    // <!> Init Monaco Editor
    return `
        document.editor.init(${configJson});

        true; // Required, or you'll sometimes get silent failures
    `;
};