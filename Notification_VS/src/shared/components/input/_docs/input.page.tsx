import * as samples from './input.page.samples';
import { initInputPageState } from './input.page.utils';
import * as assets from '../_assets/input.assets';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { colors } from '../../../style/colors';
import { CodeEditor } from '../../code-editor/code-editor';
import { Input } from '../input';
import * as React from 'react';
import { Text } from 'react-native';

export interface InputState {
    defaultInput: string;
    placeholder: string;
    placeholderColor: string;
    password: string;
    numeric: string;
    labeled: string;
    disabled: string;
    iconImageInput: string;
    iconSvgInput: string;
    inputError: string;
    inputVerified: string;
    inputVerifiedError: string;
    inputWithIconAndError: string;
    override: string;
    onblurValue: string;
    onblurValueTriggered: string;
    onFocusValue: string;
    isFocused: boolean;
}

export class InputPage extends ScrollableDocPage<InputState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: initInputPageState(),
        };
    }

    public renderDemoPage() {
        const { width } = this.state;
        const {
            defaultInput,
            placeholder,
            placeholderColor,
            password,
            numeric,
            labeled,
            disabled,
            iconImageInput,
            iconSvgInput,
            inputError,
            inputVerified,
            inputVerifiedError,
            inputWithIconAndError,
            override,
            onblurValue,
            onblurValueTriggered,
            onFocusValue,
            isFocused,
        } = this.state.overrides;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Input
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Text input can be used in various situations.
                         It was made customizable in order to perfectly represent
                        the use cases you need.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Input
                        </div.DemoTitle>

                        <Input type='text'
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'defaultInput')}
                            value={defaultInput}
                            placeholder={'Default sample'} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Placeholder */}
                <div.Demo data-cy='placeholder' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Placeholder
                        </div.DemoTitle>

                        <Input type='text'
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'placeholder')}
                            value={placeholder}
                            placeholder={'Placeholder example'} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.PLACEHOLDER} />
                </div.Demo>

                {/** Placeholder Color */}
                <div.Demo data-cy='placeholder-color' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Placeholder Color
                        </div.DemoTitle>

                        <Input type='text'
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'placeholderColor')}
                            value={placeholderColor}
                            placeholder={'Blue Placeholder'}
                            placeholderColor={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.PLACEHOLDER_COLOR} />
                </div.Demo>

                {/** Password */}
                <div.Demo data-cy='password' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Password Input
                        </div.DemoTitle>

                        <Input type='password'
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'password')}
                            value={password}
                            placeholder={'Password Input'} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.PASSWORD} />
                </div.Demo>

                {/** Numeric */}
                <div.Demo data-cy='numeric' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Numeric Input
                        </div.DemoTitle>

                        <Input numeric={true}
                            type='text'
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'numeric')}
                            value={numeric}
                            placeholder={'Numeric Input'} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.NUMERIC} />
                </div.Demo>

                {/** Labeled */}
                <div.Demo data-cy='labeled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Labeled Input
                        </div.DemoTitle>

                        <Input type='text'
                            label='Label'
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'labeled')}
                            value={labeled} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.LABELED} />
                </div.Demo>

                {/** Disabled */}
                <div.Demo data-cy='disabled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Disabled Input
                        </div.DemoTitle>

                        <Input type='text'
                            editable={false}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'disabled')}
                            value={disabled} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DISABLED} />
                </div.Demo>

                {/** With IMAGE Icon */}
                <div.Demo data-cy='with-image-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with Image icon
                        </div.DemoTitle>

                        <Input type='text'
                            icon={{ type: 'image', imageUrl: '/auth/Email.png' }}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'iconImageInput')}
                            value={iconImageInput} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ICON_IMAGE_INPUT} />
                </div.Demo>

                {/** With SVG Icon */}
                <div.Demo data-cy='with-svg-icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with SVG icon
                        </div.DemoTitle>

                        <Input type='text'
                            icon={{ type: 'svg', svgPath: assets.VERIFIED, fill: '#555555' }}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'iconSvgInput')}
                            value={iconSvgInput} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ICON_SVG_INPUT} />
                </div.Demo>

                {/** With Error */}
                <div.Demo data-cy='with-error' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with Error
                        </div.DemoTitle>

                        <Input type='text'
                            error={'This input has errors'}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'inputError')}
                            value={inputError} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ERROR_INPUT} />
                </div.Demo>

                {/** Verified */}
                <div.Demo data-cy='verified' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input Verified
                        </div.DemoTitle>

                        <Input type='text'
                            verified={true}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'inputVerified')}
                            value={inputVerified} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.VERIFIED_INPUT} />
                </div.Demo>

                {/** Verified with Error */}
                <div.Demo data-cy='verified-with-error' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input Verified with Error
                        </div.DemoTitle>

                        <Input type='text'
                            verified={true}
                            error={'This some error'}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'inputVerifiedError')}
                            value={inputVerifiedError} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.VERIFIED_ERROR_INPUT} />
                </div.Demo>

                {/** With Icon and (Error or Verified) */}
                <div.Demo data-cy='with-icon-error' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with Icon and Error / Verified
                        </div.DemoTitle>

                        <Input type='text'
                            icon={{ type: 'image', imageUrl: '/auth/Email.png' }}
                            error={'Invalid email'}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'inputWithIconAndError')}
                            value={inputWithIconAndError} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ERROR_WITH_ICON_INPUT} />
                </div.Demo>

                {/** Overrides */}
                <div.Demo data-cy='overrides' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with CSS Properties Override
                        </div.DemoTitle>

                        <Input type='text'
                            overrides={{ root: 'width: 350px;' }}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'override')}
                            value={override} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDE} />
                </div.Demo>

                {/** On Blur function */}
                <div.Demo data-cy='onblur' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with onBlur
                        </div.DemoTitle>

                        <Input type='text'
                            onBlur={() => this.onBlur(onblurValue)}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'onblurValue')}
                            value={onblurValue} />

                        <Text data-cy='onblur-triggered'>
                            {onblurValueTriggered}
                        </Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ONBLUR} />
                </div.Demo>

                {/** On Focus function */}
                <div.Demo data-cy='onfocus' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Input with onFocus
                        </div.DemoTitle>

                        <Input type='text'
                            onFocus={() => this.toggleOnFocus()}
                            onChangeText={(text: string) => this.onChangeTextInput(text, 'onFocusValue')}
                            value={onFocusValue} />

                        {
                            isFocused &&
                            <Text data-cy='onfocus-triggered'>
                                OnFocus Text
                            </Text>
                        }
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ONFOCUS} />
                </div.Demo>

            </>
        );
    }

    private onChangeTextInput(text: string, type?: string) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                [type]: text,
            },
        });
    }

    private onBlur(onblurValue: string) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                onblurValueTriggered: onblurValue,
            },
        });
    }

    private toggleOnFocus() {
        const { isFocused } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                isFocused: !isFocused,
            },
        });
    }
}