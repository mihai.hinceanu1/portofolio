export const DEFAULT = `<Input type='text'
    // This method is defined in parent and controls the input typing event
    // This property is mandatory and is doing pretty much same thing in all inputs
    // therefore it won't appear in other samples.
    onChangeText={(text: string) => this.onChangeTextInput(text, 'defaultInput')}
    // This is where you want to store the value from input.
    value={defaultInput}
/>
`;

export const PLACEHOLDER = `<Input
    // The placeholder attribute specifies a short hint
    // that describes the expected value of an input field.
    placeholder={'Placeholder example'}
/>
`;

export const PASSWORD = `
<Input
    // Specific type of input (password hides characters written)
    type='password'
/>
`;

export const PLACEHOLDER_COLOR = `<Input type='text'
    placeholder={'Placeholder example'}
    placeholderColor='blue'
    onChangeText={(text: string) => this.onChangeTextInput(text, 'placeholder')}
    value={placeholder} />
`;

export const NUMERIC = `<Input
    // Add the tag numeric to the input props to restrict user
    // from typing letters in it
    numeric={true}
/>
`;

export const LABELED = `<Input
    // Descriptive info about input
    label='Some label over here'
 />
`;

export const DISABLED = `<Input
    // Stop user from typing anything in input
    editable={false}
/>
`;

export const ICON_IMAGE_INPUT = `<Input
    // Icons are provided through an object where a type property is mandatory,
    // it can either be 'svg' or 'image' and depending on it other properties are required.
    // For 'image' an imageUrl is necessary.
    icon={{ type: 'image', imageUrl: '/auth/Email.png' }}
/>
`;

export const ICON_SVG_INPUT = `<Input
    // If icon type is 'svg', svgPath and fill must be provided
    icon={{ type: 'svg', svgPath: assets.VERIFIED, fill: '#555555' }}
/>
`;

export const ERROR_INPUT = `<Input
    // Error rendered bellow input box
    // It comes as a string, generated from external source
    error='This input has errors'
/>
`;

export const VERIFIED_INPUT = `<Input
    // Verified Icon shown in the right side of input
    // Icon is given as DEFAULT value
    // there is no possibility to change it (for now..)
    verified={true}
/>
`;

export const VERIFIED_ERROR_INPUT = `<Input
    // Verified Icon shown in the right side of input
    verified={true}
    // If error is present, verified icon should not be loaded
    error='This some error'
/>
`;

export const ERROR_WITH_ICON_INPUT = `<Input
    // The icon will appear on the left side alongside the error icon on the right side.
    icon={{ type: 'image', imageUrl: '/auth/Email.png' }}
    error={'Invalid email'}
/>
`;

export const OVERRIDE = `<Input
    // Override any CSS property
    override={{ root: 'width: 350px' }}
/>
`;

export const ONBLUR = `<Input
    // attach function with custom logic
    onBlur={() => this.onBlur(onblurValue)}
/>

    // ...

// custom function called when onBlur event occurs
// in this example, it displays input value below the input frame
private onBlur(onblurValue: string) {
    this.setState({ onblurValueTriggered: onblurValue });
}
`;

export const ONFOCUS = `<Input
    // attach custom onFocus function
    onFocus={() => this.toggleOnFocus()}
/>

    // ...

// custom function called when onFocus event occurs
// in this example, it toggles a message below input frame
private toggleOnFocus() {
    const { isFocused } = this.state;

    this.setState({ isFocused: !isFocused });
}
`;