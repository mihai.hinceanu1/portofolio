import { InputState } from './input.page';

export const initInputPageState = (): InputState => {
    return {
        defaultInput: '',
        placeholder: '',
        password: '',
        numeric: '',
        labeled: '',
        disabled: '',
        iconImageInput: '',
        iconSvgInput: '',
        inputError: '',
        inputVerified: '',
        inputVerifiedError: '',
        inputWithIconAndError: '',
        override: '',
        onblurValue: '',
        onblurValueTriggered: '',
        onFocusValue: '',
        placeholderColor: '',
        isFocused: false,
    };
};