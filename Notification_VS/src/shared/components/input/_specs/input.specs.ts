/// <reference types="cypress" />

describe('Input', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/input');
    });

    it('Renders default input', () => {
        getEl('default', 'input-container')
            .should('have.length', 1);
    });

    it('Placeholder value', () => {
        getEl('placeholder', 'input')
            .should('have.attr', 'placeholder', 'Placeholder example');
    });

    it('Input of type password', () => {
        getEl('password', 'input')
            .should('have.attr', 'type', 'password');
    });

    it('Numeric input allows only numbers', () => {
        getEl('numeric', 'input')
            .type('h123')
            .should('have.value', '123');
    });

    it('Labeled input has label of value "Label"', () => {
        getEl('labeled', 'input-label')
            .should('have.text', 'Label');
    });

    it('Disabled input is readonly', () => {
        getEl('disabled', 'input')
            .should('have.attr', 'readonly');
    });

    it('Input with Image Icon renders icon', () => {
        getEl('with-image-icon', 'input-icon')
            .should('have.length', 1);
    });

    it('Input with SVG Icon displays icon', () => {
        getEl('with-svg-icon', 'input-icon')
            .should('have.length', 1);
    });

    it('Input with error displays error text and icon', () => {
        getEl('with-error', 'error-icon')
            .should('have.length', 1);

        getEl('with-error', 'error-message')
            .should('have.length', 1)
            .should('have.text', 'This input has errors');
    });

    it('Verified input displays verify icon', () => {
        getEl('verified', 'verified-icon')
            .should('have.length', 1);
    });

    it('Verified input with errors displays only errors', () => {
        getEl('with-error', 'error-icon')
            .should('have.length', 1);

        getEl('with-error', 'error-message')
            .should('have.length', 1)
            .should('have.text', 'This input has errors');

        getEl('with-error', 'verified-icon')
            .should('have.length', 0);
    });

    it('Displays both error and left custom icon', () => {
        getEl('with-icon-error', 'error-icon')
            .should('have.length', 1);

        getEl('with-icon-error', 'error-message')
            .should('have.text', 'Invalid email');

        getEl('with-icon-error', 'input-icon')
            .should('have.length', 1);
    });

    it('Overrides input stylesheet', () => {
        getEl('overrides', 'input')
            .should('have.css', 'width', '350px');
    });

    it('Displays input value on blur', () => {
        getEl('onblur', 'input')
            .focus()
            .type('asdfa')
            .blur();

        getEl('onblur', 'onblur-triggered')
            .should('have.text', 'asdfa');
    });

    it('Toggles text on focus', () => {
        getEl('onfocus', 'input')
            .focus();

        getEl('onfocus', 'onfocus-triggered')
            .should('have.length', 1)
            .should('have.text', 'OnFocus Text');
    });
});

// ===== SELECTORS =====

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}]`)
        .find(`[data-cy=${element}]`);
}
