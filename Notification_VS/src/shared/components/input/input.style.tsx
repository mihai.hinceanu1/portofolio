import { TIcon } from '../../interfaces/icon';
import { pathBoundingBox } from '../../services/raphael.utils';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import { Icon } from '../icon/icon';
import * as React from 'react';
import { Platform } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface Override {
    override: string;
}

interface TextInputProps {
    hasIcon: boolean;
    hasError: boolean;
    height?: number;
    editable?: boolean;
    withBorder?: boolean;
    override?: string;
}

/**
 * Entire wrapper for input
 * Has label + input
 */
export const InputContainer = styled.View<Override>`
    padding: 10px;
    width: 250px;
    ${props => props.override};
`;

export const Input = styled.TextInput<TextInputProps>`
    align-items: center;
    font-size: 14px;
    width: 100%;
    border-radius: 3px;
    border-width: ${props => getInputBorder(props.withBorder)};
    border-color: ${props => getInputFieldBorderColor(props)};
    padding-left: ${props => getInputFieldPaddingLeft(props)};
    padding-right: ${props => getInputFieldPaddingRight(props)};
    height: ${props => props.height || 40}px;

    /* This is platform dependent */
    ${Platform.OS === 'web' && 'outline-style: none;'}
    ${props => props.override};
`;

export const InputLabel = styled.Text`
    margin-bottom: 5px;
    font-size: ${font.$size12}px;
    color: ${colors.$labelColor};
`;

export const InputFrame = styled.View<Override>`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    position: relative;
    ${props => props.override};
`;

export const InputIcon = styled.View`
    height: 100%;
    width: 35px;
    position: absolute;
    top: 0;
    align-items: center;
    justify-content: center;
`;

export const ErrorMessage = styled.Text`
    width: 100%;
    font-size: 14px;
    color: ${colors.$red};
    font-weight: 300;
`;

export const SvgIcon = styled.View`
    height: 100%;
    width: 35px;
    position: absolute;
    top: 0;
    right: 0;
    align-items: center;
    justify-content: center;
`;

// ====== SVG ======

export const iconSvg = (svgPath: string, fill?: string): JSX.Element => {
    let { width, height } = pathBoundingBox(svgPath);

    return (
        <Svg fill='none' width={width + 1} height={height + 1}>
            <Path d={svgPath} fill={fill || 'none'} />
        </Svg>
    );
};

export const renderIcon = (icon: TIcon): JSX.Element => {
    if (icon.type === 'image') {
        return (
            <Icon width={20}
                height={20}
                icon={icon.imageUrl}
                disabled={true} />
        );
    } else {
        return iconSvg(icon.svgPath, icon.fill);
    }
};

// ====== UTILS ======

const getInputFieldBorderColor = (props: TextInputProps) => {
    if (props.hasError) {
        return colors.$red;
    }

    return props.editable ? colors.$inputBorderColor : colors.$inputBorderColorDisabled;
};

const getInputFieldPaddingLeft = (props: TextInputProps) => {
    return props.hasIcon ? '35px' : '10px';
};

const getInputFieldPaddingRight = (props: TextInputProps) => {
    return props.hasError ? '35px' : '10px';
};

function getInputBorder(withBorder: boolean) {
    if (withBorder) {
        return '1px';
    }

    return '0px';
}