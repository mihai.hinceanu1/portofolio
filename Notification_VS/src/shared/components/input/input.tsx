import * as assets from './_assets/input.assets';
import * as div from './input.style';
import { TIcon } from '../../interfaces/icon';
import { InputOverrides } from '../../interfaces/input';
import * as React from 'react';
import { NativeSyntheticEvent, TextInputContentSizeChangeEventData } from 'react-native';

interface Props {
    /**
     * Fires when input text is changing
     * Most of the time, it sets parent's State, but can do anything
     */
    onChangeText: (text: string, type?: string) => void;

    /** Value displayed on UI */
    value: string;

    /**
     * If "password", hides text from the UI
     * The default value is "text"
     * "email" type could come with some extra validations
     */
    type?: 'text' | 'password' | 'email';

    /** Specify if the input can be writable */
    editable?: boolean;

    /**
     * Fire on out of focus
     * Example: Validate input on losing focus (login.page)
     */
    onBlur?: () => void;

    /** Fire when input is active */
    onFocus?: () => void;

    /** Rendered on top of input */
    label?: string;

    /**
     * Mask for empty inputs
     * Either "label" or "placeholder" should be given to any input (or both)
     */
    placeholder?: string;

    placeholderColor?: string;

    /** Allow user to write only numeric chars (0, 1, 2, etc...) */
    numeric?: boolean;

    /**
     * Descriptive icon rendered at the beginning of the input
     * It might represent that the input is a password or it's blocked
     * It should be IMAGE or SVG
     * Icon should have either svgPath or imageUrl
     */
    icon?: TIcon;

    /**
     * Error message that is rendered bellow input box (if there is any)
     * If "verified" is true and error exists, only error should be shown
     */
    error?: string;

    /** Verified flag that renders successfully icon in green */
    verified?: boolean;

    /** Auto Focus input when mounted */
    autoFocus?: boolean;

    /** Expand input area when overflowing */
    multiline?: boolean;

    /**
     * Define whether input shows border or not.
     * Default value is true
     */
    withBorder?: boolean;

    /** Override CSS properties */
    overrides?: InputOverrides;
}

interface State {
    /**
     * Because use can be multiline, dynamic height will be used to expand input area
     * if text overlaps available height at certain moments
     */
    height: number;
}

/**
 * Generic Input that renders as a Box, containing
 * Label (not mandatory), Icon (not mandatory)
 * and React Native TextInput
 */
export class Input extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            height: 40,
        };
    }

    public render() {
        const {
            onChangeText,
            value,
            label,
            type,
            placeholder,
            placeholderColor,
            numeric,
            icon,
            editable,
            error,
            autoFocus,
            verified,
            multiline,
            withBorder,
            overrides,
        } = this.props;
        const { height } = this.state;

        return (
            <div.InputContainer data-cy='input-container'
                override={overrides && overrides.root}>

                {/** Label */}
                {
                    !!label &&
                    <div.InputLabel data-cy='input-label'>
                        {label}
                    </div.InputLabel>
                }

                {/** Body */}
                <div.InputFrame data-cy='input-frame'
                    override={overrides && overrides.inputFrame}>

                    {/** Icon */}
                    {
                        icon &&
                        <div.InputIcon data-cy='input-icon'>
                            {div.renderIcon(icon)}
                        </div.InputIcon>
                    }

                    {/** Actual Input */}
                    <div.Input data-cy='input' testID='input'
                        onFocus={() => this.onInputFocus()}
                        onBlur={() => this.onInputBlur()}
                        value={value}
                        height={height}
                        editable={editable !== undefined ? editable : true}
                        onChangeText={onChangeText}
                        onContentSizeChange={event => this.onContentSizeChange(event)}
                        placeholderTextColor={placeholderColor ? placeholderColor : '#c4c4c4'}
                        secureTextEntry={type === 'password' ? true : false}
                        keyboardType={numeric ? 'numeric' : 'default'}
                        underlineColorAndroid={'transparent'}
                        placeholder={placeholder}
                        hasIcon={!!icon ? true : false}
                        hasError={!!error ? true : false}
                        autoFocus={autoFocus ? true : false}
                        multiline={multiline || false}
                        withBorder={withBorder !== undefined ? withBorder : true}
                        override={overrides && overrides.input}/>

                    {/** Error icon */}
                    {
                        !!error &&
                        <div.SvgIcon data-cy='error-icon'>
                            {div.iconSvg(assets.ERROR, '#EA4335')}
                        </div.SvgIcon>
                    }

                    {
                        /**
                         * Verified icon - display only if there are no errors
                         */
                        (verified && !error) &&
                        <div.SvgIcon data-cy='verified-icon'>
                            {div.iconSvg(assets.VERIFIED, '#00C3A7')}
                        </div.SvgIcon>
                    }
                </div.InputFrame>

                {/* // TODO Update data-cy to match div name */}
                {/** Error Message */}
                {
                    !!error &&
                    <div.ErrorMessage data-cy='error-message'>
                        {error}
                    </div.ErrorMessage>
                }
            </div.InputContainer>
        );
    }

    private onInputFocus() {
        const { onFocus } = this.props;

        onFocus && onFocus();
    }

    private onInputBlur() {
        const { onBlur } = this.props;

        onBlur && onBlur();
    }

    private onContentSizeChange(event: NativeSyntheticEvent<TextInputContentSizeChangeEventData>) {
        const { multiline } = this.props;
        // const { height } = this.state;
        const currentHeight = event.nativeEvent.contentSize.height;

        if (multiline && currentHeight > 40) {
            this.setState({ height: currentHeight });
        } else {
            this.setState({ height: 40 });
        }
    }
}