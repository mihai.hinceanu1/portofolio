/// <reference types="cypress" />

describe('Textarea Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/textarea');

        cy.get('[data-cy=textarea-page]')
            .should('have.length', 1);
    });

    it('Default textarea and inserts value inside', () => {
        getEl('default', 'textarea')
            .should('have.length', 1)
            .type('Value')
            .should('have.value', 'Value');
    });

    it('Textarea has Placeholder', () => {
        getEl('placeholder', 'textarea')
            .should('have.attr', 'placeholder');
    });

    it('Disabled textarea', () => {
        getEl('disabled', 'textarea')
            .should('have.length', 1)
            .should('have.attr', 'readonly');
    });

    it('Labeled textarea and label text', () => {
        getEl('labeled', 'textarea')
            .should('have.length', 1);

        getEl('labeled', 'textarea-label')
            .should('have.length', 1)
            .should('have.text', 'Nice label');
    });

    it('Textarea with errors', () => {
        getEl('with-error', 'error-message')
            .should('have.length', 1)
            .should('have.text', 'This is an error');
    });

    it('Overrided CSS Properties', () => {
        getEl('overrides', 'textarea')
            .should('have.css', 'width', '350px');
    });

    it('Handles onFocus', () => {
        getEl('onfocus', 'textarea')
            .focus();

        getEl('onfocus', 'onfocus-triggered')
            .should('have.length', 1);
    });

    it('Handles onBlur', () => {
        getEl('onblur', 'textarea')
            .focus()
            .blur();

        getEl('onblur', 'onblur-triggered')
            .should('have.length', 1);
    });
});

// ignore ts-lint
function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}]`)
        .find(`[data-cy=${element}]`);
}
