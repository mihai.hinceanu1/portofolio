import * as samples from './textarea.page.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Textarea } from '../textarea';
import * as React from 'react';
import { Text } from 'react-native';

interface TextareaState {
    defaultValue: string;
    placeholder: string;
    disabled: string;
    labeled: string;
    withError: string;
    override: string;
    onfocus: string;
    onblur: string;
    onfocusTriggered: boolean;
    onblurTriggered: boolean;
}

export class TextareaPage extends ScrollableDocPage<TextareaState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                defaultValue: '',
                placeholder: '',
                disabled: '',
                labeled: '',
                withError: '',
                override: '',
                onfocus: '',
                onblur: '',
                onfocusTriggered: false,
                onblurTriggered: false,
            },
        };
    }

    public renderDemoPage() {
        const { width } = this.state;
        const {
            defaultValue,
            placeholder,
            disabled,
            labeled,
            withError,
            override,
            onfocus,
            onblur,
            onfocusTriggered,
            onblurTriggered,
        } = this.state.overrides;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width} data-cy='textarea-page'>
                    <div.OverviewTitle>
                        Textarea
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Simulates HTML Default Textarea
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Textarea
                        </div.DemoTitle>

                        <Textarea value={defaultValue}
                            onChangeText={(text: string) => this.onChangeText(text, 'defaultValue')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Placeholder */}
                <div.Demo data-cy='placeholder' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Textarea with Placeholder
                        </div.DemoTitle>

                        <Textarea value={placeholder}
                            placeholder='Textarea placeholder'
                            onChangeText={(text: string) => this.onChangeText(text, 'placeholder')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.PLACEHOLDER} />
                </div.Demo>

                {/** Disabled */}
                <div.Demo data-cy='disabled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Disabled Textarea
                        </div.DemoTitle>

                        <Textarea value={disabled}
                            editable={false}
                            onChangeText={(text: string) => this.onChangeText(text, 'disabled')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DISABLED} />
                </div.Demo>

                {/** Labeled */}
                <div.Demo data-cy='labeled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Labeled Textarea
                        </div.DemoTitle>

                        <Textarea value={labeled}
                            label={'Nice label'}
                            onChangeText={(text: string) => this.onChangeText(text, 'labeled')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.LABELED} />
                </div.Demo>

                {/** With Error */}
                <div.Demo data-cy='with-error' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            With Error Textarea
                        </div.DemoTitle>

                        <Textarea value={withError}
                            error='This is an error'
                            onChangeText={(text: string) => this.onChangeText(text, 'withError')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.WITH_ERROR} />
                </div.Demo>

                {/** Overrides */}
                <div.Demo data-cy='overrides' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides CSS Stylesheet
                        </div.DemoTitle>

                        <Textarea value={override}
                            overrides={{ root: 'width: 350px' }}
                            onChangeText={(text: string) => this.onChangeText(text, 'override')} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDES} />
                </div.Demo>

                {/** On Focus */}
                <div.Demo data-cy='onfocus' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Textarea onFocus
                        </div.DemoTitle>

                        <Textarea value={onfocus}
                            onFocus={() => this.toggleOnFocus()}
                            onChangeText={(text: string) => this.onChangeText(text, 'onfocus')} />

                        {
                            onfocusTriggered &&
                            <Text data-cy='onfocus-triggered'>
                                Text from onfocus Event
                            </Text>
                        }
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ONFOCUS} />
                </div.Demo>

                {/** On Blur */}
                <div.Demo data-cy='onblur' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Textarea onBlur
                        </div.DemoTitle>

                        <Textarea value={onblur}
                            onBlur={() => this.toggleOnBlur()}
                            onChangeText={(text: string) => this.onChangeText(text, 'onblur')} />

                        {
                            onblurTriggered &&
                            <Text data-cy='onblur-triggered'>
                                Text from onblur Event
                            </Text>
                        }
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ONBLUR} />
                </div.Demo>
            </>
        );
    }

    private onChangeText(text: string, type?: string) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                [type]: text,
            },
        });
    }

    private toggleOnFocus() {
        const { onfocusTriggered } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                onfocusTriggered: !onfocusTriggered,
            },
        });
    }

    private toggleOnBlur() {
        const { onblurTriggered } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                onblurTriggered: !onblurTriggered
            },
        });
    }
}
