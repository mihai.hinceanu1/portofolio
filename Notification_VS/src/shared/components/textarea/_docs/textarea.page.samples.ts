export const DEFAULT = `<Textarea value={defaultValue}
    onChangeText={(text: string) => this.onChangeText(text, 'defaultValue')} />
`;

export const PLACEHOLDER = `<Textarea value={placeholder}
    // Suggestive Textarea text
    placeholder='Textarea placeholder'
    onChangeText={(text: string) => this.onChangeText(text, 'placeholder')} />
`;

export const DISABLED = `<Textarea value={disabled}
    // Tell component to be disabled
    editable={false}
    onChangeText={(text: string) => this.onChangeText(text, 'disabled')} />
`;

export const LABELED = `<Textarea value={labeled}
    // Add any suggestive string as label
    // Rendered above TextareaField
    label={'Nice label'}
    onChangeText={(text: string) => this.onChangeText(text, 'labeled')} />
`;

export const WITH_ERROR = `<Textarea value={withError}
    // Displays error below TextareaField
    error='This is an error'
    onChangeText={(text: string) => this.onChangeText(text, 'withError')} />
`;

export const OVERRIDES = `<Textarea value={override}
    // Override CSS Stylesheet (only for root)
    // Textarea width is 350px
    overrides={{ root: 'width: 350px' }}
    onChangeText={(text: string) => this.onChangeText(text, 'override')} />
`;

export const ONFOCUS = `<Textarea value={onfocus}
    // Toggles text on input focus (clicking inside)
    onFocus={() => this.toggleOnFocus()}
    onChangeText={(text: string) => this.onChangeText(text, 'onfocus')} />
`;

export const ONBLUR = `<Textarea value={onblur}
    // Toggles text on input blur (clicking outside)
    onBlur={() => this.toggleOnBlur()}
    onChangeText={(text: string) => this.onChangeText(text, 'onblur')} />
`;
