import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import styled from 'styled-components/native';

interface Override {
    override: string;
}

interface TextareaProps {
    hasError: boolean;
    editable: boolean;
}

export const Textarea = styled.View<Override>`
    margin: 10px;
    width: 250px;
    height: 100px;
    ${props => props.override};
`;

export const TextareaField = styled.TextInput<TextareaProps>`
    height: 100%;
    font-size: 14px;
    width: 100%;
    outline-width: 0px;
    border-width: 1px;
    border-radius: 3px;
    padding: 10px;
    border-color: ${props => getITextareaFieldBorderColor(props)};
`;

export const TextareaLabel = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$labelColor};
`;

export const TextareaFrame = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 5px;
    width: 100%;
    position: relative;
`;

export const Error = styled.Text`
    width: 100%;
    font-size: 14px;
    color: ${colors.$red};
    font-weight: 300;
`;

// ====== UTILS ======

const getITextareaFieldBorderColor = (props: TextareaProps) => {
    if (props.hasError) {
        return colors.$red;
    }

    return props.editable ? colors.$inputBorderColor : colors.$inputBorderColorDisabled;
};