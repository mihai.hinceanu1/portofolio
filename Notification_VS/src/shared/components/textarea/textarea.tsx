import * as div from './textarea.style';
import * as React from 'react';

interface Props {
    /** Current Textarea value displayed */
    value: string;

    /** Triggers when Textarea value is changed */
    onChangeText: (text: string) => void;

    /** Specify if Textarea can be writable */
    editable?: boolean;

    /** Fire when cursor out of Textarea */
    onBlur?: () => void;

    /** Fire when cursor is inside Textarea */
    onFocus?: () => void;

    /**
     * Rendered on top of Textarea
     * Represents suggestive meaning of the Textarea
     */
    label?: string;

    /** Mask for empty inputs */
    placeholder?: string;

    /**
     * Error message that is rendered bellow input box (if there is any)
     * Validation is done in the parent component
     * Textarea needs to know only if there is any error so that it will be rendered accordingly
     */
    error?: string;

    /** Override CSS properties */
    overrides?: {
        /** Component that encapsulated the others */
        root?: string;
    };
}
interface State {}

/**
 * Generic Textarea that renders an InputField, but bigger than Input component
 * Simulates the HTML Default Textarea
 * Can have a descriptive label
 */
export class Textarea extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const {
            value,
            onChangeText,
            editable,
            placeholder,
            error,
            overrides,
            label,
        } = this.props;

        return (
            <div.Textarea data-cy='textarea-box' override={overrides && overrides.root}>
                {/** Header */}
                {
                    label &&
                    <div.TextareaLabel data-cy='textarea-label'>
                        {label}
                    </div.TextareaLabel>
                }

                {/** Texarea input */}
                <div.TextareaField data-cy='textarea'
                    value={value}
                    onBlur={() => this.onBlur()}
                    onFocus={() => this.onFocus()}
                    onChangeText={onChangeText}
                    placeholderTextColor='#c4c4c4'
                    placeholder={placeholder}
                    multiline={true}
                    editable={editable !== undefined ? editable : true}
                    hasError={!!error ? true : false} />

                {/** Error */}
                {
                    error &&
                    <div.Error data-cy='error-message'>
                        {error}
                    </div.Error>
                }
            </div.Textarea>
        );
    }

    private onBlur() {
        const { onBlur } = this.props;

        onBlur && onBlur();
    }

    private onFocus() {
        const { onFocus } = this.props;

        onFocus && onFocus();
    }
}
