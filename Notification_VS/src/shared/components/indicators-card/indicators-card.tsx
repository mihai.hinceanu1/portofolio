import * as div from './indicators-card.style';
import { IIndicatorsCardItem } from '../../interfaces/indicators-card';
import * as React from 'react';

interface Props {
    indicators: IIndicatorsCardItem[];

    /** Displayed on top of card */
    title: string;

    /** Flag for adaptive style */
    isNarrow: boolean;

    /** Override CSS properties */
    overrides?: {

        /** Component that encapsulated the others */
        root?: string;
    };
}

/**
 * Renders a rectangle that has a title and different statistics
 * Statistics can have different colors, depending on the needs
 * The default color is black
 * Can represent an overview regarding current lessons read (10 not finished, 2 finished etc.)
 */
export const IndicatorsCard: React.FunctionComponent<Props> = (props: Props) => {
    const { title, indicators, overrides, isNarrow } = props;

    return (
        <div.IndicatorsCard data-cy='indicators-card'
            isNarrow={isNarrow}
            override={overrides && overrides.root}>

            {/** Title */}
            {
                !isNarrow &&
                <div.Title data-cy='title'>
                    {title}
                </div.Title>
            }

            {
                !!indicators.length &&
                indicators.map(indicator =>
                    <div.Indicator data-cy='indicator'
                        key={indicator._id}
                        isNarrow={isNarrow}>

                        {/** Value */}
                        <div.Count data-cy='value'
                            isNarrow={isNarrow}
                            color={indicator.color}>
                            {indicator.value}
                        </div.Count>

                        {/** Label */}
                        <div.Label data-cy='label'
                            isNarrow={isNarrow}
                            color={indicator.color}>
                            {indicator.label}
                        </div.Label>

                    </div.Indicator>
                )
            }
        </div.IndicatorsCard>
    );
};
