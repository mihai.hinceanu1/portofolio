export const DEFAULT_CODE = `
<IndicatorsCard title='Default'
    // "indicators" prop is an empty array
    indicators={[]} />
`;

export const WITH_ITEMS_CODE = `
const WITH_ITEMS: IIndicatorsCardItem[] = [{
    _id: '1',
    label: 'Bananas',
    value: '10',
}, {
    _id: '2',
    label: 'Grapes',
    value: '100',
}, {
    _id: '3',
    label: 'Oranges',
    value: '57',
}];

<IndicatorsCard title='With Items'
    // attach newly createad array to "indicators" prop
    // prop "color" is by default black
    indicators={WITH_ITEMS} />
`;

export const WITH_ITEMS_AND_COLORS_CODE = `
const WITH_ITEMS_AND_COLORS: IIndicatorsCardItem[] = [{
    _id: '1',
    label: 'Bananas',
    value: '10',
    color: colors.$blue,
}, {
    _id: '2',
    label: 'Grapes',
    value: '100',
    color: colors.$green,
}, {
    _id: '3',
    label: 'Oranges',
    value: '57',
    color: colors.$grey,
}];

<IndicatorsCard title='With Items'
    // attach newly createad array to "indicators" prop
    indicators={WITH_ITEMS_AND_COLORS} />
`;

export const OVERRIDES_CODE = `
<IndicatorsCard title='Overrides'
    // set components's width to 300px
    overrides={{ root: 'width: 300px' }}
    indicators={samples.WITH_ITEMS_AND_COLORS} />
`;