import { IIndicatorsCardItem } from '../../../interfaces/indicators-card';
import { colors } from '../../../style/colors';

export const DEFAULT: IIndicatorsCardItem[] = [];

export const WITH_ITEMS: IIndicatorsCardItem[] = [
    {
        _id: '1',
        label: 'Black Holes',
        value: '2'
    }, {
        _id: '2',
        label: 'Nebula',
        value: '3',
    }
];

export const WITH_ITEMS_AND_COLORS: IIndicatorsCardItem[] = [
    {
        _id: '1',
        label: 'Bananas',
        value: '10',
        color: colors.$blue,
    }, {
        _id: '2',
        label: 'Grapes',
        value: '100',
        color: colors.$green,
    }, {
        _id: '3',
        label: 'Oranges',
        value: '57',
        color: colors.$grey,
    }
];

export const OVERRIDES: IIndicatorsCardItem[] = [
    {
        _id: '1',
        label: 'Black Holes',
        value: '2'
    }, {
        _id: '2',
        label: 'Nebula',
        value: '3',
    }
];