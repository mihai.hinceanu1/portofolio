import * as consts from './indicators-card.const';
import * as samples from './indicators-card.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { IndicatorsCard } from '../indicators-card';
import * as React from 'react';

interface IndicatorsCardState { }

export class IndicatorsCardPage extends ScrollableDocPage<IndicatorsCardState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        const { width } = this.state;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width} data-cy='indicators-card-overview'>
                    <div.OverviewTitle>
                        Indicators Card
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Shows different stats about certain domain
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Indicators Card
                        </div.DemoTitle>

                        <IndicatorsCard title='Default'
                            isNarrow={false}
                            indicators={consts.DEFAULT} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT_CODE} />
                </div.Demo>

                {/** With Items */}
                <div.Demo data-cy='with-items' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Indicators Card With Items
                        </div.DemoTitle>

                        <IndicatorsCard title='With Values'
                            isNarrow={false}
                            indicators={consts.WITH_ITEMS} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.WITH_ITEMS_CODE} />
                </div.Demo>

                {/** With Items And Colors */}
                <div.Demo data-cy='with-items-and-colors' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Indicators Card With Items
                        </div.DemoTitle>

                        <IndicatorsCard title='With Values'
                            isNarrow={false}
                            indicators={consts.WITH_ITEMS_AND_COLORS} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.WITH_ITEMS_AND_COLORS_CODE} />
                </div.Demo>

                {/** Adaptive */}
                <div.Demo data-cy='adaptive' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Adaptive
                        </div.DemoTitle>

                        <IndicatorsCard title='With Values'
                            isNarrow={true}
                            indicators={consts.WITH_ITEMS_AND_COLORS} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.WITH_ITEMS_AND_COLORS_CODE} />
                </div.Demo>

                {/** Overrides CSS Properties */}
                <div.Demo data-cy='overrides' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Override CSS Properties
                        </div.DemoTitle>

                        <IndicatorsCard title='Overrides'
                            isNarrow={false}
                            overrides={{ root: 'width: 300px' }}
                            indicators={consts.OVERRIDES} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.OVERRIDES_CODE} />
                </div.Demo>
            </>
        );
    }
}
