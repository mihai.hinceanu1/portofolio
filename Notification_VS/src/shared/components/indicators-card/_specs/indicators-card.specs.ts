/// <reference types="cypress" />

describe('Indicators Card Tests', () => {
    it('Successfully loads the page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/indicators-card');
        getWrapper('indicators-card-overview')
            .should('have.length', 1);
    });

    it('Title with no values', () => {
        getEl('default', 'indicators-card')
            .should('have.length', 1);

        getEl('default', 'title')
            .should('have.text', 'Default');

        getEl('default', 'indicator')
            .should('have.length', 0);
    });

    it('Title with 2 values, color black by default', () => {
        getEl('with-items', 'title')
            .should('have.length', '1');

        getEl('with-items', 'indicator')
            .should('have.length', 2);

        getEl('with-items', 'value')
            .should('have.css', 'color', 'rgb(0, 0, 0)');

        getEl('with-items', 'label')
            .should('have.css', 'color', 'rgb(0, 0, 0)');
    });

    it('Black not used if color prop added', () => {
        getEl('with-items-and-colors', 'indicator')
            .should('have.length', 3);

        getEl('with-items-and-colors', 'value')
            .eq(0)
            .should('not.have.css', 'color', 'rgb(0, 0, 0)');
    });

    it('On narrow screens the card extends on the entire screen', () => {
        /** On small screens the title isn't visible */
        getEl('adaptive', 'title').should('not.exist');
        getEl('adaptive', 'indicators-card').should('have.css', 'flex-direction', 'row');
    });

    it('Overrides CSS Props', () => {
        getEl('overrides', 'indicators-card')
            .should('have.css', 'width', '300px');
    });
});

// ===== UTILS =====

function getWrapper(wrapper) {
    return cy.get(`[data-cy=${wrapper}]`);
}

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}]`)
        .find(`[data-cy=${element}]`);
}