import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import { BOX_SHADOW_CSS } from '../../style/shadow.style';
import styled from 'styled-components/native';

// ====== PROPS ======

interface IndicatorProps {
    color: string;
}

interface AdaptiveProps {
    isNarrow: boolean;
}

interface Override {
    override: string;
}

// ====== STYLE ======

export const IndicatorsCard = styled.View<AdaptiveProps & Override>`
    width: 150px;
    padding: 10px;
    min-height: 50px;
    background-color: ${colors.$white};
    align-items: center;
    ${BOX_SHADOW_CSS};
    ${props => getAdaptiveStyle(props)}
    ${props => props.override}
`;

export const Title = styled.Text`
    font-weight: 600;
    font-size: ${font.$size16}px;
    text-align: center;
`;

export const Indicator = styled.View<AdaptiveProps>`
    max-width: 80px;
    align-items: center;
    justify-content: center;
    ${props => getIndicatorAdaptiveStyle(props)};
`;

export const Count = styled.Text<IndicatorProps & AdaptiveProps>`
    font-size: ${font.$size22}px;
    ${props => getCountAdaptiveStyle(props)};
    color: ${props => getValueFrameColor(props)};
`;

export const Label = styled.Text<IndicatorProps & AdaptiveProps>`
    text-align: center;
    ${props => getLabelAdaptiveStyle(props)};
    color: ${props => getValueFrameColor(props)};
`;

// ===== UTILS =====

function getValueFrameColor(props: IndicatorProps) {
    return props.color ? props.color : colors.$black;
}

function getAdaptiveStyle(props: AdaptiveProps): string {
    if (props.isNarrow) {
        return `
            width: 100%;
            min-width: 350px;
            padding: 10px 0px;
            flex-direction: row;
            border-bottom-width: 1px;
            justify-content: space-around;
            border-color: ${colors.$lightGrey};
        `;
    } else {
        return `
            width: 160px;
            align-self: flex-start;
            flex-direction: column;
        `;
    }
}

function getIndicatorAdaptiveStyle(props: AdaptiveProps): string {
    if (props.isNarrow) {
         return `flex-direction: row;`;
    } else {
        return `
            height: 80px;
            flex-direction: column;
        `;
    }
}

function getCountAdaptiveStyle(props: AdaptiveProps): string {
    if (props.isNarrow) {
        return `font-weight: 600;`;
    } else {
        return `
            font-weight: 700;
            text-align: center;
        `;
    }
}

function getLabelAdaptiveStyle(props: AdaptiveProps): string {
    if (props.isNarrow) {
        return `
            font-size: ${font.$size10}px;
            margin-left: 5px;
        `;
    }

    return ``;
}
