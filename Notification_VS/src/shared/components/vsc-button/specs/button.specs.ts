/// <reference types='cypress' />

describe('Button Tests', () => {
    it('Loads the page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/button');
        cy.get('[data-cy=vsc-button-page]').should('exist');
    });

    it('Renders flat button', () => {
        getEl('flat', 'button').should('exist');
        getEl('flat', 'button').should('have.css', 'background-color', 'rgba(0, 0, 0, 0)');
    });

    it('Renders inverted button', () => {
        getEl('inverted', 'button').should('exist');
        getEl('inverted', 'button').should('have.css', 'background-color', 'rgb(255, 255, 255)');
    });

    it('Renders ghost button', () => {
        getEl('ghost', 'button').should('exist');
        getEl('ghost', 'button').should('have.css', 'border-bottom-width', '1px');
    });

    it('Renders fill button', () => {
        getEl('fill', 'button').should('exist');
        getEl('fill', 'button').should('have.css', 'background-color', 'rgb(13, 142, 241)');
        getEl('fill', 'button').within(() => {
            cy.get('[data-cy=text]').should('have.css', 'color', 'rgb(255, 255, 255)');
        });
    });

    it('Renders FAB button', () => {
        getEl('fab', 'button').should('exist');
        getEl('fab', 'icon').should('exist');
        getEl('fab', 'text').should('not.exist');
    });

    it('Renders rounded button', () => {
        getEl('rounded', 'button').should('exist');
        getEl('rounded', 'button').should('have.css', 'background-color', 'rgb(13, 142, 241)');
        getEl('rounded', 'button').within(() => {
            cy.get('[data-cy=text]').should('have.css', 'color', 'rgb(255, 255, 255)');
        });
    });

    it('Renders disabled button', () => {
        getEl('disabled', 'button').should('exist');
        getEl('disabled', 'button').should('have.css', 'opacity', '0.2');
    });

    it('Renders button with icon', () => {
        getEl('icon', 'button').should('exist');
        getEl('icon', 'button').within(() => {
            cy.get('[data-cy=icon]').should('exist');
            cy.get('[data-cy=text]').should('exist');
        });
    });

    it('Renders button with color', () => {
        getEl('color', 'button').should('exist');
        getEl('color', 'button').should('have.css', 'background-color', 'rgb(234, 67, 53)');
    });

    it('Calls callback when fired', () => {
        getEl('callback', 'button').should('exist');
        getEl('callback', 'button').click();
        getEl('callback', 'counter').should('have.text', '1');
    });

    it('Renders overrides button', () => {
        getEl('override', 'button').should('exist');
        getEl('override', 'button').should('have.css', 'background-color', 'rgb(0, 128, 0)');
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}
