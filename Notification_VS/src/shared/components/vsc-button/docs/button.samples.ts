export const flatButton = `
<Button type={ButtonType.FLAT}
    text='Flat Button'
    color={colors.$blue} />
`;

export const flatButtonInverted = `
<Button type={ButtonType.FLAT}
    text='Flat Button'
    inverted={true}
    color={colors.$blue} />
`;

export const ghostButton = `
<Button type={ButtonType.GHOST} />
`;

export const fillButton = `
<Button type={ButtonType.FILL} />
`;

export const fabButton = `
<Button type={ButtonType.FAB} />
`;

export const roundedButton = `
<Button type={ButtonType.ROUNDED} />
`;

export const disabled = `
<Button disabled={true} />
`;

export const icon = `
<Button icon={{ position: IconPosition.LEFT, path: CHECKMARK }} />
`;

export const color = `
<Button color={colors.$red} />
`;

export const callback = `
<Button callback={() => incrementCounter()} />
`;

export const override = `
<Button overrides={{ root: 'background-color: green' }} />
`;
