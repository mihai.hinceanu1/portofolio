import * as samples from './button.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CHECKMARK } from '../../../assets/icons';
import { ButtonType, IconPosition } from '../../../interfaces/button';
import { colors } from '../../../style/colors';
import { CodeEditor } from '../../code-editor/code-editor';
import { Button } from '../button';
import * as React from 'react';
import { Text } from 'react-native';

interface VscButtonState {
    counter: number;
}

export class VscButtonPage extends ScrollableDocPage<VscButtonState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                counter: 0,
            },
        };
    }

    public renderDemoPage() {
        const { width, overrides } = this.state;
        const { counter } = overrides;

        return (
            <>
                <div.Overview data-cy='vsc-button-page' width={width}>
                    <div.OverviewTitle>
                        Button
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Custom button that can have a custom icon.
                        It takes another form, depeding on the prop 'Type' given.
                        If it's inverted, background color will be white.
                        Disable will display the button with a slighty opacity applied.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Flat button */}
                <div.Demo data-cy='flat' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            FLAT button
                        </div.DemoTitle>

                        <Button type={ButtonType.FLAT}
                            text='Flat Button'
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.flatButton} />
                </div.Demo>

                {/** Button inverted */}
                <div.Demo data-cy='inverted' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            FLAT button
                        </div.DemoTitle>

                        <Button type={ButtonType.FLAT}
                            text='Flat Button'
                            inverted={true}
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.flatButtonInverted} />
                </div.Demo>

                {/** Ghost Button */}
                <div.Demo data-cy='ghost' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Ghost Button
                        </div.DemoTitle>

                        <Button type={ButtonType.GHOST}
                            text='Ghost Button'
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.ghostButton} />
                </div.Demo>

                {/** Fill Button */}
                <div.Demo data-cy='fill' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Fill Button
                        </div.DemoTitle>

                        <Button type={ButtonType.FILL}
                            text='FIll Button'
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.fillButton} />
                </div.Demo>

                {/** FAB Button */}
                <div.Demo data-cy='fab' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            FAB Button
                        </div.DemoTitle>

                        <Button type={ButtonType.FAB}
                            icon={{ path: CHECKMARK, position: IconPosition.LEFT }}
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.fabButton} />
                </div.Demo>

                {/** Rounded Button */}
                <div.Demo data-cy='rounded' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Rounded Button
                        </div.DemoTitle>

                        <Button type={ButtonType.ROUNDED}
                            text={'Rounded'}
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.roundedButton} />
                </div.Demo>

                {/** Disabled */}
                <div.Demo data-cy='disabled' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Disabled Button
                        </div.DemoTitle>

                        <Button type={ButtonType.FLAT}
                            text={'Rounded'}
                            disabled={true}
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.disabled} />
                </div.Demo>

                {/** Icon */}
                <div.Demo data-cy='icon' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Button with icon
                        </div.DemoTitle>

                        <Button type={ButtonType.FLAT}
                            text={'Icon'}
                            icon={{ position: IconPosition.LEFT, path: CHECKMARK }}
                            color={colors.$blue} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.icon} />
                </div.Demo>

                {/** Color */}
                <div.Demo data-cy='color' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Button with different color
                        </div.DemoTitle>

                        <Button type={ButtonType.FILL}
                            text={'Color'}
                            color={colors.$red} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.color} />
                </div.Demo>

                {/** Callback */}
                <div.Demo data-cy='callback' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callback
                        </div.DemoTitle>

                        <Button type={ButtonType.FILL}
                            text={'Callback'}
                            color={colors.$blue}
                            callback={() => this.buttonCallback()} />

                        <Text data-cy='counter'>{counter}</Text>
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.callback} />
                </div.Demo>

                {/** Override */}
                <div.Demo data-cy='override' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Override
                        </div.DemoTitle>

                        <Button type={ButtonType.FILL}
                            text={'Callback'}
                            color={colors.$blue}
                            overrides={{ root: 'background-color: green;' }} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.override} />
                </div.Demo>
            </>
        );
    }

    private buttonCallback() {
        const { counter } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                counter: counter + 1,
            },
        });
    }
}