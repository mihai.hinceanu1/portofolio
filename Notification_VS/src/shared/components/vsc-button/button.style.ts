import { ButtonType, IconPosition } from '../../interfaces/button';
import { ReactWebAttributes } from '../../interfaces/workarounds';
import { colors } from '../../style/colors';
import { BOX_SHADOW_CSS, getShadow } from '../../style/shadow.style';
import styled from 'styled-components/native';

// ===== INTERFACES =====

interface ButtonProps extends ReactWebAttributes {
    override?: string;
    type?: ButtonType;
    color?: string;
    isHover?: boolean;
    disabled?: boolean;
    inverted?: boolean;
}

interface TextProps {
    type?: ButtonType;
    color?: string;
    iconPosition?: IconPosition;
    inverted?: boolean;
    override?: string;
}

interface ButtonFrameProps {
    iconPosition?: IconPosition;
}

// ===== COMPONENTS =====

export const Button = styled.TouchableHighlight<ButtonProps>`
    background-color: ${props => getBackgroundColor(props)};
    min-width: 35px;
    height: 35px;
    padding-left: 20px;
    padding-right: 20px;
    ${props => getBorderRadius(props)};
    ${props => getBorder(props)};
    ${props => overrideFABButton(props)};
    ${props => getOpacity(props)};
    ${props => getInvertedStyles(props)};
    ${props => props.override};
`;

export const ButtonFrame = styled.View<ButtonFrameProps>`
    width: 100%;
    height: 100%;
    align-items: center;
    justify-content: center;
    ${props => getButtonFrameFlexDirection(props)}
`;

export const Text = styled.Text<TextProps>`
    font-weight: 600;
    color: ${props => getTextColor(props)};
    ${props => getTextMargin(props)}
    ${props => props.override};
`;

// ===== UTILS =====

function getBackgroundColor(props: ButtonProps) {
    const { type, color } = props;

    if (type === ButtonType.FILL ||
        type === ButtonType.FAB ||
        type === ButtonType.ROUNDED) {
        return color;
    }

    return 'transparent';
}

function getTextColor(props: TextProps) {
    const { type, color, inverted } = props;

    if (inverted) {
        return color;
    }

    if (type === ButtonType.FILL ||
        type === ButtonType.FAB ||
        type === ButtonType.ROUNDED) {
        return colors.$white;
    }

    return color;
}

function getBorder(props: ButtonProps) {
    const { type, color } = props;

    if (type === ButtonType.GHOST) {
        return `
            border-width: 1px;
            border-color: ${color};
        `;
    }

    return;
}

function getBorderRadius(props: ButtonProps) {
    const { type } = props;

    if (type === ButtonType.ROUNDED) {
        return `
            border-radius: 30px;
        `;
    }
}

function overrideFABButton(props: ButtonProps) {
    const { type } = props;

    if (type === ButtonType.FAB) {
        return `
            min-width: 50px;
            width: 50px;
            height: 50px;
            border-radius: 25px;
            padding: 0;
        `;
    }
}

function getOpacity(props: ButtonProps) {
    const { isHover, disabled } = props;

    if (disabled) {
        return `opacity: 0.2`;
    } else if (isHover) {
        return `
            opacity: 0.6;
        `;
    } else {
        return `
            opacity: 1;
        `;
    }
}

function getButtonFrameFlexDirection(props: ButtonFrameProps) {
    if (props.iconPosition === IconPosition.LEFT) {
        return `flex-direction: row-reverse`;
    } else {
        return `flex-direction: row`;
    }
}

function getTextMargin(props: TextProps) {
    if (props.iconPosition === IconPosition.LEFT) {
        return `margin-left: 10px;`;
    } else if (props.iconPosition === IconPosition.RIGHT) {
        return `margin-right: 10px;`;
    }
}

function getInvertedStyles(props: ButtonProps) {
    const { inverted, color } = props;

    if (inverted) {
        return `
            background-color: ${colors.$white};
            border-color: ${color};
            border-width: 1px;
            ${getShadow(BOX_SHADOW_CSS)};
        `;
    }
}