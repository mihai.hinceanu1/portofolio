import * as div from './button.style';
import { Icon } from '../../../icons/components/icon/icon';
import { ButtonType, IButton } from '../../interfaces/button';
import { colors } from '../../style/colors';
import * as React from 'react';

interface Props extends IButton {
    overrides?: {
        root?: string;
        text?: string;
    };
}

interface State {
    isHover: boolean;
}

export class Button extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            isHover: false,
        };
    }

    public render() {
        const { type, color, text, disabled, icon, inverted, callback, overrides } = this.props;
        const { isHover } = this.state;

        return (
            <div.Button data-cy='button'
                onPress={() => callback && callback()}
                onMouseEnter={() => this.toggleHover(true)}
                onMouseLeave={() => this.toggleHover(false)}
                isHover={isHover}
                disabled={disabled}
                type={type}
                color={color}
                inverted={inverted}
                override={overrides && overrides.root}>

                {/** Button Frame */}
                <div.ButtonFrame data-cy='button-frame'
                    iconPosition={icon && icon.position}>

                    {/** Text */}
                    {
                        !!text &&
                        <div.Text data-cy='text'
                            type={type}
                            iconPosition={icon && icon.position}
                            inverted={inverted}
                            color={color}
                            override={overrides && overrides.text}>
                            {text}
                        </div.Text>
                    }

                    {/** Icon */}
                    {
                        icon &&
                        <Icon path={icon.path}
                            width={15}
                            height={15}
                            color={this.iconColor()} />
                    }
                </div.ButtonFrame>
            </div.Button>
        );
    }

    private toggleHover(isHover: boolean) {
        this.setState({ isHover });
    }

    private iconColor() {
        const { type, color, inverted } = this.props;

        if (inverted) {
            return color;
        }

        if (type === ButtonType.GHOST || type === ButtonType.FLAT) {
            return color;
        }

        return colors.$white;
    }
}
