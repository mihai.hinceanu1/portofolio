/// <reference types="cypress" />

describe('Checkbox Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/date-picker');
    });

    it('Open the calendar', () => {
        cy.get('[data-cy=open-calendar]').click({ multiple: true });
        cy.get('[data-cy=calendar]').should('exist');
    });

    xit('Selected calendar date', () => {
        cy.get('[data-cy=active-cell]').first().click({ force: true });
        cy.get('[data-cy=date-day]').first().should(div => {
            const text = div.text();
            expect(text).to.eq(' 01 ');
        });
    });

    xit('Changes selected month', () => {
        cy.get('[data-cy=open-calendar]').click({ multiple: true });
        cy.get('[data-cy=trigger-change-month]').first().click({ multiple: true });
        cy.get('[data-cy=active-cell]').first().click({ force: true });
        cy.get('[data-cy=active-month]').first().should(div => {
            const text = div.text();
            expect(text).to.eq('February');
        });
    });

    xit('Changes the selected year', () => {
        cy.get('[data-cy=open-calendar]').click({ multiple: true });
        cy.get('[data-cy=trigger-change-month-behind]').first().click({ multiple: true });
        cy.get('[data-cy=trigger-change-month-behind]').first().click({ multiple: true });
        cy.get('[data-cy=active-cell]').first().click({ force: true });
        cy.get('[data-cy=active-year]').first().should(div => {
            const text = div.text();
            expect(text).to.eq(' 2019 ');
        });
    });
});