import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { DatePicker } from '../date-picker';
import * as React from 'react';

interface DatePickerState { }

export class DatePickerPage extends ScrollableDocPage<DatePickerState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Datepicker
                        </div.OverviewTitle>
                    <div.OverviewDescription>
                        Datepicker is used in situations such as picking a date to schedule
                        a lesson.
                        </div.OverviewDescription>
                </div.Overview>

                <div.Demo width={width}>

                    <div.DemoArea>
                        <div.DemoTitle>
                            Classic Datepicker
                        </div.DemoTitle>
                        <DatePicker />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={`
<Datepicker />
`}
                    />

                </div.Demo>
            </>
        );
    }
}
