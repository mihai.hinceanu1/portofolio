import styled from 'styled-components/native';

export const DatePicker = styled.View`
    width: 300px;
    display: flex;
    flex-flow: column nowrap;
`;

export const Date = styled.View`
    width: 300px;
    flex-flow: row nowrap;
    align-items: center;
    justify-content: space-evenly;
`;

export const Fill = styled.View`
    flex-grow: 5;
    justify-content: center;
`;

export const Day = styled.View`
    flex-grow: 1;
    justify-content: center;
`;

export const Month = styled.View`
    flex-grow: 2;
    justify-content: center;
`;

export const Year = styled.View`
    flex-grow: 1;
    justify-content: center;
`;

export const Icon = styled.View`
    flex-grow: 1;
    justify-content: center;
`;

export const HeaderContent = styled.Text`
    text-align: center;
`;