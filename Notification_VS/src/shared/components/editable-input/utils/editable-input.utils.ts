import * as icons from '../../../assets/icons';
import { ButtonConfig } from '../../../interfaces/button';
import { colors } from '../../../style/colors';

export const SAVE_BUTTON = (
    onPress: () => void,
): ButtonConfig => ({
    icon: {
        type: 'svg',
        svgPath: icons.CHECKMARK,
    },
    filled: true,
    color: colors.$darkBlue,
    onPress: () => onPress(),
});

export const CANCEL_BUTTON = (
    onPress: () => void,
): ButtonConfig => ({
    icon: {
        type: 'svg',
        svgPath: icons.CROSS,
    },
    filled: true,
    color: colors.$darkBlue,
    onPress: () => onPress(),
});
