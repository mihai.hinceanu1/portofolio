import { InputOverrides } from '../../interfaces/input';
import { ReactWebAttributes } from '../../interfaces/workarounds';
import { colors } from '../../style/colors';
import styled from 'styled-components/native';

// ===== INTERFACES =====
interface Overrides {
    override?: string;
}

interface EditableInputProps extends Overrides, ReactWebAttributes {
    shouldHaveBackground: boolean;
    withBorder: boolean;
}

// ===== COMPONENTS =====

export const EditableInput = styled.View<EditableInputProps>`
    align-items: flex-end;
    position: relative;
    min-height: 40px;
    width: 100%;
    margin-top: 20px;
    margin-bottom: 20px;
    background-color: ${props => getEditableInputBackground(props)};
    ${props => getEditableInputBorder(props.withBorder)}
`;

export const Actions = styled.View`
    justify-content: flex-end;
    flex-direction: row;
    position: absolute;
    bottom: -15px;
    right: -10px;
`;

export const TextFrame = styled.TouchableHighlight`
`;

export const Text = styled.Text`
`;

export const Button = styled.TouchableOpacity`
    width: 100%;
    min-height: 40px;
    height: 100%;
    padding-left: 15px;
    padding-right: 15px;
    justify-content: center;
`;

export const SmallButton = styled.TouchableOpacity`
    width: 10px;
    height: 10px;
    margin-right: 15px;
`;

// ===== OVERRIDES ======

export const INPUT_OVERRIDES = (overrides: string): InputOverrides => ({
    root: `
        width: 100%;
        padding: 0px;
    `,
    input: `
        ${overrides};
    `,
});

// ===== UTILS =====

function getEditableInputBackground(props: EditableInputProps) {
    if (props.shouldHaveBackground) {
        return colors.$lightGrey;
    }

    return 'transparent';
}

function getEditableInputBorder(withBorder: boolean) {
    if (withBorder) {
        return `
            border-width: 1px;
            border-color: ${colors.$darkGrey};
        `;
    }

    return `
        border-width: 0px;
    `;
}
