import * as div from './editable.input.style';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../assets/icons';
import { colors } from '../../style/colors';
import { Input } from '../input/input';
import * as React from 'react';

interface Props {
    value: string;

    onSavePress: (text: string) => void;

    onCancelPress?: (text: string) => void;

    placeholder?: string;

    multiline?: boolean;

    overrides?: {
        root?: string;
        input?: string;
    };
}

interface State {
    initialValue: string;

    newValue: string;

    isEditMode: boolean;

    isHover: boolean;

    isFocus: boolean;
}

export class EditableInput extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            initialValue: props.value,
            newValue: props.value,
            isEditMode: false,
            isHover: false,
            isFocus: false,
        };
    }

    public static getDerivedStateFromProps(props: Props, state: State) {
        const { value } = props;
        const { initialValue, isFocus } = state;

        if (initialValue !== value && !isFocus) {
            return {
                ...state,
                newValue: value,
                initialValue: value,
            };
        }

        return state;
    }

    public render() {
        const { placeholder, overrides, multiline } = this.props;
        const { newValue, isEditMode, isHover } = this.state;

        return (
            <div.EditableInput data-cy='editable-input'
                shouldHaveBackground={!isEditMode && isHover}
                withBorder={isEditMode}
                override={overrides && overrides.root}
                onMouseEnter={(e: React.MouseEvent) => this.onMouseEnter(e)}
                onMouseLeave={(e: React.MouseEvent) => this.onMouseLeave(e)}>

                {
                    !isEditMode &&
                    <div.Button onPress={() => this.onEditPress()}
                        data-cy='edit-button'>

                        {/** Text */}
                        <div.Text data-cy='text'>
                            {newValue || placeholder}
                        </div.Text>
                    </div.Button>
                }

                {
                    isEditMode &&
                    <>
                        {/** Input */}
                        <Input value={newValue}
                            placeholder={placeholder}
                            overrides={div.INPUT_OVERRIDES(overrides && overrides.input)}
                            editable={isEditMode}
                            multiline={multiline}
                            autoFocus={isEditMode}
                            withBorder={false}
                            onFocus={() => this.toggleFocus()}
                            onBlur={() => this.toggleFocus()}
                            onChangeText={(text: string) => this.onChangeText(text)} />

                        {/** Actions */}
                        <div.Actions data-cy='actions'>

                            {/** Save button */}
                            <div.SmallButton data-cy='small-button'
                                onPress={() => this.onSavePress()}>

                                <Icon path={icons.CHECKMARK}
                                    width={15}
                                    height={15}
                                    color={colors.$darkGrey} />
                            </div.SmallButton>

                            {/** Cancel button */}
                            <div.SmallButton data-cy='small-button'
                                onPress={() => this.onCancelPress()}>

                                <Icon path={icons.CROSS}
                                    width={15}
                                    height={15}
                                    color={colors.$darkGrey} />
                            </div.SmallButton>
                        </div.Actions>
                    </>
                }

                {/* {
                    (!isEditMode && isHover) &&
                    <div.EditIcon data-cy='edit-icon' onPress={() => this.onEditPress()}>
                        {iconSvg(icons.EDIT, colors.$darkBlue)}
                    </div.EditIcon>
                } */}
            </div.EditableInput>
        );
    }

    private onChangeText(text: string) {
        this.setState({ newValue: text });
    }

    private onEditPress() {
        this.setState({ isEditMode: true });
    }

    private onSavePress() {
        const { onSavePress } = this.props;
        const { newValue } = this.state;

        this.setState({ isEditMode: false, isHover: false });
        onSavePress && onSavePress(newValue);
    }

    private onCancelPress() {
        const { value } = this.props;

        this.setState({
            isEditMode: false,
            isHover: false,
            newValue: value,
        });
    }

    private onMouseEnter(_e: React.MouseEvent) {
        this.setState({ isHover: true });
    }

    private onMouseLeave(_e: React.MouseEvent) {
        const { isEditMode } = this.state;

        if (isEditMode) {
            this.setState({ isHover: true });
        } else {
            this.setState({ isHover: false });
        }
    }

    private toggleFocus() {
        const { isFocus } = this.state;

        this.setState({ isFocus: !isFocus });
    }
}
