import { APP_CFG } from '../../config/app.config';
import { changeColor } from '../../services/utils.service';
import * as React from 'react';
import { Image, LayoutChangeEvent } from 'react-native';

interface Props {
    /** pathname of the image */
    imgPath: string;

    /** if it has rounded corners */
    round?: boolean;

    /**
     * Blue, Grey, White (default is Black - without the need to add Black to the end of the file)
     * Remember to add the png in the same folder as the default image with respect to the name: icon
     * example icon, iconWhite, iconGrey, iconBlue
     */
    color?: string;
}

interface State {
    // the radius of the border
    borderRadius: number;
}

/**
 * <!> For color change Images must be saved in db as this: `image`, `imageGrey`, `imageBlue`, `imageWhite`
 * by default the color is Black
 *
 * Image fitted in parent View keeps aspect ration
 * @important parent View needs dimensions
 */
export class FitImage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            borderRadius: 0,
        };
    }

    public render() {
        const { imgPath: name, round, color } = this.props;
        const { borderRadius } = this.state;

        return (
            /** with and hight undefined because the parent View will set width and hight to fitImage */
            <Image data-cy='fit-image' testID='fit-img'
                style={{
                    flex: 1,
                    width: undefined,
                    height: undefined,
                    resizeMode: 'contain',
                    borderRadius: round ? borderRadius : 0,
                }}
                onLayout={(event: LayoutChangeEvent) => this.onLayoutFitImage(event)}
                source={{ uri: `${APP_CFG.assets}${color ? changeColor(name, color) : name}` }} />
        );
    }

    private onLayoutFitImage(event: LayoutChangeEvent) {
        // When used in Icon component, if the width is greater than the height
        // and the icon has a 1:1 ratio the borders won't actually form a circle.
        this.setState({
            borderRadius: event.nativeEvent.layout.width / 2,
        });
    }
}
