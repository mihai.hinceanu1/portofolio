/// <reference types="cypress" />

describe('Search Bar Tets', () => {
    it(' Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/search-bar');
    });

    it('Tests input functionality and placeholder', () => {
        cy.get('[data-cy=search-bar]').eq(0);
        cy.get('[data-cy=input]').eq(0).type('test');
        cy.get('[data-cy=input]').eq(0).should('have.value', 'test');
        cy.get('[data-cy=input]').eq(0).invoke('attr', 'placeholder').should('equal', 'Test searchbox');
    });

    it('Tests the disabled input', () => {
        cy.get('[data-cy=search-bar]').eq(1);
        cy.get('[data-cy=input]').eq(1).should('have.prop', 'readonly');
    });
});