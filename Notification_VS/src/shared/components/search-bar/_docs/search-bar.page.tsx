import * as samples from './search-bar.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { SearchBar } from '../search-bar';
import * as React from 'react';

interface SearchBarState {}

export class SearchBarPage extends ScrollableDocPage<SearchBarState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Search Bar
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Shared Search Bar is used as the name says for searches.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <SearchBar
                            placeholder={'Test searchbox'}
                            editable={true}
                        />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Disabled */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Disabled
                        </div.DemoTitle>

                        <SearchBar placeholder={'Test searchbox'}
                            editable={false} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DISABLED} />
                </div.Demo>
            </>
        );
    }
}
