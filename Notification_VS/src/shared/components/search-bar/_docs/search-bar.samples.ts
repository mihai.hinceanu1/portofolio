export const DEFAULT = `<Searchbox
    placeholder={'Test searchbox'}
    editable={true} />
`;

export const DISABLED = `<Searchbox
    placeholder={'Test searchbox'}
    editable={false} />
`;
