import { searchIconPath } from './_assets/search-bar.icons';
import { colors } from '../../style/colors';
import { font } from '../../style/font-sizes';
import * as React from 'react';
import { Platform } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

let isMobile = !(Platform.OS === 'web');

export const TopicSearchInput: any = styled.View`
    width: 450px;
    height: 150px;
`;

export const TopicInput: any = styled.TextInput`
    height: 25px;
    margin-left: 10px;
    font-size: ${font.$size16}px;
    align-items: center;
    width: 350px;
    color: rgb(200,200,200);
    ${!isMobile && 'outline-color: transparent;'}
`;

export const SearchBar = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 380px;
    height: 25px;
    padding: 18px;
    border-radius: 25px;
    background-color: rgba(0,0,0,0.8);
`;

const searchIconStyle = {
    backgroundColor: 'none',
    width: 28,
    height: 24
};

export const searchIconSvg = () =>
    <Svg fill={'none'} style={searchIconStyle} >
        <Path strokeLinecap={'round'} strokeWidth={3} stroke={colors.$grey} d={searchIconPath} />
    </Svg>;