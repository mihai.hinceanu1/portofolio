import * as div from './search-bar.style';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';

interface Props {
    placeholder?: string;
    editable?: boolean;
    onPressIcon?: () => void;
    onChangeText?: () => void;
}

interface State { }

export class SearchBar extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        const { placeholder, editable, onPressIcon, onChangeText } = this.props;

        return (
            <div.SearchBar data-cy='search-bar'>

                {/* Icon */}
                <TouchableOpacity data-cy='icon' onPress={() => onPressIcon()}>
                    {div.searchIconSvg()}
                </TouchableOpacity>

                {/* Input */}
                <div.TopicInput data-cy='input'
                    onChangeText={onChangeText}
                    editable={editable}
                    placeholder={placeholder} />

            </div.SearchBar>
        );
    }
}