import { pathBoundingBox } from '../../services/raphael.utils';
import { BOX_SHADOW_CSS, BOX_SHADOW_NATIVE } from '../../style/shadow.style';
import * as React from 'react';
import { Platform } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface TabProps {
    active?: boolean;
}

export const Tab = styled.TouchableHighlight<TabProps>`
    width: 150px;
    height: 25px;
    margin-right: 5px;
    justify-content: center;
    padding-left: 5px;
    background-color: ${props => getTabBackgroundColor(props)};
    ${getTabShadow()};
`;

export const TabFrame = styled.View`
    width: 100%;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding-left: 5px;
`;

export const TabText = styled.Text`
    font-weight: 300;
    font-size: 13px;
`;

export const CloseIcon = styled.TouchableHighlight`
    width: 25px;
    height: 25px;
    align-items: center;
    justify-content: center;
`;

// ===== UTILS =====
const getTabBackgroundColor = (props: TabProps) => {
    return props.active ? '#ffffff' : '#EFEBE5';
};

function getTabShadow() {
    return Platform.OS !== 'web' ? BOX_SHADOW_NATIVE : BOX_SHADOW_CSS;
}

// ===== SVG =====
export const iconSvg = (svgPath: string, fill?: string): JSX.Element => {
    let { width, height } = pathBoundingBox(svgPath);

    return (
        <Svg fill='red' width={width + 1} height={height + 1}>
            <Path d={svgPath} stroke={fill} />
        </Svg>
    );
};
