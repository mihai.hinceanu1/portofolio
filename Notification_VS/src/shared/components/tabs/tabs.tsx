import { Tab } from './tab';
import * as div from './tabs.style';
import { ITab } from '../../interfaces/tabs';
import * as React from 'react';

interface Props {
    tabs: ITab[];
}

interface State {
    /** Current active tab (represents the key of the tab) */
    activeTab: string;
}

export class Tabs extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            activeTab: props.tabs[0].key,
        };
    }

    public render() {
        const { tabs } = this.props;
        const { activeTab } = this.state;

        return (
            <div.Tabs>
                {
                    !!tabs.length &&
                    tabs.map(tab =>
                        <Tab tab={tab}
                            key={tab.key}
                            activeTab={activeTab}
                            onTabClick={() => this.onTabClick(tab)}
                            onCloseClick={() => tab.onClose()} />
                    )
                }
            </div.Tabs>
        );
    }

    private onTabClick(tab: ITab) {
        const { onClick } = tab;

        /** Set active tab inside Tabs Scope */
        this.setState({ activeTab: tab.key });

        /** If there is any custom method sent by user */
        onClick && onClick();
    }
}
