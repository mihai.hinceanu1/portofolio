import { ITab } from '../../../interfaces/tabs';

export const DEFAULT: ITab[] = [{
    key: '1',
    label: 'hello',
    onClick: () => {
        console.log('Hello');
    },
    onClose: () => {
        console.log('Close Hello');
    },
}, {
    key: '2',
    label: 'world',
    onClick: () => {
        console.log('World');
    },
    onClose: () => {
        console.log('Close World');
    },
}];
