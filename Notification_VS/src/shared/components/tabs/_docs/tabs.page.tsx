import * as samples from './tabs.sample';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { Tabs } from '../tabs';
import * as React from 'react';

interface TabsState { }

export class TabsPage extends ScrollableDocPage<TabsState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        const { width } = this.state;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Tabs
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Explore and allow navigation between groups of content.
                        Can Switch between different views.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Tabs
                        </div.DemoTitle>

                        <Tabs tabs={samples.DEFAULT} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={'hello'} />
                </div.Demo>
            </>
        );
    }
}