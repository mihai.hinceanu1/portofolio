import * as div from './tab.style';
import { CLOSE } from '../../assets/icons';
import { ITab } from '../../interfaces/tabs';
import * as React from 'react';

interface Props {
    tab: ITab;
    activeTab: string;
    onTabClick: () => void;
    onCloseClick: () => void;
}

export const Tab: React.FunctionComponent<Props> = (props: Props) => {
    const { tab, activeTab, onTabClick, onCloseClick } = props;

    return (
        <div.Tab onPress={() => onTabClick()}
            active={activeTab === tab.key}>

            {/** TouchableHighlight accepts only one child */}
            {/** Tab Frame */}
            <div.TabFrame>
                <div.TabText>
                    {tab.label}
                </div.TabText>

                <div.CloseIcon onPress={() => onCloseClick()}>
                    {div.iconSvg(CLOSE, '#000000')}
                </div.CloseIcon>
            </div.TabFrame>
        </div.Tab>
    );
};
