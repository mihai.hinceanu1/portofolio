import { DEFAULT_SVG_PATH } from './_assets/dots-menu.assets';
import { getPathFill } from './dots-menu.utils';
import { TEXT_DIRECTION } from '../../constants/dots-menu.const';
import { pathBoundingBox } from '../../services/raphael.utils';
import { font } from '../../style/font-sizes';
import { BOX_SHADOW_CSS, BOX_SHADOW_NATIVE } from '../../style/shadow.style';
import { FitImage } from '../fit-image/fit-image';
import * as React from 'react';
import { Platform, View } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

const isMobile = Platform.OS !== 'web';

interface DotsMenuItemProps {
    textDirection: TEXT_DIRECTION;
    fill?: string;
    txtColor?: string;
}

export const DotsMenuItem = styled.View<DotsMenuItemProps>`
    display: flex;
    flex-direction: ${props => labelOnLeftOrRight(props)};
    margin-top: 8px;
    margin-bottom: 8px;
`;

export const DotBgr = styled.TouchableOpacity<{ width: number, height: number, bgrColor: string }>`
    width: ${props => props.width + 'px'};
    height: ${props => props.height + 'px'};
    ${getDotShadow()};
    ${isMobile && 'background-color: rgb(255,255,255);'}
    border-radius: ${props => props.width / 2}px;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    background-color: ${props => getBgrColor(props.bgrColor)};
`;

export const Label = styled.Text<DotsMenuItemProps>`
    margin-top: 11px;
    font-size: ${font.$size16}px;
    font-weight: 600;
    color: ${props => getTextColor(props)};
    ${props => marginOnLeftOrRight(props)}
`;

// ====== UTILS ======

function getTextColor(props: DotsMenuItemProps) {
    return props.txtColor ? props.txtColor : '#000000';
}

function getDotShadow() {
    if (Platform.OS !== 'web') {
        return BOX_SHADOW_NATIVE;
    } else {
        return BOX_SHADOW_CSS;
    }
}

var labelOnLeftOrRight = (props: DotsMenuItemProps) => {
    return props.textDirection === TEXT_DIRECTION.left ?
        'row-reverse' :
        'row';
};

var marginOnLeftOrRight = (props: DotsMenuItemProps) =>
    props.textDirection === TEXT_DIRECTION.left ?
        'margin-left: 25px;' :
        'margin-right: 25px;';

var getPath = (svgPath: string) => {
    if (svgPath) {
        return svgPath;
    } else {
        return DEFAULT_SVG_PATH;
    }
};

export const getBgrColor = (bgrColor: string) => {
    return !!bgrColor ? bgrColor : 'white';
};

export const dotIcon = (svgPath: string, fill: string, assetsSrc: string, imagePath: string): JSX.Element => {
    const path = getPath(svgPath);
    let boundingBox = pathBoundingBox(path);
    let { height, width } = boundingBox;

    if (assetsSrc === 'svg') {
        return (
            <Svg data-cy='dot-menu-svg'
                width={width + 1}
                height={height + 2}
                fill='none' style={{ backgroundColor: 'none', justifyContent: 'center', alignItems: 'center' }} >
                <Path data-cy='svg-path'
                    d={path} fill={getPathFill(fill)} />
            </Svg>
        );
    } else {
        return (
            <View data-cy='anchor-image'
                style={{ width: '100%', height: '100%' }}>
                <FitImage imgPath={imagePath} />
            </View>
        );
    }

};