import { ITEM_WIDTH, TRIGGER_WIDTH } from './_constants/dots-menu.const';
import { DotsMenuItem } from './dots-menu-item';
import * as div from './dots-menu.style';
import { dotsStyle, fadeIn, slideOut } from './dots-menu.utils';
import { DotsMenuConfig } from '../../interfaces/dots-menu';
import { detectClickIsOutside, selfCloseEvents$ } from '../../services/self-close-signal.service';
import * as React from 'react';
import { Animated } from 'react-native';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

interface Props {
    config: DotsMenuConfig;
    overrides?: {
        root?: string
    };
}

interface State {
    isExpanded: boolean;
}

/**
 * Renders an anchor which expands an menu.
 * Callbacks can be defined for each option.
 * Has different render directions ( top, bottom, right, left).
 * Custom icons can be defined.
 */
export class DotsMenu extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            isExpanded: false,
        };
    }

    private fadeInValue = new Animated.Value(1);
    private marginValue = new Animated.Value(-30);

    public render() {
        const { config, overrides } = this.props;
        const { menuExpandDirection, textDirection, dots, trigger, bgrColor, txtColor } = config;
        const { isExpanded } = this.state;

        return (
            <div.DotsMenu data-cy='dots-menu'
                menuExpandDirection={menuExpandDirection}
                textDirection={textDirection}
                override={overrides && overrides.root}
                pointerEvents='box-none'>

                {/* Trigger */}
                <DotsMenuItem config={trigger}
                    height={TRIGGER_WIDTH}
                    width={TRIGGER_WIDTH}
                    expandMenu={() => this.toggleMenu()}
                    textDirection={textDirection} />

                {/* Items */}
                {
                    isExpanded &&
                    <Animated.View style={dotsStyle(this.fadeInValue, this.marginValue, menuExpandDirection, textDirection)}>
                        {
                            !!config &&
                            dots.map(dot =>
                                <DotsMenuItem
                                    txtColor={txtColor}
                                    bgrColor={bgrColor}
                                    config={dot}
                                    key={dot._id}
                                    width={ITEM_WIDTH}
                                    height={ITEM_WIDTH}
                                    textDirection={textDirection} />
                            )
                        }
                    </Animated.View>
                }

            </div.DotsMenu>
        );
    }

    public componentDidMount() {
        this.subscribeToSelfClose();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private toggleMenu() {
        if (!this.state.isExpanded) {
            this.displayMenu();
            fadeIn(this.fadeInValue);
            slideOut(this.marginValue);
        } else {
            this.hideMenu();
        }
    }

    private subscribeToSelfClose() {
        selfCloseEvents$.pipe(
            takeUntil(this.destroyed$),
            filter(() => this.state.isExpanded)
        )
            .subscribe(event => {
                let clickOutside = detectClickIsOutside(event, this);

                if (clickOutside) {
                    this.toggleMenu();
                }
            });
    }

    private displayMenu() {
        this.setState({
            isExpanded: true,
        });
    }

    private hideMenu() {
        this.setState({
            isExpanded: false,
        });
    }

}