/// <reference types="cypress" />
// cypress/support/index.js

describe('Dots menu test', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/dots-menu');
    });

    it('Anchor is rendered', () => {
        getElm('top-bottom', 'item').its('length').should('eq', 1);
    });

    it('Svg is rendered', () => {
        getElm('top-bottom', 'dot-menu-svg').its('length').should('exist');
    });

    it('Anchor triggers the menu', () => {
        getElm('top-bottom', 'item').click();
        getElm('top-bottom', 'dot-menu-svg').its('length').should('be.above', 1);
    });

    it('Labels are rendered', () => {
        getElm('top-bottom', 'label').its('length').should('eq', 3);
    });

    it('Anchor calls callback', () => {
        getElm('callback-use-case', 'item').eq(0).click();
        cy.get('[data-cy=callback-response]').then((response) => {
            expect(response.text()).to.eq('Dots Menu is expanded');
        });
    });

    it('Icon color can be changed', () => {
        getElm('text-right-side', 'svg-path').should('have.attr', 'fill', '#0D8EF1');
    });

    // TODO
    xit('Menu expands towards top', () => {});

    // TODO
    xit('Menu expands towards bottom', () => {});

    it('Label in the right side of the dot', () => {
        getElm('top-bottom', 'item').click();
        cy.get('[data-cy=top-bottom] [data-cy=dot-bgr]').eq(1).then($dot => {
            cy.get('[data-cy=label]').eq(0).should($label => {
                expect($label.position().left).be.greaterThan($dot.position().left);
            });
        });
    });

    it('Label in the left side of the dot', () => {
        getElm('text-left-side', 'item').click();
        cy.get('[data-cy=text-left-side] [data-cy=dot-bgr]').eq(1).then($dot => {
            cy.get('[data-cy=label]').eq(0).should($label => {
                expect($label.position().left).be.lessThan($dot.position().left);
            });
        });
    });

    it('Label has the right fill color', () => {
        getElm('text-left-side', 'label').eq(0).should('have.css', 'color', 'rgb(0, 0, 0)');
    });

    it('Custom anchor image exists', () => {
        getElm('custom-anchor-image', 'anchor-image').should('exist');
    });
});

// ====== SELECTORS ======

function getElm(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}