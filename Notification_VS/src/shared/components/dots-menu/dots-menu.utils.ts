import { EXPAND_ANIM_DURATION } from './_constants/dots-menu.const';
import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../../constants/dots-menu.const';
import { Animated } from 'react-native';

export const fadeIn = (value: Animated.Value) => {
    Animated.timing(value, {
        toValue: 0,
        duration: 0,
    }).start(() => {
        Animated.timing(value, {
            toValue: 1,
            duration: EXPAND_ANIM_DURATION,
        }).start();
    });
};

export const slideOut = (value: Animated.Value) => {
    Animated.timing(value, {
        toValue: -40,
        duration: 0,
    }).start(() => {
        Animated.timing(value, {
            toValue: 0,
            duration: EXPAND_ANIM_DURATION,
        }).start();
    });
};

export const dotsStyle = (fadeInValue: Animated.Value, marginValue: Animated.Value, menuExpandDirection: MENU_EXPAND_DIRECTION, textDirection: TEXT_DIRECTION) => {
    let bottom: Animated.Value = new Animated.Value(0.1);
    let top: Animated.Value = new Animated.Value(0.1);

    if (menuExpandDirection === MENU_EXPAND_DIRECTION.top) {
        bottom = marginValue;
    } else {
        top = marginValue;
    }

    return {
        opacity: fadeInValue,
        marginBottom: bottom,
        top: top,
        paddingTop: getPaddingTop(menuExpandDirection),
        paddingBottom: getPaddingBottom(menuExpandDirection),
        marginRight: getMarginRight(textDirection),
        marginLeft: getMarginLeft(textDirection),
        alignItems: getItemsAlignment(textDirection),
    };
};

export const getPaddingTop = (menuExpandDirection: MENU_EXPAND_DIRECTION) => {
    if (menuExpandDirection === MENU_EXPAND_DIRECTION.top) {
        return 30;
    } else {
        return 0;
    }
};

export const getPaddingBottom = (menuExpandDirection: MENU_EXPAND_DIRECTION) => {
    if (menuExpandDirection === MENU_EXPAND_DIRECTION.bottom) {
        return 30;
    } else {
        return 0;
    }
};

export const getMarginRight = (textDirection: TEXT_DIRECTION) => {
    if (textDirection === TEXT_DIRECTION.right) {
        return 12;
    } else {
        return 0;
    }
};

export const getMarginLeft = (textDirection: TEXT_DIRECTION) => {
    if (textDirection === TEXT_DIRECTION.left) {
        return 12;
    } else {
        return 0;
    }
};

export const getItemsAlignment = (textDirection: TEXT_DIRECTION) => {
    if (textDirection === TEXT_DIRECTION.right) {
        return 'flex-end';
    } else {
        return 'flex-start';
    }
};

export const getPathFill = (fill: string) => {
    if (fill) {
        return fill;
    } else {
        return '#0D8EF1';
    }
};