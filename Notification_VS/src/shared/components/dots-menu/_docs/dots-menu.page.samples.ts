export const TOP_BOTTOM = `
// How to use the component
<DotsMenu config={config}/>

const config: DotsMenuConfig = {

    // Change the side where you render the text
    textDirection: TextDirection.right,

    // Change the rendering from top-bottom to bottom-top
    menuExpandDirection: MenuDirection.bottom,

    // Anchor element which triggers the menu expand animation
    trigger: {
        icon: {

            // Select either cust+-om image or custom icon
            assetsSrc: 'svg',

            // Raw data for drawing the path
            svgData: svgData,

            // Color of the icon
            fill: '#0D8EF1',
        },
    },

    // Items of the expanded menu
    dots: IDot[]
};`;

export const BOTTOM_TOP = `
const config: DotsMenuConfig = {

    // Change Rendering orientation to bottom-top
    menuExpandDirection: MenuDirection.top,
}`;

export const LEFT_SIDE_TEXT = `
const config: DotsMenuConfig = {

    // Point in which side you want to render the text
    textDirection: TextDirection.left,
}`;

export const RIGHT_SIDE_TEXT = `
const config: DotsMenuConfig = {

    // Point in which side you want to render the text
    textDirection: TextDirection.right,
}`;

export const CUSTOM_ANCHOR = `
const config: DotsMenuConfig = {
    textDirection: TextDirection.left,
    menuExpandDirection: MenuDirection.bottom,
    trigger: {
        icon: {
            assetsSrc: 'svg',

            // Custom path for desired anchor
            svgData: customPath,
            fill: '#0D8EF1',
        },
    },
}`;

export const CUSTOM_ICONS = `
const config: DotsMenuConfig = {
textDirection: TextDirection.left,
menuExpandDirection: MenuDirection.bottom,
dots: [
    {
    label: 'First Dot',
    onClick: () => { console.log('Callback function'); },
    icon: {
        assetsSrc: 'svg',

        // Add a custom path for each dot
        svgData: 'customPath',
        fill: '#0D8EF1',
    },
},`;

export const CALLBACK = `
const config: DotsMenuConfig = {
    textDirection: TextDirection.left,
    menuExpandDirection: MenuDirection.bottom,
    trigger: {

        // Custom callback for the trigger
        onClick: () => this.onAnchorPress(),
        icon: {
            assetsSrc: 'svg',
            svgData: svgData,
            fill: '#0D8EF1',
        },
    },
}`;

export const CUSTOM_FILL = `
const config: DotsMenuConfig = {
    textDirection: TextDirection.left,
    menuExpandDirection: MenuDirection.bottom,
    trigger: {
        onClick: () => {console.log('Callback was pressed')},
        icon: {
            assetsSrc: 'svg',
            svgData: 'svgData',

            // Choose the filling color of each element
            fill: '#000000',
        },
    }
}`;

export const CUSTOM_ANCHOR_IMAGE = `
const config: DotsMenuConfig = {
    textDirection: TextDirection.left,
    menuExpandDirection: MenuDirection.bottom,
    trigger: {
        _id: '_id.22',
        onClick: () => { console.log('Callback was pressed'); },
        icon: {

            // Render image instead of SVF
            assetsSrc: 'image',

            imagePath: '/thumbnails/business_card_grey.png',
        },
    },
}`;

export const CUSTOM_BACKGROUND_COLOR = `
const config: DotsMenuConfig = {
    bgrColor: grey,
}`;
