import * as svg from '../_assets/dots-menu.assets';
import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../../../constants/dots-menu.const';
import { DotsMenuConfig } from '../../../interfaces/dots-menu';
import { colors } from '../../../style/colors';

export const topBottomConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.left,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.1',
        icon: {
            assetsSrc: 'svg',
            svgData: svg.LAYERS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.2',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.3',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.4',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const bottomTopConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.left,
    menuExpandDirection: MENU_EXPAND_DIRECTION.top,
    trigger: {
        _id: '_id.5',
        icon: {
            assetsSrc: 'svg',
            svgData: svg.LAYERS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.6',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.7',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.8',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.9',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const rightSideTextConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.left,
    menuExpandDirection: MENU_EXPAND_DIRECTION.top,
    trigger: {
        _id: '_id.10',
        icon: {
            assetsSrc: 'svg',
            svgData: svg.LAYERS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.11',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.12',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.13',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const leftSideTextConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.14',
        icon: {
            assetsSrc: 'svg',
            svgData: svg.LAYERS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.15',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.16',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.17',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const customAnchorConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.14',
        icon: {
            assetsSrc: 'svg',
            svgData: svg.EXTRA_DOTS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.15',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.16',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.17',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const customIconsConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.18',
        icon: {
            assetsSrc: 'svg',
            svgData: svg.EXTRA_DOTS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.19',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.BOOK,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.20',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.SINGLE_DOT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.21',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const callbackFunctionConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.18',
        onClick: () => { console.log('Callback was pressed'); },
        icon: {
            assetsSrc: 'svg',
            svgData: svg.EXTRA_DOTS,
            fill: '#0D8EF1',
        },
    },
    dots: [
        {
            _id: '_id.19',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.BOOK,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.20',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.SINGLE_DOT,
                fill: '#0D8EF1',
            },
        },
        {
            _id: '_id.21',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#0D8EF1',
            },
        },
    ],
};

export const customColorConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.22',
        onClick: () => { console.log('Callback was pressed'); },
        icon: {
            assetsSrc: 'svg',
            imagePath: '/thumbnails/business_card_grey.png',
            svgData: svg.EXTRA_DOTS,
            fill: '#000000',
        },
    },
    dots: [
        {
            _id: '_id.23',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.BOOK,
                fill: '#000000',
            },
        },
        {
            _id: '_id.24',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.SINGLE_DOT,
                fill: '#000000',
            },
        },
        {
            _id: '_id.25',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#000000',
            },
        },
    ],
};

export const imageConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    trigger: {
        _id: '_id.22',
        onClick: () => { console.log('Callback was pressed'); },
        icon: {
            assetsSrc: 'image',
            imagePath: '/thumbnails/batman.png',
            svgData: svg.EXTRA_DOTS,
            fill: '#000000',
        },
    },
    dots: [
        {
            _id: '_id.23',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.BOOK,
                fill: '#000000',
            },
        },
        {
            _id: '_id.24',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.SINGLE_DOT,
                fill: '#000000',
            },
        },
        {
            _id: '_id.25',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#000000',
            },
        },
    ],
};

export const backgroundColorConfig: DotsMenuConfig = {
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom,
    bgrColor: colors.$darkGrey,
    trigger: {
        _id: '_id.22',
        onClick: () => { console.log('Callback was pressed'); },
        icon: {
            assetsSrc: 'image',
            imagePath: '/thumbnails/batman.png',
            svgData: svg.EXTRA_DOTS,
            fill: '#000000',
        },
    },
    dots: [
        {
            _id: '_id.23',
            label: 'First Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.BOOK,
                fill: '#000000',
            },
        },
        {
            _id: '_id.24',
            label: 'Second Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.SINGLE_DOT,
                fill: '#000000',
            },
        },
        {
            _id: '_id.25',
            label: 'Third Dot',
            onClick: () => { console.log('Callback function'); },
            icon: {
                assetsSrc: 'svg',
                svgData: svg.EXPERT,
                fill: '#000000',
            },
        },
    ],
};