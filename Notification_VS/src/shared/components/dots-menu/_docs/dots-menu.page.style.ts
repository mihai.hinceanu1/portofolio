import { colors } from '../../../style/colors';
import { font } from '../../../style/font-sizes';
import styled from 'styled-components/native';

export const CallbackResponse = styled.View`
    margin-top: 50px;
`;

export const CallbackText = styled.Text`
    font-size: ${font.$size18}px;
    color: ${colors.$black};
    font-weight: 400;
`;