import * as samples from './dots-menu.page.samples';
import { CallbackResponse, CallbackText } from './dots-menu.page.style';
import * as utils from './dots-menu.page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../code-editor/code-editor';
import { DotsMenu } from '../dots-menu';
import * as React from 'react';
import { View } from 'react-native';

interface DotsMenuState {
    anchor: string;
}

export class DotsMenuPage extends ScrollableDocPage<DotsMenuState> {
    private callBackAnchorConfig = utils.callbackFunctionConfig;

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                anchor: 'is not expanded',
            }
        };
    }

    public renderDemoPage() {
        let { width } = this.state;
        const { anchor } = this.state.overrides;

        this.callBackAnchorConfig.trigger.onClick = () => this.onAnchorPress();

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Dots Menu
                        </div.OverviewTitle>
                    <div.OverviewDescription>
                        Dots menu is a shared component which can be used in various cases to describe a classic menu.
                        The menu can be used in different forms. It can have a bottom to top rendering, a top to bottom render,
                        and it can also display text in both sides (left and right).
                        In order to use it , you need to input a config which contains properties to specify the direction in which you want
                        the menu to open , the text orientation and its components. A sample of proper config is written in code editors on right
                        side. The props that should be used are:
                        </div.OverviewDescription>
                </div.Overview>

                {/** Top-Bottom expand */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Top-bottom expand
                            </div.DemoTitle>

                        <View data-cy='top-bottom'
                            style={{ justifyContent: 'flex-start', height: 350 }}>
                            <DotsMenu config={utils.topBottomConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.TOP_BOTTOM} />
                </div.Demo>

                {/** Bottom-top Expand */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Bottom-top expand
                            </div.DemoTitle>

                        <View data-cy='bottom-top' style={{ justifyContent: 'flex-end', height: 400 }}>
                            <DotsMenu config={utils.bottomTopConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.BOTTOM_TOP} />
                </div.Demo>

                {/** Text in left side */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Text in left side
                            </div.DemoTitle>

                        <View data-cy='text-left-side'
                            style={{ justifyContent: 'flex-start', alignItems: 'flex-end', height: 300 }}>
                            <DotsMenu config={utils.leftSideTextConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.LEFT_SIDE_TEXT} />
                </div.Demo>

                {/** Text in right side */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Text in right side
                            </div.DemoTitle>

                        <View data-cy='text-right-side'
                            style={{ justifyContent: 'flex-end', height: 300 }}>
                            <DotsMenu config={utils.rightSideTextConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.RIGHT_SIDE_TEXT} />
                </div.Demo>

                {/** Custom Anchor */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Anchor
                            </div.DemoTitle>

                        <View data-cy='custom-anchor'
                            style={{ height: 300 }}>
                            <DotsMenu config={utils.customAnchorConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_ANCHOR} />
                </div.Demo>

                {/** Custom icons */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Icons
                            </div.DemoTitle>

                        <View data-cy='custom-icons'
                            style={{ height: 300 }}>
                            <DotsMenu config={utils.customIconsConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_ICONS} />
                </div.Demo>

                {/** Custom Callback function */}
                <div.Demo width={width} data-cy='callback-use-case'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callback function for anchor
                            </div.DemoTitle>

                        <View data-cy='callback'
                            style={{ height: 300 }}>
                            <DotsMenu config={this.callBackAnchorConfig} />
                        </View>
                        {
                            anchor &&
                            <CallbackResponse>
                                <CallbackText data-cy='callback-response'>
                                    Dots Menu {anchor}
                                </CallbackText>
                            </CallbackResponse>
                        }

                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.CALLBACK} />
                </div.Demo>

                {/** Custom fill color */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom fill color
                            </div.DemoTitle>

                        <View data-cy='custom-fill'
                            style={{ justifyContent: 'flex-start', height: 300 }}>
                            <DotsMenu config={utils.customColorConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_FILL} />
                </div.Demo>

                {/* Anchor */}
                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom image anchor
                            </div.DemoTitle>

                        <View data-cy='custom-anchor-image'
                            style={{ justifyContent: 'flex-start', height: 300 }}>
                            <DotsMenu config={utils.imageConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_ANCHOR_IMAGE} />
                </div.Demo>

                <div.Demo width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Background Color
                        </div.DemoTitle>
                        <View data-cy='custom-anchor-image'
                            style={{ justifyContent: 'flex-start', height: 300 }}>
                            <DotsMenu config={utils.backgroundColorConfig} />
                        </View>
                    </div.DemoArea>
                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_BACKGROUND_COLOR} />
                </div.Demo>
            </>
        );
    }

    private onAnchorPress() {
        const { anchor } = this.state.overrides;
        let anchorCopy = '';

        if (anchor === 'is expanded') {
            anchorCopy = 'is not expanded';
        } else {
            anchorCopy = 'is expanded';
        }

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                anchor: anchorCopy,
            },
        });
    }
}