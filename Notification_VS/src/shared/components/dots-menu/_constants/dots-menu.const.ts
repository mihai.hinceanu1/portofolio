/** Trigger dot dimensions */
export const TRIGGER_WIDTH = 65;

/** Dot item dimensions */
export const ITEM_WIDTH = 45;

/** How much time is used to slide out the menu */
export const EXPAND_ANIM_DURATION = 250;
