import * as div from './dots-menu-item.style';
import { TEXT_DIRECTION } from '../../constants/dots-menu.const';
import { IDot } from '../../interfaces/dots-menu';
import * as React from 'react';

interface Props {
    txtColor?: string;
    config: IDot;
    width: number;
    height: number;
    bgrColor?: string;
    textDirection: TEXT_DIRECTION;
    expandMenu?: () => void;
}

export const DotsMenuItem: React.StatelessComponent<Props> = (props: Props) => {
    const { config, textDirection, width, height, bgrColor, txtColor } = props;
    const { fill, svgData, imagePath, assetsSrc } = config.icon;

    return (
        <div.DotsMenuItem data-cy='item' textDirection={textDirection}>

            {/* Label */}
            {
                !!config.label &&
                <div.Label data-cy='label'
                    fill={fill}
                    txtColor={txtColor}
                    textDirection={textDirection}>
                    {config.label}
                </div.Label>
            }

            {/* Dot Background */}
            <div.DotBgr data-cy='dot-bgr'
                width={width}
                height={height}
                bgrColor={bgrColor}
                onPress={() => onMenuItemPress(props)}>
                {div.dotIcon(svgData, fill, assetsSrc, imagePath)}
            </div.DotBgr>

        </div.DotsMenuItem>
    );
};

function onMenuItemPress(props: Props) {

    // User defined
    props.config.onClick &&
        props.config.onClick();

    // Private
    props.expandMenu &&
        props.expandMenu();
}