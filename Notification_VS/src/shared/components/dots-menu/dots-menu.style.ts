import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../../constants/dots-menu.const';
import styled from 'styled-components/native';

interface Override {
    override: {};
}

interface DotsMenuProps {
    textDirection: TEXT_DIRECTION;
    menuExpandDirection: MENU_EXPAND_DIRECTION;
}

export const DotsMenu = styled.View<DotsMenuProps & Override>`
    padding-left: 10px;
    padding-right: 10px;
    width: 100%;
    display: flex;
    flex-direction: ${props => expandTopOrBottom(props)};
    align-items: ${props => labelOnLeftOrRight(props)};
    ${props => props.override}
`;

// ====== UTILS ======

var expandTopOrBottom = (props: DotsMenuProps) =>
    props.menuExpandDirection === MENU_EXPAND_DIRECTION.top ?
        'column-reverse' :
        'column';

var labelOnLeftOrRight = (props: DotsMenuProps) =>
    props.textDirection === TEXT_DIRECTION.left ?
        'flex-start;' :
        'flex-end;';