import * as icn from '../assets/icons';
import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../constants/dots-menu.const';
import { DotsMenuConfig } from '../interfaces/dots-menu';

/**
 * Multiple pages render custom menus in the 4 corners of the app.
 * Most of them will reuse the account menu and layers menu options.
 */

/** User account options. User Settings, Notifications, Logout, etc. */
export const userMenu: DotsMenuConfig = {
    trigger: {
        _id: 'user',
        icon: {
            assetsSrc: 'image',
            imagePath: '/thumbnails/batman.png'
        },
    },
    dots: [
        {
            _id: 'my-profile',
            label: 'My Profile',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.USER,
            },
        },
        {
            _id: 'dashboard',
            label: 'Dashboard',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.DASHBOARD,
            },
        },
        {
            _id: 'goals',
            label: 'Goals',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.GOALS,
            },
        },
        {
            _id: 'settings',
            label: 'Settings',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.SETTINGS,
            },
        },
        {
            _id: 'logout',
            label: 'Logout',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.LAYOUT,
            },
        }
    ],
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom
};

/** Layers options such as: Expert, Reactions, Nano Lessons, etc */
export const layersMenu: DotsMenuConfig = {
    trigger: {
        _id: 'layers',
        icon: {
            assetsSrc: 'svg',
            svgData: icn.LAYERS
        }
    },
    dots: [
        {
            _id: 'extra-help',
            label: 'Extra Help',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.EXTRA_HELP,
            },
        },
        {
            _id: 'expert-highlights',
            label: 'Expert Highlights',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.EXPERT_HIGHLIGHT,
            },
        },
        {
            _id: 'reminders',
            label: 'Reminders',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.REMINDER,
            },
        }
    ],
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.top
};