import { Platform } from 'react-native';

const isMobile = Platform.OS !== 'web';
// APP HEADER - TOP MENU
export const APP_HEADER_HEIGHT = 50; // in px

export const ADMIN_NAV_CLOSED = 40; // in px

export const ADMIN_NAV_OPENED = 170; // in px

export const ENIAC_DIMENSIONS = isMobile ? 150 : 200;
