/** Text direction */
export enum TEXT_DIRECTION {
    'left',
    'right',
}

/** Expanding direction */
export enum MENU_EXPAND_DIRECTION {
    'top',
    'bottom',
}