/** The directions in which the menu can expand */
export enum STUB_DIRECTIONS {
    'top',
    'bottom',
}

/** The sides of the branches menu where content can appear */
export enum BRANCHES_DIRECTIONS {
    'left',
    'right',
}