/** Bellow this threshold we consider our interfaces as being mobile */
export const MOBILE_WIDTH = 600;

/** Bellow this threshold we consider our interfaces as being mobile */
export const WIDTH_XS = 512;

/** Bellow this threshold we consider our interfaces as being... phablets?!? */
export const WIDTH_S = 768;

/** Bellow this threshold we consider our interfaces as being wide enough for laptop */
export const WIDTH_M = 992;

/** Max width of a standard page */
export const PAGE_WIDTH = 1100;

/** Max width for packages versions */
export const PACKAGES_VERSIONS_PAGE_WIDTH = 1200;