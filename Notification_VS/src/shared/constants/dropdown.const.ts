export enum DROPDOWN_TYPE {

    /** Only the input field is visible */
    'OnlyInput',

    /** Only the icon is visible. Useful when used in toolbars */
    'OnlyIcon',

    /** Standard dropdown, icon and field visible */
    'Standard',
}

export enum DROPDOWN_OPTION_SIDE {
    LEFT = 'left',
    RIGHT = 'right',
}