import { SharedState } from '../interfaces/shared.state';

export const sharedInitialState: SharedState = {
    isPageLoading: false,
    dbCheck: null,
    isStoreLoading: true,
    toastrs: null,
};
