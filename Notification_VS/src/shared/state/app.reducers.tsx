import { sharedReducer } from './shared.reducer';
import { authReducer } from '../../auth/state/auth.reducer';
import { commentsReducer } from '../../comments/state/comments.reducer';
import { feedbackReducer } from '../../feedback/state/feedback.reducers';
import { iconsReducer } from '../../icons/state/icons.reducer';
import { imagesReducer } from '../../images/state/images.reducer';
import { lessonEditorReducer } from '../../lesson-editor/state/lesson-editor.reducer';
import { lessonNavigatorReducer } from '../../lesson-navigator/state/lesson-navigator.reducer';
import { lessonsCatalogReducer } from '../../lessons-catalog/state/lessons-catalog.reducer';
import { lessonsReducer } from '../../lessons/state/lessons.reducer';
import { mapReducer } from '../../maps/state/maps.reducer';
import { nanolessonsReducers } from '../../nano-lessons/state/reducers/reducer';
import { notificationsReducer } from '../../notifications/state/notifications.reducer';
import { searchReducer } from '../../search/state/search.reducer';
import { sourceCodeReducer } from '../../source-code/state/reducers/source-code.reducer';
import { taxonomyReducer } from '../../taxonomy/state/taxonomy.reducer';
import { topicsReducer } from '../../topics/state/topics.reducer';
import { usersReducer } from '../../users/state/users.reducer';
import { AppState } from '../interfaces/app.state';
import { combineReducers, Reducer } from 'redux';

/** Use alphabetic sorting */
export const appReducers: Reducer<AppState> = combineReducers({
    shared: sharedReducer,
    auth: authReducer,
    comments: commentsReducer,
    feedback: feedbackReducer,
    icons: iconsReducer,
    images: imagesReducer,
    lessons: lessonsReducer,
    lessonEditor: lessonEditorReducer,
    lessonsCatalog: lessonsCatalogReducer,
    lessonNavigator: lessonNavigatorReducer,
    map: mapReducer,
    nanolessons: nanolessonsReducers(),
    notifications: notificationsReducer,
    search: searchReducer,
    sourceCode: sourceCodeReducer(),
    taxonomy: taxonomyReducer,
    topics: topicsReducer,
    users: usersReducer,
});
