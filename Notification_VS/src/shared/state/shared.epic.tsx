import * as sharedActions from './shared.actions';
import { AppState } from '../interfaces/app.state';
import { Action, DbCheck } from '../interfaces/shared';
import * as sharedWebapi from '../webapis/shared.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getDbConnection$ = (action$: ActionsObservable<Action<DbCheck>>, _store: Store<AppState>) =>
    action$.ofType(sharedActions.GET_DB_CONNECTION_REQ).pipe(
        map(action => action.payload),
        concatMap(() => sharedWebapi.getDbConnection().pipe(
            map(dbConnection => sharedActions.getDbConnectionOk(dbConnection)),
            catchError(error => of(sharedActions.getDbConnectionFail(error))),
        )),
    );
