import { AppState } from '../interfaces/app.state';
import { Action, DbCheck } from '../interfaces/shared';
import { IToastr } from '../interfaces/toastr';

// ====== TOGGLE IS PAGE LOADING ======

export const TOGGLE_IS_PAGE_LOADING = 'TOGGLE_IS_PAGE_LOADING';
export const togglePageIsLoading = (isLoading: boolean): Action<boolean> => ({
    type: TOGGLE_IS_PAGE_LOADING,
    payload: isLoading,
});

// ====== GET GET_DB_CONNECTION SETTINGS ======

export const GET_DB_CONNECTION_REQ = 'GET_DB_CONNECTION_REQ';
export const getDbConnectionReq = (): Action<null> => ({
    type: GET_DB_CONNECTION_REQ,
    payload: null,
});

export const GET_DB_CONNECTION_OK = 'GET_DB_CONNECTION_OK';
export const getDbConnectionOk = (dbConnection: DbCheck): Action<DbCheck> => ({
    type: GET_DB_CONNECTION_OK,
    payload: dbConnection,
});

export const GET_DB_CONNECTION_FAIL = 'GET_DB_CONNECTION_FAIL';
export const getDbConnectionFail = (error: Error): Action<Error> => ({
    type: GET_DB_CONNECTION_FAIL,
    payload: error,
});

// ===== STORE =====

export const SET_STORE_LOADING = 'SET_STORE_LOADING';
export const setStoreLoading = (flag: boolean): Action<boolean> => ({
    type: SET_STORE_LOADING,
    payload: flag,
});

export const SET_STORE = 'SET_STORE';
export const setStore = (store: AppState): Action<AppState> => ({
    type: SET_STORE,
    payload: store,
});

// ===== ACTION CONFIRMATION =====

export const CREATE_TOASTR = 'CREATE_TOASTR';
export const createToastr = (action: IToastr): Action<IToastr> => ({
    type: CREATE_TOASTR,
    payload: action,
});

export const REMOVE_TOASTR = 'REMOVE_TOASTR';
export const removeToastr = (id: string): Action<string> => ({
    type: REMOVE_TOASTR,
    payload: id,
});
