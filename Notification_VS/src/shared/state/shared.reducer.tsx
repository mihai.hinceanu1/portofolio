import * as actions from './shared.actions';
import { sharedInitialState } from './shared.init-state';
import { SharedState } from '../interfaces/shared.state';
import { removeToastr } from '../services/shared.service';

/**
 * We need unique confirmations Ids.
 * Increment each time an action confirmation occurres
 */
let toastrsCount = 0;

export const sharedReducer = (state: SharedState = sharedInitialState, action: any): SharedState => {

    switch (action.type) {

        case actions.TOGGLE_IS_PAGE_LOADING: {
            return {
                ...state,
                isPageLoading: action.payload as boolean,
            };
        }

        case actions.GET_DB_CONNECTION_OK: {
            return {
                ...state,
                dbCheck: action.payload,
            };
        }

        case actions.SET_STORE_LOADING: {
            return {
                ...state,
                isStoreLoading: action.payload as boolean,
            };
        }

        case actions.CREATE_TOASTR: {
            // add id to new action
            const id = `toastr-${toastrsCount++}`;
            const newAction = { ...action.payload, id };
            let toastrs = state.toastrs;

            if (toastrs && toastrs.length) {
                toastrs.push(newAction);
            } else {
                toastrs = [].concat(newAction);
            }

            setTimeout(() => {
                removeToastr(id);
            }, 3000);

            return {
                ...state,
                toastrs,
            };
        }

        case actions.REMOVE_TOASTR: {
            const toastrs = state.toastrs
                .filter(toastr => toastr.id !== action.payload);

            return {
                ...state,
                toastrs,
            };
        }

        default:
            return state;
    }

};
