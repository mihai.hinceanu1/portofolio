import * as sharedEpics from './shared.epic';
import * as authEpics from '../../auth/state/auth.epic';
import * as commentsEpics from '../../comments/state/comments.epic';
import * as feedbackEpics from '../../feedback/state/feedback.epic';
import * as iconsEpics from '../../icons/state/icons.epic';
import * as galleryEpics from '../../images/state/images.epic';
import * as lessonEditorEpics from '../../lesson-editor/state/lesson-editor.epic';
import * as lessonsEpics from '../../lessons/state/lessons.epic';
import * as mapsEpics from '../../maps/state/maps.epic';
import * as nanolessonsLessonEpics from '../../nano-lessons/state/epics/nanolessons-lesson.epics';
import * as nanoLessonsEpics from '../../nano-lessons/state/epics/nanolessons.epic';
import * as notificationsEpics from '../../notifications/state/notifications.epics';
import * as searchEpics from '../../search/state/search.epic';
import * as dependenciesEpics from '../../source-code/state/epics/dependencies.epic';
import * as packageVersionsEpics from '../../source-code/state/epics/package-versions.epic';
import * as packageEpics from '../../source-code/state/epics/packages.epic';
import * as sourceFilesEpics from '../../source-code/state/epics/source-file.epic';
import * as taxonomyEpics from '../../taxonomy/state/taxonomy.epic';
import * as topicsEpics from '../../topics/state/topics.epic';
import * as usersEpics from '../../users/state/users.epic';
import { AppState } from '../interfaces/app.state';
import { Action } from '../interfaces/shared';
import { combineEpics, Epic } from 'redux-observable';

export const appEpics: Epic<Action<AppState>, any> = combineEpics(
    ...getEpicsArr(sharedEpics),
    ...getEpicsArr(authEpics),
    ...getEpicsArr(commentsEpics),
    ...getEpicsArr(feedbackEpics),
    ...getEpicsArr(galleryEpics),
    ...getEpicsArr(iconsEpics),
    ...getEpicsArr(lessonsEpics),
    ...getEpicsArr(lessonEditorEpics),
    ...getEpicsArr(mapsEpics),
    ...getEpicsArr(nanoLessonsEpics),
    ...getEpicsArr(nanolessonsLessonEpics),
    ...getEpicsArr(notificationsEpics),
    ...getEpicsArr(searchEpics),
    ...getEpicsArr(packageVersionsEpics),
    ...getEpicsArr(packageEpics),
    ...getEpicsArr(taxonomyEpics),
    ...getEpicsArr(topicsEpics),
    ...getEpicsArr(usersEpics),
    ...getEpicsArr(sourceFilesEpics),
    ...getEpicsArr(dependenciesEpics),
);

/** Converting from import object to array. */
function getEpicsArr(epics: any): any[] {
    let epicsArr: any[] = [];
    let epicsNames = Object.keys(epics);

    // Array is needed for combineEpics.
    epicsNames.forEach(epicName =>
        epicsArr.push(epics[epicName]),
    );

    return epicsArr;
}