import { sharedInitialState } from './shared.init-state';
import { authInitialState } from '../../auth/state/auth.init-state';
import { commentsInitialState } from '../../comments/state/comments.init-state';
import { feedbackInitialState } from '../../feedback/state/feedback.init-state';
import { iconsInitState } from '../../icons/state/icons.init-state';
import { imagesInitialState } from '../../images/state/images.init-state';
import { lessonEditorInitialState } from '../../lesson-editor/state/lesson-editor.init-state';
import { lessonNavigatorInitialState } from '../../lesson-navigator/state/lesson-navigator.init-state';
import { lessonsCatalogInitialState } from '../../lessons-catalog/state/lessons-catalog.init-state';
import { lessonsInitialState } from '../../lessons/state/lessons.init-state';
import { mapInitialState } from '../../maps/state/maps.init-state';
import { nanolessonsInitState } from '../../nano-lessons/state/init-state/nanolessons.init-state';
import { notificationsInitialState } from '../../notifications/state/notifications.init-state';
import { searchInitialState } from '../../search/state/search.init-state';
import { sourceCodeInitialState } from '../../source-code/state/init-state/source-code.init-state';
import { taxonomyInitialState } from '../../taxonomy/state/taxonomy.init-state';
import { topicsInitialState } from '../../topics/state/topics.init-state';
import { usersInitialState } from '../../users/state/users.init-state';
import { AppState } from '../interfaces/app.state';

export const appInitialState: AppState = {
    shared: sharedInitialState,
    auth: authInitialState,
    comments: commentsInitialState,
    feedback: feedbackInitialState,
    icons: iconsInitState,
    images: imagesInitialState,
    lessons: lessonsInitialState,
    lessonEditor: lessonEditorInitialState,
    lessonsCatalog: lessonsCatalogInitialState,
    lessonNavigator: lessonNavigatorInitialState,
    map: mapInitialState,
    nanolessons: nanolessonsInitState,
    notifications: notificationsInitialState,
    search: searchInitialState,
    sourceCode: sourceCodeInitialState,
    taxonomy: taxonomyInitialState,
    topics: topicsInitialState,
    users: usersInitialState,
};
