import { AppState } from '../interfaces/app.state';
import { DbCheck } from '../interfaces/shared';
import { SharedState } from '../interfaces/shared.state';
import { IToastr } from '../interfaces/toastr';
import { createSelector, Selector } from 'reselect';

const SHARED_MODULE: Selector<AppState, SharedState> = (state: AppState) => state.shared;

export const IS_PAGE_LOADING = createSelector<AppState, SharedState, boolean>(
    SHARED_MODULE,
    (state: SharedState) => state.isPageLoading,
);

/** Database connection check */
export const DB_CONNECTION = createSelector<AppState, SharedState, DbCheck>(
    SHARED_MODULE,
    (state: SharedState) => state.dbCheck,
);

export const IS_STORE_LOADING = createSelector<AppState, SharedState, boolean>(
    SHARED_MODULE,
    (state: SharedState) => state.isStoreLoading,
);

export const TOASTRS = createSelector<AppState, SharedState, IToastr[]>(
    SHARED_MODULE,
    (state: SharedState) => state.toastrs,
);
