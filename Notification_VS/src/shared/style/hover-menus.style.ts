import { MOBILE_WIDTH } from '../constants/adaptive.const';
import styled from 'styled-components/native';

/**
 * Multiple pages render custom menus in the 4 corners of the app.
 * These utils help us pin down the menus using the same positioning logic.
 */

interface Position {
    top?: number;
    left?: number;
    bottom?: number;
    right?: number;
}

interface LogoProps {
    width: number;
}

export const Menus = styled.View`
    position: absolute;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    z-index: 10;
`;

export const AppLogo = styled.View<LogoProps>`
    position: absolute;
    top: ${props => getLogoTopPos(props.width)}px;
    left: ${props => getLogoLeftPos(props.width)}px;
    z-index: 10;
`;

export const AdminNavigator = styled.View`
    position: absolute;
    top: 120px;
    left: 43px;
    flex-direction: row;
    align-items: center;
`;

export const overrideTopLeftMenu = (width: number) => {
    let { top, left } = getTopLeftMenuPadding(width);

    return {
        root: `
            position: absolute;
            top: ${top}px;
            left: ${left}px;
        `
    };
};

export const overrideBottomLeftMenu = (width: number) => {
    let { left, bottom } = getBottomLeftMenuPadding(width);

    return {
        root: `
            position: absolute;
            left: ${left}px;
            bottom: ${bottom}px;
        `
    };
};

export const overrideTopRightMenu = (width: number) => {
    let { right, top } = getTopRightMenuPadding(width);

    return {
        root: `
            position: absolute;
            right: ${right}px;
            top: ${top}px;
        `
    };
};

export const overrideBottomRightMenu = (width: number) => {
    let { right, bottom } = getBottomRightMenuPadding(width);

    return {
        root: `
            position: absolute;
            right: ${right}px;
            bottom: ${bottom}px;
        `
    };
};

// ====== UTILS ======

const getTopLeftMenuPadding = (width: number): Position => {
    return width > MOBILE_WIDTH ?
        { top: 20, left: 36, } :
        { top: 0, left: 16, };
};

const getBottomLeftMenuPadding = (width: number): Position => {
    return width > MOBILE_WIDTH ?
        { bottom: 20, left: 36, } :
        { bottom: 0, left: 16, };
};

const getTopRightMenuPadding = (width: number): Position => {
    return width > MOBILE_WIDTH ?
        { top: 28, right: 26, } :
        { top: 8, right: 6, };
};

const getBottomRightMenuPadding = (width: number): Position => {
    return width > MOBILE_WIDTH ?
        { bottom: 28, right: 26, } :
        { bottom: 8, right: 6, };
};

const getLogoTopPos = (width: number): number => {
    return width > MOBILE_WIDTH ? 53 : 35 ;
};

const getLogoLeftPos = (width: number): number => {
    return width > MOBILE_WIDTH ? 120 : 100 ;
};