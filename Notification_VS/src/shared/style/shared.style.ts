import { colors } from './colors';
import { NanolessonDifficulty } from '../../nano-lessons/interfaces/nano-lessons';

export function getColorByDifficulty(difficulty: NanolessonDifficulty) {
    if (difficulty === NanolessonDifficulty.Beginner) {
        return colors.$nanolessonJunior;
    }

    if (difficulty === NanolessonDifficulty.Intermediate) {
        return colors.$nanolessonIntermediate;
    }

    if (difficulty === NanolessonDifficulty.Expert) {
        return colors.$nanolessonExpert;
    }
}