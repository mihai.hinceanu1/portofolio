import EStyleSheet from 'react-native-extended-stylesheet';

/** Reusable styles */
export const CommonStyles = EStyleSheet.create({
    header: {
        fontSize: 26,
        fontWeight: '300',
        textAlign: 'center',
    },
    subheader: {
        fontSize: 16,
        fontWeight: '300',
        textAlign: 'center',
    },
    subheaderSmall: {
        fontSize: 12,
        fontWeight: '300',
        color: '$subheaderSmallColor',
        marginTop: 5,
        marginBottom: 5,
    },
    subheaderSmallCentered: {
        fontSize: 12,
        fontWeight: '300',
        color: '$subheaderSmallColor',
        marginBottom: 10,
        textAlign: 'center',
    },
});

/** Common padding to all pages */
export const pagePaddings = {
    paddingTop: 0,
    paddingLeft: 60,
    paddingRight: 60,
};