/**
 * Predefined thresholds for font sizes.
 * For a consistent look and feel in the app use only these values.
 */
export const font = {
    $size6: 6,
    $size8: 8,
    $size10: 10,
    $size12: 12,
    $size14: 14,
    $size16: 16,
    $size18: 18,
    $size20: 20,
    $size22: 22,
    $size40: 40,
};