// REVIEW all colors
export const colors = {
    // ====== NEW COLOR PALLET R1.0.0 ======
    $lightGrey: 'rgb(223,226,231)',
    $grey: 'rgb(196,196,196)',
    $darkGrey: 'rgb(127,127,127)',
    $blue: 'rgb(13,142,241)',
    $lightBlue: 'rgba(13,142,241,0.1)',
    $darkBlue: '#001e40',
    $lightGreen: 'rgba(0, 195, 167, 0.8)',
    $green: 'rgb(0,195,167)',
    $white: '#ffffff',
    $black: '#000000',
    $whiteFaded: 'rgba(255,255,255, 0.1)',
    $red: 'rgb(234, 67, 53)',
    $yellow: 'rgb(255, 213, 72)',

    $inactive: '#F4F8F7',

    // OVERLAYS
    $overlayBtnIcons: '#356398',

    // ====== APP ======

    $appHeaderBgr: '#f0f1f4',
    $appBgr: 'rgb(244, 243, 241)',

    // ====== PAGES ======

    $listOfUsers: 'lightblue',
    $registerPageBgr: 'rgba(1, 20, 39, 0.9)',
    $registerPageSecondBgr: '#ecf0f1',

    // ====== TEXTS ======

    $subheaderSmallColor: '#7f8c8d',
    $warningMessage: '#f39c12',
    $topicExtendedText: 'rgb(150, 150, 150)',

    // ====== Colors For Modals On Map ======
    $genericTextColor: 'white',
    $invisible: 'transparent',
    $descriptionText: 'rgb(220,220,220)',
    $iconsColor: `lightblue`,

    // ====== INPUTS ======
    $inputBox: '#F4F8F7',
    $inputBorderColor: '#c4c4c4',
    $inputBorderColorDisabled: '#ecf0f1',
    $labelColor: '#7f8c8d',
    $titleBgrHovered: '#ecf0f1',

    // ====== ACTIONS TOP BAR ======
    // $barFolderBgr: '#ecf0f1',

    $topicPackages: 'rgb(200,200,200)',

    $barFolderBgr: '#34495e',

    // ====== SIDE MENU TREE ======

    $sideTreeBgr: '#dfe6ed',

    // ====== BUTTONS ======

    $buttonBgr: '#2980b9',
    $buttonBgrHover: '#09a1f7',
    $buttonBgrInactive: '#7f8c8d',

    $secondaryButtonBgr: '#16a085',
    $secondaryButtonHover: '#1abc9c',
    // $secondaryButtonBgrInactive: '#7f8c8d',

    $buttonColor: '#ffffff',
    $textBlack: '#000000',
    $buttonListColorHover: 'rgb(224,224,224)',
    // $buttonBgrHover: '#0078bb',
    // $buttonBgrOnDarkHover: '#ffffff',
    // $buttonColorOnDarkHover: '#0078bb',
    // $btnTagBgr: '#213141',
    // $btnTagText: '#a7cff3',
    // $btnTagBgrHover: '#0a86cd',
    // $btnTagTextHover: '#ffffff',
    $menuBtnTextColor: '#1E90FF',
    $menuBtnTextError: '#c0392b',

    // ====== Modal ======
    $containerBackground: '#ffffff',
    $textTooltipsBlack: 'rgba(30, 30, 30, 0.6)',

    // ====== NANOLESSONS ======
    $nanolessonJunior: '#1E90F1',
    $highlightJunior: 'rgba(30, 144, 241, 0.1)',
    $nanolessonIntermediate: '#001E40',
    $highlightIntermediate: 'rgba(0, 30, 64, 0.1)',
    $nanolessonExpert: '#01C3A8',
    $highlightExpert: 'rgba(1, 195, 168, 0.75)',
    $nanolessonDefault: '#DFE2E7',

    $nanolessonInactiveTab: '#F4F3F1',

    // ====== NOTIFICATIONS ======

    $notificationBgrNormal: '#1E90FF',
    $notificationBgrError: '#e74c3c',
    $notificationBgrSuccess: '#27ae60',
    $notificationBgrWarning: '#e67e22',
    $notificationText: '#ffffff',

    // ====== OVERLAY POPUPS ON HOVER ======

    $popupBgr: '#7f8c8d',
    $popupText: '#ffffff',

    // ====== TOPIC NAVIGATOR ======
    $navigatorLine: '#1E90FF',
    $navSelectedItemBgr: '#f0f1f4',
    $navGreyText: '#828282',
    $navWhiteText: '#ffffff',
    // $navBgr: '#122438',
    // $navImageBgr: '#192a3a',
    // $navImageRim: 'rgba(255, 255, 255, 0.8)',
    // $navImageRimBig: 'rgba(255,255,255,0.6)',
    // // The big thumbnail has more pixel volume due to antialising rules. To compnesate it needs more transparency
    // $navImageRimHover: '#fff',
    // $navShadow: 'rgba(1, 20, 39, 0.5)',
    // $navText: '#ffffff',
    // $navLink: '#ffffff',
    // $navSubtext: '#ffffff',
    // $navStatsText: '#a7cff3',
    // $navScrollbarThumb: 'rgba(167, 207, 243, 0.3)',

    // ====== QUICK MENU  ======

    // $qmenuBgr: '#09a1f7',
    // $qmenuBgrHover: '#0078bb',
    // $qmenuText: '#ffffff',

    // ====== LINKS ======

    $link: '#09a1f7',
    $linkOnDarkBgr: '#a8cef3',
    $linkOnDarkBgrHover: '#09a1f7',

    // ====== ICONS ======

    // $icon: '#a8cef3',
    // $iconHover: '#ffffff',

    // ====== PROP TYPE NAME ======

    $propTypeStringName: 'rgba(46, 204, 113,1.0)',
    $propTypeNumberName: 'rgba(41, 128, 185,1.0)',
    $propTypeFunctionName: 'rgba(243, 156, 18,1.0)',
    $propTypeBooleanName: 'rgba(22, 160, 133,1.0)',
    $propTypeAnyName: 'rgba(231, 76, 60,1.0)',

    // ====== PROP TYPE BORDER ======

    $propTypeStringBorder: 'rgba(46, 204, 113,1.0)',
    $propTypeNumberBorder: 'rgba(41, 128, 185,1.0)',
    $propTypeFunctionBorder: 'rgba(243, 156, 18,1.0)',
    $propTypeBooleanBorder: 'rgba(22, 160, 133,1.0)',
    $propTypeAnyBorder: 'rgba(231, 76, 60,1.0)',

    // ====== PROP TYPE BACKGROUND ======

    $propTypeStringBackground: 'rgba(46, 204, 113,0.1)',
    $propTypeNumberBackground: 'rgba(41, 128, 185,0.1)',
    $propTypeFunctionBackground: 'rgba(243, 156, 18,0.1)',
    $propTypeBooleanBackground: 'rgba(22, 160, 133,0.1)',
    $propTypeAnyBackground: 'rgba(231, 76, 60,0.1)',

    // ====== SCROLLBARS ======

    // $scrollbarThumb: '#a7cff3',
    // $scrollbarThumbHover: '#a7cff3',

    // ====== CSS LOADER ======

    // $cssLoaderBgr: '#ffffff',
    // $cssLoaderIcon: '#0078bb',

    // ====== OVERLAYS ======

    $overlayBgr: 'rgba(1, 20, 39, 0.9)',
    $overlayBgrSolid: 'rgba(1, 20, 39)',
    // $overlayBgrVivid: '#09a1f7',
    $overlayText: '#ffffff',
    // $overlayIcon: '#ffffff',
    // $overlayIconFaded: '#a8cef3',
    // $overlayIconHover: '#122438',
    // $overlayIconBgrHover: '#ffffff',

    // ====== LESSON ======

    // $lessonBgr: '#f5f5f5',
    // $activeTopicBgr: '#ffffff',

    // ====== LESSON NAVIGATOR ======

    // $lessonNavBgr: '#09a1f7',
    // $lessonNavBgrHover: '#0078bb',
    // $lessonNavText: '#ffffff',

    // ====== TOPIC HELPERS ======

    $tooltipPanelBgr: '#122438',
    $tooltipPanelColor: '#ffffff',
    // $tooltipBgr: '#ececec',
    $tooltipText: '#rgb(255,255,255)',
    $highlightBgr: '#e1f0ff',

    // ====== TOOLTIPS ======

    $tooltipDefault: 'rgba(52, 73, 94, 0.1)',
    // $tooltipInfo: 'rgba(26, 188, 156, 1)',
    // $tooltipWarning: 'rgba(243, 156, 18, 1)',
    // $tooltipError: 'rgba(231, 76, 60, 1)',

    // ====== HIGHLIGHTS ======

    $highlightDefault: 'rgba(52, 73, 94, 0.5)',
    $highlightInfo: 'rgba(26, 188, 156, 0.5)',
    $highlightWarning: 'rgba(243, 156, 18, 0.5)',
    $highlightError: 'rgba(231, 76, 60, 0.5)',
    $highlightTooltip: 'rgba(149, 165, 166, 0.5)',

    // ====== SEARCH OVERLAY ======

    // $highlightTextColor: '#fff',
    // $highlightTextBgr: 'rgba(255, 153, 0, 0.31)',
    // $searchInputBorder: '#f1f1f1',
    // $searchFormBackground: 'rgba(51, 51, 51, 0)',

    // ====== MISSION CONTROL OVERLAY ======

    // $progressBarColor: '#09a1f7',
    // $activeTabColor: '#fff',
    // $borderTabsColor: 'rgb(245, 245, 245)',
    // $borderTabsOpacity: '0.2',

    // ====== TABS ======
    $selectedTabColor: '#2c3e50',

    // ====== CODE TABS ======
    $activeCodeTab: '#272822',
    $inactiveCodeTab: '#6d6e69',
    $inactiveTabTextColor: 'lightgray',
    // ====== MENU LEFT-SIDE MAP GLOBAL / LOCAL ======

    $disabledDomainText: '#95a5a6',
    $enabledDomainText: '#000000',
    $projectTitleContainer: 'rgb(230,230,230)',

    // ====== MAP GLOBAL / LOCAL ENTITIES ======

    $mapMainBranch: '#1d90f1',
    $projectTagBox: '#f1f1f1',

    $topicMarkerEasy: '#1d90f1',
    $topicMarkerGoal: '#808080',
    $topicMarkerMedium: '#b9b9b9',
    $topicMarkerHard: '#808080',
    $topicMarkerMaster: '#01C3A8',

    $rootTopicName: '#1d90f1',

    $mapMainSideMenu: '#f5f5f5',

    $projectTagBoxBgr: '#e4e4e4',

    // ====== PACKAGE ADMIN ======
    $packageToggledBorder: '#ffa000',

    // ====== REGISTER ======
    $usernameAvailableGreen: 'rgba(0, 128, 0, 0.8)',
    $usernameUnavailableRed: 'rgba(128, 0, 0, 0.8)',

    // ====== COMMENTS ======
    $commentsDivider: 'rgb(240,240,240)',
    $commentAvatarBorder: 'rgb(30, 144, 255)',
    $commentAvatarText: 'rgb(30, 144, 255)',
    $commentDate: '#95a5a6',

    // ====== TOPICS ======
    $topicTitle: 'rgb(30, 144, 255)',
    $topicSubtitle: '#bdc3c7',
    $topicTextExtendedContent: '#bdc3c7',

    // ====== SENTIMENTS ======
    $thumbsUpIconColor: '#3498db',
    $thumbsDownIconColor: '#2c3e50',
    $favedIconColor: '#e74c3c',
    $partyIconColor: '#e67e22',
    $laughIconColor: '#f1c40f',
    $confusedIconColor: '#27ae60',

    // ====== TAXONOMY ======
    $labelTextColor: '#95a5a6', // grey concrete
};

/** Needed for both formats - some cases accept only rgb (cypress is an example) */
export const color = {
    $green: {
        rgb: 'rgb(0, 195, 167)',
        hex: '#00C3A7',
    },
};