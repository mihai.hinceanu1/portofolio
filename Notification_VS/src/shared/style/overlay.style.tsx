import { StyleSheet } from 'react-native';

/**
 * Gradients are defined as full screen boxes.
 * Inside the gradient tag we place the overlaid content.
 */
export const style = StyleSheet.create({
    linearGradient: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        zIndex: 9, // Bellow the Hover menus
    },
});

const TOP_OVERLAY_GRADIENT = 'rgba(39, 102, 166, 0.9)';
const BOTTOM_OVERLAY_GRADIENT = 'rgba(29, 58, 104, 0.95)';

/** Renders a nice gradient for overlays */
export const OVERLAY_GRADIENT = [TOP_OVERLAY_GRADIENT, BOTTOM_OVERLAY_GRADIENT];