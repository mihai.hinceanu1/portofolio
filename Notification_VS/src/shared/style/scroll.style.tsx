import { getPagePaddings } from './adaptive.style';
import { PAGE_WIDTH } from '../constants/adaptive.const';
import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

interface CenterProps {
    width: number;
    override?: string;
}

/**
 * Styles used to render a scroll bar with centered content on screen.
 * This pattern seems to be reused in multiple places.
 * We decided to make
 */

export const ScrollView = styled.ScrollView`
    width: 100%;
    height: 100%;
`;

export const inner = StyleSheet.create({
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }
});

// TODO Add override for custom WIDTH
// !! Changed Page Width - To Be Replaced After a Discussion
export const Center = styled.View<CenterProps>`
    height: 100%;
    width: 100%;
    align-self: center;
    padding: 0 ${props => getPagePaddings(props)}px;
    max-width: ${PAGE_WIDTH}px;
    ${props => props.override};
`;
    /* width: ${props => props.width}px; */
