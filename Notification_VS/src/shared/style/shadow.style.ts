import { Platform } from 'react-native';

/** Native shadow - use with style={BOX_SHADOW_NAV} */
/** Native shadows - https://ethercreative.github.io/react-native-shadow-generator/ */
export const BOX_SHADOW_NATIVE = {
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
};

/** Web shadow - for styled components - used in styled components */
export const BOX_SHADOW_CSS = 'box-shadow: 0px 0px 16px rgba(0,0,0,0.1)';

// ====== UTILS ======

export const getShadow = (webShadow?: string) => {
    return Platform.OS === 'web' ? webShadow || BOX_SHADOW_CSS : BOX_SHADOW_NATIVE;
};
