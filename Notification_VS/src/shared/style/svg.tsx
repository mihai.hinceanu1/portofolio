import { colors } from './colors';
import { pathBoundingBox } from '../services/raphael.utils';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export function iconSvg(svgPath: string, fill?: string) {
    const { width, height } = pathBoundingBox(svgPath);

    return (
        <Svg width={'100%'} height={'100%'} viewBox={`0 0 ${width} ${height}`}>
            <Path d={svgPath} fill={fill || colors.$grey} />
        </Svg>
    );
}
