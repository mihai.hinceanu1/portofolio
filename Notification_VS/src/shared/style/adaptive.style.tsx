import { MOBILE_WIDTH } from '../constants/adaptive.const';

interface PagePaddingProps {
    width: number;
}

/** All pages use the same paddings. */
export const getPagePaddings = (props: PagePaddingProps): number =>
    props.width >= MOBILE_WIDTH ? 10 : 5;

/** All boxes and containers use the same paddings. */
export const getBoxPaddings = (props: PagePaddingProps): number =>
    props.width >= MOBILE_WIDTH ? 30 : 10;