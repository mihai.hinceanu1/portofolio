export interface ValidationResponse {
    isValid: boolean;
    message: string;
}

/**
 * Validator function that is called on each input - where needed
 * It takes 3 params
 * @param type - can be from password to simple text
 * @param element - string to be validated
 * @param name - name to be shown in the UI - (E.g. 'Password' instead of 'password')
 * Must be a promise so that the state of the component should be updated only if the validation passes
 */
export function inputValidator(type: string, element: string, name?: string): Promise<ValidationResponse> {
    return new Promise((resolve) => {
        let response: ValidationResponse = {
            isValid: true,
            message: '',
        };

        if (element.length < 6) { // First check if length if passed
            response.isValid = false;
            response.message = `${name} should have more than 6 characters`;
        } else { // Other specific checkers for each type of input
            switch (type) {
                case 'text': {
                    // Do some more checks -- invalid characters etc... whitespaces...
                    break;
                }

                case 'password': {
                    // Check if password has numbers in it
                    if (!element.split('').some(letter => Number.isInteger(parseInt(letter)))) {
                        response.isValid = false;
                        response.message = `${name} should contain at least 1 number`;
                    }

                    // Others...
                    break;
                }

                case 'email': {
                    // Check if it has @
                    if (element.indexOf('@') < 0) {
                        response.isValid = false;
                        response.message = `${name} does not contain '@'`;
                    }

                    break;
                }

                default:
                    break;
            }
        }

        resolve(response);
    });
}