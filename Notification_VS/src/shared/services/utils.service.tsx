import { LoginResponse } from '../../auth/interfaces/login';
import * as React from 'react';
import { Linking, Platform } from 'react-native';
import { Redirect } from 'react-router';

/**
 * @param data mandatory - Array of Objects to be sorted
 * @param key mandatory - sort array of objects by given key
 * @param isAsc not mandatory - true by default (ascending/descending)
 */
export function sortArray<T>(data: T[], key: string, isAsc?: boolean) {
    return data.sort((a: T, b: T) => (a[key as keyof T] < b[key as keyof T] ? -1 : 1) * (isAsc ? 1 : -1));
}

/**
 * Needed in order to open absolute urls
 * <Link> can open only relative URLs and <a> tag is not available in native react
 * TODO additional param for: target='new'
 */
export function goTo(url: string): void {

    if (Platform.OS === 'web') {
        window.open(url, '_blank');
    } else {
        Linking.canOpenURL(url).then(() => {
            Linking.openURL(url);
        }).catch(err => { console.error(err); });
    }
}

/**
 * Create a generator from an array.
 * This is especially useful for testing observables that emit multiple states.
 *
 * <!> Make sure that you do not have duplicate state.
 *     Distinct until changed will filter them out.
 */
export function* generatorFromArr(arr: any[]) {
    var index = 0;
    while (index < arr.length) {
        yield {
            value: arr[index],
            index: index++,
        };
    }
}

/** Sometimes you need a few ids just to fill up new mock objects */
export const genMongoUuid = function () {
    var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
    return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function () {
        return (Math.random() * 16 | 0).toString(16);
    }).toLowerCase();
};

/** Parse array of string to string - each element is delimited by comma, without spaces */
export const parseArrayToString = (arr: string[]) => {
    return arr.join();
};

/** Returns true if 2 given arrays are equal (elements at the same position are the same) */
export function arraysEqual(arr1: any[], arr2: any[]) {
    // Check if lengths are not equal => arrays not the same
    if (arr1.length !== arr2.length) {
        return false;
    }

    // Loop through arrays in order to find different values
    for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i]) {
            return false;
        }
    }

    return true;
}

export function toKebabCase(text: string) {
    return text.toLowerCase().split(' ').join('-');
}

/**
 * TODO Will be replaced by svg
 * Trims extension of the file, adds the colors + .png the end
 * @returns icon<COLOR>.png
 * @example: IN: calendar.png ---> OUT: calendarBlue.png
 */
export function changeColor(icon: string, color: string) {
    // Trim extension

    // TODO failsafe icon can be undefined
    if (icon === undefined) {
        return;
    }

    let newFileName = icon.replace(/\.[^/.]+$/, '');

    // Adds new color and .png
    newFileName = newFileName + color + '.png';

    return newFileName;
}

export function onPressMultipleItem(item: any, component: any, list: string, selectedValues: string) {
    let multipleChoiceList = component.state[list];
    let choicesString = component.state[selectedValues];
    let multipleChoiceListElementIndex = multipleChoiceList.indexOf(item);

    multipleChoiceList.splice(multipleChoiceListElementIndex, 1);
    choicesString.push(item);

    component.setState({
        dropdownMultipleChoiceList: multipleChoiceList,
        selectedValues: choicesString,
    });
}

/**
 *
 * @param props Props of the component
 * @param shouldBeAuth if the user should be auth to see the page, shouldBeAuth = true
 * @param pathToRedirect redirect to Path doesn't pass shouldBeAuth condition
 * @param Component component itself ex: LoginPage
 * @param loginResponse object received after logging in
 * Checks on Route mounting if the user is logged in or not and if the user should be allowed to visit the page or not
 */
export function renderPage(
    props: any,
    shouldBeAuth: boolean,
    pathToRedirect: string,
    Component: React.ComponentClass,
    loginResponse: LoginResponse,
) {
    let isAuth = loginResponse && loginResponse.token ? true : false;

    return isAuth === shouldBeAuth ?
        <Component {...props} /> : <Redirect to={pathToRedirect} />;
}