import { store, store$ } from '../../shared/services/state.service';
import { AppState } from '../interfaces/app.state';
import { DbCheck } from '../interfaces/shared';
import { IToastr } from '../interfaces/toastr';
import * as actions from '../state/shared.actions';
import * as sel from '../state/shared.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== TOGGLE IS PAGE LOADING ======

/** <!> All pages invoke this flag when loading the main endpoint */
export function toggleIsPageLoading(isLoading?: boolean) {
    store.dispatch(
        actions.togglePageIsLoading(!!isLoading), // Default false
    );
}

export const isPageLoading$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.IS_PAGE_LOADING(state)),
    skipWhile(isPageLoading => !isPageLoading),
);

// ====== DB CONNECTION ======

export function getDbConnection() {
    store.dispatch(
        actions.getDbConnectionReq(),
    );
}

export const dbConnection$ = (): Observable<DbCheck> => store$.pipe(
    map(state => sel.DB_CONNECTION(state)),
    skipWhile(dbConnection => !dbConnection),
    distinctUntilChanged(),
);

export const isStoreLoading$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.IS_STORE_LOADING(state)),
    distinctUntilChanged(),
);

export function setStoreLoading(flag: boolean) {
    store.dispatch(
        actions.setStoreLoading(flag),
    );
}

export function setStore(appState: AppState) {
    store.dispatch(
        actions.setStore(appState),
    );
}

// ===== ACTION CONFIRMATION =====

export function removeToastr(id: string) {
    store.dispatch(
        actions.removeToastr(id),
    );
}

export function createToastr(action: IToastr) {
    store.dispatch(
        actions.createToastr(action),
    );
}

export const toastrs$ = (): Observable<IToastr[]> => store$.pipe(
    map(state => sel.TOASTRS(state)),
    skipWhile(actions => actions === null),
);
