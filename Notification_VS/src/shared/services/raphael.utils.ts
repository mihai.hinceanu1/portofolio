import { PathBoundingBox } from '../interfaces/raphael';

// spellchecker: disable

// tslint:disable: quotemark
// tslint:disable: one-variable-per-declaration
// tslint:disable: triple-equals
// tslint:disable: curly
// tslint:disable: new-parens
// tslint:disable: no-control-regex

/**
 * We needed a way to detect path bounding box programmatically for any SVG
 * Luckily there is a library caller Raphael specialized in SVG graphics
 * The problem with it is that it only runs in the browser due to dependencies on the browser environment
 * An attempt was mode to trick Raphael into opening using jsdom but it was unsuccessful since React Native is not Node.js
 *
 * Stub for the entire Raphael library
 * It has only the dependencies needed for pathBBox to run
 */
export var R: {
    pathBBox?: any,
    _path2string?: any,
    _path2curve?: any,
    _pathToAbsolute?: any,
    is?: any,
    parsePathString?: any,
} = {};

var has = "hasOwnProperty";
var Str = String;
var lowerCase = Str.prototype.toLowerCase;
var math = Math;
var mmax = math.max;
var mmin = math.min;
var abs = math.abs;
var pow = math.pow;
var PI = math.PI;
var array = "array";
var objectToString = Object.prototype.toString;
var isnan = { "NaN": 1, "Infinity": 1, "-Infinity": 1 };
var toFloat = parseFloat;
var upperCase = Str.prototype.toUpperCase;
var p2s = /,?([achlmqrstvxz]),?/gi;
var pathCommand = /([achlmrqstvz])[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\u2028\u2029,]*((-?\d*\.?\d*(?:e[\-+]?\d+)?[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\u2028\u2029]*,?[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\u2028\u2029]*)+)/ig;
var pathValues = /(-?\d*\.?\d*(?:e[\-+]?\d+)?)[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\u2028\u2029]*,?[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\u2028\u2029]*/ig;

R.is = function (o: any, type: any) {
    type = lowerCase.call(type);
    if (type === "finite") {
        return !(isnan as any)[has](+o);
    }
    if (type === "array") {
        return o instanceof Array;
    }
    return (type === "null" && o === null) ||
        (type === typeof o && o !== null) ||
        (type === "object" && o === Object(o)) ||
        (type === "array" && Array.isArray && Array.isArray(o)) ||
        objectToString.call(o).slice(8, -1).toLowerCase() === type;
};

function clone(obj: any) {
    if (typeof obj === "function" || Object(obj) !== obj) {
        return obj;
    }
    var res = new obj.constructor();
    for (var key in obj) if (obj[has](key)) {
        res[key] = clone(obj[key]);
    }
    return res;
}

R._path2string = function () {
    return (this as any).join(",").replace(p2s, "$1");
};

function repush(array: any[], item: any) {
    for (var i = 0, ii = array.length; i < ii; i++) if (array[i] === item) {
        return array.push(array.splice(i, 1)[0]);
    }
}

function cacher(f?: any, scope?: any, postprocessor?: any) {
    var newf: any = function () {
        var arg = Array.prototype.slice.call(arguments, 0),
            args = arg.join("\u2400"),
            cache = newf.cache = newf.cache || {},
            count = newf.count = newf.count || [];
        if (cache[has](args)) {
            repush(count, args);
            return postprocessor ? postprocessor(cache[args]) : cache[args];
        }
        count.length >= 1e3 && delete cache[count.shift()];
        count.push(args);
        cache[args] = f.apply(scope, arg);
        return postprocessor ? postprocessor(cache[args]) : cache[args];
    };
    return newf;
}

// // http://schepers.cc/getting-to-the-point
function catmullRom2bezier(crp: any, z: any) {
    var d = [];
    for (var i = 0, iLen = crp.length; iLen - 2 * (!z as any) > i; i += 2) {
        var p = [
            { x: +crp[i - 2], y: +crp[i - 1] },
            { x: +crp[i], y: +crp[i + 1] },
            { x: +crp[i + 2], y: +crp[i + 3] },
            { x: +crp[i + 4], y: +crp[i + 5] }
        ];
        if (z) {
            if (!i) {
                p[0] = { x: +crp[iLen - 2], y: +crp[iLen - 1] };
            } else if (iLen - 4 === i) {
                p[3] = { x: +crp[0], y: +crp[1] };
            } else if (iLen - 2 === i) {
                p[2] = { x: +crp[0], y: +crp[1] };
                p[3] = { x: +crp[2], y: +crp[3] };
            }
        } else {
            if (iLen - 4 === i) {
                p[3] = p[2];
            } else if (!i) {
                p[0] = { x: +crp[i], y: +crp[i + 1] };
            }
        }
        d.push(["C",
            (-p[0].x + 6 * p[1].x + p[2].x) / 6,
            (-p[0].y + 6 * p[1].y + p[2].y) / 6,
            (p[1].x + 6 * p[2].x - p[3].x) / 6,
            (p[1].y + 6 * p[2].y - p[3].y) / 6,
            p[2].x,
            p[2].y
        ]);
    }

    return d;
}

R.parsePathString = function (pathString: any) {
    if (!pathString) {
        return null;
    }
    var pth = paths(pathString);
    if (pth.arr) {
        return pathClone(pth.arr);
    }

    var paramCounts = { a: 7, c: 6, h: 1, l: 2, m: 2, r: 4, q: 4, s: 4, t: 2, v: 1, z: 0 },
        data = [];
    if (R.is(pathString, array) && R.is(pathString[0], array)) { // rough assumption
        data = pathClone(pathString);
    }
    if (!data.length) {
        Str(pathString).replace(pathCommand, function (_a: any, b: any, c: any): any {
            var params: any[] = [],
                name = b.toLowerCase();
            c.replace(pathValues, function (_a: any, b: any) {
                b && params.push(+b);
            });
            if (name === "m" && params.length > 2) {
                data.push([b].concat(params.splice(0, 2)));
                name = "l";
                b = b === "m" ? "l" : "L";
            }
            if (name === "r") {
                data.push([b].concat(params));
            } else while (params.length >= (paramCounts as any)[name]) {
                data.push([b].concat(params.splice(0, (paramCounts as any)[name])));
                if (!(paramCounts as any)[name]) {
                    break;
                }
            }
        });
    }
    data.toString = R._path2string;
    pth.arr = pathClone(data);
    return data;
};

// PATHS
var paths: any = function (ps: any) {
    var p = paths.ps = paths.ps || {};
    if (p[ps]) {
        p[ps].sleep = 100;
    } else {
        p[ps] = {
            sleep: 100
        };
    }
    setTimeout(function () {
        for (var key in p) if (p.hasOwnProperty(key) && key !== ps) {
            p[key].sleep--;
            !p[key].sleep && delete p[key];
        }
    });
    return p[ps];
};

// pathDimensions
export var pathBoundingBox = R.pathBBox = function (path: any): PathBoundingBox {
    var pth = paths(path);
    if (pth.bbox) {
        return clone(pth.bbox);
    }
    if (!path) {
        return { x: 0, y: 0, width: 0, height: 0, x2: 0, y2: 0 };
    }
    path = path2curve(path);
    var x = 0,
        y = 0,
        X: any = [],
        Y: any = [],
        p;
    for (var i = 0, ii = path.length; i < ii; i++) {
        p = path[i];
        if (p[0] === "M") {
            x = p[1];
            y = p[2];
            X.push(x);
            Y.push(y);
        } else {
            var dim = curveDim(x, y, p[1], p[2], p[3], p[4], p[5], p[6]);
            X = X.concat(dim.min.x, dim.max.x);
            Y = Y.concat(dim.min.y, dim.max.y);
            x = p[5];
            y = p[6];
        }
    }
    var xmin = mmin.apply(0, X),
        ymin = mmin.apply(0, Y),
        xmax = mmax.apply(0, X),
        ymax = mmax.apply(0, Y),
        width = xmax - xmin,
        height = ymax - ymin,
        bb: PathBoundingBox = {
            x: xmin,
            y: ymin,
            x2: xmax,
            y2: ymax,
            width: width,
            height: height,
            cx: xmin + width / 2,
            cy: ymin + height / 2
        };
    pth.bbox = clone(bb);
    return bb;
};
var pathClone = function (pathArray: any) {
    var res = clone(pathArray);
    res.toString = R._path2string;
    return res;
};

var pathToAbsolute = R._pathToAbsolute = function (pathArray: any) {
    var pth = paths(pathArray);

    if (pth.abs) {
        return pathClone(pth.abs);
    }

    if (!R.is(pathArray, array) || !R.is(pathArray && pathArray[0], array)) { // rough assumption
        pathArray = R.parsePathString(pathArray);
    }

    if (!pathArray || !pathArray.length) {
        return [["M", 0, 0]];
    }

    var res = [],
        x = 0,
        y = 0,
        mx = 0,
        my = 0,
        start = 0;

    if (pathArray[0][0] === "M") {
        x = +pathArray[0][1];
        y = +pathArray[0][2];
        mx = x;
        my = y;
        start++;
        res[0] = ["M", x, y];
    }

    var crz = pathArray.length === 3 && pathArray[0][0] === "M" && pathArray[1][0].toUpperCase() === "R" && pathArray[2][0].toUpperCase() === "Z";

    for (var r, pa, i = start, ii = pathArray.length; i < ii; i++) {
        res.push(r = []);
        pa = pathArray[i];
        if (pa[0] !== upperCase.call(pa[0])) {
            r[0] = upperCase.call(pa[0]);
            switch (r[0]) {
                case "A":
                    r[1] = pa[1];
                    r[2] = pa[2];
                    r[3] = pa[3];
                    r[4] = pa[4];
                    r[5] = pa[5];
                    r[6] = +(pa[6] + x);
                    r[7] = +(pa[7] + y);
                    break;
                case "V":
                    r[1] = +pa[1] + y;
                    break;
                case "H":
                    r[1] = +pa[1] + x;
                    break;
                case "R":
                    var dots = [x, y].concat(pa.slice(1));
                    for (var j = 2, jj = dots.length; j < jj; j++) {
                        dots[j] = +dots[j] + x;
                        dots[++j] = +dots[j] + y;
                    }
                    res.pop();
                    res = res.concat(catmullRom2bezier(dots, crz) as any);
                    break;
                case "M":
                    mx = +pa[1] + x;
                    my = +pa[2] + y;
                    break;
                default:
                    for (j = 1, jj = pa.length; j < jj; j++) {
                        r[j] = +pa[j] + ((j % 2) ? x : y);
                    }
            }
        } else if (pa[0] === "R") {
            dots = [x, y].concat(pa.slice(1));
            res.pop();
            res = res.concat(catmullRom2bezier(dots, crz) as any);
            r = ["R"].concat(pa.slice(-2));
        } else {
            for (var k = 0, kk = pa.length; k < kk; k++) {
                r[k] = pa[k];
            }
        }

        switch (r[0]) {
            case "Z":
                x = mx;
                y = my;
                break;
            case "H":
                x = r[1];
                break;
            case "V":
                y = r[1];
                break;
            case "M":
                mx = r[r.length - 2];
                my = r[r.length - 1];

            break;
            default:
                x = r[r.length - 2];
                y = r[r.length - 1];
        }
    }

    res.toString = R._path2string;
    pth.abs = pathClone(res);

    return res;
};

var l2c = function (x1: any, y1: any, x2: any, y2: any) {
    return [x1, y1, x2, y2, x2, y2];
};

var q2c = function (x1: any, y1: any, ax: any, ay: any, x2: any, y2: any) {
    var _13 = 1 / 3,
        _23 = 2 / 3;

    return [
        _13 * x1 + _23 * ax,
        _13 * y1 + _23 * ay,
        _13 * x2 + _23 * ax,
        _13 * y2 + _23 * ay,
        x2,
        y2
    ];
};

var a2c: any = function (x1: any, y1: any, rx: any, ry: any, angle: any, large_arc_flag: any, sweep_flag: any, x2: any, y2: any, recursive: any) {
    // for more information of where this math came from visit:
    // http://www.w3.org/TR/SVG11/implnote.html#ArcImplementationNotes
    var _120 = PI * 120 / 180,
        rad = PI / 180 * (+angle || 0),
        res = [],
        xy,
        rotate = cacher(function (x: any, y: any, rad: any) {
            var X = x * math.cos(rad) - y * math.sin(rad),
                Y = x * math.sin(rad) + y * math.cos(rad);
            return { x: X, y: Y };
        });
    if (!recursive) {
        xy = rotate(x1, y1, -rad);
        x1 = xy.x;
        y1 = xy.y;
        xy = rotate(x2, y2, -rad);
        x2 = xy.x;
        y2 = xy.y;
        // var cos = math.cos(PI / 180 * angle),
        //     sin = math.sin(PI / 180 * angle),
        var x = (x1 - x2) / 2,
            y = (y1 - y2) / 2;
        var h = (x * x) / (rx * rx) + (y * y) / (ry * ry);
        if (h > 1) {
            h = math.sqrt(h);
            rx = h * rx;
            ry = h * ry;
        }
        var rx2 = rx * rx,
            ry2 = ry * ry,
            k = (large_arc_flag === sweep_flag ? -1 : 1) *
                math.sqrt(abs((rx2 * ry2 - rx2 * y * y - ry2 * x * x) / (rx2 * y * y + ry2 * x * x))),
            cx = k * rx * y / ry + (x1 + x2) / 2,
            cy = k * -ry * x / rx + (y1 + y2) / 2,
            f1 = math.asin(((y1 - cy) / ry).toFixed(9) as any),
            f2 = math.asin(((y2 - cy) / ry).toFixed(9) as any);

        f1 = x1 < cx ? PI - f1 : f1;
        f2 = x2 < cx ? PI - f2 : f2;
        f1 < 0 && (f1 = PI * 2 + f1);
        f2 < 0 && (f2 = PI * 2 + f2);
        if (sweep_flag && f1 > f2) {
            f1 = f1 - PI * 2;
        }
        if (!sweep_flag && f2 > f1) {
            f2 = f2 - PI * 2;
        }
    } else {
        f1 = recursive[0];
        f2 = recursive[1];
        cx = recursive[2];
        cy = recursive[3];
    }
    var df = f2 - f1;
    if (abs(df) > _120) {
        var f2old = f2,
            x2old = x2,
            y2old = y2;
        f2 = f1 + _120 * (sweep_flag && f2 > f1 ? 1 : -1);
        x2 = cx + rx * math.cos(f2);
        y2 = cy + ry * math.sin(f2);
        res = a2c(x2, y2, rx, ry, angle, 0, sweep_flag, x2old, y2old, [f2, f2old, cx, cy]);
    }
    df = f2 - f1;
    var c1 = math.cos(f1),
        s1 = math.sin(f1),
        c2 = math.cos(f2),
        s2 = math.sin(f2),
        t = math.tan(df / 4),
        hx = 4 / 3 * rx * t,
        hy = 4 / 3 * ry * t,
        m1 = [x1, y1],
        m2 = [x1 + hx * s1, y1 - hy * c1],
        m3 = [x2 + hx * s2, y2 - hy * c2],
        m4 = [x2, y2];
    m2[0] = 2 * m1[0] - m2[0];
    m2[1] = 2 * m1[1] - m2[1];
    if (recursive) {
        return [m2, m3, m4].concat(res);
    } else {
        res = [m2, m3, m4].concat(res).join().split(",");
        var newres = [];
        for (var i = 0, ii = res.length; i < ii; i++) {
            newres[i] = i % 2 ? rotate(res[i - 1], res[i], rad).y : rotate(res[i], res[i + 1], rad).x;
        }
        return newres;
    }
};

var findDotAtSegment = function (p1x: any, p1y: any, c1x: any, c1y: any, c2x: any, c2y: any, p2x: any, p2y: any, t: any) {
    var t1 = 1 - t;
    return {
        x: pow(t1, 3) * p1x + pow(t1, 2) * 3 * t * c1x + t1 * 3 * t * t * c2x + pow(t, 3) * p2x,
        y: pow(t1, 3) * p1y + pow(t1, 2) * 3 * t * c1y + t1 * 3 * t * t * c2y + pow(t, 3) * p2y
    };
};

var curveDim = cacher(function (p1x: any, p1y: any, c1x: any, c1y: any, c2x: any, c2y: any, p2x: any, p2y: any) {
    var a = (c2x - 2 * c1x + p1x) - (p2x - 2 * c2x + c1x),
        b = 2 * (c1x - p1x) - 2 * (c2x - c1x),
        c = p1x - c1x,
        t1 = (-b + math.sqrt(b * b - 4 * a * c)) / 2 / a,
        t2 = (-b - math.sqrt(b * b - 4 * a * c)) / 2 / a,
        y = [p1y, p2y],
        x = [p1x, p2x],
        dot;
    abs(t1) as any > "1e12" && (t1 = .5);
    abs(t2) as any > "1e12" && (t2 = .5);
    if (t1 > 0 && t1 < 1) {
        dot = findDotAtSegment(p1x, p1y, c1x, c1y, c2x, c2y, p2x, p2y, t1);
        x.push(dot.x);
        y.push(dot.y);
    }
    if (t2 > 0 && t2 < 1) {
        dot = findDotAtSegment(p1x, p1y, c1x, c1y, c2x, c2y, p2x, p2y, t2);
        x.push(dot.x);
        y.push(dot.y);
    }
    a = (c2y - 2 * c1y + p1y) - (p2y - 2 * c2y + c1y);
    b = 2 * (c1y - p1y) - 2 * (c2y - c1y);
    c = p1y - c1y;
    t1 = (-b + math.sqrt(b * b - 4 * a * c)) / 2 / a;
    t2 = (-b - math.sqrt(b * b - 4 * a * c)) / 2 / a;
    abs(t1) as any > "1e12" && (t1 = .5);
    abs(t2) as any > "1e12" && (t2 = .5);
    if (t1 > 0 && t1 < 1) {
        dot = findDotAtSegment(p1x, p1y, c1x, c1y, c2x, c2y, p2x, p2y, t1);
        x.push(dot.x);
        y.push(dot.y);
    }
    if (t2 > 0 && t2 < 1) {
        dot = findDotAtSegment(p1x, p1y, c1x, c1y, c2x, c2y, p2x, p2y, t2);
        x.push(dot.x);
        y.push(dot.y);
    }
    return {
        min: { x: mmin.apply(0, x), y: mmin.apply(0, y) },
        max: { x: mmax.apply(0, x), y: mmax.apply(0, y) }
    };
});

var path2curve = R._path2curve = cacher(function (path: any, path2: any) {
    var pth = !path2 && paths(path);
    if (!path2 && pth.curve) {
        return pathClone(pth.curve);
    }
    var p = pathToAbsolute(path),
        p2 = path2 && pathToAbsolute(path2),
        attrs = { x: 0, y: 0, bx: 0, by: 0, X: 0, Y: 0, qx: null as any, qy: null as any },
        attrs2 = { x: 0, y: 0, bx: 0, by: 0, X: 0, Y: 0, qx: null as any, qy: null as any },
        processPath = function (path: any, d: any, pcom: any) {
            var nx, ny, tq = { T: 1, Q: 1 };
            if (!path) {
                return ["C", d.x, d.y, d.x, d.y, d.x, d.y];
            }
            !(path[0] in tq) && (d.qx = d.qy = null);
            switch (path[0]) {
                case "M":
                    d.X = path[1];
                    d.Y = path[2];
                    break;
                case "A":
                    path = ["C"].concat(a2c.apply(0, [d.x, d.y].concat(path.slice(1)) as any));
                    break;
                case "S":
                    if (pcom === "C" || pcom === "S") { // In "S" case we have to take into account, if the previous command is C/S.
                        nx = d.x * 2 - d.bx;          // And reflect the previous
                        ny = d.y * 2 - d.by;          // command's control point relative to the current point.
                    }
                    else {                            // or some else or nothing
                        nx = d.x;
                        ny = d.y;
                    }
                    path = ["C", nx, ny].concat(path.slice(1));
                    break;
                case "T":
                    if (pcom === "Q" || pcom === "T") { // In "T" case we have to take into account, if the previous command is Q/T.
                        d.qx = d.x * 2 - d.qx;        // And make a reflection similar
                        d.qy = d.y * 2 - d.qy;        // to case "S".
                    }
                    else {                            // or something else or nothing
                        d.qx = d.x;
                        d.qy = d.y;
                    }
                    path = ["C"].concat(q2c(d.x, d.y, d.qx, d.qy, path[1], path[2]));
                    break;
                case "Q":
                    d.qx = path[1];
                    d.qy = path[2];
                    path = ["C"].concat(q2c(d.x, d.y, path[1], path[2], path[3], path[4]));
                    break;
                case "L":
                    path = ["C"].concat(l2c(d.x, d.y, path[1], path[2]));
                    break;
                case "H":
                    path = ["C"].concat(l2c(d.x, d.y, path[1], d.y));
                    break;
                case "V":
                    path = ["C"].concat(l2c(d.x, d.y, d.x, path[1]));
                    break;
                case "Z":
                    path = ["C"].concat(l2c(d.x, d.y, d.X, d.Y));
                    break;
            }
            return path;
        },
        fixArc = function (pp: any, i: any) {
            if (pp[i].length > 7) {
                pp[i].shift();
                var pi = pp[i];
                while (pi.length) {
                    pcoms1[i] = "A"; // if created multiple C:s, their original seg is saved
                    p2 && (pcoms2[i] = "A"); // the same as above
                    pp.splice(i++, 0, ["C"].concat(pi.splice(0, 6)));
                }
                pp.splice(i, 1);
                ii = mmax(p.length, (p2 && p2.length) || 0);
            }
        },
        fixM = function (path1: any, path2: any, a1: any, a2: any, i: any) {
            if (path1 && path2 && path1[i][0] === "M" && path2[i][0] !== "M") {
                path2.splice(i, 0, ["M", a2.x, a2.y]);
                a1.bx = 0;
                a1.by = 0;
                a1.x = path1[i][1];
                a1.y = path1[i][2];
                ii = mmax(p.length, (p2 && p2.length) || 0);
            }
        },
        pcoms1 = [], // path commands of original path p
        pcoms2 = [], // path commands of original path p2
        pfirst = "", // temporary holder for original path command
        pcom = ""; // holder for previous path command of original path
    for (var i = 0, ii = mmax(p.length, (p2 && p2.length) || 0); i < ii; i++) {
        p[i] && (pfirst = p[i][0]); // save current path command

        if (pfirst !== "C") // C is not saved yet, because it may be result of conversion
        {
            pcoms1[i] = pfirst; // Save current path command
            i && (pcom = pcoms1[i - 1]); // Get previous path command pcom
        }
        p[i] = processPath(p[i], attrs, pcom); // Previous path command is inputted to processPath

        if (pcoms1[i] !== "A" && pfirst === "C") pcoms1[i] = "C"; // A is the only command
        // which may produce multiple C:s
        // so we have to make sure that C is also C in original path

        fixArc(p, i); // fixArc adds also the right amount of A:s to pcoms1

        if (p2) { // the same procedures is done to p2
            p2[i] && (pfirst = p2[i][0]);
            if (pfirst !== "C") {
                pcoms2[i] = pfirst;
                i && (pcom = pcoms2[i - 1]);
            }
            p2[i] = processPath(p2[i], attrs2, pcom);

            if (pcoms2[i] !== "A" && pfirst === "C") pcoms2[i] = "C";

            fixArc(p2, i);
        }
        fixM(p, p2, attrs, attrs2, i);
        fixM(p2, p, attrs2, attrs, i);
        var seg = p[i],
            seg2 = p2 && p2[i],
            seglen = seg.length,
            seg2len = p2 && seg2.length;
        attrs.x = seg[seglen - 2];
        attrs.y = seg[seglen - 1];
        attrs.bx = toFloat(seg[seglen - 4]) || attrs.x;
        attrs.by = toFloat(seg[seglen - 3]) || attrs.y;
        attrs2.bx = p2 && (toFloat(seg2[seg2len - 4]) || attrs2.x);
        attrs2.by = p2 && (toFloat(seg2[seg2len - 3]) || attrs2.y);
        attrs2.x = p2 && seg2[seg2len - 2];
        attrs2.y = p2 && seg2[seg2len - 1];
    }
    if (!p2) {
        pth.curve = pathClone(p);
    }
    return p2 ? [p, p2] : p;
}, null, pathClone);