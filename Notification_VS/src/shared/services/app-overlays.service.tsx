import * as React from 'react';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, skipWhile } from 'rxjs/operators';

/*
 * This service was created to solve several essential problems in the app:
 *
 * <!> Register clicks on Android
 * On android children floating outside of the parent do not intercept clicks.
 *
 * <!> Collision detection
 * Dropdowns and other floating panels need collision detection.
 * This is best done using a standalone layer on top of all layers.
 *
 * <!> Modular Architecture
 * We need a way to programmatically add as many modals and overlays as we want.
 * Each module will simply call this service when it needs to render an element overlaid on top of the app.
 * Without this service we need either to insert at the root of the page all modals.
 * Or a master modal component is created that knows how to open any modal.
 * Both of these are antipatterns because of the tight coupling between modules.
 */

/**
 * Components are stored and retrieved as references.
 * This means nobody else than the reference owner can find and delete them.
 * Using arbitrary ids would make the overlay cache prone to failure.
 * Ids introduce the risk of id collision or deleting the wrong id.
 */
export const addAppOverlay =  (overlayCmp: JSX.Element) => {

    // Update Cache
    overlays.push(overlayCmp);

    // Publish
    _appOverlays$.next(
        positionAbsOverlays(overlays)
    );
};

export const removeAppOverlay = (overlayCmp: JSX.Element) => {

    // Update Cache
    overlays = overlays.filter(elem => elem !== overlayCmp);

    // Publish
    _appOverlays$.next(
        positionAbsOverlays(overlays)
    );
};

/** Keep the Subject private, nobody can next anything into it. */
const _appOverlays$ = new BehaviorSubject<JSX.Element[]>(null);

export const appOverlays$ = _appOverlays$.pipe(

    // Prevent initial useless emit
    skipWhile(overlays => !overlays),
    distinctUntilChanged(),
);

// ====== PRIVATE ======

/** Storage for all the overlays */
let overlays: JSX.Element[] = [];

/** Provide unique ids to all overlays */
let count = 0;

/**
 * Elements are mapped from the cached array.
 * Keeping the original elements without wrappers makes the cache easier to manage.
 */
const positionAbsOverlays = (overlays: JSX.Element[]): JSX.Element[] => {
    return overlays.map(overlay => positionAbsOverlay(overlay));
};

/** Place the overlaid element in a floating container that does not intercept touches. */
const positionAbsOverlay = (elem: JSX.Element): JSX.Element => {
    let uniqueId = 'overlay-id' + count++;

    return (
        <View data-cy='position-abs-overlay'
            key={uniqueId}
            style={overlayStyles}
            pointerEvents='box-none'>
            {elem}
        </View>
    );
};

const overlayStyles =  EStyleSheet.create({
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 100000,
    elevation: 100000,
});