
// TODO Looks like code smell. REVIEW and DEPRECATE
/* Check so that object is NOT undefined */
export function isNotUndefined(anyObject: any): boolean {

    if (anyObject === null || anyObject === undefined) {
        return false;
    }
    let notUndefined = !(Object.keys(anyObject).length === 0 &&
    anyObject.constructor === Object);

    return notUndefined;
}