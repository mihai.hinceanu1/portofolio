import { createBrowserHistory } from 'history';

/** Import history to change the route from any part of the app */
export let history = createBrowserHistory();