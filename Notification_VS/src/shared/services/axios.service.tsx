import { store } from './state.service';
import { APP_CFG } from '../config/app.config';
import axios from 'axios';
// import { AsyncStorage } from 'react-native';

/** Axios service
 * USE THIS INSTEAD OF NORMAL AXIOS
 * config each API Client call with headers and baseURL
 */
const http = axios.create({
    baseURL: `${APP_CFG.api}`,
    headers: {
        'Content-Type': 'application/json',
    },
});

http.interceptors.request.use(async config => {
    // const token = await AsyncStorage.getItem('token');
    const loginState = store.getState().auth.login;

    const token = loginState.response && loginState.response.token;

    if (token) {
        config.headers.Authorization = `jwt ${token}`;
    }

    return config;
}, err => {
    Promise.reject(err);
});

// Use next for checking if the user is logged in
// http.interceptors.response.use(response => response, error => {

//     if (error.response.status === 401) {
//         store.dispatch(
//             loginResp({} as LoginResp),
//         );
//         localStorage.clear();
//     }
// });

export default http;
