import axios from '../../shared/services/axios.service';
import { IIcon } from '../interfaces/icon';
import { from, Observable } from 'rxjs';

export const getIcons = (): Observable<IIcon[]> =>
    from<Promise<IIcon[]>>(
        axios.get('/icons')
            .then(res => res.data),
    );
