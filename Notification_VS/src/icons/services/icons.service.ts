import { store, store$ } from '../../shared/services/state.service';
import { IIcon } from '../interfaces/icon';
import * as iconsActions from '../state/icons.actions';
import * as iconsSelectors from '../state/icons.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ===== ICONS =====

export function getIcons() {
    store.dispatch(
        iconsActions.getIconsReq(),
    );
}

export const icons$ = (): Observable<IIcon[]> => store$.pipe(
    map(state => iconsSelectors.ICONS(state)),
    skipWhile(icons => icons === null),
    distinctUntilChanged(),
);

// ===== ICON =====

export function setIcon(icon: IIcon) {
    store.dispatch(
        iconsActions.setIcon(icon),
    );
}

export const icon$ = (): Observable<IIcon> => store$.pipe(
    map(state => iconsSelectors.ICON(state)),
    skipWhile(icon => !icon),
    // distinctUntilChanged(),
);

// ===== TOGGLE ICON PICKER =====

export function toggleIconPicker(toggle: boolean) {
    store.dispatch(
        iconsActions.toggleIconPicker(toggle),
    );
}

export const toggleIconPicker$ = (): Observable<boolean> => store$.pipe(
    map(state => iconsSelectors.TOGGLE_ICON_PICKER(state)),
    skipWhile(toggle => toggle === null),
    distinctUntilChanged(),
);
