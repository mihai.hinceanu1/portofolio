import { Action } from '../../shared/interfaces/shared';
import { IIcon } from '../interfaces/icon';

export const GET_ICONS_REQ = 'GET_ICONS_REQ';
export const getIconsReq = (): Action<null> => ({
    type: GET_ICONS_REQ,
    payload: null,
});

export const GET_ICONS_OK = 'GET_ICONS_OK';
export const getIconsOk = (icons: IIcon[]): Action<IIcon[]> => ({
    type: GET_ICONS_OK,
    payload: icons,
});

export const GET_ICONS_FAIL = 'GET_ICONS_FAIL';
export const getIconsFail = (error: string): Action<string> => ({
    type: GET_ICONS_FAIL,
    payload: error,
});

export const SET_ICON = 'SET_ICON';
export const setIcon = (icon: IIcon): Action<IIcon> => ({
    type: SET_ICON,
    payload: icon,
});

export const TOGGLE_ICON_PICKER = 'TOGGLE_ICON_PICKER';
export const toggleIconPicker = (toggle: boolean): Action<boolean> => ({
    type: TOGGLE_ICON_PICKER,
    payload: toggle,
});
