import { AppState } from '../../shared/interfaces/app.state';
import { IIcon } from '../interfaces/icon';
import { IconsState } from '../interfaces/icons-state';
import { createSelector, Selector } from 'reselect';

const ICONS_MODULE: Selector<AppState, IconsState> = (state: AppState) => state.icons;

export const ICONS = createSelector<AppState, IconsState, IIcon[]>(
    ICONS_MODULE,
    (state: IconsState) => state.icons,
);

export const ICON = createSelector<AppState, IconsState, IIcon>(
    ICONS_MODULE,
    (state: IconsState) => state.icon,
);

export const TOGGLE_ICON_PICKER = createSelector<AppState, IconsState, boolean>(
    ICONS_MODULE,
    (state: IconsState) => state.toggleIconPicker,
);
