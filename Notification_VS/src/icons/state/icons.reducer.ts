import * as iconsActions from './icons.actions';
import { iconsInitState } from './icons.init-state';
import * as nanolessonActions from '../../nano-lessons/state/actions/nanolessons-admin.actions';
import { IconsState } from '../interfaces/icons-state';

export const iconsReducer = (state: IconsState = iconsInitState, action: any) => {
    switch (action.type) {
        case iconsActions.GET_ICONS_OK: {
            return {
                ...state,
                icons: action.payload,
            };
        }

        case iconsActions.GET_ICONS_FAIL: {
            return {
                ...state,
                // error: action.payload,
            };
        }

        case iconsActions.SET_ICON: {
            return {
                ...state,
                icon: action.payload,
            };
        }

        case iconsActions.TOGGLE_ICON_PICKER: {
            return {
                ...state,
                toggleIconPicker: action.payload,
            };
        }

        case nanolessonActions.SET_CURRENT_NANOLESSON: {
            return {
                ...state,
                icon: null,
            };
        }

        default:
            return state;
    }
};
