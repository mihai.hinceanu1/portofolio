import { IconsState } from '../interfaces/icons-state';

export const iconsInitState: IconsState = {
    icons: null,
    icon: null,
    toggleIconPicker: false,
};
