import * as iconsActions from './icons.actions';
import { Action } from '../../shared/interfaces/shared';
import * as iconsWebapi from '../webapis/icons.webapi';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getIcons$ = (action$: ActionsObservable<Action<null>>) =>
    action$.ofType(iconsActions.GET_ICONS_REQ).pipe(
        concatMap(() => iconsWebapi.getIcons().pipe(
            map(icons => iconsActions.getIconsOk(icons)),
            catchError(err => of(iconsActions.getIconsFail(err))),
        ))
    );
