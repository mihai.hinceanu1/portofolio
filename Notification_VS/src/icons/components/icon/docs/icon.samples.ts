export const SVG = `
<Icon path={SVG.path} />
`;

export const SVG_WITH_COLOR = `
<Icon path={SVG.path} color='red' />
`;

export const IMAGE = `
<Icon path={IMAGE.path} />
`;

export const IMAGE_OVERRIDES = `
<Icon path={IMAGE.path}
    overrides={{ image: 'width: 40px; height: 40px;' }} />
`;