import * as samples from './icon.samples';
import * as utils from './icon.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { Icon } from '../icon';
import * as React from 'react';

interface State {}

export class IconPage extends ScrollableDocPage<State> {
    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        const { width } = this.state;

        return (
            <>
                <div.Overview width={width} data-cy='icon-page'>
                    <div.OverviewTitle>
                        Icon
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Renders either SVG or IMAGE icon.
                        Icon must be sent via Props (icon is represented as an object!!)
                    </div.OverviewDescription>

                    {/** SVG */}
                    <div.Demo data-cy='svg' width={width}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                SVG Icon
                            </div.DemoTitle>

                            <Icon path={utils.SVG.path} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.SVG} />
                    </div.Demo>

                    {/** SVG with Color */}
                    <div.Demo data-cy='svg-with-color' width={width}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                SVG Icon With Custom Color
                            </div.DemoTitle>

                            <Icon path={utils.SVG.path} color='red' />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.SVG_WITH_COLOR} />
                    </div.Demo>

                    {/** IMAGE */}
                    <div.Demo data-cy='image' width={width}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                IMAGE Icon
                            </div.DemoTitle>

                            <Icon path={utils.IMAGE.path} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.IMAGE} />
                    </div.Demo>

                    {/** IMAGE Overrides */}
                    <div.Demo data-cy='image-overrides' width={width}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                IMAGE Icon
                            </div.DemoTitle>

                            <Icon path={utils.IMAGE.path} overrides={{ image: 'width: 40px; height: 40px;' }} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.IMAGE_OVERRIDES} />
                    </div.Demo>
                </div.Overview>
            </>
        );
    }
}