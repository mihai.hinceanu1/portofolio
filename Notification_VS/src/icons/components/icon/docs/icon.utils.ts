import { IIcon } from '../../../interfaces/icon';

export const SVG: IIcon = {
    _id: '1',
    name: 'SVG Icon',
    type: 'svg',
    path: 'M12.2656 0L7 5.42578L1.73436 0L0 1.78711L7 9L14 1.78711L12.2656 0Z',
};

export const IMAGE: IIcon = {
    _id: '2',
    name: 'PNG Icon',
    type: 'image',
    path: 'https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/21_Angular_logo_logos-512.png',
};
