import styled from 'styled-components/native';

interface IconOverrides {
    width: number;
    height: number;
}

interface ImageOverrides {
    override: string;
}

export const Icon = styled.View<IconOverrides>`
    align-items: center;
    justify-content: center;
    width: ${props => props.width || 16}px;
    height: ${props => props.height || 16}px;
`;

export const Image = styled.Image<ImageOverrides>`
    width: 25px;
    height: 25px;
    ${props => props.override};
`;
