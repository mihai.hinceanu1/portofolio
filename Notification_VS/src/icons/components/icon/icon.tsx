import * as div from './icon.style';
import { iconSvg } from '../../../shared/style/svg';
import * as React from 'react';

interface Props {
    path: string;
    width?: number;
    height?: number;
    color?: string;
    overrides?: {
        image?: string;
    };
}

/** Renders SVG or PNG icon sent via Props */
export const Icon: React.FunctionComponent<Props> = (props: Props) => {
    const { color, path, overrides, width, height } = props;

    return (
        <div.Icon data-cy='icon' width={width} height={height}>

            {/** SVG */}
            {
                (path && getIconType(path) === 'svg') &&
                iconSvg(path, color)
            }

            {/** PNG */}
            {
                (path && getIconType(path) === 'image') &&
                <div.Image source={{ uri: path }}
                    override={overrides && overrides.image}
                    resizeMode='contain' />
            }
        </div.Icon>
    );
};

function getIconType(path: string) {
    const pathSplit = path.split('.');
    const extension = pathSplit[pathSplit.length - 1];

    if (!path) {
        console.error('No path provided for Icon');
        return;
    }

    if (extension === 'png' || extension === 'jpg' || extension === 'jpeg') {
        return 'image';
    } else {
        return 'svg';
    }
}