/// <reference types='cypress' />

describe('Icon Specs', () => {
    it('Loads the page', () => {
        cy.visit('http://localhost:3000/components-catalog/icons/icon');
        cy.get('[data-cy=icon-page]').should('exist');
    });

    it('Renders SVG Icon', () => {
        getEl('svg', 'icon').should('exist');
        getEl('svg', 'icon').within(() => {
            cy.get('svg').should('exist');
        });
    });

    it('Renders SVG Icon with color', () => {
        getEl('svg-with-color', 'icon').within(() => {
            cy.get('path').should('have.attr', 'fill', 'red'); // red in rgb
        });
    });

    it('Renders Image Icon', () => {
        getEl('image', 'icon').within(() => {
            cy.get('img').should('exist');
        });
    });

    it('Overrides CSS Properties when icon type is `image` (width: 40px; height: 40px;)', () => {
        getEl('image-overrides', 'icon').within(() => {
            cy.get('div').eq(0).should('have.css', 'width', '40px');
            cy.get('div').eq(0).should('have.css', 'height', '40px');
        });
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}
