import { IconFrame } from './icon-frame';
import * as div from './icon-picker.style';
import { Panel } from '../../../shared/components/panel/panel';
import { IIcon } from '../../interfaces/icon';
import * as React from 'react';
import { Subject } from 'rxjs';
// import { detectClickIsOutside, selfCloseEvents$ } from '../../../shared/services/self-close-signal.service';
// import { toggleIconPicker } from '../../services/icons.service';
// import { takeUntil } from 'rxjs/operators';

interface Props {
    icons: IIcon[];

    // callback when any icon is clicked
    onIconClick: (icon: IIcon) => void;
}

interface State {
    searchTerm: string;
}

export class IconPicker extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            searchTerm: '',
        };
    }

    public render() {
        const { searchTerm } = this.state;
        const { onIconClick, icons } = this.props;

        return (
            <div.IconPicker data-cy='icon-picker'>

                {/** Panel */}
                <Panel title='Assets'
                    search={true}
                    searchValue={searchTerm}
                    onSearchChange={(text: string) => this.onSearchChange(text)}
                    overrides={div.PANEL_OVERRIDES}>

                    {
                        (icons && !!icons.length) &&
                        icons.map(icon =>
                            <IconFrame icon={icon}
                                key={icon._id}
                                onIconClick={(icon: IIcon) => onIconClick(icon)} />
                        )
                    }

                    {
                        (icons && !icons.length) &&
                        <div.Warning data-cy='warning'>
                            No icons available...
                        </div.Warning>
                    }

                </Panel>
            </div.IconPicker>
        );
    }

    public componentDidMount() {
        // this.subscribeToSelfClose();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private onSearchChange(searchTerm: string) {
        this.setState({ searchTerm });
    }
}
