import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

interface IconBackgroundProps {
    isSvg: boolean;
}

export const IconFrame = styled.TouchableOpacity`
    width: 80px;
    height: 80px;
    align-items: center;
    justify-content: center;
    background-color: ${colors.$lightGrey};
    margin-bottom: 15px;
`;

export const IconBackground = styled.View<IconBackgroundProps>`
    width: 35px;
    height: 35px;
    align-items: center;
    justify-content: center;
    ${props => getBackgroundColor(props.isSvg)}
`;

// ===== UTILS =====
function getBackgroundColor(isSvg: boolean) {
    if (isSvg) {
        return `background-color: ${colors.$white}`;
    }

    return '';
}