/// <reference types='cypress' />

describe('Icon Picker Tests', () => {
    it('Loads the page', () => {
        cy.visit('http://localhost:3000/components-catalog/icons/icon-picker');
        cy.get('[data-cy=icon-picker]').should('exist');
    });

    it('Renders default icon-picker and message if no icons provided', () => {
        getEl('default', 'icon-picker').should('exist');
        getEl('default', 'warning').should('exist');
    });

    it('Renders icons when provided', () => {
        getEl('callback', 'icon').should('have.length', 1);
    });

    it('Executes callback when icon press', () => {
        getEl('callback', 'icon').click();
        getEl('callback', 'result').should('exist');
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}