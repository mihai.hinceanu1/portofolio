import * as mocks from './icon-picker.mock-data';
import * as samples from './icon-picker.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { IIcon } from '../../../interfaces/icon';
import { Icon } from '../../icon/icon';
import { IconPicker } from '../icon-picker';
import * as React from 'react';
import { View } from 'react-native';

interface State {
    icon: IIcon;
}

export class IconPickerPage extends ScrollableDocPage<State> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                icon: null,
            }
        };
    }

    public renderDemoPage() {
        const { width, overrides } = this.state;
        const { icon } = overrides;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width} data-cy='icon-picker'>
                    <div.OverviewTitle>
                        Icon Picker
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Given an array of icons, it renders the list
                        under a panel form with a searchbar available.
                        In the future, icons will be categorized by
                        different criteria.
                        Currently, Search is not working
                    </div.OverviewDescription>
                </div.Overview>

                {/** Default */}
                <div.Demo data-cy='default' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Icon Picker
                        </div.DemoTitle>

                        <IconPicker icons={[]} onIconClick={(icon: IIcon) => console.log(icon)} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Callback */}
                <div.Demo data-cy='callback' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callback Icon Picker
                        </div.DemoTitle>

                        <IconPicker icons={mocks.ICONS_SAMPLE}
                            onIconClick={(icon: IIcon) => this.onIconClick(icon)} />

                        {
                            icon &&
                            <View data-cy='result'>
                                <Icon path={icon.path} />
                            </View>
                        }
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CALLBACK} />
                </div.Demo>
            </>
        );
    }

    private onIconClick(icon: IIcon) {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                icon,
            },
        });
    }
}