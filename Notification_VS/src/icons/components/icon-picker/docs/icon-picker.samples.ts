export const DEFAULT = `
    <IconPicker icons={[]}
        onIconClick={(icon: IIcon) => console.log(icon)} />
`;

export const CALLBACK = `
    <IconPicker icons={ICONS_SAMPLE}
        onIconClick={(icon: IIcon) => this.setState({ icon })} />

    const ICONS_SAMPLE: IIcon[] = [{
        _id: '1',
        path: 'https://cdn3.iconfinder.com/data/icons/popular-services-brands/512/angular-js-512.png',
        type: 'png',
        name: 'ANGULAR_PNG2'
    }];
`;
