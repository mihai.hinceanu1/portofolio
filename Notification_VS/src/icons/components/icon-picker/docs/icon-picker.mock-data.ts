import { IIcon } from '../../../interfaces/icon';

export const ICONS_SAMPLE: IIcon[] = [{
    _id: '1',
    path: 'https://cdn3.iconfinder.com/data/icons/popular-services-brands/512/angular-js-512.png',
    type: 'png',
    name: 'ANGULAR_PNG2'
}];
