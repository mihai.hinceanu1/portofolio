import * as div from './icon-frame.style';
import { IIcon } from '../../interfaces/icon';
import { Icon } from '../icon/icon';
import * as React from 'react';
// import { VSCIcon } from '../vsc-icon/vsc-icon';

interface Props {
    icon: IIcon;
    onIconClick: (icon: IIcon) => void;
}

export const IconFrame: React.FunctionComponent<Props> = (props: Props) => {
    const { icon, onIconClick } = props;

    return (
        <div.IconFrame data-cy='icon-frame' onPress={() => onIconClick(icon)}>

            {/** Icon Background */}
            <div.IconBackground data-cy='icon-background' isSvg={icon.type === 'svg'}>
                {
                    !!icon &&
                    <Icon path={icon.path} />
                }
            </div.IconBackground>

        </div.IconFrame>
    );
};
