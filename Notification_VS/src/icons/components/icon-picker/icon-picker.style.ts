import styled from 'styled-components/native';

export const IconPicker = styled.View`
    width: 230px;
`;

export const Warning = styled.Text`
    text-align: center;
`;

// ===== OVERRIDES =====

export const PANEL_OVERRIDES = {
    root: '',
    children: `
        flex-wrap: wrap;
        flex-direction: row;
    `,
    height: 400,
};
