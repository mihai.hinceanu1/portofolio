import { IIcon } from './icon';

export interface IconsState {
    icons: IIcon[];

    /** Current selected icon from Icon Picker */
    icon: IIcon;

    /** Toggle icon picker rendering */
    toggleIconPicker: boolean;
}
