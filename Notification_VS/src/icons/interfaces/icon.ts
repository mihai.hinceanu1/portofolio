export interface IIcon {
    _id?: string;

    type: string;

    name: string;

    path: string;
}