import { adminRoutes } from './admin/routes/admin.routes';
import { LoginResponse } from './auth/interfaces/login';
import { authRoutes } from './auth/routes/auth.routes';
import { componentsCatalogRoutes } from './components-catalog/routes/components-catalog.routes';
import { dashboardRoutes } from './dashboard/routes/dashboard.routes';
import { learningMapRoutes } from './learning-map/routes/learning-map.routes';
import { lessonsRoutes } from './lessons/routes/lessons.routes';
import { notificationRoutes } from './notifications/routes/notification.routes';
import { Route } from './routing';
import { NotFoundPageClass } from './shared/pages/not-found-page/not-found.page';
import { renderPage } from './shared/services/utils.service';
import { userRoutes } from './users/routes/user.routes';
import * as React from 'react';
import { Switch } from 'react-router-dom';

export const appRoutes = (loginResponse: LoginResponse) => {

    return (
        <Switch>

            {/* Public */}
            {authRoutes(loginResponse)}
            {componentsCatalogRoutes(loginResponse)}
            {dashboardRoutes(loginResponse)}
            {lessonsRoutes(loginResponse)}
            {learningMapRoutes(loginResponse)}
            {notificationRoutes(loginResponse)}
            {userRoutes(loginResponse)}

            {/* Admin */}
            {adminRoutes(loginResponse)}

            {/* 404 */}
            <Route render={props => renderPage(props, true, '/auth/login', NotFoundPageClass, loginResponse)} />

        </Switch>
    );
};