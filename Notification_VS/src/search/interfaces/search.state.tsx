import { ISearchResults } from './search';

export interface SearchState {

    // Show search overlay
    isVisible: boolean;

    /** After a search query is finished the results are stored here */
    searchResults: ISearchResults;

}