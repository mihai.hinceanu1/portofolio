import { TopicDto } from '../../topics/interfaces/topic';
import { IFeaturedTopic, IRelatedTopic } from '../../topics/interfaces/topics';

// REVIEW
/**
 * Search results from the server.
 * It is optimized to be lightweight.
 * It contains multiple object types.
 * The search is executed over multiple data types.
 */
export interface ISearchResults {
    featuredTopic: IFeaturedTopic;
    parentTopic: IFeaturedTopic;
    relatedTopics: IRelatedTopic[];
    topics: TopicDto[];
    // TODO Implement generic search in the server webapi
}

// REVIEW
/**
 * Generic interface for search results items.
 * Any type of search result will be converted to this interface.
 */
export interface ISearchResult {
    image: string;
    title: string;
    content: string;
    url: string;
}
