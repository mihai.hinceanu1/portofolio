import { PaginatedSearchReq } from '../../shared/interfaces/shared';
import { store, store$ } from '../../shared/services/state.service';
import { ITopic } from '../../topics/interfaces/topic';
import { ISearchResult, ISearchResults } from '../interfaces/search';
import * as actions from '../state/actions/search.actions';
import * as sel from '../state/search.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== SEARCH OVERLAY ======

export function toggleSearchOverlay(isVisible?: boolean) {
    store.dispatch(
        actions.toggleSearch(isVisible),
    );
}

export const searchOverlayIsVisible$ = () => store$.pipe(
    map(state => sel.SEARCH_VISIBILITY(state)),
    skipWhile( isVisible => !isVisible ),
    distinctUntilChanged(),
);

export function searchKeyword(request: PaginatedSearchReq) {
    store.dispatch(
        actions.searchKeywordReq(request),
    );
}

export const searchResults$ = (): Observable<ISearchResults> => store$.pipe(
    map(state => sel.SEARCH_KEYWORD_RESULTS(state)),
    skipWhile( results => !results ),
    distinctUntilChanged(),
);

// Restore content
export function mapTopicToSearchResult(topic: ITopic): ISearchResult {
    let { /* content, */ name } = topic;

    let result: ISearchResult = {
        // content,
        content: '',
        image: 'some-image', // Wee need to add images to topics
        title: name,
        url: 'some-url', // TODO Build the full url here
    };

    return result;
}