import { APP_CFG } from '../../shared/config/app.config';
import { PaginatedSearchReq } from '../../shared/interfaces/shared';
import { ISearchResults } from '../interfaces/search';
import axios from 'axios';
import { from, Observable } from 'rxjs';

export const searchKeyword = (request: PaginatedSearchReq): Observable<ISearchResults> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<ISearchResults>>(
        axios.get<ISearchResults>(`${APP_CFG.api}/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};