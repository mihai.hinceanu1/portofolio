import { AppState } from '../../shared/interfaces/app.state';
import { ISearchResults } from '../interfaces/search';
import { SearchState } from '../interfaces/search.state';
import { createSelector, Selector } from 'reselect';

const SEARCH_MODULE: Selector<AppState, SearchState> = (state: AppState) => state.search;

export const SEARCH_VISIBILITY = createSelector<AppState, SearchState, boolean>(
    SEARCH_MODULE,
    (state: SearchState) => state.isVisible,
);

export const SEARCH_KEYWORD_RESULTS = createSelector<AppState, SearchState, ISearchResults>(
    SEARCH_MODULE,
    (state: SearchState) => state.searchResults,
);