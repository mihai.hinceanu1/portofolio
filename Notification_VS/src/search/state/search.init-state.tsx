import { SearchState } from '../interfaces/search.state';

export const searchInitialState: SearchState = {
    isVisible: false,
} as SearchState;