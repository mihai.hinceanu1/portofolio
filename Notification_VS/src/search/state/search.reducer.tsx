import * as actions from './actions/search.actions';
import { searchInitialState } from './search.init-state';
import { ISearchResults } from '../interfaces/search';
import { SearchState } from '../interfaces/search.state';

export const searchReducer = (state: SearchState = searchInitialState, action: any): SearchState => {

    switch (action.type) {

        case actions.TOGGLE_SEARCH:
            return {
                ...state,
                isVisible: action.payload ? action.payload : !state.isVisible,
            };

        case actions.SEARCH_KEYWORD_OK: {
            return {
                ...state,
                searchResults: action.payload as ISearchResults,
            };
        }

        default:
            return state;
    }

};
