import * as searchActions from './actions/search.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { Action, PaginatedSearchReq } from '../../shared/interfaces/shared';
import * as searchWebapi from '../webapis/search.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const searchTopics = (action$: ActionsObservable<Action<PaginatedSearchReq>>, _store: Store<AppState>) =>
    action$.ofType(searchActions.SEARCH_KEYWORD_REQ).pipe(
        map(action => action.payload),
        concatMap(request => searchWebapi.searchKeyword(request).pipe(
            map(results => searchActions.searchKeywordOk(results),
                catchError(error => of(searchActions.searchKeywordFail(error))),
            )),
        ),
    );