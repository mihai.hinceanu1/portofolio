import { Action, PaginatedSearchReq } from '../../../shared/interfaces/shared';
import { ISearchResults } from '../../interfaces/search';

// ====== TOGGLE SEARCH VISIBILITY ======

export const TOGGLE_SEARCH = 'TOGGLE_SEARCH';
export const toggleSearch = (isVisible: boolean): Action<boolean> => ({
    type: TOGGLE_SEARCH,
    payload: isVisible,
});

// ====== SEARCH QUERY KEYWORD ======

export const SEARCH_KEYWORD_REQ = 'SEARCH_KEYWORD_REQ';
export const searchKeywordReq = (request: PaginatedSearchReq): Action<PaginatedSearchReq> => ({
    type: SEARCH_KEYWORD_REQ,
    payload: request,
});

export const SEARCH_KEYWORD_OK = 'SEARCH_KEYWORD_OK';
export const searchKeywordOk = (keywords: any): Action<ISearchResults> => ({
    type: SEARCH_KEYWORD_OK,
    payload: keywords,
});

export const SEARCH_KEYWORD_FAIL = 'SEARCH_KEYWORD_FAIL';
export const searchKeywordFail = (error: Error): Action<Error> => ({
    type: SEARCH_KEYWORD_FAIL,
    payload: error,
});