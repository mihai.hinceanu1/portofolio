import { store, store$ } from '../../shared/services/state.service';
import { Lesson } from '../interfaces/lesson';
import { LessonCurrentNanolesson } from '../interfaces/lesson-current-nanolesson';
import * as actions from '../state/lessons.actions';
import * as sel from '../state/lessons.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== LESSONS ======

export function getLesson(lessonPath: string) {
    store.dispatch(
        actions.getLessonReq(lessonPath),
    );
}

export const lesson$ = (): Observable<Lesson> => store$.pipe(
    map(state => sel.LESSON(state)),
    skipWhile(lesson => !lesson),
);

export const lessonPending$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.LESSON_PENDING(state)),
    skipWhile(lesson => !lesson),
);

export const lessonError$ = (): Observable<string> => store$.pipe(
    map(state => sel.LESSON_ERROR(state)),
    skipWhile(lesson => !lesson),
);

// ===== CURRENT NANOLESSON =====

export function setLessonCurrentNanolesson(lessonCurrentNanolesson: LessonCurrentNanolesson) {
    store.dispatch(
        actions.setLessonCurrentNanolesson(lessonCurrentNanolesson),
    );
}

export const lessonCurrentNanolesson$ = (): Observable<LessonCurrentNanolesson> => store$.pipe(
    map(state => sel.CURRENT_NANOLESSON(state)),
    skipWhile(nano => nano === null),
    distinctUntilChanged(),
);

export function showCurrentNanolesson(show: boolean) {
    store.dispatch(
        actions.showCurrentNanolesson(show),
    );
}

export const showCurrentNanolesson$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.SHOW_CURRENT_NANOLESSON(state)),
    skipWhile(show => show === null),
    distinctUntilChanged(),
);

export function setPxScrolled(pxScrolled: number) {
    store.dispatch(
        actions.setLessonPagePxScrolled(pxScrolled),
    );
}
