import { BLOCK_TYPES } from '../../topics/constants/topics';
import { ITopic, TopicDto } from '../../topics/interfaces/topic';
import { TopicBlocksDict } from '../../topics/interfaces/topic-blocks';
import { Lesson, LessonDto } from '../interfaces/lesson';

/**
 * Lessons are returned from the aggregation in a denormalized format
 * This step normalizes them, making the payload easier to render in frontend.
 * <!> A new object is built from scratch instead of mutating the lesson Dto object.
 */
export const normalizeLessons = (lessonDto: LessonDto): Lesson => {
    let lesson: Lesson = pickLessonMetaProps(lessonDto);

    lesson.topics = moveBlocksToTopics(lessonDto);
    // lesson = mapSourceCodePackages(lesson);

    // <!> Order matters
    // lesson = moveExtraHelpTopics(lesson);

    return lesson;
};

// ====== PRIVATE ======

/**
 * Moves blocks from lesson DTO level down to topics level.
 * Later topic blocks are sorted at runtime before rendering.
 * <!> WARNING - Supports multiple block types from community plugins.
 */
const moveBlocksToTopics = (lessonDto: LessonDto): ITopic[] => {
    let topics: ITopic[];

    topics = lessonDto.topics.map(topicDto => {
        let topic: ITopic = pickTopicMetaProps(topicDto);

        moveBlocksToTopicByType(topic, topicDto, lessonDto.blocks);

        return topic;
    });

    return topics;
};

/** Search the lesson object for topics that belong to the current topic. */
const moveBlocksToTopicByType = (topic: ITopic, topicDto: TopicDto, blocks: TopicBlocksDict) => {
    let { blocksOrder } = topicDto;

    // Topic Blocks
    topic.blocks = {} as TopicBlocksDict;

    blocksOrder.forEach(({ blockId, type, order }) => {
        topic.blocks[type] = [];

        // Find in lesson
        let block = getBlockByTypeAndId(blockId, type, blocks);
        block.order = order;

        // Attach in topic
        topic.blocks[type].push(block);
    });

};

/**
 * <!> We are forced to use any.
 * Union types cannot be extended in plugins architecture.
 */
const getBlockByTypeAndId = (blockId: string, type: BLOCK_TYPES, blocksDict: TopicBlocksDict): any => {
    let blocks = blocksDict[type];
    return blocks.find(block => block._id === blockId);
};

// ====== PICK COMMON PROPERTIES ======

const pickLessonMetaProps = (lessonDto: LessonDto): Lesson => {
    let lesson: LessonDto = JSON.parse(JSON.stringify(lessonDto));

    // Remove denormalized flat lists.
    delete lesson.codePackages;
    delete lesson.blocks;
    delete lesson.images;
    delete lesson.codeSnippets;
    delete lesson.relatedTopics;

    return lesson as any as Lesson;
};

const pickTopicMetaProps = (topicDto: TopicDto): ITopic => {
    let topic: TopicDto = JSON.parse(JSON.stringify(topicDto));

    // Remove denormalized flat lists
    delete topic.attached;
    delete topic.blocksOrder;

    return topic as any as ITopic;
};