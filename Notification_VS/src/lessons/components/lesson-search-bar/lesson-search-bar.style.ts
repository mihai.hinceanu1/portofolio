import { MOBILE_WIDTH, PAGE_WIDTH } from '../../../shared/constants/adaptive.const';
import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { SEARCH_BAR_PADDING } from '../../pages/lesson-page/_const/lessons.page.const';
import { Animated } from 'react-native';
import styled from 'styled-components/native';

interface LessonSearchBarProps extends ReactWebAttributes {
    width: number;
}
export const LessonSearchBar = styled.View<LessonSearchBarProps>`
    width: ${props => getSearchBarWidth(props.width)}px;
    height: 150px;
`;

// Search bar visibility is triggered by a larger surrounding area.
export const searchBarStyle = (animation: Animated.Value) => ({
    opacity: animation,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 1, // Above the lesson content
});

function getSearchBarWidth(windowWidth: number) {
    const searchBarWidth = windowWidth - SEARCH_BAR_PADDING * 2;

    if (searchBarWidth < MOBILE_WIDTH) {

        // For small screen make the trigger area wide as the screen
        return MOBILE_WIDTH;
    }

    if (searchBarWidth > PAGE_WIDTH) {

        // Contain the bar to content width
        return PAGE_WIDTH;
    } else {

        // Contain the bar to screen width
        return searchBarWidth;
    }
}