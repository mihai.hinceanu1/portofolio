import * as div from './lesson-search-bar.style';
import * as utils from './lesson-search-bar.utils';
import { SearchBar } from '../../../shared/components/search-bar/search-bar';
import * as React from 'react';
import { Animated, Dimensions } from 'react-native';

interface Props { }

interface State {
    editable: boolean;
}

/**
 * LessonSearchBar is usually hidden for student reading comfort.
 * The search bar can be displayed on demand by hovering or by taping the area surrounding it.
 */
export class LessonSearchBar extends React.Component<Props, State> {

    private fadeInValue = new Animated.Value(0);

    constructor(props: Props) {
        super(props);
        this.state = {
            editable: false,
        };
    }

    public render() {
        const { editable } = this.state;
        const windowWidth = Math.round(Dimensions.get('window').width);

        return (
            <div.LessonSearchBar data-cy='lesson-search-bar'
                width={windowWidth}
                onMouseEnter={() => this.fadeIn()}
                onMouseLeave={() => this.fadeOut()}>

                <Animated.View
                    style={div.searchBarStyle(this.fadeInValue)}>

                    {/* Search */}
                    <SearchBar placeholder='Search'
                        onChangeText={this.onChangeText}
                        onPressIcon={this.onPressIcon}
                        editable={editable} />

                </Animated.View>
            </div.LessonSearchBar>
        );
    }

    private onChangeText = () => {
        console.log('Typing');
    }

    private onPressIcon = () => {
        console.log('Pressed');
    }

    private fadeIn() {
        this.setState({ editable: true });
        utils.fadeIn(this.fadeInValue);
    }

    private fadeOut() {
        this.setState({ editable: false });
        utils.fadeOut(this.fadeInValue);
    }
}