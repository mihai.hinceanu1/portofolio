import { Animated } from 'react-native';

// ====== HIDE SHOW ANIMATIONS ======

export const fadeIn = (value: Animated.Value) => {
    Animated.timing(value, {
        toValue: 1,
        duration: 200,
    }).start(() => {
        Animated.timing(value, {
            toValue: 0,
            duration: 400,
        });
    });
};

export const fadeOut = (value: Animated.Value) => {
    Animated.timing(value, {
        toValue: 0,
        duration: 600,
    }).start(() => {
        Animated.timing(value, {
            toValue: 1,
            duration: 400,
        });
    });
};