import * as div from './lesson-topics.style';
import { Topic } from '../../../topics/components/topic/topic';
import { expertMode$ } from '../../../topics/services/topics.service';
import { Lesson } from '../../interfaces/lesson';
import { lesson$ } from '../../services/lessons.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props { }

interface State {
    lesson: Lesson;
    expertMode: boolean;
}

/**
 * Lessons are composed of smaller topics.
 * Each lesson has a main topic that will always be rendered first
 * All the children topics of the main topic are listed one after the other
 * You can think of each lesson section as of a social media post
 * Each section is a standalone entry in the database
 * This design decision was made to facilitate several functionalities
 *  - Nano Lessons
 *  - Lesson Player
 *  - Machine Learning
 *
 * Lessons are composed of several content types such as:
 * Text, Images, Code, Layouts, Interactive Graphics, Comments, Statistics and so on
 */
export class LessonTopics extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            lesson: null,
            expertMode: false,
        };
    }

    public render() {
        const { lesson, expertMode } = this.state;

        // No empty render
        if (!lesson) { return null; }

        const { topics } = lesson;

        return (
            <div.LessonTopics data-cy='lesson-topics'>
                {
                    // Lesson Topics
                    topics && topics.map((topic, index) =>
                        <Topic topic={topic}
                            key={topic._id}
                            isMainTopic={index === 0}
                            isExpertOn={expertMode} />
                    )
                }
            </div.LessonTopics>
        );
    }

    public componentDidMount() {
        this.subscribeToLesson();
        this.subscribeToExpertMode();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToLesson() {
        lesson$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(lesson => {
                this.setState({ lesson });
            });
    }

    private subscribeToExpertMode() {
        expertMode$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(expertMode => {
                this.setState({ expertMode });
            });
    }

}
