import * as cfg from './lesson-hover-menus.configs';
import { getAppLogo } from './lesson-hover-menus.utils';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { DotsMenu } from '../../../shared/components/dots-menu/dots-menu';
import * as sharedCfg from '../../../shared/constants/hover-menus.configs';
import * as hover from '../../../shared/style/hover-menus.style';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps<never> { }

interface State {
    width: number;
}

/**
 * Contains all 4 major menus of the lesson page
 * - Learning Map Domains
 * - Lesson topics
 * - User menu
 * - Layers
 */
class _LessonHoverMenus extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        let { width } = this.state;

        return (
            <hover.Menus data-cy='lesson-hover-menus'
                pointerEvents='box-none'
                onLayout={e => this.updateWidth(e)}>

                {/* Learning Map Domains */}
                <BranchesMenu config={cfg.learningMapMenu}
                    overrides={hover.overrideTopLeftMenu(width)} />

                {/* Logo */}
                <hover.AppLogo width={width}>
                    {getAppLogo()}
                </hover.AppLogo>

                {/* Lesson Topics */}
                <BranchesMenu config={cfg.lessonMenu}
                    overrides={hover.overrideBottomLeftMenu(width)} />

                {/* User Menu */}
                <DotsMenu config={sharedCfg.userMenu}
                    overrides={hover.overrideTopRightMenu(width)} />

                {/* Layers Menu */}
                <DotsMenu config={sharedCfg.layersMenu}
                    overrides={hover.overrideBottomRightMenu(width)} />

            </hover.Menus>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

}

export const LessonHoverMenus = withRouter(_LessonHoverMenus);