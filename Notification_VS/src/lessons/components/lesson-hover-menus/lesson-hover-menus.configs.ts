import * as icn from '../../../shared/assets/icons';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../../shared/constants/branches-menu.const';
import { BranchesMenuConfig } from '../../../shared/interfaces/branches-menu';
import { mockLearningMapBranches } from '../../mocks/branches';

/** Domains and categories available on the learnings map */
export const learningMapMenu: BranchesMenuConfig = {
    branches: mockLearningMapBranches,
    icon: {
        svgPath: icn.LEARNING_MAP,
    },
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.bottom,
};

/** Topics of the current lesson */
export const lessonMenu: BranchesMenuConfig = {
    branches: [
        {
            _id: 'web-apps',
            name: 'Web Apps',
            children: [
                {
                    _id: 'css',
                    name: 'Css',
                },
                {
                    _id: 'javascript',
                    name: 'Javascript'
                },
                {
                    _id: 'html',
                    name: 'HTML'
                },
            ]
        },
        {
            _id: 'server',
            name: 'Server',
            children: [
                {
                    _id: 'sql',
                    name: 'SQL',
                },
                {
                    _id: 'mongo-db',
                    name: 'MongoDb'
                },
                {
                    _id: 'node-js',
                    name: 'Node.js'
                },
                {
                    _id: 'dot-net-core',
                    name: '.Net Core'
                },
                {
                    _id: 'express',
                    name: 'express'
                },
                {
                    _id: 'scala',
                    name: 'Scala'
                }
            ]
        },
        {
            _id: 'devops',
            name: 'Devops'
        }
    ],
    icon: {
        svgPath: icn.LESSON,
    },
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
};