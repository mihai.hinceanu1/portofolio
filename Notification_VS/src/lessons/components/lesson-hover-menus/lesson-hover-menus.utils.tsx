import * as icn from '../../../shared/assets/icons';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export const getAppLogo = (fill: string = colors.$blue): JSX.Element => {
    let boundingBox = pathBoundingBox(icn.VS_LOGO);
    const { height, width } = boundingBox;

    return (
        <Svg data-cy='logo' width={width + 1} height={height} fill='none'>
            <Path data-cy='logo-path' d={icn.VS_LOGO} fill={fill} />
        </Svg>
    );
};