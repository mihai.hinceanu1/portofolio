import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { LessonPage } from '../pages/lesson-page/lesson.page';
import * as React from 'react';
import { Route } from 'react-router';

/** Lessons Routes */
export const lessonsRoutes = (loginResponse: LoginResponse) => {
    return (
        <Route exact={true} path='/lesson/:lessonPath'
            render={props => renderPage(props, true, '/auth/login', LessonPage, loginResponse)} />
    );
};