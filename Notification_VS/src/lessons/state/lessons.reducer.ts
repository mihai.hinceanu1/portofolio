import * as actions from './lessons.actions';
import { lessonsInitialState } from './lessons.init-state';
import { Action } from '../../shared/interfaces/shared';
import { Lesson } from '../interfaces/lesson';
import { LessonsState } from '../interfaces/lessons.state';

export const lessonsReducer = (state: LessonsState = lessonsInitialState, action: Action<any>): LessonsState => {

    switch (action.type) {

        // ====== LESSON ======

        case actions.GET_LESSON_REQ: {
            return {
                ...state,
                pending: {
                    ...state.pending,
                    lesson: true,
                }
            };
        }

        case actions.GET_LESSON_OK: {
            return {
                ...state,
                lesson: action.payload as Lesson,
                pending: {
                    ...state.pending,
                    lesson: false,
                }
            };
        }

        case actions.GET_LESSON_FAIL: {
            return {
                ...state,
                pending: {
                    ...state.pending,
                    lesson: false,
                },
                error: {
                    ...state.error,
                    lesson: action.payload as string,
                }
            };
        }

        case actions.SET_LESSON_CURRENT_NANOLESSON: {
            return {
                ...state,
                currentNanolesson: action.payload,
            };
        }

        case actions.SHOW_CURRENT_NANOLESSON: {
            return {
                ...state,
                showCurrentNanolesson: action.payload,
            };
        }

        case actions.SET_LESSON_CURRENT_NANOLESSON: {
            return {
                ...state,
                currentNanolesson: {
                    ...state.currentNanolesson,
                    pxScrolled: action.payload,
                },
            };
        }

        default:
            return state;
    }

};