import { Action } from '../../shared/interfaces/shared';
import { Lesson } from '../interfaces/lesson';
import { LessonCurrentNanolesson } from '../interfaces/lesson-current-nanolesson';

// ====== GET LESSON ======

export const GET_LESSON_REQ = 'GET_LESSON_REQ';
export const getLessonReq = (lessonPath: string): Action<string> => ({
    type: GET_LESSON_REQ,
    payload: lessonPath,
});

export const GET_LESSON_OK = 'GET_LESSON_OK';
export const getLessonOk = (lesson: Lesson): Action<Lesson> => ({
    type: GET_LESSON_OK,
    payload: lesson,
});

export const GET_LESSON_FAIL = 'GET_LESSON_FAIL';
export const getLessonFail = (error: Error): Action<Error> => ({
    type: GET_LESSON_FAIL,
    payload: error,
});

// ===== SHOW CURRENT NANOLESSON =====

export const SET_LESSON_CURRENT_NANOLESSON = 'SET_LESSON_CURRENT_NANOLESSON';
export const setLessonCurrentNanolesson = (currentNanolesson: LessonCurrentNanolesson): Action<LessonCurrentNanolesson> => ({
    type: SET_LESSON_CURRENT_NANOLESSON,
    payload: currentNanolesson,
});

export const SHOW_CURRENT_NANOLESSON = 'SHOW_CURRENT_NANOLESSON';
export const showCurrentNanolesson = (show: boolean): Action<boolean> => ({
    type: SHOW_CURRENT_NANOLESSON,
    payload: show,
});

export const SET_LESSON_PAGE_PX_SCROLLED = 'SET_LESSON_PAGE_PX_SCROLLED';
export const setLessonPagePxScrolled = (pxScrolled: number) => ({
    type: SET_LESSON_PAGE_PX_SCROLLED,
    payload: pxScrolled,
});
