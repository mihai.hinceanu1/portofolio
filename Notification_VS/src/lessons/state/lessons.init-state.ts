import { LessonsState } from '../interfaces/lessons.state';

export const lessonsInitialState: LessonsState = {
    lesson: null,
    pending: {
        lesson: false,
    },
    error: {
        lesson: null,
    },
    currentNanolesson: {
        nanolesson: null,
        position: {
            x: null,
            y: null,
        },
        pxScrolled: 0,
    },
    showCurrentNanolesson: false,
};