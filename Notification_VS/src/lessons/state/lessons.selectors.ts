import { AppState } from '../../shared/interfaces/app.state';
import { Lesson } from '../interfaces/lesson';
import { LessonCurrentNanolesson } from '../interfaces/lesson-current-nanolesson';
import { LessonsState } from '../interfaces/lessons.state';
import { createSelector, Selector } from 'reselect';

const LESSONS_MODULE: Selector<AppState, LessonsState> = (state: AppState) => state.lessons;

// ====== LESSONS ======

export const LESSON = createSelector<AppState, LessonsState, Lesson>(
    LESSONS_MODULE,
    state => state.lesson
);

export const LESSON_PENDING = createSelector<AppState, LessonsState, boolean>(
    LESSONS_MODULE,
    state => state.pending.lesson
);

export const LESSON_ERROR = createSelector<AppState, LessonsState, string>(
    LESSONS_MODULE,
    state => state.error.lesson
);

export const CURRENT_NANOLESSON = createSelector<AppState, LessonsState, LessonCurrentNanolesson>(
    LESSONS_MODULE,
    state => state.currentNanolesson,
);

export const SHOW_CURRENT_NANOLESSON = createSelector<AppState, LessonsState, boolean>(
    LESSONS_MODULE,
    state => state.showCurrentNanolesson,
);
