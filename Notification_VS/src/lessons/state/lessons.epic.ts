import * as actions from './lessons.actions';
import { Action } from '../../shared/interfaces/shared';
import { toggleIsPageLoading } from '../../shared/services/shared.service';
import * as webapi from '../webapis/lessons.webapi';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map, tap } from 'rxjs/operators';

export const getLesson$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(actions.GET_LESSON_REQ).pipe(
        tap(() => toggleIsPageLoading(true)),
        map(action => action.payload),
        concatMap(lessonPath => {
            return webapi.getLesson(lessonPath).pipe(
                tap(() => toggleIsPageLoading(false)),
                map(lesson => actions.getLessonOk(lesson)),
                catchError(error => of(actions.getLessonFail(error))),
            );
        }),
    );