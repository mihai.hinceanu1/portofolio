import { IBranch } from '../../shared/interfaces/branches-menu';

export const mockLearningMapBranches: IBranch[] = [
    {
        _id: 'web-apps',
        name: 'Web Apps',
        children: [
            {
                _id: 'css',
                name: 'Css',
            },
            {
                _id: 'javascript',
                name: 'Javascript'
            },
            {
                _id: 'html',
                name: 'HTML'
            },
            {
                _id: 'react',
                name: 'React'
            },
            {
                _id: 'angular',
                name: 'Angular'
            },
            {
                _id: 'vue',
                name: 'Vue'
            }
        ]
    },
    {
        _id: 'server',
        name: 'Server',
        children: [
            {
                _id: 'sql',
                name: 'SQL',
            },
            {
                _id: 'mongo-db',
                name: 'MongoDb'
            },
            {
                _id: 'node-js',
                name: 'Node.js'
            },
            {
                _id: 'dot-net-core',
                name: '.Net Core'
            },
            {
                _id: 'express',
                name: 'express'
            },
            {
                _id: 'scala',
                name: 'Scala'
            }
        ]
    },
    {
        _id: 'devops',
        name: 'Devops'
    }
]