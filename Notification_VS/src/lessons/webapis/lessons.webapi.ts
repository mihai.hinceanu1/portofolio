import axios from '../../shared/services/axios.service';
import { Lesson, LessonDto } from '../interfaces/lesson';
import { normalizeLessons } from '../services/normalize-lessons.service';
import { from, Observable } from 'rxjs';

export const getLesson = (pathname: string): Observable<Lesson> =>
    from<Promise<Lesson>>(
        axios.get<LessonDto>(`/lessons/lesson/${pathname}`)
            .then(res => normalizeLessons(res.data)),
    );