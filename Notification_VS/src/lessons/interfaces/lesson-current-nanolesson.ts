/**
 * Specify current nanolesson and its position
 * It's triggered whenever any highlight is pressed (from lesson's topics)
 */
export interface LessonCurrentNanolesson {
    nanolesson : string;
    position: {
        x: number;
        y: number;
    };
    pxScrolled: number;
}