import { ImagesDict } from '../../images/interfaces/images';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';
import { CodePackageDict } from '../../source-code/interfaces/packages';
import { CodeSnippetsDict } from '../../source-code/interfaces/source-code';
import { RelatedTopic } from '../../topics/interfaces/related-topics';
import { ITopic, TopicDto } from '../../topics/interfaces/topic';
import { TopicBlocksDict } from '../../topics/interfaces/topic-blocks';

/**
 * Lessons are containers of topics.
 * These lesson topics are used to render the lesson content.
 *
 * <!> Composition Pattern
 * All the objects related to a lesson are returned in the lesson container as arrays.
 * The frontend will take over these lists and assemble them as it is required.
 * Mongodb aggregation pipeline stages are not nested.
 * This approach was favored because the aggregation itself is far more maintainable.
 * Also the composition pattern is far easier to apply with this setup.
 */
export interface Lesson extends LessonCommon {

    /** Give credit to the external author */
    // source?: LessonSource; // TO IMPLEMENT

    /**
     * <!> Code packages can be reused between topics of the same lesson.
     * Topics reference these code packages via code snippets.
     */
    codePackages?: CodePackageDict;

    /**
     * Lessons are composed of topics.
     * The first topic is used as a lesson preface.
     * All topics are listed in this array, including attached ones.
     */
    topics?: ITopic[];

}

/** Common fields between DB and Web app interfaces */
export interface LessonCommon extends CommonMetadata {

    /** Title displayed for navigation. */
    name: string;

    /** The url where this topic can be found. */
    pathName: string;

    /** The topic is project oriented, focused on practice, not on theory. */
    isProject: boolean;

    /** This topic was not written by VS authors. It belongs to other entities. */
    isExternal: boolean;

    /** Suggestive image for the topic. Displayed in search results and roadmap. */
    thumbnail: string;

    /**
     * A short description that can be accessed by simply hovering a topic link.
     * Also the description can be displayed in the search results.
     */
    description: string;

    /** Lessons are composed of topics. The first topic is rendered with bigger fonts. */
    topicsOrder: LessonTopicOrderFlag[];
}

/** References of the lesson object stored in the db as UUIDs */
export interface LessonDto extends LessonCommon {

    /** Give credit to the external author */
    // source: string; // TO IMPLEMENT

    /** Denormalized topics */
    topics?: TopicDto[];

    /**
     * Storing a reference to the source code packages helps us avoid doing a lookup trough this long chain:
     * Lessons > Topics > Blocks > Code FLows > Code Snippets > Files > Versions > Packages
     */
    codePackages?: [string];

    /**
     * All the blocks of all the topics of a lesson.
     * Later in the redux pipeline they are nested as required.
     * <!> The aggregation is easier to maintain in flat list format.
     * Therefore we return all blocks from all topics together at lesson level.
     * <!> Once the EP response is received these blocks are distributed at topic level for convenience.
     */
    blocks: TopicBlocksDict;

    /**
     * All images from all topics including images referenced by higher order business objects.
     * <!> The aggregation is easier to maintain in flat list format.
     */
    images: ImagesDict;

    /**
     * All codeSnippets from all topics including snippets referenced by higher order business objects.
     * <!> The aggregation is easier to maintain in flat list format.
     */
    codeSnippets: CodeSnippetsDict;

    /**
     * All related topics for all lesson topics in trimmed form.
     * Returned all at once to make sure the UI responds immediately on current topic changes.
     * <!> The aggregation is easier to maintain in flat list format.
     */
    relatedTopics: RelatedTopic;

}

/** Defines the position/order of a topic inside a lesson. */
export interface LessonTopicOrderFlag {

    topic: string;
    order: number;
}