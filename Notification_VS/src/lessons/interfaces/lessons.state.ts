import { Lesson } from './lesson';

export interface LessonsState {
    lesson: Lesson;

    pending: {
        lesson: boolean;
    };

    error: {
        lesson: string;
    };

    /**
     * Display nanolesson in lesson page when element (text, image, code) pressed
     */
    currentNanolesson: {
        nanolesson: string;
        position: {
            x: number;
            y: number;
        };
        pxScrolled: number;
    };

    /** Add/Remove appoverlay so that  */
    showCurrentNanolesson: boolean;
}
