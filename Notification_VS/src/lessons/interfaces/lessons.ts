import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * Describes the origin of the lesson.
 * In the platform we can render both own lessons and lessons from other publishers.
 * In case a lesson is from an external source we want to be able to give credit to the source.
 */
export interface LessonSource extends CommonMetadata {

    /** Name of the publication */
    name: string;

    /** Backlink to the original website */
    url: string;

    /** Logo of the publication */
    logo: string;
}