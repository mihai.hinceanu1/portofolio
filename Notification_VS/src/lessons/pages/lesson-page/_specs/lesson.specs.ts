/// <reference types="cypress" />

/** <!>TODO Update tests */
describe('Lesson page tests', () => {
    before(() => {
        cy.login();
    });

    it('Goes to lesson page', () => {
        cy.get('[data-cy=page-button]').eq(0).click();
        cy.url().should('include', 'http://localhost:3000/lesson');
    });
});