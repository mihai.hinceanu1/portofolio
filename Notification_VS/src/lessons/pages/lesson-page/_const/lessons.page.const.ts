/** determines where the trigger area of the search bar starts */
export const SEARCH_BAR_PADDING = 200;