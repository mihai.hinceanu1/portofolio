import styled from 'styled-components/native';

export const LessonPage = styled.View`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    width: 100%;
`;
