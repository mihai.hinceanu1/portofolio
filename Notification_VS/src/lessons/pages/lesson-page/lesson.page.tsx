import * as div from './lesson.page.style';
import { NanolessonModal } from '../../../nano-lessons/components/nanolesson-modal/nanolesson-modal';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import * as scroll from '../../../shared/style/scroll.style';
import { LessonHoverMenus } from '../../components/lesson-hover-menus/lesson-hover-menus';
import { LessonSearchBar } from '../../components/lesson-search-bar/lesson-search-bar';
import { LessonTopics } from '../../components/lesson-topics/lesson-topics';
import { getLesson, setPxScrolled, showCurrentNanolesson$ } from '../../services/lessons.service';
import * as React from 'react';
import {
    Dimensions,
    LayoutChangeEvent,
    NativeScrollEvent,
    NativeSyntheticEvent
    } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Params {
    lessonPath: string;
}

interface Props extends RouteComponentProps<Params> { }

interface State {
    width: number;
    pxScrolled: number;

    /** Set pxScrolled in state only if nanolesson is visible */
    isNanolessonVisible: boolean;
}

/**
 * Combines together several smart components to render a lesson.
 * Calls the lesson endpoint when the route changes.
 */
class _LessonPage extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    private nanolessonModal: JSX.Element = <NanolessonModal />;

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            pxScrolled: 0,
            isNanolessonVisible: false,
        };
    }

    public render() {
        let { width } = this.state;

        return (
            <div.LessonPage data-cy='lesson-page' onLayout={e => this.updateWidth(e)}>

                {/* Lesson Menus */}
                <LessonHoverMenus />

                {/* Search */}
                <LessonSearchBar />

                <scroll.ScrollView data-cy='scroll-view'
                    onScroll={event => this.onScroll(event)}
                    scrollEventThrottle={16}
                    contentContainerStyle={scroll.inner.content}>
                    <scroll.Center width={width}>

                        {/* Topics */}
                        <LessonTopics />

                    </scroll.Center>
                </scroll.ScrollView>

            </div.LessonPage>
        );
    }

    public componentDidMount() {
        this.loadLesson();
        this.subscribeToShowCurrentNanolesson();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    public componentDidUpdate(prevProps: Props) {
        this.loadLesson(prevProps);
    }

    /**
     * <!> Checking for identical params prevents lesson loading in infinite loop.
     * - Lesson load -> state store -> render() & component update -> lesson load
     */
    private loadLesson(prevProps?: Props) {
        const { lessonPath } = this.props.match.params;
        let prevLessonPath = '';

        if (prevProps) {
            prevLessonPath = prevProps.match.params.lessonPath;
        }

        if (lessonPath !== prevLessonPath) {
            getLesson(lessonPath);
        }
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    // ===== SUBSCRIBERS =====

    private subscribeToShowCurrentNanolesson() {
        showCurrentNanolesson$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isVisible => {
                if (isVisible) {
                    addAppOverlay(this.nanolessonModal);
                } else {
                    removeAppOverlay(this.nanolessonModal);
                    // clear pxScrolled value when nanolesson closed
                    setPxScrolled(0);
                }

                this.setState({ isNanolessonVisible: isVisible });
            });
    }

    private onScroll(event: NativeSyntheticEvent<NativeScrollEvent>) {
        const { isNanolessonVisible } = this.state;

        if (isNanolessonVisible) {
            // console.log('++ event.nativeEvent.contentOffset.y', event.nativeEvent.contentOffset.y);
            setPxScrolled(event.nativeEvent.contentOffset.y);
        }
    }
}

export const LessonPage = withRouter(_LessonPage);
