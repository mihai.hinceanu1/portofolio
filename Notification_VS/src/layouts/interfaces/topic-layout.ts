// REFACTOR
/**  Represent a topic fragment that has to be shown as a part of an entire topic */
export interface ITopicSummary {
    _id: string;

    features: TopicSummaryFeature[];
}

/**
 * Each topic summary contains an array of features
 * A feature is an object that has and unique ID, a label and a description
 */
export interface TopicSummaryFeature {
    /** Unique identifier for each part of the summary */
    _id: string;

    /** Title of the summary fragment */
    label: string;

    /** Description of the summary fragment */
    description: string;
}