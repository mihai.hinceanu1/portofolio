import { CommonMetadata } from '../../shared/interfaces/common-metadata';
import { ITopic } from '../../topics/interfaces/topic';
import { LAYOUT_TYPES } from '../constants/layouts.const';

/**
 * Layouts are containers of topics.
 * The layout topics are used to render the layout block.
 *
 * <!> These fields contain referenced data in a simple to use format on the frontend.
 * They are populated after a mongodb document was returned.
 */
export interface Layout extends LayoutCommon {

    /**
     * Layouts are composed of topics.
     * The first topic is used as a layout preface.
     */
    topics?: ITopic[];

}

/**
 * Metadata of the layout.
 * These are fields that keep the same types both in the DB and the React app.
 */
export interface LayoutCommon extends CommonMetadata {

    /** Type of the layouts. */
    type: LAYOUT_TYPES;
}

/**
 * References of the layout object.
 * These fields are used in Mongoose to store references as UUIDS.
 * After a document is retrieved they will be overwritten with referenced data.
 */
export interface LayoutDto extends LayoutCommon {

    /** Layouts are composed of topics. The first topic is rendered with bigger fonts. */
    topics: string[];

}