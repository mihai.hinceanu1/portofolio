import { AppState } from '../../shared/interfaces/app.state';
import { AuthState } from '../interfaces/auth.state';
import { LoginResponse } from '../interfaces/login';
import { createSelector, Selector } from 'reselect';

const AUTH_MODULE: Selector<AppState, AuthState> = (state: AppState) => state.auth;

// ====== REGISTER ======

export const REGISTER_ACC_RESP = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.register.response,
);

export const REGISTER_ACC_ERROR = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.register.error,
);

// ====== CONFIRM ACCOUNT ======

export const CONFIRM_ACCOUNT_RESP = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.confirmAccResp,
);

export const CONFIRM_ACCOUNT_ERROR = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.confirmAccError,
);

// ====== FORGOT PASSWORD ======

export const FORGOT_PASS_RESPONSE = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.forgotPassword.response,
);

export const FORGOT_PASS_ERROR = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.forgotPassword.error,
);

// ====== RESET PASSWORD ======

export const RESET_PASSWORD_RESPONSE = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.resetPassword.response,
);

export const RESET_PASSWORD_ERROR = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.resetPassword.error,
);

// ====== LOGIN ======

export const LOGIN_RESPONSE = createSelector<AppState, AuthState, LoginResponse>(
    AUTH_MODULE,
    (state: AuthState) => state.login.response,
);

export const LOGIN_ERROR = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.login.error,
);

// ====== USER ======

export const TOKEN = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.login.response.token,
);

export const USER_ID = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.login.response.userId,
);

export const RECONFIRM_MESSAGE = createSelector<AppState, AuthState, string>(
    AUTH_MODULE,
    (state: AuthState) => state.reconfirmLoginMessage,
);