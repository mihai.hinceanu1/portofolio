import { Action } from '../../shared/interfaces/shared';
import { LoginReq, LoginResponse } from '../interfaces/login';
import { RegisterRequest } from '../interfaces/register-account';
import { ResetPasswordRequest } from '../interfaces/reset-password';

// ====== REGISTER ACCOUNT ======

export const REGISTER_ACCOUNT_REQ = 'REGISTER_ACCOUNT_REQ';
export const registerAccountReq = (user: RegisterRequest): Action<RegisterRequest> => ({
    type: REGISTER_ACCOUNT_REQ,
    payload: user,
});

export const REGISTER_ACCOUNT_OK = 'REGISTER_ACCOUNT_OK';
export const registerAccountOk = (response: string): Action<string> => ({
    type: REGISTER_ACCOUNT_OK,
    payload: response,
});

export const REGISTER_ACCOUNT_FAIL = 'REGISTER_ACCOUNT_FAIL';
export const registerAccountFail = (error: string): Action<string> => ({
    type: REGISTER_ACCOUNT_FAIL,
    payload: error,
});

// ====== CONFIRM ACCOUNT ======

export const CONFIRM_ACCOUNT_REQ = 'CONFIRM_ACCOUNT_REQ';
export const confirmAccountReq = (token: string): Action<string> => ({
    type: CONFIRM_ACCOUNT_REQ,
    payload: token,
});

export const CONFIRM_ACCOUNT_OK = 'CONFIRM_ACCOUNT_OK';
export const confirmAccountOk = (response: string): Action<string> => ({
    type: CONFIRM_ACCOUNT_OK,
    payload: response,
});

export const CONFIRM_ACCOUNT_FAIL = 'CONFIRM_ACCOUNT_FAIL';
export const confirmAccountFail = (error: Error): Action<Error> => ({
    type: CONFIRM_ACCOUNT_FAIL,
    payload: error,
});

// ====== FORGOT PASSWORD ======

export const FORGOT_PASSWORD_REQ = 'FORGOT_PASSWORD_REQ';
export const forgotPasswordReq = (email: string): Action<string> => ({
    type: FORGOT_PASSWORD_REQ,
    payload: email,
});

export const FORGOT_PASSWORD_OK = 'FORGOT_PASSWORD_OK';
export const forgotPasswordOk = (response: string): Action<string> => ({
    type: FORGOT_PASSWORD_OK,
    payload: response,
});

export const FORGOT_PASSWORD_FAIL = 'FORGOT_PASSWORD_FAIL';
export const forgotPasswordFail = (error: string): Action<string> => ({
    type: FORGOT_PASSWORD_FAIL,
    payload: error,
});

// ====== RESET PASSWORD ======

export const RESET_PASSWORD_REQ = 'RESET_PASSWORD_REQ';
export const resetPasswordReq = (resetPasswordRequest: ResetPasswordRequest): Action<ResetPasswordRequest> => ({
    type: RESET_PASSWORD_REQ,
    payload: resetPasswordRequest,
});

export const RESET_PASSWORD_OK = 'RESET_PASSWORD_OK';
export const resetPasswordOk = (response: string): Action<string> => ({
    type: RESET_PASSWORD_OK,
    payload: response,
});

export const RESET_PASSWORD_FAIL = 'RESET_PASSWORD_FAIL';
export const resetPasswordFail = (error: string): Action<string> => ({
    type: RESET_PASSWORD_FAIL,
    payload: error,
});

// ====== LOGIN ======

export const LOGIN_REQ = 'LOGIN_REQ';
export const login = (credentials: LoginReq): Action<LoginReq> => ({
    type: LOGIN_REQ,
    payload: credentials,
});

export const LOGIN_OK = 'LOGIN_OK';
export const loginOk = (response: LoginResponse): Action<LoginResponse> => ({
    type: LOGIN_OK,
    payload: response,
});

export const LOGIN_FAIL = 'LOGIN_FAIL';
export const loginFail = (error: Error): Action<Error> => ({
    type: LOGIN_FAIL,
    payload: error,
});

export const SIGN_OUT = 'SIGN_OUT';
export const signOut = (): Action<null> => ({
    type: SIGN_OUT,
    payload: null,
});

// ====== LOGIN CONFIRM ======

export const LOGIN_RECONFIRM_REQ = 'LOGIN_RECONFIRM_REQ';
export const login_reconfirm = (username: string): Action<string> => ({
    type: LOGIN_RECONFIRM_REQ,
    payload: username,
});

export const LOGIN_RECONFIRM_OK = 'LOGIN_RECONFIRM_OK';
export const loginReconfirmOk = (message: string): Action<string> => ({
    type: LOGIN_RECONFIRM_OK,
    payload: message,
});

export const LOGIN_RECONFIRM_FAIL = 'LOGIN_RECONFIRM_FAIL';
export const loginReconfirmFail = (error: Error): Action<Error> => ({
    type: LOGIN_RECONFIRM_FAIL,
    payload: error,
});
