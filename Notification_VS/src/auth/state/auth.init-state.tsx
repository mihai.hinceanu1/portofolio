import { AuthState } from '../interfaces/auth.state';

export const authInitialState: AuthState = {

    register: {
        response: null,
        error: null,
    },

    login: {
        response: null,
        error: null,
    },

    forgotPassword: {
        response: null,
        error: null,
    },

    resetPassword: {
        response: null,
        error: null,
    },

    isUsernameAvailable: false,
} as AuthState;
