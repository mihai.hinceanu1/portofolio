import * as actions from './auth.actions';
import { authInitialState } from './auth.init-state';
import { AuthState } from '../interfaces/auth.state';
import { LoginResponse } from '../interfaces/login';

export const authReducer = (state: AuthState = authInitialState, action: any): AuthState => {

    switch (action.type) {

        // ====== REGISTER ======

        case actions.REGISTER_ACCOUNT_OK: {
            return {
                ...state,
                register: {
                    ...state.register,
                    response: action.payload as string,
                },
            };
        }

        case actions.REGISTER_ACCOUNT_FAIL: {
            return {
                ...state,
                register: {
                    ...state.register,
                    error: action.payload as string,
                },
            };
        }

        // ====== CONFIRM ACCOUNT ======

        case actions.CONFIRM_ACCOUNT_OK: {
            return {
                ...state,
                confirmAccResp: action.payload,
            };
        }

        case actions.CONFIRM_ACCOUNT_FAIL: {

            return {
                ...state,
                confirmAccError: action.payload,
            };
        }

        // ====== FORGOT PASSWORD ======

        case actions.FORGOT_PASSWORD_OK: {
            return {
                ...state,
                forgotPassword: {
                    ...state.forgotPassword,
                    response: action.payload,
                },
            };
        }

        case actions.FORGOT_PASSWORD_FAIL: {
            return {
                ...state,
                forgotPassword: {
                    ...state.forgotPassword,
                    error: action.payload,
                },
            };
        }

        // ====== LOGIN ======

        case actions.LOGIN_OK: {
            let response = action.payload as LoginResponse;

            return {
                ...state,
                login: {
                    ...state.login,
                    response,
                    error: '',
                },
            };
        }

        case actions.LOGIN_FAIL: {
            console.log('Login failed');
            return {
                ...state,
                login: {
                    ...state.login,
                    error: action.payload as string,
                    response: null,
                },
            };
        }

        case actions.SIGN_OUT: {
            return {
                ...state,
                login: {
                    ...state.login,
                    response: null,
                },
            };
        }

        // ====== LOGIN RECONFIRM ======

        case actions.LOGIN_RECONFIRM_REQ: {
            return {
                ...state,
                login: {
                    ...state.login,
                    response: action.payload as LoginResponse,
                },
            };
        }

        case actions.LOGIN_RECONFIRM_OK: {
            return {
                ...state,
                reconfirmLoginMessage: action.payload,
            };
        }

        // ===== RESET PASSWORD =====

        case actions.RESET_PASSWORD_OK: {
            return {
                ...state,
                resetPassword: {
                    ...state.resetPassword,
                    response: action.payload,
                    error: null,
                },
            };
        }

        case actions.RESET_PASSWORD_FAIL: {
            return {
                ...state,
                resetPassword: {
                    ...state.resetPassword,
                    response: null,
                    error: action.payload,
                },
            };
        }

        default:
            return state;
    }
};
