import * as authActions from './auth.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { Action } from '../../shared/interfaces/shared';
import { Login } from '../interfaces/login';
import { RegisterRequest } from '../interfaces/register-account';
import { ResetPasswordRequest } from '../interfaces/reset-password';
import * as authWebApi from '../webapis/auth.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const registerAccount$ = (action$: ActionsObservable<Action<RegisterRequest>>, _store: Store<AppState>) =>
    action$.ofType(authActions.REGISTER_ACCOUNT_REQ).pipe(
        map(action => action.payload),
        concatMap(user => authWebApi.registerAccount(user).pipe(
            map(response => authActions.registerAccountOk(response as any)),
            catchError(error => of(authActions.registerAccountFail(error.response.data))),
        )),
    );

export const confirmAccount$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(authActions.CONFIRM_ACCOUNT_REQ).pipe(
        map(action => action.payload),
        concatMap(token => authWebApi.confirmAccount(token).pipe(
            map(response => authActions.confirmAccountOk(response)),
            catchError(error => of(authActions.confirmAccountFail(error.response.data))),
        )),
    );

export const forgotPassword$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(authActions.FORGOT_PASSWORD_REQ).pipe(
        map(action => action.payload),
        concatMap(email => authWebApi.forgotPassword(email).pipe(
            map(response => authActions.forgotPasswordOk(response)),
            catchError(error => of(authActions.forgotPasswordFail(error.response.data))),
        )),
    );

export const resetPassword$ = (action$: ActionsObservable<Action<ResetPasswordRequest>>, _store: Store<AppState>) =>
    action$.ofType(authActions.RESET_PASSWORD_REQ).pipe(
        map(action => action.payload),
        concatMap(resetPasswordConfig => authWebApi.newPassword(resetPasswordConfig).pipe(
            map(response => authActions.resetPasswordOk(response)),
            catchError(error => of(authActions.resetPasswordFail(error.response.data))),
        )),
    );

export const login = (action$: ActionsObservable<Action<Login>>, _store: Store<AppState>) =>
    action$.ofType(authActions.LOGIN_REQ).pipe(
        map(action => action.payload),
        concatMap(credentials => authWebApi.login(credentials.username, credentials.password).pipe(
            map(response => authActions.loginOk(response)),
            catchError(error => of(authActions.loginFail(error.response.data))),
        )),
    );

export const loginReconfirm = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(authActions.LOGIN_RECONFIRM_REQ).pipe(
        map(action => action.payload),
        concatMap(username => authWebApi.reconfirmAccount(username).pipe(
            map(response => authActions.loginReconfirmOk(response)),
            catchError(error => of(authActions.loginReconfirmFail(error))),
        )),
    );
