import { renderPage } from '../../shared/services/utils.service';
import { LoginResponse } from '../interfaces/login';
import { ConfirmAccountPage } from '../pages/confirm-account-page/confirm-account.page';
import { ForgotPasswordPage } from '../pages/forgot-password-page/forgot-password.page';
import { LoginPage } from '../pages/login-page/login.page';
import { NewPasswordPage } from '../pages/new-password-page/new-password.page';
import { RegisterPage } from '../pages/register-page/register.page';
import * as React from 'react';
import { Route } from 'react-router';

/** Auth Routes */
export const authRoutes = (loginResponse: LoginResponse) => {
    return [
        // Login
        <Route path='/auth/login' key='login' render={props => renderPage(props, false, '/', LoginPage, loginResponse)} />,

        // Register
        <Route path='/auth/register' key='register' render={props => renderPage(props, false, '/', RegisterPage, loginResponse)} />,

        // Confirm Account
        <Route path='/auth/confirm-account' key='confirm-account' render={props => renderPage(props, false, '/', ConfirmAccountPage, loginResponse)} />,

        // Forgot Password
        <Route path='/auth/forgot-password' key='forgot-password' render={props => renderPage(props, false, '/', ForgotPasswordPage, loginResponse)} />,

        // New Password
        <Route path='/auth/new-password' key='new-password' render={props => renderPage(props, false, '/', NewPasswordPage, loginResponse)} />,
    ];
};
