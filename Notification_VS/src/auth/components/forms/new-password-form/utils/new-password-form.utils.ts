import { ButtonConfig } from '../../../../../shared/interfaces/button';
import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import { State } from '../new-password-form';

export const newPasswordButtonConfig = (state: State, onPress: () => void): ButtonConfig => ({
    fontWeight: 600,
    fontSize: font.$size16,
    text: 'Reset Password',
    width: 160,
    disabled: !state.newPassword && !state.repeatNewPassword,
    onPress: () => onPress(),
    color: colors.$blue,
});
