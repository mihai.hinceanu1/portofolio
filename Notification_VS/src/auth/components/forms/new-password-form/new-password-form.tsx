import * as div from './new-password-form.style';
import * as utils from './utils/new-password-form.utils';
import { Button } from '../../../../shared/components/button/button';
import { FitImage } from '../../../../shared/components/fit-image/fit-image';
import { Input } from '../../../../shared/components/input/input';
import { resetPassword, resetPassword$, resetPasswordError$ } from '../../../services/auth.service';
import * as queryString from 'query-string';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props extends RouteComponentProps {}

export interface State {
    newPassword: string;
    repeatNewPassword: string;
    response: string;
    error: string;
}

/** Used when clicked the email link received after forgot password */
export class _NewPasswordForm extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            newPassword: '',
            repeatNewPassword: '',
            response: null,
            error: null,
        };
    }

    public render() {
        const { newPassword, repeatNewPassword, response, error } = this.state;

        return (
            <div.NewPasswordForm data-cy='new-password-form'>
                {/* Eniac */}
                <div.Image data-cy='image'>
                    <FitImage imgPath={'/auth/EniacWithKey_GIF.gif'} />
                </div.Image>

                <div.Description data-cy='description'>
                    One more step before accessing your account
                </div.Description>

                {/* New Pass */}
                <Input onChangeText={(text: string) => this.onChangeInput(text, 'newPassword')}
                    type='password'
                    value={newPassword}
                    placeholder='New Password' />

                {/* Repeat Pass */}
                <Input onChangeText={(text: string) => this.onChangeInput(text, 'repeatNewPassword')}
                    type='password'
                    value={repeatNewPassword}
                    placeholder='Retype New Password' />

                <Button config={utils.newPasswordButtonConfig(this.state, this.resetPassword.bind(this))}
                    overrides={div.ButtonOverrides} />

                {
                    !!response &&
                    <div.Response data-cy='response'>
                        {response}
                    </div.Response>
                }

                {
                    !!error &&
                    <div.Error data-cy='error'>
                        {error}
                    </div.Error>
                }
            </div.NewPasswordForm>
        );
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    public componentDidMount() {
        this.subscribeToNewPasswordResponse();
        this.subscribeToNewPasswordError();
    }

    // ====== SUBSCRIBERS ======

    private subscribeToNewPasswordResponse() {
        resetPassword$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.setState({
                    error: null,
                    response,
                });
            });
    }

    private subscribeToNewPasswordError() {
        resetPasswordError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                this.setState({ error });
            });
    }

    // ===== HANDLERS =====

    private onChangeInput(text: string, type: string) {
        this.setState({
            ...this.state,
            [type]: text,
        });
    }

    private resetPassword() {
        const { newPassword, repeatNewPassword } = this.state;
        const { search } = this.props.location;
        const { token } = queryString.parse(search);

        let resetPassConfig = {
            resetToken: token as string,
            password: newPassword,
            repeatPassword: repeatNewPassword,
        };

        resetPassword(resetPassConfig);

    }
}

export const NewPasswordForm = withRouter(_NewPasswordForm);
