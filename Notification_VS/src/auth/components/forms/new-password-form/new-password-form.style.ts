import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const NewPasswordForm = styled.View`
    align-items: center;
    padding-bottom: 50px;
`;

export const Button = styled.View`
    align-self: center;
    margin-top: 25px;
`;

export const Image = styled.View`
    width: 400px;
    height: 200px;
`;

export const Description = styled.Text`
    color: ${colors.$darkBlue};
    margin-top: 10px;
    font-size: ${font.$size12}px;
    margin-bottom: 10px;
`;

export const UserFeedBack = styled.Text`
    align-self: center;
    margin-top: 100px;
    color: ${colors.$menuBtnTextColor};
    font-size: ${font.$size16}px;
`;

export const Response = styled.Text`
    color: ${colors.$blue};
    font-size: 14px;
    text-align: center;
`;

export const Error = styled.Text`
    color: ${colors.$red};
    font-size: 14px;
    text-align: center;
`;

export const ButtonOverrides = {
    root: `align-self: center`,
};
