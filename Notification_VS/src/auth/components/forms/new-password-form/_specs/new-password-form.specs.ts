/// <reference types="cypress" />

describe('New Password Form Tests', () => {
    it('Successfully loads the page', () => {
        cy.visit('http://localhost:3000/auth/new-password');
        cy.get('[data-cy=new-password-form]').should('exist');
    });

    it('Renders image', () => {
        getEl('new-password-form', 'image').should('exist');
    });

    it('Renders description', () => {
        getEl('new-password-form', 'description').should('exist');
    });

    it('Renders inputs', () => {
        getEl('new-password-form', 'input').eq(0).should('exist')
            .should('have.attr', 'placeholder', 'New Password');

        getEl('new-password-form', 'input').eq(1).should('exist')
            .should('have.attr', 'placeholder', 'Retype New Password');
    });

    it('Renders reset password button', () => {
        getEl('new-password-form', 'button').should('exist')
            .should('have.text', 'Reset Password');
    });

    it('Button disabled when inputs are empty', () => {
        getEl('new-password-form', 'input').eq(0).clear();
        getEl('new-password-form', 'input').eq(1).clear();

        getEl('new-password-form', 'button').should('have.attr', 'disabled');
    });

    it('Shows response if API call is success', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/forgot-password/new',
            response: 'Your password has changed',
            status: 200,
        }).as('newPass');

        getEl('new-password-form', 'input').eq(0).type('newRandomPassword');
        getEl('new-password-form', 'input').eq(1).type('newRandomPassword');

        getEl('new-password-form', 'button').click();

        cy.wait('@newPass');

        getEl('new-password-form', 'response').should('exist');
        getEl('new-password-form', 'input').clear();
    });

    it('Shows error if API call is failed', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/forgot-password/new',
            response: 'Passwords do not match',
            status: 400,
        }).as('newPass');

        getEl('new-password-form', 'input').eq(0).type('newRandom');
        getEl('new-password-form', 'input').eq(1).type('newRandom1');

        getEl('new-password-form', 'button').click();

        cy.wait('@newPass');

        getEl('new-password-form', 'error').should('exist');
        getEl('new-password-form', 'input').clear();
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}
