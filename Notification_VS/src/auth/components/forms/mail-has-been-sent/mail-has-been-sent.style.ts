import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const MailHasBenSent = styled.View`
    align-items: center;
    justify-content: center;
    width: 300px;
    height: 450px;
`;

export const ConfirmMessage = styled.Text`
    font-size: ${font.$size16}px;
    color: ${colors.$blue};
    text-align: center;
    margin-bottom: 15px;
`;

export const Image = styled.View`
    width: 200px;
    height: 200px;
    align-self: center;
`;