import * as div from './mail-has-been-sent.style';
import { Button } from '../../../../shared/components/button/button';
import { FitImage } from '../../../../shared/components/fit-image/fit-image';
import { ButtonConfig } from '../../../../shared/interfaces/button';
import { colors } from '../../../../shared/style/colors';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps<never> {
    registerSuccess: boolean;
}

interface State { }

class _MailHasBeenSent extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        const  { history } = this.props;

        let backToLoginBtnConfig: ButtonConfig = {
            fontWeight: 600,
            text: 'Back to login',
            color: colors.$blue,
            onPress: () => history.push('/auth/login'),
        };

        return (
            <div.MailHasBenSent>
                <div.Image >
                    <FitImage imgPath={'/auth/Illustration_GIF.gif'} />
                </div.Image>

                <div.ConfirmMessage>
                    You have been registered. We have sent you an email.
                    Please check your inbox to confirm your account!
                </div.ConfirmMessage>

                <Button config={backToLoginBtnConfig} overrides={{ root: 'align-self: center;'}}/>
            </div.MailHasBenSent>
        );
    }
}

export const MailHasBenSent = withRouter(_MailHasBeenSent);