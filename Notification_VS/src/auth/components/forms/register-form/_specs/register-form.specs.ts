/// <reference types="cypress" />

describe('Register Form Tests', () => {
    it('Successfully loads the page', () => {
        cy.visit('http://localhost:3000/auth/register');
        cy.get('[data-cy=register-form]').should('exist');
    });

    it('Renders inputs', () => {
        getFirstNameInput().should('have.attr', 'placeholder', 'First Name');
        getLastNameInput().should('have.attr', 'placeholder', 'Last Name');
        getUsernameInput().should('have.attr', 'placeholder', 'Username');
        getEmailInput().should('have.attr', 'placeholder', 'Email');
        getPasswordInput().should('have.attr', 'placeholder', 'Password');
        getConfirmPasswordInput().should('have.attr', 'placeholder', 'Repeat Password');
    });

    it('Renders Radio Buttons', () => {
        getEl('register-form', 'radio-button').should('be.visible');
    });

    it('Renders accept terms box', () => {
        getEl('register-form', 'user-agreement').should('be.visible');
    });

    it('Renders register button', () => {
        getEl('register-form', 'button').should('be.visible');
    });

    it('If terms not accepted, button is disabled', () => {
        getEl('custom-checkbox', 'tick').should('not.be.visible');
        getEl('register-form', 'button').should('have.attr', 'disabled');
    });

    it('Shows error message if email already exists', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/register',
            response: 'Email already in use',
            status: 400,
        }).as('postUser');
        getFirstNameInput().type('Gabossssss');
        getLastNameInput().type('Biggie');
        getUsernameInput().type('Gaboooorssss');
        getEmailInput().type('gabriel@qualia.vision');
        getPasswordInput().type('SomeReallyBigPassword49@');
        getConfirmPasswordInput().type('SomeReallyBigPassword49@');
        getEl('register-form', 'custom-checkbox').click();
        getEl('register-form', 'button').click();

        cy.wait('@postUser');

        getEl('register-form', 'error').should('exist');

        // come back to init state
        getEl('register-form', 'input').clear();
        getEl('register-form', 'custom-checkbox').click();
    });

    it('Shows success message if user is registered', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/register',
            response: 'You have been registered',
            status: 200,
        }).as('postUser');

        getFirstNameInput().type('Gabossssss');
        getLastNameInput().type('Biggie');
        getUsernameInput().type('Gabosooorssss');
        getEmailInput().type('gabriel2@qualia.vision');
        getPasswordInput().type('SomeReallyBigPassword49@');
        getConfirmPasswordInput().type('SomeReallyBigPassword49@');
        getEl('register-form', 'custom-checkbox').click();
        getEl('register-form', 'button').click();

        cy.wait('@postUser');

        getEl('register-form', 'response').should('exist');

        // come back to init state
        getEl('register-form', 'input').clear();
        getEl('register-form', 'custom-checkbox').click();
    });

    it('Renders 2 types of account', () => {
        getEl('radio-button', 'radio-option').eq(0).within(() => {
            cy.get('[data-cy=label]').should('have.text', 'Student');
        });

        getEl('radio-button', 'radio-option').eq(1).within(() => {
            cy.get('[data-cy=label]').should('have.text', 'Teacher');
        });
    });

    it('Student is checked by default', () => {
        getEl('radio-button', 'radio-option').eq(0).within(() => {
            cy.get('[data-cy=radio-checked]').should('be.visible');
        });
    });
});

// ====== SELECTORS ======

function getFirstNameInput() {
    return cy.get('[data-cy=input]').eq(0);
}

function getLastNameInput() {
    return cy.get('[data-cy=input]').eq(1);
}

function getUsernameInput() {
    return cy.get('[data-cy=input]').eq(2);
}

function getEmailInput() {
    return cy.get('[data-cy=input]').eq(3);
}

function getPasswordInput() {
    return cy.get('[data-cy=input]').eq(4);
}

function getConfirmPasswordInput() {
    return cy.get('[data-cy=input]').eq(5);
}

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}
