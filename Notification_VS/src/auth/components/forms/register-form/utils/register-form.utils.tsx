import { APP_CFG } from '../../../../../shared/config/app.config';
import { ButtonConfig } from '../../../../../shared/interfaces/button';
import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import { VALIDATION_ERRORS } from '../../../../constants/register-warning-messages.const';
import { RegisterRequest, RegisterValidation } from '../../../../interfaces/register-account';
import { isEmailValid, isPasswordSecure } from '../../../../services/auth.utils';
import { State } from '../register-form';

/**
 * The data that user has input is given to this function
 * It will check each component of user necessary for registering
 */
export const validateUser = (registerObj: RegisterValidation) => {
    let { user } = registerObj;
    const { firstName, lastName, username, email, password, repeatPassword } = user;

    let validationError: RegisterRequest = {} as RegisterRequest;

    if (firstName.length < 2) {
        validationError.firstName = VALIDATION_ERRORS.firstName;
    }

    if (lastName.length < 2) {
        validationError.lastName = VALIDATION_ERRORS.lastName;
    }

    if (username.length < 6) {
        validationError.username = VALIDATION_ERRORS.username;
    }

    if (!isEmailValid(email)) {
        validationError.email = VALIDATION_ERRORS.email;
    }

    if (!isPasswordSecure(password)) {
        validationError.password = VALIDATION_ERRORS.password;
    }

    if (password !== repeatPassword) {
        validationError.repeatPassword = VALIDATION_ERRORS.repeatPassword;
    }

    if (repeatPassword.length === 0) {
        validationError.repeatPassword = 'Empty Password!';
    }

    return validationError;

};

export const displayCaptcha = (showCaptcha: boolean, error: string) => {
    return showCaptcha && !error && APP_CFG.isProd !== false ? true : false;
};

export const getDisabled = (acceptedTerms: boolean, disabled: boolean) => {
    return APP_CFG.isProd === false && acceptedTerms === false || disabled === true;
};

export const isEmpty = (obj: RegisterRequest) => {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};

export const registerButtonConfig = (state: State, onPress: () => void): ButtonConfig => ({
    onPress: () => onPress(),
    width: 210,
    text: 'Register',
    color: colors.$blue,
    fontSize: font.$size16,
    disabled: getDisabled(state.acceptTerms, state.disabled),
});
