import { ACCOUNT_TYPES } from './consts/register-form.consts';
import * as div from './register-form.style';
import * as utils from './utils/register-form.utils';
import { Button } from '../../../../shared/components/button/button';
import { Input } from '../../../../shared/components/input/input';
import { RADIO_SIDES } from '../../../../shared/components/radio-button/_const/radio-button.const';
import { RadioButtons } from '../../../../shared/components/radio-button/radio-buttons';
import { IRadioOption } from '../../../../shared/interfaces/radio-button';
import { CAPTCHA_SITE_KEY } from '../../../constants/register-captcha-key.const';
import { RegisterRequest, RegisterValidation } from '../../../interfaces/register-account';
import * as services from '../../../services/auth.service';
import { UserAgreement } from '../../user-agreement/user-agreement';
import * as React from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props extends RouteComponentProps { }

export interface State {
    user: RegisterRequest;
    error: string;
    acceptTerms: boolean;
    validationError: RegisterRequest;
    showCaptcha: boolean;
    disabled: boolean;
    accountType: IRadioOption;
    registerResponse: string;
}

/**
 * Register Box
 * Used in auth-entry.page.tsx, has the form and the functionalities for registering
 * a new user in the app
 */
class _RegisterForm extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            user: {
                username: '',
                password: '',
                repeatPassword: '',
                email: '',
                firstName: '',
                lastName: '',
            },
            error: '',
            acceptTerms: false,
            validationError: {} as RegisterRequest,
            showCaptcha: false,
            disabled: true,
            accountType: ACCOUNT_TYPES[0],
            registerResponse: null,
        };
    }

    public render() {
        const {
            error,
            acceptTerms,
            validationError,
            user,
            showCaptcha,
            accountType,
            registerResponse,
        } = this.state;

        const {
            username,
            password,
            repeatPassword,
            email,
            firstName,
            lastName,
        } = user;

        return (
            <div.RegisterForm data-cy='register-form'>

                <div.Body data-cy='body'>

                    {/** Name */}
                    <Input type='text'
                        error={validationError.firstName}
                        onChangeText={(text: string) => this.onChangeInput(text, 'firstName')}
                        value={firstName}
                        placeholder={'First Name'} />

                    <Input type='text'
                        error={validationError.lastName}
                        onChangeText={(text: string) => this.onChangeInput(text, 'lastName')}
                        value={lastName}
                        placeholder={'Last Name'} />

                    {/** Username */}
                    <Input type='text'
                        error={validationError.username}
                        onChangeText={(text: string) => this.onChangeInput(text, 'username')}
                        placeholder={'Username'}
                        value={username} />

                    {/** Email */}
                    <Input type='email'
                        error={validationError.email}
                        onChangeText={(text: string) => this.onChangeInput(text, 'email')}
                        placeholder={'Email'}
                        value={email} />

                    {/** Password */}
                    <Input type='password'
                        error={validationError.password}
                        onChangeText={(text: string) => this.onChangeInput(text, 'password')}
                        placeholder={'Password'}
                        value={password} />

                    {/** Confirm Password */}
                    <Input type='password'
                        error={validationError.repeatPassword}
                        onChangeText={(text: string) => this.onChangeInput(text, 'repeatPassword')}
                        value={repeatPassword}
                        placeholder={'Repeat Password'} />

                    {/** User role */}
                    <RadioButtons options={ACCOUNT_TYPES}
                        label={'I am a...'}
                        onPress={option => this.onAccountTypeSelect(option)}
                        selected={accountType}
                        radioSide={RADIO_SIDES.left}
                        overrides={div.RadioButtonOverrides} />

                    {/* Validate Agreement */}
                    <UserAgreement validateAgreement={() => this.validateAgreement()}
                        acceptedTermsOfUseAndPrivacyPolicy={acceptTerms} />

                    <Button config={utils.registerButtonConfig(this.state, this.onRegisterPress.bind(this))}
                        overrides={div.ButtonOverrides} />
                </div.Body>

                {
                    !!error &&
                    <div.Error data-cy='error'>{error}</div.Error>
                }

                {
                    !!registerResponse &&
                    <div.Response data-cy='response'>{registerResponse}</div.Response>
                }

                {
                    utils.displayCaptcha(showCaptcha, error) &&
                    <div.Captcha>
                        <ReCAPTCHA data-cy='captcha'
                            sitekey={CAPTCHA_SITE_KEY}
                            onChange={() => this.onCaptchaChange()} />
                    </div.Captcha>
                }
            </div.RegisterForm>
        );
    }

    public componentDidMount() {
        this.subscribeToRegisterResponse();
        this.subscribeToRegisterError();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToRegisterResponse() {
        let validationError = {} as RegisterRequest;

        // empty register form if successfully
        let user: RegisterRequest = { username: '', password: '', repeatPassword: '', email: '', firstName: '', lastName: '' };

        services.registerAccountResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.setState({
                    validationError,
                    registerResponse: response,
                    error: null,
                    user,
                    acceptTerms: false,
                });
            });
    }

    private subscribeToRegisterError() {
        services.registerAccountError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                this.setState({
                    error,
                    registerResponse: null,
                });
            });
    }

    private onRegisterPress() {
        const { user, acceptTerms, accountType } = this.state;

        let registerObj: RegisterValidation = {
            user,
            acceptTerms,
        };

        let validationError = utils.validateUser(registerObj);

        if (!utils.isEmpty(validationError)) {
            this.setState({ validationError });
        } else {
            services.registerAccount(user, accountType.label);
        }
    }

    private onChangeInput(text: string, type: string) {
        this.setState({
            ...this.state,
            user: {
                ...this.state.user,
                [type]: text,
            },
        });
    }

    private onCaptchaChange() {
        this.setState({ showCaptcha: false, disabled: false });
    }

    private validateAgreement() {
        const { acceptTerms } = this.state;

        this.setState({
            // Only for development - to be changed
            disabled: false,
            showCaptcha: true,
            acceptTerms: !acceptTerms,
        });
    }

    private onAccountTypeSelect(option: IRadioOption) {
        this.setState({ accountType: option });
    }
}

export const RegisterForm = withRouter(_RegisterForm);
