import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const RegisterForm = styled.View`
    justify-content: center;
    align-items: center;
    padding-bottom: 50px;
    max-width: 400px;
`;

export const Body = styled.View`
    align-items: center;
    justify-content: center;
`;

export const Captcha = styled.View`
    position: absolute;
    bottom: 0;
`;

export const Error = styled.Text`
    margin-top: 10px;
    text-align: center;
    font-size: ${font.$size14}px;
    color: ${colors.$red};
    padding: 10px;
`;

export const Response = styled.Text`
    margin-top: 10px;
    text-align: center;
    font-size: ${font.$size14}px;
    color: ${colors.$green};
    padding: 10px;
`;

export const RegisteredSuccessfully = styled.Text`
    margin-top: 10px;
    width: 100%;
    font-size: 14px;
    color: ${colors.$green};
    font-weight: 300;
`;

// ===== OVERRIDES =====

export const ButtonOverrides = {
    root: 'align-self: center;',
};

export const RadioButtonOverrides = {
    options: 'flex-direction: row; justify-content: space-between',
};

// ===== UTILS =====

/**
 * On mobile, we need to have the entire form on screen
 * On web, we only need height to be 100% of the parent, because otherwise
 * some elements collapse on the others
 * Should be found a better solution soon.
 */
