export const PASSWORD_WARNING = `Password must contain at least one capital letter and a digit.`

export const EMAIL_WARNING = `Email Already in use.`;

export const ACCOUNT_TYPES = [{
    _id: '1',
    label: 'Student'
}, {
    _id: '2',
    label: 'Teacher'
}];
