import { ButtonConfig } from '../../../../../shared/interfaces/button';
import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import { State } from '../forgot-password-form';

export const forgotPasswordButtonConfig = (state: State, onPress: () => void): ButtonConfig => {
    const { email } = state;

    return {
        disabled: email.length === 0,
        fontWeight: 600,
        fontSize: font.$size16,
        text: `Reset Password`,
        width: 160,
        onPress: () => onPress(),
        color: colors.$blue,
    };
};
