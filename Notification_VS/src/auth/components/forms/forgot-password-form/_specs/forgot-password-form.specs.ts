/// <reference types="cypress" />

describe('Forgot Password Tests', () => {
    it('Successfully loads the page', () => {
        cy.visit('http://localhost:3000/auth/forgot-password');
        cy.get('[data-cy=forgot-password-form]').should('exist');
    });

    it('Renders title and description', () => {
        getEl('forgot-password-form', 'title').should('exist');
        getEl('forgot-password-form', 'description').should('exist');
    });

    it('Renders Forgot Password Input with placeholder Email Address', () => {
        getEl('forgot-password-form', 'input').should('have.attr', 'placeholder', 'Email Address');
    });

    it('Renders Reset Password button with text', () => {
        getEl('forgot-password-form', 'button').should('exist');
        getEl('forgot-password-form', 'button').should('have.text', 'Reset Password');
    });

    it('Button is disabled if input is empty', () => {
        getEl('forgot-password-form', 'input').clear();
        getEl('forgot-password-form', 'button').should('have.attr', 'disabled');
    });

    it('Shows confirm message if API call successfully', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/forgot-password',
            status: 200,
            response: 'Email sent',
        }).as('forgotPassword');

        getEl('forgot-password-form', 'input').type('someemail@qualia.vision');
        getEl('forgot-password-form', 'button').click();
        cy.wait('@forgotPassword');

        getEl('forgot-password-form', 'confirm-message').should('exist');
        getEl('forgot-password-form', 'input').clear();
    });

    it('Shows error if email does not exist', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/forgot-password',
            status: 400,
            response: 'Oops. Bad Request',
        }).as('forgotPass');

        getEl('forgot-password-form', 'input').type('SomeEmailWrong@qualia.vision');
        getEl('forgot-password-form', 'button').click();
        cy.wait('@forgotPass');

        getEl('forgot-password-form', 'error').should('exist');
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}
