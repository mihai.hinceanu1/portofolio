import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const ForgotPasswordForm = styled.View`
    justify-content: center;
    align-items: center;
    max-width: 400px;
`;

// same width as input
export const TextsFrame = styled.View`
    width: 250px;
`;

export const Title = styled.Text`
    color: ${colors.$menuBtnTextColor};
    font-size: ${font.$size16}px;
    font-weight: 600;
    text-align: center;
`;

export const Description = styled.Text`
    color: ${colors.$darkBlue};
    margin-top: 5px;
    font-size: ${font.$size14}px;
    text-align: center;
`;

export const ConfirmMessage = styled.Text`
    font-size: ${font.$size16}px;
    color: ${colors.$blue};
    text-align: center;
    padding: 10px;
`;

export const Error = styled.Text`
    color: ${colors.$red};
`;

export const ButtonOverrides = {
    root: 'align-self: center;'
};
