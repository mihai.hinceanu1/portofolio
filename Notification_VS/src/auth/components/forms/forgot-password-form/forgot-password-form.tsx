import * as div from './forgot-password-form.style';
import * as utils from './utils/forgot-password-form.utils';
import { Button } from '../../../../shared/components/button/button';
import { Input } from '../../../../shared/components/input/input';
import { forgotPassword, forgotPasswordError$, forgotPasswordResponse$ } from '../../../services/auth.service';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Params { }

interface Props extends RouteComponentProps<Params> { }

export interface State {
    email: string;

    /** Error received from server */
    error: string;

    /** Response from API call */
    response: string;
}

/** Forgot Password page - user must provide an email for changing the password */
export class _ForgotPasswordForm extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            email: '',
            error: null,
            response: null,
        };
    }

    public render() {
        const { email, error, response } = this.state;

        return (
            <div.ForgotPasswordForm data-cy='forgot-password-form'>

                {/** Texts */}
                <div.TextsFrame data-cy='texts-frame'>
                    <div.Title data-cy='title'>
                        Forgot Your Password?
                    </div.Title>

                    <div.Description data-cy='description'>
                        Tell us your email address and we'll get you back on track in no time.
                    </div.Description>
                </div.TextsFrame>

                {/** Email Input */}
                <Input type='email'
                    onChangeText={(text: string) => this.onChangeInput(text)}
                    value={email}
                    placeholder={'Email Address'} />

                {/** Reset Button */}
                <Button overrides={div.ButtonOverrides}
                    config={utils.forgotPasswordButtonConfig(this.state, this.onResetButtonPress.bind(this))} />

                {/** Response */}
                {
                    response &&
                    <div.ConfirmMessage data-cy='confirm-message'>
                        An email with the reset password link was sent to your email address.
                        Please check your inbox and click that link to reset your password!
                    </div.ConfirmMessage>
                }

                {/** Error */}
                {
                    error &&
                    <div.Error data-cy='error'>
                        Something went wrong.. :(
                    </div.Error>
                }

            </div.ForgotPasswordForm>
        );
    }

    public componentDidMount() {
        this.subscribeToForgotPassword();
        this.subscribeToForgotPasswordError();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // ===== SUBSCRIBERS ======

    private subscribeToForgotPassword() {
        forgotPasswordResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.setState({
                    email: '',
                    error: null,
                    response,
                 });
            });
    }

    private subscribeToForgotPasswordError() {
        forgotPasswordError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                this.setState({ error, response: null });
            });
    }

    // ===== HANDLERS ======

    private onChangeInput(email: string) {
        this.setState({ email });
    }

    private onResetButtonPress() {
        const { email } = this.state;

        forgotPassword(email);
    }
}

export const ForgotPasswordForm = withRouter(_ForgotPasswordForm);
