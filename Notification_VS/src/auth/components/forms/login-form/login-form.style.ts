import { colors } from '../../../../shared/style/colors';
import styled from 'styled-components/native';

export const LoginForm = styled.View`
    width: 100%;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

export const Error = styled.Text`
    text-align: center;
    font-weight: 600;
    color: ${colors.$red};
    margin-bottom: 10px;
`;
