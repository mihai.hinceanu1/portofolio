import * as div from './login-form.style';
import * as utils from './utils/login-form.utils';
import { Button } from '../../../../shared/components/button/button';
import { Input } from '../../../../shared/components/input/input';
import { Login, LoginResponse } from '../../../interfaces/login';
import * as authService from '../../../services/auth.service';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { Platform } from 'react-native';

interface Params { }

interface Props extends RouteComponentProps<Params> { }

export interface State {
    credentials: Login;
    success: boolean;
    reconfirmMessage: string;
    error: string;
}

class _LoginForm extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            credentials: {
                username: '',
                password: '',
            },
            success: false,
            reconfirmMessage: '',
            error: null,
        };
    }

    public render() {
        const { credentials, error } = this.state;
        const { username, password } = credentials;

        return (
            <div.LoginForm data-cy='login-form'>

                {/** Username */}
                <Input data-cy='username'
                    icon={{ type: 'image', imageUrl: '/auth/Email.png' }}
                    onChangeText={(text: string) => this.onChangeInput(text, 'username')}
                    type='text'
                    value={username}
                    placeholder='Username' />

                {/** Password */}
                <Input data-cy='password'
                    icon={{ type: 'image', imageUrl: '/auth/Pass.png' }}
                    placeholder={'Password'}
                    onChangeText={(text: string) => this.onChangeInput(text, 'password')}
                    type='password'
                    value={password} />

                {/** Error */}
                {
                    !!error &&
                    <div.Error data-cy='error'>
                        {error}
                    </div.Error>
                }

                {/** Centered Button */}
                <Button config={utils.loginButtonConfig(this.state, this.onLoginPress.bind(this))}
                    overrides={{ root: 'align-self: center' }} />
            </div.LoginForm>
        );
    }

    public componentDidMount() {
        // this.addEventListener();
        this.subscribeToLogin();
        this.subscribeToReconfirmAccount();
        this.subscribeToLoginError();
    }

    public componentWillUnmount() {
        // this.removeEventListener();
        this.destroyed$.next();
    }

    // ===== SUBSCRIBERS =====

    private subscribeToReconfirmAccount() {
        authService.reconfirmAccount$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.setState({ reconfirmMessage: response });
            });
    }

    private subscribeToLogin() {
        authService.loginResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.handleLoginResponse(response);
            });
    }

    private subscribeToLoginError() {
        authService.loginError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                this.setState({ error });
            });
    }

    // ===== EVENT HANDLERS =====

    private handleLoginResponse(response: LoginResponse) {
        const { history } = this.props;
        /**
         * There is a case where the user can provide correct credentials, but their account
         * is not confirmed. In that case, a special token is created on the server so that
         * the user can confir
         */
        if (response && !response.success) {
            this.setState({
                error: `Please confirm your account`,
            });

            return;
        } else if (response && response.success) {
            history.push('/');
        }
    }

    private onChangeInput(text: string, type: string) {
        this.setState({
            ...this.state,
            credentials: {
                ...this.state.credentials,
                [type]: text,
            }
        });
    }

    private onLoginPress() {
        const { credentials } = this.state;

        authService.login(credentials);
    }

    // /**
    //  * Listens for ENTER key press
    //  * Auto login user is ENTER is pressed (an alternative to button click)
    //  */
    // private addEventListener() {
    //     if (Platform.OS === 'web') {
    //         document.addEventListener('keydown', (ev) => this.handleEnterLogin(ev));
    //     }
    // }

    // private removeEventListener() {
    //     if (Platform.OS === 'web') {
    //         document.removeEventListener('keydown', this.handleEnterLogin);
    //     }
    // }

    // private handleEnterLogin(event: any) {
    //     if (event.keyCode === 13) {
    //         const { credentials } = this.state;

    //         authService.login(credentials);
    //     }
    // }
}

export const LoginForm = withRouter(_LoginForm);
