import { ButtonConfig } from '../../../../../shared/interfaces/button';
import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import { State } from '../login-form';

export const loginButtonConfig = (state: State, onPress: () => void): ButtonConfig => {
    const { password, username } = state.credentials;

    return {
        disabled: password.length * username.length === 0,
        width: 200,
        fontWeight: 500,
        color: colors.$blue,
        text: 'Login',
        fontSize: font.$size16,
        onPress: () => onPress(),
    };
};
