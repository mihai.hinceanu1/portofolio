/// <reference types="cypress" />

describe('Login Tests', () => {
    it('Successfully loads the page', () => {
        cy.visit('http://localhost:3000/auth/login');
        cy.get('[data-cy=login-form]').should('exist');
    });

    it('Renders input', () => {
        getEl('login-form', 'input').eq(0).should('have.attr', 'placeholder', 'Username');
        getEl('login-form', 'input').eq(1).should('have.attr', 'placeholder', 'Password');
    });

    it('Renders login button', () => {
        getEl('login-form', 'button').should('exist');
    });

    it('Renders forgot password link', () => {
        cy.get('[data-cy=forgot-password]').should('exist');
    });

    it('Button disabled when inputs are empty', () => {
        getEl('login-form', 'input').eq(0).clear();
        getEl('login-form', 'input').eq(1).clear();

        getEl('login-form', 'button').should('have.attr', 'disabled');
    });

    it('Error is shown when login fails', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/login',
            response: 'Username and/or password invalid',
            status: 403,
        }).as('postLogin');

        getEl('login-form', 'input').eq(0).type('helloworld');
        getEl('login-form', 'input').eq(1).type('helloWorld');

        getEl('login-form', 'button').click();
        cy.wait('@postLogin');

        getEl('login-form', 'error').should('exist');
        getEl('login-form', 'input').eq(0).clear();
        getEl('login-form', 'input').eq(1).clear();
    });

    it('Successfully login redirects to homepage', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/login',
            response: {
                userId: 1,
                token: 'asdkjlafkskfjas',
            },
            status: 200,
        }).as('postLogin');

        getEl('login-form', 'input').eq(0).type('helloworld');
        getEl('login-form', 'input').eq(1).type('helloWorld');

        getEl('login-form', 'button').click();
        cy.wait('@postLogin');

        cy.url().should('eq', 'http://localhost:3000/');
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}