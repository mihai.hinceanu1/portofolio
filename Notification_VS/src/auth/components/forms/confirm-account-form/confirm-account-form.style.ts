import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const ConfirmAccountForm = styled.View`
    justify-content: flex-start;
    align-items: center;
    flex: 1;
`;

export const Image = styled.View`
    margin-top: 40px;
    width: 400px;
    height: 100px;
`;

export const JumpIntoActionText = styled.Text`
    margin-top: 15px;
    font-weight: 600;
    font-size: ${font.$size18}px;
    color: ${colors.$blue};
    align-self: center;
`;

export const UnderJumpText = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$darkBlue};
    align-self: center;
`;

export const Button = styled.View`
    align-self: center;
    margin-top: 75px;
`;

export const ConfirmBottomBox = styled.View`
    align-self: flex-end;
    flex-direction: row;
`;

export const SignUp = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    background-color: ${colors.$inputBox};
    height: 40px;
    width: 200px;
`;

export const SignIn = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    background-color: ${colors.$inputBox};
    height: 40px;
    width: 200px;
`;

export const SignUpText = styled.Text`
    font-size: ${font.$size16}px;
    font-weight: 500;
    color: ${colors.$blue};
`;

export const SignInText = styled.Text`
    font-size: ${font.$size16}px;
    font-weight: 500;
    color: ${colors.$blue};
`;

export const ResendEmailBody = styled.View`
    justify-content: space-between;
    height: 100%;
`;