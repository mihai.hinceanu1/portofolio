import * as div from './confirm-account-form.style';
import { Button } from '../../../../shared/components/button/button';
import { FitImage } from '../../../../shared/components/fit-image/fit-image';
import { ButtonConfig } from '../../../../shared/interfaces/button';
import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';
import { confirmAccount, confirmAccount$, confirmAccountError$ } from '../../../services/auth.service';
import { History, Location } from 'history';
import * as queryString from 'query-string';
import * as React from 'react';
import { View } from 'react-native';
import { match, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    history: History;
    location: Location;
    match: match;
}

interface State {
    response: string;
    error: string;
    lastToken: string;
}

/**
 * Confirm box used in auth-entry.page.tsx
 * Opens when user clicks on the link in the email
 */
export class _ConfirmAccountForm extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            response: '',
            error: '',
            lastToken: '',
        };
    }

    public render() {
        let { response, error } = this.state;

        // Move to separate method
        let successButtonConfig: ButtonConfig = {
            fontWeight: 600,
            fontSize: font.$size16,
            text: `OK, let's go!`,
            width: 160,
            onPress: () => this.goToLoginPage(),
            color: colors.$blue,
        };

        // Move to separate method
        let failButtonConfig: ButtonConfig = {
            fontWeight: 600,
            fontSize: font.$size16,
            text: 'Re-send email',
            width: 160,
            onPress: () => this.resendEmail(),
            color: colors.$blue,
        };

        return (
            <div.ConfirmAccountForm data-cy='confirm-account-page'>
                {
                    !!response &&
                    <View>
                        <div.Image>
                            <FitImage imgPath={'/auth/eniacWithConfetti.png'} />
                        </div.Image>

                        {/* Confirmation */}
                        <div.JumpIntoActionText>
                            Your Account Has Been Confirmed!
                        </div.JumpIntoActionText>

                        <div.UnderJumpText>
                            Jump straight into action now
                        </div.UnderJumpText>

                        {/* Acknowledge */}
                        <div.Button>
                            <Button config={successButtonConfig} />
                        </div.Button>

                    </View>

                }

                {/** TODO add a button for request another confirmation email */}
                {/** TODO add a sad Eniac picture */}
                {
                    !!error &&
                    <div.ResendEmailBody>
                        <View>
                            <div.Image>
                                <FitImage imgPath={'/auth/eniac_extraSad.gif'} />
                            </div.Image>

                            {/* Fail  */}
                            <div.JumpIntoActionText>
                                Verification Failed
                                </div.JumpIntoActionText>

                            <div.UnderJumpText>
                                The verification text is either invalid or has expired
                                </div.UnderJumpText>

                            {/* Acknowledge */}
                            <div.Button>
                                <Button config={failButtonConfig} />
                            </div.Button>

                        </View>

                    </div.ResendEmailBody>

                }
            </div.ConfirmAccountForm>
        );
    }

    public componentDidMount() {
        this.subscribeToConfirmAccount();
        this.subscribeToConfirmAccountError();
        this.confirmAccount();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private confirmAccount() {
        const { search } = this.props.location;
        const { token } = queryString.parse(search);

        // token can be string | string[]. Force update to "string"
        this.setState({
            lastToken: (token as string),
        });
        confirmAccount(token as string);
    }

    private subscribeToConfirmAccount() {
        confirmAccount$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.setState({ response });
            });
    }

    private subscribeToConfirmAccountError() {
        confirmAccountError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                this.setState({ error });
            });
    }

    private goToLoginPage() {
        this.props.history.push('/auth/login');
    }

    private resendEmail() {
        console.log('TODO');
    }
}

export const ConfirmAccountForm = withRouter(_ConfirmAccountForm);