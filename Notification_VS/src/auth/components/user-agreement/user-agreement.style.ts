import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { Platform } from 'react-native';
import styled from 'styled-components/native';

let isMobile = !(Platform.OS === 'web');

export const UserAgreement = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-top: 25px;
`;

export const TextTermsPolicy = styled.Text`
    color: ${colors.$blue};
    font-size: ${font.$size12}px;
`;

export const TermsAndPolicyStartText = styled.Text`
    font-size: ${font.$size12}px;
`;

export const SubheaderTextSmall = styled.View`
   align-items: center;
   ${isMobile ? '' : 'flex-direction: row;'}
`;

export const SubheaderSmallText = styled.Text`
    color: ${colors.$subheaderSmallColor};
   text-align: center;
   font-size: ${font.$size12}px;
`;

export const SubheaderText = styled.Text`
    font-size: ${font.$size12}px;
    font-weight: 300;
    text-align: center;
`;