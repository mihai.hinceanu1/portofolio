import * as div from './user-agreement.style';
import { Checkbox } from '../../../shared/components/checkbox/checkbox';
import { goTo } from '../../../shared/services/utils.service';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';

interface Props {
    validateAgreement: () => void;
    acceptedTermsOfUseAndPrivacyPolicy: boolean;
}

interface State { }

export class UserAgreement extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { validateAgreement, acceptedTermsOfUseAndPrivacyPolicy } = this.props;
        return (
            <div.UserAgreement data-cy='user-agreement'>
                <Checkbox
                    onPress={() => validateAgreement()}
                    isChecked={acceptedTermsOfUseAndPrivacyPolicy}
                    label='' />

                <div.SubheaderTextSmall>
                    <div.TermsAndPolicyStartText>
                        I accept the&nbsp;
                </div.TermsAndPolicyStartText>

                    <TouchableOpacity data-cy='linker'
                        onPress={() => goTo('https://www.websitepolicies.com/policies/view/Dlp7VpeY')} >
                        <div.TextTermsPolicy>Terms of Use &amp;</div.TextTermsPolicy>
                    </TouchableOpacity>

                    <TouchableOpacity data-cy='linker'
                        onPress={() => goTo('https://www.websitepolicies.com/policies/view/Dlp7VpeY')}>
                        <div.TextTermsPolicy> Privacy Policy</div.TextTermsPolicy>
                    </TouchableOpacity>
                </div.SubheaderTextSmall>

            </div.UserAgreement>
        );
    }
}