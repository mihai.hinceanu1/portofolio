import { MOBILE_WIDTH } from '../../../shared/constants/adaptive.const';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';

const { width } = Dimensions.get('window');

export const SelectAuthAction = styled.View`
    flex-direction: row;
    ${() => selectAuthActionAdaptive()};
`;

export const SignUp = styled.TouchableOpacity<{current: string}>`
    justify-content: center;
    align-items: center;
    height: 40px;
    width: 200px;
    background-color: ${props =>
        ['login', 'confirm', 'forgot'].includes(props.current) ? colors.$inputBox : colors.$white};
`;

export const SignIn = styled.TouchableOpacity<{current: string}>`
    justify-content: center;
    align-items: center;
    background-color: ${props =>
        ['register', 'confirm', 'forgot'].includes(props.current) ? colors.$inputBox : colors.$white};
    height: 40px;
    width: 200px;
`;

export const SignUpText = styled.Text<{current: string}>`
    font-size: ${font.$size16}px;
    font-weight: 500;
    color: ${props => props.current === 'register' ?
        colors.$blue : colors.$darkGrey};
`;

export const SignInText = styled.Text<{current: string}>`
    font-size: ${font.$size16}px;
    font-weight: 500;
    color: ${props => props.current === 'login' ?
        colors.$blue : colors.$darkGrey};
`;

// ===== UTILS =====

const selectAuthActionAdaptive = () => {
    if (MOBILE_WIDTH >= width) {
        return `
            position: absolute;
            bottom: 0;
        `;
    }

    return ``;
};
