import * as div from './select-auth-action.style';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Params { }

interface Props extends RouteComponentProps<Params> {
    current: string;
}

interface State { }

export class _SelectAuthAction extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        const { current } = this.props;

        return (
            <div.SelectAuthAction data-cy='select-auth-action'>

                {/* Sign Up */}
                <div.SignUp
                    disabled={current === 'register'}
                    onPress={() => this.switchToSignUp()}
                    current={current}>
                    <div.SignUpText testID='sign-up-text'
                        current={current}>
                        Sign Up
                    </div.SignUpText>
                </div.SignUp>

                {/* Sign In */}
                <div.SignIn
                    disabled={current === 'login'}
                    onPress={() => this.switchToSignIn()}
                    current={current}>
                    <div.SignInText testID='sign-in-text'
                        current={current}>
                        Sign In
                    </div.SignInText>
                </div.SignIn>

            </div.SelectAuthAction>
        );
    }

    public componentDidMount() {

    }

    private switchToSignIn() {
        const { history } = this.props;
        history.push('/auth/login');
    }

    private switchToSignUp() {
        const { history } = this.props;
        history.push('/auth/register');
    }
}

export const SelectAuthAction = withRouter(_SelectAuthAction);
