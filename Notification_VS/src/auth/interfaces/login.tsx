/**
 * Login page
 * Let user login before doing another things in the app
 */
export interface Login {
    username: string;
    password: string;
}

export interface LoginReq {
    username: string;
    password: string;
}

export interface LoginResponse {
    success: boolean;
    token?: string;
    userId?: string;
    registrationToken?: string;
}
