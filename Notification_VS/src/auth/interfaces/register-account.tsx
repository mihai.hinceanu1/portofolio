/**
 * Register page
 * Let user register in order to use the app
 */
export interface RegisterRequest {
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    password: string;
    repeatPassword: string;
    role?: string;
}

export interface RegisterValidation {
    user: RegisterRequest;
    acceptTerms: boolean;
}
