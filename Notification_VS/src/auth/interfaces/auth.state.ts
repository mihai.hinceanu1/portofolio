import { LoginResponse } from './login';

export interface AuthState {
    register: ServerResponse<string, string>;

    login: ServerResponse<LoginResponse, string>;

    forgotPassword: ServerResponse<string, string>;

    resetPassword: ServerResponse<string, string>;

    confirmAccResp: string;
    confirmAccError: string;

    reconfirmLoginMessage: string;
    isUsernameAvailable: boolean;
}

interface ServerResponse<R, E> {
    response: R;
    error: E;
}
