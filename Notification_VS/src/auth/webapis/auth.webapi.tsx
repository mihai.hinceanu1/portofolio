import { APP_CFG } from '../../shared/config/app.config';
import axios from '../../shared/services/axios.service';
import { LoginResponse } from '../interfaces/login';
import { RegisterRequest } from '../interfaces/register-account';
import { ResetPasswordRequest } from '../interfaces/reset-password';
import { from, Observable } from 'rxjs';

export const registerAccount = (user: RegisterRequest): Observable<string> => {
    return from<Promise<string>>(
        axios.post<string>(`${APP_CFG.api}/register`, {
            ...user,
        })
        .then(res => res.data),
    );
};

export const confirmAccount = (token: string): Observable<string> => {
    return from<Promise<string>>(
        axios.post<string>(`${APP_CFG.api}/register/confirm`, {
            token,
        })
            .then(res => res.data),
    );
};

export const forgotPassword = (email: string): Observable<string> => {
    return from<Promise<string>>(
    axios.post<string>(`${APP_CFG.api}/forgot-password`, {
        email,
    })
      .then(res => res.data),
  );
};

export const newPassword = (resetPasswordRequest: ResetPasswordRequest): Observable<string> => {
    return from<Promise<string>>(
        axios.post<string>(`${APP_CFG.api}/forgot-password/new`, {
            ...resetPasswordRequest,
        })
        .then(res => res.data),
    );
};

export const login = (username: string, password: string): Observable<LoginResponse> => {
    return from<Promise<LoginResponse>>(
        axios.post<LoginResponse>(`${APP_CFG.api}/login`, {
            username, password,
        })
            .then(res => res.data),
    );
};

export const reconfirmAccount = (username: string): Observable<any> => {
    return from<Promise<any>>(
        axios.post<any>(`${APP_CFG.api}/register/reconfirm`, {
            username,
        })
            .then(res => res.data),
    );
};