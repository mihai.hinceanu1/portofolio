import { APP_CFG } from '../../shared/config/app.config';

/**
 * For testing in localhost
 * https://stackoverflow.com/questions/3232904/using-recaptcha-on-localhost
 */
export const CAPTCHA_SITE_KEY = APP_CFG.isProd ?
    '6LdUqsQUAAAAAKcmmYdz6brh3irXw0l3u7JlF2DD' :
    '6LeJsMQUAAAAAFCS7BrWPYciv2FN0LP_8df8JKE0';