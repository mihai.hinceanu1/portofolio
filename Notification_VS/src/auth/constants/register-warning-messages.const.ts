/** Warning for each input on register */
export const VALIDATION_ERRORS = {
    firstName: 'First Name is too short. Use at least 2 characters',
    lastName: 'Last Name is too short. Use at least 2 characters',
    username: 'Username is too short. Use at least 6 characters',
    email: 'Invalid email',
    password: 'Try to put in a stronger password',
    repeatPassword: 'Passwords do not match',
};