/**
 * Minimal checks for email validation (has at least 10 characters, has a @ and a .)
 * @param email email sent to verify
 */
export function isEmailValid(email: string): boolean {
    if (email.length < 10) {
        return false;
    }

    const emailRegex = /.{1,20}@[a-z]{1,15}\.[a-z]{1,3}/;
    return emailRegex.test(email);
}

/**
 * Check if password respects minimal strength conditions:
 * -at least an upper case character;
 * -at least a number.
 * @param password sent to verify
 */
export function isPasswordSecure(password: string): boolean {
    if (password.length < 8) {
        return false;
    }

    const passRegex = /.*[0-9]+.*[A-Z]+|[A-Z]+.*[0-9]+.*/;
    return passRegex.test(password);
}