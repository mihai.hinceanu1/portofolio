import { store, store$ } from '../../shared/services/state.service';
import { Login, LoginResponse } from '../interfaces/login';
import { RegisterRequest } from '../interfaces/register-account';
import { ResetPasswordRequest } from '../interfaces/reset-password';
import * as actions from '../state/auth.actions';
import * as sel from '../state/auth.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== REGISTER ======

export function registerAccount(user: RegisterRequest, role: string) {
    // send both user and role
    store.dispatch(
        actions.registerAccountReq({ ...user, role }),
    );
}

export const registerAccountResponse$ = (): Observable<string> => store$.pipe(
    map(state => sel.REGISTER_ACC_RESP(state)),
    skipWhile( account => !account ),
    distinctUntilChanged(),
);

export const registerAccountError$ = (): Observable<string> => store$.pipe(
    map(state => sel.REGISTER_ACC_ERROR(state)),
    skipWhile(error => error === undefined),
    distinctUntilChanged(),
);

// ====== CONFIRM ACCOUNT ======

export function confirmAccount(token: string) {
    store.dispatch(
        actions.confirmAccountReq(token),
    );
}

export const confirmAccount$ = (): Observable<string> => store$.pipe(
    map(state => sel.CONFIRM_ACCOUNT_RESP(state)),
    skipWhile( confirm => !confirm ),
    distinctUntilChanged(),
);

export const confirmAccountError$ = (): Observable<string> => store$.pipe(
    map(state => sel.CONFIRM_ACCOUNT_ERROR(state)),
    skipWhile(error => error === undefined),
    distinctUntilChanged(),
);

// ====== FORGOT PASSWORD ======

export function forgotPassword(email: string) {
    store.dispatch(
        actions.forgotPasswordReq(email),
    );
}

export const forgotPasswordResponse$ = (): Observable<string> => store$.pipe(
    map(state => sel.FORGOT_PASS_RESPONSE(state)),
    skipWhile(message => message === undefined),
    distinctUntilChanged(),
);

export const forgotPasswordError$ = (): Observable<string> => store$.pipe(
    map(state => sel.FORGOT_PASS_ERROR(state)),
    skipWhile(error => error === undefined),
    distinctUntilChanged(),
);

// ====== RESET PASSWORD ======

export function resetPassword(resetPasswordRequest: ResetPasswordRequest) {
    store.dispatch(
        actions.resetPasswordReq(resetPasswordRequest),
    );
}

export const resetPassword$ = (): Observable<string> => store$.pipe(
    map(state => sel.RESET_PASSWORD_RESPONSE(state)),
    skipWhile( reset => !reset ),
    distinctUntilChanged(),
);

export const resetPasswordError$ = (): Observable<string> => store$.pipe(
    map(state => sel.RESET_PASSWORD_ERROR(state)),
    skipWhile(error => error === null),
    distinctUntilChanged(),
);

// ====== LOGIN ======

export function login(credentials: Login) {
    store.dispatch(
        actions.login(credentials),
    );
}

export function signOut() {
    store.dispatch(
        actions.signOut(),
    );
}

export const loginResponse$ = (): Observable<LoginResponse> => store$.pipe(
    map(state => sel.LOGIN_RESPONSE(state)),
    skipWhile(login => login === undefined),
    distinctUntilChanged(),
);

export const loginError$ = (): Observable<string> => store$.pipe(
    map(state => sel.LOGIN_ERROR(state)),
    skipWhile(response => response === undefined),
    distinctUntilChanged(),
);

// ====== RECONFIRM ACCOUNT ======

export function reconfirmAccount(username: string) {
    store.dispatch(
        actions.login_reconfirm(username),
    );
}

export const reconfirmAccount$ = (): Observable<string> => store$.pipe(
    map(state => sel.RECONFIRM_MESSAGE(state)),
    skipWhile( reconfirm => !reconfirm ),
    distinctUntilChanged(),
);
