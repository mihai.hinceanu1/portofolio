import { MailHasBenSent } from '../../components/forms/mail-has-been-sent/mail-has-been-sent';
import { AuthPagesLayout } from '../auth-pages-layout/auth-pages-layout';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps { }

interface State { }

class _RegisterFeedbackPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        return (
            <AuthPagesLayout>
                <MailHasBenSent registerSuccess={true} />
            </AuthPagesLayout>
        );
    }
}

export const RegisterFeedbackPage = withRouter<Props, any>(_RegisterFeedbackPage);