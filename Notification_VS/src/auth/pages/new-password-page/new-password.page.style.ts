import styled from 'styled-components/native';

export const NewPasswordPage = styled.KeyboardAvoidingView`
    justify-content: flex-start;
    align-items: center;
`;