import * as div from './new-password.page.style';
import { NewPasswordForm } from '../../components/forms/new-password-form/new-password-form';
import { SelectAuthAction } from '../../components/select-auth-action/select-auth-action';
import { AuthPagesLayout } from '../auth-pages-layout/auth-pages-layout';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {}

interface State { }

/** Used when clicked the email link received after forgot password */
export class _NewPasswordPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {

        return (
            <AuthPagesLayout>
                <div.NewPasswordPage data-cy='new-password-page'>

                    {/* New Password */}
                    <NewPasswordForm />

                    <SelectAuthAction current={'new-password'} />

                </div.NewPasswordPage>
            </AuthPagesLayout>
        );
    }
}

export const NewPasswordPage = withRouter(_NewPasswordPage);
