import * as div from './forgot-password.page.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { ForgotPasswordForm } from '../../components/forms/forgot-password-form/forgot-password-form';
import { SelectAuthAction } from '../../components/select-auth-action/select-auth-action';
import { AuthPagesLayout } from '../auth-pages-layout/auth-pages-layout';
import * as React from 'react';

interface Props { }

interface State { }

export class ForgotPasswordPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        return (
            <AuthPagesLayout>

                <div.Image>
                    <FitImage imgPath={'/auth/EniacWithKey_GIF.gif'} />
                </div.Image>

                {/* Forgot Pass */}
                <ForgotPasswordForm />

                <SelectAuthAction current={'forgot-password'} />

            </AuthPagesLayout>
        );
    }
}