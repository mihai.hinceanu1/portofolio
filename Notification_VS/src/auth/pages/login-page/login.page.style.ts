import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Image = styled.View`
    width: 100%;
    height: 100px;
`;

export const ForgotPassword = styled.TouchableOpacity`
    margin-bottom: 35px;
`;

export const ForgotPasswordText = styled.Text`
    color: ${colors.$black};
    text-decoration-color: ${colors.$grey};
    font-size: ${font.$size12}px;
    text-decoration: underline;
    margin-top: 10px;
    text-align: center;
`;
