import * as div from './login.page.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { LoginForm } from '../../components/forms/login-form/login-form';
import { SelectAuthAction } from '../../components/select-auth-action/select-auth-action';
import { AuthPagesLayout } from '../auth-pages-layout/auth-pages-layout';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps { }

interface State { }

/**
 * Login Page that extends AuthCore
 * If user is not authenticated, redirect to Login
 */
class _LoginPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <AuthPagesLayout data-cy='login-page'>
                {/** Eniac GIF */}
                <div.Image data-cy='eniac-gif'>
                    <FitImage imgPath={'/auth/eniacHi2.gif'} />
                </div.Image>

                {/** Login Form */}
                <LoginForm />

                {/** Forgot Password Link */}
                <div.ForgotPassword data-cy='forgot-password'
                    onPress={() => this.onForgotPasswordPress()}>
                    <div.ForgotPasswordText>
                        Forgot Password?
                    </div.ForgotPasswordText>
                </div.ForgotPassword>

                <SelectAuthAction current={'login'} />
            </AuthPagesLayout>
        );
    }

    private onForgotPasswordPress() {
        const { history } = this.props;

        history.push('/auth/forgot-password');
    }
}

export const LoginPage = withRouter<Props, any>(_LoginPage);
