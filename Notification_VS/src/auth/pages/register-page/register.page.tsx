import { RegisterForm } from '../../components/forms/register-form/register-form';
import { SelectAuthAction } from '../../components/select-auth-action/select-auth-action';
import { AuthPagesLayout } from '../auth-pages-layout/auth-pages-layout';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps { }

interface State { }

class _RegisterPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        return (
            <AuthPagesLayout>

                {/* Register */}
                <RegisterForm />

                <SelectAuthAction current={'register'} />

            </AuthPagesLayout>
        );
    }
}

export const RegisterPage = withRouter<Props, any>(_RegisterPage);
