import { ConfirmAccountForm } from '../../components/forms/confirm-account-form/confirm-account-form';
import { SelectAuthAction } from '../../components/select-auth-action/select-auth-action';
import { AuthPagesLayout } from '../auth-pages-layout/auth-pages-layout';
import * as React from 'react';

interface Props { }

interface State { }

export class ConfirmAccountPage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        return (
            <AuthPagesLayout>

                {/* Confirm account */}
                <ConfirmAccountForm />

                <SelectAuthAction current={'confirm-account'} />

            </AuthPagesLayout>
        );
    }
}