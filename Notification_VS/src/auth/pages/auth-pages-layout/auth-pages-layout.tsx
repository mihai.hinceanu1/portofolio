import * as div from './auth-pages-layout.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import * as React from 'react';

interface Props {}
interface State {}

/**
 * Common wrapper for all auth pages, all of them share the same page layout.
 * It is extended by any page from auth module, even if separate login, register etc. pages are defined.
 * In the future, each page from this module could be designed in their own way.
 */
export class AuthPagesLayout extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
    }

    public render() {
        const { children } = this.props;

        return (
            <div.AuthPagesLayout data-cy='auth-pages-layout'
                behavior='padding'
                enabled={true}>

                {/** Background Image + Darker effect */}
                <div.BackgroundImage data-cy='background-image' />
                <div.DarkBackgroundOverlay data-cy='dark-background-overlay' />

                {/** Center Box */}
                <div.CenterBox data-cy='center-box'>

                    {/** VSC Logo */}
                    <div.Logo data-cy='logo'>
                        <FitImage imgPath='/header/logo.png' />
                    </div.Logo>

                    {/* Dynamic Content */}
                    {children}

                </div.CenterBox>
            </div.AuthPagesLayout>
        );
    }
}
