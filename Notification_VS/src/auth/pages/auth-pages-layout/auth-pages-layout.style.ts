import { MOBILE_WIDTH } from '../../../shared/constants/adaptive.const';
import { colors } from '../../../shared/style/colors';
import { BOX_SHADOW_CSS } from '../../../shared/style/shadow.style';
import backgroundImage from '../../assets/login-background.png';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
// import { AUTH_BOX_SIZE } from '../../constants/auth-entry-page.const';

const { width } = Dimensions.get('window');

export const AuthPagesLayout = styled.KeyboardAvoidingView`
    position: relative;
    /* flex: 1; */
    width: 100%;
    height: 100%;
    align-items: center;
    ${() => authPagesLayoutMobile()};
    justify-content: center;
`;

export const BackgroundImage = styled.View`
    position: absolute;
    width: 100%;
    height: 100%;
    ${() => getWebBackground()};
`;

export const DarkBackgroundOverlay = styled.View`
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.3);
`;

export const CenterBox = styled.View`
    align-items: center;
    justify-content: space-around;
    background-color: ${colors.$white};
    ${() => centerBoxAdaptive()};
`;

export const Logo = styled.View`
    width: 100%;
    height: 40px;
    margin-top: 20px;
`;

// ===== UTILS =====

const authPagesLayoutMobile = () => {
    if (MOBILE_WIDTH >= width) {
        return `
            width: 100%;
            height: 100%;
        `;
    }

    return '';
};

const centerBoxAdaptive = () => {
    if (MOBILE_WIDTH >= width) {
        // we are on mobile
        return `
            width: 100%;
            height: 100%;
        `;
    }

    // web only styles
    return `
        ${BOX_SHADOW_CSS};
        border-radius: 5px;
    `;
};

const getWebBackground = () => {
    if (MOBILE_WIDTH < width) {
        return `
            background-image:url(${backgroundImage});
            background-position: center center;
            background-repeat: no-repeat;
            z-index: 0;
        `;
    }

    return '';
};
