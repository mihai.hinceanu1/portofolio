import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { LearningMapPage } from '../pages/learning-map-page/learning-map.page';
import * as React from 'react';
import { Route } from 'react-router';

/** Learning Map Routes */
export const learningMapRoutes = (loginResponse: LoginResponse) => {
    return [
        <Route exact={true} path='/learning-map/' key='learning-map'
            render={props => renderPage(props, true, '/auth/login', LearningMapPage, loginResponse)} />,

        <Route exact={true} path='/learning-map/:domainPath' key='learning-map-domain'
            render={props => renderPage(props, true, '/auth/login', LearningMapPage, loginResponse)} />,

        <Route exact={true} path='/learning-map/:domainPath/:categoryPath' key='learning-map-domain-category'
            render={props => renderPage(props, true, '/auth/login', LearningMapPage, loginResponse)} />
    ];
};