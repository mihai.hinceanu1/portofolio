import { INanolessonModal } from '../../maps/interfaces/entity';
import { GetLearningMapCategoryBranchEntitiesReq, IsoPosition, LearningMapRawCategoryBranch } from '../../maps/interfaces/maps';
import * as actions from '../../maps/state/maps.actions';
import * as sel from '../../maps/state/maps.selectors';
import { store, store$ } from '../../shared/services/state.service';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export const globalMapRawCategoryBranch$ = (): Observable<LearningMapRawCategoryBranch> => store$.pipe(
    map(state => sel.GLOBAL_MAP_RAW_CATEGORY_BRANCH(state)),
    skipWhile(globalMapRaw => !globalMapRaw),
    distinctUntilChanged(),
);

export function getLearningMapRawCategoryBranch(req: GetLearningMapCategoryBranchEntitiesReq) {
    store.dispatch(
        actions.getLearningMapRawCategoryBranchReq(req),
    );
}

/** For all - used in catalog */
export const globalMapRawAllCategoryBranches$ = (): Observable<LearningMapRawCategoryBranch[]> => store$.pipe(
    map(state => sel.GLOBAL_MAP_RAW_ALL_CATEGORY_BRANCHES(state)),
    skipWhile(globalMapRaw => !globalMapRaw),
    distinctUntilChanged(),
);

export const domainLevelById$ = (): Observable<TaxonomyLevel> => store$.pipe(
    map(state => sel.GLOBAL_MAP_DOMAIN_LEVEL(state)),
    skipWhile(domainLevel => !domainLevel),
    distinctUntilChanged(),
);

/** Used to also trigger epic to bring all the raw data from children categories */
export function getDomainLevelByIdEpicTriggerRawCategories(req: string) {

    store.dispatch(
        actions.getLearningMapDomainLevelReq(req),
    );
}

// ====== Selected Domain Id ======

export const selectedDomainId$ = (): Observable<string> => store$.pipe(
    map(state => sel.SELECTED_DOMAIN_ID(state)),
    skipWhile(domainId => !domainId),
    distinctUntilChanged(),
);

export function setSelectedDomainId(domainId: string) {
    store.dispatch(
        actions.setSelectedDomainId(domainId),
    );
}

// ====== Selected Category Id ======

export const selectedCategoryId$ = (): Observable<string> => store$.pipe(
    map(state => sel.SELECTED_CATEGORY_ID(state)),
    skipWhile(selectedCategoryId => !selectedCategoryId),
    distinctUntilChanged(),
);

export function setSelectedCategoryId(categoryId: string) {
    store.dispatch(
        actions.setSelectedCategoryId(categoryId),
    );
}

// ====== SELECTED CATEGORY NAME ======

export const selectedCategoryName$ = (): Observable<string> => store$.pipe(
    map(state => sel.SELECTED_CATEGORY_NAME(state)),
    skipWhile(categoryName => categoryName === undefined),
    distinctUntilChanged(),
);

export function setSelectedCategoryName(categoryName: string) {
    store.dispatch(
        actions.setSelectedCategoryName(categoryName),
    );
}

// ====== Level Counter ======

export const LevelsCounter$ = (): Observable<number> => store$.pipe(
    map(state => sel.LEVELS_COUNTER(state)),
    skipWhile(levelsCounter => levelsCounter === undefined),
    distinctUntilChanged(),
);

export function setLevelsCounter(counter: number) {
    store.dispatch(
        actions.setLevelsCounter(counter),
    );
}

export const mapStartIsoPosition$ = (): Observable<IsoPosition> => store$.pipe(
    map(state => sel.MAP_START_ISO_POSITION(state)),
    skipWhile(startPosition => !startPosition),
    distinctUntilChanged(),
);

/** A way to reset the starting position of the scene (first category branch) when selecting a domain from the menu */
export function setMapStartIsoPosition(position: IsoPosition) {
    store.dispatch(
        actions.setMapStartIsoPosition(position),
    );
}

// REVIEW why do we need nano lesson mobile
/** Mobile: Nanolesson modal state and data */
export const nanolessonModalMobile$ = (): Observable<INanolessonModal> => store$.pipe(
    map(state => sel.NANOLESSON_STATE(state)),
    skipWhile(nanolessonModal => nanolessonModal === undefined),
    distinctUntilChanged(),
);

export function setNanolessonModal(nanolessonModalConfig: INanolessonModal) {
    store.dispatch(
        actions.setNanolessonModal(nanolessonModalConfig),
    );
}