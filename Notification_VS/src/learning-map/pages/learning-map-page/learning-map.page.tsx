import * as div from './learning-map.page.style';
import { LessonsCatalogModal } from '../../../lessons-catalog/components/lessons-catalog-modal/lessons-catalog-modal';
import { addTopicsCatalogModal$ as lessonsCatalogModalVisible$ } from '../../../lessons-catalog/services/lessons-catalog.service';
import { IsometricMap, Props as IsometricMapProps } from '../../../maps/components/isometric-map/isometric-map';
import { LearningMapHoverMenus } from '../../../maps/components/learning-map-hover-menus/learning-map-hover-menus';
import { LearningMapRawCategoryBranch } from '../../../maps/interfaces/maps';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface Params { }

interface Props extends RouteComponentProps<Params> { }

export interface State {
    lessonsCatalogIsVisible: boolean;
    categories: LearningMapRawCategoryBranch[];
    categoryId: string;
}

/**
 * Global Map Component
 * The global map showcases the entire knowledge tree from a certain domain.
 * The entire concept of the global map was born from the desire to give to the student
 * total freedom to explore. Unlike Courses that split in chapters the global map is a continuos set
 * of lesson and projects that are showcases from the best content available.
 * Using the global map the student can understand several things at once.
 * What are the most important subjects to study.
 * How much knowledge he has accumulated already.
 * Whether a certain domain is worth pursuing by analyzing the heatmaps and statistics.
 * How much he knows already, less impostor syndrome.
 * What are the study goals necessary for him to reach a certain level of experience.
 * How certain topics relate to each others.
 *
 * Contains:
 *  - Isometric map
 *  - Lesson Catalog Modal
 *  - Learning map hover menus
 */
class _LearningMapPage extends React.Component<Props, State> {

    public destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            lessonsCatalogIsVisible: false,
            categories: [] as LearningMapRawCategoryBranch[],
            categoryId: null,
        };
    }

    public render() {
        let globalMapCfg: IsometricMapProps['config'] = {};
        const { lessonsCatalogIsVisible } = this.state;

        return (
            <div.LearningMapPage data-cy='learning-map-page' style={{ overflow: 'hidden' }}>

                <LearningMapHoverMenus />

                {/* Learning Map */}
                <IsometricMap config={globalMapCfg} />

                {
                    // Lessons Catalog
                    lessonsCatalogIsVisible &&
                    <LessonsCatalogModal />
                }
            </div.LearningMapPage>
        );
    }

    public componentDidMount() {
        this.subscribeToLessonsCatalogModal();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToLessonsCatalogModal() {
        lessonsCatalogModalVisible$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(lessonsCatalogIsVisible =>
                this.setState({ lessonsCatalogIsVisible })
            );
    }
}

export const LearningMapPage = withRouter<Props, React.ComponentType<Props>>(_LearningMapPage);
