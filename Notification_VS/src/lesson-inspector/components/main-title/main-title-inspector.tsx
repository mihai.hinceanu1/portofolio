import * as div from './main-title-inspector.style';
import { LessonInspectorCore } from '../lesson-inspector-core/lesson-inspector-core';
import * as React from 'react';

interface Props {}
interface State {}

/**
 * Lesson Title Inspector
 * Cannot be deleted from inspector itself.
 *
 * ICON is displayed in the Lesson Page next to the title
 * SHORT TITLE ...
 * TITLE is the Lesson Name
 * DESCRIPTION is extra info for Lesson
 */
export class MainTitleInspector extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        return (
            <LessonInspectorCore title='Main Title Element' icon='asdfa'>
                <div.MainTitle>
                    Hello Main Title
                </div.MainTitle>
            </LessonInspectorCore>
        );
    }
}
