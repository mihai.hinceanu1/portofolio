import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

export const Inspector = styled.View`
    width: 250px;
`;

export const Header = styled.View`
    height: 100px;
    width: 100%;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    border-bottom-width: 1px;
    border-bottom-color: grey;
`;

export const TitleFrame = styled.View`
    align-items: center;
    justify-content: center;
    flex-direction: row;
`;

export const Title = styled.Text`
    font-size: 14px;
    margin-left: 5px;
`;

export const TitleIcon = styled.View`
`;

export const ExtraFrame = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    width: 50px;
`;

export const InspectorContent = styled.View`
    display: flex;
`;

// ===== SVG =====

export const iconSvg = (svgPath: string, fill?: string): JSX.Element => {
    let { width, height } = pathBoundingBox(svgPath);

    return (
        <Svg fill='none' width={width + 1} height={height + 1}>
            <Path d={svgPath} fill={fill || 'none'} />
        </Svg>
    );
};
