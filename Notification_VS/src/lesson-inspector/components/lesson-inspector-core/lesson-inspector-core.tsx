import * as div from './lesson-inspector-core.style';
import { colors } from '../../../shared/style/colors';
import * as assets from '../../assets/lesson-inspector.assets';
import * as React from 'react';

interface Props {
    // Title of the element that is shown in inspector's header
    title: string;

    // Element icon shown in the left part of title
    icon: string;

    /**
     * There are 2 types of elements
     * 1. Mandatory (Main Title Element) - lock icon
     * 2. Not mandatory (Layout, Title, Video etc..) - bin icon
     *
     * The mandatory element cannot be deleted from the inspector.
     */
    blocked?: boolean;

    /**
     * Short description of the element
     * Extra info about current element can be provided too
     */
    explanation?: string;
}
interface State {}

/**
 * Because VSC can have a lot of inspectors, splitting the core inspector into a separate component is one good solution
 * Each inspector is different by content, not wrapper
 * Core defines the structure of any other type of inspector (Main Title, Layout etc...)
 * The other inspectors are using Core
 * Core must be independent and should have the ability to be rendered as a standalone component if needed
 */
export class LessonInspectorCore extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { title, children } = this.props;

        return (
            <div.Inspector data-cy='inspector'>

                {/** Common header for each inspector */}
                <div.Header data-cy='inspector-header'>
                    <div.TitleFrame data-cy='inspector-title-frame'>

                        {/** Specific Icon */}
                        <div.TitleIcon data-cy='inspector-title-icon'>
                            {div.iconSvg(assets.TITLE, colors.$black)}
                        </div.TitleIcon>

                        {/** Title */}
                        <div.Title data-cy='inspector-title'>
                            {title}
                        </div.Title>
                    </div.TitleFrame>

                    <div.ExtraFrame>
                        {div.iconSvg(assets.INFO, colors.$black)}
                        {div.iconSvg(assets.LOCK, colors.$black)}
                    </div.ExtraFrame>
                </div.Header>

                {/** Content Wrapper */}
                <div.InspectorContent data-cy='inspector-content'>
                    {children}
                </div.InspectorContent>
            </div.Inspector>
        );
    }
}
