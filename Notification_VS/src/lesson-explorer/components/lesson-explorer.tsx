import * as div from './lesson-explorer.style';
import { Lesson } from '../../lessons/interfaces/lesson';
import * as React from 'react';
import { Text } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    lesson: Lesson;
}
interface State {}

/**
 * Renders a list of each topic from current lesson
 * Each topic section renders blocks
 */
class _LessonExplorer extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { lesson } = this.props;

        return (
            <div.Explorer>
                <Text>{lesson._id}</Text>
            </div.Explorer>
        );
    }

    public componentDidMount() {
    }
}

export const LessonExplorer = withRouter<Props, any>(_LessonExplorer);
