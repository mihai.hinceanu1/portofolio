import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

export const DashboardPage = styled.ScrollView`
    width: 100%;
    height: 100%;
`;

export const scroll = StyleSheet.create({
    contentContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }
});

export const MainHeader = styled.Text`
    color: ${colors.$blue};
    font-size: ${font.$size40}px;
    font-weight: 800;
    text-align: left;
    margin-bottom: 40px;
`;

export const SecondHeader = styled.Text`
    font-size: ${font.$size22}px;
    font-weight: 800;
    text-align: left;
    margin-bottom: 40px;
`;

export const MainPages = styled.View`
    margin-bottom: 40px;
`;

export const LogoutButton = styled.TouchableOpacity`
    width: 300px;
    height: 50px;
    border: 1px solid black;
    align-items: center;
    justify-content: center;
`;

export const LogoutText = styled.Text`
    font-size: ${font.$size14}px;
`;
