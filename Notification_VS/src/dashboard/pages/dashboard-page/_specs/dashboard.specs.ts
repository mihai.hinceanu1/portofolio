/// <reference types="cypress" />

describe('Dashboard Page Tests', () => {
    /**
     * In order to get to our homepage the user must log in first.
     */
    before('login', () => {
        cy.login();
    });

    it('Homepage loads successfully', () => {
        cy.url().should('equal', 'http://localhost:3000/');
    });

    it('Has button linking to LESSON PAGE', () => {
        checkRedirect(0, 'http://localhost:3000/lesson');
    });

    it('Has button linking to LEARNING MAP', () => {
        checkRedirect(1, 'http://localhost:3000/learning-map');
    });

    it('Has admin section separator', () => {
        cy.get('[data-cy=header]').eq(0).should('include.text', 'Admin');
    });

    it('Has button linking to LESSON EDITOR', () => {
        checkRedirect(2, 'http://localhost:3000/admin/lesson-editor');
    });

    it('Has button linking to SOURCE CODE', () => {
        checkRedirect(3, 'http://localhost:3000/admin/source-code');
    });

    /** <!>Once everything is merged there should be a link to package versions list. */
    // it('Has button linking to PACKAGE VERSIONS LIST', () => {
    //     checkRedirect(4, 'http://localhost:3000/admin/');
    // });

    it('Has button linking to TAXONOMY', () => {
        checkRedirect(5, 'http://localhost:3000/admin/taxonomy');
    });

    it('Has button linking to USER SETTINGS', () => {
        checkRedirect(6, 'http://localhost:3000/user/');
    });

    /** <!>This should be updated to clear the local storage then redirect to login. */
    // it('Has button linking to LOGIN PAGE', () => {
    //     checkRedirect(7, 'http://localhost:3000/auth/login');
    // });

    it('Has button linking to COMPONENTS CATALOG', () => {
        checkRedirect(8, 'http://localhost:3000/components-catalog/home');
    });
});

// ====== UTILS ======

function checkRedirect(buttonIndex, expectedUrl) {
    cy.get('[data-cy=page-button]').eq(buttonIndex).click();
    cy.url().should('include', expectedUrl);
    /** Go back to previous page */
    cy.go('back');
}