import * as div from './dashboard.page.style';
import { signOut } from '../../../auth/services/auth.service';
import { PageButton } from '../../../shared/components/page-button/page-button';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Params { }

interface Props extends RouteComponentProps<Params> { }

interface State { }

class _DashboardPage extends React.Component<Props, State> {

    public render() {

        return (
            <div.DashboardPage data-cy='dashboard-page'
                contentContainerStyle={div.scroll.contentContainer}>

                {/* Header */}
                <div.MainHeader data-cy='main-header'>
                    Dashboard
                </div.MainHeader>

                {/* Links */}
                <div.MainPages data-cy='main-pages'>

                    {/* Lesson */}
                    <PageButton icon='/icons/topicWhite.png'
                        text='Lesson'
                        redirectTo='/lesson/date-picker' />

                    {/* Learning map */}
                    <PageButton icon='/icons/mapWhite.png'
                        text='Learning Map'
                        redirectTo='/learning-map' />

                    <div.SecondHeader data-cy='header'>
                        Admin
                    </div.SecondHeader>

                    {/* Lesson editor */}
                    <PageButton icon='/taxonomy/new/adminTopicWhite.png'
                        text='Lesson Editor'
                        redirectTo='/admin/lesson-editor/date-picker' />

                    {/** Nanolessons */}
                    <PageButton icon='/taxonomy/new/adminTopicWhite.png'
                        text='Nanolesson Editor'
                        redirectTo='/admin/nanolessons' />

                    {/** Source code */}
                    <PageButton icon='/taxonomy/new/adminTopicWhite.png'
                        text='Code Editor Source Code'
                        redirectTo='/admin/code-editor/raw-materials-price-controller-1-0' />

                    {/* Source code */}
                    {/* <PageButton icon='/taxonomy/new/codeSourceWhite.png'
                        text='Source Code'
                        redirectTo='admin/source-code/date-picker/packages' /> */}

                    {/* Taxonomy */}
                    <PageButton icon='/taxonomy/new/levelEditWhite.png'
                        text='Taxonomy'
                        redirectTo='/admin/taxonomy' />

                    {/** Packages list */}
                    <PageButton icon='/taxonomy/new/codeSourceWhite.png'
                        text='Packages List'
                        redirectTo='admin/packages-list' />

                    {/** Package versions */}
                    <PageButton icon='/taxonomy/new/codeSourceWhite.png'
                        text='Packages Versions'
                        redirectTo='/admin/package-versions/the-ethical-hacking-guide-with-kali-linux' />

                    {/* User settings page */}
                    <PageButton icon='/taxonomy/new/levelEditWhite.png'
                        text='User'
                        redirectTo='/user/admin' />

                    <div.SecondHeader data-cy='header'>
                        Utils
                    </div.SecondHeader>

                    {/* Login */}
                    <PageButton icon='/taxonomy/new/levelEditWhite.png'
                        text='Login'
                        redirectTo='/auth/login' />

                    {/* Components catalog */}
                    <PageButton icon='/taxonomy/new/levelEditWhite.png'
                        text='Components Catalog'
                        redirectTo='/components-catalog/home' />

                    <div.LogoutButton onPress={() => this.logout()}>
                        <div.LogoutText>Logout</div.LogoutText>
                    </div.LogoutButton>

                </div.MainPages>

            </div.DashboardPage>
        );
    }

    private logout() {
        const { history } = this.props;

        signOut();
        history.push('/auth/login');
    }

}

export const DashboardPage = withRouter(_DashboardPage);
