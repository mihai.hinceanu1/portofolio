import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { DashboardPage } from '../pages/dashboard-page/dashboard.page';
import * as React from 'react';
import { Route } from 'react-router';

/** Dashboard Routes */
export const dashboardRoutes = (loginResponse: LoginResponse) => {
    return (
        <Route exact={true} path='/'
            render={props => renderPage(props, true, '/auth/login', DashboardPage, loginResponse)} />
    );
};