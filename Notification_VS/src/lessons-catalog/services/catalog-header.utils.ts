import { Filter, HeaderConfig } from '../../shared/interfaces/page-header';
import { colors } from '../../shared/style/colors';

export const getLessonsCatalogHeaderCfg = (searchBarValue: string, onChange: (text: string) => void, onSubmit: () => void, addFilter: (filter: Filter) => void): HeaderConfig => {
    return {
        title: 'Backend',
        textColor: colors.$white,
        description: 'Learn to build reliable applications using our back end guide.',
        options: [
            {
                _id: 'lessons',
                label: 'Lessons',
                callback: () => console.log('Get Lessons'),
            },
            {
                _id: 'projects',
                label: 'Projects',
                callback: () => console.log('Get Projects'),
            }
        ],
        searchBar: {
            value: searchBarValue,
            onChange: (text) => onChange(text),
            onSubmit: () => onSubmit(),
            clearSearch: () => console.log('Clear'),
        },
        filterBar: {
            filters: [
                {
                    _id: 'algorithmic',
                    label: 'Algorithmic',
                    callback: (filter) => addFilter(filter),
                },
                {
                    _id: 'technologies',
                    label: 'Technologies',
                    callback: (filter) => addFilter(filter),
                }
            ],
            selectFilter: (filter) => addFilter(filter),
        }
    };
};
