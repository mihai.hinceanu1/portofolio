import { store, store$ } from '../../shared/services/state.service';
import * as actions from '../state/lessons-catalog.actions';
import * as sel from '../state/lessons-catalog.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export const addTopicsCatalogModal$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.TOPICS_CATALOG_MODAL_OPEN(state)),
    skipWhile(topicsCatalogModal => topicsCatalogModal === undefined),
    distinctUntilChanged(),
);

export function setTopicsCatalogModal(topicsCatalogModal: boolean) {
    store.dispatch(
        actions.setTopicsCatalogModal(topicsCatalogModal),
    );
}

export const topicsCatalogDisplayType$ = (): Observable<string> => store$.pipe(
    map(state => sel.TOPICS_CATALOG_DISPLAY_TYPE(state)),
    skipWhile(catalogDisplayType => catalogDisplayType === undefined),
    distinctUntilChanged(),
);

export function setTopicsCatalogDisplayType(catalogDisplayType: string) {
    store.dispatch(
        actions.setTopicsCatalogDisplayType(catalogDisplayType),
    );
}