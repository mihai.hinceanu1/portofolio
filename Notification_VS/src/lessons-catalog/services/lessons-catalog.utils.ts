import { LearningMapRawCategoryBranch } from '../../maps/interfaces/maps';
import { ProjectTagRaw, TopicMarkerRaw } from '../../maps/interfaces/topics-cluster-body';

export const filterDataForEachTypeOfDisplay = (selectedCategoryId: string, topics: TopicMarkerRaw[], projects: ProjectTagRaw[], categories: LearningMapRawCategoryBranch[]) => {
    let category = getCategory(selectedCategoryId, categories);

    if (!!category) {
        topics = getTopics(category, topics);
        projects = getProjects(category, projects);

        return {
            projectTopics: projects,
            topics: topics,
            latestTopics: getLatest(topics),
            latestProjects: getLatest(projects),
            favedTopics: getFaved(topics),
            favedProjects: getFaved(projects),
        };
    }
};

const getLatest = (dataSet: TopicMarkerRaw[] | ProjectTagRaw[]) => {
    let sortedData = dataSet.sort(compareDate);
    return sortedData.slice(0, 5);
};

const getFaved = (dataSet: TopicMarkerRaw[] | ProjectTagRaw[]) => {
    let sortedData = dataSet.sort(compareSentiment);
    return sortedData.slice(0, 5);
};

const getCategory = (selectedCategoryId: string, categories: LearningMapRawCategoryBranch[]) => {
    let category = categories.find(category =>
        category.levelId === selectedCategoryId
    );

    return category;
};

const getTopics = (category: LearningMapRawCategoryBranch, topics: TopicMarkerRaw[]) => {
    category.clusters.forEach(cluster => {

        cluster.topics.forEach(topic => {
            topics.push(topic);
        });
    });

    return topics;
};

const getProjects = (category: LearningMapRawCategoryBranch, projects: ProjectTagRaw[]) => {
    category.clusters.forEach(cluster => {

        cluster.projects.forEach(project => {
            projects.push(project);
        });
    });

    return projects;
};

// ====== PRIVATE ======

const compareDate = (a: TopicMarkerRaw | ProjectTagRaw, b: TopicMarkerRaw | ProjectTagRaw) => {
    if (a.created < b.created) {
        return -1;
    }
    if (a.created > b.created) {
        return 1;
    }
    return 0;
};

const compareSentiment = (a: TopicMarkerRaw | ProjectTagRaw, b: TopicMarkerRaw | ProjectTagRaw) => {
    if (a.numberOfFavorites > b.numberOfFavorites) {
        return -1;
    }
    if (a.numberOfFavorites < b.numberOfFavorites) {
        return 1;
    }
    return 0;
};