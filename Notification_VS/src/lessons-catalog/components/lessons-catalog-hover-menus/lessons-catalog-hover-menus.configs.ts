import * as icn from '../../../shared/assets/icons';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../../shared/constants/branches-menu.const';
import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../../../shared/constants/dots-menu.const';
import { BranchesMenuConfig } from '../../../shared/interfaces/branches-menu';
import { DotsMenuConfig } from '../../../shared/interfaces/dots-menu';
import { colors } from '../../../shared/style/colors';
import { setTopicsCatalogModal } from '../../services/lessons-catalog.service';

/** Topics of the current lesson */
export const toggleCatalog: BranchesMenuConfig = {
    branches: [],
    icon: {
        svgPath: icn.TOPICS_CATALOG,
    },
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
    onTogglePress: () => setTopicsCatalogModal(true),
};

// TODO Move to learning map hover menus
export const closeCatalog: BranchesMenuConfig = {
    branches: [],
    icon: {
        svgPath: icn.TOPICS_CATALOG,
        color: colors.$overlayBtnIcons,
    },
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.top,
    onTogglePress: () => setTopicsCatalogModal(false),
    color: '#fff',
};

export const filterMenu: DotsMenuConfig = {
    trigger: {
        _id: 'trigger',
        icon: {
            assetsSrc: 'svg',
            svgData: icn.FILTER,
        }
    },
    dots: [
        {
            _id: 'beginner',
            label: 'Begginer',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.BEGINNER,
            },
        },
        {
            _id: 'expert',
            label: 'Expert',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.EXPERT,
            },
        },
        {
            _id: 'all',
            label: 'All',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.ALL,
            },
        },
        {
            _id: 'popular',
            label: 'Popular',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.POPULAR,
            },
        },
        {
            _id: 'latest',
            label: 'Latest',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.LATEST,
            },
        },
    ],
    txtColor: 'white',
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.top
};

export const userMenu: DotsMenuConfig = {
    trigger: {
        _id: 'user',
        icon: {
            assetsSrc: 'image',
            imagePath: '/thumbnails/batman.png'
        },
    },
    dots: [
        {
            _id: 'my-profile',
            label: 'My Profile',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.USER,
            },
        },
        {
            _id: 'dashboard',
            label: 'Dashboard',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.DASHBOARD,
            },
        },
        {
            _id: 'goals',
            label: 'Goals',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.GOALS,
            },
        },
        {
            _id: 'settings',
            label: 'Settings',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.SETTINGS,
            },
        },
        {
            _id: 'logout',
            label: 'Logout',
            icon: {
                assetsSrc: 'svg',
                svgData: icn.LAYOUT,
            },
        }
    ],
    txtColor: 'white',
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom
};