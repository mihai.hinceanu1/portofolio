import * as cfg from './lessons-catalog-hover-menus.configs';
import { getAppLogo } from '../../../lessons/components/lesson-hover-menus/lesson-hover-menus.utils';
import { learningMapDomainsCfg } from '../../../maps/components/learning-map-hover-menus/learning-map-hover-menus.utils';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { DotsMenu } from '../../../shared/components/dots-menu/dots-menu';
import { colors } from '../../../shared/style/colors';
import * as hover from '../../../shared/style/hover-menus.style';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
// import { Subject } from 'rxjs';
// import { takeUntil } from 'rxjs/operators';
// import { NewTaxonomyLevelTree } from '../../../taxonomy/interfaces/taxonomy-level';
// import { allLevelsTree$, getAllLevelsTree } from '../../../taxonomy/services/taxonomy.service';

interface Props extends RouteComponentProps<never> { }

interface State {
    width: number;
    domains: any[];
}

export class _LessonsCatalogHoverMenus extends React.Component<Props, State> {

    // private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            width: Dimensions.get('window').width,
            domains: [],
        };
    }

    public render() {
        const { width, domains } = this.state;

        return (
            <hover.Menus data-cy='lessons-catalog-hover-menus'
                pointerEvents='box-none'
                onLayout={(e: LayoutChangeEvent) => this.updateWidth(e)}>

                {/* Learning Map Domains */}
                <BranchesMenu config={learningMapDomainsCfg(domains, true)}
                    overrides={hover.overrideTopLeftMenu(width)} />

                {/* Logo */}
                <hover.AppLogo width={width}>
                    {getAppLogo(colors.$white)}
                </hover.AppLogo>

                {/* Lesson Topics */}
                <BranchesMenu config={cfg.closeCatalog}
                    overrides={hover.overrideBottomLeftMenu(width)} />

                {/* User Menu */}
                <DotsMenu config={cfg.userMenu}
                    overrides={hover.overrideTopRightMenu(width)} />

                {/* Layers Menu */}
                <DotsMenu config={cfg.filterMenu}
                    overrides={hover.overrideBottomRightMenu(width)} />

            </hover.Menus>
        );
    }

    public componentDidMount() {
        // this.subscribeToLevelsTree();
        // getAllLevelsTree();
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    // private subscribeToLevelsTree() {
    //     allLevelsTree$().pipe(
    //         takeUntil(this.destroyed$),
    //     )
    //         .subscribe(levelsTree => {

    //             this.setState({
    //                 domains: this.getDomains(levelsTree),
    //             });
    //         });
    // }

    // private getDomains(levelsTree: NewTaxonomyLevelTree) {
    //     let domains = [] as NewTaxonomyLevelTree[];

    //     if (!!levelsTree.childrenLevelsIds) {
    //         levelsTree.childrenLevelsIds.forEach((level: NewTaxonomyLevelTree) => {
    //             domains.push(level);
    //         });
    //     }
    //     return domains;
    // }

}

export const LessonsCatalogHoverMenus = withRouter(_LessonsCatalogHoverMenus);