import { LessonCardConfig } from '../../../interfaces/lesson-card';

export enum LABELS_TYPE {
    'BestSeller',
    'EditorsPick',
    'HighestRated',
}

export const CARD_CONFIG: LessonCardConfig = {
    name: 'Controlling CPU Heating by using Process Hacker',
    authors: [
        'Roboto Sanso',
    ],
    image: '/auth/eniacWithConfetti.png',
    description: 'The skills you need to become a Bl Analyst - Statistics, Database theory, SQL, Tableau de branza cu smantana',
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
    label: LABELS_TYPE.HighestRated,
};