import { LABELS_TYPE } from './constants/lesson-card.const';
import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { getShadow } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

export const LessonCard = styled.TouchableOpacity<{ override: string } & ReactWebAttributes>`
    width: 100%;
    display: flex;
    flex-direction: column;
    background-color: white;
    ${getShadow()};
    ${props => props.override};
`;

export const CardImage = styled.View`
    width: 100%;
    height: 175px;
    background-color: white;
`;

export const Image = styled.Image`
    width: 100%;
    height: 100%;
    resize-mode: cover;
`;

export const Label = styled.View<{ label: LABELS_TYPE }>`
    height: 35px;
    background-color: ${ props => getColorFromLabel(props.label)};
    position: absolute;
    top: 30px;
    left: -10px;
    padding-left: 20px;
    padding-right: 20px;
    align-items: center;
    justify-content: center;
    text-transform: uppercase;
    ${getShadow()};
`;

export const Preview = styled.TouchableOpacity<ReactWebAttributes>`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    position: absolute;
    z-index: 10;
    top: 140px;
    left: 10px;
    width: auto;
    height: auto;
`;

export const PreviewText = styled.Text`
    color: white;
    margin-left: 5px;
    font-weight: 500;
    font-size: ${font.$size14}px;
    line-height: ${font.$size14}px;
`;

export const LabelText = styled.Text`
    font-size: 15px;
    font-weight: 600;
    color: white;
    ${getShadow()};
`;

export const CardInfo = styled.View`
    width: 100%;
    height: 230px;
    padding: 25px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

export const Title = styled.Text`
    font-weight: 700;
    font-size: 20px;
`;

export const Authors = styled.Text`
    margin-top: 10px;
    margin-bottom: 10px;
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;

export const Description = styled.Text`
    font-size: 13px;
`;

export const Footer = styled.View`
    display: flex;
    flex-direction: row;
    justify-self: flex-end;
    justify-content: space-between;
    margin-top: 35px;
`;

export const Reactions = styled.View`
    display: flex;
    flex-direction: row;
`;

export const Statistics = styled.View`
    display: flex;
    flex-direction: row;
`;

export const NumberOfReact = styled.Text`
    margin-left: 3px;
    margin-right: 10px;
    font-size: ${font.$size12}px;
    font-weight: 700;
`;

export const Updated = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;

export const Dates = styled.View`
    margin-top: 5px;
    height: 20px;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

// ====== UTILS ======

export const getColorFromLabel = (label: LABELS_TYPE) => {
    if (label === LABELS_TYPE.BestSeller) {
        return colors.$yellow;
    } else if (label === LABELS_TYPE.HighestRated) {
        return colors.$red;
    } else {
        return colors.$blue;
    }
};