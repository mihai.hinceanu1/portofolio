export const fullLessonCard = `
<LessonCard config={CARD_CONFIG} />

export const CARD_CONFIG: LessonCardConfig = {
    name: {customName},
    authors: [
        'Roboto Sanso',
    ],
    image: {imagePath},
    description: {customDescription},
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
    label: LabelsType['HIGHEST RATED'],
};`;

export const customTitle = `
export const CARD_CONFIG: LessonCardConfig = {
    name: {customName}
}`;

export const customAuthors = `
export const CARD_CONFIG: LessonCardConfig = {
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ]
}`;

export const customDescription = `
export const CARD_CONFIG: LessonCardConfig = {
    description: {customDescription}
}`;

export const customImage = `
export const CARD_CONFIG: LessonCardConfig = {
    image: '/auth/eniac_extraSad.gif'
}`;

export const customReactions = `
export const CARD_CONFIG: LessonCardConfig = {
    numOfAwards: 1000,
    numOfReactions: 200,
}`;

export const customUpdate = `
export const CARD_CONFIG: LessonCardConfig = {
    updated: new Date(1995, 11, 17)
}`;

export const customLabel = `
export const CARD_CONFIG: LessonCardConfig = {
    label: LabelsType.BESTSELLER,
}`;

export const callback = `
    export const CARD_CONFIG: LessonCardConfig = {
        onMouseEnter: () => customCallback(),
        onMouseLeave: () => anotherCustomCallback(),
    }
`;