import { LessonCardPage } from './lesson-card.page';
import { LessonCardConfig } from '../../../interfaces/lesson-card';
import { LABELS_TYPE } from '../constants/lesson-card.const';

export const STANDARD: LessonCardConfig = {
    name: 'Controlling CPU Heating by using Process Hacker',
    authors: [
        'Roboto Sanso',
    ],
    image: '/lesson-catalog/example8.png',
    description: 'The skills you need to become a Bl Analyst - Statistics, Database theory, SQL, Tableau. Learn The Basics on how to start your own business',
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
    label: LABELS_TYPE.HighestRated,
};

export const CUSTOM_TITLE: LessonCardConfig = {
    name: 'Angular from scratch',
    authors: [
        'Roboto Sanso',
    ],
    image: '/lesson-catalog/example1.png',
    description: 'The skills you need to become a Bl Analyst - Statistics, Database theory, SQL, Tableau. Learn The Basics on how to start your own business',
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
};

export const CUSTOM_AUTHORS: LessonCardConfig = {
    name: 'Rxjs',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example2.png',
    description: 'The skills you need to become a Bl Analyst - Statistics, Database theory, SQL, Tableau. Learn The Basics on how to start your own business',
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
};

export const CUSTOM_DESCRIPTION: LessonCardConfig = {
    name: 'Business analysis',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example3.png',
    description: 'Learn The Basics on how to start your own business. Build your true leader character from scratch by using this limited occasion',
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
};

export const CUSTOM_IMAGE: LessonCardConfig = {
    name: 'Javascript',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example4.png',
    description: 'Learn The Basics on how to start your own business. Build your true leader character from scratch by using this limited occasion',
    numOfAwards: 25,
    numOfReactions: 344,
    updated: new Date(),
};

export const CUSTOM_REACTIONS: LessonCardConfig = {
    name: 'Algorithms',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example5.png',
    description: 'Learn The Basics on how to start your own business. Build your true leader character from scratch by using this limited occasion',
    numOfAwards: 1000,
    numOfReactions: 200,
    updated: new Date(),
    label: LABELS_TYPE.EditorsPick,
};

export const CUSTOM_DATE: LessonCardConfig = {
    name: 'Backtracking',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example6.png',
    description: 'Learn The Basics on how to start your own business. Build your true leader character from scratch by using this limited occasion',
    numOfAwards: 1000,
    numOfReactions: 200,
    updated: new Date(1995, 11, 17),
    label: LABELS_TYPE.HighestRated,
};

export const CUSTOM_LABEL: LessonCardConfig = {
    name: 'Greedy Method',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example7.png',
    description: 'Learn The Basics on how to start your own business. Build your true leader character from scratch by using this limited occasion',
    numOfAwards: 1000,
    numOfReactions: 200,
    updated: new Date(1995, 11, 17),
    label: LABELS_TYPE.BestSeller,
};

export const getCallbacksConfig = (context: LessonCardPage): LessonCardConfig => ({
    name: 'Greedy Method',
    authors: [
        'Roboto Sanso',
        'Mircea Eliade',
        'Ion L. Caragiale',
    ],
    image: '/lesson-catalog/example7.png',
    description: 'Learn The Basics on how to start your own business. Build your true leader character from scratch by using this limited occasion',
    numOfAwards: 1000,
    numOfReactions: 200,
    updated: new Date(1995, 11, 17),
    label: LABELS_TYPE.BestSeller,
    onMouseEnter: () => {
        context.incrementNumOfCallbacks();
    },
    onMouseLeave: () => {
        context.incrementNumOfCallbacks();
    },
});
