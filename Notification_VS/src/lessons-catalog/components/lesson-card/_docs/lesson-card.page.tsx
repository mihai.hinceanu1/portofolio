import * as cfg from './lesson-card-page.utils';
import * as samples from './lesson-card.sample';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { LessonCard } from '../lesson-card';
import * as React from 'react';

interface LessonCardState {
    numOfCallbacks: number;
}

export class LessonCardPage extends ScrollableDocPage<LessonCardState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                numOfCallbacks: 0,
            },
        };
    }

    public renderDemoPage() {
        let { width } = this.state;
        const { numOfCallbacks } = this.state.overrides;

        return (
            <>
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Lesson Card
                    </div.OverviewTitle>
                    <div.OverviewDescription>
                        Lesson Card was created in order to highlight every lesson that takes part in our app in Lesson Catalog.
                        It Will stretch as much as its container stretches
                    </div.OverviewDescription>
                </div.Overview>

                {/* Standard Card */}
                <div.Demo data-cy='standard-card'
                    width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard Card
                        </div.DemoTitle>

                        <LessonCard config={cfg.STANDARD} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.fullLessonCard} />
                </div.Demo>

                {/* Custom Name */}
                <div.Demo data-cy='custom-name' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Name
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_TITLE} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customTitle} />
                </div.Demo>

                {/* Custom Authors */}
                <div.Demo data-cy='custom-authors' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Authors
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_AUTHORS} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customAuthors} />
                </div.Demo>

                {/* Custom Description */}
                <div.Demo data-cy='custom-description' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Description
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_DESCRIPTION} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customDescription} />
                </div.Demo>

                {/* Custom Image */}
                <div.Demo data-cy='custom-image' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Image
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_IMAGE} override={'width: 230px;'} />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customImage} />
                </div.Demo>

                {/* Custom Reactions */}
                <div.Demo data-cy='custom-reactions' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Number Of Reactions
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_REACTIONS} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customReactions} />
                </div.Demo>

                {/* Custom Update Date */}
                <div.Demo data-cy='custom-update-date' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Update Date
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_DATE} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customUpdate} />
                </div.Demo>

                {/* Custom Label */}
                <div.Demo data-cy='custom-label' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Label
                        </div.DemoTitle>

                        <LessonCard config={cfg.CUSTOM_LABEL} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customLabel} />
                </div.Demo>

                {/* Callbacks */}
                <div.Demo data-cy='callbacks' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callbacks
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='callback-result'>
                            Callback was called: {numOfCallbacks} times
                        </div.CallbackResult>

                        <LessonCard config={cfg.getCallbacksConfig(this)} override={'width: 230px;'}/>
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.callback} />
                </div.Demo>

            </>
        );
    }

    public incrementNumOfCallbacks() {
        const { numOfCallbacks } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                numOfCallbacks: numOfCallbacks + 1,
            },
        });
    }

}