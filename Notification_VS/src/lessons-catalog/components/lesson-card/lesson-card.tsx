import * as icn from './_assets/lesson-card.assets.const';
import { LABELS_TYPE } from './constants/lesson-card.const';
import * as div from './lesson-card.style';
import { dateDiffInDays, getAuthors, getDescription } from './lesson-card.utils';
import { Icon } from '../../../icons/components/icon/icon';
import { PREVIEW } from '../../../shared/assets/icons';
import { APP_CFG } from '../../../shared/config/app.config';
import { colors } from '../../../shared/style/colors';
import { LessonCardConfig } from '../../interfaces/lesson-card';
import { History } from 'history';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    config: LessonCardConfig;
    override?: string;

    /**
     * Because of the ref, we need to provide history from parent
     * Using withRouter and exporting component as a variable it doesn't work with refTypes
     * (or a better solution was not found yet)
     */
    history?: History;
    catalogCallback?: () => void;
}

interface State { }

/**
 * Lesson card displays the Lesson details in a visually appealing way
 * It was designed to catch the attention and engage the students.
 * All the information displayed are designed to trigger interest
 */
export class LessonCard extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { name, authors, description, image, numOfAwards, numOfReactions, updated, label, onMouseEnter, onMouseLeave, onPressPreview } = this.props.config;
        const { override } = this.props;

        return (
            <div.LessonCard data-cy='lesson-card'
                override={override}
                onPress={() => this.redirectToLesson()}
                onMouseEnter={() => onMouseEnter && onMouseEnter()}
                onMouseLeave={() => onMouseLeave && onMouseLeave()}>

                {/* Image */}
                <div.CardImage data-cy='card-image' >
                    <div.Image data-cy='image' source={{ uri: `${APP_CFG.assets}${image}` }} />

                    {/* Label */}
                    {!!LABELS_TYPE[label] &&
                        <div.Label data-cy='label' label={label}>
                            <div.LabelText data-cy='label-text'>
                                {LABELS_TYPE[label]}
                            </div.LabelText>
                        </div.Label>
                    }

                    {/* Preview */}
                    <div.Preview data-cy='preview'
                        onPress={() => onPressPreview && onPressPreview()}>
                        <Icon path={PREVIEW} color={colors.$white} />

                        <div.PreviewText>
                            Preview
                        </div.PreviewText>
                    </div.Preview>
                </div.CardImage>

                <div.CardInfo data-cy='card-info'>
                    <View>

                        {/* Name */}
                        <div.Title data-cy='title'>
                            {name}
                        </div.Title>

                        {/* Authors */}
                        <div.Authors data-cy='authors'>
                            {getAuthors(authors)}
                        </div.Authors>

                        {/* Description */}
                        <div.Description data-cy='description'>
                            {getDescription(description)}
                        </div.Description>
                    </View>

                    <div.Footer>
                        <div.Reactions data-cy='reactions'>

                            {/* Awards */}
                            <div.Statistics data-cy='statistics'>
                                <Icon path={icn.AWARD} color={div.getColorFromLabel(label)} />

                                <div.NumberOfReact data-cy='num-of-awards'>
                                    {numOfAwards}
                                </div.NumberOfReact>
                            </div.Statistics>

                            {/* Faves */}
                            <div.Statistics data-cy='favorites'>
                                <Icon path={icn.FAVORITE} color={div.getColorFromLabel(label)} />

                                <div.NumberOfReact data-cy='num-of-favorites'>
                                    {numOfReactions}
                                </div.NumberOfReact>
                            </div.Statistics>
                        </div.Reactions>

                        {/* Updated */}
                        <div.Updated data-cy='updated'>
                            Updated {dateDiffInDays(updated)} days ago
                        </div.Updated>

                    </div.Footer>
                </div.CardInfo>

            </div.LessonCard>
        );
    }

    private redirectToLesson() {
        const { catalogCallback, history } = this.props;
        const { name } = this.props.config;
        const pathname = name.replace(/ /g, '-');

        !!catalogCallback && catalogCallback();
        history.push(`/lesson/${pathname.toLowerCase()}`);
    }
}
