/// <reference types="cypress" />

describe('Lesson Card Tests', () => {
    it('Lesson Card Page loads successfully', () => {
        cy.visit('http://localhost:3000/components-catalog/lesson-catalog/card');
    });

    it('Is Rendered', () => {
        getElmn('standard-card', 'lesson-card').should('exist');
    });

    it('Title is Rendered', () => {
        getElmn('standard-card', 'title').should('exist');
    });

    it('Title Value', () => {
        getElmn('custom-name', 'title').should('exist').then(div => {
            expect(div.text()).to.eq('Angular from scratch');
        });
    });

    it('Label Is Rendered', () => {
        getElmn('standard-card', 'label').should('exist');
    });

    it('Label Value', () => {
        getElmn('standard-card', 'label').then(div => {
            expect(div.text()).to.eq('HighestRated');
        });
    });

    it('Label Text Is Rendered', () => {
        getElmn('standard-card', 'label-text').should('exist');
    });

    it('Description Is Rendered', () => {
        getElmn('standard-card', 'description').should('exist');
    });

    it('Authors Name', () => {
        getElmn('standard-card', 'authors').then(div => {
            expect(div.text()).to.eq('Roboto Sanso');
        });
    });

    it('Reactions are Rendered', () => {
        getElmn('standard-card', 'reactions').should('exist');
    });

    it('Favorites Value', () => {
        getElmn('standard-card', 'num-of-favorites').then(div => {
            expect(div.text()).to.eq('344');
        });
    });

    it('Awards Value', () => {
        getElmn('standard-card', 'num-of-awards').then(div => {
            expect(div.text()).to.eq('25');
        });
    });

    it('Updated Date is Rendered', () => {
        getElmn('standard-card', 'updated').then(div => {
            expect(div.text()).to.eq('Updated 0 days ago');
        });
    });

    it('Image is Rendered', () => {
        getElmn('standard-card', 'card-image').should('exist');
    });

    it('Calls Hover Callback', () => {
        getElmn('callbacks', 'lesson-card').trigger('mouseover');
        getElmn('callbacks', 'callback-result').then(div => {
            expect(div.text()).to.eq('Callback was called: 1 times');
        });
    });
});

// ====== SELECTORS ======

function getElmn(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}