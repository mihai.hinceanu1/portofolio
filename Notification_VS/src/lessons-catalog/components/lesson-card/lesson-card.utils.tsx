export const getDescription = (description: string) => {
    return description.substring(0, 70) + '...';
};

export const getAuthors = (authors: string[]) => {
    let computedAuthors = '';
    if (authors.length === 1) {
        return authors[0];
    } else {
        authors.forEach(author => {
            computedAuthors = computedAuthors + author + ', ';
        });
    }

    return computedAuthors.substring(0, computedAuthors.length - 2);
};

/** a and b are javascript Date objects */
export const dateDiffInDays = (a: Date) => {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    let b = new Date();
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
};