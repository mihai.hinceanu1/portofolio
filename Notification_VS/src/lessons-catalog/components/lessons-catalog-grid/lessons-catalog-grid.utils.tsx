import { LessonCardConfig } from '../../interfaces/lesson-card';
import { LessonCardDetailsConfig } from '../../interfaces/lesson-card-details';
import { ARROW_POSITION } from '../lesson-card-details/constants/lesson-card-details.const';
import { LessonCardDetails } from '../lesson-card-details/lesson-card-details';
import * as cfg from '../lesson-card/_docs/lesson-card-page.utils';
import { LABELS_TYPE } from '../lesson-card/constants/lesson-card.const';
import * as React from 'react';

export const getLessonCardDetails = (config: LessonCardDetailsConfig, xOffset: number, yOffset: number, cardWidth: number, showImage: boolean, label: LABELS_TYPE): JSX.Element => {
    return (
        <LessonCardDetails label={label}
            config={config}
            showImage={showImage}
            overrides={getLessonCardDetailsOverride(config.arrowPosition, xOffset, yOffset, cardWidth)} />
    );
};

export const getLessonCardDetailsOverride = (arrowPos: ARROW_POSITION, xOffset: number, yOffset: number, cardWidth: number) => {
    let override = '';

    if (arrowPos === ARROW_POSITION.right) {
        override = `position: absolute; left: ${xOffset - 400}px; top: ${yOffset}px; width: 370px`;
    } else {
        override = `position: absolute; left: ${xOffset + cardWidth + 30}px; top: ${yOffset}px; width: 370px`;
    }

    return override;
};

export const lessons: LessonCardConfig[] = [
    cfg.STANDARD,
    cfg.CUSTOM_AUTHORS,
    cfg.CUSTOM_DATE,
    cfg.CUSTOM_DESCRIPTION,
    cfg.CUSTOM_IMAGE,
    cfg.CUSTOM_LABEL,
    cfg.CUSTOM_REACTIONS,
    cfg.CUSTOM_TITLE
];