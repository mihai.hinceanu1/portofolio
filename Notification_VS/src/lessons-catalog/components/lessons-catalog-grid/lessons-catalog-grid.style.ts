import { MOBILE_WIDTH } from '../../../shared/constants/adaptive.const';
import styled from 'styled-components/native';

interface CatalogGridProps {
    width?: number;
    override?: string;
}

export const LessonsCatalogGrid = styled.View<CatalogGridProps>`
    ${props => props.width > MOBILE_WIDTH ? getCatalogGridWebStyle() : getCatalogGridMobileStyle()};
    ${props => props.override}
`;

// ====== UTILS ======

export const getLessonCardOverride = (width: number) => {
    return width > MOBILE_WIDTH ? 'margin-bottom: 10px;' : 'margin-top: 15px;';
};

const getCatalogGridWebStyle = () => `
    margin-top: 30px;
    width: 100%;
    display: grid;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    grid-template-columns: 1fr 1fr 1fr;
    padding-bottom: 30px;
`;

const getCatalogGridMobileStyle = () => `
    width: 100%;
    flex-direction: column;
    padding-bottom: 20px;
    height: 30%;
`;
