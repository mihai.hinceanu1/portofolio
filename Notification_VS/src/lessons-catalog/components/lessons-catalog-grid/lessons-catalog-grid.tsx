import * as div from './lessons-catalog-grid.style';
import { getLessonCardDetails } from './lessons-catalog-grid.utils';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { LessonCardConfig } from '../../interfaces/lesson-card';
import { LessonCardDetailsConfig } from '../../interfaces/lesson-card-details';
import { ARROW_POSITION, lessonCardConfig } from '../lesson-card-details/constants/lesson-card-details.const';
import { LABELS_TYPE } from '../lesson-card/constants/lesson-card.const';
import { LessonCard } from '../lesson-card/lesson-card';
import * as React from 'react';
import {
    Dimensions,
    findNodeHandle,
    LayoutChangeEvent,
    Platform,
    UIManager
    } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    lessons: LessonCardConfig[];
    override?: string;
}

interface State {
    selectedName: string;
    width: number;
    showImage: boolean;
}

/**
 * Renders a grid of lesson cards.
 * The lesson cards can render extra information when hovered.
 * The grid is overlaid on top of the learning map.
 */
export class _LessonsCatalogGrid extends React.Component<Props, State> {

    private isWeb = Platform.OS === 'web';

    // We use an array because sometimes multiple cards remain open.
    // We want to flush them out all in one shot to make sure nothing remains
    private lessonCardDetails: JSX.Element;

    private lessonCardsRefs: LessonCard[] = [];
    private pageWidth: number = Math.round(Dimensions.get('window').width);

    constructor(props: Props) {
        super(props);
        this.state = {
            selectedName: '',
            width: 0,
            showImage: false,
        };
    }

    public render() {
        const { lessons, override, history } = this.props;
        const { width } = this.state;

        return (
            <div.LessonsCatalogGrid data-cy='lessons-catalog-grid'
                onLayout={e => this.updateWidth(e)}
                width={width}
                override={override}>

                {
                    // Lesson Cards
                    lessons.map((lesson, index) =>

                        <LessonCard
                            config={lesson}
                            key={lesson.name}
                            history={history}
                            catalogCallback={() => this.hideLessonCardDetails()}
                            override={div.getLessonCardOverride(width)}
                            ref={card => { this.lessonCardsRefs[index] = card; }} />

                    )
                }

            </div.LessonsCatalogGrid>
        );
    }

    public componentDidMount() {
        const { lessons } = this.props;
        this.isWeb && this.addCardDetailsVisibilityTriggers(lessons);
    }

    private addCardDetailsVisibilityTriggers(lessons: LessonCardConfig[]) {
        // this.lessonCardDetails = [];
        lessons.forEach((lesson, index) => {
            lesson.onMouseEnter = () => this.showLessonCardDetails(this.lessonCardDetails, lessonCardConfig, index, lesson.label);
            lesson.onMouseLeave = () => this.hideLessonCardDetails();
            lesson.onPressPreview = () => this.PressPreview();
        });
    }

    private showLessonCardDetails(lessonCardDetails: JSX.Element, config: LessonCardDetailsConfig, index: number, label: LABELS_TYPE) {
        const { showImage } = this.state;

        // In case mouse leave was not properly detected
        this.hideLessonCardDetails();

        // Get position
        UIManager.measure(
            findNodeHandle(this.lessonCardsRefs[index]),
            (_fx, _fy, _width, _height, px, py) => { // TODO Maybe move all of this to standalone method ?

                // Arrow position
                config.arrowPosition = this.getArrowPosition(px);

                // Cache
                lessonCardDetails = getLessonCardDetails(config, px, py, _width, showImage, label);

                const detailsCard = lessonCardDetails;

                // Overlay
                addAppOverlay(detailsCard);
                this.lessonCardDetails = lessonCardDetails;
            }
        );

    }

    private getArrowPosition(px: number) {
        if (px > this.pageWidth / 2) {
            return ARROW_POSITION.right;
        } else {
            return ARROW_POSITION.left;
        }
    }

    private hideLessonCardDetails() {
        removeAppOverlay(this.lessonCardDetails);
        this.lessonCardDetails = null;
        // !!this.lessonCardDetails && this.lessonCardDetails.forEach(el => removeAppOverlay(el));
        // delete this.lessonCardDetails;
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private PressPreview() {
        const { showImage } = this.state;
        this.setState({
            showImage: !showImage,
        });
    }
}

export const LessonsCatalogGrid = withRouter(_LessonsCatalogGrid);