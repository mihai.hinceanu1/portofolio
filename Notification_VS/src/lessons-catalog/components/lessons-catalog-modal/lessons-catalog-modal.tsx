import * as div from './lessons-catalog-modal.style';
import { selectedCategoryName$ } from '../../../learning-map/services/learning-map.service';
import { PageHeader } from '../../../shared/components/page-header/page-header';
import { Filter } from '../../../shared/interfaces/page-header';
import * as overlay from '../../../shared/style/overlay.style';
import * as scroll from '../../../shared/style/scroll.style';
import { TOPIC_TYPE } from '../../../topics/constants/topics';
import { getLessonsCatalogHeaderCfg } from '../../services/catalog-header.utils';
import { LessonsCatalogGrid } from '../lessons-catalog-grid/lessons-catalog-grid';
import { lessons } from '../lessons-catalog-grid/lessons-catalog-grid.utils';
import { LessonsCatalogHoverMenus } from '../lessons-catalog-hover-menus/lessons-catalog-hover-menus';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props { }

interface State {
    width: number;
    categoryName: string;
    topicType: TOPIC_TYPE;
    searchTerm: string;
    activeFilters: Filter[];
}

/**
 * Renders all lessons and projects listed under a learning map category.
 * All possible lessons and projects available under one category are listed in a traditional list format.
 *
 * <!> Extra screen space
 * On the learning map we list only the top content for a each topic cluster.
 * Because the learning map renders a limited set of information we need extra screen space.
 * This modal solves the problem of listing more than 10-20 links in each topic cluster.
 */
export class LessonsCatalogModal extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            width: Dimensions.get('window').width,
            categoryName: null,
            topicType: TOPIC_TYPE.lesson,
            searchTerm: '',
            activeFilters: [],
        };
    }

    public render() {
        let { width, searchTerm } = this.state;

        return (
            <div.LessonsCatalogModal data-cy='lessons-catalog-modal'
                onLayout={e => this.updateWidth(e)}>

                {/* Hover Menus */}
                <LessonsCatalogHoverMenus />

                <LinearGradient colors={overlay.OVERLAY_GRADIENT}
                    style={overlay.style.linearGradient}>

                    <scroll.ScrollView data-cy='scroll-view'
                        contentContainerStyle={scroll.inner.content}>
                        <scroll.Center width={width}>

                            {/** Header */}
                            <PageHeader config={getLessonsCatalogHeaderCfg(
                                searchTerm,
                                this.handleSearchBarChange.bind(this),
                                this.handleSearchBarSubmit.bind(this),
                                this.addFilters.bind(this),
                            )}
                                overrides={div.PAGE_HEADER_OVERRIDES} />

                            {/* Lessons Grid */}
                            <LessonsCatalogGrid lessons={lessons} />

                        </scroll.Center>
                    </scroll.ScrollView>

                </LinearGradient>
            </div.LessonsCatalogModal>
        );
    }

    public componentDidMount() {
        this.subscribeToCategoryName();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToCategoryName() {
        selectedCategoryName$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(selectedCategoryName =>
                this.setState({
                    categoryName: selectedCategoryName
                })
            );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private handleSearchBarChange(text: string) {
        this.setState({
            searchTerm: text,
        });
    }

    private handleSearchBarSubmit() {
        // const { searchTerm } = this.state;
    }

    private addFilters(filter: Filter) {
        const { activeFilters } = this.state;
        let copy = [...activeFilters];
        copy.push(filter);

        this.setState({
            activeFilters: copy,
        });
    }
}