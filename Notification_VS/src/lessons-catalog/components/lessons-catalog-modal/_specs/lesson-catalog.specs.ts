/// <reference types="cypress" />

describe('Lesson Catalog Tests', () => {
    it('Successfully  loads the page', () => {
        openCatalog();
    });

    it('Page Header is Rendered', () => {
        cy.get('[data-cy=page-header]').should('exist');
    });

    it('Lesson Cards Should Exist', () => {
        cy.get('[data-cy=lesson-card]').should('exist');
    });

    it('On Hover Lesson Card Render Lesson Card Details', () => {
        cy.get('[data-cy=lesson-card]').eq(0).trigger('mouseover');
        cy.get('[data-cy=details]').should('exist');
    });

    it('On Mouse Leave Lesson Card Details Disappear', () => {
        cy.get('[data-cy=lesson-card]').eq(0).trigger('mouseout');
        cy.get('[data-cy=details]').should('not.exist');
    });

    it('Hovering Transition between cards', () => {
        cy.get('[data-cy=lesson-card]').eq(0).trigger('mouseover');
        cy.get('[data-cy=lesson-card]').eq(4).trigger('mouseover');
        cy.get('[data-cy=details]').its('length').should('eq', 1);
    });

    it('On Press Card Url Changes', () => {
        cy.get('[data-cy=lesson-card]').eq(1).click();
        cy.url().should('not.eq', 'http://localhost:3000/lesson');
    });
});

// ====== UTILS ======

function openCatalog() {
    cy.visit('http://localhost:3000');
    cy.get('[data-cy=input]').eq(0).type('admin');
    cy.get('[data-cy=input]').eq(1).type('admin');
    cy.get('[data-cy=button]').click();
    cy.get('[data-cy=page-button]').eq(1).click();
    cy.get('[data-cy=toggle-button]').eq(1).click();
}
