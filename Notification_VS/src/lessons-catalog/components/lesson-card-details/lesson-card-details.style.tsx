import { LESSON_CARD_ARROW_LEFT, LESSON_CARD_ARROW_RIGHT } from './_assets/lesson-card-details.assets.const';
import { ARROW_POSITION } from './constants/lesson-card-details.const';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { BOX_SHADOW_CSS, BOX_SHADOW_NATIVE } from '../../../shared/style/shadow.style';
import { LABELS_TYPE } from '../lesson-card/constants/lesson-card.const';
import { getColorFromLabel } from '../lesson-card/lesson-card.style';
import * as React from 'react';
import { Platform, StyleProp, ViewStyle } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

const isMobile = Platform.OS !== 'web';

interface LessonCardDetailsProps {
    arrowPosition?: ARROW_POSITION;
    overrides?: string;
    imageExist?: boolean;
}

export const LessonCardDetails = styled.View<LessonCardDetailsProps>`
    display: flex;
    flex-direction: row;
    ${isMobile ? BOX_SHADOW_NATIVE : BOX_SHADOW_CSS};
    background-color: white;
    ${props => props.overrides};
`;

export const PreviewImage = styled.View`
    flex: 1;
    width: 100%;
    justify-content: center;
    align-items: center;
`;

export const LessonInfo = styled.View<LessonCardDetailsProps>`
    padding: 20px;
    width: 100%;
    justify-content: flex-start;
    align-items: flex-start;
`;

export const Title = styled.Text`
    margin-bottom: 15px;
    font-size: 18px;
    color: black;
    font-weight: 600;
    text-align: left;
`;

export const Difficulty = styled.Text`
    margin-bottom: 15px;
    font-size: ${font.$size10}px;
    color: ${colors.$grey};
    text-align: left;
`;

export const Description = styled.Text`
    font-size:${font.$size12}px;
    text-align: left;
    margin-bottom: 15px;
`;

export const LearnList = styled.View`
    margin-top: 12.5px;
    display: flex;
    flex-direction: column;
`;

export const ListTitle = styled.Text`
    font-size:${font.$size16}px;
    font-weight: 600;
    margin-bottom: 10px;
`;

export const ListElem = styled.View`
    font-size: 12px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
`;

export const ElemBullet = styled.View<{label: LABELS_TYPE}>`
    width: 10px;
    height: 10px;
    background-color: ${props => getColorFromLabel(props.label)};
    margin-right: 10px;
`;

export const ElemText = styled.Text`
    font-size: ${font.$size12}px;
`;

export const Chart = styled.View`
    height: 20%;
    width: 100%;
    justify-content: flex-start;
    align-items: flex-start;
    margin-top: 15px;
    overflow: hidden;
`;

export const Dates = styled.View`
    height: 20px;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const Date = styled.Text`
    font-size:${font.$size12}px;
`;

// ====== UTILS ======

export const getArrowPosition = (arrowPosition: ARROW_POSITION) => {
    return arrowPosition === ARROW_POSITION.left ? 'row' : 'row-reverse';
};

const getConnectorArrow = (arrowPosition: ARROW_POSITION) => {
    return arrowPosition === ARROW_POSITION.left ? LESSON_CARD_ARROW_LEFT : LESSON_CARD_ARROW_RIGHT;
};

export const getConnector = (arrowPosition: ARROW_POSITION): JSX.Element => {
    const svgPath = getConnectorArrow(arrowPosition);

    let boundingBox = pathBoundingBox(svgPath);
    const { height, width } = boundingBox;

    let arrowSide: StyleProp<ViewStyle> = {
        backgroundColor: 'none', justifyContent: 'center', alignItems: 'center', marginTop: '150px', zIndex: 100, position: 'absolute',
    };

    if (arrowPosition === ARROW_POSITION.right) {
        arrowSide.right = -width;
    } else {
        arrowSide.left = -width;
    }

    return (
        <Svg data-cy='dropdown-svg'
            width={width}
            height={height}
            fill='none' style={arrowSide} >
            <Path data-cy='dropdown-svg-path'
                d={svgPath} fill={('white')} />
        </Svg>
    );
};