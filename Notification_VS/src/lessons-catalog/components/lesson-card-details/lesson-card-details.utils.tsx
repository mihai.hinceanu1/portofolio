import { LABELS_TYPE } from '../lesson-card/constants/lesson-card.const';
import { getColorFromLabel } from '../lesson-card/lesson-card.style';
import * as React from 'react';
import { LineChart } from 'react-native-chart-kit';

/**
 * Used to format the date
 */
export const getFullDate = (date: Date) => {
    return date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + (date.getDate());
};

/**
 * Renders a bezier chart with the number of views in the last few months.
 */
export const getPopularityLineChart = (labels: string[], values: number[], width: number, height: number, label: LABELS_TYPE) => {
    const chartColor = getColorFromLabel(label);

    return (
        <LineChart
            data={{
                labels: labels,
                datasets: [{
                    data: values,
                }]
            }
            }
            width={width} // from react-native
            height={height}
            chartConfig={{
                strokeWidth: 3,
                backgroundColor: chartColor,
                backgroundGradientFrom: 'white',
                backgroundGradientTo: 'white',
                decimalPlaces: 0,
                fillShadowGradient: chartColor,
                color: () => chartColor,
                labelColor: () => chartColor,
                style: {
                    borderRadius: 16
                }
            }}
            withInnerLines={false}
            withShadow={true}
            bezier={true}
            fromZero={true}
            segments={2}
            formatYLabel={(yValue) => { return roundToNearestHundredth((parseInt(yValue))).toString(); }}
            style={{
                marginLeft: '-20px',
                borderRadius: 16
            }}
        />
    );
};

function roundToNearestHundredth(num: number) {
    return Math.ceil(num / 100) * 100;
}

/** a and b are javascript Date objects */
export const dateDiffInDays = (a: Date) => {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    let b = new Date();
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
};