import { Difficulties } from './constants/lesson-card-details.const';
import * as div from './lesson-card-details.style';
import { dateDiffInDays, getPopularityLineChart } from './lesson-card-details.utils';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { LessonCardDetailsConfig } from '../../interfaces/lesson-card-details';
import { LABELS_TYPE } from '../lesson-card/constants/lesson-card.const';
import * as React from 'react';
import { LayoutChangeEvent } from 'react-native';

interface Props {
    config?: LessonCardDetailsConfig;
    overrides?: string;
    showImage?: boolean;
    label?: LABELS_TYPE;
}

interface State {
    width: number;
    labels: string[];
    values: number[];
    height: number;
}

/** Lesson card details displays more details about a lesson when its card is hovered */
export class LessonCardDetails extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: 0,
            labels: [],
            values: [],
            height: 0,
        };
    }

    public render() {
        const { overrides, showImage, label } = this.props;
        const { name, difficulty, description, learningThemes, image, updated, published, arrowPosition } = this.props.config;
        const { width, labels, values, height } = this.state;

        return (
            <div.LessonCardDetails data-cy='details' overrides={overrides}>

                {
                    // Arrow
                    div.getConnector(arrowPosition)
                }

                {!showImage &&
                    < div.LessonInfo data-cy='info' imageExist={!!image}>

                        {/* Title */}
                        <div.Title data-cy='title'>
                            {name}
                        </div.Title>

                        {/* Difficulty */}
                        <div.Difficulty data-cy='difficulty'>
                            Estimated difficulty: {Difficulties[difficulty]}
                        </div.Difficulty>

                        {/* Description */}
                        <div.Description data-cy='description'>
                            {description}
                        </div.Description>

                        <div.LearnList data-cy='learn-list'>
                            <div.ListTitle data-cy='learn-list-title'>
                                What you'll learn
                            </div.ListTitle>

                            {
                                // Learning Goals
                                learningThemes.map((theme, index) =>
                                    <div.ListElem key={index}>
                                        <div.ElemBullet label={label}/>

                                        <div.ElemText >
                                            {theme}
                                        </div.ElemText>
                                    </div.ListElem>)
                            }

                        </div.LearnList>

                        {/* Popularity Chart */}
                        {   values.length !== 0 &&
                            <div.Chart data-cy='chart'
                                onLayout={(event) => this.getChartLayout(event)} >
                                {
                                    getPopularityLineChart(labels, values, width, height, label)
                                }
                            </div.Chart>
                        }

                        <div.Dates data-cy='dates'>

                            {/* Published */}
                            <div.Date data-cy='published'>
                                Published {dateDiffInDays(published)} days ago
                            </div.Date>

                            {/* Updated */}
                            <div.Date data-cy='updated'>
                                Updated {dateDiffInDays(updated)} days ago
                            </div.Date>
                        </div.Dates>

                    </div.LessonInfo>
                }

                {
                    !!image && showImage &&
                    // Preview Image
                    <div.PreviewImage data-cy='preview-image'>
                        <FitImage imgPath={image} />
                    </div.PreviewImage>
                }

            </div.LessonCardDetails >
        );
    }

    public componentDidMount() {
        const { popularity } = this.props.config;
        const labels = popularity.map(el => el.date.toString().split('')[0]);
        const values = popularity.map(el => el.value);

        this.setState({
            labels,
            values,
        });
    }

    private getChartLayout(event: LayoutChangeEvent) {
        var { width, height } = event.nativeEvent.layout;

        this.setState({ width: width, height: height });
    }
}