import { POPULARITY } from './lesson-card-details.mocks';
import { LessonCardDetailsConfig } from '../../../interfaces/lesson-card-details';

export enum ARROW_POSITION {
    'left',
    'right',
    'mid',
}

export enum Difficulties {
    'junior',
    'mid',
    'senior',
}

export const lessonCardConfig: LessonCardDetailsConfig = {
    name:'The Business Intelligence Analyst Course',
    difficulty: Difficulties.junior,
    arrowPosition: ARROW_POSITION.left,
    description: 'The skills you need to become a B. Analyst - Statistics Database theory, SQL, Tableau- Everything is here for you Persuasion, Assertiveness and all the Business Communication Needs',
    popularity: POPULARITY,
    learningThemes:['Become a master in leadership', 'Be a better boss, run a highly effective team', 'Delegate effectively to empower their team'],
    updated: new Date(),
    image: '/lesson-catalog/example2.png',
    published: new Date('2015-03-10'),
};