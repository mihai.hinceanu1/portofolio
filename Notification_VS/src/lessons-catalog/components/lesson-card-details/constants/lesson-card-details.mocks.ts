export const POPULARITY = [
    {
        date: new Date('2015-03-25'),
        value: 40,
    },
    {
        date: new Date('2015-03-26'),
        value: 60,
    },
    {
        date: new Date('2015-03-24'),
        value: 70,
    },
    {
        date: new Date('2015-03-23'),
        value: 75,
    },
    {
        date: new Date('2015-03-22'),
        value: 80,
    },
    {
        date: new Date('2015-03-21'),
        value: 95,
    },
    {
        date: new Date('2015-03-20'),
        value: 105,
    },
    {
        date: new Date('2015-03-19'),
        value: 120,
    },
    {
        date: new Date('2015-03-18'),
        value: 130,
    },
    {
        date: new Date('2015-03-26'),
        value: 135,
    },
    {
        date: new Date('2015-03-27'),
        value: 30,
    },
    {
        date: new Date('2015-03-28'),
        value: 300,
    },
    {
        date: new Date('2015-03-29'),
        value: 305,
    },
    {
        date: new Date('2015-03-30'),
        value: 310,
    },
];