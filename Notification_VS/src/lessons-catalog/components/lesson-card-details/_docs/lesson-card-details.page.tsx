import * as cfg from './lesson-card-details-page.utils';
import * as samples from './lesson-card-details.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { LessonCardDetails } from '../lesson-card-details';
import * as React from 'react';
import { View } from 'react-native';

interface LessonCardDetailsState {
    width: number;
}

export class LessonCardDetailsPage extends ScrollableDocPage<LessonCardDetailsState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Lesson Card Details
                    </div.OverviewTitle>
                    <div.OverviewDescription>
                        Lesson Card Details was created in order to give details about every lesson that takes part in our app in Lesson Catalog.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Standard card details */}
                <div.Demo data-cy='standard-details' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard Card Details
                        </div.DemoTitle>

                        <LessonCardDetails config={cfg.FULL_DETAILS} overrides={'width: 600px;'} />
                    </div.DemoArea>
                </div.Demo>

                <div.Demo width={width}>
                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.fullDetails} />
                    <View style={{ height: '250px' }} />
                </div.Demo>

                {/* Custom Image */}
                <div.Demo data-cy='custom-image' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Image
                        </div.DemoTitle>

                        <LessonCardDetails config={cfg.CUSTOM_IMAGE} overrides={'width: 600px;'} />
                    </div.DemoArea>
                </div.Demo>

                <div.Demo width={width}>
                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customImage} />

                    <View style={{ height: '100px' }} />
                </div.Demo>

                {/* Custom Difficulty */}
                <div.Demo data-cy='custom difficulty' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Difficulty
                        </div.DemoTitle>
                        <LessonCardDetails config={cfg.CUSTOM_DIFFICULTY} overrides={'width: 600px;'} />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customDifficulty} />
                </div.Demo>

                {/* Custom Description */}
                <div.Demo data-cy='custom-description' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Description
                        </div.DemoTitle>
                        <LessonCardDetails config={cfg.CUSTOM_DESCRIPTION} overrides={'width: 600px;'} />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customDescription} />
                </div.Demo>

                {/* Custom Chart */}
                <div.Demo data-cy='custom-chart' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Chart
                        </div.DemoTitle>
                        <LessonCardDetails config={cfg.CUSTOM_CHART} overrides={'width: 600px;'} />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customChart} />
                </div.Demo>

                {/* Custom Learning Themes */}
                <div.Demo data-cy='custom-learning-themes' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Learning Themes
                        </div.DemoTitle>
                        <LessonCardDetails config={cfg.CUSTOM_THEMES} overrides={'width: 600px;'} />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customThemes} />
                </div.Demo>

                {/* Custom Dates */}
                <div.Demo data-cy='custom-dates' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Dates
                        </div.DemoTitle>
                        <LessonCardDetails config={cfg.CUSTOM_DATES} overrides={'width: 600px;'} />
                    </div.DemoArea>

                    <CodeEditor
                        {...codeSampleConfig}
                        srcCode={samples.customDates} />
                </div.Demo>
            </>
        );
    }

}