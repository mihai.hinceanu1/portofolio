import { LessonCardDetailsConfig } from '../../../interfaces/lesson-card-details';
import { ARROW_POSITION, Difficulties } from '../constants/lesson-card-details.const';

export const FULL_DETAILS: LessonCardDetailsConfig = {
    name: 'The Business Intelligence Analyst Course',
    difficulty: Difficulties.junior,
    arrowPosition: ARROW_POSITION.right,
    description: 'The skills you need to become a B. Analyst - Statistics Database theory, SQL, Tableau- Everything is here for you Persuasion, Assertiveness and all the Business Communication Needs',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 10,
        },
        {
            date: new Date('2015-03-26'),
            value: 21,
        },
        {
            date: new Date('2015-03-24'),
            value: 22,
        },
        {
            date: new Date('2015-03-23'),
            value: 23,
        },
        {
            date: new Date('2015-03-22'),
            value: 210,
        },
        {
            date: new Date('2015-03-21'),
            value: 5,
        },
        {
            date: new Date('2015-03-20'),
            value: 1,
        },
        {
            date: new Date('2015-03-19'),
            value: 333,
        },
        {
            date: new Date('2015-03-18'),
            value: 232,
        },
        {
            date: new Date('2015-03-26'),
            value: 123,
        },
        {
            date: new Date('2015-03-27'),
            value: 321,
        },
        {
            date: new Date('2015-03-28'),
            value: 1210,
        },
        {
            date: new Date('2015-03-29'),
            value: 102,
        },
        {
            date: new Date('2015-03-30'),
            value: 1210,
        },
    ],
    learningThemes: ['Become a master in leadership', 'Be a better boss, run a highly effective team', 'Delegate effectively to empower their team'],
    image: '/auth/eniacWithConfetti.png',
    updated: new Date(),
    published: new Date('2015-03-10'),
};

export const CUSTOM_IMAGE: LessonCardDetailsConfig = {
    name: 'The Business Intelligence Analyst Course',
    difficulty: Difficulties.junior,
    arrowPosition: ARROW_POSITION.left,
    description: 'The skills you need to become a B. Analyst - Statistics Database theory, SQL, Tableau- Everything is here for you Persuasion, Assertiveness and all the Business Communication Needs',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 10,
        },
        {
            date: new Date('2015-03-26'),
            value: 21,
        },
        {
            date: new Date('2015-03-24'),
            value: 22,
        },
        {
            date: new Date('2015-03-23'),
            value: 23,
        },
        {
            date: new Date('2015-03-22'),
            value: 210,
        },
        {
            date: new Date('2015-03-21'),
            value: 5,
        },
        {
            date: new Date('2015-03-20'),
            value: 1,
        },
        {
            date: new Date('2015-03-19'),
            value: 333,
        },
        {
            date: new Date('2015-03-18'),
            value: 232,
        },
        {
            date: new Date('2015-03-26'),
            value: 123,
        },
        {
            date: new Date('2015-03-27'),
            value: 321,
        },
        {
            date: new Date('2015-03-28'),
            value: 1210,
        },
        {
            date: new Date('2015-03-29'),
            value: 102,
        },
        {
            date: new Date('2015-03-30'),
            value: 1210,
        },
    ],
    learningThemes: ['Become a master in leadership', 'Be a better boss, run a highly effective team', 'Delegate effectively to empower their team'],
    image: '/auth/eniac_extraSad.gif',
    updated: new Date(),
    published: new Date('2015-03-10'),
};

export const CUSTOM_DIFFICULTY: LessonCardDetailsConfig = {
    name: 'The Business Intelligence Analyst Course',
    difficulty: Difficulties.senior,
    arrowPosition: ARROW_POSITION.mid,
    description: 'The skills you need to become a B. Analyst - Statistics Database theory, SQL, Tableau- Everything is here for you Persuasion, Assertiveness and all the Business Communication Needs',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 10,
        },
        {
            date: new Date('2015-03-26'),
            value: 21,
        },
        {
            date: new Date('2015-03-24'),
            value: 22,
        },
        {
            date: new Date('2015-03-23'),
            value: 23,
        },
        {
            date: new Date('2015-03-22'),
            value: 210,
        },
        {
            date: new Date('2015-03-21'),
            value: 5,
        },
        {
            date: new Date('2015-03-20'),
            value: 1,
        },
        {
            date: new Date('2015-03-19'),
            value: 333,
        },
        {
            date: new Date('2015-03-18'),
            value: 232,
        },
        {
            date: new Date('2015-03-26'),
            value: 123,
        },
        {
            date: new Date('2015-03-27'),
            value: 321,
        },
        {
            date: new Date('2015-03-28'),
            value: 1210,
        },
        {
            date: new Date('2015-03-29'),
            value: 102,
        },
        {
            date: new Date('2015-03-30'),
            value: 1210,
        },
    ],
    learningThemes: ['Become a master in leadership', 'Be a better boss, run a highly effective team', 'Delegate effectively to empower their team'],
    updated: new Date(),
    published: new Date('2015-03-10'),
};

export const CUSTOM_DESCRIPTION: LessonCardDetailsConfig = {
    name: 'The Business Intelligence Analyst Course',
    difficulty: Difficulties.senior,
    arrowPosition: ARROW_POSITION.mid,
    description: 'Start your first business by using our amazing lesson on this topic. From zero to hero!',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 10,
        },
        {
            date: new Date('2015-03-26'),
            value: 21,
        },
        {
            date: new Date('2015-03-24'),
            value: 22,
        },
        {
            date: new Date('2015-03-23'),
            value: 23,
        },
        {
            date: new Date('2015-03-22'),
            value: 210,
        },
        {
            date: new Date('2015-03-21'),
            value: 5,
        },
        {
            date: new Date('2015-03-20'),
            value: 1,
        },
        {
            date: new Date('2015-03-19'),
            value: 333,
        },
        {
            date: new Date('2015-03-18'),
            value: 232,
        },
        {
            date: new Date('2015-03-26'),
            value: 123,
        },
        {
            date: new Date('2015-03-27'),
            value: 321,
        },
        {
            date: new Date('2015-03-28'),
            value: 1210,
        },
        {
            date: new Date('2015-03-29'),
            value: 102,
        },
        {
            date: new Date('2015-03-30'),
            value: 1210,
        },
    ],
    learningThemes: ['Become a master in leadership', 'Be a better boss, run a highly effective team', 'Delegate effectively to empower their team'],
    updated: new Date(),
    published: new Date('2015-03-10'),
};

export const CUSTOM_CHART: LessonCardDetailsConfig = {
    name: 'The Business Intelligence Analyst Course',
    difficulty: Difficulties.senior,
    arrowPosition: ARROW_POSITION.mid,
    description: 'Start your first business by using our amazing lesson on this topic. From zero to hero!',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 1500,
        },
        {
            date: new Date('2015-03-26'),
            value: 500,
        },
        {
            date: new Date('2015-03-24'),
            value: 800,
        },
        {
            date: new Date('2015-03-23'),
            value: 1300,
        },
        {
            date: new Date('2015-03-22'),
            value: 300,
        },
        {
            date: new Date('2015-03-21'),
            value: 500,
        },
        {
            date: new Date('2015-03-20'),
            value: 1000,
        },
        {
            date: new Date('2015-03-19'),
            value: 480,
        },
        {
            date: new Date('2015-03-18'),
            value: 660,
        },
        {
            date: new Date('2015-03-26'),
            value: 770,
        },
        {
            date: new Date('2015-03-27'),
            value: 880,
        },
        {
            date: new Date('2015-03-28'),
            value: 1000,
        },
        {
            date: new Date('2015-03-29'),
            value: 1100,
        },
        {
            date: new Date('2015-03-30'),
            value: 2,
        },
    ],
    learningThemes: ['Become a master in leadership', 'Be a better boss, run a highly effective team', 'Delegate effectively to empower their team'],
    updated: new Date(),
    published: new Date('2015-03-10'),
};

export const CUSTOM_THEMES: LessonCardDetailsConfig = {
    name: 'The Business Intelligence Analyst Course',
    difficulty: Difficulties.senior,
    arrowPosition: ARROW_POSITION.mid,
    description: 'Start your first business by using our amazing lesson on this topic. From zero to hero!',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 1500,
        },
        {
            date: new Date('2015-03-26'),
            value: 500,
        },
        {
            date: new Date('2015-03-24'),
            value: 800,
        },
        {
            date: new Date('2015-03-23'),
            value: 1300,
        },
        {
            date: new Date('2015-03-22'),
            value: 300,
        },
        {
            date: new Date('2015-03-21'),
            value: 500,
        },
        {
            date: new Date('2015-03-20'),
            value: 1000,
        },
        {
            date: new Date('2015-03-19'),
            value: 480,
        },
        {
            date: new Date('2015-03-18'),
            value: 660,
        },
        {
            date: new Date('2015-03-26'),
            value: 770,
        },
        {
            date: new Date('2015-03-27'),
            value: 880,
        },
        {
            date: new Date('2015-03-28'),
            value: 1000,
        },
        {
            date: new Date('2015-03-29'),
            value: 1100,
        },
        {
            date: new Date('2015-03-30'),
            value: 2,
        },
    ],
    learningThemes: ['Be first', 'Never Be Second'],
    updated: new Date(),
    published: new Date('2015-03-10'),
};

export const CUSTOM_DATES: LessonCardDetailsConfig = {
    name: `It's Angular Time`,
    difficulty: Difficulties.senior,
    arrowPosition: ARROW_POSITION.mid,
    description: 'Start your first business by using our amazing lesson on this topic. From zero to hero!',
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 1500,
        },
        {
            date: new Date('2015-03-26'),
            value: 500,
        },
        {
            date: new Date('2015-03-24'),
            value: 800,
        },
        {
            date: new Date('2015-03-23'),
            value: 1300,
        },
        {
            date: new Date('2015-03-22'),
            value: 300,
        },
        {
            date: new Date('2015-03-21'),
            value: 500,
        },
        {
            date: new Date('2015-03-20'),
            value: 1000,
        },
        {
            date: new Date('2015-03-19'),
            value: 480,
        },
        {
            date: new Date('2015-03-18'),
            value: 660,
        },
        {
            date: new Date('2015-03-26'),
            value: 770,
        },
        {
            date: new Date('2015-03-27'),
            value: 880,
        },
        {
            date: new Date('2015-03-28'),
            value: 1000,
        },
        {
            date: new Date('2015-03-29'),
            value: 1100,
        },
        {
            date: new Date('2015-03-30'),
            value: 2,
        },
    ],
    learningThemes: ['Be first', 'Never Be Second'],
    updated: new Date('2019-03-11'),
    published: new Date('1999-03-10'),
};