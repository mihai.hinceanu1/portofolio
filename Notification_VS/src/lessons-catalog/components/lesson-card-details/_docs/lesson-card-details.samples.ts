export const fullDetails = `
<LessonCardDetails config={CARD_DETAILS} />

export const CARD_DETAILS: LessonCardDetailsConfig = {
    name: {customName},
    difficulty: Difficulties.junior,
    description: {customDescription},
    popularity: [CustomTimeSeries],
    learningThemes: [CustomThemes],
    image: '/auth/eniacWithConfetti.png',
    updated: new Date(),
    published: new Date('2015-03-10'),
};`;

export const customImage = `
export const CARD_DETAILS: LessonCardDetailsConfig = {
    image: '/auth/eniac_extraSad.gif'
}`;

export const customDifficulty = `
export const CARD_DETAILS: LessonCardDetailsConfig = {
    difficulty: Difficulties.senior,
}`;

export const customDescription = `
export const CARD_DETAILS: LessonCardDetailsConfig = {
    description: {customDescription},
}`;

export const customChart = `
export const CARD_DETAILS: LessonCardDetailsConfig = {
    popularity: [
        {
            date: new Date('2015-03-25'),
            value: 1500,
        }
    ]
}`;

export const customThemes = `
export const CARD_DETAILS: LessonCardDetailsConfig = {
    learningThemes: ['Be first', 'Never Be Second']
}`;

export const customDates = `
export const CARD_DETAILS: LessonCardDetailsConfig = {
    updated: new Date('2019-03-11'),
    published: new Date('1999-03-10'),
}`;