/// <reference types="cypress" />

describe('Lesson Card Tests', () => {
    it('Lesson Card Page loads successfully', () => {
        cy.visit('http://localhost:3000/components-catalog/lesson-catalog/card-details');
    });

    it('Is Rendered', () => {
        getElement('standard-details', 'details').should('exist');
    });

    it('Card Body is Rendered', () => {
        getElement('standard-details', 'info').should('exist');
    });

    it('Title is Rendered', () => {
        getElement('standard-details', 'title').then(div => {
            expect(div.text()).to.eq('The Business Intelligence Analyst Course');
        });
    });

    it('Description is Rendered', () => {
        getElement('standard-details', 'description').should('exist');
    });

    it('Difficulty is Rendered', () => {
        getElement('standard-details', 'difficulty').then(div => {
            expect(div.text()).to.eq('Estimated difficulty: junior');
        });
    });

    it('Learn List is Rendered', () => {
        getElement('standard-details', 'learn-list').should('exist');
    });

    it('Learn List Title is Rendered', () => {
        getElement('standard-details', 'learn-list-title').then(div => {
            expect(div.text()).to.eq(`What you'll learn`);
        });
    });

    it('Chart is Rendered', () => {
        getElement('standard-details', 'chart').should('exist');
    });

    it('Dates are Rendered', () => {
        getElement('standard-details', 'dates').should('exist');
    });

    it('Published is Rendered', () => {
        getElement('standard-details', 'published').then(div => {
            expect(div.text()).to.eq('Published on 2015/3/10');
        });
    });

    it('Updated is Rendered', () => {
        getElement('standard-details', 'updated').then(div => {
            /** <!>The date updates when you run the test. */
            expect(div.text()).to.eq('Updated on 2020/3/5');
        });
    });

});

// ====== SELECTORS ======

function getElement(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}