import axios from '../../shared/services/axios.service';
import { ITopic } from '../../topics/interfaces/topic';
import { from, Observable } from 'rxjs';

export const getCatalogLessons = (categoryId: string): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/lesson-catalog/lesson-catalog/${categoryId}`)
            .then(res => res.data)
    );