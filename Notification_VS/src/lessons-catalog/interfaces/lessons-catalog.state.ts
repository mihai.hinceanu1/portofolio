
export interface LessonsCatalogState {
    topicsCatalogModal: boolean;
    catalogDisplayType: string;
    catalogLayoutType: string;
}
