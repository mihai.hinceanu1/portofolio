import { LABELS_TYPE } from '../components/lesson-card/constants/lesson-card.const';

/** Most eye catchy information about lessons */
export interface LessonCardConfig {

    /** Title */
    name: string;

    /**  Authors */
    authors: string[];

    /** Description */
    description: string;

    /** Top image of card */
    image: string;

    /** Number of awards earned */
    numOfAwards: number;

    /** Number of reactions given by user */
    numOfReactions: number;

    /** Last Date of Update */
    updated: Date;

    /** Label - if any earned ( EditorsPick/Bestseller/HighestRated ) */
    label?: LABELS_TYPE;

    /** Callback for hover */
    onMouseEnter?: () => void;

    /** Callback for hover exit */
    onMouseLeave?: () => void;

    /** Callback to change lesson card details content */
    onPressPreview?: () => void;
}