import { ARROW_POSITION, Difficulties } from '../components/lesson-card-details/constants/lesson-card-details.const';

/**
 * Details that complete the amount
 * of information gave in a lesson card
 */
export interface LessonCardDetailsConfig {

    /** Title */
    name: string;

    /** Estimated Difficulty */
    difficulty: Difficulties;

    /** Description */
    description: string;

    /** Learning topics presented in card */
    learningThemes: string[];

    /**
     * Number of visits in a month
     * presented by a bezier chart
     */
    popularity: TimeSeries[];

    /** Image to be displayed on preview press */
    image?: string;

    /** Last update date */
    updated: Date;

    /** Publish date */
    published: Date;

    /**
     * Pointing arrow appears in the
     * left side or the right side
     * of the card body
     */
    arrowPosition: ARROW_POSITION;
}

/** Dataset used for the bezier chart */
export interface TimeSeries {
    date: Date;
    value: number;
}
