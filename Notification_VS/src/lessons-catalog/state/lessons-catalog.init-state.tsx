import { LessonsCatalogState } from '../interfaces/lessons-catalog.state';

export const lessonsCatalogInitialState: LessonsCatalogState = {
    topicsCatalogModal: false,
    catalogDisplayType: '',
    catalogLayoutType: '',
};