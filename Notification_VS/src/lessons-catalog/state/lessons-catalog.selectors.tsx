import { AppState } from '../../shared/interfaces/app.state';
import { LessonsCatalogState } from '../interfaces/lessons-catalog.state';
import { createSelector, Selector } from 'reselect';

const LESSONS_CATALOG_MODULE: Selector<AppState, LessonsCatalogState> = (state: AppState) => state.lessonsCatalog;

export const TOPICS_CATALOG_MODAL_OPEN = createSelector<AppState, LessonsCatalogState, boolean>(
    LESSONS_CATALOG_MODULE,
    (state: LessonsCatalogState) => state.topicsCatalogModal,
);

export const TOPICS_CATALOG_DISPLAY_TYPE = createSelector<AppState, LessonsCatalogState, string>(
    LESSONS_CATALOG_MODULE,
    (state: LessonsCatalogState) => state.catalogDisplayType,
);