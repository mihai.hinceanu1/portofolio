import * as actions from './lessons-catalog.actions';
import { lessonsCatalogInitialState } from './lessons-catalog.init-state';
import { Action } from '../../shared/interfaces/shared';
import { LessonsCatalogState } from '../interfaces/lessons-catalog.state';

export const lessonsCatalogReducer =
    (state: LessonsCatalogState = lessonsCatalogInitialState, action: Action<any>): LessonsCatalogState => {

        let newState: LessonsCatalogState;

        switch (action.type) {
            case actions.SET_TOPICS_CATALOG_MODAL_OPEN: {
                newState = {
                    ...state,
                    topicsCatalogModal: action.payload as boolean,
                };

                return newState;
            }

            case actions.SET_TOPICS_CATALOG_DISPLAY_TYPE: {
                newState = {
                    ...state,
                    catalogDisplayType: action.payload as string,
                };

                return newState;
            }

            default:
                return state;

        }

    };