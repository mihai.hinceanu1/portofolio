import { Action } from '../../shared/interfaces/shared';

export const SET_TOPICS_CATALOG_MODAL_OPEN = 'SET_TOPICS_CATALOG_MODAL_OPEN';
export const setTopicsCatalogModal = (topicsCatalogModal: boolean): Action<boolean> => ({
    type: SET_TOPICS_CATALOG_MODAL_OPEN,
    payload: topicsCatalogModal,
});

export const SET_TOPICS_CATALOG_DISPLAY_TYPE = 'SET_TOPICS_CATALOG_DISPLAY_TYPE';
export const setTopicsCatalogDisplayType = (catalogDisplayType: string): Action<string> => ({
    type: SET_TOPICS_CATALOG_DISPLAY_TYPE,
    payload: catalogDisplayType,
});