import { Comment, CommentExtended } from './comment';

/**
 * Comment with additional info about the user.
 */
export interface UserComment {
    comment: Comment;
    authorName: string;
}

/**
 * List of topic ids used to associate a topic id with its comments
 */
export interface UserCommentsByTopicId {
    [topicUuid: string]: UserComment[];
}

export interface EditCommentExtended {
    editedContent: string;
    commentId: string;
    topicId: string;
}
/**
 * Config needed to display the modal asking the user to confirm his action
 * before deleting a comment.
 */
export interface DeleteCommentConfirmConfig {
    displayModal: boolean;
}

/**
 * Used when creating a new comment to link it to a topic.
 */
export interface NewComment {
    comment: {
        content: string;
        tooltips: any[];
    };

    topicId: string;
}

/**
 * Used when editing comment, need main topic id to get topic comments after edit.
 */
export interface EditCommentConfig {
    comment: Comment;
    mainTopicId: string;
}

export interface AddCommentActionOk {
    /** Comment received from server */
    comment: CommentExtended;

    /** topicId of the comment */
    topicId: string;
}