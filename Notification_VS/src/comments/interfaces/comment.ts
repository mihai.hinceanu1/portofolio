import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * Comments are added by users and can be associated with any topic or child topic.
 * Comments can be parsed by the NLP algo to create autolinks.
 * <!> `authorId` is used from CommonMetadata to identify which user has written the comment.
 */
export interface Comment extends CommonMetadata {

    content: string;

    /**
     * Autolinks can be created even inside of the user comments.
     * Only text tooltips so far.
     */
    tooltips: any[];
}

export interface CommentExtended extends CommonMetadata {
    content: string;

    tooltips: any[];
}