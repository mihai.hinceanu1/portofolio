import { Comment } from './comment';
import { UserCommentsByTopicId } from './comments-utils';

export interface CommentsState {

    /** This is the full DTO of the current selected comment. */
    comment: Comment;

    /** Stores the comments summary DTOs. */
    comments: Comment[];

    /**
     * After a search query is finished the results are stored here.
     * This search is specialized solely on comments.
     */
    searchCommentsResults: Comment[];

    /**
     * Topic comments with author names.
     */
    topicComments: UserCommentsByTopicId;

    /** Special place where responses will be added when adding a comment */
    addComment: {
        response: string;

        error: string;
    };

    modal: {
        deleteCommentConfirmModal: boolean;
        seeAllCommentsModal: boolean;
    }
}