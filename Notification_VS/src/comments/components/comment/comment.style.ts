import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Comment = styled.View`
    border-bottom-style: solid;
    border-bottom-width: 0.5px;
    border-bottom-color: ${colors.$commentsDivider};
    min-width: 305px;
    margin: 10px 0;
    padding: 10px;
    justify-content: center;
`;

export const Author = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const Avatar = styled.View`
    /* border-radius: 50%; */
    /** On mobile it does not work with percent */
    border-radius: ${25 / 2}px;
    border-color: ${colors.$buttonBgr};
    border-style: solid;
    border-width: 1px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 25px;
    height: 25px;
`;

export const AvatarInitials = styled.Text`
    font-weight: 700;
    font-size: ${font.$size10}px;
    color: ${colors.$linkOnDarkBgr};
`;

export const AuthorName = styled.Text`
    padding-left: 5px;
    font-weight: 600;
    font-size: ${font.$size12}px;
    color: ${colors.$buttonBgr};
`;

export const Content = styled.View`
    width: 100%;
    flex-direction: row;
    align-items: center;
    min-height: 20px;
`;

export const CommentText = styled.Text`
    padding-left: 30px;
    flex: 10;
    font-size: ${font.$size10}px;
`;

export const EditButton = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const DeleteButton = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const OwnerMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const OwnerMenu2 = styled.View`
    width: 100px;
    height: 50px;
`;

export const DateText = styled.Text`
    padding-top: 5px;
    padding-left: 30px;
    font-size: ${font.$size10}px;
    color: ${colors.$textTooltipsBlack};
`;