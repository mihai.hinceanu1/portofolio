import * as div from './comment.style';
import { UserComment } from '../../interfaces/comments-utils';
import moment from 'moment';
import * as React from 'react';
import { Subject } from 'rxjs';
// import { setDeleteCommentConfirmModal } from '../../services/topic-comments.service';

interface Props {
    /** the entire comment object received from parent */
    commentWithAuthor: UserComment;

    /** whether or not to render admin specific functions for comments like delete comment */
    hasAdminRights?: boolean;
    topicId?: string;
    mainTopicId?: string;

    /** if the user didn't write the comment he can't edit/delete it */
    isCommentOwner?: boolean;
}

interface State {
    newContent: string;
}

// TODO delete this after refactoring topic-new
/**
 * Component used to render a single comment, will display the comment content,
 * the author and the date.
 */
export class Comment extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        // TODO REVIEW This looks odd. Why would you set in state things coming from props?
        const { commentWithAuthor } = this.props;
        this.state = {
            newContent: commentWithAuthor.comment.content,
        };
    }

    public render() {
        const { commentWithAuthor } = this.props;
        let date: any;

        if (Object.keys(commentWithAuthor).length) {
            date = moment(commentWithAuthor.comment.created).format('MMMM Do YYYY, h:mm:ss a');
        }

        return (
            <div.Comment data-cy='comment'>

                {
                    !!Object.keys(commentWithAuthor).length &&
                    <div.Author>
                        <div.Avatar data-cy='author-avatar'>
                            <div.AvatarInitials>
                                {this.getInitials(commentWithAuthor.authorName)}
                            </div.AvatarInitials>
                        </div.Avatar>

                        <div.AuthorName data-cy='comment-author'>
                            {commentWithAuthor.authorName}
                        </div.AuthorName>
                    </div.Author>
                }
                {/* <div.Content>
                    {
                        (hasAdminRights || isCommentOwner) &&
                        <div.DeleteButton data-cy='delete-comment-container'>
                            <Icon
                                icon='/taxonomy/delete_grey.png'
                                iconHovered='/taxonomy/delete.png'
                                onHoverOverlayMessage='Delete Comment'
                                onPress={() => this.handleDeleteCommentClick()} />
                        </div.DeleteButton>
                    }
                </div.Content> */}
                {
                    date &&
                    <div.DateText data-cy='comment-date'>{date}</div.DateText>
                }

            </div.Comment>
        );
    }

    public componentDidMount() { }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // private renderContentWithTooltips() {
    //     const { commentWithAuthor } = this.props;
    //     let shouldRender = Object.keys(commentWithAuthor).length;

    //     if (shouldRender) {
    //         let tooltips = commentWithAuthor.comment.tooltips;
    //         let comment = commentWithAuthor.comment.content;

    //         if (tooltips.length > 1) {
    //             this.combineCommentWithTooltips(comment, tooltips);
    //         } else {
    //             this.handleSingleOrNoTooltip(comment, tooltips);
    //         }
    //     }
    // }

    // private combineCommentWithTooltips(comment: string, tooltips: any[]) {
    //     let tooltipEnd = tooltips[0].position.end;

    //     return tooltips.map((tooltip, index) => {
    //         if (tooltipEnd === tooltip.position.end) {
    //             this.getFirstFragment(comment, tooltip, index);
    //         } else {
    //             let previousTooltipEnd = tooltipEnd;
    //             tooltipEnd = tooltip.position.end;
    //             let isLastFragment = index === tooltips.length - 1;

    //             this.getMiddleFragment(comment, tooltip, isLastFragment, previousTooltipEnd, index);
    //         }
    //     });
    // }

    /** Returns first fragment of a comment and its tooltip
     * TODO This case could have been handled by only one component
     */
    // private getFirstFragment(_comment: string, _tooltip: any, _index: number) {
    //     return <Text>Hello</Text>;
    // }

    // /** Returns a middle fragment with its tooltip or the last fragment */
    // private getMiddleFragment(_comment: string, _tooltip: any, _isLastFragment: boolean, _previousTooltipEnd: number, _index?: number) {
    //     return;
    // }

    /** Return a tooltip with its preceding and following comment fragments
     * or just the comment when there are no tooltips.
     */
    // private handleSingleOrNoTooltip(comment: string, tooltips: any[]) {
    //     if (tooltips.length === 1) {
    //         let tooltip = tooltips[0];
    //         this.getMiddleFragment(comment, tooltip, true, 0);
    //     } else {
    //         return comment;
    //     }
    // }

    // private handleTextChange(text: string) {
    //     this.setState({
    //         newContent: text,
    //     });
    // }

    // private handleSaveClick() {
    //     const { commentWithAuthor, mainTopicId } = this.props;
    //     const { newContent } = this.state;

    //     let updatedComment = {
    //         ...commentWithAuthor.comment,
    //         content: newContent,
    //     };

    //     let editCommentConfig = {
    //         comment: updatedComment,
    //         mainTopicId,
    //     };
    //     editCommentConfig;
    // }

    // private handleDeleteCommentClick() {
    //     const { hasAdminRights, isCommentOwner } = this.props;
    //     if (hasAdminRights || isCommentOwner) {
    //         setDeleteCommentConfirmModal(true);
    //     }
    // }

    private getInitials(name: string) {
        const firstInitial = name.charAt(0).toUpperCase();
        let names = name.split(' ');
        const lastInitial = names[names.length - 1].charAt(0).toUpperCase();

        return firstInitial + lastInitial;
    }
}