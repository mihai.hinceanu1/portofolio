import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { colors } from '../../../shared/style/colors';
import { BOX_SHADOW_CSS } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

export const AddCommentForm = styled.View`
    width: 320px;
    flex-direction: row;
    justify-content: center;
    align-self: center;
`;

export const CommentForm = styled.View`
    flex-direction: row;
    padding-left: 5px;
    justify-content: space-between;
    flex-wrap: wrap;
`;

export const TextInput = styled.TextInput`
    height: 25px;
    width: 230px;
    /* outline: none; */
    margin-left: 10px;
`;

export const InputAndButton = styled.View`
    ${BOX_SHADOW_CSS};
    width: 260px;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    height: 45px;
`;

export const Icon = styled.TouchableOpacity<ReactWebAttributes>`
    width: 20px;
    height: 20px;
    margin-right: 10px;
`;

export const Line = styled.View`
    width: 60px;
    height: 3px;
    background-color: ${colors.$menuBtnTextColor};
    align-self: center;
`;