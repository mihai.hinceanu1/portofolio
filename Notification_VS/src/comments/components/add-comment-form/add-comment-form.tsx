import * as div from './add-comment-form.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { createComment } from '../../services/comments.service';
import * as React from 'react';
import { Subject } from 'rxjs';

interface Props {
    topicId: string;
}

interface State {
    isAddCommentPress: boolean;
    comment: string;
    hovered: boolean;
}

// TODO Reuse in the new comments design if possible
export class AddCommentForm extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            isAddCommentPress: false,
            comment: '',
            hovered: false,
        };
    }

    public render() {
        const { comment, hovered } = this.state;

        return (
            <div.AddCommentForm data-cy='add-comment-form'>

                <div.Line />

                <div.InputAndButton data-cy='add-comment-form'>
                    <div.TextInput data-cy='comment-input'
                        underlineColorAndroid='#FFF'
                        placeholder={'Type your comment '}
                        onChangeText={(text) => this.setState({ comment: text })}
                        value={comment}
                    />

                    <div.Icon data-cy='send-comment-button'
                        onMouseEnter={e => this.onMouseEnterButton(e)}
                        onMouseLeave={e => this.onMouseLeaveButton(e)}
                        onPress={() => this.onCommentSendPress()}>
                        {
                            !hovered &&
                            <FitImage imgPath='/topics/send_comment1.png' />
                        }
                        {
                            !!hovered &&
                            <FitImage imgPath='/topics/send_comment2.png' />
                        }
                    </div.Icon>
                </div.InputAndButton>

            </div.AddCommentForm>
        );
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // private toggleCommentPress(bool: boolean) {
    //     this.setState({
    //         isAddCommentPress: bool,
    //         comment: '', /** Should let comment history? */
    //     });
    // }

    // private onCommentInputChange(text: string) {
    //     this.setState({
    //         comment: text,
    //     });
    // }

    private onMouseEnterButton(_event: React.MouseEvent) {
        this.setState({ hovered: true });
    }

    private onMouseLeaveButton(_event: React.MouseEvent) {
        this.setState({ hovered: false });
    }

    private onCommentSendPress() {
        const { topicId } = this.props;
        const { comment } = this.state;

        // object for new comment.
        // it has to have tooltips in order to respect the interface.
        // check if we have something, otherwise don't send anything to db
        /** TODO Let user add tooltips on comments */
        if (comment) {
            let newComment = {
                content: comment,
                tooltips: [] as any[],
            };

            createComment({
                topicId,
                comment: newComment,
            });

            this.setState({
                comment: '',
            });
        }
    }
}
