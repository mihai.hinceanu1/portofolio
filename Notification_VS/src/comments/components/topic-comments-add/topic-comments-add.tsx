import * as div from './topic-comments-add.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { AddCommentForm } from '../add-comment-form/add-comment-form';
import * as React from 'react';
import { Subject } from 'rxjs';

interface Props {
    topicId: string;
}

interface State {
    addCommentForm: boolean;
}

// TODO To reuse in new comments system
export class TopicCommentsAdd extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            addCommentForm: false,
        };
    }

    public render() {
        const { topicId } = this.props;
        const { addCommentForm } = this.state;

        return (
            <div.TopicCommentsAdd data-cy='topic-comments-add'>

                {
                    <div.Icon data-cy='add-comment-button'
                        onPress={() => this.addCommentForm()}>
                        <FitImage imgPath={'/topics/comments/addBlue.png'} />
                    </div.Icon>
                }

                {
                    (addCommentForm) &&
                    <AddCommentForm topicId={topicId} />
                }

            </div.TopicCommentsAdd>
        );
    }

    public componentDidMount() { }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private addCommentForm() {
        this.setState({ addCommentForm: !this.state.addCommentForm });
    }
}