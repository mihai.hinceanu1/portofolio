import styled from 'styled-components/native';

export const Icon = styled.TouchableOpacity`
    width: 20px;
    height: 20px;
    margin: 5px;
`;

export const TopicCommentsAdd = styled.View`
    align-self: flex-start;
    height: 60px;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    left: 10px;
`;
