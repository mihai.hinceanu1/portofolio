import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { Platform } from 'react-native';
import styled from 'styled-components/native';

const isMobile = Platform.OS !== 'web';

export const SPACING_COMMENTS = 15;

export const TopicComment = styled.View`
    margin-top: ${SPACING_COMMENTS}px;
    ${!isMobile ? `width: calc(100% - 40px);` : 'width: 90%'}
    ${isMobile && 'max-height: 300px;'}
`;

export const TopicCommentBottomBorder = styled.View`
    margin-top: ${SPACING_COMMENTS}px;
    ${!isMobile && `border-bottom-width: 1px;
    border-bottom-style: solid;
    border-bottom-color: ${colors.$grey};`}
`;

// name of the author, date of comment
export const AuthorNameDateView = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    height: 20px;
`;

export const AuthorNameText = styled.Text`
    /* font-weight: 500; */
    color: ${colors.$link};
    font-size: ${font.$size12}px;
`;

export const DateText = styled.Text`
    margin-left: 5px;
    font-weight: 300;
    font-size: 12px;
    color: ${colors.$commentDate};
`;

export const DeleteButton = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const ActionIcons = styled.View`
    justify-content: center;
    align-items: center;
    margin-left: 25px;
    align-self: flex-start;
    margin-top: 20px;
`;

// comment text
export const Body = styled.View``;

export const Text = styled.Text`
    font-size: ${font.$size12}px;
`;

export const CommentContent = styled.View`
    flex-direction: row;
    /* width: 320px; */
`;