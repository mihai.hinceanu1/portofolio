import * as div from './topic-comment.style';
import { loginResponse$ } from '../../../auth/services/auth.service';
import { Tooltip } from '../../../shared/components/tooltip/tooltip';
import { CommentExtended } from '../../interfaces/comment';
import moment from 'moment';
import * as React from 'react';
import { Platform } from 'react-native';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { setDeleteCommentConfirmModal } from '../../services/topic-comments.service';
// import { EditCommentExtended } from '../../interfaces/comments-utils';
// import { editTopicComment } from '../../services/comments.service';

interface Props {
    comment: CommentExtended;
    topicId: string;
}

interface State {
    commentTextCopy: string;
    userId: string;
}

const isMobile = Platform.OS !== 'web';

export class TopicComment extends React.Component<Props, State>  {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            commentTextCopy: props.comment.content,
            userId: '',
        };
    }

    public render() {

        const { authorId, created } = this.props.comment;
        let contentWithTooltips = [] as any[];
        // let userId = localStorage.getItem('userId');
        const { userId } = this.state;

        return (
            <div.TopicComment data-cy='topic-comment'>

                <div.Body data-cy='body'>
                    {
                        !!contentWithTooltips.length &&
                        authorId !== userId &&
                        contentWithTooltips.map((fragment, index) =>
                            fragment.isTooltip && !isMobile ?
                                <Tooltip
                                    key={index}
                                    targetedAt='text'
                                    label={fragment.text}
                                    tooltip={fragment.tooltip} />
                                :
                                <div.Text key={index}>{fragment.text}</div.Text>,

                        )
                    }

                    {/* {
                        authorId === userId &&
                        <div.CommentContent data-cy='comment-content'>

                            {
                                authorId === userId &&
                                <div.ActionIcons>
                                    {
                                        <Icon
                                            width={15}
                                            height={15}
                                            icon='/taxonomy/delete_grey.png'
                                            iconHovered='/taxonomy/delete.png'
                                            onPress={() =>
                                                this.handleDeleteCommentClick()} />
                                    }
                                </div.ActionIcons>
                            }
                        </div.CommentContent>
                    } */}

                </div.Body>

                <div.AuthorNameDateView>

                    <div.AuthorNameText>
                        {
                            authorId
                        }
                    </div.AuthorNameText>

                    <div.DateText>
                        {moment(created).format('MMMM Do YYYY, h:mm:ss')}
                    </div.DateText>

                </div.AuthorNameDateView>

                <div.TopicCommentBottomBorder />

            </div.TopicComment>
        );
    }

    public componentDidMount() {
        this.subscribeToLoginResponse();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // private handleDeleteCommentClick() {
    //     setDeleteCommentConfirmModal(true);
    // }

    // private onEditSaveClick(commentId: string) {
    //     let { commentTextCopy } = this.state;
    //     let { topicId } = this.props;

    //     let editCommentConfig: EditCommentExtended = {
    //         editedContent: commentTextCopy,
    //         commentId,
    //         topicId,
    //     };

    //     if (commentTextCopy !== this.props.comment.content) {
    //         editTopicComment(editCommentConfig);
    //     }
    // }

    // private onEditCloseClick() {
    //     this.setState({
    //         commentTextCopy: this.props.comment.content,
    //     });
    // }

    private subscribeToLoginResponse() {
        loginResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                this.setState({
                    userId: response.userId,
                });
            });
    }

    // private getFirstLetters(name: string) {
    //     return name.split(' ').map(field => field[0].toUpperCase());
    // }

    // Used to update the specific fields of the topic
    // private changeText(text: string) {
    //     this.setState({
    //         commentTextCopy: text,
    //     });
    // }
}