import { Platform } from 'react-native';
import styled from 'styled-components/native';

const isMobile = Platform.OS !== 'web';

export const TopicComments = styled.View`
    ${isMobile ? 'width: 80%;' : 'width: 310px;'}
    ${!isMobile && 'margin-left: 30px;'}margin-left: 30px;
`;

export const Button = styled.TouchableOpacity`
    width: 300px;
    margin-bottom: 10px;
`;

export const Text = styled.Text`
    font-weight: 500;
    text-align: center;
`;