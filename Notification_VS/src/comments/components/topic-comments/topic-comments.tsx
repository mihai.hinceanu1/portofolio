import * as div from './topic-comments.style';
import { Comment } from '../../interfaces/comment';
import { TopicComment } from '../topic-comment/topic-comment';
import * as React from 'react';

interface Props {
    comments: Comment[];
    topicId: string;
    commentsIndex: number;
}

interface State {
    newComment: string;
    extendAll: boolean;
    topicStateId: string;
}

// REFACTOR to be reused
export class TopicComments extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            topicStateId: '',
            extendAll: false,
            newComment: null,
        };
    }

    public render() {
        const { comments, topicId, commentsIndex } = this.props;
        const { topicStateId } = this.state;
        return (
            <div.TopicComments data-cy='topic-comments'>
                {
                    !!comments &&
                    <>
                        {
                            comments.length > 0 &&
                            comments.map((comment, index) => {
                                return (commentsIndex > index && commentsIndex !== 101
                                    && topicStateId === topicId &&
                                    <TopicComment topicId={topicId}
                                        comment={comment} key={comment._id} />);
                            },
                            )
                        }
                    </>
                }

            </div.TopicComments>
        );
    }

    public componentDidMount() {
    }
}
