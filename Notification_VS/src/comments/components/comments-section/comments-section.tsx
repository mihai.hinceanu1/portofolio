import * as div from './comments-section.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { ButtonConfig } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { MAX_DISPLAYED_COMMENTS } from '../../constants/comments.const';
import { UserComment } from '../../interfaces/comments-utils';
import { Comment } from '../comment/comment';
import * as React from 'react';
import { LayoutChangeEvent, Text, TouchableOpacity } from 'react-native';

interface Props {
    topicComments: UserComment[];
    topicId: string;
    mainTopicId: string;

    /** OPTIONAL prop sent to comment component telling it whether or not to display admin functionalities */
    hasAdminRights?: boolean;
}

interface State {
    isExtended: boolean;
    height: number;
    newCommentContent: string;
}

/** Renders all comments */
export class CommentsSection extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            isExtended: false,
            height: -1,
            newCommentContent: '',
        };
    }

    public render() {
        const { topicComments, hasAdminRights, topicId, mainTopicId } = this.props;
        const { isExtended, height, newCommentContent } = this.state;
        // const loggedUserId = JSON.parse(localStorage.getItem('userId'));
        const loggedUserId = '';

        let buttonConfig: ButtonConfig = {
            disabled: !newCommentContent.length,
            onPress: () => this.handleSubmitCommentClick(),
            text: 'Submit',
        };

        return (
            <div.CommentsSection data-cy='comments-section' isExtended={isExtended}>

                {/** TODO extend button with a prop @svgIcon so that it can take any name line 'faAddressBook' */}
                {/* <FontAwesomeIcon icon={faAddressBook} style={{ color: 'red' }} /> */}

                {!!topicComments.length &&
                    <div.CommentsList data-cy='comments-list'
                        isExtended={isExtended}
                        height={height}
                        onLayout={ev => this.measureCommentsList(ev)}>
                        {
                            this.handleDisplayedComments().map((comment) =>
                                <Comment key={comment.comment._id}
                                    commentWithAuthor={comment}
                                    hasAdminRights={hasAdminRights}
                                    topicId={topicId}
                                    mainTopicId={mainTopicId}
                                    isCommentOwner={comment.comment.authorId === loggedUserId}
                                />,
                            )

                        }
                    </div.CommentsList>
                }

                {
                    topicComments.length > MAX_DISPLAYED_COMMENTS &&
                    <div.ExpandButton>

                        <TouchableOpacity
                            onPress={() => this.toggleIsExtended()} >

                            <Text style={{
                                color: colors.$menuBtnTextColor,
                                fontWeight: '600', alignSelf: 'center',
                                fontFamily: 'Arial', fontSize: font.$size12,
                            }}>
                                {isExtended ?
                                    'Show less'
                                    :
                                    `Show ${topicComments.length - MAX_DISPLAYED_COMMENTS} more comments`}
                            </Text>
                        </TouchableOpacity>

                    </div.ExpandButton>
                }

                {/** Add new Comment */}
                {
                    !hasAdminRights &&
                    <div.AddNewComment data-cy='new-comment-input'>
                        <Input
                            onChangeText={(text) => this.handleTextChange(text)}
                            value={newCommentContent}
                            placeholder='Your comment here...' />

                        <Button config={buttonConfig}
                            // disabled={!newCommentContent.length}
                            // onPress={() => this.handleSubmitCommentClick()}
                            // text='Submit'
                        />
                    </div.AddNewComment>
                }

            </div.CommentsSection>
        );
    }

    private measureCommentsList(event: LayoutChangeEvent) {
        const { height } = this.state;
        const { topicComments } = this.props;

        if (height === -1 && topicComments.length) {
            let newHeight = event.nativeEvent.layout.height;
            if (newHeight < 125) {
                this.setState({
                    height: 125,
                });
            } else {
                this.setState({
                    height: newHeight,
                });
            }

        }
    }

    private handleDisplayedComments() {
        const { isExtended } = this.state;
        const { topicComments } = this.props;
        if (isExtended || topicComments.length <= MAX_DISPLAYED_COMMENTS) {
            return topicComments;
        } else {
            return topicComments.slice(0, MAX_DISPLAYED_COMMENTS);
        }
    }

    private toggleIsExtended() {
        this.setState({
            isExtended: !this.state.isExtended,
        });
    }

    private handleTextChange(text: string) {
        this.setState({
            newCommentContent: text,
        });
    }

    private handleSubmitCommentClick() {
        // const { newCommentContent } = this.state;
        // const { topicId, mainTopicId } = this.props;
        // let authorId  = JSON.parse(localStorage.getItem('userId'));

        // let newCommentConfig = {
        //     comment: {
        //         content: newCommentContent,
        //         authorId,
        //         tooltips: [] as TextTooltip[],
        //     } as Comment,
        //     topicId,
        //     mainTopicId,
        // } as NewComment;

        // createComment(newCommentConfig);
        this.setState({
            newCommentContent: '',
        });
    }
}