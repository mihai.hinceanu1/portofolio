import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const CommentsSection = styled.View<{ isExtended: boolean }>`
    min-width: ${props => props.isExtended ? '342px' : '325px'};
`;

export const CommentsList = styled.ScrollView<{ isExtended: boolean, height: number }>`
    ${props => props.isExtended && props.height !== -1 ?
        'height:' + props.height + 'px;'
        :
        'min-height: 106px;'
    }
`;

export const ExpandButton = styled.View`
    justify-content: center;
    padding-bottom: 10px;
    width: 100%;
    align-self: center;
    border-bottom-color: ${colors.$commentsDivider};
    border-bottom-style: solid;
    border-bottom-width: 0.5px;
`;

export const AddNewComment = styled.View`
    width: 80%;
    align-self: center;
    align-items: center;
    flex-direction: row;
    justify-content: space-between;
`;
