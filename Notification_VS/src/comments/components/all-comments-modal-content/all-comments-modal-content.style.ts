import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Comments = styled.ScrollView`
    display: flex;
    flex-direction: column;
    align-self: center;
    width: 800px;
    border-bottom-width: 0.5px;
    border-bottom-color: rgb(220,220,220);
    border-bottom-style: dashed;
    margin-top: 20px;
`;

export const Comment = styled.View`
    width: 800px;
    align-self: center;
`;

export const TextContent = styled.Text`
    color: white;
    font-size: ${font.$size12}px;
    margin-bottom: 20px;
    margin-left: 15px;
`;

export const Header = styled.View`
    flex-direction: row;
    align-items: center;
    height: 20px;
    margin-left: 10px;
`;

export const Author = styled.Text`
    font-weight: 600;
    color: white;
`;

export const Date = styled.Text`
    margin-left: 10px;
    font-weight: 300;
    font-size: 12px;
    color: ${colors.$commentDate};
`;