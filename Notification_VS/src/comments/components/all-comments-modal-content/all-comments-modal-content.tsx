import * as div from './all-comments-modal-content.style';
import { CommentExtended } from '../../interfaces/comment';
import moment from 'moment';
import * as React from 'react';
import { ScrollView } from 'react-native';

interface Props {
    comments: CommentExtended[];
}

interface State { }

// TODO REFACTOR to match the new design.
// Or DEPRECATE
export class AllCommentsModalContent extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        const { comments } = this.props;

        return (
            <ScrollView data-cy='all-comments-modal-content' style={{ maxHeight: '500px', width: '850px' }} >

                {
                    comments && comments.map((comm, index) =>
                        <div.Comments key={index}>
                            <div.Header>
                                <div.Author>
                                    {
                                        comm.authorId
                                    }
                                </div.Author>

                                <div.Date>
                                    {moment(comm.created).format('MMMM Do YYYY, h:mm:ss')}
                                </div.Date>
                            </div.Header>

                            <div.Comment>
                                <div.TextContent>
                                    {comm.content}
                                </div.TextContent>
                            </div.Comment>
                        </div.Comments>,
                    )}

            </ScrollView>
        );
    }

}