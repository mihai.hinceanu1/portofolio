import { Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { store, store$ } from '../../shared/services/state.service';
import { Comment } from '../interfaces/comment';
import * as actions from '../state/actions/comments.actions';
import * as sel from '../state/comments.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';
import {
    UserCommentsByTopicId,
    NewComment,
    EditCommentExtended,
} from '../interfaces/comments-utils';

// ====== GET COMMENT ======

export function getComment(id: string) {
    store.dispatch(
        actions.getCommentReq(id),
    );
}

export const comment$ = () => store$.pipe(
    map(state => sel.COMMENT(state)),
    skipWhile(comment => !comment),
    distinctUntilChanged(),
);

// ====== EDIT COMMENT ======

export function editTopicComment(comment: EditCommentExtended) {
    store.dispatch(
        actions.editCommentReq(comment),
    );
}

// ====== CREATE COMMENT ======

export function createComment(newComment: NewComment) {
    store.dispatch(
        actions.createCommentReq(newComment),
    );
}

export const commentResponse$ = (): Observable<string> => store$.pipe(
    map(state => sel.CREATE_COMMENT_RESPONSE(state)),
    skipWhile(response => response === undefined),
    distinctUntilChanged(),
);

// ====== GET ALL COMMENTS ======

export function getAllComments(request?: Paginated) {
    store.dispatch(
        actions.getAllCommentsReq(request),
    );
}

export const comments$ = (): Observable<Comment[]> => store$.pipe(
    map(state => sel.COMMENTS(state)),
    skipWhile(comments => !comments),
    distinctUntilChanged(),
);

// ======  SEARCH COMMENTS ======

export function searchComments(request: PaginatedSearchReq) {
    store.dispatch(
        actions.searchCommentsReq(request),
    );
}

export const commentsSearchResults$ = (): Observable<Comment[]> => store$.pipe(
    map(state => sel.SEARCH_COMMENTS_RESULTS(state)),
    skipWhile(comments => !comments),
    distinctUntilChanged(),
);

// ====== GET COMMENTS WITH AUTHOR NAMES BY TOPIC ID ======

export const topicComments$ = (): Observable<UserCommentsByTopicId> => store$.pipe(
    map(state => sel.TOPIC_COMMENTS(state)),
    skipWhile(topicComments => !topicComments),
    distinctUntilChanged(),
);

export function getTopicComments(mainTopicId: string) {
    store.dispatch(
        actions.getTopicCommentsReq(mainTopicId),
    );
}
