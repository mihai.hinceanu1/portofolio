import { store, store$ } from '../../shared/services/state.service';
import * as topicsModalsActions from '../../topics/state/actions/topics-modals.actions';
import { SEE_ALL_COMMENTS_MODAL } from '../state/comments.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export function setDeleteCommentConfirmModal(isVisibile: boolean) {
    store.dispatch(
        topicsModalsActions.setDeleteCommentConfirmModal(isVisibile),
    );
}

export const seeAllCommentsModal$ = (): Observable<boolean> => store$.pipe(
    map(state => SEE_ALL_COMMENTS_MODAL(state)),
    skipWhile(seeAllCommentsModal => seeAllCommentsModal === undefined),
    distinctUntilChanged(),
);

export function setSeeAllCommentsModal(seeAllCommentsFlag: boolean) {
    store.dispatch(
        topicsModalsActions.setSeeAllCommentsModal(seeAllCommentsFlag),
    );
}