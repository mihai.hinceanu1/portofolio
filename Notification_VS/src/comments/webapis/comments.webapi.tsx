import { APP_CFG } from '../../shared/config/app.config';
import { Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import axios from '../../shared/services/axios.service';
import { Comment, CommentExtended } from '../interfaces/comment';
import { from, Observable } from 'rxjs';
import {
    NewComment,
    UserCommentsByTopicId,
    EditCommentExtended,
} from '../interfaces/comments-utils';

export const getComment = (id: string): Observable<Comment> =>
    from<Promise<Comment>>(
        axios.get<Comment>(`${APP_CFG.api}/comments/comments/${id}`)
            .then(res => res.data),
    );

export const editTopicComment = (commentConfig: EditCommentExtended): Observable<EditCommentExtended> =>
    from<Promise<EditCommentExtended>>(
        axios.put<null>(`${APP_CFG.api}/comments/comment/${commentConfig.commentId}/${commentConfig.topicId}`,
            commentConfig)
            .then(res => res.data),
    );

export const createComment = (newComment: NewComment): Observable<CommentExtended> =>
    from<Promise<CommentExtended>>(
        axios.post<CommentExtended>(`${APP_CFG.api}/comments/comments`, { ...newComment })
            .then(res => res.data),
    );

export const getAllComments = (
    paginated: Paginated = { page: 0, perPage: 20 },
): Observable<Comment[]> =>
    from<Promise<Comment[]>>(
        axios.get<Comment[]>(`${APP_CFG.api}/comments/comments/page/${paginated.page}/${paginated.perPage}`)
            .then(res => res.data),
    );

export const searchCommentsByKeyword = (request: PaginatedSearchReq): Observable<Comment[]> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<Comment[]>>(
        axios.get<Comment[]>(`${APP_CFG.api}/comments/comments/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};

export const getTopicsComments = (mainTopicId: string): Observable<UserCommentsByTopicId> =>
    from<Promise<UserCommentsByTopicId>>(
        axios.get<UserCommentsByTopicId>(`${APP_CFG.api}/comments/comments/topic/${mainTopicId}`)
            .then(res => res.data),
    );