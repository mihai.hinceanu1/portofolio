/** The number of comments displayed by default. */
export const MAX_DISPLAYED_COMMENTS = 5;