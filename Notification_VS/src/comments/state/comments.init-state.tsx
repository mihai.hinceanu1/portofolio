import { UserCommentsByTopicId } from '../interfaces/comments-utils';
import { CommentsState } from '../interfaces/comments.state';

export const commentsInitialState: CommentsState = {
    topicComments: {} as UserCommentsByTopicId,

    /** Add Comment API Call Feedback */
    addComment: {} as any,
    modal: {
        deleteCommentConfirmModal: false,
        seeAllCommentsModal: false,
    }
} as CommentsState;