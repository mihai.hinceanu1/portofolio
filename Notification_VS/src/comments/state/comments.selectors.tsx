import { AppState } from '../../shared/interfaces/app.state';
import { Comment } from '../interfaces/comment';
import { UserCommentsByTopicId } from '../interfaces/comments-utils';
import { CommentsState } from '../interfaces/comments.state';
import { createSelector, Selector } from 'reselect';

const COMMENTS_MODULE: Selector<AppState, CommentsState> = (state: AppState) => state.comments;

/** List comments details in admin */
export const COMMENT = createSelector<AppState, CommentsState, Comment>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.comment,
);

/** List all comments in admin */
export const COMMENTS = createSelector<AppState, CommentsState, Comment[]>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.comments,
);

export const SEARCH_COMMENTS_RESULTS = createSelector<AppState, CommentsState, Comment[]>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.searchCommentsResults,
);

export const TOPIC_COMMENTS = createSelector<AppState, CommentsState, UserCommentsByTopicId>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.topicComments,
);

// ====== CREATE COMMENTS ======
export const CREATE_COMMENT_RESPONSE = createSelector<AppState, CommentsState, string>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.addComment.response,
);

export const CREATE_COMMENT_ERROR = createSelector<AppState, CommentsState, string>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.addComment.error,
);

export const DELETE_COMMENT_CONFIRM_MODAL = createSelector<AppState, CommentsState, boolean>(
    COMMENTS_MODULE,
    (state: CommentsState) => state.modal.deleteCommentConfirmModal,
);

export const SEE_ALL_COMMENTS_MODAL = createSelector<AppState, CommentsState, boolean> (
    COMMENTS_MODULE,
    (state: CommentsState) => state.modal.seeAllCommentsModal,
);