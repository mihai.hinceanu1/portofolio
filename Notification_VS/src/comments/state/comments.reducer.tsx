import * as actions from './actions/comments.actions';
import { commentsInitialState } from './comments.init-state';
import { Comment } from '../interfaces/comment';
import { UserCommentsByTopicId } from '../interfaces/comments-utils';
import { CommentsState } from '../interfaces/comments.state';

export const commentsReducer = (state: CommentsState = commentsInitialState, action: any): CommentsState => {
    switch (action.type) {
        case actions.GET_COMMENT_OK: {
            return {
                ...state,
                comment: action.payload as Comment,
            };
        }

        case actions.GET_ALL_COMMENTS_OK: {
            return {
                ...state,
                comments: action.payload as Comment[],
            };
        }

        case actions.SEARCH_COMMENTS_OK: {
            return {
                ...state,
                searchCommentsResults: action.payload as Comment[],
            };
        }

        case actions.GET_TOPIC_COMMENTS_OK: {
            return {
                ...state,
                topicComments: { ...action.payload } as UserCommentsByTopicId,
            };
        }

        case actions.CREATE_COMMENT_OK: {
            return {
                ...state,
                addComment: {
                    ...state.addComment,
                    response: 'Comment added',
                    error: '',
                },
            };
        }

        case actions.CREATE_COMMENT_FAIL: {
            return {
                ...state,
                addComment: {
                    ...state.addComment,
                    error: action.payload as string,
                    response: '',
                },
            };
        }

        default:
            return state;
    }

};
