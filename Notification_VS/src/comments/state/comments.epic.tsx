import * as commentsActions from './actions/comments.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { Action, Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { EditCommentExtended, NewComment } from '../interfaces/comments-utils';
import * as commentsWebapi from '../webapis/comments.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
// import { Comment } from '../interfaces/comment';

export const getComment$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(commentsActions.GET_COMMENT_REQ).pipe(
        map(action => action.payload),
        concatMap(id => commentsWebapi.getComment(id).pipe(
            map(comment => commentsActions.getCommentOk(comment)),
            catchError(error => of(commentsActions.getCommentFail(error))),
        )),
    );

export const editTopicComment$ = (action$: ActionsObservable<Action<EditCommentExtended>>, _store: Store<AppState>) =>
            action$.ofType(commentsActions.EDIT_COMMENT_REQ).pipe(
                map(action => action.payload),
            concatMap((editCommentExtendedConfig) => commentsWebapi.editTopicComment(editCommentExtendedConfig).pipe(
                map((response) => {
                    return commentsActions.editCommentOk(response);
                }),

                catchError(error => of(commentsActions.editCommentFail(error))),
            )),
            );

export const createComment$ = (action$: ActionsObservable<Action<NewComment>>, _store: Store<AppState>) =>
    action$.ofType(commentsActions.CREATE_COMMENT_REQ).pipe(
        map(action => action.payload),
        concatMap(newComment => commentsWebapi.createComment(newComment).pipe(
            map(comment => {
                const { topicId } = newComment;

                return commentsActions.createCommentOk({ comment, topicId });
            }),
            catchError(error => of(commentsActions.createCommentFail(error))),
        )),
    );

export const getAllComments$ = (action$: ActionsObservable<Action<Paginated>>, _store: Store<AppState>) =>
    action$.ofType(commentsActions.GET_ALL_COMMENTS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => commentsWebapi.getAllComments(request).pipe(
            map(comments => commentsActions.getAllCommentsOk(comments)),
            catchError(error => of(commentsActions.getAllCommentsFail(error))),
        )),
    );

export const searchComments = (action$: ActionsObservable<Action<PaginatedSearchReq>>, _store: Store<AppState>) =>
    action$.ofType(commentsActions.SEARCH_COMMENTS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => commentsWebapi.searchCommentsByKeyword(request).pipe(
            map(comments => commentsActions.searchCommentsOk(comments)),
            catchError(error => of(commentsActions.searchCommentsFail(error))),
        )),
    );

export const getTopicsComments$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(commentsActions.GET_TOPIC_COMMENTS_REQ).pipe(
        map(action => action.payload),
        concatMap(mainTopicId => commentsWebapi.getTopicsComments(mainTopicId).pipe(
            map(topicComments => commentsActions.getTopicCommentsOk(topicComments)),
            catchError(error => of(commentsActions.getTopicCommentsFail(error))),
        )),
    );
