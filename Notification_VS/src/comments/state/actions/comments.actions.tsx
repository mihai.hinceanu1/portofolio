import { Action, Paginated, PaginatedSearchReq } from '../../../shared/interfaces/shared';
import { Comment } from '../../interfaces/comment';
import {
    UserCommentsByTopicId,
    NewComment,
    AddCommentActionOk,
    EditCommentExtended,
} from '../../interfaces/comments-utils';

// ====== GET COMMENT ======

export const GET_COMMENT_REQ = 'GET_COMMENT_REQ';
export const getCommentReq = (id: string): Action<string> => ({
    type: GET_COMMENT_REQ,
    payload: id,
});

export const GET_COMMENT_OK = 'GET_COMMENT_OK';
export const getCommentOk = (comment: Comment): Action<Comment> => ({
    type: GET_COMMENT_OK,
    payload: comment,
});

export const GET_COMMENT_FAIL = 'GET_COMMENT_FAIL';
export const getCommentFail = (error: Error): Action<Error> => ({
    type: GET_COMMENT_FAIL,
    payload: error,
});

// ====== EDIT COMMENT NEW ====
export const EDIT_COMMENT_REQ = 'EDIT_COMMENT_REQ';
export const editCommentReq = (editCommentNewConfig: EditCommentExtended): Action<EditCommentExtended> => ({
    type: EDIT_COMMENT_REQ,
    payload: editCommentNewConfig,
});

export const EDIT_COMMENT_OK = 'EDIT_COMMENT_OK';
export const editCommentOk = (response: EditCommentExtended) => ({
    type: EDIT_COMMENT_OK,
    payload: response,
});

export const EDIT_COMMENT_FAIL = 'EDIT_COMMENT_FAIL';
export const editCommentFail = (error: Error): Action<Error> => ({
    type: EDIT_COMMENT_FAIL,
    payload: error,
});

// ====== CREATE COMMENT ======
export const CREATE_COMMENT_REQ = 'CREATE_COMMENT_REQ';
export const createCommentReq = (newComment: NewComment): Action<NewComment> => ({
    type: CREATE_COMMENT_REQ,
    payload: newComment,
});

export const CREATE_COMMENT_OK = 'CREATE_COMMENT_OK';
export const createCommentOk = (response: AddCommentActionOk): Action<AddCommentActionOk> => ({
    type: CREATE_COMMENT_OK,
    payload: response,
});

export const CREATE_COMMENT_FAIL = 'CREATE_COMMENT_FAIL';
export const createCommentFail = (error: Error): Action<Error> => ({
    type: CREATE_COMMENT_FAIL,
    payload: error,
});

// ====== GET ALL COMMENTS ======

export const GET_ALL_COMMENTS_REQ = 'GET_ALL_COMMENTS_REQ';
export const getAllCommentsReq = (request: Paginated): Action<Paginated> => ({
    type: GET_ALL_COMMENTS_REQ,
    payload: request,
});

export const GET_ALL_COMMENTS_OK = 'GET_ALL_COMMENTS_OK';
export const getAllCommentsOk = (comments: Comment[]): Action<Comment[]> => ({
    type: GET_ALL_COMMENTS_OK,
    payload: comments,
});

export const GET_ALL_COMMENTS_FAIL = 'GET_ALL_COMMENTS_FAIL';
export const getAllCommentsFail = (error: Error): Action<Error> => ({
    type: GET_ALL_COMMENTS_FAIL,
    payload: error,
});

// ====== SEARCH COMMENTS ======

export const SEARCH_COMMENTS_REQ = 'SEARCH_COMMENTS_REQ';
export const searchCommentsReq = (request: PaginatedSearchReq): Action<PaginatedSearchReq> => ({
    type: SEARCH_COMMENTS_REQ,
    payload: request,
});

export const SEARCH_COMMENTS_OK = 'SEARCH_COMMENTS_OK';
export const searchCommentsOk = (comments: Comment[]): Action<Comment[]> => ({
    type: SEARCH_COMMENTS_OK,
    payload: comments,
});

export const SEARCH_COMMENTS_FAIL = 'SEARCH_COMMENTS_FAIL';
export const searchCommentsFail = (error: Error): Action<Error> => ({
    type: SEARCH_COMMENTS_FAIL,
    payload: error,
});

// ====== GET COMMENTS WITH AUTHOR NAMES BY TOPIC ID ======
export const GET_TOPIC_COMMENTS_REQ = 'GET_TOPIC_COMMENTS_REQ';
export const getTopicCommentsReq = (mainTopicId: string): Action<string> => ({
    type: GET_TOPIC_COMMENTS_REQ,
    payload: mainTopicId,
});

export const GET_TOPIC_COMMENTS_OK = 'GET_TOPIC_COMMENTS_OK';
export const getTopicCommentsOk = (topicComments: UserCommentsByTopicId): Action<UserCommentsByTopicId> => ({
    type: GET_TOPIC_COMMENTS_OK,
    payload: topicComments,
});

export const GET_TOPIC_COMMENTS_FAIL = 'GET_TOPIC_COMMENTS_FAIL';
export const getTopicCommentsFail = (error: Error): Action<Error> => ({
    type: GET_TOPIC_COMMENTS_FAIL,
    payload: error,
});
