import { Lesson } from '../../lessons/interfaces/lesson';

export interface LessonEditorState {
    /** Main lesson that is currently in edit mode */
    lesson: Lesson;
}