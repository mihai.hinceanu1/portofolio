import { Lesson } from '../../lessons/interfaces/lesson';
import axios from '../../shared/services/axios.service';
import { from, Observable } from 'rxjs';

export const getEditorLesson = (pathname: string): Observable<Lesson> =>
    from<Promise<Lesson>>(
        axios.get<Lesson>(`/lessons/lesson-admin/${pathname}`)
            .then(res => res.data)
    );
