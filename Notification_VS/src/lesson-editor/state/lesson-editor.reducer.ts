import * as actions from './lesson-editor.actions';
import { lessonEditorInitialState } from './lesson-editor.init-state';
import { Action } from '../../shared/interfaces/shared';
import { LessonEditorState } from '../interfaces/lesson-editor.state';

export const lessonEditorReducer = (
    state: LessonEditorState = lessonEditorInitialState,
    action: Action<any>,
): LessonEditorState => {
    switch (action.type) {
        case actions.GET_EDITOR_LESSON_OK: {
            return {
                ...state,
                lesson: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};
