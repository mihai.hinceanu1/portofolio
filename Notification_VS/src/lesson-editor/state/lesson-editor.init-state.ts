import { LessonEditorState } from '../interfaces/lesson-editor.state';

export const lessonEditorInitialState: LessonEditorState = {
    lesson: null,
};
