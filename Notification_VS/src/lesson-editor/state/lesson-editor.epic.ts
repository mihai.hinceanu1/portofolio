import * as actions from './lesson-editor.actions';
import { Action } from '../../shared/interfaces/shared';
import * as webapi from '../webapis/lesson-editor.webapi';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getLessonEditor$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(actions.GET_EDITOR_LESSON_REQ).pipe(
        map(action => action.payload),
        concatMap(pathname => webapi.getEditorLesson(pathname).pipe(
            map(lesson => actions.getEditorLessonOk(lesson)),
            catchError(error => of(actions.getEditorLessonFail(error)))
        ))
    );
