import { Lesson } from '../../lessons/interfaces/lesson';
import { Action } from '../../shared/interfaces/shared';

export const GET_EDITOR_LESSON_REQ = 'GET_EDITOR_LESSON_REQ';
export const getEditorLessonReq = (pathname: string): Action<string> => ({
    type: GET_EDITOR_LESSON_REQ,
    payload: pathname,
});

export const GET_EDITOR_LESSON_OK = 'GET_EDITOR_LESSON_OK';
export const getEditorLessonOk = (lesson: Lesson): Action<Lesson> => ({
    type: GET_EDITOR_LESSON_OK,
    payload: lesson,
});

export const GET_EDITOR_LESSON_FAIL = 'GET_EDITOR_LESSON_FAIL';
export const getEditorLessonFail = (error: Error): Action<Error> => ({
    type: GET_EDITOR_LESSON_FAIL,
    payload: error,
});
