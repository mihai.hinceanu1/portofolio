import { Lesson } from '../../lessons/interfaces/lesson';
import { AppState } from '../../shared/interfaces/app.state';
import { LessonEditorState } from '../interfaces/lesson-editor.state';
import { createSelector, Selector } from 'reselect';

const LESSON_EDITOR_MODULE: Selector<AppState, LessonEditorState> = (state: AppState) => state.lessonEditor;

export const LESSON = createSelector<AppState, LessonEditorState, Lesson>(
    LESSON_EDITOR_MODULE,
    state => state.lesson,
);
