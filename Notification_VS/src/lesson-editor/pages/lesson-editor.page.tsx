import { LessonExplorer } from '../../lesson-explorer/components/lesson-explorer';
import { MainTitleInspector } from '../../lesson-inspector/components/main-title/main-title-inspector';
import { Lesson } from '../../lessons/interfaces/lesson';
import { getLessonEditor, lessonEditor$ } from '../services/lesson-editor.service';
import * as React from 'react';
import { Text, View } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Params {
    pathname: string;
}
interface Props extends RouteComponentProps<Params> {}
interface State {
    lesson: Lesson;
}

/**
 * Renders Lesson Editor Page with multiple smart and dump components (Lesson Navigator etc...)
 */
class _LessonEditorPage extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            lesson: null,
        };
    }

    public render() {
        const { lesson } = this.state;

        if (!lesson) {
            return <Text>Loading...</Text>;
        }

        return (
            <View>
                <LessonExplorer lesson={lesson} />

                <MainTitleInspector />
            </View>
        );
    }

    public componentDidMount() {
        this.subscribeToLessonEditor();
        this.loadLesson();
    }

    private subscribeToLessonEditor() {
        lessonEditor$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(lesson => {
                this.setState({ lesson });
            });
    }

    private loadLesson() {
        const { pathname } = this.props.match.params;

        getLessonEditor(pathname);
    }
}

export const LessonEditorPage = withRouter<Props, any>(_LessonEditorPage);
