/// <reference types="cypress" />

/** <!>TODO Update tests */
describe('Lesson editor tests', () => {
    before('Login', () => {
        cy.login();
    });

    it('Can access topic admin', () => {
        // no need to open another server instance because we've opened one in login command
        // and login will be executed in the first test.
        cy.route({ method: 'GET', url: '/api/lessons/lesson-admin/*' }).as('lessonAdminEP');

        cy.get('[data-cy=page-button]').eq(2).click();
        cy.wait('@lessonAdminEP');
        cy.url().should('include', 'http://localhost:3000/admin/lesson-editor');
    });
});