import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { LessonEditorPage } from '../pages/lesson-editor.page';
import * as React from 'react';
import { Route } from 'react-router';

/** Lesson Editor Admin Routes */
export const lessonEditorAdminRoutes = (loginResponse: LoginResponse, key: string) => {
    return (
        <Route path='/admin/lesson-editor/:pathname' key={key}
            render={props => renderPage(props, true, '/auth/login', LessonEditorPage, loginResponse)} />
    );
};