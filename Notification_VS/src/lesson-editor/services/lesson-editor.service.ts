import { Lesson } from '../../lessons/interfaces/lesson';
import { store, store$ } from '../../shared/services/state.service';
import * as actions from '../state/lesson-editor.actions';
import * as selectors from '../state/lesson-editor.selectors';
import { Observable } from 'rxjs';
import { map, skipWhile } from 'rxjs/operators';

export function getLessonEditor(pathname: string): void {
    store.dispatch(
        actions.getEditorLessonReq(pathname),
    );
}

export const lessonEditor$ = (): Observable<Lesson> => store$.pipe(
    map(state => selectors.LESSON(state)),
    skipWhile(lesson => !lesson),
);
