import { APP_CFG } from '../../shared/config/app.config';
import { Paginated, PaginatedChildrenReq, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { TopicImageEdit } from '../../topics/interfaces/topic-edit-DELETE';
import { Image } from '../interfaces/image';
import axios from 'axios';
import { from, Observable } from 'rxjs';

export const getImage = (id: string): Observable<Image> =>
    from<Promise<Image>>(
        axios.get<Image>(`${APP_CFG.api}/images/images/${id}`)
            .then(res => res.data),
    );

export const editImage = (image: Image): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`${APP_CFG.api}/images/images/${image._id}`, image)
            .then(res => res.data),
    );

export const deleteImage = (id: string): Observable<null> =>
    from<Promise<null>>(
        axios.delete(`${APP_CFG.api}/images/images/${id}`)
            .then(res => res.data),
    );

export const createImage = (image: Image): Observable<null> =>
    from<Promise<null>>(
        axios.post<null>(`${APP_CFG.api}/images/images`, image)
            .then(res => res.data),
    );

export const getAllImages = ({ page = 0, perPage = 20 }: Paginated): Observable<Image[]> =>
    from<Promise<Image[]>>(
        axios.get<Image[]>(`${APP_CFG.api}/images/images/${page}/${perPage}`)
            .then(res => res.data),
    );

export const searchImagesByKeyword = (request: PaginatedSearchReq): Observable<Image[]> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<Image[]>>(
        axios.get<Image[]>(`${APP_CFG.api}/images/images/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};

export const getTopicImages = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<Image[]> =>
    from<Promise<Image[]>>(
        axios.get<Image[]>(`${APP_CFG.api}/images/images/topic/${uuid}/${page}/${perPage}`)
            .then(res => res.data),
    );

export const getGalleryImages = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<Image[]> =>
    from<Promise<Image[]>>(
        axios.get<Image[]>(`${APP_CFG.api}/images/images/images/${uuid}/${page}/${perPage}`)
            .then(res => res.data),
    );

export const editTopicImages = (imageIdAndTopic: TopicImageEdit): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`/topics/topics/images/${imageIdAndTopic.imageId}`,
            imageIdAndTopic.topic,
        )
            .then(res => res.data),
    );
