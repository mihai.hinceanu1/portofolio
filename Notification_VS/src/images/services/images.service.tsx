import { Paginated, PaginatedChildrenReq, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { store, store$ } from '../../shared/services/state.service';
import { TopicImageEdit } from '../../topics/interfaces/topic-edit-DELETE';
import { TopicsDict } from '../../topics/interfaces/topics';
import { UNIQUE_TOPICS } from '../../topics/state/topics.selectors';
import { Image } from '../interfaces/image';
import * as actions from '../state/images.actions';
import * as sel from '../state/images.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== GET IMAGE ======

export function getImage(id: string) {
    store.dispatch(
        actions.getImageReq(id),
    );
}

export const image$ = () => store$.pipe(
    map(state => sel.IMAGE(state)),
    skipWhile( image => !image ),
    distinctUntilChanged(),
);

// ====== EDIT IMAGE ======

export function editImage(image: Image) {
    store.dispatch(
        actions.editImageReq(image),
    );
}

// ====== DELETE IMAGE ======

export function deleteImage(id: string) {
    store.dispatch(
        actions.deleteImage(id),
    );
}

// ====== CREATE IMAGE ======

export function createImage(image: Image) {
    store.dispatch(
        actions.createImageReq(image),
    );
}

// ====== GET ALL IMAGES ======

export function getAllImages(request?: Paginated) {
    store.dispatch(
        actions.getAllImagesReq(request),
    );
}

export const images$ = (): Observable<Image[]> => store$.pipe(
    map(state => sel.IMAGES(state)),
    skipWhile( images => !images ),
    distinctUntilChanged(),
);

// ======  SEARCH IMAGES ======

export function searchImages(request: PaginatedSearchReq) {
    store.dispatch(
        actions.searchImagesReq(request),
    );
}

export const imagesSearchResults$ = (): Observable<Image[]> => store$.pipe(
    map(state => sel.SEARCH_IMAGES_RESULTS(state)),
    skipWhile( images => !images ),
    distinctUntilChanged(),
);

// ====== GET GALLERY IMAGES ======

/** Get the images of a gallery in paginated list. */
export function getGalleryImages(request: PaginatedChildrenReq) {
    store.dispatch(
        actions.getGalleryImagesReq(request),
    );
}

export const galleryImages$ = (): Observable<Image[]> => store$.pipe(
    map(state => sel.GALLERY_IMAGES(state)),
    skipWhile( images => !images ),
    distinctUntilChanged(),
);

// ====== GET TOPIC IMAGES ======

/** Get the images of a topic in paginated list. */
export function getTopicImages(request: PaginatedChildrenReq) {
    store.dispatch(
        actions.getTopicImagesReq(request),
    );
}

export const topicImages$ = (): Observable<Image[]> => store$.pipe(
    map(state => sel.TOPIC_IMAGES(state)),
    skipWhile( images => !images ),
    distinctUntilChanged(),
);

export const addImageModalOpen$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.ADD_IMAGE_MODAL_OPEN(state)),
    skipWhile(addNewImageModalOpen => addNewImageModalOpen === undefined),
    distinctUntilChanged(),
);

export function setAddImageModalOpen(addImageModalOpen: boolean) {
    store.dispatch(
        actions.setAddImageModalOpen(addImageModalOpen),
    );
}

// ====== EDIT TOPICS IMAGES ======

export function editTopicImages(imageIdAndTopic: TopicImageEdit) {
    store.dispatch(
        actions.editTopicImagesReq(imageIdAndTopic),
    );
}

export const uniqueTopics$ = (): Observable<TopicsDict> => store$.pipe(
    map(state => UNIQUE_TOPICS(state)),
    skipWhile(topics => !topics),
    distinctUntilChanged(),
);
