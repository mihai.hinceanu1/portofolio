import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * Images are stored separately in the db. Each topic can have an image.
 * <!> Server webapi adds related DTOs data depending on the usecase.
 */
export interface Image extends CommonMetadata {
    /** Image description */
    name: string;

    /** Path of the image file on disk (src) */
    pathName: string;

    /** Image description */
    description: string;

    /** Tooltips */
    tooltips?: any[];
}