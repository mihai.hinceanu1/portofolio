import { Image } from './image';

/**
 * Dictionary containing all images from all higher order business objects.
 * This format is preferred by the lesson endpoint for easier maintenance.
 */
export interface ImagesDict {
    sliders: Image[];
}