import { Image } from './image';

export interface ImagesState {

    /** This is the full DTO of the current selected image. */
    image: Image;

    /** Stores the images summry DTOs. */
    images: Image[];

    /** Images from a gallery. */
    galleryImages: Image[];

    /** Images from a topic. */
    topicImages: Image[];

    modal: {
        addImageModal: boolean,
    };

    /**
     * After a search query is finished the results are stored here.
     * This search is specialised solely on images.
     */
    searchImagesResults: Image[];

}