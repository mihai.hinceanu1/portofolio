import { Image } from './image';
import { Trigger, TriggerAction } from '../../player/interfaces/triggers';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * Images sliders are containers of images.
 * Slide are designed such way to explain basic concepts trough interactive graphics.
 *
 * <!> These fields contain referenced data in a simple to use format on the frontend.
 * They are populated after a mongodb document was returned.
 */
export interface ImagesSlider extends ImagesSliderCommon {

    images?: Image[];

    /**
     * Actions dispatched when block is loaded.
     * These actions can control the initial state of the block.
     * For blocks that are rendered as floating blocks, initial state is mandatory.
     */
    onInit?: TriggerAction[];

    /**
     * Triggers configured to execute certain actions on events
     * For example, scrolling down past a threshold can trigger a block to transition in visibility.
     */
    triggers?: Trigger[];

}

/**
 * Metadata of the lesson.
 * These are fields that keep the same types both in the DB and the React app.
 */
export interface ImagesSliderCommon extends CommonMetadata {
    // So far no metadata
}

/**
 * References of the lesson object.
 * These fields are used in Mongoose to store references as UUIDS.
 * After a document is retrieved they will be overwritten with referenced data.
 */
export interface ImagesSliderDto extends ImagesSliderCommon {

    images: string[];

}