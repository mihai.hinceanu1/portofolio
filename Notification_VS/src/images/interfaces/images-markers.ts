/**
 * Markers are used to isolate image regions.
 * These markers are then used as anchors for comments, sentiments and nano lessons.
 * Image markers can be user defined.
 */
export interface ImageMarker extends ImageMarkerCommon {

    // TODO REFACTOR when all interfaces are ready
    nanoLesson: any;
    comments: any;
    sentiment: any;
}

/** Common metadata between server and frontend. */
export interface ImageMarkerCommon {

    /** Marker area defined as image dimensions percents. */
    position: {
        x: number;
        y: number;
        width: number;
        height: number;
    };
}

/** References to other objects in mongoDB */
export interface ImageMarkerDto extends ImageMarkerCommon {
    nanoLesson: string;
    comments: string;
    sentiment: string;
}