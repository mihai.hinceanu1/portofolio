import * as div from './topic-images.style';
import { ImageWithTooltips } from '../../../shared/components/image-with-tooltips/image-with-tooltips';
import { Image } from '../../interfaces/image';
import * as React from 'react';

interface Props {
    images: Image[];
    isExpertOn?: boolean;
}

export const TopicImages: React.SFC<Props> = (props: Props) => {

    const { images, isExpertOn } = props;

    // TODO Create separate TopicImage component
    return (
        <div.TopicImages data-cy='topic-images'>

            {
                !!images.length &&
                images.map(image =>
                    <ImageWithTooltips key={image._id}
                        tooltips={image.tooltips}
                        imagePath={image.pathName}
                        imageDescription={image.description}
                        isExpertOn={isExpertOn}
                    />,
                )
            }

        </div.TopicImages>
    );
};

// function getImageTooltips(image: Image, tooltips: ImageTooltip[]) {
//     let imageTooltips: ImageTooltip[] = [];

//     imageTooltips = tooltips.filter(tooltip => tooltip.imageId === image._id);

//     return imageTooltips;
// }