import * as div from './selected-image.style';
import { GraphDimensions } from '../../../graphs/interfaces/graphs';
import { Button } from '../../../shared/components/button/button';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { Input } from '../../../shared/components/input/input';
import { ButtonConfig } from '../../../shared/interfaces/button';
import { genMongoUuid } from '../../../shared/services/utils.service';
import { ITopic } from '../../../topics/interfaces/topic';
import { editTopic } from '../../../topics/services/topics.service';
import { Image } from '../../interfaces/image';
import * as React from 'react';

interface Props {
    resetSelectedImage: () => void;
    imageDimensions: GraphDimensions;
    selectedImage: Image;
    tooltips: any[];
    topic: ITopic;
}

interface State {
    selectMode: boolean;
    // addedTooltips: ImageTooltip[];
    tooltip: any;
    requiredError: boolean;
}

export class SelectedImage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            // addedTooltips: [],
            selectMode: false,
            tooltip: {
                _id: '',
                topicId: '',
                name: '',
                isAutoLink: false,
                weight: 0,
                isPrimer: false,
                isRemember: false,
                role: '',
                content: '',
                thumbnail: '',
                imageId: '',
                position: {
                    x: 0,
                    y: 0,
                    width: 20,
                    height: 20,
                },
            },
            requiredError: false,
        };
    }

    public render() {
        const { imageDimensions,
            selectedImage } = this.props;

        const { tooltip, requiredError, selectMode } = this.state;

        let stopSelectBtnConfig: ButtonConfig = {
            onPress: () => this.setSelectMode(),
            text: 'Stop select mode',
        };

        let saveBtnConfig: ButtonConfig = {
            onPress: () => this.saveTooltip(),
            text: 'Save',
        };

        let cancelBtnConfig: ButtonConfig = {
            onPress: () => this.resetTooltip(),
            text: 'Cancel',
        };

        let addTooltipBtnConfig: ButtonConfig = {
            onPress: () => this.setSelectMode(),
            text: 'Add tooltip',
        };

        return (
            selectMode ?
                <div.SelectedImage data-cy='selected-image'>
                    <div.ImageTopMenu>
                        {/* <div.BackIcon>
                            <Icon icon='/shared/arrow-big-left.png'
                                onPress={resetSelectedImage} />
                        </div.BackIcon> */}

                        <Button config={stopSelectBtnConfig}
                        // onPress={() => this.setSelectMode()}
                        // text='Stop select mode'
                        />

                        <div.ModalText>
                            You are now in select mode.
                        </div.ModalText>
                    </div.ImageTopMenu>

                    <div.TooltipMenu>
                        <div.Image>
                            <div.SelectedImage2 dim={imageDimensions}>
                                {
                                    !!Object.keys(selectedImage).length ?
                                        <FitImage imgPath={selectedImage.pathName} />
                                        :
                                        <div.ModalText>
                                            No selected image.
                                        </div.ModalText>
                                }
                            </div.SelectedImage2>
                        </div.Image>

                        <div.TooltipForm>
                            {
                                requiredError &&
                                <div.ModalText>
                                    Please fill all required fields.
                                </div.ModalText>
                            }

                            <div.ModalText>
                                * - required field
                            </div.ModalText>

                            <Input onChangeText={(topDistance: string) =>
                                this.changeTooltipPositions(topDistance, 'y')}
                                label='Top Distance*'
                                value={tooltip.position.y.toString()}
                                numeric={true} />

                            <Input onChangeText={(leftDistance: string) =>
                                this.changeTooltipPositions(leftDistance, 'x')}
                                label='Left Distance*'
                                value={tooltip.position.x.toString()}
                                numeric={true} />

                            <Input onChangeText={(width: string) =>
                                this.changeTooltipDimensions(width, 'width')}
                                label='Width*'
                                value={tooltip.position.width.toString()}
                                numeric={true} />

                            <Input onChangeText={(height: string) =>
                                this.changeTooltipDimensions(height, 'height')}
                                label='Height*'
                                value={tooltip.position.height.toString()}
                                numeric={true} />

                            <Input onChangeText={(name: string) =>
                                this.changeTooltip(name, 'name')}
                                label='Name*'
                                value={tooltip.name} />

                            <Input onChangeText={(content: string) =>
                                this.changeTooltip(content, 'content')}
                                label='Content'
                                value={tooltip.content} />

                            <Input onChangeText={(role: string) =>
                                this.changeTooltip(role, 'role')}
                                label='Role*'
                                value={tooltip.role} />

                            <Button config={saveBtnConfig}
                            // onPress={() => this.saveTooltip()}
                            // text='Save'
                            />

                            <Button config={cancelBtnConfig}
                            // onPress={() => this.resetTooltip()}
                            // text='Cancel'
                            />
                        </div.TooltipForm>

                    </div.TooltipMenu>

                </div.SelectedImage>

                :

                <div.SelectedImage>
                    <div.ImageTopMenu>
                        {/* <div.BackIcon>
                            <Icon icon='/shared/arrow-big-left.png'
                                onPress={resetSelectedImage} />
                        </div.BackIcon> */}

                        <Button config={addTooltipBtnConfig}
                        // onPress={() => this.setSelectMode()}
                        // text='Add tooltip'
                        />
                    </div.ImageTopMenu>

                    <div.SelectedImage2 dim={imageDimensions}>
                        {
                            !!Object.keys(selectedImage).length ?
                                <FitImage imgPath={selectedImage.pathName} />
                                :
                                <div.ModalText>
                                    No selected image.
                                </div.ModalText>
                        }
                    </div.SelectedImage2>
                </div.SelectedImage>
        );
    }

    private setSelectMode() {
        this.setState({
            selectMode: !this.state.selectMode,
            requiredError: false,
        });
    }

    private changeTooltipPositions(value: string, type: string) {
        let numberValue = +value;
        const { imageDimensions } = this.props;
        const { tooltip } = this.state;

        if (type === 'x') {
            // Check if value fits in image dimensions.
            if (numberValue >= 0 && numberValue < imageDimensions.width - 20) {

                tooltip.position.x = numberValue;
                this.setState({
                    ...this.state,
                    tooltip: {
                        ...this.state.tooltip,
                        position: {
                            ...this.state.tooltip.position,
                            x: numberValue,
                        },
                    },
                });
            }
        } else {
            if (type === 'y') {
                // Check if value fits in image dimensions.
                if (numberValue >= 0 && numberValue < imageDimensions.height - 20) {

                    tooltip.position.y = numberValue;
                    this.setState({
                        ...this.state,
                        tooltip: {
                            ...this.state.tooltip,
                            position: {
                                ...this.state.tooltip.position,
                                y: numberValue,
                            },
                        },
                    });
                }
            }
        }
    }

    private changeTooltipDimensions(value: string, type: string) {
        let numberValue = +value;
        const { imageDimensions } = this.props;
        const { tooltip } = this.state;

        if (numberValue >= 0 &&
            numberValue + tooltip.position[`${type === 'width' ? 'x' : 'y'}`] < imageDimensions[type]) {

            if (numberValue + tooltip.position[`${type === 'width' ? 'x' : 'y'}`] > imageDimensions[type]) {
                numberValue = imageDimensions[type] - tooltip.position[`${type === 'width' ? 'x' : 'y'}`];
            }
            this.setState({
                ...this.state,
                tooltip: {
                    ...this.state.tooltip,
                    position: {
                        ...this.state.tooltip.position,
                        [type]: numberValue,
                    },
                },
            });
        }
    }

    private changeTooltip(text: string, type: string) {
        this.setState({
            tooltip: {
                ...this.state.tooltip,
                [type]: text,
            },
        });
    }

    private saveTooltip() {
        const { tooltip } = this.state;

        if (tooltip.name.trim() === '' || tooltip.role.trim() === '') {
            this.setState({
                requiredError: true,
            });
        } else {

            /**
             * TODO Create separate EP to add tooltip in topic.
             * Send all data from FE, generate id from BE.
             */
            const { topic, selectedImage } = this.props;
            const { tooltip } = this.state;
            // Create a copy for tooltip
            let tooltipCopy = { ...tooltip };
            // Generate an id
            tooltipCopy._id = genMongoUuid();
            // Asign topic id
            tooltipCopy.topicId = topic._id;
            // Asign image id
            tooltipCopy.imageId = selectedImage._id;
            // Create a topic copy
            let topicCopy = { ...topic };

            // topicCopy.tooltips.image.push(tooltipCopy); // RESTORE
            editTopic(topicCopy);

            this.setState({
                requiredError: false,
                tooltip: {
                    ...this.state.tooltip,
                    _id: tooltipCopy._id,
                    topicId: topic._id,
                    imageId: selectedImage._id,
                },
            });
        }
    }

    private resetTooltip() {
        this.setState({
            requiredError: false,
            tooltip: {
                ...this.state.tooltip,
                _id: '',
                topicId: '',
                name: '',
                isAutoLink: false,
                weight: 0,
                isPrimer: false,
                isRemember: false,
                role: '',
                content: '',
                thumbnail: '',
                imageId: '',
                position: {
                    x: 0,
                    y: 0,
                    width: 20,
                    height: 20,
                },
            },
        });
    }
}