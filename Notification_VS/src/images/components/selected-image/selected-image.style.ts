import { GraphDimensions } from '../../../graphs/interfaces/graphs';
import styled from 'styled-components/native';

export const SelectedImage = styled.View`
    width: 100%;
    height: 420px;
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
`;

export const ImageTopMenu = styled.View`
    width: 100%;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
`;

export const BackIcon = styled.View`
    width: 25px;
    height: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const ModalText = styled.Text`
    font-weight: 600;
`;

export const TooltipMenu = styled.ScrollView`
    display: flex;
    max-height: 380px;
    max-width: 400px;
`;

export const Image = styled.View`
    max-width: 380px;
    max-height: 380px;
`;

export const SelectedImage2 = styled.View<{dim: GraphDimensions}>`
    width: ${props => (props.dim.width !== -1) ? props.dim.width : 380}px;
    height: ${props => (props.dim.height !== -1) ? props.dim.height : 380}px;
`;

export const TooltipLayer = styled.TouchableOpacity<{dim: GraphDimensions}>`
    width: ${props => (props.dim.width !== -1) ? props.dim.width : 380}px;
    height: ${props => (props.dim.height !== -1) ? props.dim.height : 380}px;
    position: relative;
    z-index: 10;
    top: -${props => (props.dim.height !== -1) ? props.dim.height : 380}px;
    background-color: rgba(255, 255, 255, 0.5);
`;

export const TooltipForm = styled.View`
    min-width: 180px;
    max-width: 380px;
    min-height: 300px;
`;