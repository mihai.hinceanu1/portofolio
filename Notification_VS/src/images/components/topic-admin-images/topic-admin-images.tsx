import { Image } from '../../interfaces/image';
import * as React from 'react';
import { Text } from 'react-native';

interface Props {
    topicGallery: Image[];
    imageTooltips: any[];
}

interface State { }

/** Render Images Related To The Selected Topic */
export class TopicAdminImages extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        return (
            <Text>Hello</Text>
        );
    }

    // private getImageTooltips(image: Image, tooltips: ImageTooltip[]) {
    //     let imageTooltips: ImageTooltip[] = [];
    //     imageTooltips = tooltips.filter(tooltip => tooltip.imageId === image._id);
    //     return imageTooltips;
    // }
}
