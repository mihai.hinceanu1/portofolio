import styled from 'styled-components/native';

export const Images = styled.TouchableOpacity`
    borderStyle: solid;
    borderWidth: 1px;
    borderColor: lightpink;
    padding: 10px 5px 10px 10px;
    display: flex;
    flex-flow: row wrap;
`;

export const SmallImage = styled.View`
    width: 90px;
    height: 90px;
    margin-right: 5px;
`;