import * as div from './images.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { Image } from '../../interfaces/image';
import { ImagesSlider } from '../../interfaces/images-slider';
import * as React from 'react';

interface Props {
    images: Image[];
    focusOnSingleGallery: (galleryId: string) => void;
    slider: ImagesSlider;
}

// REFACTOR to updated interfaces
export const Images: React.StatelessComponent<Props> = (props: Props) => {
    const { focusOnSingleGallery, slider } = props;
    return (
        <div.Images data-cy='images'
            onPress={() => focusOnSingleGallery(slider._id)}>
            {
                !!filterImages(slider.images).length &&
                filterImages(slider.images).map((image, index) =>
                    <div.SmallImage key={index}>
                        <FitImage imgPath={image.pathName} />
                    </div.SmallImage>,
                )
            }
        </div.Images>
    );

    function filterImages(topicImagesIds: any[]) {
        const { images } = props;

        let topicImages = images.filter(image => topicImagesIds.includes(image._id));

        return topicImages;
    }
};