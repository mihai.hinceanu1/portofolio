import * as imagesActions from './images.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { TopicImageEdit } from '../../topics/interfaces/topic-edit-DELETE';
import { TopicPageReq } from '../../topics/interfaces/topics';
import { getTopicPage } from '../../topics/services/topics.service';
import { Image } from '../interfaces/image';
import * as imagesService from '../services/images.service';
import * as imagesWebapi from '../webapis/images.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import {
    Action, Paginated, PaginatedChildrenReq, PaginatedSearchReq,
} from '../../shared/interfaces/shared';

export const getImage$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.GET_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(id => imagesWebapi.getImage(id).pipe(
            map(image => imagesActions.getImageOk(image)),
            catchError(error => of(imagesActions.getImageFail(error))),
        )),
    );

export const editImage$ = (action$: ActionsObservable<Action<Image>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.EDIT_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(request => imagesWebapi.editImage(request).pipe(
            map(() => {
                imagesService.getAllImages();
                return imagesActions.editImageOk();
            }),
            catchError(error => of(imagesActions.editImageFail(error))),
        )),
    );

export const deleteImage$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.DELETE_IMAGE).pipe(
        map(action => action.payload),
        concatMap(id => imagesWebapi.deleteImage(id).pipe(
            map(() => {
                imagesService.getAllImages();
                return imagesActions.deleteImageOk();
            }),
            catchError(error => of(imagesActions.deleteImageFail(error))),
        )),
    );

export const createImage$ = (action$: ActionsObservable<Action<Image>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.CREATE_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(image => imagesWebapi.createImage(image).pipe(
            map(() => {
                imagesService.getAllImages();
                return imagesActions.createImageOk();
            }),
            catchError(error => of(imagesActions.createImageFail(error))),
        )),
    );

export const getAllImages$ = (action$: ActionsObservable<Action<Paginated>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.GET_ALL_IMAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(request => imagesWebapi.getAllImages(request).pipe(
            map(images => imagesActions.getAllImagesOk(images)),
            catchError(error => of(imagesActions.getAllImagesFail(error))),
        )),
    );

export const searchImages = (action$: ActionsObservable<Action<PaginatedSearchReq>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.SEARCH_IMAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(request => imagesWebapi.searchImagesByKeyword(request).pipe(
            map(images => imagesActions.searchImagesOk(images)),
            catchError(error => of(imagesActions.searchImagesFail(error))),
        )),
    );

export const getGalleryImages$ = (action$: ActionsObservable<Action<PaginatedChildrenReq>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.GET_GALLERY_IMAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(request => imagesWebapi.getGalleryImages(request).pipe(
            map(images => imagesActions.getGalleryImagesOk(images)),
            catchError(error => of(imagesActions.getGalleryImagesFail(error))),
        )),
    );

export const getTopicImages$ = (action$: ActionsObservable<Action<PaginatedChildrenReq>>, _store: Store<AppState>) =>
    action$.ofType(imagesActions.GET_TOPIC_IMAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(request => imagesWebapi.getTopicImages(request).pipe(
            map(images => imagesActions.getTopicImagesOk(images)),
            catchError(error => of(imagesActions.getTopicImagesFail(error))),
        )),
    );

    export const editTopicImages$ = (action$: ActionsObservable<Action<TopicImageEdit>>) =>
    action$.ofType(imagesActions.EDIT_TOPIC_IMAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(imageIdAndTopic => imagesWebapi.editTopicImages(imageIdAndTopic).pipe(
            map(() => {
                let req: TopicPageReq = {
                    pathName: 'date-picker',
                    childPathName: 'basic-structure-of-a-date-picker',
                    // userId: JSON.parse(localStorage.getItem('userId')),
                    userId: '',
                };
                getTopicPage(req);
                return imagesActions.editTopicImagesOk();
            }),
            catchError(error => of(imagesActions.editTopicImagesFail(error))),
        )),
    );