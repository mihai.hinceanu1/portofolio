import { TopicImageEdit } from '../../topics/interfaces/topic-edit-DELETE';
import { Image } from '../interfaces/image';
import {
    Action, Paginated, PaginatedChildrenReq, PaginatedSearchReq,
} from '../../shared/interfaces/shared';

// ====== GET IMAGE ======

export const GET_IMAGE_REQ = 'GET_IMAGE_REQ';
export const getImageReq = (id: string): Action<string> => ({
    type: GET_IMAGE_REQ,
    payload: id,
});

export const GET_IMAGE_OK = 'GET_IMAGE_OK';
export const getImageOk = (image: Image): Action<Image> => ({
    type: GET_IMAGE_OK,
    payload: image,
});

export const GET_IMAGE_FAIL = 'GET_IMAGE_FAIL';
export const getImageFail = (error: Error): Action<Error> => ({
    type: GET_IMAGE_FAIL,
    payload: error,
});

// ====== EDIT IMAGE ======

export const EDIT_IMAGE_REQ = 'EDIT_IMAGE_REQ';
export const editImageReq = (image: Image): Action<Image> => ({
    type: EDIT_IMAGE_REQ,
    payload: image,
});

export const EDIT_IMAGE_OK = 'EDIT_IMAGE_OK';
export const editImageOk = (): Action<null> => ({
    type: EDIT_IMAGE_OK,
});

export const EDIT_IMAGE_FAIL = 'EDIT_IMAGE_FAIL';
export const editImageFail = (error: Error): Action<Error> => ({
    type: EDIT_IMAGE_FAIL,
    payload: error,
});

// ====== DELETE IMAGE ======

export const DELETE_IMAGE = 'DELETE_IMAGE';
export const deleteImage = (id: string): Action<string> => ({
    type: DELETE_IMAGE,
    payload: id,
});

export const DELETE_IMAGE_OK = 'DELETE_IMAGE_OK';
export const deleteImageOk = (): Action<null> => ({
    type: DELETE_IMAGE_OK,
});

export const DELETE_IMAGE_FAIL = 'DELETE_IMAGE_FAIL';
export const deleteImageFail = (error: Error): Action<Error> => ({
    type: DELETE_IMAGE_FAIL,
    payload: error,
});

// ====== CREATE IMAGE ======

export const CREATE_IMAGE_REQ = 'CREATE_IMAGE_REQ';
export const createImageReq = (image: Image): Action<Image> => ({
    type: CREATE_IMAGE_REQ,
    payload: image,
});

export const CREATE_IMAGE_OK = 'CREATE_IMAGE_OK';
export const createImageOk = (): Action<null> => ({
    type: CREATE_IMAGE_OK,
});

export const CREATE_IMAGE_FAIL = 'CREATE_IMAGE_FAIL';
export const createImageFail = (error: Error): Action<Error> => ({
    type: CREATE_IMAGE_FAIL,
    payload: error,
});

// ====== GET ALL IMAGES ======

export const GET_ALL_IMAGES_REQ = 'GET_ALL_IMAGES_REQ';
export const getAllImagesReq = (request: Paginated): Action<Paginated> => ({
    type: GET_ALL_IMAGES_REQ,
    payload: request,
});

export const GET_ALL_IMAGES_OK = 'GET_ALL_IMAGES_OK';
export const getAllImagesOk = (images: Image[]): Action<Image[]> => ({
    type: GET_ALL_IMAGES_OK,
    payload: images,
});

export const GET_ALL_IMAGES_FAIL = 'GET_ALL_IMAGES_FAIL';
export const getAllImagesFail = (error: Error): Action<Error> => ({
    type: GET_ALL_IMAGES_FAIL,
    payload: error,
});

// ====== SEARCH IMAGES ======

export const SEARCH_IMAGES_REQ = 'SEARCH_IMAGES_REQ';
export const searchImagesReq = (request: PaginatedSearchReq): Action<PaginatedSearchReq> => ({
    type: SEARCH_IMAGES_REQ,
    payload: request,
});

export const SEARCH_IMAGES_OK = 'SEARCH_IMAGES_OK';
export const searchImagesOk = (images: Image[]): Action<Image[]> => ({
    type: SEARCH_IMAGES_OK,
    payload: images,
});

export const SEARCH_IMAGES_FAIL = 'SEARCH_IMAGES_FAIL';
export const searchImagesFail = (error: Error): Action<Error> => ({
    type: SEARCH_IMAGES_FAIL,
    payload: error,
});

// ====== GET GALLERY IMAGES ======

export const GET_GALLERY_IMAGES_REQ = 'GET_GALLERY_IMAGES_REQ';
export const getGalleryImagesReq = (request: PaginatedChildrenReq): Action<PaginatedChildrenReq> => ({
    type: GET_GALLERY_IMAGES_REQ,
    payload: request,
});

export const GET_GALLERY_IMAGES_OK = 'GET_GALLERY_IMAGES_OK';
export const getGalleryImagesOk = (images: Image[]): Action<Image[]> => ({
    type: GET_GALLERY_IMAGES_OK,
    payload: images,
});

export const GET_GALLERY_IMAGES_FAIL = 'GET_GALLERY_IMAGES_FAIL';
export const getGalleryImagesFail = (error: Error): Action<Error> => ({
    type: GET_GALLERY_IMAGES_FAIL,
    payload: error,
});

// ====== GET TOPIC IMAGES ======

export const GET_TOPIC_IMAGES_REQ = 'GET_TOPIC_IMAGES_REQ';
export const getTopicImagesReq = (request: PaginatedChildrenReq): Action<PaginatedChildrenReq> => ({
    type: GET_TOPIC_IMAGES_REQ,
    payload: request,
});

export const GET_TOPIC_IMAGES_OK = 'GET_TOPIC_IMAGES_OK';
export const getTopicImagesOk = (images: Image[]): Action<Image[]> => ({
    type: GET_TOPIC_IMAGES_OK,
    payload: images,
});

export const GET_TOPIC_IMAGES_FAIL = 'GET_TOPIC_IMAGES_FAIL';
export const getTopicImagesFail = (error: Error): Action<Error> => ({
    type: GET_TOPIC_IMAGES_FAIL,
    payload: error,
});

export const SET_ADD_IMAGE_MODAL_OPEN = 'SET_ADD_IMAGE_MODAL_OPEN';
export const setAddImageModalOpen = (addImageModalOpen: boolean): Action<boolean> => ({
    type: SET_ADD_IMAGE_MODAL_OPEN,
    payload: addImageModalOpen,
});

// ====== EDIT TOPIC IMAGES ======
export const EDIT_TOPIC_IMAGES_REQ = 'EDIT_TOPICS_IMAGES_REQ';
export const editTopicImagesReq = (imageIdAndTopic: TopicImageEdit): Action<TopicImageEdit> => ({
    type: EDIT_TOPIC_IMAGES_REQ,
    payload: imageIdAndTopic,
});

export const EDIT_TOPIC_IMAGES_OK = 'EDIT_TOPIC_IMAGES_OK';
export const editTopicImagesOk = (): Action<null> => ({
    type: EDIT_TOPIC_IMAGES_OK,
});

export const EDIT_TOPIC_IMAGES_FAIL = 'EDIT_TOPIC_IMAGES_FAIL';
export const editTopicImagesFail = (error: Error): Action<Error> => ({
    type: EDIT_TOPIC_IMAGES_FAIL,
    payload: error,
});
