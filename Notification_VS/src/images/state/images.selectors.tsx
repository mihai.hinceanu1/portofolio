import { AppState } from '../../shared/interfaces/app.state';
import { Image } from '../interfaces/image';
import { ImagesState } from '../interfaces/images.state';
import { createSelector, Selector } from 'reselect';

const GALLERY_MODULE: Selector<AppState, ImagesState> = (state: AppState) => state.images;

/** List images details in admin */
export const IMAGE = createSelector<AppState, ImagesState, Image>(
    GALLERY_MODULE,
    (state: ImagesState) => state.image,
);

/** List all images in admin */
export const IMAGES = createSelector<AppState, ImagesState, Image[]>(
    GALLERY_MODULE,
    (state: ImagesState) => state.images,
);

export const GALLERY_IMAGES = createSelector<AppState, ImagesState, Image[]>(
    GALLERY_MODULE,
    (state: ImagesState) => state.galleryImages,
);

export const TOPIC_IMAGES = createSelector<AppState, ImagesState, Image[]>(
    GALLERY_MODULE,
    (state: ImagesState) => state.topicImages,
);

export const SEARCH_IMAGES_RESULTS = createSelector<AppState, ImagesState, Image[]>(
    GALLERY_MODULE,
    (state: ImagesState) => state.searchImagesResults,
);

export const ADD_IMAGE_MODAL_OPEN = createSelector<AppState, ImagesState, boolean>(
    GALLERY_MODULE,
    (state: ImagesState) => state.modal.addImageModal,
);