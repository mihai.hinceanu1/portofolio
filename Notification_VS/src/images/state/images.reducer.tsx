import * as actions from './images.actions';
import { imagesInitialState } from './images.init-state';
import { Image } from '../interfaces/image';
import { ImagesState } from '../interfaces/images.state';
// import { TopicPageResp } from '../../topics/interfaces/topic-page-resp';
// import * as topicsActions from '../../topics/state/actions/topics.actions';

export const imagesReducer = (state: ImagesState = imagesInitialState, action: any): ImagesState => {

    switch (action.type) {

        case actions.GET_IMAGE_OK: {
            return {
                ...state,
                image: action.payload as Image,
            };
        }

        case actions.GET_ALL_IMAGES_OK: {
            return {
                ...state,
                images: action.payload as Image[],
            };
        }

        case actions.GET_GALLERY_IMAGES_OK: {
            return {
                ...state,
                galleryImages: action.payload as Image[],
            };
        }

        case actions.GET_TOPIC_IMAGES_OK: {
            return {
                ...state,
                topicImages: action.payload as Image[],
            };
        }

        case actions.SEARCH_IMAGES_OK: {
            return {
                ...state,
                searchImagesResults: action.payload as Image[],
            };
        }

        case actions.SET_ADD_IMAGE_MODAL_OPEN: {
            // let newModalsState = resetModals(state);

            return {
                ...state,
                modal: {
                    addImageModal: action.payload as boolean,
                },
            };
        }

        // case topicsActions.GET_TOPIC_PAGE_OK: {
        //     let topicPage = action.payload as TopicPageResp;
        //     let { images } = topicPage;
        //     return {
        //         ...state,
        //         images,
        //     };
        // }

        default:
            return state;
    }

};
