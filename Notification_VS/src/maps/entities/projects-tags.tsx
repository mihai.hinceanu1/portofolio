import { TILE } from '../../graphics-engine/constants/map-world.const';
import { APP_CFG } from '../../shared/config/app.config';
import { colors } from '../../shared/style/colors';
import { font } from '../../shared/style/font-sizes';
import { PROJECT_TAG_BOX, TOPIC_MARKER_DIMENSIONS } from '../assets/topics-cluster.assets';
import { FONT_LABELS, THUMBNAIL_SIZE } from '../constants/global-map.const';
import { TopicsClusterBody } from '../interfaces/topics-cluster-body';
import * as React from 'react';
import { Platform } from 'react-native';
import {
    Defs,
    G,
    Image,
    Path,
    Pattern,
    Rect,
    Text
    } from 'react-native-svg';
// import { GraphDimensions, GraphPosition } from '../../graphs/interfaces/graphs';

interface Props {
    body: TopicsClusterBody;
}

/**
 * @TopicsCluster sub-component
 *
 * Project tags left-side of the category cluster
 * Contains
 * Thumbnail
 * Project name
 */
export const ProjectTags: React.StatelessComponent<Props> = (props: Props) => {
    let body = props.body;
    let { direction, projects } = body;
    // Offset
    // Relative start for the first Project Tag
    let offset = {
        x: -30, // TODO find the real width from assets

        /** TODO handle bottom initial offset */
        y: -TILE.height - TILE.height / 2 - TOPIC_MARKER_DIMENSIONS.height,
        // end of the curve, straight line from the main branch
        // -TILE.height - TILE.height / 2 - markerHeight (23) to align a topic marker on the bottom TILE
    };

    // Start Positions
    let startPositionsOnY: number[] = [];
    // Fail safe
    if (projects) {
        startPositionsOnY = getProjectsTagsPositions(offset.y, projects.length, direction);
    }

    // Path for the area (rectangle) in which the thumbnail is. Background Box (container) for Icon and Project Name
    // const thumbnailViewport: GraphDimensions & GraphPosition = {
    //     x: 0, y: 0, height: THUMBNAIL_SIZE.height, width: THUMBNAIL_SIZE.width,
    // };

    return (
        <React.Fragment>
            {/** Projects Tags */}
            {
                startPositionsOnY.map((yRelativePosition: number, index: number) =>
                    <G key={yRelativePosition} x={offset.x} y={yRelativePosition}>
                        {/** ===== Project tag ===== */}
                        <Defs>
                            <Pattern
                                id='test-pattern'
                                patternUnits='userSpaceOnUse'
                                width={THUMBNAIL_SIZE.width}
                                height={THUMBNAIL_SIZE.height}
                                x={0}
                                y={0}
                            >
                                <Image href={{ uri: getThumbnail(projects[index].thumbnail) }}
                                    width={THUMBNAIL_SIZE.width} height={THUMBNAIL_SIZE.height} />
                            </Pattern>
                        </Defs>
                        {/*  */}
                        <Path d={PROJECT_TAG_BOX} fill={colors.$projectTagBoxBgr} />

                        {/* Render Icon -140 -15*/}
                        {
                            /**
                             *  TODO fix error (NSString is not a image source)
                             * on IOS - restore prj tags thumbnails on mobile
                             */
                        }
                        {
                            Platform.OS === 'web' &&
                            // Path container for Icon
                            <Rect x={-140} y={-15}
                                fill='url(#test-pattern)'
                            // new Pattern(`${getThumbnail(projects[index].thumbnail)}`,
                            // THUMBNAIL_SIZE.width, THUMBNAIL_SIZE.height, 0, 0)
                            />
                            // <Path d={bgrRect} x={-140} y={-15} />
                        }

                        {/* Render Project Name */}
                        {/** 10 px from the most left, -7 center on y */}
                        <Text x={-100} y={-7}
                            font={{
                                fontSize: font.$size10,
                                fontFamily: FONT_LABELS,
                            }}
                            fill={'black'}>{projects[index].name}</Text>

                    </G>)
            }
        </React.Fragment>
    );
};

/**
 * Positions on y axis for Project Tags
 * <!> Do NOT modify this without handling overlapping buttons positions
 */
export function getProjectsTagsPositions(yFirstProjTag: number, numberOfProjTags: number, direction: string): number[] {

    let positions: number[] = [];
    let currentPosition: number;

    if (direction === 'top') {
        currentPosition = yFirstProjTag;
    }
    if (direction === 'bottom') {
        currentPosition =
            TILE.height * 1.5 + TOPIC_MARKER_DIMENSIONS.height;
    }

    for (let i = 0; i < numberOfProjTags; i++) {
        positions.push(currentPosition);
        direction === 'top' ?
            currentPosition -= TOPIC_MARKER_DIMENSIONS.height * 2
            : currentPosition += TOPIC_MARKER_DIMENSIONS.height * 2;
    }

    return positions;
}

// PARED Until we switch to react-native-svg
function getThumbnail(name: string) {

    let imgSrc = `${APP_CFG.assets}${name}`;
    return imgSrc;
}
