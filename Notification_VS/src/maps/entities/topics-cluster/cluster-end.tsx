import { TopicsClusterBody } from '../../interfaces/topics-cluster-body';
import * as React from 'react';
// import { View } from 'react-native';
import { getClusterEndOffset, getEndMarkerOffset } from '../../utils/global-map.offsets';
// import { Group, Shape } from '@react-native-community/art';
import {
    CLUSTER_END_MARKER_BOTTOM,
    CLUSTER_END_MARKER_TOP, CLUSTER_END_SEMI_LINE, PROJECT_TAG_DIMENSIONS_OFFSET_INCLUDED, TOPIC_MARKER_DIMENSIONS,
} from '../../assets/topics-cluster.assets';
import { G, Path } from 'react-native-svg';
import { TILE } from '../../../graphics-engine/constants/map-world.const';

interface Props {
    body: TopicsClusterBody;
}

/**
 * @TopicsCluster sub-component
 * Straight line + circle at the end of the Topics Cluster
 * connects Topic Markers (if any) or the Start curve (if no topics markers) with the end
 * which is Root Topic (cluster label = level name)
 */
export const ClusterEnd: React.StatelessComponent<Props> = (props: Props) => {

    let body = props.body;
    let { direction, fill, topics, projects } = body;

    let clusterEndOffset = getClusterEndOffset(direction, topics.length, projects.length);
    let endMarkerOffset = getEndMarkerOffset(direction);

    return (
        // <View>{{}}</View>
        <G x={clusterEndOffset.x} y={clusterEndOffset.y}>

            {/** Filling Semi-Line(s) if necessary - <!> in reverse relative to clusterEndOffset */}
            {drawConnectorSemiLine(body)}

            {/** End Semi-Line */}
            <Path x={0} y={0}
                d={drawLastSemiLine(body)} fill={fill || 'blue'} />

            {/** Cluster end marker (Line + Circle) */}
            <Path x={endMarkerOffset.x} y={endMarkerOffset.y}
                d={drawClusterEndMarker(direction)} fill={fill || 'blue'} />

        </G>
    );
};

/** Last semi-line */
function drawLastSemiLine(body: TopicsClusterBody): string {

    let { direction } = body;
    let curveBranch: string;

    switch (direction) {
        case 'top': {
            curveBranch = CLUSTER_END_SEMI_LINE;
            break;
        }
        case 'bottom': {
            curveBranch = CLUSTER_END_SEMI_LINE;
        }
    }

    return curveBranch;
}

/** EndMarker - Line with filled circle */
function drawClusterEndMarker(direction: string): string {
    let clusterEndMarker: string;

    switch (direction) {
        case 'top': {
            clusterEndMarker = CLUSTER_END_MARKER_TOP;
            break;
        }
        case 'bottom': {
            clusterEndMarker = CLUSTER_END_MARKER_BOTTOM;
            break;
        }
    }

    return clusterEndMarker;
}

/**
 * Filling with Dynamic connector Semi-Line
 * when distance of Project Tags is greater the Total distance of Topic Markers
 */
function drawConnectorSemiLine(body: TopicsClusterBody): JSX.Element {

    let { direction, fill, topics, projects } = body;

    let semiLines: JSX.Element;

    let extraOffsetBottom = 0;

    // More space on Y axis taken by project tags then topic markers
    let projectTagsTotalHeight = projects.length * PROJECT_TAG_DIMENSIONS_OFFSET_INCLUDED.height;
    let topicMarkersTotalHeight = topics.length * TOPIC_MARKER_DIMENSIONS.height;

    if (projectTagsTotalHeight > topicMarkersTotalHeight) {
        let distanceNeeded = projectTagsTotalHeight - topicMarkersTotalHeight;

        if (direction === 'bottom') {
            distanceNeeded = (-1) * distanceNeeded;
        } else {
            extraOffsetBottom = TILE.height;
        }

        const CLUSTER_END_CONNECTOR_SEMI_LINE =
            `
            M-0.000,-0.000
            L4.000,-0.000
            L4.000,${distanceNeeded}
            L-0.000,${distanceNeeded}
            L-0.000,-0.000
            Z`;

        semiLines = <Path x={0} y={0 + extraOffsetBottom}
            d={CLUSTER_END_CONNECTOR_SEMI_LINE} fill={fill || 'blue'} />;
    }

    return semiLines;
}