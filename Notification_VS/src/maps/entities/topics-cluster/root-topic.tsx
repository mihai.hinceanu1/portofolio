import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { ROOT_HEART } from '../../assets/topics-cluster.assets';
import { FONT_LABELS, ROOT_HEART_WIDTH, TOPICS_CLUSTER_ROOT_TOPIC_TEXT_SIZE } from '../../constants/global-map.const';
import { TopicsClusterBody } from '../../interfaces/topics-cluster-body';
import { getRootNameOffset } from '../../utils/global-map.offsets';
import * as React from 'react';
import { G, Path, Text } from 'react-native-svg';

interface Props {
    body: TopicsClusterBody;
}

/**
 * @TopicsCluster sub-component
 * Root Topic
 */
export const RootTopic: React.StatelessComponent<Props> = (props: Props) => {
    let body = props.body;
    let { direction, topics, projects, rootTopic, numOfFavVotes } = body;

    let rootNameOffset = getRootNameOffset(direction, topics.length, projects.length);

    return (
        <G x={rootNameOffset.x} y={rootNameOffset.y}>
            <Text x={0} y={0}
                font={
                    {
                        fontSize: TOPICS_CLUSTER_ROOT_TOPIC_TEXT_SIZE,
                        fontFamily: FONT_LABELS,
                    }
                }
                fill={colors.$rootTopicName}
                // alignment='center'
                >
                {rootTopic}
            </Text>

            {/** Favorite <3 Icon */}
            <G x={-ROOT_HEART_WIDTH} y={setHeartY(direction)}>
                <Path d={ROOT_HEART} stroke='#000000' strokeWidth={2.5} />
                <Text x={ROOT_HEART_WIDTH + 3} y={5}
                    font={
                        {
                            fontSize: font.$size18,
                            fontFamily: FONT_LABELS,
                        }
                    }
                    fill='#000000'>{numOfFavVotes.toString()}</Text>
            </G>

            {/** Rating */}
        </G>

    );
};

function setHeartY(direction: string): number {
    if (direction === 'top') {
        return TOPICS_CLUSTER_ROOT_TOPIC_TEXT_SIZE;
    } else {
        return -TOPICS_CLUSTER_ROOT_TOPIC_TEXT_SIZE - 5;
    }
}
