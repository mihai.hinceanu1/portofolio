import { TILE } from '../../../graphics-engine/constants/map-world.const';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { TOPIC_GOAL_BOTTOM, TOPIC_GOAL_TOP, TOPIC_MARKER_BOTTOM, TOPIC_MARKER_DIMENSIONS, TOPIC_MARKER_TOP } from '../../assets/topics-cluster.assets';
import { FONT_LABELS } from '../../constants/global-map.const';
import { TopicsClusterBody } from '../../interfaces/topics-cluster-body';
import * as React from 'react';
import { G, Path, Text } from 'react-native-svg';
// import { fontSizes } from '../../../shared/style/font-sizes';
// import { FONT_LABELS } from '../../constants/global-map.constants';
// import { Group, Shape, Text } from '@react-native-community/art';
// import {
//     TOPIC_GOAL_BOTTOM,
//     TOPIC_GOAL_TOP,
//     TOPIC_MARKER_BOTTOM,
//     TOPIC_MARKER_DIMENSIONS,
//     TOPIC_MARKER_TOP,
// } from '../../assets/topics-cluster.assets';

interface Props {
    body: TopicsClusterBody;
}

/**
 * @TopicsCluster sub-component
 *
 * Connects to straight line
 * Renders all the TopicsMarkers
 * Renders the Topics Name next to the Topics Markers
 */
export const TopicsMarkers: React.StatelessComponent<Props> = (props: Props) => {
    let body = props.body;
    let { direction, topics } = body;
    // Offset
    // Relative start for the first topic marker
    // for every iteration only y will increase with TILE.height / 2
    let offset = {
        x: -TOPIC_MARKER_DIMENSIONS.width / 2,

        /** TODO handle bottom initial offset */
        y: -TILE.height - TILE.height / 2 - TOPIC_MARKER_DIMENSIONS.height,
        // end of the curve, straight line from the main branch
        // -TILE.height - TILE.height / 2 - markerHeight (23) to align a topic marker on the bottom TILE
    };

    // Start positions
    let startPositions: number[] = [];
    // Fail safe
    if (topics) {
        startPositions = getTopicsMarkersPositions(offset.y, topics.length, direction);
    }

    // Offset for topic name positioned to the right of the cluster branch
    let topicNameOffset = {
        x: TOPIC_MARKER_DIMENSIONS.width + 5,
        y: direction === 'top' ? 3 : TOPIC_MARKER_DIMENSIONS.height / 2,
    };
    return (
        <React.Fragment>

            {/** Topic Markers and Topics Names */}
            {
                startPositions.map((yRelativePosition: number, index: number) =>
                    <G key={yRelativePosition}
                        x={(topics[index].difficulty === 'Goal') ? (offset.x - 3) : offset.x}
                        y={((topics[index].difficulty === 'Goal') && (direction === 'bottom')) ?
                            (yRelativePosition - 1) : yRelativePosition}>

                        {/** ====== Topic Marker ====== * /}

                        {/** Marker (line with circle) */}
                        {
                            (topics[index].difficulty === 'Goal') ?
                                /** Goal Shape */
                                <Path d={drawGoalMarker(direction)}
                                    fill={getMarkerColor(topics[index].difficulty)} /> :
                                /** Regular Topic Marker shape */
                                <Path d={drawTopicMarker(direction)}
                                    fill={getMarkerColor(topics[index].difficulty)} />
                        }

                        {/** Topic Name */}
                        {/** +5 as it was too close - TODO get a better constant */}
                        <Text x={topicNameOffset.x + (topics[index].difficulty === 'Goal' ? 3 : 0)}
                            y={topicNameOffset.y}
                            font={{
                                fontSize: font.$size10,
                                fontFamily: FONT_LABELS
                            }}
                            fill='#000000'>
                            {topics[index].name}
                        </Text>
                    </G>)
            }
        </React.Fragment>

    );
};

function drawTopicMarker(direction: string): string {
    let topicsMarker: string;

    switch (direction) {
        case 'top': {
            topicsMarker = TOPIC_MARKER_TOP;
            break;
        }
        case 'bottom': {
            topicsMarker = TOPIC_MARKER_BOTTOM;
            break;
        }
    }

    return topicsMarker;
}

function drawGoalMarker(direction: string): string {
    let topicsMarker: string;

    switch (direction) {
        case 'top': {
            topicsMarker = TOPIC_GOAL_TOP;
            break;
        }
        case 'bottom': {
            topicsMarker = TOPIC_GOAL_BOTTOM;
            break;
        }
    }

    return topicsMarker;
}

/**
 *  Positions on y axis for markers for start
 * <!> Do NOT modify this without handling overlapping buttons positions
 */
function getTopicsMarkersPositions(yFirstMarker: number, numberOfMarkers: number, direction: string): number[] {
    // TODO DESIGN smaller marker (height) so that two fit in one TILE at even spacing

    let positions: number[] = [];
    let currentPosition: number;
    if (direction === 'top') {
        currentPosition = yFirstMarker;
    }
    if (direction === 'bottom') {
        currentPosition = TILE.height * 1.5;
    }

    for (let i = 0; i < numberOfMarkers; i++) {
        positions.push(currentPosition);
        direction === 'top' ?
            currentPosition -= TOPIC_MARKER_DIMENSIONS.height
            : currentPosition += TOPIC_MARKER_DIMENSIONS.height;
    }
    return positions;
}

/** @returns color based on difficulty */
export function getMarkerColor(difficulty: string): string {

    /** Failsafe */
    let color = '';

    /** @BUG SERVER <!> category difficulty does not enter any of the bellow cases on server */
    switch (difficulty) {
        case 'Easy': {
            color = colors.$topicMarkerEasy;
            break;
        }
        case ('Goal'): {
            color = colors.$topicMarkerGoal;
            break;
        }
        case ('Medium'): {
            color = colors.$topicMarkerMedium;
            break;
        }
        case 'Hard': {
            color = colors.$topicMarkerHard;
            break;
        }
        case 'Master': {
            color = colors.$green;
            break;
        }
        default: {
            color = colors.$topicMarkerEasy;
        }
    }
    return color;
}
