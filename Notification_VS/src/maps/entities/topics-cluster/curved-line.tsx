import { TILE } from '../../../graphics-engine/constants/map-world.const';
import { CURVE_MAIN_BRANCH_TO_BOTTOM_RIGHT, CURVE_MAIN_BRANCH_TO_TOP_RIGHT } from '../../assets/topics-cluster.assets';
import { TopicsClusterBody } from '../../interfaces/topics-cluster-body';
import { getStartCurveOffset } from '../../utils/global-map.offsets';
import * as React from 'react';
import { Path } from 'react-native-svg';

interface Props {
    body: TopicsClusterBody;
}

/**
 * @TopicsCluster sub-component
 * Curved line at the start of the Topics Cluster
 * connects Main Branch with the start of the Topics Cluster via straight line
 * followed by topic markers
 */
export const CurvedLine: React.StatelessComponent<Props> = (props: Props) => {

    let { body } = props;
    let { direction, fill } = body;

    let startCurveOffset = getStartCurveOffset(direction);

    return (
        <Path x={startCurveOffset.x - TILE.width / 2} y={startCurveOffset.y - TILE.height / 2}
            d={drawTopicsBranchStartCurve(body)} fill={fill || 'blue'} />
    );
};

function drawTopicsBranchStartCurve(body: TopicsClusterBody): string {

    let { direction } = body;
    let curveBranch: string;

    switch (direction) {
        case 'top': {
            curveBranch = CURVE_MAIN_BRANCH_TO_TOP_RIGHT;
            break;
        }
        case 'bottom': {
            curveBranch = CURVE_MAIN_BRANCH_TO_BOTTOM_RIGHT;
        }
    }

    return curveBranch;
}
