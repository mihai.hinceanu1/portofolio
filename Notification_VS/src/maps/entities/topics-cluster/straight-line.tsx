import { STRAIGHT_LINE_BOTTOM, STRAIGHT_LINE_TOP } from '../../assets/topics-cluster.assets';
import { TopicsClusterBody } from '../../interfaces/topics-cluster-body';
import { getStraightLineOffset } from '../../utils/global-map.offsets';
// import { Shape } from '@react-native-community/art';
import { Path } from 'react-native-svg';
import * as React from 'react';

interface Props {
    body: TopicsClusterBody;
}

/**
 * @TopicsCluster sub-component
 * Straight line at the start within Topics Cluster
 * follows Curve Line
 * connects the Bezier Curve with the first Topic Marker
 */
export const StraightLine: React.StatelessComponent<Props> = (props: Props) => {

    let body = props.body;
    let { direction, fill } = body;

    let straightLineOffset = getStraightLineOffset(direction);

    return (
        <Path x={straightLineOffset.x} y={straightLineOffset.y}
            d={drawStraightLine(direction)} fill={fill} />
    );
};

/**
 * Different straightLine for top / bottom so that it ends
 * exactly at the 2nd TILE corner counting from start curve
 */
function drawStraightLine(direction: string): string {
    let straightLine: string;

    switch (direction) {
        case ('top'): {
            straightLine = STRAIGHT_LINE_TOP;
            break;
        }
        case ('bottom'): {
            straightLine = STRAIGHT_LINE_BOTTOM;
            break;
        }
    }

    return straightLine;
}