import * as React from 'react';
// import { View } from 'react-native';
// import { colors } from '../../../shared/style/colors';
// import { CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE, FONT_LABELS } from '../../constants/global-map.constants';
// import { Group, Text } from '@react-native-community/art';
import { G, Text } from 'react-native-svg';
import { TILE } from '../../../graphics-engine/constants/map-world.const';
import { FONT_LABELS, CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE } from '../../constants/global-map.const';
import { colors } from '../../../shared/style/colors';

interface Props {
    categoryLabel: string;
}

/**
 * @CategoryBranch sub-component
 * Tilted Category Label
 * iterating over all letters of the categoryLabel and incrementing x, y positions
 * to simulate a tilted text
 */
export const CategoryLabel: React.StatelessComponent<Props> = (props: Props) => {

    let { categoryLabel } = props;

    if (categoryLabel) {
        // <!> Font must be monospace or else the space between different letters will not be the same
        let lettersArray = [] as any;

        let letterSpaceX = 0;
        let letterSpaceY = 0;

        for (let i = 0; i < categoryLabel.length; i++) {
            lettersArray.push(
                <Text
                    key={i}
                    x={TILE.width / 3 + letterSpaceX}
                    y={-TILE.height / 1.2 - letterSpaceY}
                    font={{
                        fontFamily: FONT_LABELS,
                        fontSize: CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE,
                    }
                        // `${CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE}px "${FONT_LABELS}"`
                    }
                    fill={colors.$rootTopicName}
                    // alignment='right'
                    >
                    {categoryLabel[i]}
                </Text>,
            );

            // Manual space incrementation for each letter
            letterSpaceX += CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE / 2;
            letterSpaceY += CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE / 5.3;
        }

        let tiltedLabel =
            <G x={0} y={0}>
                {lettersArray}
            </G>;

        return tiltedLabel;

    } else {
        // Failsafe
        return (
            <G x={0} y={0} />
        );
    }

    // return <View>{{}}</View>

};
