import { CategoryLabel } from './category-label-tilted';
import { TILE } from '../../../graphics-engine/constants/map-world.const';
import { mapIsometricPositionToScreenPosition } from '../../../graphics-engine/services/isometric.utils';
import { MainBranchBody } from '../../interfaces/main-branch-body';
import * as React from 'react';
import { G, Path } from 'react-native-svg';
// import { Group, Shape } from '@react-native-community/art';

interface Props {
    body: MainBranchBody;
}

/** MainBranchLine will return a thick line to which the Topics Cluster are attached */
export const MainBranchLine: React.StatelessComponent<Props> = (props: Props) => {
    let body = props.body;
    return (
        /* Entity */
        /* In relation to the entity origin, we move the graphical assets */
        <G x={0} y={0}>

            {/** Category label */}
            <CategoryLabel categoryLabel={body.categoryLabel} />

            {/* Render geometry - Line */}
            <Path d={drawBasicLinePath(body)} fill={body.fill || 'blue'} />

        </G>
    );
};

/**
 * Given start(x,y) length ( stop(x,y) = start+length) and thickness, returns the path of the line
 * it is a path of a very slim Quadrilateral with fill (not a line - because it has thickness)
 * TODO: rounded ends
 * Adrian: I'll generate paths in PSD
 * REVIEW Do not keep GENERIC positioning logic and SPECIFIC GEOMETRY generic code in the same file.
 * Separate these utils to: geometry/primitives/rectilinear-line-geometry.utils
 * In time this code will become cluttered if it's not properly segregated by purpose from the very beginning.
 */
export function drawBasicLinePath(body: MainBranchBody): string {

    // Relative start from the given position component
    let start = { x: 0, y: 0, z: 0 };
    let stop = { x: 0, y: 0, z: 0 };

    let thickness: number;

    // The start and end of the line must be parallel with the TILE lines,
    // to crate this illusion, we add/subtract bellow value
    let isoParallelStartEnd: number;

    // Set thickness
    switch (body.thickness) {
        case 'S': {
            thickness = 11.5;
            isoParallelStartEnd = 2.5;
            break;
        }
        case 'M': {
            thickness = 15;
            isoParallelStartEnd = 3;
            break;
        }
        case 'L': {
            thickness = 25;
            isoParallelStartEnd = 5;
            break;
        }
    }
    // Set line stop based on length
    switch (body.direction) {
        case 'x': {
            stop.x += body.length;
            break;
        }
        case 'y': {
            stop.y += body.length;
            break;
        }
        case 'z': {
            stop.z += body.length;
            break;
        }
    }

    // Handle offset, alters start, stop based on thickness
    /** TODO thickness should be recalculated on Ox, Oz axis and look the same as Oy */
    let innerOffset = thickness / 2;

    let direction: string = body.direction;

    // Center of the start, stop TILE
    // <!> We call mapIsometricPositionToScreenPosition() on a higher level in mapEntityToART
    // Is bellow still necessary?
    let start2D = mapIsometricPositionToScreenPosition(TILE, start);
    let stop2D = mapIsometricPositionToScreenPosition(TILE, stop);

    // Compute distance from TILE center to given corner
    let centerToCorner = { x: 0, y: 0 };
    switch (body.corner) {
        case 'top': {
            centerToCorner.x = 0;
            centerToCorner.y = -TILE.height / 2;
            break;
        }
        case 'bottom': {
            centerToCorner.x = 0;
            centerToCorner.y = TILE.height / 2;
            break;
        }
        case 'left': {
            centerToCorner.x = -TILE.width / 2;
            centerToCorner.y = 0;
            break;
        }
        case 'right': {
            centerToCorner.x = TILE.width / 2;
            centerToCorner.y = 0;
            break;
        }
    }

    // Move from center to TILE corner
    start2D.x += centerToCorner.x;
    start2D.y += centerToCorner.y;

    stop2D.x += centerToCorner.x;
    stop2D.y += centerToCorner.y;

    let line: string;

    switch (direction) {
        /** TODO discuss if 'z' direction is still needed, remove if not */
        case 'z': {
            line = `M ${start2D.x} ${start2D.y}
                L${start2D.x} ${start2D.y - innerOffset}
                L${stop2D.x} ${stop2D.y - innerOffset}
                L${stop2D.x} ${stop2D.y}
                L${start2D.x} ${start2D.y}
                Z`;
            break;
        }

        case 'y': {
            line = `M ${start2D.x - innerOffset} ${start2D.y}
                L${stop2D.x - innerOffset} ${stop2D.y}
                L${start2D.x + innerOffset} ${stop2D.y}
                L${start2D.x + innerOffset} ${start2D.y}
                Z`;

            break;

        }

        case 'x': {
            line = `M ${start2D.x + innerOffset + isoParallelStartEnd} ${start2D.y + innerOffset - isoParallelStartEnd}
                L${stop2D.x + innerOffset + isoParallelStartEnd} ${stop2D.y + innerOffset - isoParallelStartEnd}
                L${stop2D.x - innerOffset - isoParallelStartEnd} ${stop2D.y - innerOffset + isoParallelStartEnd}
                L${start2D.x - innerOffset - isoParallelStartEnd} ${start2D.y - innerOffset + isoParallelStartEnd}
                Z`;
            break;
        }
    }

    return line;
}
