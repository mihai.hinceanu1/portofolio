import { ProjectTags } from './projects-tags';
import { ClusterEnd } from './topics-cluster/cluster-end';
import { CurvedLine } from './topics-cluster/curved-line';
import { RootTopic } from './topics-cluster/root-topic';
import { StraightLine } from './topics-cluster/straight-line';
import { TopicsMarkers } from './topics-cluster/topics-markers';
import { TopicsClusterBody } from '../interfaces/topics-cluster-body';
import * as React from 'react';

interface Props {
    body: TopicsClusterBody;
}

/**
 * TopicCluster is a secondary branch with a variety of topic-related elements,
 * it connects to Main Branch and it contains:
 * Topics markers,
 * Topics Names,
 * Project Tags,
 * Cluster End,
 * Root Topic
 */
export const TopicsCluster: React.StatelessComponent<Props> = (props: Props) => {

    let { body } = props;

    return (
        /* Entity */
        /* In relation to the entity origin, we move the graphical assets */

        <React.Fragment>

            {/** Curved line */}
            <CurvedLine body={body} />

            {/** Straight line */}
            <StraightLine body={body} />

            {/** Topic Markers and topics names */}
            <TopicsMarkers body={body} />

            {/** Project Tags */}
            <ProjectTags body={body} />

            {/** Cluster End */}
            <ClusterEnd body={body} />

            {/** Root Topic */}
            <RootTopic body={body} />

        </React.Fragment>
    );
};
