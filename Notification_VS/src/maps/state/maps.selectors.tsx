import { AppState } from '../../shared/interfaces/app.state';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';
import { INanolessonModal } from '../interfaces/entity';
import { IsoPosition, LearningMapRawCategoryBranch } from '../interfaces/maps';
import { MapState } from '../interfaces/maps.state';
import { createSelector, Selector } from 'reselect';

/**
 * Global Map
 * Local Map - future use
 */
const MAP_MODULE: Selector<AppState, MapState> = (state: AppState) => state.map;

export const GLOBAL_MAP_RAW_CATEGORY_BRANCH = createSelector<AppState, MapState, LearningMapRawCategoryBranch>(
    MAP_MODULE,
    (state: MapState) => state.globalMapRawCategoryBranch,
);

export const GLOBAL_MAP_RAW_ALL_CATEGORY_BRANCHES = createSelector<AppState, MapState, LearningMapRawCategoryBranch[]>(
    MAP_MODULE,
    (state: MapState) => state.globalMapRawAllCategoryBranches,
);

export const GLOBAL_MAP_DOMAIN_LEVEL = createSelector<AppState, MapState, TaxonomyLevel>(
    MAP_MODULE,
    (state: MapState) => state.globalMapDomainLevel,
);

export const SELECTED_DOMAIN_ID = createSelector<AppState, MapState, string>(
    MAP_MODULE,
    (state: MapState) => state.selectedDomainId,
);

/** Send selected category id from LearningMapMainMenu to IsometricMap */
export const SELECTED_CATEGORY_ID = createSelector<AppState, MapState, string>(
    MAP_MODULE,
    (state: MapState) => state.selectedCategoryId,
);

export const LEVELS_COUNTER = createSelector<AppState, MapState, number>(
    MAP_MODULE,
    (state: MapState) => state.levelsCounter,
);

export const MAP_START_ISO_POSITION = createSelector<AppState, MapState, IsoPosition>(
    MAP_MODULE,
    (state: MapState) => state.startPosition,
);

export const SELECTED_CATEGORY_NAME = createSelector<AppState, MapState, string>(
    MAP_MODULE,
    (state: MapState) => state.selectedCategoryName,
);

/** Learning map nanolesson state selector */
export const NANOLESSON_STATE = createSelector<AppState, MapState, INanolessonModal>(
    MAP_MODULE,
    (state: MapState) => state.nanolessonModal,
);