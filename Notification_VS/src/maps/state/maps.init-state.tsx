import { NANOLESSON_DIFFICULTIES } from '../../nano-lessons/constants/difficulties.const';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';
import { LearningMapRawCategoryBranch } from '../interfaces/maps';
import { MapState } from '../interfaces/maps.state';
import { TopicsClusterRaw } from '../interfaces/topics-cluster-body';

export const mapInitialState: MapState = {
    levelsCounter: 0,
    globalMapRawCategoryBranch: {
        categoryLabel: '',
        clusters: [] as TopicsClusterRaw[],
    } as LearningMapRawCategoryBranch,
    globalMapRawAllCategoryBranches: [] as LearningMapRawCategoryBranch[],

    globalMapDomainLevel: {} as TaxonomyLevel,

    selectedDomainId: '',
    selectedCategoryId: '',
    selectedCategoryName: '',

    startPosition: {
        x: 0,
        y: 0,
        z: 0,
    },
    nanolessonModal: {
        isVisible: false,
        nanolesson: {
            _id: '',
            difficulty: NANOLESSON_DIFFICULTIES.Junior,
            position: {},
            tabs: [],
        },
    },
} as MapState;