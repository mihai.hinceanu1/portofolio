import * as globalMapActions from './maps.actions';
import { Action } from '../../shared/interfaces/shared';
import { CATEGORY_BRANCH_START_POSITION } from '../constants/global-map.const';
import { GetLearningMapCategoryBranchEntitiesReq } from '../interfaces/maps';
import * as globalMapWebapi from '../webapis/maps.webapi';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { /*getLearningMapRawCategoryBranch, */ setLevelsCounter, setMapStartIsoPosition } from '../../learning-map/services/learning-map.service';

let levelCounter = 0;

export const getLearningMapRawCategoryBranchByDomainIdCategoryId =
    (action$: ActionsObservable<Action<GetLearningMapCategoryBranchEntitiesReq>>) =>
        action$.ofType(globalMapActions.GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_REQ).pipe(
            map(action => action.payload),
            concatMap(idsDomainCategory =>
                globalMapWebapi.getLearningMapRawCategoryBranch(
                    idsDomainCategory.domainId, idsDomainCategory.categoryId,
                ).pipe(
                    map((globalMapRaw) => {
                        levelCounter++;
                        setLevelsCounter(levelCounter);
                        return globalMapActions.getLearningMapRawCategoryBranchOk(globalMapRaw);
                    }),
                    catchError(err => of(globalMapActions.getLearningMapRawCategoryBranchFail(err))),
                ),
            ),
        );

export const getDomainLevelById =
    (action$: ActionsObservable<Action<string>>) =>
        action$.ofType(globalMapActions.GET_GLOBAL_MAP_DOMAIN_LEVEL_REQ).pipe(
            map(action => action.payload),
            concatMap((domainId: string) => {
                return (globalMapWebapi.getLevelByIdEpicTriggerCategoriesBranches(domainId).pipe(
                    map(domainLevel => {
                        levelCounter = 0;
                        setLevelsCounter(0);
                        // let categoriesIds = domainLevel.childrenLevelsIds;

                        // Reset start position to 0, 0, 0
                        let resetPos = CATEGORY_BRANCH_START_POSITION;
                        setMapStartIsoPosition(resetPos);

                        // categoriesIds.forEach(categoryIdParam => {
                        //     getLearningMapRawCategoryBranch({
                        //         domainId: domainLevel._id,
                        //         categoryId: categoryIdParam._id,
                        //     });
                        // });
                        return globalMapActions.getLearningMapDomainLevelOk(domainLevel);

                    }),
                    catchError(err => of(globalMapActions.getLearningMapDomainLevelFail(err))),
                ));
            }),
        );