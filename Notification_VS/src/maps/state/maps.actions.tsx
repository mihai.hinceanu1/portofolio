import { Action } from '../../shared/interfaces/shared';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';
import { INanolessonModal } from '../interfaces/entity';
import { GetLearningMapCategoryBranchEntitiesReq, IsoPosition, LearningMapRawCategoryBranch } from '../interfaces/maps';
// import { MapEntity } from '../interfaces/entity';

// ====== ENTITIES FOR THE GLOBAL MAP SCENE ======

// export const GET_GLOBAL_MAP_ENTITIES_REQ = 'GET_GLOBAL_MAP_ENTITIES_REQ';
// export const getLearningMapEntitiesReq = (req: GetLearningMapEntitiesReq): Action<GetLearningMapEntitiesReq> => ({
//     type: GET_GLOBAL_MAP_ENTITIES_REQ,
//     payload: req,
// });

// export const GET_GLOBAL_MAP_ENTITIES_OK = 'GET_GLOBAL_MAP_ENTITIES_OK';
// export const getLearningMapEntitiesOk = (entities: MapEntity<any>[]): Action<MapEntity<any>[]> => ({
//     type: GET_GLOBAL_MAP_ENTITIES_OK,
//     payload: entities,
// });

// export const GET_GLOBAL_MAP_ENTITIES_FAIL = 'GET_GLOBAL_MAP_ENTITIES_FAIL';
// export const getLearningMapEntitiesFail = (error: Error): Action<Error> => ({
//     type: GET_GLOBAL_MAP_ENTITIES_FAIL,
//     payload: error,
// });

// ====== RAW DATA FOR THE GLOBAL MAP SCENE ======
export const GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_REQ = 'GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_REQ';
export const getLearningMapRawCategoryBranchReq =
    (req: GetLearningMapCategoryBranchEntitiesReq): Action<GetLearningMapCategoryBranchEntitiesReq> => ({
        type: GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_REQ,
        payload: req,
    });

export const GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_OK = 'GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_OK';
export const getLearningMapRawCategoryBranchOk =
    (globalMapRawCategoryBranch: LearningMapRawCategoryBranch): Action<LearningMapRawCategoryBranch> => ({
        type: GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_OK,
        payload: globalMapRawCategoryBranch,
    });

export const GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_FAIL = 'GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_FAIL';
export const getLearningMapRawCategoryBranchFail =
    (error: Error): Action<Error> => ({
        type: GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_FAIL,
        payload: error,
    });

// ====== DOMAIN LEVEL FOR THE (CHILDREN) CATEGORIES IDS IN GLOBAL MAP SCENE ======
export const GET_GLOBAL_MAP_DOMAIN_LEVEL_REQ = 'GET_GLOBAL_MAP_DOMAIN_LEVEL_REQ';
export const getLearningMapDomainLevelReq =
    (req: string): Action<string> => ({
        type: GET_GLOBAL_MAP_DOMAIN_LEVEL_REQ,
        payload: req,
    });

export const GET_GLOBAL_MAP_DOMAIN_LEVEL_OK = 'GET_GLOBAL_MAP_DOMAIN_LEVEL_OK';
export const getLearningMapDomainLevelOk =
    (domainLevel: TaxonomyLevel): Action<TaxonomyLevel> => ({
        type: GET_GLOBAL_MAP_DOMAIN_LEVEL_OK,
        payload: domainLevel,
    });

export const GET_GLOBAL_MAP_DOMAIN_LEVEL_FAIL = 'GET_GLOBAL_MAP_DOMAIN_LEVEL_FAIL';
export const getLearningMapDomainLevelFail =
    (error: Error): Action<Error> => ({
        type: GET_GLOBAL_MAP_DOMAIN_LEVEL_FAIL,
        payload: error,
    });

// ====== DOMAIN LEVEL ID SELECTED FROM SIDE-PANEL MAIN MENU ======
export const SET_SELECTED_DOMAIN_ID = 'SET_SELECTED_DOMAIN_ID';
export const setSelectedDomainId = (domainId: string): Action<string> => ({
    type: SET_SELECTED_DOMAIN_ID,
    payload: domainId,
});

// ====== CATEGORY ID SELECTED FROM SIDE-PANEL MAIN MENU ======
export const SET_SELECTED_CATEGORY_ID = 'SET_SELECTED_CATEGORY_ID';
export const setSelectedCategoryId = (categoryId: string): Action<string> => ({
    type: SET_SELECTED_CATEGORY_ID,
    payload: categoryId,
});

// ====== SELECTED CATEGORY NAME FROM SIDE-PANEL ======
export const SET_SELECTED_CATEGORY_NAME = 'SET_SELECTED_CATEGORY_NAME';
export const setSelectedCategoryName = (categoryName: string): Action<string> => ({
    type: SET_SELECTED_CATEGORY_NAME,
    payload: categoryName,
});

export const SET_LEVELS_COUNTER = 'SET_LEVELS_COUNTER';
export const setLevelsCounter = (counter: number): Action<number> => ({
    type: SET_LEVELS_COUNTER,
    payload: counter,
})

// ======START POSITION OF THE FIRST ENTITY ======
/**
 * used for incrementing start position for each category brach
 * and for reset when switching a domain
 */
export const SET_MAP_START_ISO_POSITION = 'MAP_START_ISO_POSITION';
export const setMapStartIsoPosition = (position: IsoPosition): Action<IsoPosition> => ({
    type: SET_MAP_START_ISO_POSITION,
    payload: position,
});

// ====== SET NANOLESSON ======
export const SET_NANOLESSON_MODAL = 'SET_NANOLESSON_MODAL';
export const setNanolessonModal = (nanolessonState: INanolessonModal): Action<INanolessonModal> => ({
    type: SET_NANOLESSON_MODAL,
    payload: nanolessonState,
});