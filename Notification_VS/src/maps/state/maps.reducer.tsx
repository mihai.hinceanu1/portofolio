import * as actions from './maps.actions';
import { mapInitialState } from './maps.init-state';
import { Action } from '../../shared/interfaces/shared';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';
import { INanolessonModal } from '../interfaces/entity';
import { IsoPosition, LearningMapRawCategoryBranch } from '../interfaces/maps';
import { MapState } from '../interfaces/maps.state';

export const mapReducer = (state: MapState = mapInitialState, action: Action<any>): MapState => {

    let newState: MapState;

    switch (action.type) {

        case actions.GET_GLOBAL_MAP_RAW_CATEGORY_BRANCH_OK: {
            newState = {
                ...state,
                globalMapRawCategoryBranch: {
                    ...action.payload as LearningMapRawCategoryBranch,
                },
                /** Add to all so we can filter the ones needed in catalog (for selected category from menu) */
                globalMapRawAllCategoryBranches: [...state.globalMapRawAllCategoryBranches, action.payload],
            };

            return newState;
        }

        case actions.GET_GLOBAL_MAP_DOMAIN_LEVEL_OK: {
            newState = {
                ...state,
                globalMapDomainLevel: {
                    ...action.payload as TaxonomyLevel,
                },
            };

            return newState;
        }

        case actions.SET_LEVELS_COUNTER: {
            newState = {
                ...state,
                levelsCounter: action.payload as number,
            };

            return newState;
        }

        case actions.SET_SELECTED_DOMAIN_ID: {
            newState = {
                ...state,
                selectedDomainId: action.payload as string,
            };

            return newState;
        }

        case actions.SET_SELECTED_CATEGORY_ID: {
            newState = {
                ...state,
                selectedCategoryId: action.payload as string,
            };

            return newState;
        }

        case actions.SET_SELECTED_CATEGORY_NAME: {
            newState = {
                ...state,
                selectedCategoryName: action.payload as string,
            }

            return newState;
        }

        case actions.SET_MAP_START_ISO_POSITION: {
            newState = {
                ...state,
                startPosition: action.payload as IsoPosition,
            };

            return newState;
        }

        case actions.SET_NANOLESSON_MODAL: {
            return {
                ...state,
                nanolessonModal: action.payload as INanolessonModal,
            };
        }

        default:
            return state;
    }
};