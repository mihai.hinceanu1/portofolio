/** Entity types are used to identify what rendering method to apply for a entity config. */
export const ENTITY_TYPE = {

    /** Origin marker used for debugging. */
    OriginMarker: 'OriginMarker',

    /** Tile contours is a basic shape around a single tile that creates a border around the tile edges. */
    TileContour: 'TileContour',

    /** Line traveling along the X Y Z axis */
    MainBranch: 'MainBranch',

    /** Line with markers (circles for topics - a circle next to each topic) */
    TopicsCluster: 'TopicsCluster',

    /** Renders a project label that includes icon and text on the left side of the markers line. */
    /** UPDATE 30 Jul: TopicCluster contains project tags */
    // ProjectTags: 'ProjectTags',
};

/**
 * Each graphical asset has a default behavior for rendering geometry.
 * This behavior can be overridden with data defined on the entity (global or per graphical asset)
 * Think of these as of classes (general recipes on haw to build stuff).
 *
 * <!> This architecture might look odd at first sight.
 *     It was selected such that it is future compatible with functional programming and ECS game engines.
 *     For example, the data set will become the components in the ECS engine.
 *     The path method will become the systems in the ECS engine.
 *
 * <!> We did not use traditional OOP. OOP leads to diamond inheritance problem.
 *     Sticking with pure OOP will lead to a lot of trouble later on.
 */
