import { IsoPosition } from '../interfaces/maps';
import { Platform } from 'react-native';

// ====== LOGIC ======
/** Fixed distance between topics clusters on a main branch */
export const TOPICS_CLUSTERS_DISTANCE: number = 7;

/** Fixed distance between category branches on a main branch */
export const CATEGORY_BRANCHES_DISTANCE: number = 24; // TODO dynamic based on the max height of topics cluster
// Added 22 to be sure (it is too much but safe until dynamic)

/** First category branch start position */
export const CATEGORY_BRANCH_START_POSITION: IsoPosition = {
    x: 0,
    y: 0,
    z: 0,
};

/** First Cluster start position based on the main branch */
// TODO remove after dynamic start based on cluster branch will be functional
export const TOPICS_CLUSTER_START_POSITION: IsoPosition = {
    x: 3,
    y: 0,
    z: 0,
};

/** Distance between Main Branch Y and first Topics Clusters */
export const DEFAULT_LENGTH_CATEGORY_BRANCH: number = 6;

// ====== RENDERING STYLE ======
export const CATEGORY_BRANCH_ROOT_TOPIC_TEXT_SIZE: number = 35;
export const TOPICS_CLUSTER_ROOT_TOPIC_TEXT_SIZE: number = 25;
export const COORDINATES_DEBUGGING_TEXT_SIZE: number = 10;

// Thumbnails must respect 1:1 ratio
export const THUMBNAIL_SIZE = {
    width: 25,
    height: 25,
};

/** TODO investigate a more reliable solution for font */
export const FONT_LABELS: string = Platform.OS === 'web' ? 'Consolas' :
    Platform.OS === 'ios' ? 'Times New Roman' :
        'normal';

/** Measure of the left side menu */
export const LEFT_SIDE_MENU_WIDTH = 15;

/** Approximate width of the heart svg used in root topic */
export const ROOT_HEART_WIDTH = 25;

// SIDE MENU CONSTANTS
export const PADDING_GLOBAL_MAP_NAV = 20;
export const SIZE_OF_DOMAIN_ICON = 40;