import { APP_CFG } from '../../shared/config/app.config';
import axios from '../../shared/services/axios.service';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';
import { LearningMapRawCategoryBranch } from '../interfaces/maps';
import { from, Observable } from 'rxjs';
/**
 * Levels and Topics needed for global map
 */
export const getLearningMapRawCategoryBranch =
    (domainId: string, categoryId: string): Observable<LearningMapRawCategoryBranch> =>
        from<Promise<LearningMapRawCategoryBranch>>(
            axios.get<LearningMapRawCategoryBranch>(`${APP_CFG.api}/maps/global-map/${domainId}/${categoryId}`)
                .then(res => res.data),
        );

/**
 * Having the domain Id (from query param in global map),
 * will get all the level and we can use the childrenLevelsIds for categories
 */
export const getLevelByIdEpicTriggerCategoriesBranches =
    (domainId: string): Observable<TaxonomyLevel> =>
        from<Promise<TaxonomyLevel>>(
            axios.get<TaxonomyLevel>(`${APP_CFG.api}/taxonomy/levels/id/${domainId}`)
                .then(res => res.data),
        );