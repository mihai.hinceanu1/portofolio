
/**
 * Describes various entities that can be rendered on the screen.
 * To keep things simple we will allow only the currently defined properties.
 * <!> Any additional behavior and properties must be stored using Systems.
 * <!> Currently no systems are defined.
 */
export interface MapEntity<ComponentSet> {

    /** We need an id to easily identify the entities. */
    id: string;
    name?: string;

    /** Explains why this entity was created */
    description?: string;

    /**
     * Each Entity type can have it's own data set of properties.
     * These are global attributes for the entity.
     * Using these params we can enable various behaviors.
     * Each entity has it's own interface for the data object.
     */
    components?: ComponentSet;
}

/** A layer can have multiple entities, */
export interface MapLayer {
    id: string;
    name?: string;

    visibility: boolean;

    entities: MapEntity<any>[];
}

/** The map can have multiple layers. */
export interface MapScene {
    layers: MapLayer[];
}

/** Mobile: Learning map nanolesson modal */
export interface INanolessonModal {
    isVisible: boolean;
    nanolesson: any;
}