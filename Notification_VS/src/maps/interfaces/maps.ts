import { ITopicMarker, TopicsClusterBody, TopicsClusterRaw } from './topics-cluster-body';
// import { ProjectTagBody } from './geometry/project-tag-body';

/**
 * We are tracking all objects on the isometric map using "virtual" coordinates.
 * Check maps/_docs folder to see extra explanations on how this works.
 * The isometric coordinates get translated in 2d screen coordinates using a special util.
 */
export interface IsoPosition {
    x: number;
    y: number;
    z: number;
}

export interface OffsetPosition {
    x: number;
    y: number;
}

/**
 * Format the global map entities for easy manipulation at generating the scene
 * transform the data from topics and levels to the minimal needed for global map
 *
 * Global Map elements:
 * Category-name (single, selected category),
 * Topic Cluster(previously Top Level Topics),
 * Topic
 *
 * <!> Domains and Categories will be treated separately in the left-side menu
 */
// TODO remove after LearningMapRaw is functional
export interface CategoryCluster {

    /** Name of the category, rendered at the start of the main branch */
    categoryLabel: string;

    /** Isometric position (x,y,z) for the start of the branch */
    // a constant might be sufficient, directly in scene
    // startPosition: IsoPosition;

    /** List of all the Topics Clusters that are on this branch */
    topicsClusters: TopicsCluster[];
}

/**
 * TopicsCluster contains a group of topics, and projects tags along a secondary branch
 */
export interface TopicsCluster extends TopicsClusterBody {

    // Isometric position (x,y,z), location of the TILE that contains the Bezier Curve
    startPosition: IsoPosition;

    topics: ITopicMarker[];
    // projectTags: ProjectTagBody[];
}

/** Used in ActionsObservable from maps.epic for getting entities by Domain Id, Category Id */
export interface GetLearningMapCategoryBranchEntitiesReq {
    domainId: string;
    categoryId: string;
}

/**
 * Map raw data contains formatted information based on TaxonomyLevels and Topics
 * needed to create entities that generate the scene for drawing the map
 *
 * TODO add Local Map Raw
 */
export interface LearningMapRawCategoryBranch {
    /** The id of the level that contains all the TLTs - clusters as children  */
    levelId: string;

    categoryLabel: string;
    clusters: TopicsClusterRaw[];
}