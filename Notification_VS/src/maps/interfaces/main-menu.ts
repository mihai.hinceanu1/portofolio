export interface MenuItem {
    numberOfChildren?: number;
    _id: string;
    name: string;
    pathName: string;
}