import { INanolessonModal } from './entity';
import { IsoPosition, LearningMapRawCategoryBranch } from './maps';
import { TaxonomyLevel } from '../../taxonomy/interfaces/taxonomy-level';

export interface MapState {

    /** count number of ok */
    levelsCounter: number;
    /** global map levels and topics */
    globalMapRawCategoryBranch: LearningMapRawCategoryBranch;

    /** global map levels and topics used for topics catalog (with some filtering) */
    globalMapRawAllCategoryBranches: LearningMapRawCategoryBranch[];

    /** domain level */
    globalMapDomainLevel: TaxonomyLevel;

    /** selected domain Id from left-side main menu  */
    selectedDomainId: string;

    /** selected category id from left-side main menu  */
    selectedCategoryId: string;

    selectedCategoryName: string;

    /** start position of the global map first entity */
    startPosition: IsoPosition;

    /** TODO George -> only one flag? */
    topicsCatalogModal: boolean;

    popularTopicsModal: boolean;

    /** Mobile: Learning map nanolesson */
    nanolessonModal: INanolessonModal;
}
