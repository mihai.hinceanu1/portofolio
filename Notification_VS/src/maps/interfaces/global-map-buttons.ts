import { GraphPosition } from '../../graphs/interfaces/graphs';

export interface ButtonLearningMap {
    name: string;
    // _id to topic
    position: GraphPosition;
}