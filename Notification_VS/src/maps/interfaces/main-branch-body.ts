// ====== BRANCH LINE ======

/** Describes a branch line on the knowhow graph. */
export interface MainBranchBody {

    /** From the original position on which direction does the line grow */
    direction: 'x' | 'y' | 'z' | 'xy'; // axis for direction

    /** How many tiles does the path traverses? */
    length: number; // number of tiles

    /** How thick should the line be */
    thickness: 'S' | 'M' | 'L';

    /** Overrides default color. */
    fill?: string;

    /** Corner of the TILE that the line is anchored to */
    corner: 'top' | 'bottom' | 'left' | 'right';

    /** Text Label at the origin of the Main Branch */
    categoryLabel?: string;
}
