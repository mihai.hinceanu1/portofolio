import { GraphPosition } from '../../graphs/interfaces/graphs';

// ====== TOPICS CLUSTER ======
/**
 * Topic markers
 * Project tags
 */

/** Describes a branch (line with markers) on the graph (Entity) */
export interface TopicsClusterBody extends TopicsCluster {

    /** Direction of the line growth */
    direction: 'top' | 'bottom';

    /** Overrides default color. */
    fill?: string;
}

export interface TopicsCluster extends TopicsClusterRaw {
    // TODO check if this is redundant can we use the Raw array again here?
    topics: ITopicMarker[];
    projects: IProjectTag[];
}

/**
 * Raw interfaces for data only needed for Endpoint
 * The interfaces are extended with cosmetic properties for the layout of the Front End
 */
export interface TopicsClusterRaw {

    /** Root topic */
    rootTopic: string;
    numOfFavVotes: number;

    /** List of topics for rendering on the right of the topics markers */
    topics: TopicMarkerRaw[];

    /** List of topics for rendering on the right of the topics markers */
    projects: ProjectTagRaw[];
}

export interface ITopicMarker extends TopicMarkerRaw {

    /** Topic Tag Color / flag icon */
    // TODO replace with real meaning for colors blue, grey, light grey, orange
    // difficulty: 'Easy' | 'Goal' | 'Medium' | 'Hard' | 'Master';

    /** Screen position; used for creating a overlapping button on canvas */
    screenPosition: GraphPosition;
}
export interface IProjectTag extends ProjectTagRaw {

    /* Changes bgr color, icon color, etc... */
    type: 'Default' | 'Studied' | 'Goal' | 'Master';

    /** TODO handle difficulty instead of type */

    /** Screen position; used for creating a overlapping button on canvas */
    screenPosition: GraphPosition;
}

export interface TopicMarkerRaw {
    /** Topic Id  */
    topicId: string;

    /** Topic Name */
    name: string;

    /** Description is used in latest, popular topics catalog, and also can be used in nanolesson */
    description: string;
    created: number;
    numberOfFavorites: number;
    pathName: string;

    difficulty: any;
    sentimentId: string;
}

export interface ProjectTagRaw {
    /** Name of the project. */
    name: string;

    /** Icon rendered in the project tag. */
    thumbnail: string;

    /** Linked topic (used for navigation) */
    topicId: string;

    /** Description is used in latest, popular topics catalog, and also can be used in nanolesson */
    description: string;
    created: number;
    numberOfFavorites: number;
    pathName: string;

    difficulty: string;
    sentimentId: string;
}