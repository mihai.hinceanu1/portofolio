import { TILE } from '../../graphics-engine/constants/map-world.const';

/** SVG paths for constant shapes used in Global Map */

// ====== BEZIER CURVES ======

// Topics branch start, goes top-right connects Main Branch with the topic cluster
// TODO bezier investigate
export const CURVE_MAIN_BRANCH_TO_TOP_RIGHT = `
    M 27.016 4.121
    C 27.016 3.474 27.016 0.689 27.016 0.014
    L 30.975 0.014
    C 30.975 1.159 30.974 4.398 30.974 5.465
    C 30.974 23.475 22.237 34.571 9.793 39.844
    C 8.825 40.254 5.729 41.559 1.867 43.196
    L 0.197 39.56
    C 3.942 37.972 6.491 36.893 7.571 36.435
    C 21.938 30.347 27.016 19.519 27.016 4.121
    Z`;

// Topics branch start, goes bottom-right connects Main Branch with the topic cluster
export const CURVE_MAIN_BRANCH_TO_BOTTOM_RIGHT = `
    M 21.016 37.156
    C 21.016 37.719 21.016 38.401 21.016 38.988
    L 24.975 38.988
    C 24.975 37.992 24.974 36.915 24.974 35.987
    C 24.974 13.994 24.705 -4.381 8.726 2.403
    C 7.776 2.807 4.735 4.176 0.94 5.768
    L 1.873 9.22
    C 5.555 7.676 8.059 6.906 9.121 6.462
    C 23.241 0.54 21.016 23.767 21.016 37.156
    Z`;

// ====== STRAIGHT LINES Y-AXIS ======

// Straight line from PSD svg design connects curve with topics markers for top cluster
export const STRAIGHT_LINE_TOP = `
    M 0 0
    L 4 0
    L 4 44
    L 0 44
    L 0 0
    Z`;

// Straight line from PSD svg design connects curve with topics markers for bottom cluster
export const STRAIGHT_LINE_BOTTOM = `
    M 0 0
    L 4 0
    L 4 ${TILE.height}
    L 0 ${TILE.height}
    L 0 0
    Z`;

// ====== TOPICS MARKERS ======
// Line with circles, first marker is connected with a straight line and with the following topics

export const TOPIC_MARKER_TOP = `
    M 9 13.082
    L 9 23
    L 5 23
    L 5 13.105
    C 2.479 12.277 0.656 9.908 0.656 7.109
    C 0.656 3.623 3.482 0.797 6.969 0.797
    C 10.455 0.797 13.281 3.623 13.281 7.109
    C 13.281 9.884 11.488 12.235 9 13.082
    Z
    M 6.969 3.692
    C 5.081 3.692 3.552 5.222 3.552 7.109
    C 3.552 8.997 5.081 10.527 6.969 10.527
    C 8.856 10.527 10.386 8.997 10.386 7.109
    C 10.386 5.222 8.856 3.692 6.969 3.692
    Z`;

// TOPICS_MARKER_TOP flipped horizontally
export const TOPIC_MARKER_BOTTOM = `
    M13.344,15.891
    C13.344,19.377 10.518,22.203 7.031,22.203
    C3.545,22.203 0.719,19.377 0.719,15.891
    C0.719,13.116 2.512,10.765 5.000,9.918
    L5.000,0.000 L9.000,0.000 L9.000,9.895
    C11.521,10.723 13.344,13.092 13.344,15.891 ZM7.031,12.473
    C5.144,12.473 3.614,14.003 3.614,15.891
    C3.614,17.778 5.144,19.308 7.031,19.308
    C8.919,19.308 10.448,17.778 10.448,15.891
    C10.448,14.003 8.919,12.473 7.031,12.473
    Z`;


export const TOPIC_GOAL_BOTTOM = `
M19.989 14.4648C19.989 19.7115 15.7357 23.9648 10.489 23.9648C5.24234 23.9648 0.989014 19.7115 0.989014 14.4648C0.989014 9.90424 4.20269 6.0942 8.48901 5.17574L8.48901 0.964844H12.489V5.17574C16.7753 6.09419 19.989 9.90423 19.989 14.4648ZM10.9102 17.0859L17.1484 12.9258L10.9102 8.76562V17.0859L9.65625 13.9727V8.76562H7.58174V13.4981V17.3478V21.2466H9.65625V13.9727L10.9102 17.0859Z
`;

export const TOPIC_GOAL_TOP = `
M0.5 9.90674C0.5 4.66005 4.75331 0.406738 10 0.406738C15.2467 0.406738 19.5 4.66005 19.5 9.90674C19.5 14.4673 16.2863 18.2774 12 19.1958V23.4067H8V19.1958C3.71367 18.2774 0.5 14.4674 0.5 9.90674ZM9.15801 4.19385H7.07959V16.6644H9.15801V4.19385ZM10.4044 12.5075L16.6397 8.35068L10.4044 4.19385V12.5075Z

`;
// ====== PROJECT TAG ======
// Container box
// TODO use this for dynamic PROJECT_TAG_BOX
export const PROJECT_TAG_BOX_SIZE = {
    width: 140,
    height: 30,
};

export const PROJECT_TAG_BOX = `
    M -150 -15
    L -10 -15
    L -10 15
    L -150 15
    L -150 -15
    `;

// dimensions (width, height) of the SVG path for the TOPIC_MARKER
export const TOPIC_MARKER_DIMENSIONS = {
    width: 14, // @design please provide with svg without white spaces
    height: 23 - 1, // @design please provide with svg without white spaces (remove -1 after)
};

// TODO change the dummy values for width and height
export const PROJECT_TAG_DIMENSIONS = {
    width: 150,
    height: 30,
};

export const PROJECT_TAG_DIMENSIONS_OFFSET_INCLUDED = {
    width: 150,
    height: 30 + TOPIC_MARKER_DIMENSIONS.height, // TODO double check if this is correct
};

// CLUSTER_END STRAIGHT SEMI_LINE
// multiple (based on number of topic tags / project markers)semi-lines create the end line
export const CLUSTER_END_SEMI_LINE = `
    M-0.000,-0.000
    L4.000,-0.000
    L4.000,39.000
    L-0.000,39.000
    L-0.000,-0.000
    Z`;

export const CLUSTER_END_MARKER_TOP = `
    M9.000,13.082
    L9.000,23.000
    L5.000,23.000
    L5.000,13.104
    C2.479,12.277 0.656,9.907 0.656,7.109
    C0.656,3.623 3.482,0.797 6.969,0.797
    C10.455,0.797 13.281,3.623 13.281,7.109
    C13.281,9.884 11.488,12.235 9.000,13.082
    Z
    `;

// CLUSTER_END_MARKER_TOP flipped horizontally
export const CLUSTER_END_MARKER_BOTTOM = `
    M 13.281 13.109
    C 13.281 16.595 10.455 19.422 6.969 19.422
    C 3.482 19.422 0.656 16.595 0.656 13.109
    C 0.656 10.311 2.479 7.942 5 7.114
    L 5 0
    L 9 0
    L 9 7.137
    C 11.488 7.983 13.281 10.334 13.281 13.109
    Z`;

export const ROOT_HEART = `
    M 17.04, 4.92,
    c 2.535, 0, 4.595, 2.065, 4.595, 4.615,
    c 0, 1.31, -0.545, 2.49, -1.415, 3.33,
    L 12.8, 20.355,
    L 5.25, 12.73,
    c -0.79, -0.83, -1.28, -1.955, -1.28, -3.195,
    c 0, -2.55, 2.055, -4.615, 4.595, -4.615,
    c 1.91, 0, 3.545, 1.17, 4.24, 2.84,
    C 13.49, 6.095, 15.13, 4.92, 17.04, 4.92
    z`;