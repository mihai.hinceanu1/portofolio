import * as div from './isometric-map.style';
import { TILE } from '../../../graphics-engine/constants/map-world.const';
import { mapIsometricPositionToScreenPosition } from '../../../graphics-engine/services/isometric.utils';
import { mapSceneEntitiesToSVG } from '../../../graphics-engine/services/render-viewport';
import { GraphDimensions, GraphPosition } from '../../../graphs/interfaces/graphs';
import { isNotUndefined } from '../../../shared/services/failsafe.utils';
import { CATEGORY_BRANCH_START_POSITION, CATEGORY_BRANCHES_DISTANCE } from '../../constants/global-map.const';
import { MapScene } from '../../interfaces/entity';
import { IsoPosition, LearningMapRawCategoryBranch } from '../../interfaces/maps';
import { IProjectTag, ITopicMarker } from '../../interfaces/topics-cluster-body';
import { drawCategoryBranchScene } from '../../utils/global-map.create-entities';
import { ProjectNanoLesson } from '../project-nano-lesson/project-nano-lesson';
import { TopicNanoLesson } from '../topic-nano-lesson/topic-nano-lesson';
import * as React from 'react';
import Svg, {
    Defs,
    G,
    Image,
    LinearGradient,
    Pattern,
    Rect,
    Stop
    } from 'react-native-svg';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
    getDomainLevelByIdEpicTriggerRawCategories,
    globalMapRawCategoryBranch$,
    selectedDomainId$,
    setMapStartIsoPosition,
    mapStartIsoPosition$,
    selectedCategoryId$,
    setSelectedDomainId,
    setSelectedCategoryId,
} from '../../../learning-map/services/learning-map.service';
import {
    LayoutChangeEvent,
    PanResponder,
    PanResponderGestureState,
    PanResponderInstance,
    Platform,
    Dimensions,
} from 'react-native';

export interface Props {
    taxonomy?: any; // We have to decide what format to use
    topics?: any; // We have to decide what format to use
    config: {

    };
}

interface State {
    width: number;
    height: number;

    /** <!> Everything on the map is positioned in relation to these two coordinates. */
    pan: {
        x: number;
        y: number;
    };
    zoom: number;

    globalMapRawCategoryBranch: LearningMapRawCategoryBranch;

    globalMapSceneAllCategoryBranches: MapScene;

    selectedCategoryId: string;

    selectedNanolessonId: string;
}

/**
 * Isometric map is a generic component that can be used to render all the VSC knowledge maps.
 *
 * Features:
 *  - Stretches to parent dimensions
 *  - Renders isometric floor tiles
 *  - Pans viewport on pointer drag
 *  - Isometric positioning logic
 *
 * <!> Architecture
 * Currently the rendering logic is completely custom made.
 * This was the least expensive route to take in performance and training terms.
 * However this has severe limitations when it comes to rendering complex game worlds and animations.
 * An entity component system graphics engine should be implemented with a real game loop.
 * This is a planed feature for future releases.
 * Currently we will avoid any fancy animations and interactions.
 * To render multiple animations in parallel at the current moment we have to simply create multiple loops.
 *
 * TODO
 *  - Optimization - Make sure that elements outside of the canvas are not even requested for a rendering cycle.
 */
export class IsometricMap extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    private panResponder: PanResponderInstance;

    // Cache the start position to compute viewport movement in relation to pan start.
    private panStart: State['pan'];

    constructor(props: Props) {
        super(props);
        this.state = {
            /** Debugging dimensions for android  */
            width: 0,
            height: 0,
            pan: {
                x: Platform.OS === 'web' ? 350 : Dimensions.get('window').width / 2 + 50,
                y: 180, // some space for the menu
            },
            selectedCategoryId: '',
            globalMapSceneAllCategoryBranches: {} as MapScene,
            globalMapRawCategoryBranch: null,
            selectedNanolessonId: null,
            zoom: null,
        };

        // Setup
        this.setupViewportPan();
    }

    public render() {
        let { height, width, pan, globalMapSceneAllCategoryBranches } = this.state;
        let globalMapReceived = isNotUndefined(globalMapSceneAllCategoryBranches);

        // Entities
        let entities: JSX.Element[];
        if (globalMapReceived) {
            entities = mapSceneEntitiesToSVG(globalMapSceneAllCategoryBranches, this.state.pan);
        }

        // Viewport
        const viewport: GraphDimensions & GraphPosition = { x: 0, y: 0, height, width };

        // Floor tiles
        /** TODO investigate if we want floor pattern on mobile */
        // TO DO Restore with react-native-svg
        // const floorGradient = this.getFloorGradient();

        return (
            globalMapReceived &&
            <div.IsometricMap data-cy='isometric-map'
                {...this.panResponder.panHandlers}
                onLayout={(e: LayoutChangeEvent) => this.fitInView(e)}>

                <div.Surface data-cy='surface-container'>
                    <Svg width={viewport.width} height={viewport.height} viewBox={`0 0 ${viewport.width} ${viewport.height}`}>
                        <Defs>
                            {/* Define the pattern */}
                            <Pattern
                                id='floorPattern'
                                patternUnits='userSpaceOnUse'
                                // Change the pattern according to the pan movement.
                                x={pan.x}
                                y={pan.y}
                                width={TILE.width}
                                height={TILE.height}
                                viewBox={`0 0 ${TILE.width} ${TILE.height}`}
                            >
                                <Image href={require('../../assets/isometric-tile.png')} width={TILE.width} height={TILE.height} />
                            </Pattern>

                            {/* This will fix when the viewport height will be correct */}
                            {/* Define the gradient */}
                            <LinearGradient id='grad' x1={0} y1={viewport.height - 300} x2={0} y2={viewport.height} gradientUnits='userSpaceOnUse'>
                                <Stop offset='0' stopColor='rgba(255,255,255,0)' stopOpacity='0' />
                                <Stop offset='1' stopColor='rgba(208, 228, 239,0.65)' stopOpacity='0.65' />
                            </LinearGradient>
                        </Defs>

                        <G x={0} y={0}>
                            <G>
                                {/* Gradient effect */}
                                <Rect fill='url(#grad)' x='0' y='0' width={viewport.width} height={viewport.height} stroke='red' strokeWidth='1' />
                                {/* Floor pattern */}
                                <Rect fill='url(#floorPattern)' x='0' y='0' width={viewport.width} height={viewport.height} />
                            </G>

                            {/* The entire content */}
                            {entities}
                        </G>
                    </Svg>
                </div.Surface>

                {/** Buttons */}
                {/** Overlapping buttons for topics markers, projects tags TODO Separate Component */}
                {
                    /**
                     * Added a bug after messing with the size of the View Container (also with method fitInView)
                     * <!> overlapping buttons are no longer clickable
                     */
                }
                <div.OverlappingButtons data-cy='overlapping-buttons'
                    {...this.panResponder.panHandlers}
                    onLayout={(e: LayoutChangeEvent) => this.fitInView(e)}

                    /** Reposition the buttons when panning the map */
                    style={{
                        left: pan.x,
                        top: pan.y,
                    }}>

                    {/** Creating the arrays of TouchableOpacity for: */}

                    {/** - Project Tags */}
                    {this.getProjectTagsButtons(globalMapSceneAllCategoryBranches)}

                    {/** - Topic Markers */}
                    {this.getTopicMarkersButtons(globalMapSceneAllCategoryBranches)}
                </div.OverlappingButtons>
            </div.IsometricMap>
        );

        // TODO create overlapping view with all the buttons at the same positions as topics markers / projects tags

    }

    public componentDidMount() {

        /** TODO Remove this - small workaround for fast solution to show a toggled id for kick */
        setSelectedDomainId('5bf5d57bc9fac78ca21e88f5');
        /** TODO Remove this - small workaround for fast solution to show a toggled id for kick */

        setSelectedCategoryId('');

        this.subscribeToLearningMapRawCategoryBranch();
        this.subscribeToSelectedDomainId();
        this.subscribeToSelectedCategoryId();
        // From URL for now / STATE STORE can be a viable alternative

        // For each category, Push the globalMapRaw(single MainBranch) into an array from this component state
        // <!> UNIQUE only
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    /** TODO move to parent ans send here as props --> also to menu */
    private subscribeToSelectedDomainId() {
        selectedDomainId$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(selectedDomainId => {
                this.setState({
                    globalMapSceneAllCategoryBranches: {} as MapScene,
                }, () => getDomainLevelByIdEpicTriggerRawCategories(selectedDomainId));
            });
    }

    /** Pan to the selected category */
    private subscribeToSelectedCategoryId() {
        selectedCategoryId$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(selectedCategoryId => {
                let targetPan = this.getTargetCategoryPan(selectedCategoryId);
                this.setState({
                    selectedCategoryId,
                    pan: {
                        ...targetPan,
                    },
                });
            });
    }

    /**
     * More that a simple subscribe
     * TODO split into single responsibility functions
     */
    private subscribeToLearningMapRawCategoryBranch() {
        let startPosition: IsoPosition = CATEGORY_BRANCH_START_POSITION;

        // Reset start position
        mapStartIsoPosition$().pipe(takeUntil(this.destroyed$)).subscribe(newStartPosition => {
            startPosition = newStartPosition;
        });

        globalMapRawCategoryBranch$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(globalMapRawCategoryBranch => {

                // <!> Adding all category clusters into a copy that is saved in component state
                // globalMapSceneAllCategoryClusters

                // TODO change this condition, make it based on null id, after adding id
                // this is a workaround for now
                let categoryBranchDistance = CATEGORY_BRANCHES_DISTANCE;

                // Category branch has no topics clusters (is empty) but has category label, is child of a domain
                if (!globalMapRawCategoryBranch.clusters.length) {
                    categoryBranchDistance = 3;
                }

                if (globalMapRawCategoryBranch.categoryLabel !== '') {
                    let sceneSingleCategoryCluster =
                        drawCategoryBranchScene(globalMapRawCategoryBranch, startPosition, categoryBranchDistance);

                    // Compute start position for next Category Branch
                    startPosition = {
                        ...startPosition,
                        y: startPosition.y - categoryBranchDistance,
                        // instead of -categoryBranchDistance
                        // TODO dynamic step here between category clusters
                        // (max of bottom anterior cluster + max of current top topics cluster)
                    };
                    setMapStartIsoPosition(startPosition);

                    let entitiesSingleCategoryCluster = sceneSingleCategoryCluster.layers[0].entities;

                    let copyAllCategoryClusters = this.state.globalMapSceneAllCategoryBranches;

                    // Check if copy is empty
                    let copyAllIsNotEmpty =
                        !(Object.keys(copyAllCategoryClusters).length === 0 &&
                            copyAllCategoryClusters.constructor === Object);

                    // For the first category branch
                    if (copyAllIsNotEmpty) {
                        // Append entities to copy
                        copyAllCategoryClusters.layers[0].entities.push(...entitiesSingleCategoryCluster);
                        this.setState({
                            globalMapSceneAllCategoryBranches: copyAllCategoryClusters,
                        });
                    } else {
                        // If copy is empty initialize it
                        this.setState({ globalMapSceneAllCategoryBranches: sceneSingleCategoryCluster });
                    }

                }

            });
    }

    private fitInView(event: LayoutChangeEvent) {

        /** QUICKFIX for having something render on the page. Height should be taken dynamically. */
        let { width /*, height*/ } = (Platform.OS === 'web') ? event.nativeEvent.layout :
            {
                width: Math.round(Dimensions.get('window').width),
                // height: Math.round(Dimensions.get('window').height),
            };

        /** TODO investigate why extra white space appeared on top of the app */
        /** <!> Added a bug: SCROLL ON PAGE */
        /** Possible fix: remove top space from app header? */
        this.setState({
            // Avoid scrollbars rendered due to subpixel imprecision.

            // Part of hotfix, uncomment these once it's fixed.
            // width: width - 1,
            // height: height - 1,

            // Removed these once height issue is fixed.
            width,
            height: 2000,
        });
    }

    // TO DO Restore with react-native-svg
    /**
     * Render a gradient that has fixed position to the bottom of screen.
     * On smaller screen sizes this gradient simply shrinks down.
     * We put the trouble of computing this gradient so we
     * can also render at the top a gradient using one layer (if need be).
     */
    // private getFloorGradient(): any {
    //     let { height } = this.state;

    //     // We want the gradient to stick to the bottom of the screen at bottom:300px position
    //     let gradientHeight = 300;
    //     let midPointPos = height - gradientHeight;
    //     let ratio = midPointPos / height || 1;

    //     if (ratio < 0.5) {
    //         // Small screen sizes
    //         ratio = 0.5;
    //     } else if (ratio > 1) {
    //         // Failsafe
    //         ratio = 1;
    //     }

    //     let gradientBands: { [ratio: string]: string } = {
    //         '0': 'rgba(255, 255, 255, 0)',
    //         '1': 'rgba(208, 228, 239, .65)',
    //     };

    //     // Mid point where the gradient fades out
    //     // Yep, this how the API actually wants this ratio... as a property name
    //     gradientBands[ratio] = 'rgba(255, 255, 255, 0)';

    //     return new LinearGradient(gradientBands, 0, 0, 0, height || 0);

    // }

    /**
     * Uses cached start position to compute viewport movement in relation to pan start.
     * This will make sure that we position our viewport using only cumulated and properly rounded values.
     * Adding small floating point increments seems a bit risky.
     */
    private updateViewportPosition(gestureState: PanResponderGestureState) {

        this.setState({
            pan: {
                x: this.panStart.x + gestureState.dx,
                y: this.panStart.y + gestureState.dy,
            },
        });

        // TODO make this work
        let { selectedCategoryId } = this.state;
        // To be able to click one category twice in a with a pan in between
        if (selectedCategoryId !== '') {
            this.setState({
                selectedCategoryId: '',
            });
        }
    }

    /**
     * Pan feature allows the student to navigate by dragging the map with the fingers / pointer.
     * https://facebook.github.io/react-native/docs/panresponder
     */
    private setupViewportPan() {

        // this.panResponder; // DELETE
        this.panResponder = PanResponder.create({

            // Ask to be the responder
            onStartShouldSetPanResponder: (_event, _gestureState) => true,

            // To allow clickable elements over pan - this should be false
            onStartShouldSetPanResponderCapture: (_event, _gestureState) => false,

            onMoveShouldSetPanResponder: (_event, _gestureState) => true,

            // Possible alternatives that might be useful for clickable elements
            // onMoveShouldSetPanResponder: (_event, gestureState) => {
            //     // return true if user is swiping, return false if it's a single click
            //     return !(gestureState.dx === 0 && gestureState.dy === 0);
            // },

            // onMoveShouldSetPanResponder: (_event, gestureState) => {
            //     const { dx, dy } = gestureState;
            //     return dx > 2 || dx < -2 || dy > 2 || dy < -2;
            // },

            onMoveShouldSetPanResponderCapture: (_event, _gestureState) => true,

            onPanResponderGrant: (_event, _gestureState) => {
                // Clone starting point
                this.panStart = { ...this.state.pan };
            },
            onPanResponderMove: (_event, gestureState) => {
                this.updateViewportPosition(gestureState);
            },
        });
    }

    /** Creates Buttons for project tags overlapping the canvas */
    private getProjectTagsButtons(globalMapSceneAllCategoryBranches: MapScene): JSX.Element[] {
        let buttonsRendered: JSX.Element[] = [];
        const { selectedNanolessonId } = this.state;

        let entities = globalMapSceneAllCategoryBranches.layers[0].entities;

        entities.forEach(entity => {
            // Is Cluster and has projects tags
            if ((entity.components.body.type === 'TopicsCluster') && (entity.components.body.projects.length !== 0)) {
                let projectTags = entity.components.body.projects;

                projectTags.forEach((projectTag: IProjectTag) => {
                    buttonsRendered.push(
                        <ProjectNanoLesson
                            key={projectTag.topicId}
                            projectTag={projectTag}
                            nanolessonId={selectedNanolessonId}
                            onNanolessonPress={(id: string) => this.setSelectedNanolesson(id)}
                        />,

                    );
                });
            }

        });
        return buttonsRendered;
    }

    private setSelectedNanolesson(id: string) {

        this.setState({
            selectedNanolessonId: id,
        });
    }

    /** Creates Buttons for topic markers overlapping the canvas */
    private getTopicMarkersButtons(globalMapSceneAllCategoryBranches: MapScene): JSX.Element[] {
        let buttonsRendered: JSX.Element[] = [];
        const { selectedNanolessonId } = this.state;

        let entities = globalMapSceneAllCategoryBranches.layers[0].entities;
        entities.forEach(entity => {
            // Is Cluster and has topics (markers)
            if ((entity.components.body.type === 'TopicsCluster') && (entity.components.body.topics.length !== 0)) {
                let topicMarkers = entity.components.body.topics;

                topicMarkers.forEach((topicMarker: ITopicMarker) => {
                    buttonsRendered.push(
                        <TopicNanoLesson
                            key={topicMarker.topicId}
                            topicMarker={topicMarker}
                            nanolessonId={selectedNanolessonId}
                            onNanolessonPress={(id: string) => this.setSelectedNanolesson(id)}
                            markerDirection={entity.components.body.direction}
                        />,
                    );
                });
            }

        });
        return buttonsRendered;
    }

    /** Position for panning to when a certain category is selected from left-side menu */
    private getTargetCategoryPan(categoryId: string): GraphPosition {

        let { globalMapSceneAllCategoryBranches, height } = this.state;

        // Isometric Position of the target category branch
        let isometricPosition;
        if (globalMapSceneAllCategoryBranches.layers !== undefined) {
            isometricPosition = globalMapSceneAllCategoryBranches.layers[0].entities.find(entity => {
                return entity.id === categoryId;
            }).components.position;
        } else {
            let screenPosition = {
                x: 6,
                y: 0,
            };
            return screenPosition;
        }

        // Map to screen pos
        let screenPosition = mapIsometricPositionToScreenPosition(TILE, isometricPosition);

        // Further math for perfect pan location
        screenPosition = {
            x: 350,
            y: (-1) * screenPosition.y + height / 2,
        };

        return screenPosition;
    }
}