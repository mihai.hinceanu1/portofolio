import styled from 'styled-components/native';

export const IsometricMap = styled.View`
    /* Uncomment this once every page is back to normal sizes */
    /* flex: 1; */
    flex-direction: row;

    /* Delete this once every page is back to normal page */
    height: 3000px;

    max-width: 3000px;
    max-height: 3000px;
`;

export const Surface = styled.View`
    width: 100%;
    height: 100%;
`;

export const OverlappingButtons = styled.View`
    position: absolute;
    top: 0;
    left: 0;
    /* Set dimensions so we can be able to press buttons on android
       Otherwise this container would have 0 width & 0 height and buttons can't be pressed */
    width: 3000px;
    height: 3000px;
    background-color: transparent;
`;