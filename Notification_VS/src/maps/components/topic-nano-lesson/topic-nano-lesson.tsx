import { TOPIC_MARKER_DIMENSIONS } from '../../assets/topics-cluster.assets';
import { ITopicMarker } from '../../interfaces/topics-cluster-body';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    topicMarker: ITopicMarker;
    onNanolessonPress: (id: string) => void;
    nanolessonId: string;
    markerDirection: string;
}

interface State {
    toggledNanolesson: boolean;
}

export class TopicNanoLesson extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            toggledNanolesson: false,
        };
    }

    public render() {

        const { topicMarker } = this.props;

        return (

            <View data-cy='topic-nano-lesson'
                style={{
                    position: 'absolute',
                    left: topicMarker.screenPosition.x,
                    top: topicMarker.screenPosition.y,
                    height: TOPIC_MARKER_DIMENSIONS.height,
                    width: TOPIC_MARKER_DIMENSIONS.width,
                    // backgroundColor: 'rgba(0, 255, 0, 0.5)',
                }}/>
        );
    }
}