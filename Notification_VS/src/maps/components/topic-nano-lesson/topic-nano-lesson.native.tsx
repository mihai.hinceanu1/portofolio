import { NANOLESSON_DIFFICULTIES } from '../../../nano-lessons/constants/difficulties.const';
import { APP_CFG } from '../../../shared/config/app.config';
import { ITopicMarker } from '../../interfaces/topics-cluster-body';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    topicMarker: ITopicMarker;
    difficulty: NANOLESSON_DIFFICULTIES;
}

interface State {
    nanolesson: any;
}

export class TopicNanoLesson extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            nanolesson: {
                _id: 'default', // Only on is active at a time, but id field is required
                difficulty: props.difficulty,
                position: {},
                tabs: [
                    {
                        _id: 'default', // Only on is active at a time, but id field is required
                        icon: '/thumbnails/open-bookWhite.png',
                        name: props.topicMarker.name,
                        title: props.topicMarker.description,
                        link: {
                            title: 'Explore',
                            url: `${APP_CFG.domain + '/topic-new/' + props.topicMarker.pathName}`,
                            // TODO handle external
                        },
                    },
                ],
            },
        };
    }

    public render() {
        return (
            <View />
        );
    }
}