import { learningMapDomainsCfg } from './learning-map-hover-menus.utils';
import { toggleCatalog } from '../../../lessons-catalog/components/lessons-catalog-hover-menus/lessons-catalog-hover-menus.configs';
import { addTopicsCatalogModal$ } from '../../../lessons-catalog/services/lessons-catalog.service';
import { getAppLogo } from '../../../lessons/components/lesson-hover-menus/lesson-hover-menus.utils';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { DotsMenu } from '../../../shared/components/dots-menu/dots-menu';
import * as sharedCfg from '../../../shared/constants/hover-menus.configs';
import * as hover from '../../../shared/style/hover-menus.style';
import { getTaxonomyTree, tree$ } from '../../../taxonomy/services/taxonomy.service';
import * as React from 'react';
import { LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { NewTaxonomyLevelTree } from '../../../taxonomy/interfaces/taxonomy-level';
// import { allLevelsTree$, getAllLevelsTree } from '../../../taxonomy/services/taxonomy.service';

interface Props extends RouteComponentProps<never> { }

interface State {
    width: number;
    catalogOpened: boolean;
    domains: any[];
}

class _LearningMapHoverMenus extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            width: 0,
            catalogOpened: false,
            domains: [],
        };
    }

    public render() {
        const { width, domains } = this.state;

        return (
            <hover.Menus data-cy='lesson-hover-menus'
                pointerEvents='box-none'
                onLayout={(e: LayoutChangeEvent) => this.updateWidth(e)}>

                {/* Learning Map Domains */}
                <BranchesMenu config={learningMapDomainsCfg(domains)}
                    overrides={hover.overrideTopLeftMenu(width)} />

                {/* Logo */}
                <hover.AppLogo width={width}>
                    {getAppLogo()}
                </hover.AppLogo>

                {/* Lesson Catalog Toggle */}
                <BranchesMenu config={toggleCatalog}
                    overrides={hover.overrideBottomLeftMenu(width)} />

                {/* User Menu */}
                <DotsMenu config={sharedCfg.userMenu}
                    overrides={hover.overrideTopRightMenu(width)} />

                {/* Layers Menu */}
                <DotsMenu config={sharedCfg.layersMenu}
                    overrides={hover.overrideBottomRightMenu(width)} />

            </hover.Menus>
        );
    }

    public componentDidMount() {
        this.subscribeToCatalogOpened();
        this.subscribeToTaxonomyTree();
        getTaxonomyTree();
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private subscribeToTaxonomyTree() {
        tree$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(tree => {
                this.setState({
                    domains: tree,
                });
            });
    }

    private subscribeToCatalogOpened() {
        addTopicsCatalogModal$().pipe(
            takeUntil(this.destroyed$)
        )
            .subscribe(catalogOpened => {
                this.setState({ catalogOpened });
            });
    }
}

export const LearningMapHoverMenus = withRouter(_LearningMapHoverMenus);