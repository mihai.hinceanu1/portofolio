import { setSelectedCategoryId, setSelectedCategoryName, setSelectedDomainId } from '../../../learning-map/services/learning-map.service';
import { LEARNING_MAP } from '../../../shared/assets/icons';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../../shared/constants/branches-menu.const';
import { BranchesMenuConfig, IBranch } from '../../../shared/interfaces/branches-menu';
import { colors } from '../../../shared/style/colors';

export const learningMapDomainsCfg = (domains: any[], isOverlayBellow: boolean = false) => {
    let branchesMenuCfg: BranchesMenuConfig = getRawConfig(isOverlayBellow);
    branchesMenuCfg = buildConfig(domains, branchesMenuCfg);

    return branchesMenuCfg;
};

export const getRawConfig = (isOverlayBellow: boolean = false) => {
    let branchesMenuCfg: BranchesMenuConfig = {
        icon: {
            svgPath: LEARNING_MAP,
            color: getBranchesIconColor(isOverlayBellow),
        },
        branchesDirection: BRANCHES_DIRECTIONS.right,
        stubDirection: STUB_DIRECTIONS.bottom,
        branches: [],
        color: getBranchesColor(isOverlayBellow),
        textColor: getTextColor(isOverlayBellow),
    };

    return branchesMenuCfg;
};

export const buildConfig = (domains: any[], branchesMenuCfg: BranchesMenuConfig) => {
    domains.forEach((domain) => {
        let newIBranch: IBranch = {
            _id: domain._id,
            name: domain.name,
            onPress: () => setSelectedDomainId(domain._id),
            children: [],
        };

        domain.children.forEach((children: any) => {
            let newChildrenBranch: IBranch = {
                _id: children._id,
                name: children.name,
                onPress: () => onChildPress(children.name, children._id),
            };

            newIBranch.children.push(newChildrenBranch);
        });

        branchesMenuCfg.branches.push(newIBranch);
    });

    return branchesMenuCfg;
};

export const onChildPress = (name: string, id: string) => {
    setSelectedCategoryName(name);
    setSelectedCategoryId(id);
};

function getBranchesIconColor(isOverlayBellow: boolean): string {
    return isOverlayBellow ? colors.$overlayBtnIcons : colors.$white;
}

function getBranchesColor(isOverlayBellow: boolean): string {
    return isOverlayBellow ? '#fff' : colors.$blue;
}

function getTextColor(isOverlayBellow: boolean): string {
    return isOverlayBellow ? '#fff' : colors.$black;
}