import { PROJECT_TAG_BOX_SIZE } from '../../assets/topics-cluster.assets';
import { IProjectTag } from '../../interfaces/topics-cluster-body';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    projectTag: IProjectTag;
    onNanolessonPress: (id: string) => void;
    nanolessonId: string;
}

interface State {
    toggledNanolesson: boolean;
}

/** Toggle a Nanolesson on the Global Map */
export class ProjectNanoLesson extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            toggledNanolesson: false,
        };
    }

    public render() {
        const { projectTag } = this.props;

        return (
            <View data-cy='project-nano-lesson'
                style={{
                    position: 'absolute',
                    left: projectTag.screenPosition.x,
                    top: projectTag.screenPosition.y,
                    height: PROJECT_TAG_BOX_SIZE.height,
                    width: PROJECT_TAG_BOX_SIZE.width,
                    // backgroundColor: 'rgba(0, 255, 0, 0.5)',
                }} />
        );
    }
}