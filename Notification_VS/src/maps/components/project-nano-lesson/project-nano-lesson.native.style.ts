import styled from 'styled-components/native';

interface Props {
    top: number;
    left: number;
    height: number;
    width: number;
}

/** Use for positioning on the global map */
export const ProjectNanoLesson = styled.TouchableOpacity<Props>`
    top: ${props => props.top ? props.top : 0}px;
    left: ${props => props.left ? props.left : 0}px;
    min-height: ${props => props.height ? props.height : 20}px;
    min-width: ${props => props.width ? props.width : 20}px;
    /* background-color: rgba(0, 255, 0, 0.5); */
    position: absolute;
`;