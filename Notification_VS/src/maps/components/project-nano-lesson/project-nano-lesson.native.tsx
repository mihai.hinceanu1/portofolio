import * as div from './project-nano-lesson.native.style';
import { setNanolessonModal } from '../../../learning-map/services/learning-map.service';
import { NANOLESSON_DIFFICULTIES } from '../../../nano-lessons/constants/difficulties.const';
import { APP_CFG } from '../../../shared/config/app.config';
import { PROJECT_TAG_BOX_SIZE } from '../../assets/topics-cluster.assets';
import { INanolessonModal } from '../../interfaces/entity';
import { IProjectTag } from '../../interfaces/topics-cluster-body';
import * as React from 'react';

interface Props {
    projectTag: IProjectTag;
    onNanolessonPress: (id: string) => void;
    nanolessonId: string;
}

interface State {
    nanolesson: any;
}

/**
 * We use this component when we want to toggle a Nanolesson on the Global Map
 */
export class ProjectNanolesson extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            nanolesson: {
                _id: 'default', // Only on is active at a time, but id field is required
                difficulty: NANOLESSON_DIFFICULTIES.Intermediate,
                position: {},
                tabs: [
                    {
                        _id: 'default', // Only on is active at a time, but id field is required
                        icon: props.projectTag.thumbnail,
                        name: props.projectTag.name,
                        title: props.projectTag.description,
                        link: {
                            title: 'Explore',
                            url: `${APP_CFG.domain + '/topic-new/' + props.projectTag.pathName}`,
                            // TODO handle external
                        },
                    },
                ],
            },
        };
    }

    public render() {
        const { projectTag } = this.props;

        return (
            <div.ProjectNanoLesson data-cy='project-nano-lesson'
                top={projectTag.screenPosition.y}
                left={projectTag.screenPosition.x}
                width={PROJECT_TAG_BOX_SIZE.width}
                height={PROJECT_TAG_BOX_SIZE.height}
                onPressIn={() => this.displayNanolesson()}
            />
        );
    }

    private displayNanolesson() {
        const { nanolesson } = this.state;
        let config: INanolessonModal = {
            isVisible: true,
            nanolesson,
        };
        setNanolessonModal(config);
    }
}