import { TILE } from '../../graphics-engine/constants/map-world.const';
import { PROJECT_TAG_DIMENSIONS_OFFSET_INCLUDED, TOPIC_MARKER_DIMENSIONS } from '../assets/topics-cluster.assets';
import { OffsetPosition } from '../interfaces/maps';

/**
 * Offsets are computed based of the relative origin which is the base of the topics cluster
 * situated at the given position in the scene
 */

/**
 * Manual offset to align the bezier curve to the center of the y axis
 * @returns offset for start curve, it differs for top / bottom
 */
export function getStartCurveOffset(direction: string): OffsetPosition {
    let offset: OffsetPosition;

    switch (direction) {
        case 'top': {
            offset = {
                x: -TILE.width / 4 - 4,
                y: -TILE.height / 2 + 4,
            };
            break;
        }
        case 'bottom': {
            offset = {
                x: -TILE.width / 4 + 2,
                y: -TILE.height / 2 + 5.5,
            };
            break;
        }
    }

    return offset;
}

/**
 * Manual offset to align the straight line to the center of the y axis
 * @returns offset for straight line following the start curve, it differs for top / bottom
 */
export function getStraightLineOffset(direction: string): OffsetPosition {
    let offset: OffsetPosition;

    switch (direction) {
        case 'top': {
            offset = {
                x: -2, // -width of the line(4) / 2
                y: -TILE.height - TILE.height / 2,
            };
            break;
        }
        case 'bottom': {
            offset = {
                x: -2,
                y: TILE.height / 2,
            };
        }
    }

    return offset;
}

/** For positioning the root name text, situated at the end of the topics cluster branch */
export function getRootNameOffset(
    direction: string,
    numberOfTopicMarkers: number,
    numberOfProjectTags: number): OffsetPosition {
    let offset: OffsetPosition;

    let offsetTop = {
        x: 0,
        y:
            (-2 * TILE.height) + // first two tiles (curve and straight line)
            (-numberOfTopicMarkers * TOPIC_MARKER_DIMENSIONS.height) + // topic markers
            (- TILE.height / 2) // extra space
            - 2 * TILE.height, // end marker space
    };

    let offsetBottom = {
        x: 0,

        y:
            (2 * TILE.height) + // first two tiles (curve and straight line)
            (numberOfTopicMarkers * TOPIC_MARKER_DIMENSIONS.height) // topic markers
            // no need for extra space text is bellow relative origin
            + 2 * TILE.height, // end marker space

    };

    // More space on Y axis taken by project tags then topic markers
    let projectTagsTotalHeight = numberOfProjectTags * PROJECT_TAG_DIMENSIONS_OFFSET_INCLUDED.height;
    let topicMarkersTotalHeight = numberOfTopicMarkers * TOPIC_MARKER_DIMENSIONS.height;

    if (projectTagsTotalHeight > topicMarkersTotalHeight) {
        let distanceNeeded = projectTagsTotalHeight - topicMarkersTotalHeight;
        switch (direction) {
            case 'top': {
                offsetTop.y += -distanceNeeded;
                break;
            }
            case 'bottom': {
                offsetBottom.y += distanceNeeded;
                break;
            }
        }
    }

    switch (direction) {
        case 'top': {
            offset = offsetTop;
            break;
        }
        case 'bottom': {
            offset = offsetBottom;
            break;
        }
    }
    return offset;
}

/**
 * Screen position for the start of the cluster end - semi-line(s) and endMarker
 * Default for no topic markers, dynamic based on the number of topic markers / project tags
 */
export function getClusterEndOffset(
    direction: string,
    numberOfTopicMarkers: number,
    numberOfProjectTags: number): OffsetPosition {

    let offset: OffsetPosition;

    switch (direction) {
        case 'top': {
            offset = {
                x: -2,
                y:
                    -2 * TILE.height - TILE.height / 2 + 1 +
                    (-numberOfTopicMarkers * TOPIC_MARKER_DIMENSIONS.height), // topic markers
            };
            break;
        }
        case 'bottom': {
            offset = {
                x: -2,
                y:
                    TILE.height * 1.5 - 1 +
                    (numberOfTopicMarkers * TOPIC_MARKER_DIMENSIONS.height), // topic markers
            };
            break;
        }
    }

    // More space on Y axis taken by project tags then topic markers
    let projectTagsTotalHeight = numberOfProjectTags * PROJECT_TAG_DIMENSIONS_OFFSET_INCLUDED.height;
    let topicMarkersTotalHeight = numberOfTopicMarkers * TOPIC_MARKER_DIMENSIONS.height;

    if (projectTagsTotalHeight > topicMarkersTotalHeight) {
        let distanceNeeded = projectTagsTotalHeight - topicMarkersTotalHeight;

        switch (direction) {
            case 'top': {
                offset = {
                    x: -2,
                    y:
                        -2 * TILE.height - TILE.height / 2 + 1 - topicMarkersTotalHeight - distanceNeeded,
                };
                break;
            }
            case 'bottom': {
                offset = {
                    x: -2,
                    y:
                        TILE.height * 1.5 - 1 + topicMarkersTotalHeight + distanceNeeded,
                };
                break;
            }
        }
    }

    return offset;
}

/**
 * Screen position for the endMarker relative to cluster end
 * endMarker and semi-lines are wrapped in a group with cluster end offset
 */
export function getEndMarkerOffset(direction: string): OffsetPosition {
    let offset: OffsetPosition;

    switch (direction) {
        case 'top': {
            offset = {
                x: -5,
                y: -TILE.height / 2 - 3,
            };
            break;
        }
        case 'bottom': {
            offset = {
                x: -5,
                y: TILE.height - 1,
            };
            break;
        }
    }

    return offset;
}