import { TILE } from '../../graphics-engine/constants/map-world.const';
import { mapIsometricPositionToScreenPosition } from '../../graphics-engine/services/isometric.utils';
import { GraphPosition } from '../../graphs/interfaces/graphs';
import { genMongoUuid } from '../../shared/services/utils.service';
import { colors } from '../../shared/style/colors';
import { TOPIC_MARKER_DIMENSIONS } from '../assets/topics-cluster.assets';
import { ENTITY_TYPE } from '../constants/entities-classes.const';
import { DEFAULT_LENGTH_CATEGORY_BRANCH, TOPICS_CLUSTERS_DISTANCE } from '../constants/global-map.const';
import { MapEntity, MapScene } from '../interfaces/entity';
import { IsoPosition, LearningMapRawCategoryBranch } from '../interfaces/maps';
import {
    IProjectTag,
    ITopicMarker,
    ProjectTagRaw,
    TopicsClusterRaw,
    TopicMarkerRaw,
} from '../interfaces/topics-cluster-body';

/**
 * Transforms @param globalMapRaw into Global Map entities
 * From raw date it draws(creates entities)
 * - MainBranch
 * - Topics Cluster
 * - Topics Markers, Names
 * - Root Topic
 * - Project Tags
 */
export function drawCategoryBranchScene(
    categoryBranch: LearningMapRawCategoryBranch, startPosition: IsoPosition, connectorBranchLength: number,
): MapScene {

    // Find if there are no topics clusters
    // let categoryBranchHasTopicsClusters: boolean = !!categoryBranch.clusters.length;

    // Starting point of the scene, in entities we push each generated entity

    // TODO project tag entity
    let scene: MapScene = {
        layers: [
            {
                id: 'test-layer',
                name: 'Test layer',
                entities: [
                    /** Main Branch */
                    {
                        id: categoryBranch.levelId, // genMongoUuid(),
                        name: 'Main Branch Ox for ' + categoryBranch.categoryLabel,
                        components: {
                            body: {
                                type: ENTITY_TYPE.MainBranch,
                                direction: 'x',

                                // 1 cluster up, 1 down * number of TILES
                                // (TOPICS_CLUSTERS_DISTANCE for now) for each group of 2 clusters,
                                length: Math.max(DEFAULT_LENGTH_CATEGORY_BRANCH,
                                    Math.ceil(categoryBranch.clusters.length / 2)
                                    * TOPICS_CLUSTERS_DISTANCE), // -3 (-step(is TOPICS_CLUSTERS_DISTANCE) / 2)

                                thickness: 'S',
                                fill: colors.$mapMainBranch,
                                corner: 'bottom',

                                /** Category label */
                                categoryLabel: categoryBranch.categoryLabel,
                            },
                            position: startPosition,
                        },
                    },
                    /** Connector branch with the next (if any) category branch */
                    {
                        id: genMongoUuid(), // 'main-branch-x', // TODO dynamic id and name
                        name: 'Connector Branch Oy',
                        components: {
                            body: {
                                type: ENTITY_TYPE.MainBranch,
                                direction: 'y',

                                // If no topics clusters
                                length: -connectorBranchLength,

                                thickness: 'S',
                                fill: colors.$mapMainBranch,
                                corner: 'bottom',
                            },
                            position: startPosition,
                        },
                    },
                ],
                visibility: true,
            },
        ],
    };

    // Main Branches - one for each category
    // let mainBranches: MapEntity<any>[] = getMainBranches();
    // mainBranches;

    // Topics Clusters - create entities from cluster raw data

    let topicsClusters: MapEntity<any>[] = drawTopicsClusters(categoryBranch.clusters, startPosition);
    scene.layers[0].entities.push(...topicsClusters);

    return scene;
}

/**
 * Creates all topics clusters
 * Consider returning TopicsClusterBody instead of MapEntity
 * @param clusters
 */
function drawTopicsClusters(clustersRaw: TopicsClusterRaw[], onMainBranchStartPosition: IsoPosition): MapEntity<any>[] {
    // Number of TILES on the Main Branch between each cluster
    // TODO move to constants

    let clustersEntities: MapEntity<any>[] = [];

    let distanceBetweenClusters = TOPICS_CLUSTERS_DISTANCE;
    // let clusterPosition: IsoPosition = CLUSTER_START_POSITION;
    let clusterPosition: IsoPosition = onMainBranchStartPosition;

    // Add some space at the start of the Main Branch before the topics clusters
    clusterPosition = {
        ...clusterPosition,
        x: clusterPosition.x + DEFAULT_LENGTH_CATEGORY_BRANCH,
    };

    let clusterDirection: string = 'top';

    clustersRaw.forEach((clusterRaw: TopicsClusterRaw) => {
        clustersEntities.push(
            drawCluster(clusterRaw, clusterPosition, clusterDirection),
        );

        // Alternates direction for next Cluster, switches top to bottom and vice-versa
        if (clusterDirection === 'top') {
            clusterDirection = 'bottom';
            // <!> clusterPosition for BOTTOM should have y = -1 as Bezier curve starts on the main branch
            clusterPosition = {
                ...clusterPosition,
                y: clusterPosition.y - 1,
            };
        } else {
            clusterDirection = 'top';
            // <!> clusterPosition for TOP should have y = 0 as Bezier curve starts on the main branch
            clusterPosition = {
                ...clusterPosition,
                y: onMainBranchStartPosition.y,
            };

            // Compute position for next Cluster only for top cluster (after a pair of top and bottom cluster is on map)
            // clusterPosition.x += distanceBetweenClusters;
            clusterPosition = {
                ...clusterPosition,
                x: clusterPosition.x + distanceBetweenClusters,
            };
        }
    });

    return clustersEntities;
}

/**
 * Draw cluster as entity from cluster raw data
 * topics markers
 * project tags
 * @param clusterRaw
 */
function drawCluster(clusterRaw: TopicsClusterRaw, clusterStartPosition: IsoPosition, clusterDirection: string)
    : MapEntity<any> {

    let clusterEntity: MapEntity<any> = {} as MapEntity<any>;
    clusterEntity = {
        id: genMongoUuid(),
        name: `Topics Cluster ${clusterDirection} ${clusterRaw.rootTopic}`,
        description: 'TODO add description',
        components: {
            body: {
                type: ENTITY_TYPE.TopicsCluster,
                fill: colors.$mapMainBranch,
                rootTopic: clusterRaw.rootTopic,
                numOfFavVotes: clusterRaw.numOfFavVotes,
                /** Topics Markers */
                topics: getTopicMarkers(
                    clusterRaw.topics,
                    mapIsometricPositionToScreenPosition(TILE, clusterStartPosition),
                    clusterDirection),
                /** Project tags */
                projects:
                    getProjectTags(
                        clusterRaw.projects,
                        mapIsometricPositionToScreenPosition(TILE, clusterStartPosition),
                        clusterDirection,
                    ),
                direction: clusterDirection,
            },
            position: clusterStartPosition,
        },
    };

    return clusterEntity;
}

/**
 * Array of Topic Markers
 * Each topic tag contains: name, difficulty - further fields will be added here
 */
function getTopicMarkers(
    topicsRaw: TopicMarkerRaw[],
    clusterScreenStartPosition: GraphPosition,
    clusterDirection: string): ITopicMarker[] {

    let topicsTags: ITopicMarker[] = [] as ITopicMarker[];

    // Initialize with start position for cluster
    let position = clusterScreenStartPosition;

    // Add same space as Project Tag Box from the main branch
    let initialDistance = getInitialDistanceTopicMarker(clusterDirection);

    // Position of the first Topic Marker
    position.x += initialDistance.x;
    position.y += initialDistance.y;

    // let difficulties = ['Easy', 'Medium', 'Hard', 'Goal', 'Master'];

    let spaceBetweenTopicMarkers = getDistanceBetweenTopicMarkers(clusterDirection);

    // Add difficulty - TODO add to database, remove hardcoded after
    // after that topics should be sorted based on difficulty
    topicsRaw.forEach(topicRaw => {
        // let randomDifficulty = difficulties[Math.floor(Math.random() * difficulties.length)];

        topicsTags.push({
            name: topicRaw.name,
            difficulty: topicRaw.difficulty,
            topicId: topicRaw.topicId,
            pathName: topicRaw.pathName,
            description: topicRaw.description,
            numberOfFavorites: topicRaw.numberOfFavorites,
            screenPosition: {
                x: position.x,
                y: position.y,
            },
        } as ITopicMarker);

        // Only increment the position on y axis for each project tag
        position.y += spaceBetweenTopicMarkers;
    });

    /** TODO sort topicsTags based on difficulty field */
    /** Used in global map to sort topics markers by difficulty */

    return topicsTags;
}

/**
 * Array of Project Tags
 * Each topic tag contains: name, difficulty - further fields will be added here
 */
function getProjectTags(
    projectTagsRaw: ProjectTagRaw[],
    clusterScreenStartPosition: GraphPosition,
    clusterDirection: string): IProjectTag[] {

    let projectsTags: IProjectTag[] = [] as IProjectTag[];

    // Initialize with start position for cluster
    let position = clusterScreenStartPosition;

    // Add same space as Project Tag Box from the main branch
    let initialDistance = getInitialDistanceProjectTag(clusterDirection);

    // Position of the first Project Tag
    position.x += initialDistance.x;
    position.y += initialDistance.y;

    let spaceBetweenProjectTags = getDistanceBetweenProjectTags(clusterDirection);

    projectTagsRaw.forEach(projectRaw => {

        projectsTags.push({
            name: projectRaw.name,
            difficulty: projectRaw.difficulty,
            thumbnail: projectRaw.thumbnail,
            topicId: projectRaw.topicId,
            pathName: projectRaw.pathName,
            description: projectRaw.description,
            numberOfFavorites: projectRaw.numberOfFavorites,
            screenPosition: {
                x: position.x,
                y: position.y,
            },
        } as IProjectTag);

        // Only increment the position on y axis for each project tag
        position.y += spaceBetweenProjectTags;

    });

    return projectsTags;
}

/**
 * The distance between main branch and the first project tag, some space is needed for better
 * readability of the project tags
 * @param clusterDirection
 */
function getInitialDistanceProjectTag(clusterDirection: string): GraphPosition {

    // For direction === 'top' (default)
    let initialDistance = {
        x: -30 - TILE.width,
        y: -TILE.height - TILE.height / 2 - TOPIC_MARKER_DIMENSIONS.height + 5,
    };

    // For direction ==='bottom' there is more space needed as entity start point is lower from the main branch
    if (clusterDirection === 'bottom') {
        initialDistance.y = TILE.height * 1.5 + TOPIC_MARKER_DIMENSIONS.height + 5;
    }

    return initialDistance;
}

/**
 * Distance between two consecutive project tags
 * @param clusterDirection
 */
function getDistanceBetweenProjectTags(clusterDirection: string): number {

    let spaceOnY: number;
    if (clusterDirection === 'top') {
        spaceOnY = (-1) * TOPIC_MARKER_DIMENSIONS.height * 2;
    } else {
        spaceOnY = TOPIC_MARKER_DIMENSIONS.height * 2;
    }

    return spaceOnY;
}

function getInitialDistanceTopicMarker(clusterDirection: string): GraphPosition {
    let initialDistance = {
        x: TILE.width / 2 - TOPIC_MARKER_DIMENSIONS.width / 2,

        /** TODO handle bottom initial offset */
        y: -1.5 * TILE.height - 1,
        // end of the curve, straight line from the main branch
        // -TILE.height - TILE.height / 2 - markerHeight (23) to align a topic marker on the bottom TILE
    };

    if (clusterDirection === 'bottom') {
        initialDistance.y = TILE.height / 2 + TILE.height * 1.5; // one TILE and half of TILE height from cluster origin
    }

    return initialDistance;
}

/**
 * Distance between two consecutive topic markers
 * @param clusterDirection
 */
function getDistanceBetweenTopicMarkers(clusterDirection: string): number {

    let spaceOnY: number;
    if (clusterDirection === 'top') {
        spaceOnY = (-1) * TOPIC_MARKER_DIMENSIONS.height;
    } else {
        spaceOnY = TOPIC_MARKER_DIMENSIONS.height;
    }

    return spaceOnY;
}