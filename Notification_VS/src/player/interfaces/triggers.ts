/**
 * Triggers are triggered by synthetic events dispatched to the state store.
 * Triggers emit commands that can be dispatched in the state store.
 * Triggers are mostly used by the player module to render the lesson as a programmatic video.
 *
 * Triggers can originate from:
 * - Player timestamps (during lesson player mode)
 * - Scroll interactions
 * - Other custom events
 *
 * <!> Using this system allows enormous opportunity for customization without inserting code in the core.
 * Authors can control certain aspects of the application from their own tutorials without injecting any code.
 * Marketplace developers can provide plugins that accept actions from the authors.
 *
 * <!> Triggers do not have their own collection.
 * Triggers are stored on content blocks.
 * Reshuffling content blocks also reshuffles triggers order.
 */
export interface Trigger {
    name: string;
    pathName: string;
    description: string;

    /** Events are generated from certain app interactions. */
    event: TriggerAction;

    /** Actions can be dispatched as a consequence of triggers being activated. */
    action: TriggerAction[];
}

/**
 * Thanks to the state store already being implemented we can allow authors to launch commands in the app.
 */
export interface TriggerAction {
    type: string;
    payload: any;
}