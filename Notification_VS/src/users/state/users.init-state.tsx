import { UsersState } from '../interfaces/users.state';

export const usersInitialState: UsersState = {
    user: null,
    accountSettings: {
        passwordUpdated: null,
        userInfoUpdated: null,
        userDeleted: null,
        userDetails: null,
    },
    searchUsers: null,
    editUser: null,
    modals: {
        deleteUserConfirm: false,
    },
    admin: {
        users: null,
    }
};