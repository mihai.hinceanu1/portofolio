import * as actions from './users.actions';
import { usersInitialState } from './users.init-state';
import { User } from '../interfaces/user';
import { UsersState } from '../interfaces/users.state';

export const usersReducer = (state: UsersState = usersInitialState, action: any): UsersState => {

    switch (action.type) {

        case actions.GET_USER_BY_USERNAME_OK: {
            return {
                ...state,
                user: action.payload as User,
            };
        }

        case actions.GET_USER_OK: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    userDetails: action.payload,
                    passwordUpdated: null,
                    userInfoUpdated: null,
                    userDeleted: null,
                },
            };
        }

        case actions.GET_ALL_USERS_OK: {
            return {
                ...state,
                admin: {
                    ...state.admin,
                    users: action.payload as User[]
                },
            };
        }

        case actions.EDIT_USER_OK: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    userInfoUpdated: true,
                },
            };
        }

        case actions.EDIT_USER_FAIL: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    userInfoUpdated: false,
                },
            };
        }

        case actions.UPDATE_PASSWORD_OK: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    passwordUpdated: true,
                }
            };
        }

        case actions.UPDATE_PASSWORD_FAIL: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    passwordUpdated: false,
                }
            };
        }

        case actions.SEARCH_USERS_OK: {
            return {
                ...state,
                searchUsers: action.payload as User[],
            };
        }

        case actions.SET_DELETE_USER_CONFIRM_MODAL: {
            return {
                ...state,
                modals: {
                    deleteUserConfirm: action.payload as boolean,
                },
            };
        }

        case actions.DELETE_USER_OK: {
            return {
                ...state,
                modals: {
                    ...state.modals,
                    deleteUserConfirm: false,
                },
                accountSettings: {
                    ...state.accountSettings,
                    userDeleted: true,
                }
            };
        }

        case actions.DELETE_USER_FAIL: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    userDeleted: false,
                }
            };
        }

        case actions.RESET_USER_INFO_RESPONSE: {
            return {
                ...state,
                accountSettings: {
                    ...state.accountSettings,
                    passwordUpdated: null,
                    userInfoUpdated: null,
                    userDeleted: null,
                }
            };
        }

        default:
            return state;
    }

};
