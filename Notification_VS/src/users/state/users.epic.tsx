import * as usersActions from './users.actions';
import * as authActions from '../../auth/state/auth.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { Action, Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { UpdatePasswordConfig } from '../interfaces/account-settings';
import { User } from '../interfaces/user';
import * as usersService from '../services/users.service';
import * as usersWebapi from '../webapis/users.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getUser$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.GET_USER_REQ).pipe(
        map(action => action.payload),
        concatMap(() => usersWebapi.getUser().pipe(
            map(user => usersActions.getUserOk(user)),
            catchError(error => of(usersActions.getUserFail(error))),
        )),
    );

export const getUserByUsername$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.GET_USER_BY_USERNAME_REQ).pipe(
        map(action => action.payload),
        concatMap(username => usersWebapi.getUserByUsername(username).pipe(
            map(user => usersActions.getUserByUsernameOk(user)),
            catchError(error => of(usersActions.getUserByUsernameFail(error))),
        )),
    );

export const editUser$ = (action$: ActionsObservable<Action<User>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.EDIT_USER_REQ).pipe(
        map(action => action.payload),
        concatMap(user => usersWebapi.editUser(user).pipe(
            map(() => {
                // Commented for now because it throws error.
                // TODO fix this (page is undefined)
                // usersService.getAllUsers();
                return usersActions.editUserOk();
            }),
            catchError(error => of(usersActions.editUserFail(error))),
        )),
    );

export const updatePassword$ = (action$: ActionsObservable<Action<UpdatePasswordConfig>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.UPDATE_PASSWORD_REQ).pipe(
        map(action => action.payload),
        concatMap(passwordConfig => usersWebapi.updatePassword(passwordConfig).pipe(
            map(() => usersActions.updatePasswordOk()),
            catchError(error => of(usersActions.updatePasswordFail(error))),
        )),
    );

export const deleteUser$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.DELETE_USER).pipe(
        map(action => action.payload),
        concatMap((password) => usersWebapi.deleteUser(password).pipe(
            map(() => {
                // TODO fix Cannot read property 'page' of undefined error
                // usersService.getAllUsers();
                return usersActions.deleteUserOk();
            }),
            catchError(error => of(usersActions.deleteUserFail(error.response.data))),
        )),
    );

/**
 * <!> PAY ATTENTION WHEN REVIEWING <!> Calling the signOut service before
 * returning the deleteOk action caused a redirect to login while the modal
 * was still on top, therefore I created this action observable which will
 * dispatch the signOut action when the deleteOk action is triggered.
 */
export const singOut$ = (action$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.DELETE_USER_OK).pipe(
        map(() => authActions.signOut()),
    );

export const createUser$ = (action$: ActionsObservable<Action<User>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.CREATE_USER_REQ).pipe(
        map(action => action.payload),
        concatMap(user => usersWebapi.createUser(user).pipe(
            map(() => {
                usersService.getAllUsers();
                return usersActions.createUserOk();
            }),
            catchError(error => of(usersActions.createUserFail(error))),
        )),
    );

export const getAllUsers$ = (action$: ActionsObservable<Action<Paginated>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.GET_ALL_USERS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => usersWebapi.getAllUsers(request).pipe(
            map(users => usersActions.getAllUsersOk(users)),
            catchError(error => of(usersActions.getAllUsersFail(error))),
        )),
    );

export const searchUsers = (action$: ActionsObservable<Action<PaginatedSearchReq>>, _store: Store<AppState>) =>
    action$.ofType(usersActions.SEARCH_USERS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => usersWebapi.searchUsersByKeyword(request).pipe(
            map(users => usersActions.searchUsersOk(users)),
            catchError(error => of(usersActions.searchUsersFail(error))),
        )),
    );