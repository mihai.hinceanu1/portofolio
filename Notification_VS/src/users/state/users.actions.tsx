import { Action, Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { UpdatePasswordConfig, UserInfo } from '../interfaces/account-settings';
import { User } from '../interfaces/user';

// ====== GET USER ======

export const GET_USER_BY_USERNAME_REQ = 'GET_USER_BY_USERNAME_REQ';
export const getUserByUsernameReq = (username: string): Action<string> => ({
    type: GET_USER_BY_USERNAME_REQ,
    payload: username,
});

export const GET_USER_BY_USERNAME_OK = 'GET_USER_BY_USERNAME_OK';
export const getUserByUsernameOk = (user: User): Action<User> => ({
    type: GET_USER_BY_USERNAME_OK,
    payload: user,
});

export const GET_USER_BY_USERNAME_FAIL = 'GET_USER_BY_USERNAME_FAIL';
export const getUserByUsernameFail = (error: Error): Action<Error> => ({
    type: GET_USER_BY_USERNAME_FAIL,
    payload: error,
});

export const GET_USER_REQ = 'GET_USER_REQ';
export const getUserReq = (): Action<null> => ({
    type: GET_USER_REQ,
});

export const GET_USER_OK = 'GET_USER_OK';
export const getUserOk = (user: User): Action<User> => ({
    type: GET_USER_OK,
    payload: user,
});

export const GET_USER_FAIL = 'GET_USER_FAIL';
export const getUserFail = (error: Error): Action<Error> => ({
    type: GET_USER_FAIL,
    payload: error,
});

// ====== EDIT USER ======

export const EDIT_USER_REQ = 'EDIT_USER_REQ';
export const editUserReq = (user: UserInfo): Action<UserInfo> => ({
    type: EDIT_USER_REQ,
    payload: user,
});

export const EDIT_USER_OK = 'EDIT_USER_OK';
export const editUserOk = (): Action<null> => ({
    type: EDIT_USER_OK,
});

export const EDIT_USER_FAIL = 'EDIT_USER_FAIL';
export const editUserFail = (error: Error): Action<Error> => ({
    type: EDIT_USER_FAIL,
    payload: error,
});

// ====== UPDATE PASSWORD ======

export const UPDATE_PASSWORD_REQ = 'UPDATE_PASSWORD_REQ';
export const updatePasswordReq = (passwordConfig: UpdatePasswordConfig): Action<UpdatePasswordConfig> => ({
    type: UPDATE_PASSWORD_REQ,
    payload: passwordConfig,
});

export const UPDATE_PASSWORD_OK = 'UPDATE_PASSWORD_OK';
export const updatePasswordOk = (): Action<null> => ({
    type: UPDATE_PASSWORD_OK,
});

export const UPDATE_PASSWORD_FAIL = 'UPDATE_PASSWORD_FAIL';
export const updatePasswordFail = (error: Error): Action<Error> => ({
    type: UPDATE_PASSWORD_FAIL,
    payload: error,
});

// ====== DELETE USER ======

export const DELETE_USER = 'DELETE_USER';
/** Needs password to confirm user identity */
export const deleteUser = (password: string): Action<string> => ({
    type: DELETE_USER,
    payload: password,
});

export const DELETE_USER_OK = 'DELETE_USER_OK';
export const deleteUserOk = (): Action<null> => ({
    type: DELETE_USER_OK,
});

export const DELETE_USER_FAIL = 'DELETE_USER_FAIL';
export const deleteUserFail = (error: Error): Action<Error> => ({
    type: DELETE_USER_FAIL,
    payload: error,
});

// ====== CREATE USER ======

export const CREATE_USER_REQ = 'CREATE_USER_REQ';
export const createUserReq = (user: User): Action<User> => ({
    type: CREATE_USER_REQ,
    payload: user,
});

export const CREATE_USER_OK = 'CREATE_USER_OK';
export const createUserOk = (): Action<null> => ({
    type: CREATE_USER_OK,
});

export const CREATE_USER_FAIL = 'CREATE_USER_FAIL';
export const createUserFail = (error: Error): Action<Error> => ({
    type: CREATE_USER_FAIL,
    payload: error,
});

// ====== GET ALL USERS ======

export const GET_ALL_USERS_REQ = 'GET_ALL_USERS_REQ';
export const getAllUsersReq = (page: Paginated): Action<Paginated> => ({
    type: GET_ALL_USERS_REQ,
    payload: page,
});

export const GET_ALL_USERS_OK = 'GET_ALL_USERS_OK';
export const getAllUsersOk = (users: User[]): Action<User[]> => ({
    type: GET_ALL_USERS_OK,
    payload: users,
});

export const GET_ALL_USERS_FAIL = 'GET_ALL_USERS_FAIL';
export const getAllUsersFail = (error: Error): Action<Error> => ({
    type: GET_ALL_USERS_FAIL,
    payload: error,
});

// ====== SEARCH USERS ======

export const SEARCH_USERS_REQ = 'SEARCH_USERS_REQ';
export const searchUsersReq = (request: PaginatedSearchReq): Action<PaginatedSearchReq> => ({
    type: SEARCH_USERS_REQ,
    payload: request,
});

export const SEARCH_USERS_OK = 'SEARCH_USERS_OK';
export const searchUsersOk = (users: User[]): Action<User[]> => ({
    type: SEARCH_USERS_OK,
    payload: users,
});

export const SEARCH_USERS_FAIL = 'SEARCH_USERS_FAIL';
export const searchUsersFail = (error: Error): Action<Error> => ({
    type: SEARCH_USERS_FAIL,
    payload: error,
});

// ====== DELETE USER CONFIRM MODAL ======

export const SET_DELETE_USER_CONFIRM_MODAL = 'SET_DELETE_USER_CONFIRM_MODAL';
export const setDeleteUserConfirmModal = (isVisible: boolean): Action<boolean> => ({
    type: SET_DELETE_USER_CONFIRM_MODAL,
    payload: isVisible,
});

// ====== ACCOUNT SETTINGS ======

export const RESET_USER_INFO_RESPONSE = 'RESET_USER_INFO_RESPONSE';
export const resetUserInfoResponse = (): Action<null> => ({
    type: RESET_USER_INFO_RESPONSE,
});