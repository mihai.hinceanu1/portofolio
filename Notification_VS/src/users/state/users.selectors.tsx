import { AppState } from '../../shared/interfaces/app.state';
import { UserInfo } from '../interfaces/account-settings';
import { User } from '../interfaces/user';
import { UsersState } from '../interfaces/users.state';
import { createSelector, Selector } from 'reselect';

const USERS_MODULE: Selector<AppState, UsersState> = (state: AppState) => state.users;

export const USER = createSelector<AppState, UsersState, User>(
    USERS_MODULE,
    state => state.user,
);

export const USER_DETAILS = createSelector<AppState, UsersState, UserInfo>(
    USERS_MODULE,
    state => state.accountSettings.userDetails,
);

/** Selector for response from server for password update. */
export const UPDATE_PASSWORD_RESULT = createSelector<AppState, UsersState, boolean>(
    USERS_MODULE,
    state => state.accountSettings.passwordUpdated,
);

/** Selector for server response when user attempts to update his info */
export const UPDATE_USER_INFO_RESULT = createSelector<AppState, UsersState, boolean>(
    USERS_MODULE,
    state => state.accountSettings.userInfoUpdated,
);

export const EDIT_USER = createSelector<AppState, UsersState, User>(
    USERS_MODULE,
    state => state.editUser,
);

/** List all users in admin */
export const USERS = createSelector<AppState, UsersState, User[]>(
    USERS_MODULE,
    state => state.admin.users,
);

export const SEARCH_USERS = createSelector<AppState, UsersState, User[]>(
    USERS_MODULE,
    state => state.searchUsers,
);

export const DELETE_USER_CONFIRM_MODAL = createSelector<AppState, UsersState, boolean>(
    USERS_MODULE,
    state => state.modals.deleteUserConfirm,
);

/** Selector to server response for delete user */
export const DELETE_USER_RESPONSE = createSelector<AppState, UsersState, boolean>(
    USERS_MODULE,
    state => state.accountSettings.userDeleted,
);
