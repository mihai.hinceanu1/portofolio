import { Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import axios from '../../shared/services/axios.service';
import { UpdatePasswordConfig } from '../interfaces/account-settings';
import { User } from '../interfaces/user';
import { from, Observable } from 'rxjs';

export const getCurrentUser = (): Observable<User> =>
    from<Promise<User>>(
        axios.get<User>(`/users/user`)
            .then(res => res.data),
    );

export const getUser = (): Observable<User> =>
    from<Promise<User>>(
        axios.get<User>(`/users/user`)
            .then(res => res.data),
    );

export const getUserByUsername = (username: string): Observable<User> =>
    from<Promise<User>>(
        axios.get<User>(`/users/users/username/${username}`)
            .then(res => res.data),
    );

export const editUser = (user: User): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`/users/user`, user)
            .then(res => res.data),
    );

export const updatePassword = (passwordConfig: UpdatePasswordConfig): Observable<null> =>
    from<Promise<null>>(
        axios.post<null>(`/users/update-password`, passwordConfig)
            .then(res => res.data),
    );

export const deleteUser = (password: string): Observable<null> =>
    from<Promise<null>>(
        /** Send the password through headers as delete can't have a body. */
        axios.delete(`/users/user`, { headers: { IDENTITY: password } })
            .then(res => res.data),
    );

export const createUser = (user: User): Observable<null> =>
    from<Promise<null>>(
        axios.post<null>(`/users/users`, user)
            .then(res => res.data),
    );

export const getAllUsers = ({ page = 0, perPage = 20 }: Paginated): Observable<User[]> =>
    from<Promise<User[]>>(
        axios.get<User[]>(`/users/users/page/${page}/${perPage}`)
            .then(res => res.data),
    );

export const searchUsersByKeyword = (request: PaginatedSearchReq): Observable<User[]> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<User[]>>(
        axios.get<User[]>(`/users/users/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};