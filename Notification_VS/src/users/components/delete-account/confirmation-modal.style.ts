import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const ConfirmationModal = styled.View`
    width: 55%;
    min-width: 320px;
    max-width: 750px;
    min-height: 200px;
    background-color: ${colors.$white};
    border-radius: 2px;
    padding: 25px 35px 15px 35px;
`;

export const Header = styled.Text`
    font-weight: 600;
    margin-bottom: 20px;
`;

export const ConfirmationMessage = styled.Text`
    margin-bottom: 30px;
`;

export const Actions = styled.View`
    width: 250px;
    margin-top: 20px;
    margin-bottom: 5px;
    flex-direction: row;
    align-self: flex-end;
    justify-content: space-between;
`;
