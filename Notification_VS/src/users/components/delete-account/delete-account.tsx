import { ConfirmationModal } from './confirmation-modal';
import * as div from './delete-account.style';
import { Button } from '../../../shared/components/button/button';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { colors } from '../../../shared/style/colors';
import { deleteUserConfirmModal$, setDeleteUserConfirmModal } from '../../services/users.service';
import { DELETE } from '../account-settings-menu/_assets/account-settings-menu.assets';
import { ToggleForm } from '../toggle-form/toggle-form';
import React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    isNarrow: boolean;
}

interface State {
    isToggled: boolean;
    showModal: boolean;
}

/**
 * A user can remove his account.
 * Clicking the Delete Account button will render a modal where
 * user interaction is needed in order to confirm his action.
 */
export class DeleteAccount extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();
    private modal: JSX.Element;

    constructor(props: Props) {
        super(props);

        this.state = {
            isToggled: false,
            showModal: false,
        };
    }

    public render() {
        const { isNarrow } = this.props;
        const { isToggled } = this.state;
        let shouldDisplayForm = isToggled || !isNarrow;

        return (
            <div.DeleteAccount data-cy='delete-account' isNarrow={isNarrow}>
                {
                    isNarrow &&
                    <ToggleForm isToggled={isToggled}
                        icon={DELETE}
                        label='Delete Account'
                        onToggle={() => this.toggleForm()} />
                }
                {
                    shouldDisplayForm &&
                    <>
                        {/** Warning */}
                        <div.Warning data-cy='delete-warning'>
                            You may delete your account at any time.
                            However, this action is irreversible.
                        </div.Warning>

                        {/** Delete button */}
                        <Button config={{
                            text: 'Delete Account',
                            color: colors.$red,
                            width: 146,
                            onPress: () => this.toggleModal(),
                        }}
                            overrides={{ root: 'margin-top: 20px;' }} />
                    </>
                }
            </div.DeleteAccount>
        );
    }

    public componentDidMount() {
        this.subscribeToDeleteUserConfirmModal();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToDeleteUserConfirmModal() {
        deleteUserConfirmModal$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isVisible => {
                if (isVisible) {
                    this.showModal();
                } else {
                    this.hideModal();
                }
            });
    }

    private toggleForm() {
        const { isToggled } = this.state;
        this.setState({
            isToggled: !isToggled,
        });
    }

    private toggleModal() {
        const { showModal } = this.state;

        setDeleteUserConfirmModal(!showModal);

        this.setState({
            showModal: !showModal,
        });
    }

    private showModal() {
        this.modal = <ConfirmationModal callback={() => this.toggleModal()} />;
        addAppOverlay(this.modal);
    }

    private hideModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }
}
