import * as div from './confirmation-modal.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { Modal } from '../../../shared/components/modal/modal';
import { colors } from '../../../shared/style/colors';
import { deleteUser, deleteUserResponse$ } from '../../services/users.service';
import React from 'react';
import { Text } from 'react-native';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    callback: () => void;
}
interface State {
    /** Input value */
    password: string;

    /** Flag for empty password, used to notify the user */
    passwordEmptyError: boolean;

    /**
     * The response from server for the delete request.
     *
     * If the request was successful the user will just be redirected to
     * login, but if it failed he will get a message.
     */
    deleteUserResponse: boolean;
}

/**
 * The modal that opens when the user wants to delete his account,
 * in order to confirm his action.
 *
 * This way we make sure that the button wasn't clicked by accident.
 * Also we require the user password to check his identity.
 */
export class ConfirmationModal extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            password: '',
            passwordEmptyError: null,
            deleteUserResponse: null,
        };
    }
    public render() {
        const { callback } = this.props;
        const { password } = this.state;

        return (
            <Modal callback={callback}>
                <div.ConfirmationModal data-cy='confirmation-modal'>
                    {/* Header */}
                    <div.Header>Action required</div.Header>

                    {/* Message */}
                    <div.ConfirmationMessage>Deleting your account is irreversible, we need to check you are the owner of this account.</div.ConfirmationMessage>

                    {/* Password input for confirmation */}
                    <Input label='PASSWORD'
                        value={password}
                        type='password'
                        onFocus={() => this.onInputFocus()}
                        onChangeText={(text) => this.onChangePassword(text)}
                        overrides={{ root: 'margin: 0px;' }} />

                    {/* Show error */}
                    {this.showError()}

                    <div.Actions>
                        {/* Cancel */}
                        <Button config={{
                            text: 'Cancel',
                            onPress: () => this.handleCancelClick(),
                        }}
                            overrides={{ root: 'margin: 0px' }} />

                        {/* Delete account */}
                        <Button config={{
                            text: 'Delete Account',
                            onPress: () => this.deleteAccountClick(),
                            color: colors.$red,
                        }}
                            overrides={{ root: 'margin: 0px' }} />

                    </div.Actions>
                </div.ConfirmationModal>
            </Modal>
        );
    }

    public componentDidMount() {
        this.subscribeToUserDeletedResponse();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToUserDeletedResponse() {
        deleteUserResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(deleteUserResponse => {
                this.setState({
                    passwordEmptyError: null,
                    deleteUserResponse,
                });
            });
    }

    private onChangePassword(password: string) {
        this.setState({
            password: password,
        });
    }

    /** Reset the error flags. */
    private onInputFocus() {
        this.setState({
            deleteUserResponse: null,
            passwordEmptyError: null,
        });
    }

    /** Close the modal by pressing cancel. */
    private handleCancelClick() {
        const { callback } = this.props;
        if (callback) {
            callback();
        }
    }

    /** Submit method */
    private deleteAccountClick() {
        const { password } = this.state;

        if (!password) {
            this.setState({
                passwordEmptyError: true,
            });
        } else {
            deleteUser(password);

            this.setState({
                passwordEmptyError: null,
            });
        }
    }

    /** Show a message when user does something wrong */
    private showError(): JSX.Element {
        const { passwordEmptyError, deleteUserResponse } = this.state;

        if (passwordEmptyError) {
            return <Text data-cy='password-empty-error'>Please type in your password to check your identity.</Text>;
        }

        if (deleteUserResponse === false) {
            return <Text data-cy='delete-failed-error'>Operation failed, make sure that you entered the password correctly.</Text>;
        }
    }
}