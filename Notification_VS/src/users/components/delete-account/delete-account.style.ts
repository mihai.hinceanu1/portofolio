import { font } from '../../../shared/style/font-sizes';
import { MIN_FORM_WIDTH } from '../forms/_constants/forms.const';
import { getMarginTop, getPaddings } from '../forms/_utils/forms.utils';
import styled from 'styled-components/native';

interface DeleteAccountProps {
    isNarrow: boolean;
}

export const DeleteAccount = styled.View<DeleteAccountProps>`
    background: white;
    border-radius: 1px;
    margin-bottom: 30px;
    min-width: ${MIN_FORM_WIDTH}px;
    padding: ${props => getPaddings(props.isNarrow)};
    margin-top: ${props => getMarginTop(props.isNarrow)}px;
`;

export const Warning = styled.Text`
    margin-left: 10px;
    font-size: ${font.$size16}px;
`;