import { colors } from '../../../shared/style/colors';
import { PEN_SVG } from '../account-settings-menu/_assets/account-settings-menu.assets';
import { PHOTO_DIM } from '../account-settings-menu/_constants/account-settings-menu.const';
import { MIN_FORM_WIDTH } from '../forms/_constants/forms.const';
import React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface AvatarProps {
    isNarrow: boolean;
}

export const Avatar = styled.View<AvatarProps>`
    width: 100%;
    background-color: ${colors.$white};
    ${props => getMargin(props)}
    ${props => getMinWidth(props)}
    height: ${props => getAvatarHeight(props)}px;
`;

export const PhotoFrame = styled.View<AvatarProps>`
    width: ${PHOTO_DIM}px;
    height: ${PHOTO_DIM}px;
    align-self: center;
    ${props => movePhotoFrameUp(props)}
`;

export const Photo = styled.View`
    border-radius: ${PHOTO_DIM / 2}px;
    width: ${PHOTO_DIM}px;
    height: ${PHOTO_DIM}px;
`;

export const Edit = styled.TouchableHighlight`
    background-color: ${colors.$blue};
    width: 30px;
    height: 30px;
    border-radius: 15px;;
    align-items: center;
    justify-content: center;
    align-self: flex-end;
    position: absolute;
    bottom: 1px;
    right: 3px;
`;

// ====== SVG ======

export const editProfileSvg = () =>
    <Svg width={14} height={14}>
        <Path d={PEN_SVG} fill='white' />
    </Svg>;

// ====== UTILS ======

let getMinWidth = (props: AvatarProps): string =>
    props.isNarrow ? `min-width: ${MIN_FORM_WIDTH}px;` : '';

let getAvatarHeight = (props: AvatarProps): number =>
    props.isNarrow ? 3 * PHOTO_DIM / 4 : PHOTO_DIM;

let getMargin = (props: AvatarProps): string =>
    props.isNarrow ? '' :
        `
            margin-top: 35px;
            margin-bottom: 30px;
        `;

let movePhotoFrameUp = (props: AvatarProps): string =>
    props.isNarrow ? `bottom: ${PHOTO_DIM / 2}px;` : '';