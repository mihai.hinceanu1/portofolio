import * as div from './avatar.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import React from 'react';

interface Props {
    isNarrow: boolean;
}

/**
 * A presentational component for now.
 * Shows the photo the user selected for his avatar.
 *
 * TODO The user can change his photo anytime by clicking the
 *   edit button in the bottom right of the photo
 *
 * TODO When no photo is selected a default photo is shown.
 */
export const Avatar: React.FunctionComponent<Props> = (props: Props) => {
    const { isNarrow } = props;

    return (
        <div.Avatar data-cy='avatar' isNarrow={isNarrow}>

            <div.PhotoFrame data-cy='photo-frame' isNarrow={isNarrow}>

                {/* Photo */}
                <div.Photo data-cy='photo'>
                    <FitImage imgPath='/thumbnails/batman.png' round={true} />
                </div.Photo>

                {/* Overlapping edit button */}
                <div.Edit data-cy='edit'>
                    {div.editProfileSvg()}
                </div.Edit>

            </div.PhotoFrame>
        </div.Avatar>
    );
};