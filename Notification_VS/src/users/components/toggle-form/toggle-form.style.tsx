import { CARET_DOWN } from '../../../shared/assets/icons';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface ToggleFormProps {
    isToggled: boolean;
}

// ====== STYLES ======

export const ToggleForm = styled.TouchableHighlight`
    padding: 10px;
`;

export const CenterOnRow = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const LabeledIcon = styled.View`
    flex-direction: row;
    align-items: center;
`;

export const Label = styled.Text<ToggleFormProps>`
    margin-left: 20px;
    font-weight: bold;
    align-self: center;
    color: ${props => getColor(props)};
`;

// ====== SVG ======

export const drawIcon = (icon: string, props: ToggleFormProps): JSX.Element => {
    const { width, height } = pathBoundingBox(icon);
    return (
        <Svg width={width + 1} height={height + 1} style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Path d={icon} fill={getColor(props)} />
        </Svg>
    );
};

export const getPointingArrow = (props: ToggleFormProps): JSX.Element => {
    const { width, height } = pathBoundingBox(CARET_DOWN);
    return (
        <Svg width={width} height={height}
            style={{ transform: ([{ rotate: `${arrowPointingDownOrRight(props)}deg` }]) }}>
            <Path d={CARET_DOWN} fill={getColor(props)} />
        </Svg>
    );
};

// ====== UTILS ======

let getColor = (props: ToggleFormProps) =>
    props.isToggled ? colors.$blue : colors.$black;

let arrowPointingDownOrRight = (props: ToggleFormProps) =>
    props.isToggled ? 0 : -90;