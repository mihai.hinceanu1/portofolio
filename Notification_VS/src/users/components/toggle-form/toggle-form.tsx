import * as div from './toggle-form.style';
import React from 'react';

interface Props {
    icon: string;
    label: string;
    isToggled: boolean;
    onToggle: () => void;
}

/**
 * An accordion style toggle menu.
 */
export const ToggleForm: React.FunctionComponent<Props> = (props: Props) => {

    const { label, icon, isToggled } = props;

    return (
        <div.ToggleForm data-cy='toggle-form'
            underlayColor='none'
            onPress={() => onPress()}>
            <div.CenterOnRow>
                <div.LabeledIcon data-cy='labeled-icon'>
                    {/* Icon */}
                    {div.drawIcon(icon, { isToggled })}

                    {/* Label */}
                    <div.Label data-cy='label'
                        isToggled={isToggled}>
                        {label}
                    </div.Label>
                </div.LabeledIcon>

                {/* Arrow */}
                {div.getPointingArrow({ isToggled })}
            </div.CenterOnRow>
        </div.ToggleForm>
    );

    function onPress() {
        const { onToggle } = props;

        if (onToggle) {
            onToggle();
        }
    }
};