import { LEFT_RIGHT_PADDING, TOP_BOTTOM_PADDING } from '../_constants/forms.const';
import { BasicInformationState } from '../basic-information';
import { UpdatePasswordState } from '../update-password';

/**
 * For mobile the forms are glued, therefore this would've been
 * repeated in every style sheet so I moved it here.
 */
export const getMarginTop = (isMobile: boolean): number =>
    isMobile ? 0 : 30;

export const getFlexBasis = (isMobile: boolean): string =>
    isMobile ? '100%' : '80px';

export const getPaddings = (isMobile: boolean): string =>
    isMobile ? `10px ${LEFT_RIGHT_PADDING}px;` : `${TOP_BOTTOM_PADDING}px ${LEFT_RIGHT_PADDING}px;`;

/**
 * Another checkpoint for tabled adaptive design.
 */
export const isWideScreen = (width: number): boolean =>
    width >= 905;

/**
 * Flag that makes the inputs in the update password forms
 * take up the available space when screens are narrow.
 */
export const shouldExtendForm = (width: number): boolean =>
    !isWideScreen(width);

// ====== BASIC INFORMATION ======

export const initBasicInfoState = (): BasicInformationState => {
    return {
        isToggled: true,
        initialUserInfo: {
            firstName: '',
            lastName: '',
            email: '',
            username: '',
        },
        userInfo: {
            firstName: '',
            lastName: '',
            email: '',
            username: '',
        },
        // <!>Parked<!>This is parked until we get over basic functionalities
        // website: '',
        // github: '',
        // linkedin: '',
        errors: {
            firstNameError: null,
            lastNameError: null,
            emailError: null,
            usernameError: null,
        },
        userInfoUpdated: null,
        fieldsEmpty: null,
    };
};

// ====== UPDATE PASSWORD ======

export const initUpdatePasswordState = (): UpdatePasswordState => {
    return {
        isToggled: false,
        currentPassword: '',
        newPassword: '',
        confirmNewPassword: '',
        wasUpdateSuccessful: null,
        errors: {
            passwordError: null,
            newPasswordError: null,
            confirmPasswordError: null,
        },
    };
};