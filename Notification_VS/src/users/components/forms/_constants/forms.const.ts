import { IRadioOption } from '../../../../shared/interfaces/radio-button';

/** A fixed size for some inputs in the form */
export const INPUT_SIZE = 250;

/** TODO Check if this should be in a separate file
 * The border radius used in styles.
 */
export const BORDER_RADIUS = 2;

/** Padding for the forms */
export const TOP_BOTTOM_PADDING = 20;

/** Padding for the forms */
export const LEFT_RIGHT_PADDING = 30;

/** The width of a button having the Update text inside so it matches our design */
export const UPDATE_BUTTON_WIDTH = 97;

/** The space above the content in account settings page */
export const TOP_DISTANCE = 110;

/**
 * 335px - The minimal width for the forms,
 * must be set and be the same for each form.
 */
export const MIN_FORM_WIDTH = 335;

export const reminderFrequencies: IRadioOption[] = [
    {
        _id: '1',
        label: 'Daily',
    },
    {
        _id: '2',
        label: 'Weekly',
    },
    {
        _id: '3',
        label: 'Monthly'
    },
];