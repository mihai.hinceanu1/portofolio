import { reminderFrequencies, UPDATE_BUTTON_WIDTH } from './_constants/forms.const';
import * as div from './email-preferences.style';
import { Button } from '../../../shared/components/button/button';
import { Checkbox } from '../../../shared/components/checkbox/checkbox';
import { RADIO_SIDES } from '../../../shared/components/radio-button/_const/radio-button.const';
import { RadioButtons } from '../../../shared/components/radio-button/radio-buttons';
import { colors } from '../../../shared/style/colors';
import React from 'react';
import { Text } from 'react-native';

/**
 * <!> This will change in the future
 * <!> This component is parked for now until the notifications module
 * is added.
 * The user can change his settings for the notifications
 */
export function EmailPreferences() {
    return (
        <div.EmailPreferences data-cy='email-preferences'>
            {/** New content notifications */}
            <Checkbox label='New Content Announcements'
                isChecked={false}
                onPress={() => { }}
                description='Stay up to date on new courses, Paths, and product features.' />

            {/** Tips and tricks notifications */}
            <Checkbox label='Tips & Tricks'
                isChecked={false}
                onPress={() => { }}
                description='Get the most out of Visual School, including helpful info on how to get started.' />

            {/** Special promotions notifications */}
            <Checkbox label='Special Promotions'
                isChecked={false}
                onPress={() => { }}
                description={`A few times a year, we'll offer subscription discounts that you won't want to miss.`} />

            {/** Progress updates notifications */}
            <Checkbox label='Progress Updates'
                isChecked={false}
                onPress={() => { }}
                description={`Keep track of how far you've come in your courses or Paths`} />

            {/** Learning reminders */}
            <Checkbox label='Learning Reminders'
                isChecked={false}
                onPress={() => { }}
                description='Receive daily, weekly or monthly reminders to help you keep you momentum up.' />

            <Text>Frequency of reminders:</Text>

            {/** Select frequency of learning reminders */}
            <RadioButtons options={reminderFrequencies}
                selected={null}
                radioSide={RADIO_SIDES.left}
                onPress={() => { }} />

            {
                /**
                 * <!> This requires more work.
                 * Check design. A slider is needed.
                 */
            }
            <Text>Unsubscribe from everything</Text>
            <Text>We'll continue to send necessary emails about your account.</Text>

            {/** Submit Button */}
            <Button config={{
                text: 'Update',
                color: colors.$blue,
                width: UPDATE_BUTTON_WIDTH,
                fontSize: 15,
                onPress: () => { },
            }} />
        </div.EmailPreferences>
    );
}