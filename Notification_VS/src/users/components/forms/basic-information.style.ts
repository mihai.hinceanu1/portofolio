import { BORDER_RADIUS, MIN_FORM_WIDTH } from './_constants/forms.const';
import { getPaddings } from './_utils/forms.utils';
import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

// ====== PROPS ======

interface BasicInformationProps {
    isMobile: boolean;
}

interface FormProps {
    isNarrow: boolean;
}

// ====== STYLE ======

export const BasicInformation = styled.View<BasicInformationProps>`
    background-color: white;
    min-width: ${MIN_FORM_WIDTH}px;
    border-radius: ${BORDER_RADIUS}px;
    padding: ${props => getPaddings(props.isMobile)};
`;

export const Form = styled.View<FormProps>`
    ${props => getFormPaddings(props)}
`;

export const UserInfo = styled.View``;

export const Row = styled.View`
    flex-direction: row;
    flex-wrap: wrap;
    flex: 1;
    flex-basis: 100%;
`;

export const Warning = styled.Text`
    margin-left: 10px;
    font-weight: 500;
`;

export const Success = styled(Warning)`
    color: ${colors.$green};
`;

export const Fail = styled(Warning)`
    color: ${colors.$red};
`;

// ====== UTILS ======

let getFormPaddings = (props: FormProps): string =>
    props.isNarrow ?
        `
        padding-top: 10px;
        padding-bottom: 20px;
        ` : '';