import { UPDATE_BUTTON_WIDTH } from './_constants/forms.const';
import { getFlexBasis, initBasicInfoState } from './_utils/forms.utils';
import * as div from './basic-information.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { colors } from '../../../shared/style/colors';
import { UserInfo, UserInfoErrors } from '../../interfaces/account-settings';
import { validateUserData } from '../../services/account-settings.utils';
import {
    editUser,
    resetResponseFlags,
    updateUserInfoResult$,
    userDetails$
    } from '../../services/users.service';
import { BASIC_INFO } from '../account-settings-menu/_assets/account-settings-menu.assets';
import { ToggleForm } from '../toggle-form/toggle-form';
import React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    isNarrow: boolean;
}

export interface BasicInformationState {
    isToggled: boolean;
    initialUserInfo: UserInfo;
    userInfo: UserInfo;

    // This is parked until we get over basic functionalities
    // website: string;
    // github: string;
    // linkedin: string;
    errors: UserInfoErrors;
    userInfoUpdated: boolean;
    fieldsEmpty: boolean;
}

/**
 * First section of the user settings forms, here the user can update
 * some of his personal information like first name, last name, email
 * username.
 *
 * This also handles the minimal validations of the said information like
 * checking that some information is provided, or a correct email template
 * is provided.
 *
 * Also notifies the user if the update was successful or not.
 *
 * TODO Maybe later add some contact data or add some social profiles.
 */
export class BasicInformation extends React.Component<Props, BasicInformationState> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = initBasicInfoState();
    }

    public render() {
        const { isNarrow } = this.props;
        const { isToggled, userInfo, errors/*, website, github, linkedin*/ } = this.state;
        const { firstName, lastName, email, username } = userInfo;
        const { firstNameError, lastNameError, emailError, usernameError } = errors;
        let shouldRenderForms = isToggled || !isNarrow;

        return (
            <div.BasicInformation data-cy='basic-information' isMobile={isNarrow}>
                {
                    isNarrow &&
                    <ToggleForm label={'Basic Information'} icon={BASIC_INFO} isToggled={isToggled} onToggle={() => this.toggleForm()} />
                }
                {
                    shouldRenderForms &&
                    <div.Form isNarrow={isNarrow}>

                        {/** This is for the double columns layout on wide screens */}
                        <div.UserInfo data-cy='user-info'>
                            <div.Row data-cy='row'>
                                {/* First Name */}
                                <Input value={firstName}
                                    label={'First Name'.toUpperCase()}
                                    error={firstNameError}
                                    overrides={{ root: `width: auto; flex: 1; flex-basis: ${getFlexBasis(isNarrow)}; height: 80px;` }}
                                    onFocus={() => this.handleInputFocus()}
                                    onChangeText={(text: string) => this.handleUserInfoChange(text, 'firstName')} />

                                {/* Last Name */}
                                <Input value={lastName}
                                    label={'Last Name'.toUpperCase()}
                                    error={lastNameError}
                                    overrides={{ root: `width: auto; flex: 1; flex-basis: ${getFlexBasis(isNarrow)}; height: 80px;` }}
                                    onFocus={() => this.handleInputFocus()}
                                    onChangeText={(text: string) => this.handleUserInfoChange(text, 'lastName')} />
                            </div.Row>

                            <div.Row data-cy='row'>
                                {/* Email */}
                                <Input value={email}
                                    label={'Email'.toUpperCase()}
                                    error={emailError}
                                    overrides={{ root: `width: auto; flex: 1; flex-basis: ${getFlexBasis(isNarrow)}; height: 80px;` }}
                                    onFocus={() => this.handleInputFocus()}
                                    onChangeText={(text: string) => this.handleUserInfoChange(text, 'email')} />

                                {/* Username */}
                                <Input value={username}
                                    label={'Username'.toUpperCase()}
                                    error={usernameError}
                                    overrides={{ root: `width: auto; flex: 1; flex-basis: ${getFlexBasis(isNarrow)}; height: 80px;` }}
                                    onFocus={() => this.handleInputFocus()}
                                    onChangeText={(text: string) => this.handleUserInfoChange(text, 'username')} />
                            </div.Row>

                        </div.UserInfo>

                        {/* This is parked until we get over basic functionalities*/}
                        {/** Additional website page field */}
                        {/* <Input value={website}
                            label={'Website'.toUpperCase()}
                            overrides={{ root: 'width: auto; flex: 2; flex-basis: 80px;' }}
                            onFocus={() => this.handleInputFocus()}
                            onChangeText={(text: string) => this.handleUserInfoChange(text, 'website')} /> */}

                        {/** Additional github profile field */}
                        {/* <Input value={github}
                            label={'Github Username'.toUpperCase()}
                            overrides={{ root: 'width: auto; flex: 2; flex-basis: 80px;' }}
                            onFocus={() => this.handleInputFocus()}
                            onChangeText={(text: string) => this.handleUserInfoChange(text, 'github')} /> */}

                        {/** Additional linkedin profile field */}
                        {/* <Input value={linkedin}
                            label={'LinkedIn Profile'.toUpperCase()}
                            overrides={{ root: 'width: auto; flex: 2; flex-basis: 80px;' }}
                            onFocus={() => this.handleInputFocus()}
                            onChangeText={(text: string) => this.handleUserInfoChange(text, 'linkedin')} /> */}

                        {/* Update result */}
                        {this.notifyUser()}

                        {/** Submit */}
                        <Button config={{
                            text: 'Update',
                            color: colors.$blue,
                            width: UPDATE_BUTTON_WIDTH,
                            fontSize: 15,
                            onPress: () => this.submitUpdate(),
                        }}
                            overrides={{ root: 'margin-top: 5px;' }} />
                    </div.Form>
                }
            </div.BasicInformation>
        );
    }

    public componentDidMount() {
        this.subscribeToUserDetails();
        this.subscribeToUpdatedUserInfo();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToUserDetails() {
        userDetails$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(user => {
                this.setState({
                    ...this.state,
                    initialUserInfo: { ...user },
                    userInfo: { ...user },
                });
            });
    }

    private subscribeToUpdatedUserInfo() {
        updateUserInfoResult$().pipe(
            takeUntil(this.destroyed$)
        )
            .subscribe(userInfoUpdated => {
                this.setState({
                    userInfoUpdated,
                });
            });
    }

    private toggleForm() {
        const { isToggled } = this.state;
        this.setState({
            isToggled: !isToggled,
        });
    }

    private handleInputFocus() {
        const { userInfoUpdated, fieldsEmpty } = this.state;

        let shouldResetFlags = userInfoUpdated !== null || fieldsEmpty !== null;

        if (shouldResetFlags) {
            resetResponseFlags();

            this.setState({
                userInfoUpdated: null,
                fieldsEmpty: null,
            });
        }
    }

    /**
     * Notify user about whether or not the update was successful.
     */
    private notifyUser(): JSX.Element {
        const { userInfoUpdated } = this.state;

        if (userInfoUpdated) {
            return <div.Success data-cy='info-update-success'>Update successful</div.Success>;
        } else {
            /**
             * We must check for false because null is also "falsy" and we
             * don't want to confuse the user by saying that something
             * failed when nothing actually happened.
             */
            if (userInfoUpdated === false) {
                return <div.Fail data-cy='info-update-fail'>Update failed</div.Fail>;
            }
        }
    }

    private handleUserInfoChange(text: string, field: string) {
        this.setState({
            ...this.state,
            userInfo: {
                ...this.state.userInfo,
                [field]: text,
            }
        });
    }

    private submitUpdate() {
        const { userInfo: updatedUserInfo } = this.state;
        const { firstName, lastName, email, username } = updatedUserInfo;

        if (this.shouldSendData()) {
            let updatedUser: UserInfo = {
                firstName,
                lastName,
                username,
                email,
            };

            editUser(updatedUser);
        }
    }

    /**
     * Check if the data filled by the user meets the submit conditions:
     * 1) It is different from previous data;
     * 2) Is valid;
     * 3) All fields are filled;
     */
    private shouldSendData(): boolean {
        this.validateData();

        const { errors: warnings } = this.state;

        /** Check if there are no warnings. */
        let isDataValid = Object.keys(warnings).every(warning => warnings[warning] === null);

        /**
         * Check if there is something different, otherwise
         * it wouldn't make sense to call the endpoint
         */
        let areDifferences = this.checkForDifferences();

        return isDataValid && areDifferences;
    }

    /** Specific validations for fields. */
    private validateData() {
        const { firstName, lastName, email, username } = this.state.userInfo;

        let userInfo: UserInfo = {
            firstName,
            lastName,
            email,
            username,
        };

        let userDataErrors: UserInfoErrors = validateUserData(userInfo);

        this.setState({
            errors: { ...userDataErrors },
            fieldsEmpty: false,
        });
    }

    /** Check if there were any changes made in the user data, so we know if we should call the EP or not */
    private checkForDifferences(): boolean {
        const { userInfo, initialUserInfo } = this.state;

        return !Object.keys(userInfo).every(property => userInfo[property] === initialUserInfo[property]);
    }
}