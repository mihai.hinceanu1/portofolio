import { initUpdatePasswordState, isWideScreen, shouldExtendForm } from './_utils/forms.utils';
import * as div from './update-password.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { colors } from '../../../shared/style/colors';
import { UpdatePasswordConfig, UpdatePasswordErrors } from '../../interfaces/account-settings';
import { validatePasswords } from '../../services/account-settings.utils';
import { resetResponseFlags, updatePassword, updatePasswordResult$ } from '../../services/users.service';
import { PASSWORD } from '../account-settings-menu/_assets/account-settings-menu.assets';
import { ToggleForm } from '../toggle-form/toggle-form';
import React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    email: string;
    width: number;
    isNarrow: boolean;
}

export interface UpdatePasswordState {
    isToggled: boolean;
    currentPassword: string;
    newPassword: string;
    confirmNewPassword: string;
    wasUpdateSuccessful: boolean;
    errors: UpdatePasswordErrors;
}

export class UpdatePassword extends React.Component<Props, UpdatePasswordState> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = initUpdatePasswordState();
    }

    public render() {
        const { email, width, isNarrow } = this.props;
        const { isToggled, currentPassword, newPassword, confirmNewPassword, errors } = this.state;
        const { passwordError, newPasswordError, confirmPasswordError } = errors;
        let shouldDisplayForm = isToggled || !isNarrow;

        return (
            <div.UpdatePassword data-cy='update-password'
                isNarrow={isNarrow}
                shouldExtend={shouldExtendForm(width)}>
                {
                    isNarrow &&
                    <ToggleForm isToggled={isToggled}
                        icon={PASSWORD}
                        label='Password'
                        onToggle={() => this.toggleForm()} />
                }
                {
                    shouldDisplayForm &&
                    <>
                        {/* Form */}
                        <div.Form data-cy='form'
                            shouldExtend={shouldExtendForm(width)}>

                            {/* Current password */}
                            <Input value={currentPassword}
                                type='password'
                                error={passwordError}
                                label={'Current Password'.toUpperCase()}
                                overrides={{ root: 'height: 80px; flex-grow: 1; margin-top: 20px;' }}
                                onFocus={() => this.handleInputFocus('passwordError')}
                                onChangeText={(text: string) => this.handleTextChange(text, 'currentPassword')} />

                            {/* New password */}
                            <Input value={newPassword}
                                type='password'
                                label={'New Password'.toUpperCase()}
                                error={newPasswordError}
                                overrides={{ root: 'height: 80px; flex-grow: 1;' }}
                                onFocus={() => this.handleInputFocus('newPasswordError')}
                                onChangeText={(text: string) => this.handleTextChange(text, 'newPassword')} />

                            {/* Confirm new password */}
                            <Input value={confirmNewPassword}
                                type='password'
                                label={'Confirm New Password'.toUpperCase()}
                                error={confirmPasswordError}
                                overrides={{ root: 'height: 80px; flex-grow: 1;' }}
                                onFocus={() => this.handleInputFocus('confirmPasswordError')}
                                onChangeText={(text: string) => this.handleTextChange(text, 'confirmNewPassword')} />

                            {/* Inform user if the update was successful or not */}
                            {this.getUpdateMessage(width)}
                        </div.Form>

                        {/* Guidelines for creating a new password */}
                        {
                            isWideScreen(width) &&
                            <div.Guidelines data-cy='guidelines'>
                                <div.Text>
                                    Enter a new password for
                                    {/* Bold email highlight */}
                                    <div.Email>&nbsp; {email}</div.Email>.
                                    We highly recommend you create a new password - one that you don't use for any other website.
                                </div.Text>

                                <div.Break />

                                <div.Text>Note: You can't reuse your old password once you change it.</div.Text>
                            </div.Guidelines>
                        }

                        {/* Submit */}
                        <Button config={{
                            text: 'Change Password',
                            color: colors.$blue,
                            fontSize: 15,
                            onPress: () => this.updatePassword(),
                        }}
                            overrides={{ root: 'margin-top: 5px;' }} />
                    </>
                }

            </div.UpdatePassword>
        );
    }

    public componentDidMount() {
        this.subscribeToUpdatePasswordResult();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToUpdatePasswordResult() {
        updatePasswordResult$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(wasUpdateSuccessful => {
                this.setState({
                    wasUpdateSuccessful: wasUpdateSuccessful,
                    errors: {
                        passwordError: null,
                        newPasswordError: null,
                        confirmPasswordError: null,
                    },
                });
            });
    }

    private toggleForm() {
        const { isToggled } = this.state;
        this.setState({
            isToggled: !isToggled,
        });
    }

    private handleInputFocus(fieldWithError: string) {
        const { wasUpdateSuccessful } = this.state;

        if (wasUpdateSuccessful !== null) {
            resetResponseFlags();
        }

        this.setState({
            wasUpdateSuccessful: null,
            errors: {
                ...this.state.errors,
                [fieldWithError]: null,
            },
        });
    }

    private handleTextChange(text: string, field: string) {
        this.setState({
            ...this.state,
            [field]: text,
        });
    }

    /**
     * Called when submit button is clicked, first it validates
     * the inputs, then sends the config to the server.
     */
    private updatePassword() {
        const { currentPassword, newPassword, confirmNewPassword } = this.state;

        const passConfig: UpdatePasswordConfig = {
            currentPassword,
            newPassword,
            newPasswordRepeat: confirmNewPassword,
        };

        let shouldSendConfig = validatePasswords(passConfig);

        if (shouldSendConfig.isValid) {
            updatePassword(passConfig);

            this.setState({
                currentPassword: '',
                newPassword: '',
                confirmNewPassword: '',
            });
        } else {
            this.setState({
                errors: {
                    passwordError: shouldSendConfig.passwordError,
                    newPasswordError: shouldSendConfig.newPasswordError,
                    confirmPasswordError: shouldSendConfig.confirmPasswordError,
                }
            });
        }
    }

    /**
     * Inform user on the success of the password update.
     */
    private getUpdateMessage(width: number): JSX.Element {
        const { wasUpdateSuccessful } = this.state;

        if (wasUpdateSuccessful === true) {
            return <div.Success data-cy='password-update-success'>Password updated successfully.</div.Success>;
        }

        if (wasUpdateSuccessful === false) {
            return (
                <div.Fail data-cy='password-update-fail'
                    isTablet={!isWideScreen(width)}>
                    Update failed, make sure your password is correct.
                </div.Fail>
            );
        }
    }
}
