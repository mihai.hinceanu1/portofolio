import { BORDER_RADIUS, MIN_FORM_WIDTH } from './_constants/forms.const';
import { getMarginTop, getPaddings } from './_utils/forms.utils';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface UpdatePasswordProps {
    isNarrow: boolean;
    shouldExtend: boolean;
}

interface FormProps {
    shouldExtend: boolean;
}

interface FailProps {
    isTablet: boolean;
}

export const UpdatePassword = styled.View<UpdatePasswordProps>`
    background-color: white;
    min-width: ${MIN_FORM_WIDTH}px;
    ${props => getFlexWrap(props)};
    border-radius: ${BORDER_RADIUS}px;
    padding: ${props => getPaddings(props.isNarrow)};
    flex-direction: ${props => getFlexDirection(props)};
    margin-top: ${props => getMarginTop(props.isNarrow)}px;
`;

export const Form = styled.View<FormProps>`
    ${props => extendForm(props)}
`;

export const Success = styled.Text`
    margin-left: 10px;
    font-weight: 500;
    color: ${colors.$green};
`;

export const Fail = styled.Text<FailProps>`
    ${props => getFailWidth(props)}
    margin-left: 10px;
    font-weight: 500;
    color: ${colors.$red};
`;

export const Guidelines = styled.View`
    max-width: 220px;
    margin-left: 30px;
    padding-top:  10px;
`;

export const Text = styled.Text`
    font-size: ${font.$size12}px;
    line-height: 20px;
`;

export const Email = styled(Text)`
    font-weight: bold;
`;

export const Break = styled.View`
    height: 20px;
`;

// ====== UTILS ======

let getFlexWrap = (props: UpdatePasswordProps): string =>
    props.isNarrow ? '' : 'flex-wrap: wrap;';

let getFlexDirection = (props: UpdatePasswordProps): string =>
    props.shouldExtend ? 'column' : 'row';

let extendForm = (props: FormProps): string => {
    let css: string;

    if (props.shouldExtend) {
        css = `
            width: 100%;
            flex-direction: row;
            flex-wrap: wrap;
        `;
    }
    return css;
};

let getFailWidth = (props: FailProps): string =>
    props.isTablet ? '' : 'max-width: 250px;';