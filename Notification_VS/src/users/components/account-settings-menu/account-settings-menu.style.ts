import { BORDER_RADIUS, TOP_DISTANCE } from '../forms/_constants/forms.const';
import styled from 'styled-components/native';

export const AccountSettingsMenu = styled.View`
    background-color: white;
    box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.2);
    width: 260px;
    top: ${TOP_DISTANCE}px;
    border-radius: ${BORDER_RADIUS}px;
    align-self: flex-start;
    padding-bottom: 15px;
    position: sticky;
`;

export const Header = styled.Text`
    font-family: 'Open Sans';
    /* Because we are using an external font family be careful when
     changing this as the desired weight must be imported, otherwise
     it will take the closest value imported. */
    font-weight: 700;

    width: 100%;
    text-align: center;
    margin-top: 15px;
`;

export const SettingsMenu = styled.View``;
