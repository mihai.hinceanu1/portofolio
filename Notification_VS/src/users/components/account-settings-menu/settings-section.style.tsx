import { colors } from '../../../shared/style/colors';
import { SVG } from '../../interfaces/account-settings';
import React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface TextProps {
    isSelected: boolean;
}

export const SettingsSection = styled.TouchableHighlight`
    flex-direction: row;
    margin: 12px 25px;
`;

export const SymbolName = styled.View`
    margin-left: 25px;
    align-items: center;
    justify-content: center;
`;

export const Text = styled.Text<TextProps>`
    color: ${props => blackOrGreyText(props)};
    text-align: center;
    /* Caution when changing this value as the font family must have support for other values
    it must be linked properly in head (index.html) */
    font-weight: 600;
    font-family: 'Open Sans';
`;

// ====== SVG ======

export const sectionIcon = (sectionIcon: SVG, isSelected: boolean) =>
    <Svg width={sectionIcon.width} height={sectionIcon.height}>
        <Path d={sectionIcon.path} fill={isSelected ? colors.$blue : 'grey'} />
    </Svg>;

// ====== UTILS ======

let blackOrGreyText = (props: TextProps) =>
    props.isSelected ? colors.$blue : 'grey';