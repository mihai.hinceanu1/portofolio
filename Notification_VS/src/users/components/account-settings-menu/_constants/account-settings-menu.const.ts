import { ISettingsSection } from '../../../interfaces/account-settings';
import {
    DELETE,
    PASSWORD,
    /**
     * <!>Svg path for an icon, needed in the config,
     * parked as the functionalities aren't implemented
     * and the options shouldn't appear in the menu.
     */
    // EMAIL_ALERTS,
    // PAYMENTS,
    BASIC_INFO
    } from '../_assets/account-settings-menu.assets';

/** The dimensions of the container for user photo */
export const PHOTO_DIM = 135;

export const settingsSections: ISettingsSection[] = [
    {
        sectionTitle: 'Basic Information',
        sectionIcon: {
            path: BASIC_INFO,
            width: 19,
            height: 22,
        },
    },
    {
        sectionTitle: 'Password',
        sectionIcon: {
            path: PASSWORD,
            width: 22,
            height: 26
        },
    },
    // Parked until the functionalities are implemented
    // {
    //     sectionTitle: 'Email Alerts',
    //     sectionIcon: {
    //         path: EMAIL_ALERTS,
    //         width: 23,
    //         height: 25,
    //     },
    // },
    // {
    //     sectionTitle: 'Manage Payments',
    //     sectionIcon: {
    //         path: PAYMENTS,
    //         width: 22,
    //         height: 16,
    //     },
    // },
    {
        sectionTitle: 'Delete Account',
        sectionIcon: {
            path: DELETE,
            width: 22,
            height: 28,
        },
    }
];