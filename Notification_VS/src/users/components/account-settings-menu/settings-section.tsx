import * as div from './settings-section.style';
import { ISettingsSection } from '../../interfaces/account-settings';
import * as React from 'react';

interface Props {
    isSelected: boolean;
    section: ISettingsSection;
    // onPress: () => void;
}

/**
 * A menu category with an icon and a label
 * TODO Has hover effects
 * @param isSelected boolean
 * @param section Mock - represents the category, has all the data about it, including the icon.
 */
export function SettingsSection(props: Props) {
    const { isSelected, section } = props;
    const { sectionTitle, sectionIcon } = section;
    return (
        <div.SettingsSection data-cy='settings-section'
            underlayColor='transparent'
            onPress={() => console.log('Clicked')}>

            <>
                {/* Icon */}
                {div.sectionIcon(sectionIcon, isSelected)}

                {/* Label */}
                <div.SymbolName>
                    <div.Text data-cy='label' isSelected={isSelected}>
                        {sectionTitle}
                    </div.Text>
                </div.SymbolName>
            </>

        </div.SettingsSection>
    );
}