import * as div from './account-settings-menu.style';
import { SettingsSection } from './settings-section';
import { ISettingsSection } from '../../interfaces/account-settings';
import { Avatar } from '../avatar/avatar';
import * as React from 'react';

interface Props {
    settingsSections: ISettingsSection[];
}

export const AccountSettingsMenu: React.FunctionComponent<Props> = (props: Props) => {
    const { settingsSections } = props;

    return (
        <div.AccountSettingsMenu data-cy='account-settings-menu'>

            {/* Header */}
            <div.Header data-cy='header'>
                Account Settings
            </div.Header>

            {/* Avatar */}
            <Avatar isNarrow={false} />

            {/* Settings Menu */}
            <div.SettingsMenu data-cy='settings-menu'>
                {
                    settingsSections.map((section, index) =>
                        <SettingsSection key={index}
                            section={section}
                            isSelected={index === 0} />
                    )
                }
            </div.SettingsMenu>

        </div.AccountSettingsMenu>
    );
};
