import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

// ====== PROPS ======

interface NavItemProps {
    isSelected: boolean;
    isFirst: boolean;
    isLast: boolean;
}

interface ItemNameProps {
    isSelected: boolean;
}

// ====== STYLES ======

export const NavItem = styled.TouchableHighlight<NavItemProps>`
    background-color: ${props => getItemBackground(props)};
    height: 100%;
    width: 140px;
    align-items: center;
    justify-content: center;
    ${props => getItemBorder(props)}
    border-color: ${colors.$blue};
`;

export const ItemName = styled.Text<ItemNameProps>`
    color: ${props => getItemTextColor(props)};
    font-size: ${font.$size16}px;
`;

// ====== UTILS ======

let getItemBackground = (props: NavItemProps): string =>
    props.isSelected ? colors.$blue : colors.$white;

let getItemBorder = (props: NavItemProps): string => {
    if (props.isFirst) {
        return `
            border-width: 2px;
            border-right-width: 1px;
            border-top-left-radius: 15px;
            border-bottom-left-radius: 15px;
        `;
    }

    if (props.isLast) {
        return `
            border-width: 2px;
            border-left-width: 1px;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
        `;
    }

    return `
        border-left-width: 1px;
        border-right-width: 1px;
        border-top-width: 2px;
        border-bottom-width: 2px;
    `;
};

let getItemTextColor = (props: ItemNameProps): string =>
    props.isSelected ? colors.$white : colors.$blue;