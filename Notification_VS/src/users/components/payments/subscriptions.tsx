import * as div from './subscriptions.style';
import React from 'react';

interface Props {}

/**
 * The subscriptions menu appears when the subscription menu is selected.
 * It shows the ongoing subscriptions of the user.
 */
export const Subscriptions: React.FunctionComponent<Props> = (props: Props) => {

    const {} = props;

    return (
        <div.Subscriptions data-cy='subscriptions'>
            No subscriptions yet.
        </div.Subscriptions>
    );
};