import * as div from './history.style';
import React from 'react';
import { Text } from 'react-native';

interface Props { }

/**
 * This menu appears when the history menu is selected.
 *
 * Here the user can see his purchase history.
 */
export const History: React.FunctionComponent<Props> = (props: Props) => {
    const { } = props;

    return (
        <div.History data-cy='history'>
            <Text> No purchases made </Text>
        </div.History>
    );
};