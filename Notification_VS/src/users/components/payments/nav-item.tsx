import { NAV_ITEMS } from './const/payments.const';
import * as div from './nav-item.style';
import { colors } from '../../../shared/style/colors';
import React from 'react';

interface Props {
    selectedMenu: NAV_ITEMS;
    onItemPress: (menuItem: NAV_ITEMS) => void;
    item: NAV_ITEMS;
    /**
     * Flag controlled by the parent to check if the item is FIRST
     * in the list. When true, it will make the LEFT borders rounded
     * to match design.
     */
    isFirst: boolean;

    /**
     * Flag controlled by the parent to check if the item is LAST
     * in the list. When true, it will make the RIGHT borders rounded
     * to match design.
     */
    isLast: boolean;
}

/**
 * An item in the payments navbar.
 * When selected they appear highlighted.
 * Flags must be set for first and last items as they appear distinct from others
 * because of the rounded borders.
 */
export const NavItem: React.FunctionComponent<Props> = (props: Props) => {
    const { isFirst, isLast, selectedMenu, onItemPress, item } = props;

    return (
        <div.NavItem data-cy='nav-item'
            isLast={isLast}
            isFirst={isFirst}
            isSelected={selectedMenu === item}
            underlayColor={colors.$blue}
            onPress={() => onItemPress(item)}>

            {/** Item name */}
            <div.ItemName data-cy='item-name'
                isSelected={selectedMenu === item}>
                {item}
            </div.ItemName>
        </div.NavItem>
    );
};