import { NAV_ITEMS } from './const/payments.const';
import * as div from './nav-bar.style';
import { NavItem } from './nav-item';
import React from 'react';

interface Props {
    onItemPress: (menuItem: NAV_ITEMS) => void;
    selectedMenu: NAV_ITEMS;
    items: NAV_ITEMS[];
}

/**
 * A navigation bar to switch between menus in payments.
 * First and last item have the borders rounded, like in the design.
 */
export const NavBar: React.FunctionComponent<Props> = (props: Props) => {
    const { onItemPress, selectedMenu, items } = props;

    return (
        <div.NavBar data-cy='nav-bar'>
            {
                items.length > 0 &&
                items.map((item, index) =>
                    <NavItem key={index}
                        selectedMenu={selectedMenu}
                        onItemPress={onItemPress}
                        item={item}
                        isFirst={index === 0}
                        isLast={index === items.length - 1}
                    />
                )
            }
        </div.NavBar>
    );
};