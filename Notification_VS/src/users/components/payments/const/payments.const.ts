export enum NAV_ITEMS {
    SUBSCRIPTIONS = 'Subscriptions',
    PAYMENT_METHOD = 'Payment Method',
    HISTORY = 'History'
}

/** The menus appearing in the payments navigation bar. */
export const navigatorItems: NAV_ITEMS[] = [
    NAV_ITEMS.SUBSCRIPTIONS,
    NAV_ITEMS.PAYMENT_METHOD,
    NAV_ITEMS.HISTORY,
];