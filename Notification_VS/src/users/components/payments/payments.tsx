import { NAV_ITEMS, navigatorItems } from './const/payments.const';
import { History } from './history';
import { NavBar } from './nav-bar';
import { PaymentMethod } from './payment-method';
import * as div from './payments.style';
import { Subscriptions } from './subscriptions';
import React from 'react';

interface Props { }
interface State {
    selectedMenu: NAV_ITEMS;
}

/**
 * <!> For now this is just a visual component, will be functional once the payments module is
 * finished.
 *
 * This controls all the menus for payments module.
 * Here the user can see all the information about his payments,
 * like history and ongoing subscriptions, as well as change his
 * payment method.
 */
export class Payments extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            selectedMenu: navigatorItems[0],
        };
    }

    public render() {
        const { selectedMenu } = this.state;

        return (
            <div.Payments>
                {/** Navigation bar */}
                <NavBar items={navigatorItems}
                    selectedMenu={selectedMenu}
                    onItemPress={(menuItem) => this.selectNavMenu(menuItem)}/>

                {
                    // Subscriptions
                    selectedMenu === NAV_ITEMS.SUBSCRIPTIONS &&
                    <Subscriptions />
                }
                {
                    // Payment Method
                    selectedMenu === NAV_ITEMS.PAYMENT_METHOD &&
                    <PaymentMethod />
                }
                {
                    // History
                    selectedMenu === NAV_ITEMS.HISTORY &&
                    <History />
                }
            </div.Payments>
        );
    }

    private selectNavMenu(menu: NAV_ITEMS) {
        this.setState({
            selectedMenu: menu,
        });
    }
}