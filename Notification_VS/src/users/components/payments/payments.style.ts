import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const Payments = styled.View`
    margin-top: 30px;
    padding: 25px;
    background-color: ${colors.$white};
    border-radius: 1px;
`;
