import * as div from './payment-method.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import React from 'react';

interface Props { }

/**
 * <!> Should be class component once the payments module is finished.
 *
 * This menu appears when the payment method menu is selected.
 *
 * The user can see and edit his payment information.
 * He must select first one of the compatible payment methods and add in
 * his information.
 * Here he can change that information at any time.
 */
export const PaymentMethod: React.FunctionComponent<Props> = (props: Props) => {

    const { } = props;

    return (
        <div.PaymentMethod data-cy='payment-method'>

            {/* Card */}
            <div.Card data-cy='card'>
                <FitImage imgPath={'/users/mastercard-card.png'} />
            </div.Card>

        </div.PaymentMethod>
    );
};