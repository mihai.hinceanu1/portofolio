/**
 * An icon for a section, contains the dimensions for the icon,
 * but they can be removed and we can use getBoundingBox().
 */
export interface SVG {
    path: string;
    // TODO remove
    width: number;
    height: number;
}

/**
 * Representation of a section for settings in
 * account settings menu, having a title and an icon.
 */
export interface ISettingsSection {
    sectionTitle: string;
    sectionIcon: SVG;
}

/**
 * Necessary data for a request to update password.
 */
export interface UpdatePasswordConfig {
    currentPassword: string;
    newPassword: string;
    newPasswordRepeat: string;
}

/**
 * All the fields a user can update.
 */
export interface UserInfo {
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    [key: string]: string;
}

/** Specific error messages for the update password form */
export interface UpdatePasswordErrors {
    passwordError: string;
    newPasswordError: string;
    confirmPasswordError: string;
}

/**
 * Contains a boolean indicating if the config
 * is valid or specific errors for each field
 */
export interface ValidateUpdatePasswordConfig extends UpdatePasswordErrors {
    isValid: boolean;
}

/** Warnings for user info */
export interface UserInfoErrors {
    firstNameError: string;
    lastNameError: string;
    emailError: string;
    usernameError: string;
    [key: string]: string;
}