import { UserInfo } from './account-settings';
import { User } from './user';

export interface UsersState {

    /** Current user logged in the app. */
    user: User;

    accountSettings: {
        userDetails: UserInfo;

        /** Save server response for update password */
        passwordUpdated: boolean;

        /** Save server response when user attempts to update his info */
        userInfoUpdated: boolean;

        /** Server response for delete user */
        userDeleted: boolean;
    };

    /**
     * User currently edited.
     * Most of the time it will be the current logged in user.
     * In some cases, the admin will edit user details from other users.
     */
    editUser: User; // TO IMPLEMENT - Not used yet

    /** Search users results. */
    searchUsers: User[]; // TO IMPLEMENT - Not used yet

    modals: {
        deleteUserConfirm: boolean; // TO IMPLEMENT - Not used yet
    };

    admin: {

        /** Stores the users summary DTOs. */
        users: User[]; // TO IMPLEMENT - Not used yet
    };
}
