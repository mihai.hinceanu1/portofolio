import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * Our users system retains the typical information a user system needs.
 * All other modules extend this interface if needed.
 * Users do not know of the existence of other modules such as lessons.
 *
 * <!> Author of was removed because all the objects have this information already defined.
 *     This db is read optimized. For this reason we want to keep the author info on the objects.
 *     Unlike `topic` the `user` object does not track the children. The children track the parent.
 *     If we need to list the articles of an author we can use a cached table.
 */
export interface User extends CommonMetadata {

    /** Username used to login. */
    username: string;

    /** Bcrypted password. */
    password: string;

    /** User name. */
    firstName: string;

    lastName: string;

    /** Vanity url. */
    pathName: string;

    /** User email. */
    email: string;

    // REVIEW should be in auth ???
    /** Flag to check whether an user has confirmed the registration or not. */
    isConfirmed: boolean;

    /**
     * Can be one of 'Admin', 'Student', 'Teacher', 'Guest'
     * Each role come up with different access to resources
     */
    role: string;

    // CODE SMELL should be in auth
    /**
     * Token created when user registers on the app.
     * Userful for checking if registration was confirmed by user via email.
     */
    registrationToken: string;

    // CODE SMELL should be in auth
    /** Token created when password have to change. */
    resetPasswordToken: string;

}
