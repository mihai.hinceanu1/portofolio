import { Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { store, store$ } from '../../shared/services/state.service';
import { UpdatePasswordConfig, UserInfo } from '../interfaces/account-settings';
import { User } from '../interfaces/user';
import * as actions from '../state/users.actions';
import * as sel from '../state/users.selectors';
import { Base64 } from 'js-base64';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== GET USER ====== // TO RESTORE

export function getUserByUsername(username: string) {
    store.dispatch(
        actions.getUserByUsernameReq(username),
    );
}

export function getUser() {
    store.dispatch(
        actions.getUserReq(),
    );
}

export const user$ = () => store$.pipe(
    map(state => sel.USER(state)),
    skipWhile(user => !user),
    distinctUntilChanged(),
);

export const userDetails$ = () => store$.pipe(
    map(state => sel.USER_DETAILS(state)),
    skipWhile(user => !user),
    distinctUntilChanged(),
);

// ====== EDIT USER ======

export function editUser(user: UserInfo) {
    store.dispatch(
        actions.editUserReq(user),
    );
}

export function resetResponseFlags() {
    store.dispatch(
        actions.resetUserInfoResponse(),
    );
}

export const updateUserInfoResult$ = () => store$.pipe(
    map(state => sel.UPDATE_USER_INFO_RESULT(state)),
    skipWhile(userInfoUpdated => userInfoUpdated === null),
    // distinctUntilChanged(),
);

// ====== UPDATE USER PASSWORD ======

export function updatePassword(passwordConfig: UpdatePasswordConfig) {
    store.dispatch(
        actions.updatePasswordReq(passwordConfig),
    );
}

export const updatePasswordResult$ = () => store$.pipe(
    map(state => sel.UPDATE_PASSWORD_RESULT(state)),
    skipWhile(passwordUpdated => passwordUpdated === null),
    distinctUntilChanged(),
);

// ====== DELETE USER ======

export function deleteUser(password: string) {
    let encryptedPassword = Base64.encode(password);

    store.dispatch(
        actions.deleteUser(encryptedPassword),
    );
}

export const deleteUserResponse$ = () => store$.pipe(
    map(state => sel.DELETE_USER_RESPONSE(state)),
    skipWhile(userDeleted => userDeleted === null),
    // distinctUntilChanged(),
);

// ====== DELETE USER CONFIRM MODAL ====== // TO RESTORE

export const deleteUserConfirmModal$ = () => store$.pipe(
    map(state => sel.DELETE_USER_CONFIRM_MODAL(state)),
    skipWhile(deleteUserConfirmModal => !deleteUserConfirmModal),
    distinctUntilChanged(),
);

export function setDeleteUserConfirmModal(isVisible: boolean) {
    store.dispatch(
        actions.setDeleteUserConfirmModal(isVisible),
    );
}

// ====== CREATE USER ====== // TO RESTORE

export function createUser(user: User) {
    store.dispatch(
        actions.createUserReq(user),
    );
}

// ====== GET ALL USERS ====== // TO RESTORE

export function getAllUsers(request?: Paginated) {
    store.dispatch(
        actions.getAllUsersReq(request),
    );
}

export const users$ = (): Observable<User[]> => store$.pipe(
    map(state => sel.USERS(state)),
    skipWhile(users => !users),
    distinctUntilChanged(),
);

// ======  SEARCH USERS ====== // TO RESTORE

export function searchUsers(request: PaginatedSearchReq) {
    store.dispatch(
        actions.searchUsersReq(request),
    );
}

export const searchUsers$ = (): Observable<User[]> => store$.pipe(
    map(state => sel.SEARCH_USERS(state)),
    skipWhile(users => !users),
    distinctUntilChanged(),
);