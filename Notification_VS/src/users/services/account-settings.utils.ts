import {
    UpdatePasswordConfig,
    UserInfo,
    UserInfoErrors,
    ValidateUpdatePasswordConfig
    } from '../interfaces/account-settings';
import {
    isEmailValid,
    // <!> Parked until the password strength check is implemented in the server.
    // isPasswordSecure
} from '../../auth/services/auth.utils';

/**
 * Check if the required fields for password updated are filled
 * correctly and if the new password is strong enough.
 */
export function validatePasswords(passConfig: UpdatePasswordConfig): ValidateUpdatePasswordConfig {
    const { currentPassword, newPassword, newPasswordRepeat } = passConfig;

    let result: ValidateUpdatePasswordConfig = {
        isValid: true,
        passwordError: null,
        newPasswordError: null,
        confirmPasswordError: null,
    };

    if (currentPassword.length === 0) {
        result.isValid = false;
        result.passwordError = 'Type in the password to confirm your identity.';
        return result;
    }

    if (newPassword.length === 0) {
        result.isValid = false;
        result.newPasswordError = 'Type in the new password.';
        return result;
    }

    if (newPasswordRepeat.length === 0) {
        result.isValid = false;
        result.confirmPasswordError = 'Confirm the new password.';
        return result;
    }

    if (currentPassword === newPassword) {
        result.isValid = false;
        result.newPasswordError = 'New password can\'t be the same as the old password';
        return result;
    }

    if (newPassword !== newPasswordRepeat) {
        result.isValid = false;
        result.confirmPasswordError = 'Please make sure that the passwords match';
        return result;
    }

    /** <!> Parked until this strength check is also implemented in the server */
    // if (!isPasswordSecure(newPassword)) {
        // result.isValid = false;
        // result.newPasswordError = 'New password isn\'t strong enough.';
        // return result;
    // }

    return result;
}

/**
 * Validate the fields an user can modify.
 * Validations include:
 * Checking if fields are filled;
 * Checking if they have the correct number of characters;
 * Email regex validations.
 *
 * Conditions are verified in order by their importance,
 * most important condition being last.
 */
export function validateUserData(userInfo: UserInfo): UserInfoErrors {
    const { firstName, lastName, email, username } = userInfo;

    let errors: UserInfoErrors = {
        firstNameError: null,
        lastNameError: null,
        emailError: null,
        usernameError: null,
    };

    if (!isEmailValid(email)) {
        errors = {
            ...errors,
            emailError: 'Invalid email',
        };
    }

    if (!firstName) {
        errors = {
            ...errors,
            firstNameError: 'First name is required.',
        };
    }

    if (!lastName) {
        errors = {
            ...errors,
            lastNameError: 'Last name is required.',
        };
    }

    if (!email) {
        errors = {
            ...errors,
            emailError: 'Email is required.',
        };
    }

    if (!username) {
        errors = {
            ...errors,
            usernameError: 'Username is required.',
        };
    }

    return errors;
}