import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { UserSettingsPage } from '../pages/user-settings-page/user-settings.page';
import * as React from 'react';
import { Route } from 'react-router';

/** User Routes */
export const userRoutes = (loginResponse: LoginResponse) => {
    return (
        <Route exact={true} path='/user/:username'
            render={props => renderPage(props, true, '/auth/login', UserSettingsPage, loginResponse)} />
    );
};