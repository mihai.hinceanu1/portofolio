import { MOBILE_WIDTH } from '../../../shared/constants/adaptive.const';

export const shouldRenderSideMenu = (width: number): boolean => width >= MOBILE_WIDTH;