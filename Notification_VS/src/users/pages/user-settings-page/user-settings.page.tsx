import * as div from './user-settings.page.style';
import { shouldRenderSideMenu } from './user-settings.page.utils';
import { settingsSections } from '../../components/account-settings-menu/_constants/account-settings-menu.const';
import { AccountSettingsMenu } from '../../components/account-settings-menu/account-settings-menu';
import { Avatar } from '../../components/avatar/avatar';
import { DeleteAccount } from '../../components/delete-account/delete-account';
import { BasicInformation } from '../../components/forms/basic-information';
import { UpdatePassword } from '../../components/forms/update-password';
import { UserInfo } from '../../interfaces/account-settings';
import { getUser, userDetails$ } from '../../services/users.service';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { EmailPreferences } from '../../components/forms/email-preferences'; // PARKED
// import { Payments } from '../../components/payments/payments'; // PARKED

interface Params {
    username: string;
}

interface Props extends RouteComponentProps<Params> { }

interface State {
    initialUserInfo: UserInfo;
    width: number;
}

/**
 * Main Page where the user can edit his info and password.
 * The user can also delete his/her account.
 *
 * TODO Edit avatar.
 * TODO Add social contact.
 * TODO Add payments system.
 * TODO Add preferences for notifications.
 */
class _UserSettingsPage extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            initialUserInfo: {
                firstName: null,
                lastName: null,
                email: null,
                username: null,
            },
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { width, initialUserInfo } = this.state;
        const { email } = initialUserInfo;
        const isNarrow = !shouldRenderSideMenu(width);

        return (
            <div.UserSettingsPage data-cy='user-settings-page'
                nativeID='user-settings-page'
                onLayout={e => this.updateWidth(e)}>

                <div.CenterChildren data-cy='center-children'>
                    {
                        // Sidebar
                        !isNarrow &&
                        <AccountSettingsMenu settingsSections={settingsSections} />
                    }

                    {/* Forms */}
                    <div.Forms data-cy='forms' isNarrow={isNarrow}>

                        {
                            // Avatar
                            isNarrow &&
                            <Avatar isNarrow={isNarrow} />
                        }

                        {/* Basic Info */}
                        <BasicInformation isNarrow={isNarrow} />

                        {/* Password */}
                        <UpdatePassword email={email} width={width} isNarrow={isNarrow} />

                        {/* Email Preferences - PARKED */}
                        {/* <EmailPreferences /> */}

                        {/* Email Payments - PARKED */}
                        {/* <Payments /> */}

                        <DeleteAccount isNarrow={isNarrow} />

                    </div.Forms>
                </div.CenterChildren>
            </div.UserSettingsPage>
        );
    }

    public componentDidMount() {
        getUser();
        this.subscribeToUserDetails();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private subscribeToUserDetails() {
        userDetails$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(user => {
                this.setState({
                    ...this.state,
                    initialUserInfo: { ...user },
                });
            });
    }
}

export const UserSettingsPage = withRouter(_UserSettingsPage);
