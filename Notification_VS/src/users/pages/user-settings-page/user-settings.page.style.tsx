import * as React from 'react';
import styled from 'styled-components/native';

interface FormsProps {
    isNarrow: boolean;
}

export const UserSettingsPage = styled.ScrollView`
    width: 100%;
    height: 100%;
`;

const Center = styled.View`
    flex-direction: row;
    justify-content: center;
`;

/**
 * Needed to float a sticky menu and a scrollable view in the center of the page.
 * <!> It seems that using a styled component simply does not render the same behavior.
 */
export const CenterChildren: React.FunctionComponent = ({ children }) =>
    <Center data-cy='center'>
        {children}
    </Center>;

export const Forms = styled.View<FormsProps>`
    flex: 1 1 0;
    max-width: 615px;
    margin-top: 110px;
    margin-left: ${props => getMarginLeft(props)}px;
    border-radius: 2px;
`;

// ====== UTILS ======

let getMarginLeft = (props: FormsProps) =>
    props.isNarrow ? 0 : 30;