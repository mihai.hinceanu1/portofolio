/// <reference types="cypress" />

describe('User settings tests', () => {
    it('Log in and go to the user settings page', () => {
        cy.login();

        /** Stub get user */
        cy.fixture('user.json').as('user');
        cy.route({
            method: 'GET',
            url: '/api/users/user',
            response: '@user',
            status: 200
        }).as('getUser');

        cy.visit('http://localhost:3000/user/admin');

        /** Wait for the user */
        cy.wait('@getUser');

        cy.url().should('eq', 'http://localhost:3000/user/admin');
        cy.get('#user-settings-page').should('exist');
    });

    it('Forms exist', () => {
        getEl('forms').should('exist');
    });

    it('On wider screens the left side menu appears', () => {
        cy.viewport(1200, 700);
        getEl('account-settings-menu').should('exist');
    });

    it('On screens narrower than 600px, the left menu disappears', () => {
        cy.viewport(500, 600);
        getEl('account-settings-menu').should('not.exist');
    });

    it('User Info fields should be filled', () => {
        isInputEmpty('user-info', 0);
        isInputEmpty('user-info', 1);
        isInputEmpty('user-info', 2);
        isInputEmpty('user-info', 3);
    });

    it('Each form has a submit button', () => {
        getNestedElement('basic-information', 'button').should('exist');
        getNestedElement('update-password', 'button').should('exist');
        getNestedElement('delete-account', 'button').should('exist');
    });

    it('When an user info update fails a message is shown.', () => {
        cy.server();
        cy.route({
            method: 'PUT',
            url: '/api/users/user',
            response: {
                isSuccess: false,
                message: 'User not found'
            },
            status: 400,
        }).as('editUser');

        getNestedElement('basic-information', 'input').eq(0).type('el');
        getNestedElement('basic-information', 'button').click();

        /** Wait for the update response */
        cy.wait('@editUser');
        getEl('info-update-fail').should('exist');

        /** Reset the input */
        getNestedElement('basic-information', 'input').eq(0).clear().type('John');
    });

    it('When an user info update is successful a message is shown.', () => {
        cy.server();
        cy.route({
            method: 'PUT',
            url: '/api/users/user',
            response: {
                isSuccess: true,
                message: 'Update successful'
            },
            status: 200,
        }).as('editUser');

        getNestedElement('basic-information', 'input').eq(0).type('el');
        getNestedElement('basic-information', 'button').click();

        /** Wait for the update response */
        cy.wait('@editUser');
        getEl('info-update-success').should('exist');

        /** Check that we still have the updated value */
        getNestedElement('basic-information', 'input').eq(0).should('have.value', 'Johnel');
    });

    it('Clicking update password button without typing any password will show an error message', () => {
        getNestedElement('update-password', 'button').click();
        getNestedElement('update-password', 'error-message').should('have.text', 'Type in the password to confirm your identity.');
    });

    it('When new password and confirm password don\'t match an error is shown', () => {
        getNestedElement('update-password', 'input').eq(0).type('admin');
        getNestedElement('update-password', 'input').eq(1).type('newpassword');
        getNestedElement('update-password', 'input').eq(2).type('notmatchingnewpassword');
        getNestedElement('update-password', 'button').click();
        getNestedElement('update-password', 'error-message').should('have.text', 'Please make sure that the passwords match');

        /** Clear fields so they don't interfere with other tests. */
        getNestedElement('update-password', 'input').clear();
    });

    it('When any field is not completed an error is shown', () => {
        getNestedElement('update-password', 'input').eq(1).type('newpassword');
        getNestedElement('update-password', 'input').eq(2).type('notmatchingnewpassword');
        getNestedElement('update-password', 'button').click();
        getNestedElement('update-password', 'error-message').should('have.text', 'Type in the password to confirm your identity.');

        /** Clear fields so they don't interfere with other tests. */
        getNestedElement('update-password', 'input').clear();

        getNestedElement('update-password', 'input').eq(0).type('admin');
        getNestedElement('update-password', 'input').eq(2).type('notmatchingnewpassword');
        getNestedElement('update-password', 'button').click();
        getNestedElement('update-password', 'error-message').should('have.text', 'Type in the new password.');

        /** Clear fields so they don't interfere with other tests. */
        getNestedElement('update-password', 'input').clear();

        getNestedElement('update-password', 'input').eq(0).type('admin');
        getNestedElement('update-password', 'input').eq(1).type('newpassword');
        getNestedElement('update-password', 'button').click();
        getNestedElement('update-password', 'error-message').should('have.text', 'Confirm the new password.');

        /** Clear fields so they don't interfere with other tests. */
        getNestedElement('update-password', 'input').clear();
    });

    it('Trying to update with the incorrect password will throw an error', () => {
        /** Define route */
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/users/update-password',
            response: 'Passwords do not match',
            status: 400,
        }).as('updatePassword');
        getNestedElement('update-password', 'input').eq(0).type('wrongpassword');
        getNestedElement('update-password', 'input').eq(1).type('newpassword');
        getNestedElement('update-password', 'input').eq(2).type('newpassword');
        getNestedElement('update-password', 'button').click();

        /** Wait response */
        cy.wait('@updatePassword');
        getEl('password-update-fail').should('exist');
    });

    it('When all fields are correctly field update is successful', () => {
        /** Define route */
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/users/update-password',
            response: 'Password update successful',
            status: 200,
        }).as('updatePassword');

        getNestedElement('update-password', 'input').eq(0).type('admin');
        getNestedElement('update-password', 'input').eq(1).type('newpassword');
        getNestedElement('update-password', 'input').eq(2).type('newpassword');
        getNestedElement('update-password', 'button').click();

        /** Wait response */
        cy.wait('@updatePassword');
        getEl('password-update-success').should('exist');
    });

    it('When clicking on delete account button a modal appears', () => {
        getNestedElement('delete-account', 'button').click();
        getEl('modal').should('exist');
        getEl('confirmation-modal').should('exist');
    });

    it('In the confirmation modal should exist an input and two buttons', () => {
        getNestedElement('confirmation-modal', 'input').should('exist');
        getNestedElement('confirmation-modal', 'button').should('have.length', 2);
    });

    it('Clicking on the cancel button will close the modal', () => {
        getNestedElement('confirmation-modal', 'button').first().click();
        getEl('modal').should('not.exist');
    });

    it('Attempting to delete without typing the password will show an error', () => {
        getNestedElement('delete-account', 'button').click();
        getNestedElement('confirmation-modal', 'button').eq(1).click();
        getNestedElement('confirmation-modal', 'password-empty-error').should('exist');
    });

    it('Focusing on the input will remove that error', () => {
        getNestedElement('confirmation-modal', 'input').trigger('focus');
        getNestedElement('confirmation-modal', 'password-empty-error').should('not.exist');
    });

    it('Typing in the wrong password will show an error after clicking delete', () => {
        /** Define route */
        cy.server();
        cy.route({
            method: 'DELETE',
            url: '/api/users/user',
            response: 'Incorrect password',
            status: 400,
        }).as('deleteUser');

        getNestedElement('confirmation-modal', 'input').type('wrongPassword');
        getNestedElement('confirmation-modal', 'button').eq(1).click();

        /** Wait for the request */
        cy.wait('@deleteUser');
        getNestedElement('confirmation-modal', 'delete-failed-error').should('exist');

        /** Clear the input so the test doesn't interfere with other tests */
        getNestedElement('confirmation-modal', 'input').clear();
    });

    it('Delete successful', () => {
        /** Define route */
        cy.server();
        cy.route({
            method: 'DELETE',
            url: '/api/users/user',
            response: 'User deleted',
            status: 200,
        }).as('deleteUser');

        getNestedElement('confirmation-modal', 'input').type('wrongPassword');
        getNestedElement('confirmation-modal', 'button').eq(1).click();

        /** Wait for the request */
        cy.wait('@deleteUser');

        /** User is redirected to login */
        cy.url().should('equal', 'http://localhost:3000/auth/login');
    });
});

// ====== SELECTORS ======

function getEl(element) {
    return cy.get(`[data-cy=${element}]`);
}

function getNestedElement(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

// ====== UTILS ======

function isInputEmpty(wrapper, index) {
    return cy.get(`[data-cy=${wrapper}] input`).eq(index).should('not.have.value', '');
}
