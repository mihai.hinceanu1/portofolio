import styled from 'styled-components/native';

export const LessonNavigator = styled.View`
    flex-direction: row;
    height: 100%;
    width: 100%;
`;
