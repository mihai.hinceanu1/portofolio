/// <reference types="cypress" />

/** <!>TODO Update tests */
describe('Topic page navigator tests', () => {
    before('Login', () => {
        cy.login();
    });

    it('Go to lesson page', () => {
        cy.get('[data-cy=page-button]').eq(0).click();
        cy.url().should('include', 'http://localhost:3000/lesson');
    });
});