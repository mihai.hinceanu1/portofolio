import * as div from './lesson-navigator.style';
import { Lesson } from '../../../lessons/interfaces/lesson';
import { LessonNavLink, NavSelectedTopic } from '../../interfaces/lesson-navigator';
import { relatedTopicsInNavigator$, selectedChildTopicInNavigator$ } from '../../services/lesson-navigator-2.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface Props {
    config: {
        currentTopic: Lesson;
    };
}

interface State {
    relatedTopics: LessonNavLink[];
    selectedTopicNavigator: NavSelectedTopic;
    isExpanded: boolean;
}

/**
 * TODO REFACTOR THIS using the new branched options component
 *
 * Provides an easy way to navigate trough the current lesson.
 * Each child topic will have related topics defined.
 * Progress is visible in a blue stripe.
 * Goals are visible with a pole flag.
 * Grayed out topics are additional reading.
 */
export class LessonNavigator extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            relatedTopics: [] as LessonNavLink[],
            selectedTopicNavigator: {} as NavSelectedTopic,
            isExpanded: false,
        };
    }
    public render() {
        return (
            <div.LessonNavigator data-cy='lesson-navigator'/>
        );
    }

    public componentDidMount() {
        this.subscribeToRelatedTopicsInNavigator();
        this.subscribeToSelectedChildTopicInNavigator();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToRelatedTopicsInNavigator() {
        relatedTopicsInNavigator$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(relatedTopics => {
                this.setState({ relatedTopics });
            });
    }

    private subscribeToSelectedChildTopicInNavigator() {
        selectedChildTopicInNavigator$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(selectedTopicNavigator => {
                this.setState({ selectedTopicNavigator });
            });
    }

}