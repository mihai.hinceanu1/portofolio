import { store, store$ } from '../../shared/services/state.service';
import * as navigatorActions from '../state/lesson-navigator.actions';
import * as sel from '../state/lesson-navigator.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== LEARNING MAP NAVIGATOR VISIBILITY ======

// TODO REVIEW, we have the state here but the components somewhere else. We need to review which is which.
export function toggleLearningMapNav(isVisible?: boolean) {
    store.dispatch(
        navigatorActions.toggleLearningMapNav(isVisible),
    );
}

export const globalMapNavVisibility$ = (): Observable<boolean> => store$.pipe(
    map( state => sel.GLOBAL_MAP_NAV_VISIBILITY(state) ),
    skipWhile( isVisible => !isVisible ),
    distinctUntilChanged(),
);