import { store, store$ } from '../../shared/services/state.service';
import { LessonNavLink, NavSelectedTopic } from '../interfaces/lesson-navigator';
import * as actions from '../state/lesson-navigator.actions';
import * as selectors from '../state/lesson-navigator.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// CODE SMELL
export function setRelatedTopicsInNavigator(relatedTopics: LessonNavLink[]) {
    store.dispatch(
        actions.setRelatedTopicsForSelectedTopic(relatedTopics),
    );
}

export const relatedTopicsInNavigator$ = (): Observable<LessonNavLink[]> => store$.pipe(
    map(state => selectors.RELATED_TOPICS_FOR_SELECTED_TOPIC(state)),
    skipWhile(relatedTopics => !relatedTopics),
    distinctUntilChanged(),
);

// CODE SMELL
export function setSelectedChildTopicInNavigator(selectedTopic: NavSelectedTopic) {
    store.dispatch(
        actions.setSelectedChildTopicInNavigator(selectedTopic),
    );
}

/** Add props if necessary (create interface) */
export const selectedChildTopicInNavigator$ = (): Observable<NavSelectedTopic> => store$.pipe(
    map(state => selectors.SELECTED_CHILD_TOPIC_IN_NAVIGATOR(state)),
    skipWhile(selectedTopic => !selectedTopic),
    distinctUntilChanged(),
);