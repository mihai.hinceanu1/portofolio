// TODO REVIEW, bad naming or code smell
export interface LessonNavLink {
    _id: string;
    name: string;
    pathName: string;
    thumbnail: string;
}

// REVIEW
export interface NavSelectedTopic {
    _id: string;
    name: string;

    /**
     * For computing the distance from top for the right side container with related
     * Alignment with the selected child topic icon
     */
    selfIndex: number;
}