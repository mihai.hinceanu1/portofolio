import { LessonNavLink, NavSelectedTopic } from './lesson-navigator';

export interface LessonNavigatorState {

    /** Lessons navigator is render as a branches menu */
    isLessonNavVisible: boolean;

    // TODO RENAME
    /** Topics Navigator - children, related topics */
    relatedTopicsForSelectedTopic: LessonNavLink[];

    // TODO RENAME
    /** Topics Navigator - selected children topic */
    selectedChildTopicInNavigator: NavSelectedTopic;

}