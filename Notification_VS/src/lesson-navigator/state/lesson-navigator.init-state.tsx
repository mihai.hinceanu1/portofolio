import { LessonNavigatorState } from '../interfaces/lesson-navigator.state';

export const lessonNavigatorInitialState: LessonNavigatorState = {
    isLessonNavVisible: false,
    relatedTopicsForSelectedTopic: null,
    selectedChildTopicInNavigator: null
};