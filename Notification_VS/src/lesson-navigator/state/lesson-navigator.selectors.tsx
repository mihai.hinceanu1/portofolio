import { AppState } from '../../shared/interfaces/app.state';
import { LessonNavLink, NavSelectedTopic } from '../interfaces/lesson-navigator';
import { LessonNavigatorState } from '../interfaces/lesson-navigator.state';
import { createSelector, Selector } from 'reselect';

const LESSON_NAVIGATOR_MODULE: Selector<AppState, LessonNavigatorState> = (state: AppState) => state.lessonNavigator;

export const GLOBAL_MAP_NAV_VISIBILITY = createSelector<AppState, LessonNavigatorState, boolean>(
    LESSON_NAVIGATOR_MODULE,
    (state: LessonNavigatorState) => state.isLessonNavVisible,
);

/** Topics navigator - children, related */
export const RELATED_TOPICS_FOR_SELECTED_TOPIC = createSelector<AppState, LessonNavigatorState, LessonNavLink[]>(
    LESSON_NAVIGATOR_MODULE,
    state => state.relatedTopicsForSelectedTopic,
);

/** Topics navigator - children, related */
export const SELECTED_CHILD_TOPIC_IN_NAVIGATOR = createSelector<AppState, LessonNavigatorState, NavSelectedTopic>(
    LESSON_NAVIGATOR_MODULE,
    state => state.selectedChildTopicInNavigator,
);