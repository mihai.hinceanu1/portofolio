import * as actions from './lesson-navigator.actions';
import { lessonNavigatorInitialState } from './lesson-navigator.init-state';
import { LessonNavLink, NavSelectedTopic } from '../interfaces/lesson-navigator';
import { LessonNavigatorState } from '../interfaces/lesson-navigator.state';

export const lessonNavigatorReducer = (state: LessonNavigatorState = lessonNavigatorInitialState, action: any): LessonNavigatorState => {

    switch (action.type) {

        case actions.TOGGLE_GLOBAL_MAP_NAV: {
            return {
                ...state,
                isLessonNavVisible: action.payload !== undefined ? action.payload : !state.isLessonNavVisible,
            };
        }

        case actions.SET_RELATED_TOPICS_FOR_SELECTED_TOPIC: {
            return {
                ...state,
                relatedTopicsForSelectedTopic: action.payload as LessonNavLink[],
            };
        }

        case actions.SET_SELECTED_CHILD_TOPIC_IN_NAVIGATOR: {
            return {
                ...state,
                selectedChildTopicInNavigator: action.payload as NavSelectedTopic,
            };
        }

        default:
            return state;
    }

};
