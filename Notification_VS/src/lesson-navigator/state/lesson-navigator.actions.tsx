import { Action } from '../../shared/interfaces/shared';
import { LessonNavLink, NavSelectedTopic } from '../interfaces/lesson-navigator';

// ====== TOGGLE GLOBAL MAP NAV ======

export const TOGGLE_GLOBAL_MAP_NAV = 'TOGGLE_GLOBAL_MAP_NAV';
export const toggleLearningMapNav = (isVisible: boolean): Action<boolean> => ({
    type: TOGGLE_GLOBAL_MAP_NAV,
    payload: isVisible,
});

export const SET_RELATED_TOPICS_FOR_SELECTED_TOPIC = 'SET_RELATED_TOPICS_FOR_SELECTED_TOPIC';
export const setRelatedTopicsForSelectedTopic = (relatedTopics: LessonNavLink[]):
    Action<LessonNavLink[]> => ({
        type: SET_RELATED_TOPICS_FOR_SELECTED_TOPIC,
        payload: relatedTopics,
    });

export const SET_SELECTED_CHILD_TOPIC_IN_NAVIGATOR = 'SET_SELECTED_CHILD_TOPIC_IN_NAVIGATOR';
export const setSelectedChildTopicInNavigator = (selectedTopic: NavSelectedTopic): Action<NavSelectedTopic> => ({
    type: SET_SELECTED_CHILD_TOPIC_IN_NAVIGATOR,
    payload: selectedTopic,
});

export const SET_TOPICS_SCROLLVIEW_REF = 'SET_TOPICS_SCROLLVIEW_REF';
export const setTopicsScrollViewRef = (ref: any): Action<any> => ({
    type: SET_TOPICS_SCROLLVIEW_REF,
    payload: ref,
});