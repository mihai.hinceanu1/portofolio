import axios from '../../shared/services/axios.service';
import { IDependency } from '../interfaces/floating-menu';
import {
    AddPackageVersionImage,
    AddVersionDependencyReq,
    CreatePackageVersionCfg,
    DeletePackageVersionImage,
    EditPackageVersionImage,
    EditPackageVersionImageResponse,
    PackageVersion,
    UpdatePackageVersion,
    UploadPackageVersionImageResponse
    } from '../interfaces/package-version';
import { DeletePackageVersionConfig } from '../interfaces/source-code';
import { from, Observable } from 'rxjs';

export const editPackageVersion = (version: UpdatePackageVersion): Observable<PackageVersion> => {

    return from<Promise<PackageVersion>>(
        axios.patch<PackageVersion>(`/source-code/package-versions/${version.versionId}`, version
        )
            .then(res => res.data),
    );
};

export const getAllPackageVersions = (packagePathName: string): Observable<PackageVersion[]> =>
    from<Promise<PackageVersion[]>>(
        axios.get<PackageVersion[]>(`/source-code/package/package-versions/${packagePathName}`)
            .then(res => {
                return res.data;
            }),
    );

export const deletePackageVersion = (deletePackageVersionConfig: DeletePackageVersionConfig): Observable<string> =>
    from<Promise<string>>(axios.delete(
        `/source-code/package-versions/${deletePackageVersionConfig.packageVersionId}/${deletePackageVersionConfig.packageId}`)
        .then(res => res.data),
    );

export const createPackageVersion = (version: CreatePackageVersionCfg): Observable<{ packageVersion: PackageVersion, parentPathname: string }> => {
    return from<Promise<{ packageVersion: PackageVersion, parentPathname: string }>>(
        axios.post<{ packageVersion: PackageVersion, parentPathname: string }>(`/source-code/package-versions`, version)
            .then(res => res.data),
    );
};

export const addPackageVersionImage = (request: AddPackageVersionImage): Observable<UploadPackageVersionImageResponse> => {
    let formData = new FormData();

    formData.append('file', request.file as any);

    return from<Promise<UploadPackageVersionImageResponse>>(axios.post<UploadPackageVersionImageResponse>(
        `/source-code/package-versions/image/${request.versionId}`, formData, {
        headers: {
            'Content-Type': `multipart/form-data`,
        }
    })
        .then(res => res.data)
    );
};

export const editPackageVersionImage = (request: EditPackageVersionImage): Observable<EditPackageVersionImageResponse> => {
    let formData = new FormData();
    formData.append('file', request.file as any);

    return from<Promise<EditPackageVersionImageResponse>>(axios.patch<EditPackageVersionImageResponse>(
        `/source-code/package-version/edit-image/${request.versionId}/${request.imageToReplacePath}`, formData, {
        headers: {
            'Content-Type': `multipart/form-data`,
        }
    })
        .then(res => res.data)
    );
};

export const deletePackageVersionImage = (request: DeletePackageVersionImage): Observable<DeletePackageVersionImage> => {

    return from<Promise<DeletePackageVersionImage>>(axios.put<DeletePackageVersionImage>(
        `/source-code/package-versions/delete-image`, request
    )
        .then(res => res.data)
    );
};

export const addPackageVersionDependency = (request: AddVersionDependencyReq): Observable<IDependency[]> =>
    from<Promise<IDependency[]>>(
        axios.patch<IDependency[]>(`/source-code/package-version/add-dependency/${request.pathName}`, {dependencyPathName: request.dependencyPathName})
            .then(res => res.data)
    );
