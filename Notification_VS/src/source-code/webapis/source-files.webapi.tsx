import { APP_CFG } from '../../shared/config/app.config';
import axios from '../../shared/services/axios.service';
import { AddSourceFile, SourceFile, UpdateSrcCode } from '../interfaces/source-file';
import { SourceFileTree } from '../interfaces/source-file-tree';
import { from, Observable } from 'rxjs';

export const getSourceFilesByPackageVersionId = (id: string): Observable<SourceFile[]> =>
    from<Promise<SourceFile[]>>(
        axios.get<SourceFile[]>(`${APP_CFG.api}/source-code/source-files/${id}`)
            .then(res => res.data),
    );

export const getSourceFileTreeLevel = (pathName: string): Observable<SourceFileTree[]> =>
    from<Promise<SourceFileTree[]>>(
        axios.get<SourceFileTree[]>(`/source-code/source-files/tree/${pathName}`)
            .then(res => res.data)
    );

export const addSourceFile = (addSourceFileReq: AddSourceFile): Observable<string> => {
    return from<Promise<string>>(
        axios.post<string>('/source-code/source-files', addSourceFileReq)
            .then(res => res.data),
    );
};

export const updateSourceFileSrcCode = (updateSourceFileCode: UpdateSrcCode): Observable<string> => {
    return from<Promise<string>>(
        axios.patch<string>('/source-code/source-files', updateSourceFileCode)
            .then(res => res.data)
    );
};
