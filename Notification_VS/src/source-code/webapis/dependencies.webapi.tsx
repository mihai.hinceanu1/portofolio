import axios from '../../shared/services/axios.service';
import { IDependency } from '../interfaces/floating-menu';
import { from, Observable } from 'rxjs';

export const getDependencies = (): Observable<IDependency[]> =>
    from<Promise<IDependency[]>>(
        axios.get<IDependency[]>('/source-code/dependencies/')
            .then(res => res.data),
    );

export const getVersionDependencies = (pathName: string): Observable<IDependency[]> =>
    from<Promise<IDependency[]>>(
        axios.get<IDependency[]>(`/source-code/dependencies/${pathName}`)
            .then(res => res.data),
    );
