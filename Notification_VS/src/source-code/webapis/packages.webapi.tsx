import { APP_CFG } from '../../shared/config/app.config';
import axios from '../../shared/services/axios.service';
import { ICodePackage, NewPackageConfig, UploadPackageImageConfig } from '../interfaces/packages';
import { from, Observable } from 'rxjs';

export const getAllPackages = (): Observable<ICodePackage[]> =>
    from<Promise<ICodePackage[]>>(
        axios.get<ICodePackage[]>(`${APP_CFG.api}/source-code/packages`)
            .then(res => res.data),
    );

export const createPackage = (codePackage: NewPackageConfig): Observable<ICodePackage> =>
    from<Promise<ICodePackage>>(
        axios.post<ICodePackage>(`${APP_CFG.api}/source-code/packages`, codePackage)
            .then(res => res.data),
    );

export const editPackage = (codePackage: ICodePackage): Observable<null> =>
    from<Promise<null>>(
        axios.patch<null>(`${APP_CFG.api}/source-code/packages/${codePackage._id}`, codePackage)
            .then(res => res.data),
    );

export const deletePackage = (id: string): Observable<string> =>
    from<Promise<string>>(
        axios.delete(`${APP_CFG.api}/source-code/packages/${id}`)
            .then(res => res.data),
    );

export const uploadImage = (config: UploadPackageImageConfig): Observable<ICodePackage> => {
    let formData = new FormData();
    formData.append('file', config.image as any);

    return from<Promise<ICodePackage>>(
        axios.post<ICodePackage>(`${APP_CFG.api}/source-code/packages/add-image/${config.packageId}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(res => res.data),
    );
};
