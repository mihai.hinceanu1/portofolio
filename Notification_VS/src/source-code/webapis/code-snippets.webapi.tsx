import { Paginated } from '../../shared/interfaces/shared';
import axios from '../../shared/services/axios.service';
import { CodeSnippet } from '../interfaces/code-snippet';
import { AddCodeSnippetToSourceFile } from '../interfaces/source-code';
import { from, Observable } from 'rxjs';

/** Get an entire package of source files including all the versions */
export const getCodeSnippet = (id: string): Observable<CodeSnippet> =>
    from<Promise<CodeSnippet>>(
        axios.get<CodeSnippet>(`/source-code/code-snippets/${id}`)
            .then(res => res.data),
    );

export const editCodeSnippet = (codeSnippet: CodeSnippet): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`/source-code/code-snippets/${codeSnippet._id}`, codeSnippet)
            .then(res => res.data),
    );

// export const deleteCodeSnippet = (id: string): Observable<null> =>
//     from<Promise<null>>(
//         axios.delete(`/source-code/code-snippets/${id}`)
//             .then(res => res.data),
//     );

export const createCodeSnippet = (codeSnippet: CodeSnippet): Observable<null> =>
    from<Promise<null>>(
        axios.post<null>(`/source-code/code-snippets`, codeSnippet)
            .then(res => res.data),
    );

// ====== NEW ======

export const getSCodeSnippetsBySourceFileId = (srcFileId: string): Observable<CodeSnippet[]> =>
    from<Promise<CodeSnippet[]>>(
        axios.get(`/source-code/code-snippets/parent-source-file/${srcFileId}`)
            .then(res => res.data),
    );

export const createCodeSnippetOnSourceFile = (codeSnippet: AddCodeSnippetToSourceFile): Observable<CodeSnippet> =>
    from<Promise<CodeSnippet>>(
        axios.post<CodeSnippet>(`/source-code/source-files/code-snippets/${codeSnippet.addCodeSnippetFileId}`, codeSnippet)
            .then(res => res.data),
    );

export const updateCodeSnippet = (codeSnippet: CodeSnippet): Observable<null> =>
    from<Promise<null>>(
        axios.patch<null>(`/source-code/code-snippets`, codeSnippet)
            .then(res => res.data),
    );

export const deleteCodeSnippet = (snippetId: string): Observable<string> =>
    from<Promise<string>>(
        axios.delete(`/source-code/code-snippets/${snippetId}`)
            .then(res => res.data),
    );

// ==== // ===

export const getAllCodeSnippets = ({ page = 0, perPage = 20 }: Paginated): Observable<CodeSnippet[]> =>
    from<Promise<CodeSnippet[]>>(
        axios.get<CodeSnippet[]>(`/source-code/code-snippets/${page}/${perPage}`)
            .then(res => res.data),
    );
