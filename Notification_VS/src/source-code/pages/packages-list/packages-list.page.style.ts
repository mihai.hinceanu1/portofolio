import styled from 'styled-components/native';

// ====== PROPS ======

interface ContentProps {
    isNarrow: boolean;
}

// ====== STYLE ======

export const PackagesListPage = styled.View`
    width: 100%;
    height: 100%;
    flex-grow: 1;
`;

export const Content = styled.View<ContentProps>`
    margin-top: 60px;
    flex-direction: ${props => getFlexDirection(props)};
    margin-bottom: 50px;
`;

// ====== UTILS ======

let getFlexDirection = (props: ContentProps): string =>
    props.isNarrow ? 'column' : 'row';

export function pageTopMargin(width: number) {
    let pageHeaderOverride = {
        root: 'margin-top: 50px;'
    };

    if (width <= 1450) {
        pageHeaderOverride = {
            root: 'margin-top: 120px;'
        };
    }

    return pageHeaderOverride;
}

// ===== OVERRIDES =====

export const SCROLL_CENTER_OVERRIDES = `
    height: 100%;
    padding: 0;
`;
