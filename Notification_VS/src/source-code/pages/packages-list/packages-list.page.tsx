import * as div from './packages-list.page.style';
import { IndicatorsCard } from '../../../shared/components/indicators-card/indicators-card';
import { PageHeader } from '../../../shared/components/page-header/page-header';
import { WIDTH_S } from '../../../shared/constants/adaptive.const';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { sortArray } from '../../../shared/services/utils.service';
import * as scroll from '../../../shared/style/scroll.style';
import { AddPackageModal } from '../../components/add-package-modal/add-package-modal';
import { PackageListHoverMenus } from '../../components/package-hover-menus/package-list-hover-menus';
import { List } from '../../components/packages-list/list';
import { ICodePackage } from '../../interfaces/packages';
import * as packageListService from '../../services/packages-list.service';
import { getPackagesListHeaderConfig } from '../../services/packages-list.utils';
import { getAllPackages, packages$ } from '../../services/packages.service';
import React from 'react';
import { LayoutChangeEvent } from 'react-native';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props { }
interface State {
    width: number;
    searchTerm: string;
    packages: ICodePackage[];
}

/**
 * Page for a content creator.
 * Shows the user's code packages along a small overview about
 * the milestones the packages achieved.
 */
export class PackagesListPage extends React.Component<Props, State> {
    private modal: JSX.Element;
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            width: null,
            searchTerm: '',
            packages: null,
        };
    }

    public render() {
        const { width, packages } = this.state;
        let isNarrow = width <= WIDTH_S;

        return (
            <div.PackagesListPage data-cy='packages-list-page' onLayout={(e) => this.updateWidth(e)}>

                {/** ScrollView */}
                <scroll.ScrollView keyboardShouldPersistTaps='handled'
                    data-cy='scroll-view'
                    contentContainerStyle={{ alignItems: 'center' }}>

                    {/** Package List Menus */}
                    <PackageListHoverMenus />

                    {/** Center */}
                    <scroll.Center data-cy='center'
                        width={width}
                        override={div.SCROLL_CENTER_OVERRIDES}>

                        {/** Page header */}
                        <PageHeader config={this.getPageHeaderConfig()} overrides={div.pageTopMargin(width)} />

                        {/** Content */}
                        <div.Content data-cy='content' isNarrow={isNarrow}>

                            {/** Overview */}
                            {
                                !!packages &&
                                <IndicatorsCard title='Overview'
                                    isNarrow={isNarrow}
                                    indicators={packageListService.getPackageListOverview(packages)} />
                            }

                            {/** Packages List */}
                            {
                                !!packages &&
                                <List packages={packages} isNarrow={isNarrow} />
                            }

                        </div.Content>
                    </scroll.Center>

                </scroll.ScrollView>

            </div.PackagesListPage>
        );
    }

    public componentDidMount() {
        this.subscribeToPackages();

        getAllPackages();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToPackages() {
        packages$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe((packages) => {
                const sortedPackages = sortArray<ICodePackage>(packages, 'created', false);

                this.setState({ packages: sortedPackages });
            });
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private handleSearchBarChange(text: string) {
        this.setState({
            searchTerm: text,
        });
    }

    private clearSearch() {
        this.setState({
            searchTerm: '',
        });
    }

    private orderPackagesByName() {
        const { packages } = this.state;

        let sorted = sortArray<ICodePackage>(packages, 'name', true);

        this.setState({ packages: sorted });
    }

    private orderPackagesByDate() {
        const { packages } = this.state;

        let sortedPackages = sortArray<ICodePackage>(packages, 'created', false);

        this.setState({ packages: sortedPackages });
    }

    private getPageHeaderConfig() {
        const { searchTerm } = this.state;

        return getPackagesListHeaderConfig(
            () => this.openAddPackageModal(),
            (text: string) => this.handleSearchBarChange(text),
            searchTerm,
            () => console.log(searchTerm),
            () => this.clearSearch(),
            () => this.orderPackagesByName(),
            () => this.orderPackagesByDate(),
        );
    }

    private openAddPackageModal() {
        this.modal = <AddPackageModal closeModal={() => this.closeAddPackageModal()} />;
        addAppOverlay(this.modal);
    }

    private closeAddPackageModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }
}
