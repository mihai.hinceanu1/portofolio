/// <reference types="cypress" />

describe('Packages List Page Tests', () => {
    before('Login', () => {
        cy.login();
    });

    it('Open Packages List Page', () => {
        cy.visit('/admin/packages-list');
        cy.url().should('include', 'packages-list');
    });

    it('Indicators Card is Rendered', () => {
        cy.get('[data-cy=indicators-card]').should('exist');
    });

    it('Indicators Card Displays The Correct Number of Packages', () => {
        cy.get('[data-cy=code-package]').its('length').then(length => {
            getEl('indicators-card', 'value').eq(0).should(el => {
                expect(el.text()).to.eq(length.toString());
            });
        });
    });

    it('Page Header Is Rendered', () => {
        cy.get('[data-cy=page-header]').should('exist');
    });

    it('New Package Button Opens Modal', () => {
        getEl('page-header', 'button').click();
        cy.get('[data-cy=add-package-modal]').should('exist');
    });

    it('Add Package Works', () => {
        cy.server();
        cy.fixture('add-package.json').as('package');
        cy.route({
            method: 'POST',
            url: '/api/source-code/packages',
            status: 200,
            response: '@package',
        }).as('addPackage');

        /**
         * There's no need to complete these as the request is stubbed,
         * they're here just so we have a general idea about the data,
         * and for the sake of pretending.
         */
        cy.get('[data-cy=input]').eq(0).type('TestPackage');
        cy.get('[data-cy=textarea-box]').type('TestDescription');

        cy.get('[data-cy=button]').eq(1).click();

        cy.wait('@addPackage');
    });

    it('Check Title For Last Added Package', () => {
        const wrapper = getEl('list', 'code-package').first();

        wrapper.within(() => {
            cy.get('[data-cy=name]').should(el => {
                expect(el.text()).to.eq('TestPackage');
            });
        });
    });

    it('Check Description For Last Added Package', () => {
        const wrapper = getEl('list', 'code-package').first();

        wrapper.within(() => {
            cy.get('[data-cy=description]').should(el => {
                expect(el.text()).to.eq('TestDescription');
            });
        });
    });

    it('A modal opens for editing a package', () => {
        getEl('code-package', 'dropdown-button').first().click();

        /** Select edit option from the dropdown */
        getEl('dropdown-options', 'option')
            .first()
            .should('have.text', 'Edit details');

        getEl('dropdown-options', 'option')
            .first()
            .click();

        /** Check if the modal opens */
        getEl('modal', 'edit-package').should('exist');
    });

    it('When editing the newly created package, the data updates correctly', () => {
        /** Mock */
        cy.fixture('edit-package.json').as('package');

        /** Define request */
        cy.server();
        cy.route({
            method: 'PATCH',
            url: '/api/source-code/packages/*',
            status: 200,
            response: '@package',
        }).as('editPackage');

        /**
         * There's no need to complete these as the request is stubbed,
         * they're here just so we have a general idea about the data,
         * and for the sake of pretending.
         */
        getElByTag('modal', 'input').type(' edited');
        getElByTag('modal', 'textarea').type(' edited');

        /** Click on update */
        cy.get('[data-cy=button]').eq(1).click();

        /** Wait for response */
        cy.wait('@editPackage');

        /** Check if the modal closes after */
        getEl('modal', 'edit-package').should('not.exist');

        /** Get the updated package */
        const wrapper = getEl('list', 'code-package').first();

        /** Check if name and description update */
        wrapper.within(() => {
            cy.get('[data-cy=name]').should(el => {
                expect(el.text()).to.eq('TestPackage edited');
            });

            cy.get('[data-cy=description]').should(el => {
                expect(el.text()).to.eq('TestDescription edited');
            });
        });
    });

    it('A modal opens when deleting a package for confirmation', () => {
        /** Check that we target the right package */
        getEl('code-package', 'name').first().should('have.text', 'TestPackage edited');

        getEl('code-package', 'dropdown-button').first().click();

        /** Select the "Delete" option from the dropdown */
        getEl('dropdown-options', 'option')
            .last()
            .should('have.text', 'Delete');

        getEl('dropdown-options', 'option')
            .last()
            .click();

        /** Check if the modal opens */
        getEl('modal', 'delete-package').should('exist');
    });

    it('A confirmation message appears inside the modal', () => {
        getEl('delete-package', 'confirmation-message').should('exist');
        getEl('delete-package', 'package-name').should('exist');

        /** Check that the right package name is displayed */
        getEl('delete-package', 'package-name').should('have.text', 'TestPackage edited');
    });

    it('After being deleted, a package will not be displayed anymore', () => {
        /** Define request */
        cy.server();
        cy.route({
            method: 'DELETE',
            url: '/api/source-code/packages/*',
            status: 200,
            response: '5ec27800890ab73378de15b6',
        }).as('deletePackage');

        /** Confirm deletion */
        cy.get('[data-cy=button]').eq(1).click();

        /** Wait for response */
        cy.wait('@deletePackage');

        /** Check if the modal closes after */
        getEl('modal', 'delete-package').should('not.exist');

        /** Check that the package doesn't exist anymore */
        getEl('code-package', 'name').each((codePackage) => {
            expect(codePackage).not.to.have.text('TestPackage edited');
        });
    });
});

// ====== SELECTORS ======

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

function getElByTag(wrapper, tag) {
    return cy.get(`[data-cy=${wrapper}] ${tag}`);
}