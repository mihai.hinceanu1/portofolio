/// <reference types="cypress" />

describe('Package versions page tests', () => {
    it('open package versions page', () => {
        openPackageVersionsPage();
    });

    it('Package Versions Cards Are Rendered', () => {
        cy.get('[data-cy=code-version-card]').should('exist');
    });

    it('Version Title is Rendered', () => {
        getElmnt('code-version-card', 'version-title').should('exist');
    });

    it('Additional Description is Rendered', () => {
        getElmnt('code-version-card', 'additional-description').should('exist');
    });

    it('Stage Cards are Rendered', () => {
        getElmnt('code-version-card', 'stage-card').should('exist');
    });

    it('Timeline Bar is Rendered', () => {
        getElmnt('code-version-card', 'timeline-bar').should('exist');
    });

    it('Preview and Edit buttons are rendered', () => {
        getElmnt('code-version-card', 'buttons').should('exist');
    });

    it('Version Navbar Is Rendered', () => {
        getElmnt('code-version-card', 'version-navbar').should('exist');
    });

    it('Version Preview Image Is Rendered', () => {
        getElmnt('code-version-card', 'version-preview-image').should('exist');
    });

    it('Add Code Version Button Opens Modal', () => {
        cy.get('[data-cy=button]').last().click();
        cy.get('[data-cy=modal]').should('exist');
    });

    it('Add A Package Version', () => {
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/source-code/package-versions',
            response: {
                packageVersion: {
                    _id: '5ec796965c5b11e691556bdf',
                    pathName: 'testpackage11',
                    name: 'TestPackage',
                    description: 'TestPackageDescription',
                    packageId: '5ea71ee172d634ee43e8b628',
                    images: [],
                    files: [],
                },
                parentPathName: '',
            },
            status: 200,
        }).as('postPackage');

        cy.get('[data-cy=input]').eq(0).type('TestPackage');
        cy.get('[data-cy=description-input]').type('TestPackageDescription');
        cy.get('[data-cy=button]').eq(1).click({ force: true });

        cy.wait(['@postPackage']);

    });

    it('Last Package Added Has Correct Data', () => {

        const wrapper = cy.get('[data-cy=code-version-card]').first();
        wrapper.within(() => {
            cy.get('[data-cy=version-title] [data-cy=title]').then(div => {
                expect(div.text()).to.eq('TestPackage');
            });

            cy.get('[data-cy=version-title] [data-cy=description]').then(div => {
                expect(div.text()).to.eq('TestPackageDescription');
            });
        });
    });

    it('Update Package Version Modal Opens', () => {
        const wrapper = cy.get('[data-cy=code-version-card]').first();
        wrapper.within(() => {
            cy.get('[data-cy=button]').eq(0).click();
        });

        cy.get('[data-cy=update-package-version-form]').should('exist');
    });

    it('Update Package Version Works', () => {
        cy.server();
        cy.route({
            method: 'PATCH',
            url: '/api/source-code/package-versions/*',
            response: {
                _id: '5ec796965c5b11e691556bdf',
                pathName: 'testpackage11',
                name: 'TestPackage1',
                description: 'TestPackageDescription1',
                packageId: '5ea71ee172d634ee43e8b628',
                images: [],
                files: [],
            }
        }).as('updatePackageVersion');

        const wrapper = cy.get('[data-cy=update-package-version-form]');
        wrapper.within(() => {
            cy.get('[data-cy=input]').eq(0).type('1');
            cy.get('[data-cy=description-input]').type('1');
            cy.get('[data-cy=button]').eq(1).click();
        });

        cy.wait(['@updatePackageVersion']);

    });

    it('Updated Package Has Correct Data', () => {
        const wrapper = cy.get('[data-cy=code-version-card]').first();
        wrapper.within(() => {
            cy.get('[data-cy=version-title] [data-cy=title]').then(div => {
                expect(div.text()).to.eq('TestPackage1');
            });

            cy.get('[data-cy=version-title] [data-cy=description]').then(div => {
                expect(div.text()).to.eq('TestPackageDescription1');
            });
        });
    });

    it('Delete Package Modal Opens', () => {
        const wrapper = cy.get('[data-cy=code-version-card]').first();
        wrapper.within(() => {
            cy.get('[data-cy=button]').eq(3).click();
        });

        cy.get('[data-cy=delete-package-version-form]').should('exist');
    });

    it('Delete Package', () => {
        cy.server();
        cy.route({
            method: 'delete',
            url: '/api/source-code/package-versions/*/*',
            response: '5ec796965c5b11e691556bdf',
            status: 200,
        }).as('delete');

        cy.get('[data-cy=button]').eq(1).click();

        cy.wait('@delete').then(xhr => expect(xhr.status).to.eq(200));
    });

    it('Create a new empty package', () => {
        openPackagePage();

        cy.server();
        cy.route({
            method: 'POST',
            url: `/api/source-code/packages`,
            response: {
                pathName: 'testpackage',
                name: 'TestPackage',
                description: 'TestPackageDescription',
                image: '',
                awards: 1,
                reactions: 1,
                milestones: '',
                comment: 2,
                newComments: 2,
                codeLines: 12,
                technologyStack: [],
                deprecated: false,
            },
            status: 200,
        }).as('createPackage');
        const wrapper = cy.get('[data-cy=add-package-form]');

        wrapper.within(() => {
            cy.get('[data-cy=input]').type('TestPackage');
            cy.get('[data-cy=textarea-box]').type('TestPackageDescription');
            cy.get('[data-cy=button]').eq(1).click();
        });

        cy.wait('@createPackage');
    });

    it('Open Newly create package', () => {
        cy.get('[data-cy=code-package]').last().click();
    });

    it('Create New Version In Empty Package', () => {
        cy.get('[data-cy=button]').last().click();
        cy.server();
        cy.route({
            method: 'POST',
            url: '/api/source-code/package-versions',
            response: {
                packageVersion: {
                    _id: '5ec796965c5b11e691556bdf',
                    pathName: 'testpackage11',
                    name: 'TestPackage',
                    description: 'TestPackageDescription',
                    packageId: '5ea71ee172d634ee43e8b628',
                    images: [],
                    files: [],
                },
                parentPathName: '',
            },
            status: 200,
        }).as('postPackage');

        const wrapper = cy.get('[data-cy=modal]');

        wrapper.within(() => {
            cy.get('[data-cy=input]').eq(0).type('TestPackage');
            cy.get('[data-cy=description-input]').type('TestPackageDescription');
            cy.get('[data-cy=button]').eq(1).click({ force: true });
        });

        cy.wait(['@postPackage']);
    });
});

function openPackageVersionsPage() {
    cy.visit('http://localhost:3000');
    cy.get('[data-cy=input]').eq(0).type('admin');
    cy.get('[data-cy=input]').eq(1).type('admin');
    cy.get('[data-cy=button]').click();
    cy.get('[data-cy=page-button]').eq(6).click();
    cy.get('[data-cy=code-package]').eq(1).click();
}

function openPackagePage() {
    cy.visit('http://localhost:3000');
    cy.get('[data-cy=input]').eq(0).type('admin');
    cy.get('[data-cy=input]').eq(1).type('admin');
    cy.get('[data-cy=button]').click();
    cy.get('[data-cy=page-button]').eq(6).click();
    cy.get('[data-cy=button]').eq(0).click();
}
// ====== SELECTORS ======

function getElmnt(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}