import { colors } from '../../../shared/style/colors';
import { STAGE_CARD_WIDTH } from '../../components/stage-card/stage-card.style';
import styled from 'styled-components/native';

interface ContentProps {
    isNarrow: boolean;
}

export const BottomButton = styled.View<ContentProps>`
    margin-top: 50px;
    padding-bottom: 150px;
    justify-content: center;
    align-items: center;
    align-self: center;
    ${props => !props.isNarrow && `margin-right: -${STAGE_CARD_WIDTH}px;`}
`;

export const BottomButtonText = styled.Text`
    width: 170px;
    text-align: center;
    color: ${colors.$darkGrey};
    font-size: 11px;
`;

export const HeaderPlaceholder = styled.View`
    margin-top: 60px;
    margin-bottom: 30px;
    height: 60px;
    width: 100%;
    background-color: black;

`;

export const Content = styled.View<ContentProps>`
    margin-top: 20px;
    align-items: center;
    justify-content: center;
`;

// ===== OVERRIDES =====
export const SCROLL_CENTER_OVERRIDES = `
    height: 100%;
    padding: 0;
`;
