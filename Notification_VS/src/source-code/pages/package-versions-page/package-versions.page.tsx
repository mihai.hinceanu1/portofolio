import {
    getPackagesVersionListHeaderConfig,
    getPackageVersionsCfg,
    getPageHeaderOverride,
    getVersionPackagePageBtnCfg
    } from './package-versions-page.utils';
import * as div from './package-versions.page.style';
import { Button } from '../../../shared/components/button/button';
import { PageHeader } from '../../../shared/components/page-header/page-header';
import { WIDTH_M } from '../../../shared/constants/adaptive.const';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import * as scroll from '../../../shared/style/scroll.style';
import { AddPackageVersionModal } from '../../components/add-package-version-modal/add-package-version-modal';
import { CodeVersionCard } from '../../components/code-version-card/code-version-card';
import { CodeVersionHoverMenus } from '../../components/code-version-hover-menus/code-version-hover-menus';
import { PackageVersion } from '../../interfaces/package-version';
import { getAllPackageVersions, packageVersions$ } from '../../services/package-version.service';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Params {
    pathName: string;
}
interface Props extends RouteComponentProps<Params> { }

interface State {
    packageVersions: PackageVersion[];
    width: number;
    searchTerm: string;
}

/**
 * Page used to render the package versions that are located
 * inside a specific code package.
 */
export class _PackageVersionsPage extends React.Component<Props, State> {

    public destroyed$ = new Subject<void>();

    private modal: JSX.Element;

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            packageVersions: null,
            searchTerm: '',
        };
    }

    public render() {
        let { width, packageVersions } = this.state;
        let isNarrow = width <= WIDTH_M;
        let isNarrowPageHeader = width < 1150;

        return (
            <scroll.ScrollView data-cy='packages-version-home-page'
                contentContainerStyle={scroll.inner.content}
                onLayout={e => this.updateWidth(e)}>

                {/* Code Version Hover Menus */}
                <CodeVersionHoverMenus />

                <scroll.Center data-cy='center' width={width} override={div.SCROLL_CENTER_OVERRIDES}>

                    {/* Page Header */}
                    <PageHeader config={this.getPageHeaderConfig()} overrides={{ root: getPageHeaderOverride(isNarrowPageHeader) }} />

                    <div.Content data-cy='content' isNarrow={isNarrow}>

                        {/* Page Body */}
                        {
                            !!packageVersions &&
                            getPackageVersionsCfg(packageVersions).map((codeVersionCard, index) => <CodeVersionCard key={index} codeVersionCardCfg={codeVersionCard} isNarrow={isNarrow} />)
                        }

                        {/* Bottom Button*/}
                        <div.BottomButton data-cy='bottom-button' isNarrow={isNarrow}>
                            <Button config={getVersionPackagePageBtnCfg(this.showModal.bind(this))} overrides={{ root: `align-self:center;` }} />

                            <div.BottomButtonText data-cy='bottom-button-text'>
                                Package versions indicate major milestones
                                in the project
                        </div.BottomButtonText>
                        </div.BottomButton>

                    </div.Content>

                </scroll.Center>

            </scroll.ScrollView>
        );
    }

    public componentDidMount() {
        const { pathName } = this.props.match.params;
        this.subscribeToPackageVersions();
        getAllPackageVersions(pathName);
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private showModal() {
        const { pathName } = this.props.match.params;

        this.modal = <AddPackageVersionModal callback={() => this.hideModal()} packagePathName={pathName} />;
        addAppOverlay(this.modal);
    }

    private handleSearchBarChange(text: string) {
        this.setState({
            searchTerm: text,
        });
    }

    private clearSearch() {
        this.setState({
            searchTerm: '',
        });
    }

    private getPageHeaderConfig() {
        const { searchTerm } = this.state;

        return getPackagesVersionListHeaderConfig(
            () => this.showModal(),
            (text: string) => this.handleSearchBarChange(text),
            searchTerm,
            () => console.log(searchTerm),
            () => this.clearSearch(),
        );
    }

    private subscribeToPackageVersions() {
        packageVersions$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(packageVersions => {
                this.setState({ packageVersions });
            });
    }

    private hideModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }
}

export const PackageVersionsPage = withRouter<Props, any>(_PackageVersionsPage);