import { FILE, JAVA, NEW_VERSION } from '../../../shared/assets/icons';
import { ButtonConfig } from '../../../shared/interfaces/button';
import { HeaderConfig } from '../../../shared/interfaces/page-header';
import { IStatsBar } from '../../../shared/interfaces/stats-bar';
import { colors } from '../../../shared/style/colors';
import { stageCardFirstIteration, stageCardSecondIteration } from '../../components/code-version-card/code-version-card.utils';
import { timelineBarDefault, timelineBarLinked } from '../../components/timeline-bar/_docs/timeline-bar.page.utils';
import { customTitle } from '../../components/version-preview-image/_docs/version-preview-image-page.utils';
import { CodeVersionCardCfg } from '../../interfaces/code-version-card';
import { FramedImageCfg } from '../../interfaces/framed-image';
import { PackageVersion } from '../../interfaces/package-version';

/**
 * Build the CodeVersionCards config
 * it receives as param an Array of package versions
 * from which it extracts only the necessary data and
 * return it back to its parent page
 */
export const getPackageVersionsCfg = (packageVersions: PackageVersion[]): CodeVersionCardCfg[] => {
    let config: CodeVersionCardCfg[] = [];

    packageVersions.forEach((packageVersion, index) => {
        config[index] = {
            stageCards: [stageCardFirstIteration, stageCardSecondIteration],
            timelineBarCfg: index === 0 ? timelineBarDefault : timelineBarLinked,
            codeVersionCardBodyCfg: {
                packageId: packageVersion.packageId,
                packageVersionId: packageVersion._id,
                pathName: packageVersion.pathName,
                versionTitleCfg: {
                    title: packageVersion.name,
                    isLocked: false,
                    isLast: index === packageVersions.length,
                    description: packageVersion.description,
                    noOfVersion: index,

                },
                additionalDescription: 'This description was added in order to see if the cards extends properly and the ratio looks ok.This description was added in order to see if the cards extends properly and the ratio looks ok.This description was added in order to see if the cards extends properly and the ratio looks ok.This description was added in order to see if the cards extends properly and the ratio looks ok.',
                versionNavbarCfg: null,
                statsBarCfg: STATS_BAR_CFG,
                framedImagesCfg: packageVersion.images.map(image => {
                    return {
                        imagePath: image,
                        fileTypeSvgFill: 'white',
                        editButton: true,
                        fileTypeSvg: FILE,
                        title: {
                            name: 'Get Started',
                            iconPath: JAVA,
                        }
                    };
                }),
                versionPreviewImagesCfg: [customTitle, customTitle, customTitle]
            },
        };
    });

    return config;
};

export const FramedImages: FramedImageCfg[] = [
    {
        imagePath: '/auth/eniacWithConfetti.png',
        fileTypeSvgFill: 'white',
        fileTypeSvg: FILE,
        title: {
            name: 'Get Started',
            iconPath: JAVA,
        }
    },
    {
        imagePath: '/auth/eniacWithConfetti.png',
        fileTypeSvgFill: 'white',
        fileTypeSvg: FILE,
        title: {
            name: 'Get Started',
            iconPath: JAVA,
        }
    },
    {
        imagePath: '/auth/eniacWithConfetti.png',
        fileTypeSvgFill: 'white',
        fileTypeSvg: FILE,
        title: {
            name: 'Get Started',
            iconPath: JAVA,
        }
    }
];

export const STATS_BAR_CFG: IStatsBar = {
    awards: 10,
    reactions: 20,
    comments: 100,
    codeLines: 350,
    disabled: false,
    milestone: null,
    color: colors.$blue,
    techStack: ['angular', 'react'],
};

export const getVersionPackagePageBtnCfg = (onPress: () => void): ButtonConfig => {
    return ({
        text: ' Add',
        fontSize: 20,
        filled: true,
        icon: {
            type: 'svg',
            svgPath: NEW_VERSION,
        },
        rounded: true,
        width: 230,
        color: colors.$darkBlue,
        onPress: () => onPress(),
    });
};

export const getPageHeaderOverride = (isNarrow: boolean) => {
    return isNarrow ? 'margin-top: 125px;' : `margin-top: 50px;`;
};

export const getPackagesVersionListHeaderConfig = (
    btnCallback: () => void,
    onChange: (text: string) => void,
    searchTerm: string,
    onSubmit: () => void,
    clearSearch: () => void,
): HeaderConfig => {

    let config: HeaderConfig = {
        title: 'Dashboard Code Version',
        button: {
            onPress: btnCallback,
            text: 'New Version'.toUpperCase(),
            icon: {
                type: 'svg',
                svgPath: FILE,
            }
        },
        searchBar: {
            onChange: onChange,
            value: searchTerm,
            onSubmit: onSubmit,
            clearSearch: clearSearch,
        }
    };

    return config;
};