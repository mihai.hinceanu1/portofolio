import styled from 'styled-components/native';

export const CodeEditorPage = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
    height: 100%;
    width: 100%;
`;

// ====== UTILS ======

export function pageTopMargin(width: number) {
    let pageHeaderOverride = {
        root: 'margin-top: 50px;'
    };

    if (width <= 1550) {
        pageHeaderOverride = {
            root: 'margin-top: 120px;'
        };
    }

    return pageHeaderOverride;
}

// ====== CONST =======

export const FLOATING_MENU_OVERRIDES = {
    root: `
        position: absolute;
        left: 60px;
        top: 200px;
    `
};