import { FILE } from '../../../../shared/assets/icons';
import { DROPDOWN_TYPE } from '../../../../shared/constants/dropdown.const';
import { DropdownConfig, IDropdownOption } from '../../../../shared/interfaces/dropdown';
import { Filter, HeaderConfig } from '../../../../shared/interfaces/page-header';
import { floatingMenuConfig } from '../../../components/floating-menu/floating-menu.utils';
import { FLOATING_MENU_TABS, FloatingMenuBodyCfg, FloatingMenuCfg } from '../../../interfaces/floating-menu';

export const getCodeEditorHeaderConfig = (
    btnCallback: () => void,
    onChange: (text: string) => void,
    searchTerm: string,
    onSubmit: () => void,
    clearSearch: () => void,
): HeaderConfig => {

    let config: HeaderConfig = {
        title: 'Code Editor',
        button: {
            onPress: btnCallback,
            text: 'New File'.toUpperCase(),
            icon: {
                type: 'svg',
                svgPath: FILE,
            }
        },
        searchBar: {
            onChange: onChange,
            value: searchTerm,
            onSubmit: onSubmit,
            clearSearch: clearSearch,
        },
        filterBar: {
            filters: [{
                _id: '1',
                label: 'Date',
                callback: () => console.log('Date')
            }, {
                _id: '2',
                label: 'Name',
                callback: () => console.log('Names'),
            }],
            selectFilter: (filter: Filter) => (console.log(filter)),
        },
    };

    return config;
};

export function getFloatingMenuConfig(addDependency?: () => void): FloatingMenuCfg {
    let body: FloatingMenuBodyCfg = {
        ...floatingMenuConfig.body,
    };
    let config: FloatingMenuCfg = {
        body,
        tabs: [...floatingMenuConfig.tabs],
        addDependency,
    };

    return config;
}

/**
 * Dropdown replacing the floating menu navigation tabs
 * on narrow screens.
 */
export function getDropdownConfig(
    callback: (item: IDropdownOption) => void,
    selectedOption: IDropdownOption,
    // openOverlay: (option: IDropdownOption) => void
): DropdownConfig {
    let config: DropdownConfig = {
        options: [
            {
                _id: 'dependencies',
                label: FLOATING_MENU_TABS.DEPENDENCIES,
                // callback: (option) => openOverlay(option)
            },
            {
                _id: 'snippets',
                label: FLOATING_MENU_TABS.SNIPPETS,
                // callback: (option) => openOverlay(option)
            },
            {
                _id: 'code-flows',
                label: FLOATING_MENU_TABS.FLOWS,
                // callback: (option) => openOverlay(option)
            },
            {
                _id: 'revisions',
                label: FLOATING_MENU_TABS.REVISIONS,
                // callback: (option) => openOverlay(option)
            },
            {
                _id: 'pseudocode',
                label: FLOATING_MENU_TABS.PSEUDOCODE,
            },
            {
                _id: 'annotations',
                label: FLOATING_MENU_TABS.ANNOTATIONS,
            },
            {
                _id: 'showcase',
                label: FLOATING_MENU_TABS.SHOWCASE,
            }
        ],
        type: DROPDOWN_TYPE.Standard,
        callback,
        selected: selectedOption,
    };

    return config;
}
