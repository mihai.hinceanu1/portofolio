import * as utils from './_utils/code-editor-page.utils';
import * as div from './code-editor-page.style';
import { AdminNavigator } from '../../../admin/components/admin-navigator/admin-navigator';
import { Dropdown } from '../../../shared/components/dropdown/dropdown';
import { PageHeader } from '../../../shared/components/page-header/page-header';
import { DropdownConfig, IDropdownOption } from '../../../shared/interfaces/dropdown';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import * as scroll from '../../../shared/style/scroll.style';
import { AddDependencyModal } from '../../components/add-dependency-modal/add-dependency-modal';
import { CodeEditorHoverMenus } from '../../components/code-editor-hover-menus/code-editor-hover-menus';
import { CodeEditorIde } from '../../components/code-editor-ide/code-editor-ide';
import { FloatingMenuMobileBody } from '../../components/floating-menu/components/floating-menu-body/mobile-body';
import { FloatingMenu } from '../../components/floating-menu/floating-menu';
import { FloatingMenuCfg } from '../../interfaces/floating-menu';
import { showAdminNavigator$ } from '../../services/source-code-common.service';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Params {
    pathName: string;
}
interface Props extends RouteComponentProps<Params> { }

interface State {
    width: number;
    isAdminNavigatorVisible: boolean;
    searchTerm: string;
    option: IDropdownOption;
}

export class _CodeEditorPage extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();
    private modal: JSX.Element;
    private floatingMenu: JSX.Element;

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            isAdminNavigatorVisible: false,
            searchTerm: '',
            option: null,
        };
    }

    public render() {
        const { width, isAdminNavigatorVisible } = this.state;

        return (
            <div.CodeEditorPage data-cy='code-editor-page'
                onLayout={e => this.updateWidth(e)}>

                <scroll.ScrollView data-cy='scroll-view'
                    nativeID='scroll-view'
                    contentContainerStyle={scroll.inner.content}>

                    {/** Admin Navigator */}
                    {
                        isAdminNavigatorVisible &&
                        <AdminNavigator width={width} />
                    }

                    {/** Code Editor Hover Menus */}
                    <CodeEditorHoverMenus />

                    <scroll.Center width={width}>

                        {/** Page Header */}
                        <PageHeader config={this.getPageHeaderConfig()} overrides={div.pageTopMargin(width)} />

                        {
                            !this.floatingMenu &&
                            <Dropdown config={this.getDropdownConfig()} />
                        }

                        {/** Code Editor IDE */}
                        <CodeEditorIde />

                    </scroll.Center>

                </scroll.ScrollView>

            </div.CodeEditorPage>
        );
    }

    public componentDidMount() {
        this.subscribeToShowAdminNavigator();

        const { width } = this.state;

        if (width >= 1800 && !this.floatingMenu) {
            this.showFloatingMenu();
        }
    }

    public componentDidUpdate() {
        const { width } = this.state;

        /** If it's already mounted and the width shrinks remove floating menu */
        if (width < 1800 && this.floatingMenu) {
            this.hideFloatingMenu();
        }

        /** If it's not mounted and the width expands, add it. */
        if (width >= 1750 && !this.floatingMenu) {
            this.showFloatingMenu();
        }
    }

    public componentWillUnmount() {
        this.destroyed$.next();
        if (this.floatingMenu) {
            this.hideFloatingMenu();
        }
    }

    // ====== SUBSCRIBES ======

    private subscribeToShowAdminNavigator() {
        showAdminNavigator$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isVisible => {
                this.setState({ isAdminNavigatorVisible: isVisible });
            });
    }

    // ====== UPDATE STATE ======

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private clearSearch() {
        this.setState({
            searchTerm: '',
        });
    }

    private handleSearchBarChange(text: string) {
        this.setState({
            searchTerm: text,
        });
    }

    private selectDropdownOption(option: IDropdownOption) {
        this.setState({
            option,
        }, () => {
            const { pathName } = this.props.match.params;
            console.log(option.label);
            this.modal = <FloatingMenuMobileBody pressedTab={option}
                body={this.getFloatingMenuConfig().body}
                addDependency={() => { }}
                pathName={pathName} />;

            addAppOverlay(this.modal);
        });
    }

    // ====== UTILS ======

    private showFloatingMenu() {
        const { pathName } = this.props.match.params;
        this.floatingMenu = <FloatingMenu pathName={pathName}
            floatingMenuCfg={this.getFloatingMenuConfig()}
            overrides={div.FLOATING_MENU_OVERRIDES} />;

        addAppOverlay(this.floatingMenu);
    }

    private hideFloatingMenu() {
        removeAppOverlay(this.floatingMenu);
        delete this.floatingMenu;
    }

    private showAddDependencyModal() {
        const { pathName } = this.props.match.params;
        this.modal = <AddDependencyModal closeModal={() => this.closeAddDependencyModal()} pathName={pathName} />;

        addAppOverlay(this.modal);
    }

    private closeAddDependencyModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }

    // ====== CONFIGS ======

    private getDropdownConfig(): DropdownConfig {
        const { option } = this.state;

        return utils.getDropdownConfig(
            (option) => this.selectDropdownOption(option),
            option,
        );
    }

    private getFloatingMenuConfig(): FloatingMenuCfg {
        return utils.getFloatingMenuConfig(() => this.showAddDependencyModal());
    }

    private getPageHeaderConfig() {
        const { searchTerm } = this.state;

        return utils.getCodeEditorHeaderConfig(
            () => console.log('first'),
            (text: string) => this.handleSearchBarChange(text),
            searchTerm,
            () => console.log(searchTerm),
            () => this.clearSearch(),
        );
    }
}

export const CodeEditorPage = withRouter<Props, any>(_CodeEditorPage);
