// Style elements

// Spacing ( indentation ) of the two consecutive parent-child related menu items; in px
export const INDENTATION_STEP = 15;