import { NEW_PACKAGE } from '../../shared/assets/icons';
import { Filter, HeaderConfig } from '../../shared/interfaces/page-header';
import { colors } from '../../shared/style/colors';
import { Technology } from '../interfaces/packages';

export enum MILESTONES {
    Package = 'PACKAGE',
    Editors_Choice = 'EDITOR\'S CHOICE',
    Highest_Rated = 'HIGHEST RATED',
    Bestseller = 'BESTSELLER',
}

/**
 * Returns specific color for some elements based on the milestone the
 * package has reached.
 */
export const getPackageElementsColor = (milestone: MILESTONES, disabled?: boolean, color?: string): string => {
    let resultColor: string = colors.$grey;

    if (disabled) {
        return resultColor;
    }

    if (color) {
        return color;
    }

    if (milestone === MILESTONES.Highest_Rated) {
        resultColor = colors.$green;
    }

    if (milestone === MILESTONES.Editors_Choice) {
        resultColor = colors.$blue;
    }

    if (milestone === MILESTONES.Bestseller) {
        resultColor = colors.$black;
    }

    return resultColor;
};

/** Generates the header config for packages list page */
export const getPackagesListHeaderConfig = (
    btnCallback: () => void,
    onChange: (text: string) => void,
    searchTerm: string,
    onSubmit: () => void,
    clearSearch: () => void,
    orderPackagesByName: () => void,
    orderPackagesByDate: () => void,
): HeaderConfig => {

    let config: HeaderConfig = {
        title: 'Dashboard Code Package',
        button: {
            onPress: btnCallback,
            text: 'New Package'.toUpperCase(),
            icon: {
                type: 'svg',
                svgPath: NEW_PACKAGE,
            }
        },
        searchBar: {
            onChange: onChange,
            value: searchTerm,
            onSubmit: onSubmit,
            clearSearch: clearSearch,
        },
        filterBar: {
            filters: [{
                _id: '1',
                label: 'Date',
                callback: () => console.log('Date')
            }, {
                _id: '2',
                label: 'Name',
                callback: () => orderPackagesByName(),
            }],
            selectFilter: (filter: Filter) => { filter.label === 'Name' && orderPackagesByName(); filter.label === 'Date' && orderPackagesByDate(); },
        },
    };

    return config;
};

/**
 * <!>Temporary
 *
 * Converts technology stack from ICodePackage into a string
 * array, representing the technology stack used in stats-bar
 */
export function getTechStack(codePackageTechStack: Technology[]): string[] {
    let techStack: string[] = [];

    techStack = codePackageTechStack.map(tech => tech.name.toLocaleLowerCase());

    return techStack;
}