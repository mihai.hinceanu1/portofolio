import { store, store$ } from '../../shared/services/state.service';
import {
    AddPackageVersionImage,
    AddVersionDependencyReq,
    CreatePackageVersionCfg,
    DeletePackageVersionImage,
    EditPackageVersionImage,
    PackageVersion,
    UpdatePackageVersion
    } from '../interfaces/package-version';
import { DeletePackageVersionConfig } from '../interfaces/source-code';
import * as actions from '../state/actions/package-versions.actions';
import * as sel from '../state/selectors/package-versions.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== GET ALL PACKAGE VERSIONS BY PACKAGE ID ======

export function getAllPackageVersions(packagePathName: string) {
    store.dispatch(
        actions.getAllPackageVersionsReq(packagePathName),
    );
}

export const packageVersions$ = (): Observable<PackageVersion[]> => store$.pipe(
    map(state => sel.PACKAGE_VERSIONS(state)),
    skipWhile(packagesVersions => !packagesVersions),
    distinctUntilChanged(),
);

// ====== EDIT SRC Package VERSION ======

export function editPackageVersion(version: UpdatePackageVersion) {
    store.dispatch(
        actions.editPackageVersionReq(version),
    );
}

// ====== DELETE SRC Package Version ======

export function deletePackageVersion(deleteVersionRequest: DeletePackageVersionConfig) {
    store.dispatch(
        actions.deletePackageVersion(deleteVersionRequest)
    );
}

// ====== CREATE SRC Package VERSION ======

export function createPackageVersion(version: CreatePackageVersionCfg) {
    store.dispatch(
        actions.createPackageVersionReq(version),
    );
}

// ======= ADD Package Version Image ======

export function addPackageVersionImage(request: AddPackageVersionImage) {
    store.dispatch(
        actions.addPackageVersionImageReq(request),
    );
}

export function editPackageVersionImage(request: EditPackageVersionImage) {
    store.dispatch(
        actions.editPackageVersionImageReq(request),
    );
}

export function deletePackageVersionImage(request: DeletePackageVersionImage) {

    store.dispatch(
        actions.deletePackageVersionImageReq(request),
    );
}

// ====== PACKAGE VERSION DEPENDENCIES ======

export function addPackageVersionDependency(request: AddVersionDependencyReq) {
    store.dispatch(
        actions.addVersionDependencyReq(request),
    );
}