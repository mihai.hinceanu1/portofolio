import { getPackageElementsColor, MILESTONES } from './packages-list.utils';
import { IIndicatorsCardItem } from '../../shared/interfaces/indicators-card';
import { ICodePackage } from '../interfaces/packages';

/** Returns the number of packages for each milestone. */
export function getPackageListOverview(packages: ICodePackage[]): IIndicatorsCardItem[] {
    let result: IIndicatorsCardItem[] = [
        {
            _id: 'packages',
            label: 'Packages',
            value: packages.length.toString(),
            color: getPackageElementsColor(MILESTONES.Package),
        }
    ];

    let editorsChoiceCount = packages.filter(codePackage => codePackage.milestone === MILESTONES.Editors_Choice).length;
    let highestRatedCount = packages.filter(codePackage => codePackage.milestone === MILESTONES.Highest_Rated).length;
    let bestsellerCount = packages.filter(codePackage => codePackage.milestone === MILESTONES.Bestseller).length;

    if (editorsChoiceCount) {
        let editorsChoice: IIndicatorsCardItem = {
            _id: 'editors_choice',
            label: MILESTONES.Editors_Choice,
            value: editorsChoiceCount.toString(),
            color: getPackageElementsColor(MILESTONES.Editors_Choice),
        };

        result.push(editorsChoice);
    }

    if (highestRatedCount) {
        let highestRated: IIndicatorsCardItem = {
            _id: 'highest_rated',
            label: MILESTONES.Highest_Rated,
            value: highestRatedCount.toString(),
            color: getPackageElementsColor(MILESTONES.Highest_Rated),
        };

        result.push(highestRated);
    }

    if (bestsellerCount) {
        let bestseller: IIndicatorsCardItem = {
            _id: 'bestseller',
            label: MILESTONES.Bestseller,
            value: bestsellerCount.toString(),
            color: getPackageElementsColor(MILESTONES.Bestseller),
        };

        result.push(bestseller);
    }

    return result;
}
