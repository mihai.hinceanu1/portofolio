import { store, store$ } from '../../shared/services/state.service';
import { ICodePackage, NewPackageConfig, UploadPackageImageConfig } from '../interfaces/packages';
import { DeletePackageConfig, UploadPackageImageResponse } from '../interfaces/source-code';
import * as actions from '../state/actions/packages.actions';
import * as sel from '../state/selectors/packages.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== GET ALL PACKAGES ======

export function getAllPackages() {
    store.dispatch(
        actions.getAllPackagesReq(),
    );
}

export const packages$ = (): Observable<ICodePackage[]> => store$.pipe(
    map(state => sel.PACKAGES(state)),
    skipWhile(packages => !packages ),
    distinctUntilChanged(),
);

// ====== EDIT PACKAGE ======

export function editPackage(codePackage: ICodePackage) {
    store.dispatch(
        actions.editPackageReq(codePackage),
    );
}

// ====== CREATE PACKAGE ======

export function createPackage(codePackage: NewPackageConfig) {
    store.dispatch(
        actions.createPackageReq(codePackage),
    );
}

// ====== DELETE PACKAGE ======

export function deletePackage(packageId: DeletePackageConfig) {

    store.dispatch(
        actions.deletePackage(packageId),
    );
}

// ====== UPLOAD IMAGE ======

export function uploadImage(config: UploadPackageImageConfig) {
    store.dispatch(
        actions.uploadImageReq(config),
    );
}

export const uploadPackageImage$ = (): Observable<UploadPackageImageResponse> => store$.pipe(
    map(state => sel.UPLOAD_PACKAGE_IMAGE(state)),
    skipWhile(imageUploaded => !imageUploaded.error && !imageUploaded.response),
);
