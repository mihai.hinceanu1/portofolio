import { store, store$ } from '../../shared/services/state.service';
import { IDependency } from '../interfaces/floating-menu';
import * as actions from '../state/actions/dependencies.actions';
import * as sel from '../state/selectors/dependencies.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== GET ALL DEPENDENCIES ======

export const dependencies$ = (): Observable<IDependency[]> => store$.pipe(
    map(state => sel.DEPENDENCIES(state)),
    skipWhile(dependencies => !dependencies),
    distinctUntilChanged(),
);

export function getDependencies() {
    store.dispatch(
        actions.getDependenciesReq(),
    );
}

// ====== GET VERSION DEPENDENCIES ======

export const versionDependencies$ = (): Observable<IDependency[]> => store$.pipe(
    map(state => sel.VERSION_DEPENDENCIES(state)),
    skipWhile(versionDependencies => !versionDependencies),
    distinctUntilChanged(),
);

export function getVersionDependencies(pathName: string) {
    store.dispatch(
        actions.getVersionDependenciesReq(pathName),
    );
}