import { CodeSnippet, CodeSnippetExtended } from '../interfaces/code-snippet';
import { SourceFile } from '../interfaces/source-file';
// import { IMarker } from 'react-ace/lib/types';

/**
 * Give codeSnippet and sourceFile
 * Transform sourceFile.code to codeSnippet.position coordinates in a string
 */
export function getSnippetFromSourceFile(codeSnippet: CodeSnippetExtended) {
    // get positions
    let sourceFile = codeSnippet.sourceFile;
    // return substring
    if (codeSnippet && codeSnippet.position && sourceFile) {
        const { position } = codeSnippet;
        return sourceFile.code.substring(position.start, position.end);
    } else {
        return '';
    }
}

// codeSnippets can only be found inside the sourceFile
// transform each code snippet position (start, end) to IMarker specific object (startCol, endCol, startRow, endRow)
export function getMarkerCoordinates(sourceFile: SourceFile, codeSnippets: CodeSnippet[]): /* IMarker */any[] {
    let { code } = sourceFile;
    // let markersArr: IMarker[] = []; // TODO Update to monaco
    let markersArr: any[] = [];

    let splitCode = code.split('\n');

    codeSnippets.forEach(cs => {
        // startRow -> number of "\n" before position.start
        // endRow -> number of "\n" until position.end
        // startCol/endCol -> search for left nearest "\n" in code (get its position)

        let startRow = -1;
        let endRow = -1;
        let startCol = 0;
        let endCol = 0;
        let totalLength = 0;

        splitCode.forEach((codeBunch, index) => {
            totalLength += (codeBunch.length + 2);
            if (totalLength > cs.position.start && startRow < 0) {
                startRow = index;
            }

            if (totalLength >= cs.position.end && endRow < 0) {
                endRow = index + 1;
            }

            // get columns (start + end)

            if (startRow && endRow) {
                return;
            }
        });
        startRow = startRow + 2;
        endRow = endRow + 2;
        markersArr.push({
            startRow,
            endRow,
            startCol, endCol,
            className: 'code-snippet',
            type: 'background',
        });
    });

    return markersArr;
}