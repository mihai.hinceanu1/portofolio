import { store, store$ } from '../../shared/services/state.service';
import * as commonActions from '../state/actions/source-code-common.actions';
import * as commonSelectors from '../state/selectors/source-code-common.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export function setShowAdminNavigator() {
    store.dispatch(
        commonActions.toggleAdminNavigator(),
    );
}

export const showAdminNavigator$ = (): Observable<boolean> => store$.pipe(
    map(state => commonSelectors.ADMIN_NAVIGATOR(state)),
    skipWhile(isVisible => isVisible === null),
    distinctUntilChanged(),
);
