import { store, store$ } from '../../shared/services/state.service';
import { AddSourceFile, UpdateSrcCode } from '../interfaces/source-file';
import { SourceFileTree } from '../interfaces/source-file-tree';
import * as actions from '../state/actions/source-files.actions';
import * as sel from '../state/selectors/source-file.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';
// import { SourceFile } from '../interfaces/source-file';

// ====== GET SOURCE FILES BY VERSION ID ======

export function getSourceFilesByVersionId(versionId: string) {
    store.dispatch(
        actions.getSourceFilesByPackageVersionIdReq(versionId),
    );
}

// export const sourceFiles$ = (): Observable<SourceFile[]> => store$.pipe(
//     map(state => sel.SOURCE_FILES(state)),
//     skipWhile(sourceFiles => !sourceFiles),
//     distinctUntilChanged(),
// );

export const sourceFilesTree$ = (): Observable<SourceFileTree[]> => store$.pipe(
    map(state => sel.SOURCE_FILE_TREE(state)),
    skipWhile(tree => tree === null),
    distinctUntilChanged(),
);

export function getSourceFilesTree(pathName: string) {
    store.dispatch(
        actions.getSourceFilesTreeReq(pathName),
    );
}

export function addSourceFile(addSourceFile: AddSourceFile) {
    store.dispatch(
        actions.addSourceFileReq(addSourceFile),
    );
}

export function updateSrcCode(updateSrcCode: UpdateSrcCode) {
    store.dispatch(
        actions.updateSourceFileCodeReq(updateSrcCode)
    );
}