import { RELATED_PACKAGES } from './_assets/version-title.assets';
import * as div from './version-title.style';
import { VersionTitleCfg } from '../../interfaces/version-title';
import * as React from 'react';

interface Props {
    config: VersionTitleCfg;
    override?: string;
}

interface State {
    hovered: boolean;
}

/**
 * Version Title - Used to assure a title and a short description for a
 * code version, it can be locked/unlocked and last/notLast.
 */
export class VersionTitle extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            hovered: false,
        };
    }

    public render() {
        const { config, override } = this.props;
        const { hovered } = this.state;

        return (
            <div.VersionTitle data-cy='version-title'
                override={override}
                disabled={true}
                onMouseEnter={() => this.onMouseEnter()}
                onMouseLeave={() => this.onMouseLeave()}>
                {/* Title Cluster */}
                <div.TitleCluster data-cy='title-cluster'>
                    <div.Title data-cy='title' isLast={config.isLast}>
                        {config.title}
                    </div.Title>

                    {/* Version Bullet */}
                    <div.Version data-cy='version' isLast={config.isLast}>
                        <div.VersionText data-cy='version-text' >
                            V{config.noOfVersion}
                        </div.VersionText>
                    </div.Version>

                    {/* Related Packages*/}
                    {
                        div.relatedPackagesIcon(RELATED_PACKAGES)
                    }

                    {/* Locked/Unlocked*/}
                    {
                        hovered &&
                        div.lockIcon(config.isLast, config.isLocked)
                    }
                </div.TitleCluster>

                {/* Description */}
                <div.Description data-cy='description'>
                    {config.description}
                </div.Description>
            </div.VersionTitle>
        );
    }

    private onMouseEnter() {
        this.setState({
            hovered: true,
        });
    }

    private onMouseLeave() {
        this.setState({
            hovered: false,
        });
    }
}