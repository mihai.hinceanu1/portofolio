/// <reference types="cypress" />

describe('Version Title Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/source-code/version-title');
    });

    it('Component is Rendered', () => {
        getEln('standard', 'version-title').should('exist');
    });

    it('Title Is Rendered', () => {
        getEln('standard', 'title').should(div => {
            expect(div.text()).to.eq('Server Upgrades');
        });
    });

    it('Description Is Rendered', () => {
        getEln('standard', 'description').should(div => {
            expect(div.text()).to.eq('Some key points on server updates');
        });
    });

    it('Lock Icon Is Rendered', () => {
        getEln('standard', 'lock-icon').should('exist');
    });

    it('Version Number Is Rendered', () => {
        getEln('standard', 'version-text').should(div => {
            expect(div.text()).to.eq('V5');
        });
    });

    it('Bgr Color Is Correct', () => {
        getEln('standard', 'version').should('have.css', 'background-color', 'rgb(0, 195, 167)');
    });
});

// ====== SELECTORS ======
function getEln(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}