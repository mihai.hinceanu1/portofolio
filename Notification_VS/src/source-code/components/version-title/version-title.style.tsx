import { LOCKED, UNLOCKED } from './_assets/version-title.assets';
import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface VersionTitleProps {
    isLast?: boolean;

}

export const VersionTitle = styled.TouchableOpacity<ReactWebAttributes & { override: string }>`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    max-width: 340px;
    ${props => props.override}
`;

export const TitleCluster = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const Title = styled.Text<VersionTitleProps>`
    font-size: ${font.$size20}px;
    color: ${props => getColor(props.isLast)};
    font-weight: 600;
`;

export const Version = styled.View<VersionTitleProps>`
    height: 24px;
    width: 24px;
    border-radius: 12px;
    background-color: ${props => getColor(props.isLast)};
    margin-left: 15px;
    justify-content: center;
    align-items: center;
`;

export const VersionText = styled.Text`
    font-size: ${font.$size10}px;
    color: white;
`;

export const Description = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$grey};
    margin-top: 8px;
`;

export const SvgWrapper = styled.View`
    margin-left: 15px;
`;

// ====== UTILS ======

export const getColor = (isLast: boolean) => {
    return isLast ? colors.$green : colors.$darkBlue;
};

export const getPath = (isLocked: boolean) => {
    return isLocked ? LOCKED : UNLOCKED;
};



export const lockIcon = (isLast: boolean, isLocked: boolean) => {

    const path = getPath(isLocked);
    let boundingBox = pathBoundingBox(path);
    let { height, width } = boundingBox;

    return (
        <SvgWrapper>
            <Svg data-cy='lock-icon'
                width={width}
                height={height}
                fill='none' style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Path data-cy='lock-icon-path'
                    d={path} fill={getColor(isLast)} />
            </Svg>
        </SvgWrapper>
    );
};

export const relatedPackagesIcon = (svgPath: string) => {
    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;

    return (
        <SvgWrapper>
            <Svg data-cy='related-packages-svg'
                width={width}
                height={height}
                fill='none' style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Path d={svgPath} fill={colors.$darkGrey} />
            </Svg>
        </SvgWrapper>
    );
};