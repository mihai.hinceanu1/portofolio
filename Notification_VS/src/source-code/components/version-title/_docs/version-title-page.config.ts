import { VersionTitleCfg } from '../../../interfaces/version-title';

export const standard: VersionTitleCfg = {
    title: 'Server Upgrades',
    description: 'Some key points on server updates',
    isLast: true,
    isLocked: false,
    noOfVersion: 5,
};

export const customTitleAndDescription: VersionTitleCfg = {
    title: 'Front End Refactoring',
    description: 'Front End renewed',
    isLast: true,
    isLocked: false,
    noOfVersion: 5,
};

export const customVersionNumber: VersionTitleCfg = {
    title: 'Front End Refactoring',
    description: 'Front End renewed',
    isLast: true,
    isLocked: false,
    noOfVersion: 1,
};

export const isLast: VersionTitleCfg = {
    title: 'Front End Refactoring',
    description: 'Front End renewed',
    isLast: false,
    isLocked: false,
    noOfVersion: 1,
};

export const locked: VersionTitleCfg = {
    title: 'Front End Refactoring',
    description: 'Front End renewed',
    isLast: false,
    isLocked: true,
    noOfVersion: 1,
};