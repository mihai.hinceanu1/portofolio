export const STANDARD = `
export const config: VersionTitleCfg = {
    title: 'Server Upgrades',
    description: 'Some key points on server updates',
    isLast: true,
    isLocked: false,
    hovered: true,
    noOfVersion: 5,
};`;

export const CUSTOM_TITLE_DESCRIPTION = `
export const config: VersionTitleCfg = {
    title: {CUSTOM_TITLE},
    description: {CUSTOM_DESCRIPTION}
}`;

export const CUSTOM_VERSION_NUMBER = `
export const config: VersionTitleCfg = {
    noOfVersion: 1,
}`;

export const IS_LAST = `
export const config: VersionTitleCfg = {
    isLast: false,
}`;

export const LOCKED = `
export const config: VersionTitleCfg = {
    isLocked: true,
};`;