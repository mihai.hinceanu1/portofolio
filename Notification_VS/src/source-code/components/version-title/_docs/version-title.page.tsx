import * as cfg from './version-title-page.config';
import * as samples from './version-title-page.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { VersionTitle } from '../version-title';
import * as React from 'react';

export class VersionTitlePage extends ScrollableDocPage<{}> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state
        };
    }

    public renderDemoPage() {
        const { width } = this.state;

        return (
            <>
                <div.Overview width={width} nativeID='checkbox-page'>
                    <div.OverviewTitle>
                        Version Title
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Version title will be used in order to highlight the title of a current version, give a short description
                        and also to tell if the version is the last one or if its status is locked/unlocked.
                    </div.OverviewDescription>
                </div.Overview>

                {/* STANDARD */}
                <div.Demo width={width} data-cy='standard'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <VersionTitle config={cfg.standard} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.STANDARD} />
                </div.Demo>

                {/* CUSTOM TITLE/DESCRIPTION */}
                <div.Demo width={width} data-cy='custom-title-description'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Title/Description
                        </div.DemoTitle>

                        <VersionTitle config={cfg.customTitleAndDescription} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CUSTOM_TITLE_DESCRIPTION} />
                </div.Demo>

                {/* VERSION NO */}
                <div.Demo width={width} data-cy='version-number'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Version Number
                        </div.DemoTitle>

                        <VersionTitle config={cfg.customVersionNumber} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CUSTOM_VERSION_NUMBER} />
                </div.Demo>

                {/* IS (NOT) LAST */}
                <div.Demo width={width} data-cy='is-last'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Is (not) Last
                        </div.DemoTitle>

                        <VersionTitle config={cfg.isLast} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.IS_LAST} />
                </div.Demo>

                {/* LOCKED */}
                <div.Demo width={width} data-cy='locked'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Locked
                        </div.DemoTitle>

                        <VersionTitle config={cfg.locked} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.LOCKED} />
                </div.Demo>
            </>

        );
    }
}