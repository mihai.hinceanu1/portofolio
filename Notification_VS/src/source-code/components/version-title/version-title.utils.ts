import { RELATED_PACKAGES } from './_assets/version-title.assets';
import { DROPDOWN_TYPE } from '../../../shared/constants/dropdown.const';
import { DropdownConfig } from '../../../shared/interfaces/dropdown';
import { colors } from '../../../shared/style/colors';

export const dropdownCfg: DropdownConfig = {
    options: [
        {
            _id: 'label1',
            label: 'label1',
        },
        {
            _id: 'label2',
            label: 'label2',
        },
        {
            _id: 'label3',
            label: 'label3',
        },
        {
            _id: 'label4',
            label: 'label4',
        },
    ],
    icon: {
       svgData: RELATED_PACKAGES,
       fill: colors.$darkGrey,
    },
    type: DROPDOWN_TYPE.OnlyIcon,
};