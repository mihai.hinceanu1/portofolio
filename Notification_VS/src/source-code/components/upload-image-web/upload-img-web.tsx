import './upload-img.style.css';
import * as React from 'react';

interface Props {
  onChange: () => void;
}

interface State {
  file: any;
}

/**
 * Component used to open the file explorer from computer
 * and get a single file from it
 */
export class UploadFile extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      file: null
    };
  }

  render() {
    const { onChange } = this.props;

    return (
        <div className='inputWrapper' >
          <input className='fileInput' type='file' name='file1' onChange={() => onChange()} />
        </div>
    );
  }
}