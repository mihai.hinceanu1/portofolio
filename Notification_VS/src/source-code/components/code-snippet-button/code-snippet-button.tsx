import * as div from './code-snippet-button.style';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import { CodeSnippet } from '../../interfaces/code-snippet';
import * as React from 'react';
import { Text } from 'react-native';

interface Props {
    codeEditorOpen: (codeSnippet: CodeSnippet) => void;
    index: number;
    iconName: string;
}

interface State { }

export class CodeSnippetButton extends React.Component<Props, State> {

    public constructor(props: Props) {
        super(props);
    }

    public render() {
        return (
            <div.CodeSnippetButton data-cy='code-snippet-button'
                onPress={() => {
                    this.props.codeEditorOpen;
                }}>
                <div.Icon>
                    <FitImage imgPath={this.props.iconName} />
                </div.Icon>
                <Text>  code snippet {this.props.index + 1} </Text>
            </div.CodeSnippetButton>
        );
    }
}