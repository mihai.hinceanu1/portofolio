import styled from 'styled-components/native';

export const CodeSnippetButton = styled.TouchableOpacity`
    margin-top: 10px;
    margin-left: 15%;
    width:90%;
    height:15px;
    display:flex;
    flex-direction:row;
`;

export const Icon = styled.View`
    width:15%;
    height:15px;
`;