export const SOURCE_FILE_INPUT_OVERRIDES = {
    root: 'width: auto; margin: 15px 30px 0px 30px; padding: 0px'
};

export const TEXTAREA_OVERRIDES = {
    root: `
        flex-grow: 1;
        width: auto;
        margin-left: 30px;
        margin-right: 30px;
        outline-width: 0px;
        outline-color: transparent;
    `,
};