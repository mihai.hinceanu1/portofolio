import styled from 'styled-components/native';

export const AddSourceFileForm = styled.View`
    width: 500px;
    height: 600px;
    display: flex;
    flex-direction: column;
    background-color: white;
`;

export const Actions = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
    margin-right: 20px;
    margin-top: 10px;
    margin-bottom: 15px;
`;