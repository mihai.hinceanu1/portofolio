import * as div from './add-source-file-modal.style';
import { SOURCE_FILE_INPUT_OVERRIDES, TEXTAREA_OVERRIDES } from './utils/add-source-file-modal.utils';
import { NEW_FILE } from '../../../../../shared/assets/icons';
import { Button } from '../../../../../shared/components/button/button';
import { Input } from '../../../../../shared/components/input/input';
import { ModalBoxHeader } from '../../../../../shared/components/modal-box-header/modal-box-header';
import { Modal } from '../../../../../shared/components/modal/modal';
import { Textarea } from '../../../../../shared/components/textarea/textarea';
import * as overlay from '../../../../../shared/style/overlay.style';
import { AddSourceFile } from '../../../../interfaces/source-file';
import { addSourceFile } from '../../../../services/source-files.service';
import * as React from 'react';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
    callback: () => void;
    currentFolderId: string;
    packageVersionId: string;
    packageVersionPathName: string;
}

interface State {
    validationError: string;
    sourcefileName: string;
    code: string;
}

export class AddSourceFileModal extends React.Component<Props, State> {

    constructor(props: Props) {

        super(props);

        this.state = {
            validationError: '',
            sourcefileName: '',
            code: '',
        };
    }

    public render() {
        const { callback } = this.props;
        const { sourcefileName, validationError, code } = this.state;

        return (
            <LinearGradient colors={overlay.OVERLAY_GRADIENT}
                style={overlay.style.linearGradient}>
                <Modal callback={callback} data-cy='add-new-file-modal' hasBackgroundColor={false}>
                    <div.AddSourceFileForm>
                        <ModalBoxHeader icon={NEW_FILE}
                            title='CREATE NEW FILE'
                            closeModal={callback} />

                        <Input data-cy='version-name-input'
                            placeholder={'Source File Name...'}
                            label='Name'
                            error={validationError}
                            value={sourcefileName}
                            overrides={SOURCE_FILE_INPUT_OVERRIDES}
                            onChangeText={(text: string) => this.handleTextChange(text, 'sourcefileName')} />

                        <Textarea value={code}
                            label='Code'
                            placeholder='Code..'
                            onChangeText={(text) => this.handleTextChange(text, 'code')}
                            overrides={TEXTAREA_OVERRIDES} />

                        {/** Submit/Cancel */}
                        <div.Actions data-cy='actions'>
                            <Button config={{
                                text: 'Cancel',
                                onPress: callback,
                            }} />

                            <Button config={{
                                text: 'Create',
                                onPress: () => this.insertSrcFileDB(),
                            }} />
                        </div.Actions>
                    </div.AddSourceFileForm>
                </Modal>
            </LinearGradient>
        );
    }

    private handleTextChange(text: string, field: string) {
        this.setState({
            ...this.state,
            [field]: text,
        });
    }

    private insertSrcFileDB() {
        const { currentFolderId, packageVersionId, packageVersionPathName, callback } = this.props;
        const { sourcefileName, code } = this.state;

        if (!sourcefileName.length) {
            this.setState({
                validationError: 'Name cannot be empty',
            });
            return;
        }

        if (!!sourcefileName.length) {
            this.setState({
                validationError: '',
            });
        }

        const addFileReq: AddSourceFile = {
            addSourceFile: {
                name: sourcefileName,
                parentId: currentFolderId,
                packageVersionId: packageVersionId,
                code: code,
            },
            packageVersionPathName: packageVersionPathName,
        };

        addSourceFile(addFileReq);
        callback();
    }
}