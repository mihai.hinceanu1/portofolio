import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const CodeEditorWrapper = styled.View`
    margin-top: 25px;
    display: flex;
    flex-direction: column;
`;

export const SaveIcon = styled.TouchableOpacity`
    position: absolute;
    right: 30px;
    top: 30px;
`;

export const Editor = styled.View`
    width: 100%;
    height: 750px;
    display: flex;
    flex-direction: row;
`;

export const FileTreeMenu = styled.View`
    display: flex;
    flex-direction: column;
    width: 20%;
    height: 100%;
    background-color: white;
    border-right-color: ${colors.$lightGrey};
    border-right-width: 0.5px;
    border-right-style: solid;
`;

