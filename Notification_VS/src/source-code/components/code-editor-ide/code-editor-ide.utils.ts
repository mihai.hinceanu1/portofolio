import { SourceFileTree, SourceFileTreeLevel } from '../../interfaces/source-file-tree';

export const testConfig = {
    showGutter: false,
    mode: 'javascript',
    editable: true,
    theme: 'iplastic',
    height: 750,
    highlightActiveLine: false,
};

export const getSourceFileTreeLevels = (sourceFileTree: SourceFileTree[]) => {
    if (!!sourceFileTree.length) {
        let newSrcFileTree = sourceFileTree.map(file => { Object.assign(file, { isRoot: false }); return file; });

        let updated: SourceFileTreeLevel[] = [{
            name: 'root',
            isRoot: true,
            parent: null,
            isFolder: true,
            packageVersionId: '',
            children: newSrcFileTree as any,
        }];

        return updated;
    }
};

export function getConfig(width: number) {
    return {
        ...testConfig,
        width,
    };
}