import * as div from './code-editor-ide.style';
import * as utils from './code-editor-ide.utils';
import { AddSourceFileModal } from './modals/add-source-file-modal/add-source-file-modal';
import { Icon } from '../../../icons/components/icon/icon';
import { FILE } from '../../../shared/assets/icons';
import { CodeEditor } from '../../../shared/components/code-editor/code-editor';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { UpdateSrcCode } from '../../interfaces/source-file';
import { SourceFileTreeLevel } from '../../interfaces/source-file-tree';
import { getSourceFilesTree, sourceFilesTree$, updateSrcCode } from '../../services/source-files.service';
import { CodeEditorFileTree } from '../code-editor-file-tree/code-editor-file-tree';
import { CodeEditorHeader } from '../code-editor-header/code-editor-header';
import { FileTreeMenuHeader } from '../file-tree-menu-header/file-tree-menu-header';
import * as React from 'react';
import { LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Params {
    pathName: string;
}
interface Props extends RouteComponentProps<Params> { }

interface State {
    pressedLevels: SourceFileTreeLevel[];
    sourceFileTree: SourceFileTreeLevel[];
    pressedFile: SourceFileTreeLevel;
    editorWidth: number;
    fileTreeWidth: number;
    currentFolder: SourceFileTreeLevel;
    packageVersionPathName: string;
    fileSrcCode: string;
    shouldSave: boolean;
}

export class _CodeEditorIde extends React.Component<Props, State> {

    public destroyed$ = new Subject<void>();

    private addSrcFileModal: JSX.Element;

    constructor(props: Props) {
        super(props);

        this.state = {
            pressedLevels: null,
            sourceFileTree: null,
            pressedFile: null,
            editorWidth: 0,
            fileTreeWidth: 0,
            currentFolder: null,
            packageVersionPathName: null,
            fileSrcCode: '',
            shouldSave: false,
        };
    }

    public render() {
        const { pressedLevels, pressedFile, sourceFileTree, editorWidth, fileTreeWidth, currentFolder, shouldSave } = this.state;

        return (
            <div.CodeEditorWrapper>
                {/* Code Editor Header */}
                <CodeEditorHeader width={editorWidth - fileTreeWidth}
                    cutFileFromHeader={(file: SourceFileTreeLevel) => this.cutFileFromHeader(file)}
                    onPressHeaderFile={(file: SourceFileTreeLevel) => this.setState({ pressedFile: file })}
                    files={pressedLevels} pressedFile={pressedFile} />

                {/* Editor */}
                <div.Editor onLayout={e => this.updateEditorWidth(e)}>
                    <div.FileTreeMenu onLayout={e => this.updateFileTreeWidth(e)}>
                        {
                            !!sourceFileTree &&
                            <FileTreeMenuHeader currentFolder={!!currentFolder ? currentFolder : sourceFileTree[1]} openAddSrcFileModal={() => this.openAddSourceFileModal()} />
                        }

                        {/* Code Editor File Tree*/}
                        <CodeEditorFileTree levels={sourceFileTree} onLevelClick={(level: SourceFileTreeLevel) => this.onLevelPress(level)} />
                    </div.FileTreeMenu>

                    {/* Code Editor*/}
                    <CodeEditor
                        {...utils.getConfig(editorWidth - fileTreeWidth)}
                        srcCode={this.getCodeEditorCode()}
                        onChangeValue={(value: string) => this.onEditorChangeValue(value)} />

                    {shouldSave &&
                        < div.SaveIcon onPress={() => this.onSaveCode()}>
                            <Icon path={FILE} width={30} height={30} />
                        </div.SaveIcon>
                    }

                </div.Editor>
            </div.CodeEditorWrapper >
        );
    }

    public componentDidMount() {
        const { pathName } = this.props.match.params;

        this.subscribeToSourceFilesTree();

        this.setState({
            packageVersionPathName: pathName,
        });

        getSourceFilesTree(pathName);
    }

    private onSaveCode() {
        const { pressedFile, packageVersionPathName } = this.state;

        let updateSrcCodeObj: UpdateSrcCode = {
            srcFileId: pressedFile._id,
            code: pressedFile.code,
            packageVersionPathName: packageVersionPathName
        };

        updateSrcCode(updateSrcCodeObj);
        this.setState({
            shouldSave: false,
        });
    }

    public onEditorChangeValue(value: string) {
        const { pressedFile } = this.state;

        if (!pressedFile) {
            return;
        }

        pressedFile.code = value;

        this.setState({
            pressedFile,
            shouldSave: true,
        });
    }

    private updateEditorWidth(event: LayoutChangeEvent) {
        this.setState({
            editorWidth: event.nativeEvent.layout.width,
        });
    }

    private updateFileTreeWidth(event: LayoutChangeEvent) {
        this.setState({
            fileTreeWidth: event.nativeEvent.layout.width,
        });
    }

    private cutFileFromHeader(file: SourceFileTreeLevel) {
        const { pressedLevels, pressedFile } = this.state;

        if (pressedFile === file) {
            if (!pressedLevels) {
                this.setState({
                    pressedFile: null,
                });
            }
        }

        const filteredPressedLevels = pressedLevels.filter(level => level !== file);

        if (!!filteredPressedLevels) {
            this.setState({
                pressedFile: filteredPressedLevels[filteredPressedLevels.length - 1],
            });
        }

        this.setState({
            pressedLevels: filteredPressedLevels,
        });
    }

    private onLevelPress(level: SourceFileTreeLevel) {
        let { pressedLevels, currentFolder, sourceFileTree } = this.state;

        if (!pressedLevels) {
            pressedLevels = [];
        }

        if (level.isFolder && level !== currentFolder) {
            this.setState({
                currentFolder: level,
                shouldSave: false,
            });
        } else if (level.isFolder) {
            this.setState({
                currentFolder: sourceFileTree[0],
            });
        }

        const pressedLvlsIds = pressedLevels.map(level => level._id);

        if (level.isFolder === false && !pressedLvlsIds.includes(level._id)) {
            pressedLevels.push(level);
            this.setState({
                pressedFile: level,
                shouldSave: false,
            });

        } else if (pressedLvlsIds.includes(level._id)) {
            this.setState({
                pressedFile: level,
                shouldSave: false,
            });
        }

        if (pressedLevels.length === 0) {
            pressedLevels = null;
        }

        this.setState({
            pressedLevels
        });
    }

    private getCodeEditorCode() {
        const { pressedFile, pressedLevels } = this.state;

        if (!pressedFile) {
            if (!pressedLevels || !pressedLevels.length) {
                return 'Welcome';
            } else {
                return pressedLevels[pressedLevels.length - 1].code;
            }
        } else {
            return pressedFile.code;
        }
    }

    private openAddSourceFileModal() {
        const { currentFolder, packageVersionPathName } = this.state;

        this.addSrcFileModal = <AddSourceFileModal
            callback={() => this.hideAddSrcFileModal()} packageVersionPathName={packageVersionPathName}
            currentFolderId={currentFolder._id} packageVersionId={currentFolder.packageVersionId} />;
        addAppOverlay(this.addSrcFileModal);
    }

    private hideAddSrcFileModal() {
        removeAppOverlay(this.addSrcFileModal);
        delete this.addSrcFileModal;
    }

    private subscribeToSourceFilesTree() {
        sourceFilesTree$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(sourceFileTree => {
                let updated = utils.getSourceFileTreeLevels(sourceFileTree);
                if (!!updated) {
                    this.setState({
                        sourceFileTree: updated,
                        currentFolder: updated[0].children[0],
                    });
                }
            });
    }

}

export const CodeEditorIde = withRouter<Props, any>(_CodeEditorIde);