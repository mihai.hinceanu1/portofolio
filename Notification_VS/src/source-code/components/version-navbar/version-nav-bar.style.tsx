import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface Override {
    overrides: {};
}

export const VersionNavbar = styled.View<Override>`
    display: flex;
    flex-direction: row;
    height: 45px;
    ${props => props.overrides}
`;

export const VersionNavItem = styled.TouchableOpacity`
    display: flex;
    flex-direction: column;
    margin-right: 25px;
    align-items: center;
    justify-content: space-between;
`;

export const Label = styled.Text`
    font-size: ${font.$size10}px;
    color: ${colors.$darkGrey};
    margin-top: 5px;
`;

// ====== UTILS ======

export const itemSvg = (svgPath: string) => {
    const boundingBox = pathBoundingBox(svgPath);
    const { height, width } = boundingBox;

    return (
        <Svg data-cy='svg'
            width={width + 1}
            height={height + 1}
            fill='none' style={{ backgroundColor: 'none', justifyContent: 'center', alignItems: 'center' }}>
            <Path data-cy='svg-path'
                d={svgPath} fill={colors.$blue} />
        </Svg>
    );
};