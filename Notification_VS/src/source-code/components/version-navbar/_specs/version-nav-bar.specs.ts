/// <reference types="cypress" />

describe('Version Navigation Menu Specs', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/source-code/version-navbar');
    });

    it('Menu is Rendered', () => {
        getElm('standard', 'version-navbar').should('exist');
    });

    it('NavItem is Rendered', () => {
        getElm('standard', 'version-nav-item').should('exist');
    });

    it('SVG is Rendered', () => {
        getElm('standard', 'svg').should('exist');
    });

    it('Label is Rendered', () => {
        getElm('standard', 'label').eq(0).should(item => {
            expect(item.text()).to.eq('Snippets');
        });
    });

    it('Given callback calls properly', () => {
        getElm('callback', 'version-nav-item').eq(0).click();
        getElm('callback', 'callback-result').should('have.text', 'Nav items were pressed: 1 times');
    });
});

// ====== SELECTORS ======

function getElm(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}