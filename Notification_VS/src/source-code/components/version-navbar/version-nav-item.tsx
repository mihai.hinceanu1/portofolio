import * as div from './version-nav-bar.style';
import * as React from 'react';

interface Props {
    asset: string;
    label: string;
    onPress: () => void;
}

export const VersionNavItem: React.FunctionComponent<Props> = (props: Props) => {
    const { asset, label, onPress } = props;

    return (
        <div.VersionNavItem data-cy='version-nav-item' onPress={onPress}>
            {div.itemSvg(asset)}

            <div.Label data-cy='label'>
                {label}
            </div.Label>
        </div.VersionNavItem>
    );
}
