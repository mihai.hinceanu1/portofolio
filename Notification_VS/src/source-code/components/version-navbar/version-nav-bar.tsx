import { VERSION_NAVBAR } from './_assets/version-nav-bar.const';
import * as div from './version-nav-bar.style';
import { VersionNavItem } from './version-nav-item';
import * as React from 'react';

export interface IVersionNavbarProps {
    /** CSS Styles overrides */
    overrides?: string;

    /** Executed on navbar item click. Returns the label that was clicked (might be changed in the future) */
    callback?: (label: string) => void;
}

/**
 * Basic Admin Navigation through Version in Source Code
 * Easy navigate between one version's elements (snippets, revisions etc.)
 */
export const VersionNavbar: React.StatelessComponent<IVersionNavbarProps> = (props: IVersionNavbarProps) => {
    const { overrides, callback } = props;

    return (
        <div.VersionNavbar data-cy='version-navbar' overrides={overrides}>
            {
                VERSION_NAVBAR.map(item =>
                    <VersionNavItem key={item.label}
                        asset={item.svg}
                        label={item.label}
                        onPress={() => callback && callback(item.label)} />)
            }
        </div.VersionNavbar>
    );
};
