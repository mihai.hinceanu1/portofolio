export const DEFAULT = `<VersionNavBar />`;

export const OVERRIDES = `<VersionNavbar
    overrides={ 'flex-direction: column; height: 450px; justify-content: space-between' } />
`;

export const CALLBACK = `<VersionNavbar callback={myFunction} />

    function myFunction(item) {
        console.log(item);
    }
`;