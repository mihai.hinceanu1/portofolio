import * as samples from './version-nav-bar.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { VersionNavbar } from '../version-nav-bar';
import * as React from 'react';

interface VersionNavbarState {
    callbackResult: number;
}

export class VersionNavbarPage extends ScrollableDocPage<VersionNavbarState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                callbackResult: 0,
            }
        };
    }

    public renderDemoPage() {
        let { width, overrides } = this.state;
        let { callbackResult } = overrides;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Version Navigation Bar
                    </div.OverviewTitle>
                </div.Overview>

                {/** Standard */}
                <div.Demo data-cy='standard' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <VersionNavbar />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.DEFAULT} />
                </div.Demo>

                {/** Overrides */}
                <div.Demo data-cy='overrides' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides
                        </div.DemoTitle>

                        <VersionNavbar overrides={'flex-direction: column; height: 450px; justify-content: space-between'} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.OVERRIDES} />
                </div.Demo>

                {/** Callback */}
                <div.Demo data-cy='callback' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callback
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='callback-result'>
                            Nav items were pressed: {callbackResult} times
                        </div.CallbackResult>

                        <VersionNavbar callback={(item: string) => this.onPressItem(item)} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CALLBACK} />
                </div.Demo>
            </>

        );
    }

    private onPressItem(_item: string) {
        const { callbackResult } = this.state.overrides;

        this.setState({
            ...this.state,
            overrides: {
                callbackResult: callbackResult + 1,
            }
        });
    }
}