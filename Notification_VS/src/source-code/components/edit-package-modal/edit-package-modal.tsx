import * as div from './edit-package-modal.style';
import { EDIT_PACKAGE_DETAILS } from '../../../shared/assets/icons';
import { ModalBoxHeader } from '../../../shared/components/modal-box-header/modal-box-header';
import { Modal } from '../../../shared/components/modal/modal';
import { ICodePackage } from '../../interfaces/packages';
import { EditPackageForm } from '../edit-package-form/edit-package-form';
import React from 'react';

interface Props {
    codePackage: ICodePackage;
    closeModal: () => void;
}

/** The modal opening when an author wants to edit a code package.  */
export const EditPackageModal: React.FunctionComponent<Props> = (props: Props) => {
    const { closeModal, codePackage } = props;

    return (
        <Modal callback={closeModal}>
            <div.EditPackage data-cy='edit-package'>
                {/** Header */}
                <ModalBoxHeader icon={EDIT_PACKAGE_DETAILS}
                    title='EDIT PACKAGE DETAILS'
                    closeModal={closeModal} />

                {/** Form */}
                <EditPackageForm closeModal={closeModal} codePackage={codePackage} />
            </div.EditPackage>
        </Modal>
    );
};