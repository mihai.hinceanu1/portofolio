import * as div from './code-editor-tree-node.style';
import { getLevelIcon } from './code-editor-tree.utils';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import { SourceFileTreeLevel } from '../../interfaces/source-file-tree';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    isFolder: boolean;
    level: SourceFileTreeLevel;
    collapseIcon: string;
    expandIcon: string;
    onLevelExpand: (level: SourceFileTreeLevel) => void;
    onLevelCollapse: (level: SourceFileTreeLevel) => void;
    onLevelClick: (level: SourceFileTreeLevel) => void;
}

interface State {
    isOpen: boolean;
}

class _CodeEditorTreeNode extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            isOpen: false,
        };
    }

    public render() {
        const { level, collapseIcon, expandIcon, onLevelCollapse, onLevelExpand, onLevelClick, isFolder } = this.props;
        const { isOpen } = this.state;

        return (
            <div.TaxonomyTreeNode data-cy='taxonomy-tree-node'>

                {/** Level */}
                <div.Level data-cy='level'>

                    {/** Collapse icons */}
                    {
                        isOpen && isFolder &&
                        <div.CollapseIcons data-cy='collapse-icons'
                            onPress={() => this.setIsOpen(!isOpen, level)}>

                            {this.renderExpandCollapseIcon(collapseIcon)}
                            <Icon path={getLevelIcon(level, isOpen)} color={colors.$blue} />
                            {/* {this.renderLevelIcon()} */}
                        </div.CollapseIcons>
                    }

                    {/** Expand icons */}
                    {
                        !isOpen && isFolder &&
                        <div.ExpandIcons data-cy='expand-icons'
                            onPress={() => this.setIsOpen(!isOpen, level)}>

                            {this.renderExpandCollapseIcon(expandIcon)}
                            <Icon path={getLevelIcon(level, isOpen)} color={colors.$blue} />
                            {/* {this.renderLevelIcon()} */}
                        </div.ExpandIcons>
                    }

                    {/** Name */}
                    <div.Name data-cy='name'
                        onPress={() => this.onLevelPress(level)}>
                            {
                                !isFolder &&
                                div.getTitleSvg(icons.FILE)
                            }
                        <div.Label data-cy='label'>
                            {level.name}
                        </div.Label>
                    </div.Name>
                </div.Level>

                {/** Children list */}
                {
                    (isOpen && !!level.children) &&
                    level.children.map((node, index) =>
                        <CodeEditorTreeNode key={index}
                            level={node}
                            isFolder={node.isFolder}
                            collapseIcon={collapseIcon}
                            expandIcon={expandIcon}
                            onLevelCollapse={(level: SourceFileTreeLevel) => onLevelCollapse(level)}
                            onLevelExpand={(level: SourceFileTreeLevel) => onLevelExpand(level)}
                            onLevelClick={(level: SourceFileTreeLevel) => onLevelClick(level)} />,
                    )
                }
            </div.TaxonomyTreeNode>
        );
    }

    private setIsOpen(isOpen: boolean, level: SourceFileTreeLevel) {
        const { onLevelCollapse, onLevelExpand } = this.props;

        if (isOpen) {
            // means that tree node is going to expand
            // execute callback if defined
            onLevelExpand && onLevelExpand(level);
        } else {
            // tree node is collapsing
            // execute callback if defined
            onLevelCollapse && onLevelCollapse(level);
        }

        this.setState({ isOpen });
    }

    /**
     * Each level might have custom icon (Angular, React etc..)
     * Even though the state is false or true, if the level has a custom icon, it should be rendered,
     * regarding the current state of the node
     * Default icon should be "FOLDER" (either OPEN or CLOSE)
     * depending on the case
     */
    // private renderLevelIcon() {
    //     const { level } = this.props;
    //     const { isOpen } = this.state;

    //     if (level.isFolder) {
    //         // "import * as X" doesn't have typings :(
    //         return div.iconSvg(icons.FOLDER_CLOSE, 'level-icon', colors.$blue);
    //     }

    //     if (isOpen) {
    //         return div.iconSvg(icons.FOLDER_OPEN, 'level-icon', colors.$blue);
    //     }

    //     return div.iconSvg(icons.FOLDER_CLOSE, 'level-icon', colors.$blue);
    // }

    private renderExpandCollapseIcon(icon: string): JSX.Element {
        const { isOpen } = this.state;

        if (icon) {
            return div.iconSvg(icon, 'expand-collapse-icon', colors.$grey);
        }

        if (isOpen) {
            return div.iconSvg(icons.DROPDOWN_ARROW, 'expand-collapse-icon', colors.$grey);
        }

        return div.iconSvg(icons.DROPDOWN_ARROW, 'expand-collapse-icon', colors.$grey, -90);
    }

    private onLevelPress(level: SourceFileTreeLevel) {
        const { onLevelClick } = this.props;
        const { isOpen } = this.state;

        this.setState({ isOpen: !isOpen });

        onLevelClick && onLevelClick(level);
    }
}

export const CodeEditorTreeNode = withRouter<Props, any>(_CodeEditorTreeNode);
