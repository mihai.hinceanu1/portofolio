import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

export const TaxonomyTreeNode = styled.View`
    padding-left: 10px;
`;

export const Level = styled.View`
    width: 100%;
    flex-direction: row;
    align-items: center;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 10px;
`;

export const Name = styled.TouchableOpacity`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const Label = styled.Text`
    font-weight: 600;
`;

export const CollapseIcons = styled.TouchableOpacity`
    width: 50px;
    height: 20px;
    align-items: center;
    justify-content: flex-start;
    flex-direction: row;
`;

export const ExpandIcons = styled.TouchableOpacity`
    width: 50px;
    height: 20px;
    align-items: center;
    justify-content: flex-start;
    flex-direction: row;
`;

// ====== SVGs =====

export const iconSvg = (svgPath: string, dataCy?: string, fill?: string, rotation?: number): JSX.Element => {
    let { width, height } = pathBoundingBox(svgPath);

    return (
        <Svg fill='none' width={width}
            data-cy={dataCy ? dataCy : 'svg'}
            height={height}
            // transform={`rotate(${rotation || 0})`}
            style={{ marginRight: 10, transform: ([{ rotate: `${rotation || 0}deg` }]) }}>

            <Path d={svgPath} fill={fill || colors.$grey} />
        </Svg>
    );
};

export const getTitleSvg = (path: string) => {
    let boundingBox = pathBoundingBox(path);
    let { height, width } = boundingBox;

    return (
        <Svg data-cy='title-svg'
            width={width}
            height={height + 2}
            fill='none' style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 5, marginRight: 5 }} >
            <Path data-cy='svg-path'
                d={path} fill={colors.$blue} />
        </Svg>
    );
};