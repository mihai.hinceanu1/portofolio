import * as icons from '../../../shared/assets/icons';
import { SourceFileTreeLevel } from '../../interfaces/source-file-tree';

export function getLevelIcon(level: SourceFileTreeLevel, isOpen: boolean) {
    if (level.isFolder) {
        return icons.FOLDER_CLOSE;
    }

    if (isOpen) {
        return icons.FOLDER_OPEN;
    }

    return icons.FOLDER_CLOSE;
}