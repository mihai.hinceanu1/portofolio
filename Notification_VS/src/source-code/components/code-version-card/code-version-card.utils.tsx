import { CodeVersionCard } from './code-version-card';
import { ANGULAR, JAVA, REACT } from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import { StageCardAssets, StageCardCfg } from '../../interfaces/stage-card';

export const onStageCardPress = (cardName: string, context: CodeVersionCard) => {
    context.setState({
        cardName,
    });
};

export const stageCardFirstIteration: StageCardCfg = {
    title: 'First Stage',
    description: 'Long description created to describe the stage of the project and also to test the stage card ability to capture text',
    isActive: '',
    callback: () => onStageCardPress,
    assets: [
        {
            path: ANGULAR,
            fill: colors.$green,
        },
        {
            path: REACT,
            fill: colors.$black,
        },
        {
            path: JAVA,
        }
    ]
};

export const stageCardSecondIteration: StageCardCfg = {
    title: 'Second Stage',
    description: 'Long description created to describe the stage of the project and also to test the stage card ability to capture text',
    isActive: '',
    callback: () => onStageCardPress,
    assets: [
        {
            path: ANGULAR,
            fill: colors.$green,
        },
        {
            path: REACT,
            fill: colors.$black,
        },
        {
            path: JAVA,
        }
    ]
};

export const getStageCardConfig = (context: CodeVersionCard, config: StageCardCfg): StageCardCfg => {
    return (
        {
            title: config.title,
            description: config.description,
            isActive: context.state.cardName,
            callback: () => onStageCardPress(config.title, context),
            assets: getAssets(config.assets),
        }
    );
};

export const getAssets = (assets: StageCardAssets[]): StageCardAssets[] => {
    let stageCardAssets: StageCardAssets[] = [];

    assets.forEach(asset => {
        stageCardAssets.push(asset);
    });

    return stageCardAssets;
};