import * as div from './code-version-card.style';
import { getStageCardConfig } from './code-version-card.utils';
import { CodeVersionCardCfg } from '../../interfaces/code-version-card';
import { CodeVersionCardBody } from '../code-version-card-body/code-version-card-body';
import { StageCard } from '../stage-card/stage-card';
import { TimelineBar } from '../timeline-bar/timeline-bar';
import * as React from 'react';

interface Props {
    codeVersionCardCfg: CodeVersionCardCfg;
    isNarrow: boolean;
}

interface State {
    cardName: string;
}

/**
 * Components which encapsulates the TimelineBar
 * and the CodeVersionCardBody - used to have continuity
 * of the design
 */
export class CodeVersionCard extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            cardName: props.codeVersionCardCfg.stageCards[0].title,
        };
    }

    public render() {
        const { codeVersionCardCfg, isNarrow } = this.props;

        return (
            <div.CodeVersionCard data-cy='code-version-card' isNarrow={isNarrow}>

                {/* Stage Cards*/}
                {
                    <div.StageCards data-cy='stage-cards' isNarrow={isNarrow}>
                        {
                            !!codeVersionCardCfg &&
                            codeVersionCardCfg.stageCards.map((stageCard, index) => <StageCard isNarrow={isNarrow} key={index} config={getStageCardConfig(this, stageCard)} />)
                        }
                    </div.StageCards>
                }

                {/* Card Data Wrapper */}
                <div.CardDataWrapper data-cy='card-data-wrapper'>
                    {
                        !isNarrow &&
                        <TimelineBar {...codeVersionCardCfg.timelineBarCfg} overrides={'position: absolute; left:-35px;'} />
                    }

                    <CodeVersionCardBody data-cy='code-version-card-body' config={codeVersionCardCfg.codeVersionCardBodyCfg} isNarrow={isNarrow}/>

                </div.CardDataWrapper>

            </div.CodeVersionCard>
        );
    }

}