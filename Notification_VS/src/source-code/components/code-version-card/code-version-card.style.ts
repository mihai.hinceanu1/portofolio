import styled from 'styled-components/native';

interface CodeVersionCardContent {
    isNarrow: boolean;
}

export const CodeVersionCard = styled.View<CodeVersionCardContent>`
    ${props => getCodeVersionCardStyle(props.isNarrow)}
`;

export const StageCards = styled.View<CodeVersionCardContent>`
    ${props => getStageCardsWrapperStyle(props.isNarrow)}
`;

export const CardDataWrapper = styled.View`
    flex-grow: 1;
    width: 100%;
`;

export const getStageCardsWrapperStyle = (isNarrow: boolean) => {
    return isNarrow ?
        `display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
        margin-top: 30px;
        margin-left: 10px;`
        :
        `display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;`;
};

export const getCodeVersionCardStyle = (isNarrow: boolean) => {

    return !isNarrow ?
        `display: flex;
        flex-direction: row;
        justify-content: flex-start;
        align-items: flex-start;`
        :
        `display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;`;
};