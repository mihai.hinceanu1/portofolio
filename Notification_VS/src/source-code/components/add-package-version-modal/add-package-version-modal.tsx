import { Modal } from '../../../shared/components/modal/modal';
import * as overlay from '../../../shared/style/overlay.style';
import { AddPackageVersionFrom } from '../add-package-version-form/add-package-version-form';
import * as React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Subject } from 'rxjs';

interface Props {
    topicId?: string;
    packagePathName?: string;
    match?: any;
    callback?: () => void;
}

interface State {
    name: string;
    description: string;
    warningMessage: boolean;
}

export class AddPackageVersionModal extends React.Component<Props, State> {

    public destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            warningMessage: false,
            name: '',
            description: '',
        };
    }

    public render() {
        const { callback, packagePathName } = this.props;
        return (
            <LinearGradient colors={overlay.OVERLAY_GRADIENT}
                style={overlay.style.linearGradient}>
                <Modal callback={callback} data-cy='add-package-version-modal' hasBackgroundColor={false}>
                    <AddPackageVersionFrom callback={callback} packagePathName={packagePathName}/>
                </Modal>
            </LinearGradient>

        );
    }
}