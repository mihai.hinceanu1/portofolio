import * as div from './add-package-modal.style';
import { NEW_PACKAGE } from '../../../shared/assets/icons';
import { ModalBoxHeader } from '../../../shared/components/modal-box-header/modal-box-header';
import { Modal } from '../../../shared/components/modal/modal';
import { AddPackageForm } from '../add-package-form/add-package-form';
import React from 'react';

interface Props {
    closeModal: () => void;
}

/**
 * Modal opening when the user wants to create a new code package.
 */
export const AddPackageModal: React.FunctionComponent<Props> = (props: Props) => {
    const { closeModal } = props;

    return (
        <Modal callback={closeModal}>
            <div.AddPackageModal data-cy='add-package-modal'>

                {/** Header */}
                <ModalBoxHeader icon={NEW_PACKAGE}
                    title='CREATE NEW PACKAGE'
                    closeModal={closeModal} />

                {/** Form */}
                <AddPackageForm closeModal={closeModal} />
            </div.AddPackageModal>
        </Modal>
    );
}