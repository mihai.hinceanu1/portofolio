import { RIGHT_FRAME_CORNER } from './_assets/framed-image.assets';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import Svg, { Line, Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface Overrides {
    override: string;
}

interface FramedImageProps {
    width?: number;
    titleColor?: string;
}

export const FramedImage = styled.View<Overrides>`
    display: flex;
    flex-direction:row;
    justify-content: center;
    align-items: center;
    margin: 15px;
    ${props => props.override};
`;

export const Title = styled.View`
    min-width: 60px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: -20px;
`;

export const TitleText = styled.Text<FramedImageProps>`
    text-align: left;
    font-size: 17px;
    font-weight: 600;
    color: ${props => getPathFill(props.titleColor)};
`;

export const ImageCluster = styled.View<FramedImageProps>`
    height: 90%;
    width: ${props => props.width}px;
    position: absolute;
    justify-content: center;
    align-items: center;
    z-index: -5;
`;

export const Image = styled.Image<FramedImageProps>`
    height: 90%;
    width: ${props => props.width - 20}px;
`;

export const RoundedBgr = styled.View`
    background-color: ${colors.$blue};
    position: absolute;
    justify-content: center;
    align-items: center;
    right: -7px;
    bottom: -5px;
    width: 35px;
    height: 35px;
    border-radius: 17.5px;
`;

export const DeleteButton = styled.TouchableOpacity`
    background-color: ${colors.$blue};
    position: absolute;
    justify-content: center;
    align-items: center;
    left: -10px;
    top: -15px;
    width: 40px;
    height: 40px;
    border-radius: 20px;
`;

export const SvgTitleWrapper = styled.View`
    margin-right: 15px;
`;

// ====== UTILS ======

let calculatedHeight = 0;

export const getPathFill = (fill: string) => {
    if (fill) {
        return fill;
    } else {
        return '#0D8EF1';
    }
};

export const frameSvg = (svgPath: string, fill: string) => {
    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;
    calculatedHeight = height;

    return (
        <Svg data-cy='frame'
            width={width}
            height={height + 2}
            fill='none' style={{ backgroundColor: 'none', justifyContent: 'center', alignItems: 'center' }} >
            <Path data-cy='svg-path'
                d={svgPath} fill={getPathFill(fill)} />
        </Svg>
    );
};

export const getFrameExtension = (extraWidth: number, fill: string) => {

    return (
        <Svg data-cy='frame-extension'
            height={calculatedHeight}
            width={extraWidth}
            style={{ backgroundColor: 'none', position: 'relative', marginTop: 11 }}>
            <Line x1='0' y1='1.5' x2={extraWidth.toString()} y2='1.5' stroke={getPathFill(fill)} strokeWidth='1.2' />
            <Line x1='0' y1={calculatedHeight - 7} x2={extraWidth.toString()} y2={calculatedHeight - 7} stroke={getPathFill(fill)} strokeWidth='1.2' />
            <Line x1={extraWidth.toString()} y1='1.5' x2={extraWidth.toString()} y2={calculatedHeight - 7} stroke={getPathFill(fill)} strokeWidth='2.5' />
        </Svg>
    );
};

export const getFrameCorner = (fill: string) => {
    let boundingBox = pathBoundingBox(RIGHT_FRAME_CORNER);
    let { height, width } = boundingBox;

    return (
        <Svg data-cy='frame-corner'
            width={width}
            height={height}
            fill='none' style={{ backgroundColor: 'none', justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 10, top: -0.1 }} >
            <Path data-cy='svg-path'
                d={RIGHT_FRAME_CORNER} fill={getPathFill(fill)} />
        </Svg>
    );
};

export const getTitleSvg = (path: string, fill: string) => {
    let boundingBox = pathBoundingBox(path);
    let { height, width } = boundingBox;

    return (
        <SvgTitleWrapper>
            <Svg data-cy='title-svg'
                width={width}
                height={height + 2}
                fill='none' style={{ justifyContent: 'center', alignItems: 'center' }} >
                <Path data-cy='svg-path'
                    d={path} fill={getPathFill(fill)} />
            </Svg>
        </SvgTitleWrapper>
    );
};

export const getFileTypeSvg = (path: string, fill: string) => {
    let boundingBox = pathBoundingBox(path);
    let { height, width } = boundingBox;

    return (
        <RoundedBgr>
            <Svg data-cy='file-type-svg'
                width={width}
                height={height + 2}
                fill='none' style={{ justifyContent: 'center', alignItems: 'center' }} >
                <Path data-cy='svg-path'
                    d={path} fill={getPathFill(fill)} />
            </Svg>
        </RoundedBgr>

    );
};
