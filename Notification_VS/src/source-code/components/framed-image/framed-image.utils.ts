import { CUSTOM_ICON } from '../../../shared/components/dropdown/_assets/dropdown.assets';

export const MAX_WIDTH = 400;

export const getDropdownCfg = (editCallback: () => void, deleteCallback: () => void) => {

    return {
        options: [
            {
                _id: 'edit',
                label: 'Edit',
                callback: () => editCallback()
            },
            {
                _id: 'delete',
                label: 'Delete',
                callback: () => deleteCallback()
            }
        ],
        icon: {
            svgData: CUSTOM_ICON,
            fill: '#000000',
        },
    };
};