import { FRAME } from './_assets/framed-image.assets';
import * as div from './framed-image.style';
import { getDropdownCfg, MAX_WIDTH } from './framed-image.utils';
import { Dropdown } from '../../../shared/components/dropdown/dropdown';
import { APP_CFG } from '../../../shared/config/app.config';
import { FramedImageCfg } from '../../interfaces/framed-image';
import * as React from 'react';
import { Image } from 'react-native';

interface Props {
    config: FramedImageCfg;
    override?: string;
    isNarrow?: boolean;
    onPressDelete?: () => void;
    onPressEdit?: () => void;
}

interface State {
    width: number;
}

/**
 * Its main purpose is to highlight code selections in Source Code Admin
 *
 * <!> WARNING
 * The minimum image width should be at least 1px
 * Considering that if it will be under 1px the component
 * wouldn't be able to draw the ending line
 */
export class FramedImage extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            width: 0,
        };
    }

    public render() {
        const { width } = this.state;
        const { config, override, isNarrow, onPressDelete, onPressEdit } = this.props;
        const { editButton } = this.props.config;

        return (
            <div.FramedImage data-cy='framed-image' override={override}>

                {/* Image */}
                {
                    !!width &&
                    <div.ImageCluster width={width} data-cy='image-cluster'>
                        <div.Image width={width} source={{ uri: `${APP_CFG.uploads}${config.imagePath}` }} />
                    </div.ImageCluster>
                }

                {/* Title */}
                <div.Title data-cy='title'>
                    {
                        div.getTitleSvg(config.title.iconPath, config.title.titleColor)
                    }
                    <div.TitleText data-cy='title-text' titleColor={config.title.titleColor}>
                        {config.title.name}
                    </div.TitleText>
                </div.Title>

                {/* Gets The Frame */}
                {
                    div.frameSvg(FRAME, 'blue')
                }

                {/* Scales the frame*/}
                {/* width - 59 represents the minimum width that a framed image should be in order to be
                rendered, the 340 width used for isNarrow because we need the image to be scaled at the
                maximum size that fits in the mobile display*/}
                {
                    !!width &&
                    div.getFrameExtension(isNarrow && width > 350 ? 340 : width - 59, 'blue')
                }

                {/* Right corner design */}
                {
                    div.getFrameCorner('blue')
                }

                {/* File type (ex: Java, Py, Node )*/}
                {
                    div.getFileTypeSvg(config.fileTypeSvg, config.fileTypeSvgFill)
                }

                {
                    editButton &&
                    <div.DeleteButton>
                        <Dropdown config={getDropdownCfg(() => onPressEdit(), () => onPressDelete())} />
                    </div.DeleteButton>
                }
            </div.FramedImage>
        );
    }

    public componentDidMount() {
        const { config } = this.props;

        this.getImgSize(config.imagePath);
    }

    private getImgSize(imgPath: string) {
        const { isNarrow } = this.props;

        Image.getSize(`${APP_CFG.uploads}${imgPath}`, (width) => {
            if (width > MAX_WIDTH) {
                width = MAX_WIDTH;

            }

            if (isNarrow && width > 330) {
                width = 330;
            }

            this.setState({ width });
        }, (err: Error) => { console.log(err); });
    }
}

