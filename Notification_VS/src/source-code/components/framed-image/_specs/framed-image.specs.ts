/// <reference types="cypress" />

describe('Framed Image Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/shared/framed-image');
    });

    it('Component Is Rendered', () => {
        getElm('standard', 'framed-image').should('exist');
    });

    it('Image is Rendered', () => {
        getElm('standard', 'image-cluster').should('exist');
    });

    it('Frame Is Rendered', () => {
        getElm('standard', 'frame').should('exist');
    });

    it('Frame Extension is Rendered', () => {
        getElm('standard', 'frame-extension').should('exist');
    });

    it('Frame Corner Is Rendered', () => {
        getElm('standard', 'frame-corner').should('exist');
    });

    it('Title is Rendered', () => {
        getElm('standard', 'title').should('exist');
    });

    it('Title text', () => {
        getElm('standard', 'title-text').should(title => {
            expect(title.text()).to.eq('Framed Image');
        });
    });

    it('File Type is Rendered', () => {
        getElm('standard', 'file-type-svg').should('exist');
    });

    it('Title Svg is Rendered', () => {
        getElm('standard', 'title-svg').should('exist');
    });

    it('Custom Title Color', () => {
        getElm('custom-title-color', 'title-text').should('have.css', 'color', 'rgb(0, 0, 0)');
    });

    it('Custom Title Svg Color', () => {
        getElm('custom-title-color', 'svg-path').should('have.css', 'fill', 'rgb(0, 0, 0)');
    });
});

// ====== SELECTORS ======

function getElm(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}