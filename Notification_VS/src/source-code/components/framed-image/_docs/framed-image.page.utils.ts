import * as icn from '../../../../shared/assets/icons';
import { colors } from '../../../../shared/style/colors';
import { FramedImageCfg } from '../../../interfaces/framed-image';

export const StandardFramedCfg: FramedImageCfg = {
    imagePath: '/lesson-catalog/example7.png',
    fileTypeSvg: icn.EXPERT,
    fileTypeSvgFill: 'white',
    title: {
        name: 'Framed Image',
        iconPath: icn.REACT,
    }
};

export const CustomImgCfg: FramedImageCfg = {
    imagePath: '/lesson-catalog/example2.png',
    fileTypeSvg: icn.POPULAR,
    fileTypeSvgFill: 'white',
    title: {
        name: 'Framed Image Customized',
        iconPath: icn.ANGULAR,
    }
};

export const CustomTitleCfg: FramedImageCfg = {
    imagePath: '/lesson-catalog/example1.png',
    fileTypeSvg: icn.ALL,
    fileTypeSvgFill: 'white',
    title: {
        name: 'Code Version 3',
        iconPath: icn.ANGULAR,
    }
};

export const CustomFileTypeCfg: FramedImageCfg = {
    imagePath: '/lesson-catalog/example6.png',
    fileTypeSvg: icn.LATEST,
    fileTypeSvgFill: 'red',
    title: {
        name: 'Code Version 3',
        iconPath: icn.ANGULAR,
    }
};

export const OverrideCfg: FramedImageCfg = {
    imagePath: '/lesson-catalog/example3.png',
    fileTypeSvgFill: 'black',
    fileTypeSvg: icn.EXPERT,
    title: {
        name: 'Code Version 3',
        iconPath: icn.JAVA,
    }
};

export const CustomTitleColor: FramedImageCfg = {
    imagePath: '/lesson-catalog/example9.png',
    fileTypeSvgFill: 'black',
    fileTypeSvg: icn.EXPERT,
    title: {
        titleColor: colors.$black,
        name: 'Code Version 3',
        iconPath: icn.JAVA,
    }
};