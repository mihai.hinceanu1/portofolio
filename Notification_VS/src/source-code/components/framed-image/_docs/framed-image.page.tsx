import * as cfg from './framed-image.page.utils';
import * as samples from './framed-image.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { FramedImage } from '../framed-image';
import * as React from 'react';

export class FramedImagePage extends ScrollableDocPage<{}> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state
        };
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Framed Image
                        </div.OverviewTitle>
                    <div.OverviewDescription>
                        Framed Image will be used in Source Code Page. It will
                        highlight a selected portion of code.
                        It will automatically resize by the width of the image
                        containing the selected code.
                        </div.OverviewDescription>
                </div.Overview>

                {/** Standard */}
                <div.Demo data-cy='standard' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <FramedImage override={'top: 50px;'} config={cfg.StandardFramedCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.STANDARD_FRAMED} />
                </div.Demo>

                {/** Custom Image */}
                <div.Demo data-cy='custom-image' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Image
                        </div.DemoTitle>
                        <FramedImage override={'top: 50px;'} config={cfg.CustomImgCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_IMAGE} />
                </div.Demo>

                {/* Custom Title */}
                <div.Demo data-cy='custom-title' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Title
                        </div.DemoTitle>
                        <FramedImage override={'top: 50px;'} config={cfg.CustomTitleCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_TITLE} />
                </div.Demo>

                {/* Custom File Type */}
                <div.Demo data-cy='custom-file-type' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom File Type
                        </div.DemoTitle>
                        <FramedImage override={'top: 50px;'} config={cfg.CustomFileTypeCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CUSTOM_FILE_TYPE} />
                </div.Demo>

                {/* Override */}
                <div.Demo data-cy='override' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Override
                        </div.DemoTitle>
                        <FramedImage override={'opacity: 0.8; top: 50px;'} config={cfg.OverrideCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.OVERRIDE} />
                </div.Demo>

                {/* Custom Title */}
                <div.Demo data-cy='custom-title-color' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Title Color
                        </div.DemoTitle>
                        <FramedImage override={'top: 50px;'} config={cfg.CustomTitleColor} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={samples.CustomTitleColor} />
                </div.Demo>
            </>
        );
    }
}