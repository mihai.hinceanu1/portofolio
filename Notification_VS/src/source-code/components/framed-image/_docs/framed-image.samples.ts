export const STANDARD_FRAMED = `
export const StandardFramedCfg: FramedImageCfg = {
    imagePath: '/lesson-catalog/example7.png',
    fileTypeSvg: ALL,
    title: {
        name: 'Framed Image',
        iconPath: REACT,
    }
};`;

export const CUSTOM_IMAGE = `
export const StandardFramedCfg: FramedImageCfg = {
    imagePath: 'CustomPath',
};`;

export const CUSTOM_TITLE = `
export const StandardFramedCfg: FramedImageCfg = {
    title: {
        name: 'CustomName',
        iconPath: CustomIcon,
    }
};`;

export const CUSTOM_FILE_TYPE = `
export const StandardFramedCfg: FramedImageCfg = {
    fileTypeSvg: CustomSvg,
};`;

export const OVERRIDE = `
<FramedImage
    override={'opacity: 0.8;'}
    config={cfg} />`;

export const CustomTitleColor = `
export const CustomTitleColor: FramedImageCfg = {
    title: {
        titleColor: colors.$black,
        name: 'Code Version 3',
        iconPath: JAVA,
    }
};`;