import * as div from './edit-package-form.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { Textarea } from '../../../shared/components/textarea/textarea';
import { ICodePackage } from '../../interfaces/packages';
import { editPackage } from '../../services/packages.service';
import React from 'react';

interface Props {
    closeModal: () => void;
    codePackage: ICodePackage;
}
interface State {
    name: string;
    description: string;
}

/**
 * The form with the editable data.
 * Also contains and handles the submit/cancel functionalities.
 */
export class EditPackageForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            name: !!props.codePackage ? props.codePackage.name : '',
            description: !!props.codePackage ? props.codePackage.description : '',
        };
    }

    public render() {
        const { closeModal } = this.props;
        const { name, description } = this.state;

        return (
            <div.EditPackageForm data-cy='edit-package-form'>

                {/** Name field */}
                <Input value={name}
                    label='Package name'
                    onChangeText={(text) => this.handleTextChange(text, 'name')}
                    overrides={div.INPUT_OVERRIDES} />

                {/** Description field */}
                <Textarea value={description}
                    label='Package description'
                    onChangeText={(text) => this.handleTextChange(text, 'description')}
                    overrides={div.TEXTAREA_OVERRIDES} />

                {/** Submit/Cancel */}
                <div.Actions data-cy='actions'>
                    <Button config={{
                        text: 'Cancel',
                        onPress: closeModal,
                    }} />

                    <Button config={{
                        text: 'Save',
                        onPress: () => this.handleSubmit(),
                    }} />
                </div.Actions>
            </div.EditPackageForm>
        );
    }

    private handleTextChange(text: string, field: string) {
        this.setState({
            ...this.state,
            [field]: text,
        });
    }

    private handleSubmit() {
        const { closeModal, codePackage } = this.props;
        const { name, description } = this.state;
        const { pathName, image, awards, reactions, milestone, comments, newComments, codeLines, technologyStack, deprecated, _id, isActive, authorId, created, updated } = codePackage;

        let updatedPackage: ICodePackage = {
            name,
            description,
            pathName,
            image,
            awards,
            reactions,
            milestone,
            comments,
            newComments,
            codeLines,
            technologyStack,
            deprecated,
            _id,
            isActive,
            authorId,
            created,
            updated
        };
        editPackage(updatedPackage);
        closeModal();
    }
}