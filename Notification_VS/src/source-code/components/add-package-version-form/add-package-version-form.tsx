import * as div from './add-package-version-form.style';
import * as utils from './add-package-version-form.utils';
import { NEW_VERSION } from '../../../shared/assets/icons';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { CreatePackageVersionCfg } from '../../interfaces/package-version';
import { createPackageVersion } from '../../services/package-version.service';
import * as React from 'react';
import { Subject } from 'rxjs';

interface Props {
    callback?: () => void;
    packagePathName: string;
}

interface State {
    name: string;
    description: string;
    validationError: string;
    sourcefile: string;
}

export class AddPackageVersionFrom extends React.Component<Props, State> {

    public destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            validationError: '',
            name: '',
            description: '',
            sourcefile: '',
        };
    }

    public render() {
        const { name, description, validationError, sourcefile } = this.state;

        return (
            <div.AddPackageVersionForm>
                <div.Header>
                    {
                        div.getFormIcon(NEW_VERSION)
                    }

                    <div.FormTitle>
                        CREATING NEW VERSION
                    </div.FormTitle>

                </div.Header>

                <div.Body>

                    <Input data-cy='version-name-input'
                        placeholder={'Version name...'}
                        label='Name'
                        error={validationError}
                        value={name}
                        overrides={utils.VERSION_NAME_INPUT_OVERRIDES}
                        onChangeText={(text: string) => this.onChangeName(text)} />

                    <Input data-cy='source-file-input'
                        placeholder={'Select your sourcefile...'}
                        label='Sourcefile'
                        error={validationError}
                        value={sourcefile}
                        overrides={utils.SOURCE_FILE_INPUT_OVERRIDES}
                        onChangeText={(text: string) => this.onChangeSourcefileName(text)} />

                    <div.DescriptionWrapper>
                        <div.DescriptionLabel>
                            Description
                        </div.DescriptionLabel>

                        <div.Description data-cy='description-input'
                            underlineColorAndroid={'none'}
                            placeholder='Description...'
                            placeholderTextColor='#c4c4c4'
                            numberOfLines={10}
                            value={description}
                            onChangeText={(text: string) => this.onChangeDescription(text)}
                            multiline={true}
                        />

                    </div.DescriptionWrapper>

                </div.Body>

                <div.Footer>
                    <Button
                        overrides={{ root: 'margin: 0px 15px 0px 0px;' }}
                        config={{ text: 'Cancel', onPress: () => this.cancelButton() }} />

                    <Button
                        overrides={{ root: 'margin: 0;' }}
                        config={{ text: 'Submit', onPress: () => this.insertPackageInDB() }} />
                </div.Footer>
            </div.AddPackageVersionForm>
        );
    }

    private onChangeName(text: string) {
        this.setState({
            name: text,
        });
    }

    private onChangeDescription(text: string) {
        this.setState({
            description: text,
        });
    }

    private onChangeSourcefileName(text: string) {
        this.setState({
            sourcefile: text,
        });
    }

    private cancelButton() {
        let { callback } = this.props;
        !!callback && callback();
    }

    private insertPackageInDB() {
        let { callback, packagePathName } = this.props;
        let { name, description } = this.state;
        let configToSend: CreatePackageVersionCfg = {
            name: name,
            description: description,
            packagePathName: packagePathName,
        };

        if (name.length < 5) {
            this.setState({
                validationError: 'Name is too short.',
            });
        } else {
            this.setState({
                validationError: '',
            });
            createPackageVersion(configToSend);
            callback && callback();
        }
    }
}