import { AddPackageVersionFrom } from './add-package-version-form';
import { DROPDOWN_TYPE } from '../../../shared/constants/dropdown.const';
import { DropdownConfig, IDropdownOption } from '../../../shared/interfaces/dropdown';
import { PackageVersion } from '../../interfaces/package-version';

// UNUSED YET - Don't Delete
export const getAddPackageVersionDropdownCfg = (_context: AddPackageVersionFrom, options: PackageVersion[]): DropdownConfig => {
    let dropdownOptions: IDropdownOption[] = [];

    options.map((option) => {
        let IDropdownOption: IDropdownOption = {
            _id: option._id,
            label: option.name,
        };

        dropdownOptions.push(IDropdownOption);
    });

    return ({
        placeholder: 'Select from version files',
        options: dropdownOptions,
        selected: { _id: '', label: '' },
        type: DROPDOWN_TYPE.OnlyInput
    });
};

export const SOURCE_FILE_INPUT_OVERRIDES = {
    root: 'width: 100%; margin: 15px 0px 0px 0px; padding: 0px'
};

export const VERSION_NAME_INPUT_OVERRIDES = {
    root: 'width: 100%; margin: 0; padding: 0px;'
};