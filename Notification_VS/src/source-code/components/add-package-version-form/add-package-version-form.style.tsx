import { Icon } from '../../../icons/components/icon/icon';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import * as React from 'react';
import styled from 'styled-components/native';

export const AddPackageVersionForm = styled.View`
    width: 500px;
    height: 600px;
    display: flex;
    flex-direction: column;
`;

export const Header = styled.View`
    background-color: ${colors.$blue};
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 20%;
    width: 100%;
`;

export const Body = styled.View`
    padding: 40px;
    height: 65%;
    width: 100%;
    display: flex;
    flex-direction: column;
    background-color: white;
    flex-grow: 1;
`;

export const Footer = styled.View`
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    background-color: white;
    height: 15%;
    padding-right: 40px;
`;

export const FormTitle = styled.Text`
    font-size: 28px;
    color: white;
    font-weight: 600;
`;

export const Description = styled.TextInput`
    width: 100%;
    height: 100px;
    outline-color: none;
    outline-width: 0px;
    padding: 5px;
    border-color: ${colors.$grey};
    border-width: 1px;
    border-style: solid;
`;

export const DescriptionWrapper = styled.View`
    margin: 20px 0px 0px 0px;
    justify-content: flex-start;
    align-items: flex-start;
`;

export const DescriptionLabel = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$labelColor};
    margin-bottom: 3px;
`;

// ====== UTILS ======

export const getFormIcon = (svgPath: string) => {
    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;

    return (
        <Icon path={svgPath} height={height + 2} width={width + 1} color={colors.$white} />
    );
};