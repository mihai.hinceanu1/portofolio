import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { BOX_SHADOW_CSS, getShadow } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

interface ContentProps {
    isNarrow: boolean;
}

export const StatisticsCards = styled.View<ContentProps>`
    ${props => getStatisticsCardStyle(props.isNarrow)};
`;

export const Card = styled.View`
    width: 100%;
    height: 25%;
    padding: 15px;
    align-items: flex-start;
    justify-content: center;
    ${getShadow(BOX_SHADOW_CSS)};
`;

export const CardTitle = styled.Text`
    font-size: ${font.$size18}px;
    font-weight: 800;
`;

export const CardDescription = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$darkGrey};
`;

export const getStatisticsCardStyle = (isNarrow: boolean) => {
    if (isNarrow) {
        return `
        width: 100%;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;`;
    } else {
        return `width: 25%;
        margin-left: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;`;
    }
};