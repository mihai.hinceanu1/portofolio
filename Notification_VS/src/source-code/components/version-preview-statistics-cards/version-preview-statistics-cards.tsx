import * as div from './version-preview-statistics-cards.style';
import * as React from 'react';

interface Props {
    isNarrow: boolean;
 }

interface State { }

export class VersionPreviewStatisticsCards extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { isNarrow } = this.props;

        return (
            <div.StatisticsCards data-cy='statistics-cards' isNarrow={isNarrow}>
                <div.Card>
                    <div.CardTitle>
                        2362
                    </div.CardTitle>

                    <div.CardDescription>
                        Package Version
                    </div.CardDescription>
                </div.Card>

                <div.Card>
                    <div.CardTitle>
                        13
                    </div.CardTitle>

                    <div.CardDescription>
                        Package Purchases
                    </div.CardDescription>
                </div.Card>

                <div.Card>
                    <div.CardTitle>
                        #4
                    </div.CardTitle>

                    <div.CardDescription>
                        Rank 4 in your packages list
                    </div.CardDescription>
                </div.Card>
            </div.StatisticsCards>
        );
    }
}