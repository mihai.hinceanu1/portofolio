import * as div from './add-dependency-modal.style';
import { DEPENDENCIES } from '../../../shared/assets/icons';
import { ModalBoxHeader } from '../../../shared/components/modal-box-header/modal-box-header';
import { Modal } from '../../../shared/components/modal/modal';
import { AddDependencyForm } from '../add-dependency-form/add-dependency-form';
import React from 'react';

interface Props {
    closeModal: () => void;
    pathName: string;
}

/**
 * Modal opening when the user wants to add a new dependency to a version.
 */
export const AddDependencyModal: React.FunctionComponent<Props> = (props: Props) => {
    const { closeModal, pathName } = props;

    return (
        <Modal callback={closeModal}>
            <div.AddDependencyModal data-cy='add-dependency-modal'>

                {/** Header */}
                <ModalBoxHeader icon={DEPENDENCIES}
                    title='ADD DEPENDENCY'
                    closeModal={closeModal} />

                {/** Form */}
                <AddDependencyForm closeModal={closeModal} pathName={pathName} />
            </div.AddDependencyModal>
        </Modal>
    );
};