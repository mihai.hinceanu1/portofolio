import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const AddDependencyModal = styled.View`
    width: 50%;
    min-width: 300px;
    max-width: 450px;
    min-height: 300px;
    background-color: ${colors.$white};
    border-radius: 2px;
`;