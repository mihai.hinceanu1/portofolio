import { Icon } from '../../../icons/components/icon/icon';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import styled from 'styled-components/native';

interface Overrides {
    overrides: string;
}

interface Narrow {
    isNarrow: boolean;
}

export const STAGE_CARD_WIDTH = 250;

export const StageCard = styled.TouchableOpacity<Overrides & Narrow>`
    ${props => getStageCardStyle(props.isNarrow, props.overrides)};
`;

export const ActiveBar = styled.View<Narrow>`
    position: absolute;
    background-color: ${colors.$blue};
    width: ${props => props.isNarrow ? '3px' : '1px'};
    height: 100%;
    left: -1px;
`;

export const Title = styled.Text<Narrow>`
    font-size: 18px;
    text-align: left;
    font-weight: 600;
    ${props => props.isNarrow && 'padding: 10px;'}

`;

export const Description = styled.Text`
    margin-top: 5px;
    font-size: 11px;
    color: ${colors.$darkGrey};
    text-align: left;
`;

export const Assets = styled.View`
    display: flex;
    flex-direction:row;
    margin-top: 20px;
`;

const SvgWrapper = styled.View`
    margin-right: 25px;
`;

// ====== UTILS ======

export const getStageCardStyle = (isNarrow: boolean, overrides: string) => {
    if (isNarrow) {
        return `background-color: white;
        margin-right: 5px;`;
    } else {
        return `padding: 30px 60px 30px 40px;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: center;
        background-color: white;
        width: ${STAGE_CARD_WIDTH}px;
        ${overrides};`;
    }
};

export const getAsset = (svgPath: string, fill?: string) => {

    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;
    let color = getPathFill(fill);

    return (
        <SvgWrapper key={svgPath}>
            <Icon path={svgPath} color={color} height={height} width={width} />
        </SvgWrapper>
    );
};

const getPathFill = (fill: string) => {
    if (fill) {
        return fill;
    } else {
        return '#0D8EF1';
    }
};