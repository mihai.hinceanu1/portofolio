import * as div from './stage-card.style';
import { StageCardCfg } from '../../interfaces/stage-card';
import * as React from 'react';

interface Props {
    overrides?: string;
    config: StageCardCfg;
    isNarrow?: boolean;
}

export const StageCard: React.StatelessComponent<Props> = (props: Props) => {

    const { overrides, config, isNarrow } = props;
    const { title, description, assets, callback, isActive } = config;
    const pressed = (isActive === title);

    return (
        <div.StageCard data-cy='stage-card'
            isNarrow={isNarrow}
            overrides={overrides}
            activeOpacity={1}
            onPress={() => callback()}>

            {/* Side Bar Representing Status (active/inactive)*/}
            {
                pressed &&
                <div.ActiveBar data-cy='active-bar' isNarrow={isNarrow}/>
            }

            {/* Title */}
            <div.Title data-cy='title' isNarrow={isNarrow}>
                {title}
            </div.Title>

            {/* Description */}
            {
                !isNarrow &&
                <>
                    <div.Description data-cy='description'>
                        {description}
                    </div.Description>
                    {/* Assets */}
                    <div.Assets data-cy='assets'>
                        {
                            assets.map((asset) => div.getAsset(asset.path, asset.fill))
                        }
                    </div.Assets>
                </>
            }
        </div.StageCard>
    );
};