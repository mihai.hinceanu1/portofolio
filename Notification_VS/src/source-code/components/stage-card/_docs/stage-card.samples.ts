export const STANDARD = `
<StageCard config={StageCardConfig} />

export const StageCardConfig: StageCardCfg = {
    title: 'Web App',
    description: description,
    isActive: false,
    callback: callback
    assets: {path, fill}[]
};`;

export const CUSTOM_ICONS = `
export const StageCardConfig: StageCardCfg = {
    // Custom fill for each icon
    assets: {path, 'red'}[]
};
`;

export const CUSTOM_TITLE_DESCRIPTION = `
export const StageCardConfig: StageCardCfg = {
    title: 'Upgrading Server',
    // Add any description in description prop
    description: CustomDescription,
}`;

export const CALLBACK = `
export const StageCardConfig: StageCardCfg = {
    callback: () => CustomCallbackFunction();
}`;