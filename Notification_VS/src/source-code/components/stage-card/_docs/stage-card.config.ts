import * as icn from '../../../../shared/assets/icons';
import { colors } from '../../../../shared/style/colors';
import { StageCardCfg } from '../../../interfaces/stage-card';

export const stageCardConfig: StageCardCfg = {
    title: 'Web App',
    description: 'Long description created to describe the stage of the project and also to test the stage card ability to capture text',
    isActive: '',
    callback: () => { },
    assets: [
        {
            path: icn.ANGULAR,
            fill: colors.$green,
        },
        {
            path: icn.REACT,
            fill: colors.$black,
        },
        {
            path: icn.JAVA,
        }
    ]
};

export const customIcons: StageCardCfg = {
    title: 'Web App',
    description: 'Long description created to describe the stage of the project and also to test the stage card ability to capture text',
    isActive: '',
    callback: () => { },
    assets: [
        {
            path: icn.ANGULAR,
            fill: colors.$black,
        },
        {
            path: icn.REACT,
            fill: colors.$red,
        },
        {
            path: icn.JAVA,
            fill: colors.$grey,
        }
    ]
};

export const customTitleDescription: StageCardCfg = {
    title: 'Upgrading Server',
    description: 'Server was upgraded in order to support image scaling for mobile and web use cases',
    isActive: '',
    callback: () => { },
    assets: [
        {
            path: icn.ANGULAR,
            fill: colors.$black,
        },
        {
            path: icn.REACT,
            fill: colors.$red,
        },
        {
            path: icn.JAVA,
            fill: colors.$grey,
        }
    ]
};

export const overrides: StageCardCfg = {
    title: 'Upgrading Server',
    description: 'Server was upgraded in order to support image scaling for mobile and web use cases',
    isActive: '',
    callback: () => { },
    assets: [
        {
            path: icn.ANGULAR,
            fill: colors.$black,
        },
        {
            path: icn.REACT,
            fill: colors.$red,
        },
        {
            path: icn.JAVA,
            fill: colors.$grey,
        }
    ]
};

export const callback: StageCardCfg = {
    title: 'Upgrading Server',
    description: 'Server was upgraded in order to support image scaling for mobile and web use cases',
    isActive: '',
    callback: () => { },
    assets: [
        {
            path: icn.ANGULAR,
            fill: colors.$black,
        },
        {
            path: icn.REACT,
            fill: colors.$red,
        },
        {
            path: icn.JAVA,
            fill: colors.$grey,
        }
    ]
};