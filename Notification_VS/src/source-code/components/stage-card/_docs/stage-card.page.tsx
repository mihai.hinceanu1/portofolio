import * as cfg from './stage-card.config';
import * as samples from './stage-card.samples';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { StageCard } from '../stage-card';
import * as React from 'react';

interface StageCardState {
    isActive: string;
}

export class StageCardPage extends ScrollableDocPage<StageCardState> {
    public callbackCfg = cfg.callback;

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
            overrides: {
                isActive: '',
            },
        };

    }

    public renderDemoPage() {
        const { width } = this.state;
        const { isActive } = this.state.overrides;

        this.callbackCfg.callback = () => this.onCardPress();
        this.callbackCfg.isActive = isActive;

        return (
            <div.Overview width={width} data-cy='stage-card-page'>
                <div.OverviewTitle>
                    Stage Card
                </div.OverviewTitle>

                <div.OverviewDescription>
                    Represents the process stage of building a project
                    by an editor
                </div.OverviewDescription>

                {/* Default */}
                <div.Demo data-cy='default' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Default Stage Card
                            </div.DemoTitle>

                        <StageCard config={cfg.stageCardConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.STANDARD} />
                </div.Demo>

                {/* Custom Icons */}
                <div.Demo data-cy='custom-icons' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Icons
                            </div.DemoTitle>

                        <StageCard config={cfg.customIcons} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CUSTOM_ICONS} />
                </div.Demo>

                {/* Custom Title And Description */}
                <div.Demo data-cy='custom-title-description' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Title and Description
                            </div.DemoTitle>

                        <StageCard config={cfg.customTitleDescription} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CUSTOM_TITLE_DESCRIPTION} />
                </div.Demo>

                {/* Callback */}
                <div.Demo data-cy='callback' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Callback
                        </div.DemoTitle>

                        <div.CallbackResult data-cy='callback-result'>
                            Card is active: {isActive.toString()}
                        </div.CallbackResult>

                        <StageCard config={this.callbackCfg} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.CALLBACK} />
                </div.Demo>

                {/* Overrides */}
                <div.Demo data-cy='override' width={width} wide={true}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Overrides
                            </div.DemoTitle>

                        <StageCard config={cfg.overrides} overrides={'width: 500px;'} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={`<StageCard config={cfg.overrides} overrides={'width: 500px;'}/>`} />
                </div.Demo>
            </div.Overview>
        );
    }

    private onCardPress() {
        this.setState({
            ...this.state,
            overrides: {
                ...this.state.overrides,
                isActive: '',
            },
        });
    }
}