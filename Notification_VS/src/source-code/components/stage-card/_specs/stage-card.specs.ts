/// <reference types="cypress" />

describe('Stage Card Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/source-code/stage-card');
    });

    it('Card is Rendered', () => {
        selectElem('default', 'stage-card').should('exist');
    });

    it('Title is Rendered', () => {
        selectElem('default', 'title').should(div => {
            expect(div.text()).to.eq('Web App');
        });
    });

    it('Description is Rendered', () => {
        selectElem('default', 'description').should('exist');
    });

    it('Assets are Rendered', () => {
        selectElem('default', 'assets').children().should('have.length', 3);
    });

    it('Calls Callback Method', () => {
        selectElem('callback', 'stage-card').click();
        selectElem('callback', 'callback-result').should(div => {
            expect(div.text()).to.eq('Card is active: true');
        });
    });

    it('Active Bar is Rendered', () => {
        selectElem('callback', 'active-bar').should('exist');
    });

    it('Svg Fill', () => {
        selectElem('default', 'svg-path').eq(0).should('have.css', 'fill', 'rgb(0, 195, 167)');
    });

    it('Overrides', () => {
        selectElem('override', 'stage-card').should('have.css', 'width', '500px');
    });

});

function selectElem(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}