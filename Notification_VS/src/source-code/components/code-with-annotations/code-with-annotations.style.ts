import styled from 'styled-components/native';

export const LeftView = styled.View`
    width: 300px;
    height: 600px;
    overflow: hidden;
`;

export const RightView = styled.View``;

export const CodeWithAnnotations = styled.View`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;
`;