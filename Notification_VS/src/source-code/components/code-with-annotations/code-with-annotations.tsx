import * as div from './code-with-annotations.style';
import { CodeEditor } from '../../../shared/components/code-editor/code-editor';
import { CodeSnippet } from '../../interfaces/code-snippet';
import { SourceFile } from '../../interfaces/source-file';
import { getMarkerCoordinates } from '../../services/source-code-utils';
import { TopicCodeAnnotations } from '../topic-code-annotations/topic-code-annotations';
import * as React from 'react';

interface Props {
    sourceFile: SourceFile;
    codeSnippets: CodeSnippet[];
    activeCodeSnippetId?: string;

    /**
     * Line and description are by default white (based on a dark background as in overlay)
     * for use on a white background added a secondary color theme
     */
    colorpalette?: string;

}

interface State {
    // markers: IMarker[]; // TODO Update to monaco
    markers: any[];
    codeLines: number;
    pxScrolled: number;
    codeSnippets: CodeSnippet[];
}

/** CodeEditor (Right) with Annotations on the left (Snippet name and description) */
export class CodeWithAnnotations extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            markers: [],
            codeLines: 0,
            pxScrolled: 0,
            codeSnippets: null,
        };
    }

    public render() {
        const { sourceFile, activeCodeSnippetId, colorpalette, codeSnippets } = this.props;
        const { markers, codeLines, pxScrolled } = this.state;

        return (
            <div.CodeWithAnnotations data-cy='code-with-annotations'>
                {/** Annotations */}
                <div.LeftView>
                    {
                        !!codeSnippets.length &&
                        <TopicCodeAnnotations markers={markers}
                            sourceFile={this.props.sourceFile}
                            activeCodeSnippetId={activeCodeSnippetId}
                            codeSnippets={codeSnippets}
                            totalNumberOfLines={codeLines}
                            pxScrolled={pxScrolled}
                            colorpalette={colorpalette ? colorpalette : 'default'} />
                    }

                </div.LeftView>

                {/** Code */}
                <div.RightView>
                    <CodeEditor srcCode={sourceFile ? sourceFile.code : ''}
                        width={700}
                        markers={markers}
                        height={600}
                        editable={false}
                        language='javascript' />
                </div.RightView>

            </div.CodeWithAnnotations>
        );
    }

    // onScroll={(px: number) => this.setPxScrolled(px)} // TODO Restore for monaco

    public componentDidMount() {
        this.getMarkersFromSourceFile();
    }

    public componentDidUpdate(prevProps: any) {
        if ((prevProps.sourceFile._id !== this.props.sourceFile._id) ||
            prevProps.codeSnippets.length !== this.props.codeSnippets.length) {
            this.getMarkersFromSourceFile();
        }
    }

    private getMarkersFromSourceFile() {
        const { codeSnippets, sourceFile } = this.props;

        let markers = getMarkerCoordinates(sourceFile, codeSnippets);
        let codeLines = sourceFile.code.split('\n').length;

        this.setState({ markers, codeLines });
    }

    // private setPxScrolled(pxScrolled: number) {
    //     this.setState({ pxScrolled });
    // }
}
