import { Button } from '../../../shared/components/button/button';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    handleTooltipsClick: () => void;
}

interface State { }

export class DefineAnnotationMenu extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { handleTooltipsClick } = this.props;
        return (
            <View data-cy='define-annotation-menu'>
                <Button config={{
                    onPress: handleTooltipsClick,
                    text: 'Tooltips',
                }} />

                <Button config={{
                    onPress: () => this.handleImageTooltipsClick(),
                    text: 'Image tooltips',
                }} />
            </View>
        );
    }

    private handleImageTooltipsClick() {
    }
}