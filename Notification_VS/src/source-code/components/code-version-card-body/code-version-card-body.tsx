import * as div from './code-version-card-body.style';
import * as cfg from './code-version-card-body.utils';
import { Button } from '../../../shared/components/button/button';
import { UploadImage } from '../../../shared/components/upload-image/upload-image';
import { FileInput } from '../../../shared/interfaces/file-input';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { CodeVersionCardBodyCfg } from '../../interfaces/code-version-card';
import { AddPackageVersionImage, DeletePackageVersionImage, EditPackageVersionImage } from '../../interfaces/package-version';
import { addPackageVersionImage, deletePackageVersionImage, editPackageVersionImage } from '../../services/package-version.service';
import { FramedImage } from '../framed-image/framed-image';
import { PackageVersionModal } from '../package-version-modal/package-version-modal';
import { PreviewModal } from '../preview-modal.tsx/preview-modal';
import { StatsBar } from '../stats-bar/stats-bar';
import { VersionNavbar } from '../version-navbar/version-nav-bar';
import { VersionPreviewImage } from '../version-preview-image/version-preview-image';
import { VersionTitle } from '../version-title/version-title';
import * as React from 'react';
import { Dimensions } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    config: CodeVersionCardBodyCfg;
    isNarrow: boolean;
}

interface State {
    width: number;
}

/**
 * CodeVersionCardBody contains the most important
 * info about a version - it encapsulates VersionTitle
 * which highlights why the card is used
 * VersionNavbar which is used to filter the content of the card body
 * Stats Bar - used to notify the user about comments, reacts etc...
 * AdditionalDescription - used for completion on the description provided
 * by the version title Preview and Edit Buttons
 * And two array , one of Framed Images and one of Preview Images
 */
class _CodeVersionCardBody extends React.Component<Props, State> {

    private modal: JSX.Element;

    private addImageModal: JSX.Element;

    private deletePackageModal: JSX.Element;

    private updateVersionModal: JSX.Element;

    private editImageModal: JSX.Element;

    constructor(props: Props) {

        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { isNarrow } = this.props;
        const {
            versionTitleCfg,
            versionNavbarCfg,
            packageVersionId,
            framedImagesCfg,
            statsBarCfg,
            additionalDescription,
            versionPreviewImagesCfg } = this.props.config;

        return (
            <div.CardBody data-cy='card-body' isNarrow={isNarrow}>

                {/* Header */}
                <div.Header data-cy='header' isNarrow={isNarrow}>
                    <VersionTitle config={versionTitleCfg} />

                    <VersionNavbar {...versionNavbarCfg} overrides={cfg.getVersionNavbarOverride(isNarrow)} />
                </div.Header>

                {/* Stats Bar */}
                <div.StatsBar data-cy='stats-bar' isNarrow={isNarrow}>
                    <StatsBar config={statsBarCfg} isNarrow={isNarrow} />
                </div.StatsBar>

                {/* Additional Description */}
                {
                    !!additionalDescription &&
                    <div.AdditionalDescription data-cy='additional-description'>
                        {additionalDescription}
                    </div.AdditionalDescription>
                }

                {/* Buttons */}
                <div.Buttons data-cy='buttons' isNarrow={isNarrow}>

                    <Button config={cfg.getUpdateButtonCfg(this.showUpdateVersionModal.bind(this))} overrides={{ root: cfg.versionCardButtonOverride }} />

                    <Button config={cfg.getEditButtonCfg(this.showAddImageModal.bind(this))} overrides={{ root: cfg.versionCardButtonOverride }} />

                    <Button config={cfg.getPreviewButtonCfg(this.showModal.bind(this))} overrides={{ root: cfg.versionCardButtonOverride }} />

                    <Button config={cfg.getDeleteButtonCfg(this.showDeleteImageModal.bind(this))} overrides={{ root: cfg.versionCardButtonOverride }} />

                </div.Buttons>

                {/* Images */}
                <div.Images data-cy='images' isNarrow={isNarrow}>
                    {
                        !!framedImagesCfg &&
                        framedImagesCfg.map((framedImage, index) =>
                            <FramedImage
                                onPressEdit={() => this.onPressEditImage(framedImage.imagePath)}
                                onPressDelete={() => this.onPressDeleteFramedImage(framedImage.imagePath, packageVersionId)}
                                key={index} config={framedImage} override={`top: 15px;`} isNarrow={isNarrow} />)
                    }

                    {
                        !!versionPreviewImagesCfg &&
                        versionPreviewImagesCfg.map((previewImg, index) =>
                            <VersionPreviewImage key={index} config={previewImg} override={!isNarrow && 'margin-left: 35px;'} />)
                    }
                </div.Images>

                <Button config={cfg.getRoutingButton(() => this.onRoutePress())} overrides={{ root: 'align-self: center; margin-top: 100px;' }} />

            </div.CardBody>

        );
    }

    private onRoutePress() {
        const { config, history } = this.props;

        history.push('/admin/code-editor/' + config.pathName);
    }

    private showAddImageModal() {
        this.addImageModal = <UploadImage onClose={() => this.hideAddImageModal()} onSavePress={(image: FileInput, inputUrl: string) => this.onSaveImgPress(image, inputUrl)} />;
        addAppOverlay(this.addImageModal);
    }

    private onPressDeleteFramedImage(imgPath: string, versionId: string) {
        const request: DeletePackageVersionImage = {
            imgPath,
            versionId
        };

        deletePackageVersionImage(request);
    }

    private hideEditImageModal() {
        removeAppOverlay(this.editImageModal);
        delete this.editImageModal;
    }

    private onPressEditImage(imgPathToReplace: string) {
        this.editImageModal = <UploadImage onClose={() => this.hideEditImageModal()} onSavePress={(image: FileInput, inputUrl: string) => this.onSaveEdit(image, inputUrl, imgPathToReplace)} />
        addAppOverlay(this.editImageModal);
    }

    private onSaveEdit(image: FileInput, inputUrl: string, imgPathToReplace: string) {
        const { packageVersionId } = this.props.config;

        if (inputUrl) {
            return;
        } else if (image) {
            let imgPathArray = imgPathToReplace.split('/');
            let imgPath = imgPathArray[imgPathArray.length - 1];

            const editImageReq: EditPackageVersionImage = {
                versionId: packageVersionId,
                file: image,
                imageToReplacePath: imgPath,
            };

            editPackageVersionImage(editImageReq);
            this.hideEditImageModal();
        }
    }


    private onSaveImgPress(image: FileInput, inputUrl: string) {
        const { packageVersionId } = this.props.config;

        if (inputUrl) {
            return;
        } else if (image) {
            const addImageReq: AddPackageVersionImage = {
                versionId: packageVersionId,
                file: image,
            };
            addPackageVersionImage(addImageReq);
            this.hideAddImageModal();
        }

    }

    private showDeleteImageModal() {
        const { config } = this.props;
        this.deletePackageModal = <PackageVersionModal
            packageId={config.packageId}
            render={'delete-package-version'} versionId={config.packageVersionId} callback={() => this.hideDeletePackageModal()} />;

        addAppOverlay(this.deletePackageModal);
    }

    private showUpdateVersionModal() {
        const { config } = this.props;

        this.updateVersionModal = <PackageVersionModal versionTitle={config.versionTitleCfg.title} description={config.versionTitleCfg.description} versionId={config.packageVersionId}
            packageId={config.packageId} render={'update-package-version'} callback={() => this.hideUpdateVersionModal()} />;

        addAppOverlay(this.updateVersionModal);
    }

    private hideUpdateVersionModal() {
        removeAppOverlay(this.updateVersionModal);
        delete this.updateVersionModal;
    }

    private hideAddImageModal() {
        removeAppOverlay(this.addImageModal);
        delete this.addImageModal;
    }

    private hideDeletePackageModal() {
        removeAppOverlay(this.deletePackageModal);
        delete this.deletePackageModal;
    }

    private showModal() {
        const { config } = this.props;

        this.modal = <PreviewModal codeVersionCardBody={config} callback={() => this.hideModal()} />;
        addAppOverlay(this.modal);
    }

    private hideModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }
}

export const CodeVersionCardBody = withRouter<Props, any>(_CodeVersionCardBody);