import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { BOX_SHADOW_CSS, getShadow } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

export const CARD_BODY_WIDTH = 830;

interface ContentProps {
    isNarrow: boolean;
}

export const CardBody = styled.View<ContentProps>`
    ${props => getCardPaddings(props.isNarrow)};
    display: flex;
    flex-direction: column;
    background-color: white;
    z-index: -1;
    ${props => props.isNarrow ? `width: 100%; align-items: center; justify-content: center;` : `width: ${CARD_BODY_WIDTH}px;`}
    ${props => !props.isNarrow && getShadow(BOX_SHADOW_CSS)};
`;

export const Header = styled.View<ContentProps>`
    ${props => getHeaderStyle(props.isNarrow)};
`;

export const StatsBar = styled.View<ContentProps>`
    margin-top: 20px;
    margin-bottom: 20px;
    ${props => props.isNarrow ? `width: 100%; left: 7px;` : 'width: 380px;'};
`;

export const Buttons = styled.View<ContentProps>`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    ${props => props.isNarrow && 'align-items: center; justify-content: center;'}
`;

export const Images = styled.View<ContentProps>`
    ${props => getImagesWrapperStyle(props.isNarrow)};
    margin-top: 30px;
    flex-basis: 100%;
`;

export const AdditionalDescription = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$grey};
    margin-bottom: 30px;
`;

// ====== UTILS ======

const getImagesWrapperStyle = (isNarrow: boolean) => {
    return isNarrow ?
        `flex-direction: column; justify-content: center; align-items: center;`
        :
        'flex-direction: row; justify-content: flex-start; align-items: flex-start; flex-wrap: wrap;';
};

const getCardPaddings = (isNarrow: boolean) => {
    return isNarrow ?
        `padding: 25px 25px 25px 25px;`
        :
        `padding: 40px 30px 120px 60px;`;

};

const getHeaderStyle = (isNarrow: boolean) => {
    if (isNarrow) {
        return `display: flex;
        flex-direction: column;
        justify-content: center;`;
    } else {
        return `display: flex;
        flex-direction: row;
        justify-content: space-between;`;
    }
};