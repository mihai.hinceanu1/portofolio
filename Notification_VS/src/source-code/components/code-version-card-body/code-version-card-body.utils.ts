import { CODE, EXTRA_HELP, PREVIEW } from '../../../shared/assets/icons';
import { ButtonConfig } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { DELETE, PEN_SVG } from '../../../users/components/account-settings-menu/_assets/account-settings-menu.assets';

export const getVersionNavbarOverride = (isNarrow: boolean) => {
    if (isNarrow) {
        return `margin-top: 20px; margin-bottom: 20px;`;
    }
};

export const getEditButtonCfg = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            color: colors.$darkBlue,
            text: 'Add Image',
            fontWeight: 600,
            fontSize: font.$size14,
            icon: {
                type: 'svg',
                svgPath: EXTRA_HELP,
            },
            rounded: true,
            width: 140,
        }
    );
};

export const getRoutingButton = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            color: colors.$darkBlue,
            text: 'Code Editor',
            fontWeight: 600,
            fontSize: font.$size14,
            icon: {
                type: 'svg',
                svgPath: CODE,
            },
            rounded: true,
            width: 200,
        }
    );
};

export const getDeleteButtonCfg = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            color: colors.$darkBlue,
            text: 'Delete',
            fontSize: font.$size14,
            fontWeight: 600,
            rounded: true,
            icon: {
                type: 'svg',
                svgPath: DELETE,
            },
            width: 140,
        }
    );
};

export const getUpdateButtonCfg = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            color: colors.$darkBlue,
            text: 'Edit',
            fontSize: font.$size14,
            fontWeight: 600,
            rounded: true,
            icon: {
                type: 'svg',
                svgPath: PEN_SVG,
            },
            width: 140,
        }
    );
};

export const getPreviewButtonCfg = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            color: colors.$darkBlue,
            rounded: true,
            text: 'Preview',
            fontSize: font.$size14,
            fontWeight: 600,
            icon: {
                type: 'svg',
                svgPath: PREVIEW,
            },
            width: 140,
        }
    );
};

export const versionCardButtonOverride = `margin-right: 30px; border-width: 2px; border-color:${colors.$darkBlue};`;