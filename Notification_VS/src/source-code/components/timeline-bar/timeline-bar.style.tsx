import { CHECKPOINT } from './_assets/timeline-bar.assets';
import { CHECKPOINT_HEIGHT, CHECKPOINT_WIDTH, LINE_WIDTH } from './_const/timeline-bar.const';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import React from 'react';
import { StyleSheet } from 'react-native';
import Svg, { G, Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface BarProps {
    isFirst: boolean;
    overrides?: string;
}

interface LineProps {
    color: string;
}

// Temporary fix - TODO (@Dan) - height isn't getting the right value
export const TimelineBar = styled.View<BarProps>`
    height: 101%
    ${props => getTopPosition(props)};
    ${props => props.overrides};
`;

export const BottomLine = styled.View<LineProps>`
    top: -8px;
    left: ${CHECKPOINT_WIDTH / 2 - LINE_WIDTH / 2}px;
    width: ${LINE_WIDTH}px;
    min-height: 30px;
    flex-grow: 1;
    background-color: ${props => props.color};
`;

export const style = StyleSheet.create({
    topLineGradient: {
        bottom: -8,
        left: CHECKPOINT_WIDTH / 2 - LINE_WIDTH / 2,
        width: LINE_WIDTH,
        height: 20,
    },
});

// ====== SVG ======

/**
 * Draws the big bulb which serves as a delimitation point between
 * package versions as well as the icon inside it.
 */
export const getCheckpoint = (icon: string, color: string): JSX.Element => {
    const { width, height } = pathBoundingBox(icon);

    return (
        <Svg width={CHECKPOINT_WIDTH} height={CHECKPOINT_HEIGHT} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Path d={CHECKPOINT} fill={color} />
            <G x={CHECKPOINT_WIDTH / 2 - width / 2} y={CHECKPOINT_HEIGHT / 2 - height / 2}>
                <Path d={icon} fill={colors.$white} />
            </G>
        </Svg>
    );
};

// ====== UTILS ======

let getTopPosition = (props: BarProps): string =>
    props.isFirst ? '' : 'top: -20px;';

/** Gradient for the connection point */
export const getConnectionPointGradient = (color: string, topColor?: string): string[] => {
    const TOP_GRADIENT = topColor ? topColor : colors.$darkBlue;
    const BOTTOM_GRADIENT = color;

    return [TOP_GRADIENT, BOTTOM_GRADIENT];
};