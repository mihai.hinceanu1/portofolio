import * as div from './timeline-bar.style';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

export interface ITimelineProps {
    /** Svg path of an icon */
    icon: string;

    /** Flag first fragment  */
    isFirst: boolean;

    /** The color filling the line and the checkpoint */
    color: string;

    /** The color of the top line must be provided to make it look continuous */
    topColor?: string;

    overrides?: string;
}

/**
 * A visual component used to aid with the delimitation between package versions.
 * It has a modular design, meaning that multiple bars will appear as one continuous bar
 * It can have a custom color and an icon in the checkpoint.
 */
export const TimelineBar: React.FunctionComponent<ITimelineProps> = (props: ITimelineProps) => {
    const { icon, isFirst, color, topColor, overrides } = props;

    return (
        <div.TimelineBar data-cy='timeline-bar' isFirst={isFirst} overrides={overrides}>

            {/** Gradient top line  */}
            {
                !isFirst &&
                <LinearGradient data-cy='linear-gradient'
                    colors={div.getConnectionPointGradient(color, topColor)}
                    start={{ x: 1, y: 0 }}
                    end={{ x: 1, y: 0.9 }}
                    style={div.style.topLineGradient} />
            }

            {/** Icon */}
            {div.getCheckpoint(icon, color)}

            {/** Gradient bottom line */}
            <div.BottomLine color={color} />

        </div.TimelineBar>
    );
};