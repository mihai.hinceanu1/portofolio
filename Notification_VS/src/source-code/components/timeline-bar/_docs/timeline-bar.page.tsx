import * as samples from './timeline-bar.page.samples';
import * as cfg from './timeline-bar.page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { TimelineBar } from '../timeline-bar';
import React from 'react';

export class TimelineBarPage extends ScrollableDocPage<{}> {
    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        const { width } = this.state;
        return (
            <>
                {/** Overview */}
                <div.Overview width={width} data-cy='timeline-bar-page'>
                    <div.OverviewTitle>
                        Timeline Bar
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        A design element appearing in the left side of the source code admin
                        page, with the purpose of delimiting the package versions by having a
                        big visual checkpoint bullet at the start of each version, also containing
                        an icon which indicates the state of the version.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Preferred use case */}
                <div.Demo width={width} data-cy='standard'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Preferred use case
                        </div.DemoTitle>

                        <div.DemoDescription>
                            This is the preferred use case where the timeline bar is on the left and some content on right,
                            which will cause the timeline bar take up the entire height.
                        </div.DemoDescription>

                        <TimelineBar {...cfg.timelineBarDefault} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.timelineBarDefault} />
                </div.Demo>

                {/** A line linked to another */}
                <div.Demo width={width} data-cy='linked-bar'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Linked bar
                        </div.DemoTitle>

                        <div.DemoDescription>
                            A timeline bar continued by another to check their linking behavior.
                        </div.DemoDescription>

                        <TimelineBar {...cfg.timelineBarDefault} />
                        <TimelineBar {...cfg.timelineBarLinked} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.timelineBarLinked} />
                </div.Demo>
            </>
        );
    }
}