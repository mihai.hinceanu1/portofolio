import { OK_SIGN } from '../_assets/timeline-bar.assets';
import { colors } from '../../../../shared/style/colors';

export const timelineBarDefault = {
    icon: OK_SIGN,
    isFirst: true,
    color: colors.$darkBlue,
};

export const timelineBarLinked = {
    icon: OK_SIGN,
    isFirst: false,
    color: colors.$green,
    topColor: colors.$darkBlue,
};