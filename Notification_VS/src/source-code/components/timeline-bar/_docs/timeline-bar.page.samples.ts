export const timelineBarDefault = `
export const timelineBarProps = {
    // Svg path for the icon
    icon: OK_SIGN,
    // Flag for first bar so we know when
    // to add the connecting fragment
    isFirst: true,
    // The color filling the line and the checkpoint
    color: colors.$darkBlue,
};

<TimelineBar {...timelineBarProps} />
`;

export const timelineBarLinked = `
let timelineBarProps = {
    icon: OK_SIGN,
    isFirst: true,
    color: colors.$darkBlue,
};

// The props for the second timeline bar
// which will be connected to first.
let timelineBarLinked = {
    icon: OK_SIGN,
    // This basically tells that there is another bar
    // above this one and they should be linked.
    isFirst: false,
    color: colors.$green,
    // This is the color of the bar above to make the
    // gradient give the effect of a  smooth transition.
    topColor: colors.$darkBlue,
};

<TimelineBar {...timelineBarProps} />
<TimelineBar {...timelineBarLinked} />
`;