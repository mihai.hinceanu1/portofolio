/// <reference types="cypress"/>

describe('Timeline bar test', () => {
    it('Go to doc page', () => {
        cy.visit('components-catalog/source-code/timeline-bar');
        cy.get('[data-cy=timeline-bar-page]').should('exist');
    });

    it('The timeline bar renders', () => {
        getEl('standard', 'timeline-bar').should('exist');
    });

    it('The icon provided is the same displayed', () => {
        cy.get('[data-cy=standard] path')
            // The second path is for the icon inside the checkpoint,
            // the first one is the checkpoint itself.
            .eq(1)
            .should('have.attr', 'd', OK_SIGN);
    });

    it('The color provided is the same displayed when a single timeline bar is present', () => {
        cy.get('[data-cy=standard] path').should('have.attr', 'fill', COLOR);
    });

    it('When 2 or more timeline bars are rendered the linear gradient appears', () => {
        getEl('linked-bar', 'timeline-bar').should('have.length.greaterThan', 1);
        getEl('linked-bar', 'linear-gradient').should('exist');
    });

    it('When multiple timeline bars are connected, the colors provided are same as displayed', () => {
        getEl('linked-bar', 'linear-gradient').should('have.css', 'background-image', `linear-gradient(rgb(0, 30, 64), rgb(0, 195, 167))`);
    });
});

// ====== ASSETS ======

const COLOR = '#001e40';

const OK_SIGN = 'M0.5 17L9.5 9.5L17 18.5L38 0.5L45.5 9.5L15.5 33.5L0.5 17Z';

// ====== SELECTORS ======

function getEl(usecase, elem) {
    return cy.get(`[data-cy=${usecase}] [data-cy=${elem}]`);
}