import { getShadow } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

export const DeletePackage = styled.View`
    width: 350px;
    height: 330px;
    display: flex;
    flex-direction: column;
    align-self: center;
    align-items: center;
    background-color: white;
    ${getShadow()};
`;

export const ConfirmationMessage = styled.Text`
    margin: 30px;
    text-align: center;
`;

export const PackageName = styled.Text`
    font-weight: 600;
    margin-left: 30px;
    margin-right: 30px;
    text-align: center;
`;

export const ButtonsContainer = styled.View`
    display: flex;
    flex-direction: row;
    margin-top: 20px;
    justify-content: space-between;
`;