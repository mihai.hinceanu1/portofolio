import * as div from './delete-package-modal.style';
import { EDIT_PACKAGE_DETAILS } from '../../../shared/assets/icons';
import { Button } from '../../../shared/components/button/button';
import { ModalBoxHeader } from '../../../shared/components/modal-box-header/modal-box-header';
import { Modal } from '../../../shared/components/modal/modal';
import { colors } from '../../../shared/style/colors';
import { DeletePackageConfig } from '../../interfaces/source-code';
import { deletePackage } from '../../services/packages.service';
import * as React from 'react';

interface Props {
    packageName: string;
    packageId: string;
    closeModal: () => void;
}

/** Deleting a package requires confirmation. */
export const DeletePackageModal: React.FunctionComponent<Props> = (props: Props) => {
    const { closeModal, packageId, packageName } = props;

    return (
        <Modal callback={closeModal}>
            <div.DeletePackage data-cy='delete-package'>

                {/** Header */}
                <ModalBoxHeader icon={EDIT_PACKAGE_DETAILS}
                    title='DELETE PACKAGE'
                    closeModal={closeModal} />

                {/** Confirmation Message */}
                <div.ConfirmationMessage data-cy='confirmation-message'>
                    Are you sure you want to delete the following code package?&nbsp;
                </div.ConfirmationMessage>

                {/** PackageName */}
                <div.PackageName data-cy='package-name'>
                    {packageName}
                </div.PackageName>

                {/** Confirm or cancel */}
                <div.ButtonsContainer>
                    <Button config={{
                        width: 100,
                        text: 'Cancel',
                        onPress: closeModal,
                    }} />

                    <Button config={{
                        width: 100,
                        text: 'Delete',
                        color: colors.$red,
                        onPress: () => handleYesPress(packageId, closeModal),
                    }} />
                </div.ButtonsContainer>
            </div.DeletePackage>
        </Modal>
    );
};

const handleYesPress = (packageId: string, closeModal: () => void) => {
    let cfg: DeletePackageConfig = {
        packageId: packageId,
    };

    deletePackage(cfg);
    !!closeModal && closeModal();
};
