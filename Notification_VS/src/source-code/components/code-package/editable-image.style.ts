import { getPackageElementsColor, MILESTONES } from '../../services/packages-list.utils';
import { SPACING_NARROW } from '../stats-bar/stats-bar.style';
import styled from 'styled-components/native';

// ====== CONST ======

const IMAGE_DIMENSIONS_NARROW = 70;
const IMAGE_DIMENSIONS = 100;

// ====== PROPS ======

interface AdaptiveProps {
    isNarrow: boolean;
}

interface ImageProps {
    milestone: MILESTONES;
}

// ====== STYLE ======

export const EditableImage = styled.View<AdaptiveProps>`
    align-self: center;
    align-items: center;
    justify-content: center;
    ${props => getEditableImageAdaptiveStyle(props)}
`;

export const ImageFrame = styled.View<ImageProps & AdaptiveProps>`
    align-items: center;
    justify-content: center;
    background-color: ${props => getPackageElementsColor(props.milestone)};
    ${props => getImageFrameAdaptiveStyle(props)};
`;

export const Image = styled.Image<AdaptiveProps>`
    width: ${props => getMaxImageDimensions(props)}px;
    height: ${props => getMaxImageDimensions(props)}px;
`;

export const Edit = styled.TouchableHighlight<AdaptiveProps>`
    background-color: rgba(127, 127, 127, 0.7);
    width: 30px;
    height: 30px;
    border-radius: 15px;;
    align-items: center;
    justify-content: center;
    align-self: flex-end;
    position: absolute;
    ${props => getEditBtnPosition(props)}
`;

// ====== UTILS ======

let getEditableImageAdaptiveStyle = (props: AdaptiveProps): string => {
    let style = '';

    if (props.isNarrow) {
        style = `
            margin-right: ${SPACING_NARROW}px;
            width: ${IMAGE_DIMENSIONS_NARROW}px;
            height: ${IMAGE_DIMENSIONS_NARROW}px;
        `;
    } else {
        style = `
            margin-right: 30px;
            width: ${IMAGE_DIMENSIONS}px;
            height: ${IMAGE_DIMENSIONS}px;
        `;
    }

    return style;
};

let getImageFrameAdaptiveStyle = (props: AdaptiveProps): string => {
    if (props.isNarrow) {
        return `
            flex-basis: ${IMAGE_DIMENSIONS_NARROW}px;
            width: ${IMAGE_DIMENSIONS_NARROW}px;
            height: ${IMAGE_DIMENSIONS_NARROW}px;
            border-radius: ${IMAGE_DIMENSIONS_NARROW / 2}px;
        `;
    } else {
        return `
            flex-basis: ${IMAGE_DIMENSIONS}px;
            width: ${IMAGE_DIMENSIONS}px;
            height: ${IMAGE_DIMENSIONS}px;
            border-radius: ${IMAGE_DIMENSIONS / 2}px;
        `;
    }
};

function getMaxImageDimensions(props: AdaptiveProps): number {
    let side: number;
    let squareHypotenuse;

    if (props.isNarrow) {
        squareHypotenuse = Math.pow(IMAGE_DIMENSIONS_NARROW, 2);
    } else {
        squareHypotenuse = Math.pow(IMAGE_DIMENSIONS, 2);
    }

    side = Math.sqrt(squareHypotenuse / 2);
    return side;
}

function getEditBtnPosition(props: AdaptiveProps): string {
    if (props.isNarrow) {
        return `
            right: -5px;
            bottom: -2px;
        `;
    } else {
        return `
            bottom: 1px;
            right: 3px;
        `;
    }
}