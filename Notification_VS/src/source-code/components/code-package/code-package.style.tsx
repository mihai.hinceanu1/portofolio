import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { getPackageElementsColor, MILESTONES } from '../../services/packages-list.utils';
import styled from 'styled-components/native';

// ====== PROPS ======

interface CodePackageProps extends AdaptiveProps {
    isHovered: boolean;
}

interface AdaptiveProps {
    isNarrow: boolean;
}

interface DeprecatedLabelProps {
    milestone: MILESTONES;
}

// ====== STYLE ======

export const CodePackage = styled.TouchableHighlight<CodePackageProps & ReactWebAttributes>`
    min-width: 395px;
    flex-direction: row;
    background-color: ${colors.$white};
    ${props => getShadowFocusEffect(props)}
    ${props => getCodePackageAdaptiveStyle(props)}
`;

export const Details = styled.View`
    flex: 1;
    min-width: 280px;
    justify-content: space-around;
`;

export const Name = styled.Text<AdaptiveProps>`
    flex-wrap: wrap;
    ${props => getNameAdaptiveStyle(props)};
`;

export const Description = styled.Text``;

export const WebUtils = styled.View`
    flex-direction: row-reverse;
    align-items: center;
    align-self: center;
    flex-basis: 100px;
    margin-left: 10px;
`;

export const Deprecated = styled.View`
    align-items: center;
    flex-direction: row;
    margin-right: 5px;
`;

export const DeprecatedLabel = styled.Text<DeprecatedLabelProps>`
    margin-right: 5px;
    color: ${props => getPackageElementsColor(props.milestone)};
`;

export const Actions = styled.View`
    justify-content: center;
`;

// ====== UTILS ======

function getShadowFocusEffect(props: CodePackageProps): string {
    if (props.isHovered) {
        return 'box-shadow: 1px 1px 30px rgba(0, 0, 0, 0.3);';
    }
}

let getCodePackageAdaptiveStyle = (props: AdaptiveProps): string => {
    let style = '';

    if (props.isNarrow) {
        style = `padding: 10px;`;
    } else {
        style = `
            padding: 30px 10px 30px 30px;
            border-left-width: 1px;
            border-right-width: 1px;
            border-bottom-width: 1px;
            border-color: ${colors.$lightGrey};
        `;
    }

    return style;
};

let getNameAdaptiveStyle = (props: AdaptiveProps): string => {
    let style = '';

    if (!props.isNarrow) {
        style = `
            font-weight: 600;
            font-size: ${font.$size18}px;
        `;
    }

    return style;
};
