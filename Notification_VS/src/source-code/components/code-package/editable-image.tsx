import * as div from './editable-image.style';
import { Icon } from '../../../icons/components/icon/icon';
import { PEN_SVG } from '../../../shared/assets/icons';
import { UploadImage } from '../../../shared/components/upload-image/upload-image';
import { APP_CFG } from '../../../shared/config/app.config';
import { FileInput } from '../../../shared/interfaces/file-input';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { colors } from '../../../shared/style/colors';
import { ICodePackage, UploadPackageImageConfig } from '../../interfaces/packages';
import { editPackage, uploadImage } from '../../services/packages.service';
import React from 'react';

interface Props {
    isNarrow: boolean;
    codePackage: ICodePackage;
}
interface State { }

/**
 * Code package image. Handles the interactions with the image (adding, changing it).
 * Has a default image set when the package has no image.
 *
 * <!> If the package has an image and it can't be retrieved, it should also (maybe) display
 * the default image.
 */
export class EditableImage extends React.Component<Props, State> {
    private uploadModal: JSX.Element;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { isNarrow, codePackage } = this.props;
        const { image, milestone } = codePackage;
        return (
            <div.EditableImage data-cy='editable-image'
                isNarrow={isNarrow}>

                {/** Image background */}
                <div.ImageFrame data-cy='image-frame'
                    milestone={milestone}
                    isNarrow={isNarrow}>

                    {/** Image */}
                    <div.Image data-cy='image'
                        source={{ uri: this.getImageUrl(image) }}
                        isNarrow={isNarrow}
                        resizeMode='contain' />
                </div.ImageFrame>

                {/* Overlapping edit button */}
                <div.Edit data-cy='edit'
                    isNarrow={isNarrow}
                    onPress={() => this.openModal()}>
                    <Icon path={PEN_SVG} color={colors.$white} />
                </div.Edit>
            </div.EditableImage>
        );
    }

    private openModal() {
        this.uploadModal = <UploadImage onClose={() => this.closeModal()}
            onSavePress={(image, imageUrl) => this.savePackageImage(image, imageUrl)} />;

        addAppOverlay(this.uploadModal);
    }

    private closeModal() {
        removeAppOverlay(this.uploadModal);
        delete this.uploadModal;
    }

    private savePackageImage(image: FileInput, imageUrl: string) {
        const { _id } = this.props.codePackage;
        if (image) {
            // Upload image and edit the path in the package.
            let config: UploadPackageImageConfig = {
                image,
                packageId: _id,
            };

            uploadImage(config);
        }

        if (imageUrl) {
            // Edit the image path in the package.
            let updatedCodePackage = { ...this.props.codePackage };
            updatedCodePackage.image = imageUrl;
            editPackage(updatedCodePackage);
        }

        this.closeModal();
    }

    /** Show a default image if we don't have one set. */
    private getImageUrl(imagePath: string): string {
        if (imagePath) {
            return imagePath;
        } else {
            return `${APP_CFG.assets}/shared/code_snippet_black.png`;
        }
    }
}