import * as div from './code-package.style';
import * as utils from './code-package.utils';
import { EditableImage } from './editable-image';
import { Icon } from '../../../icons/components/icon/icon';
import { WARNING } from '../../../shared/assets/icons';
import { Dropdown } from '../../../shared/components/dropdown/dropdown';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { ICodePackage } from '../../interfaces/packages';
import { getPackageElementsColor } from '../../services/packages-list.utils';
import { DeletePackageModal } from '../delete-package-modal/delete-package-modal';
import { EditPackageModal } from '../edit-package-modal/edit-package-modal';
import { StatsBar } from '../stats-bar/stats-bar';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    isNarrow: boolean;
    codePackage: ICodePackage;
}
interface State {
    isHovered: boolean;
}
/**
 * An item in the packages list representing a code package.
 * Shows some details about the package as well as some stats.
 */
class _CodePackage extends React.Component<Props, State> {
    private modal: JSX.Element;

    constructor(props: Props) {
        super(props);

        this.state = {
            isHovered: false,
        };
    }

    public render() {
        const { codePackage, isNarrow } = this.props;
        const { milestone, name, description, deprecated } = codePackage;
        const { isHovered } = this.state;

        return (
            <div.CodePackage data-cy='code-package'
                isNarrow={isNarrow}
                isHovered={isHovered}
                underlayColor='none'
                onPress={() => this.redirectToPackageVersions(codePackage)}
                onMouseEnter={() => this.togglePackageDetails(true)}
                onMouseLeave={() => this.togglePackageDetails(false)}>

                <React.Fragment>

                    {/** Image */}
                    <EditableImage isNarrow={isNarrow}
                        codePackage={codePackage} />

                    {/** Details */}
                    <div.Details data-cy='details'>

                        {/** Name */}
                        <div.Name data-cy='name' isNarrow={isNarrow}>
                            {name}
                        </div.Name>

                        {/** Description */}
                        {
                            !isNarrow &&
                            <div.Description data-cy='description'>
                                {description}
                            </div.Description>
                        }

                        {/** Stats */}
                        <StatsBar isNarrow={isNarrow} config={utils.getStatsBarConfig(codePackage)} />
                    </div.Details>

                    {/** Deprecated Warning */}

                    <div.WebUtils data-cy='web-utils'>

                        {/** Deprecated warning */}
                        {
                            deprecated &&
                            <div.Deprecated data-cy='deprecated'>
                                {
                                    !isNarrow &&
                                    <div.DeprecatedLabel data-cy='deprecated-label' milestone={milestone}>
                                        Deprecated
                                    </div.DeprecatedLabel>
                                }
                                {/** Warning icon */}
                                <Icon path={WARNING}
                                    color={getPackageElementsColor(milestone)}
                                    width={23}
                                    height={23} />
                            </div.Deprecated>
                        }
                    </div.WebUtils>

                    {/** Actions */}
                    <div.Actions data-cy='actions'>
                        <Dropdown config={this.getDropdownConfig()} />
                    </div.Actions>
                </React.Fragment>
            </div.CodePackage>
        );
    }

    private togglePackageDetails(flag: boolean) {
        this.setState({
            isHovered: flag,
        });
    }

    private redirectToPackageVersions(codePackage: ICodePackage) {
        const { pathName } = codePackage;
        const { history } = this.props;

        history.push(`/admin/package-versions/${pathName}`);
    }

    private openEditModal() {
        const { codePackage } = this.props;

        this.modal = <EditPackageModal codePackage={codePackage}
            closeModal={() => this.closeModal()} />;

        addAppOverlay(this.modal);
    }

    private closeModal() {
        removeAppOverlay(this.modal);
        delete this.modal;
    }

    private openDeleteModal() {
        const { codePackage } = this.props;

        this.modal = <DeletePackageModal packageName={codePackage.name}
            packageId={codePackage._id}
            closeModal={() => this.closeModal()} />;

        addAppOverlay(this.modal);
    }

    private getDropdownConfig() {
        const { codePackage } = this.props;

        return utils.getDropdownConfig(
            codePackage.milestone,
            this.openEditModal.bind(this),
            this.openDeleteModal.bind(this),
        );
    }
}

export const CodePackage = withRouter(_CodePackage);
