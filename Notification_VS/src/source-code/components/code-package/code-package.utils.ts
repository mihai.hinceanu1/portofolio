import { DOTTED_MENU } from '../../../shared/assets/icons';
import { DROPDOWN_OPTION_SIDE, DROPDOWN_TYPE } from '../../../shared/constants/dropdown.const';
import { DropdownConfig, IDropdownOption } from '../../../shared/interfaces/dropdown';
import { IStatsBar } from '../../../shared/interfaces/stats-bar';
import { ICodePackage } from '../../interfaces/packages';
import { getPackageElementsColor, getTechStack, MILESTONES } from '../../services/packages-list.utils';

export function getStatsBarConfig(codePackage: ICodePackage): IStatsBar {
    const {
        awards,
        reactions,
        comments,
        newComments,
        codeLines,
        milestone,
        technologyStack,
    } = codePackage;

    let config: IStatsBar = {
        awards,
        reactions,
        comments,
        newComments,
        codeLines,
        milestone,
        techStack: getTechStack(technologyStack),
    };

    return config;
}

export function getDropdownConfig(
    milestone: MILESTONES,
    openEditModal: () => void,
    openDeleteModal: () => void,
): DropdownConfig {
    let dropdownOptions: IDropdownOption[] = [
        {
            _id: 'edit-details',
            label: 'Edit details',
            // Open edit modal
            callback: () => openEditModal(),
        },
        {
            _id: 'download-package',
            label: 'Download package',
            callback: () => { },
        },
        {
            _id: 'share',
            label: 'Share',
            callback: () => { },
        },
        {
            _id: 'copy-link',
            label: 'Copy Link',
            callback: () => { },
        },
        {
            _id: 'manage-access',
            label: 'Manage Access',
            callback: () => { },
        },
        {
            _id: 'delete',
            label: 'Delete',
            // Open delete modal.
            callback: () => openDeleteModal(),
        }
    ];

    let config: DropdownConfig = {
        options: dropdownOptions,
        side: DROPDOWN_OPTION_SIDE.LEFT,
        icon: {
            svgData: DOTTED_MENU,
            fill: getPackageElementsColor(milestone),
        },
        type: DROPDOWN_TYPE.OnlyIcon,
    };

    return config;
}
