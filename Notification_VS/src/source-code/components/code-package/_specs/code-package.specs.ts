/// <reference types="cypress" />

describe('Test code package component', () => {
    it('Go to code package doc page', () => {
        cy.login();
        cy.visit('/components-catalog/source-code/code-package');
        cy.url().should('include', 'components-catalog/source-code/code-package');
        cy.get('[data-cy=code-package-page]').should('exist');
    });

    it('Code package renders with basic props', () => {
        getEl('default', 'code-package');
    });

    it('The elements have the correct data', () => {
        getEl('default', 'name').should('have.text', 'The ultimate guide to tagging prices. Now updated for 2020');
        getEl('default', 'description').should('have.text', 'Inspired by Jessie J, we\'ve added a price tagging tutorial.');
        getElByTag('default', 'img').should('have.attr', 'src', 'http://192.168.0.111:8080/assets/shared/code_snippet_black.png');
    });

    it('When the deprecated flag is true, a warning appears', () => {
        getEl('deprecated-package', 'deprecated').should('exist');
    });

    it('On narrow screens some elements disappear', () => {
    getEl('narrow', 'description').should('not.exist');
        getEl('narrow', 'deprecated-label').should('not.exist');
    });

    it('When "Editor\'s Choice" milestone is reached, all elements are colored based on the milestone', () => {
        const blue = 'rgb(13, 142, 241)';
        const bluePath = 'rgb(13,142,241)';

        /** Image frame */
        getEl('editors-choice', 'image-frame').should('have.css', 'background-color', blue);

        /** Deprecated  */
        getEl('editors-choice', 'deprecated-label').should('have.css', 'color', blue);
        getNestedElementByTag('editors-choice', 'deprecated', 'path').should('have.attr', 'fill', bluePath);

        /** Menu */
        getNestedElementByTag('editors-choice', 'actions', 'path').should('have.attr', 'fill', bluePath);
    });

    it('When "Highest Rated" milestone is reached, all elements are colored based on the milestone', () => {
        const green = 'rgb(0, 195, 167)';
        const greenPath = 'rgb(0,195,167)';

        /** Image frame */
        getEl('highest-rated', 'image-frame').should('have.css', 'background-color', green);

        /** Deprecated  */
        getEl('highest-rated', 'deprecated-label').should('have.css', 'color', green);
        getNestedElementByTag('highest-rated', 'deprecated', 'path').should('have.attr', 'fill', greenPath);

        /** Menu */
        getNestedElementByTag('highest-rated', 'actions', 'path').should('have.attr', 'fill', greenPath);
    });

    it('When "Bestseller" milestone is reached, all elements are colored based on the milestone', () => {
        const black = 'rgb(0, 0, 0)';
        const blackPath = '#000000';

        /** Image frame */
        getEl('bestseller', 'image-frame').should('have.css', 'background-color', black);

        /** Deprecated  */
        getEl('bestseller', 'deprecated-label').should('have.css', 'color', black);
        getNestedElementByTag('bestseller', 'deprecated', 'path').should('have.attr', 'fill', blackPath);

        /** Menu */
        getNestedElementByTag('bestseller', 'actions', 'path').should('have.attr', 'fill', blackPath);
    });
});

// ====== SELECTORS =======

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

function getElByTag(wrapper, tag) {
    return cy.get(`[data-cy=${wrapper}] ${tag}`);
}

function getNestedElementByTag(useCase, wrapper, elementTag) {
    return cy.get(`[data-cy=${useCase}] [data-cy=${wrapper}] ${elementTag}`);
}