import { MILESTONES } from '../../../services/packages-list.utils';

export const DEFAULT = {
    isNarrow: false,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: `Inspired by Jessie J, we've added a price tagging tutorial.`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Package,
        comments: 33,
        newComments: 0,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: false,
        isActive: true,
    },
};

export const DEPRECATED = {
    isNarrow: false,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: `Inspired by Jessie J, we've added a price tagging tutorial.`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Package,
        comments: 33,
        newComments: 0,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: true,
        isActive: true,
    },
};

export const NARROW = {
    isNarrow: true,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: `Inspired by Jessie J, we've added a price tagging tutorial.`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Package,
        comments: 33,
        newComments: 0,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: true,
        isActive: true,
    },
};

export const EDITORS_CHOICE = {
    isNarrow: false,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: `Inspired by Jessie J, we've added a price tagging tutorial.`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Editors_Choice,
        comments: 33,
        newComments: 0,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: true,
        isActive: true,
    },
};

export const HIGHEST_RATED = {
    isNarrow: false,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: `Inspired by Jessie J, we've added a price tagging tutorial.`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Highest_Rated,
        comments: 33,
        newComments: 0,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: true,
        isActive: true,
    },
};

export const BESTSELLER = {
    isNarrow: false,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: `Inspired by Jessie J, we've added a price tagging tutorial.`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Bestseller,
        comments: 33,
        newComments: 0,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: true,
        isActive: true,
    },
};