import * as samples from './code-package.samples';
import * as utils from './code-package.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { CodePackage } from '../code-package';
import React from 'react';

interface CodePackageState { }

export class CodePackagePage extends ScrollableDocPage<CodePackageState> {
    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        const { width } = this.state;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width} data-cy='code-package-page'>
                    <div.OverviewTitle>
                        Code Package
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        A presentation component, built to show details about a Code Package.
                        * Used in: Admin page for listing packages.
                        * Purpose:
                        - Show details about a code package
                        - Enable the author to manage the package.
                        - Briefly show the community feedback toward a package by having some stats like awards, reactions, comments, milestones and even the deprecated warning.
                    </div.OverviewDescription>

                    {/** Default */}
                    <div.Demo data-cy='default' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Default code package
                            </div.DemoTitle>

                            <CodePackage {...utils.DEFAULT} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.DEFAULT} />
                    </div.Demo>

                    {/** Deprecated */}
                    <div.Demo data-cy='deprecated-package' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Deprecated code package
                            </div.DemoTitle>

                            <CodePackage {...utils.DEPRECATED} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.DEPRECATED} />
                    </div.Demo>

                    {/** Narrow */}
                    <div.Demo data-cy='narrow' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Narrow
                            </div.DemoTitle>

                            <CodePackage {...utils.NARROW} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.NARROW} />
                    </div.Demo>

                    {/** Editor's choice */}
                    <div.Demo data-cy='editors-choice' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Editor's choice
                            </div.DemoTitle>

                            <CodePackage {...utils.EDITORS_CHOICE} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.EDITORS_CHOICE} />
                    </div.Demo>

                    {/** Highest rated */}
                    <div.Demo data-cy='highest-rated' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Highest rated
                            </div.DemoTitle>

                            <CodePackage {...utils.HIGHEST_RATED} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.HIGHEST_RATED} />
                    </div.Demo>

                    {/** Bestseller */}
                    <div.Demo data-cy='bestseller' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Bestseller
                            </div.DemoTitle>

                            <CodePackage {...utils.BESTSELLER} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.BESTSELLER} />
                    </div.Demo>
                </div.Overview>
            </>
        );
    }
}