export const DEFAULT = `
const codePackageConfig = {
    isNarrow: false,
    codePackage: {
        _id: '5ea715d392a335c5db28d1ba',
        created: 1588008522091,
        updated: 1588008522091,
        authorId: '5c20ae4212ac5d56d130d2d0',
        pathName: 'the-ultimate-guide-to-tagging-prices',
        name: 'The ultimate guide to tagging prices. Now updated for 2020',
        description: \`Inspired by Jessie J, we've added a price tagging tutorial.\`,
        image: '',
        awards: 21,
        reactions: 231,
        milestone: MILESTONES.Package,
        comments: 33,
        newComments: 6,
        codeLines: 542,
        technologyStack: [
            {
                name: 'Angular',
                icon: 'angular.png'
            }
        ],
        deprecated: false,
        isActive: true,
    },
};

<CodePackage {...codePackageConfig} />
`;

export const DEPRECATED = `
const codePackageConfig = {
    isNarrow: false,
    codePackage: {
        // Same config but set the deprecated flag to "true"
        deprecated: true,
    },
};

<CodePackage {...codePackageConfig} />
`;

export const NARROW = `
const codePackage = {
    ...
    },
};

<CodePackage codePackage={codePackage}
    isNarrow={true} />
`;

export const EDITORS_CHOICE = `
const codePackageConfig = {
    isNarrow: false,
    codePackage: {
        // Same config but change the milestone.
        milestone: MILESTONES.Editors_Choice,
    },
};

<CodePackage {...codePackageConfig} />
`;

export const HIGHEST_RATED = `
const codePackageConfig = {
    isNarrow: false,
    codePackage: {
        // Same config but change the milestone.
        milestone: MILESTONES.Highest_Rated,
    },
};

<CodePackage {...codePackageConfig} />
`;

export const BESTSELLER = `
const codePackageConfig = {
    isNarrow: false,
    codePackage: {
        // Same config but change the milestone.
        milestone: MILESTONES.Bestseller,
    },
};

<CodePackage {...codePackageConfig} />
`;