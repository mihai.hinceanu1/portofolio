import styled from 'styled-components/native';

// ====== PROPS ======

interface AdaptiveProps {
    isNarrow: boolean;
}

// ====== STYLE ======

export const List = styled.View<AdaptiveProps>`
    flex-grow: 1;
    ${props => getAdaptiveStyle(props)}
`;

// ====== UTILS ======

function getAdaptiveStyle(props: AdaptiveProps): string {
    if (!props.isNarrow) {
        return 'flex-basis: 0;';
    }
}