import * as div from './list.style';
import { ICodePackage } from '../../interfaces/packages';
import { CodePackage } from '../code-package/code-package';
import React from 'react';

interface Props {
    isNarrow: boolean;
    packages: ICodePackage[];
}

/**
 * List of code packages.
 */
export const List: React.FunctionComponent<Props> = (props: Props) => {
    const { packages, isNarrow } = props;

    return (
        <div.List data-cy='list' isNarrow={isNarrow}>
            {
                !!packages && packages.map((codePackage, index) =>
                    <CodePackage key={index}
                        codePackage={codePackage}
                        isNarrow={isNarrow} />
                )
            }
        </div.List>
    );
};