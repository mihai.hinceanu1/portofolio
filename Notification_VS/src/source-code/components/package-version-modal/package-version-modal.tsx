import * as div from './package-version-modal.style';
import * as overlay from '../../../shared/style/overlay.style';
import * as scroll from '../../../shared/style/scroll.style';
import { DeletePackageForm } from '../delete-package-version-form/delete-package-version-form';
import { PackageVersionPreviewHoverMenus } from '../package-version-preview-hover-menus/package-version-preview-hover-menus';
import { UpdatePackageVersionForm } from '../update-package-form/update-package-version-form';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
// import { UploadImageForm } from '../upload-image-form/upload-image-form';

interface Props {
    callback: () => void;
    versionId?: string;
    render: string;
    packageId?: string;
    versionTitle?: string;
    description?: string;
}

interface State {
    width: number;
}

export class PackageVersionModal extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { callback, versionId, render, packageId, versionTitle, description } = this.props;
        const { width } = this.state;
        let isNarrow = width < 600;

        return (
            <div.PackageVersionModal data-cy='package-version-modal' onLayout={e => this.updateWidth(e)}>
                <PackageVersionPreviewHoverMenus />

                <LinearGradient colors={overlay.OVERLAY_GRADIENT}
                    style={overlay.style.linearGradient}>

                    <scroll.ScrollView data-cy='scroll-view'
                        contentContainerStyle={scroll.inner.content}>
                        <scroll.Center width={width}>
                            {
                                render === 'delete-package-version' &&
                                <DeletePackageForm close={callback} versionId={versionId} packageId={packageId} isNarrow={isNarrow}/>
                            }

                            {
                                render === 'update-package-version' &&
                                <UpdatePackageVersionForm callback={callback} versionId={versionId} title={versionTitle} description={description}/>
                            }
                        </scroll.Center>
                    </scroll.ScrollView>

                </LinearGradient>
            </div.PackageVersionModal>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

}