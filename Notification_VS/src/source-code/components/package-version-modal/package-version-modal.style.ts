import styled from 'styled-components/native';

export const PackageVersionModal = styled.View`
    z-index: 100;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
    overflow: hidden;
    align-items: center;
    justify-content: center;
`;