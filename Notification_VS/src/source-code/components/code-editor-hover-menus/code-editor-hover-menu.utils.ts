import { LEARNING_MAP } from '../../../shared/assets/icons';
import { MENU_EXPAND_DIRECTION, TEXT_DIRECTION } from '../../../shared/constants/dots-menu.const';
import { DotsMenuConfig } from '../../../shared/interfaces/dots-menu';

export const dotsMenuMap: DotsMenuConfig = {
    trigger: {
        _id: 'user',
        icon: {
            assetsSrc: 'svg',
            imagePath: LEARNING_MAP,
        },
    },
    dots: [
        {
            _id: 'my-profile',
            label: 'My Profile',
            icon: {
                assetsSrc: 'svg',
                svgData: LEARNING_MAP
            },
        },
    ],
    textDirection: TEXT_DIRECTION.right,
    menuExpandDirection: MENU_EXPAND_DIRECTION.bottom
};