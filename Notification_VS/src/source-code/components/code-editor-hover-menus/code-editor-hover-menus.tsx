import { AdminNavigatorBullet } from '../../../admin/components/admin-navigator-bullet/admin-navigator-bullet';
import { learningMapMenu } from '../../../lessons/components/lesson-hover-menus/lesson-hover-menus.configs';
import { getAppLogo } from '../../../lessons/components/lesson-hover-menus/lesson-hover-menus.utils';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { DotsMenu } from '../../../shared/components/dots-menu/dots-menu';
import * as sharedCfg from '../../../shared/constants/hover-menus.configs';
import * as hover from '../../../shared/style/hover-menus.style';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps<never> { }

interface State {
    width: number;
}

export class _CodeEditorHoverMenus extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { width } = this.state;

        return (
            <hover.Menus data-cy='code-editor-hover-menus'
                pointerEvents='box-none'
                onLayout={e => this.updateWidth(e)} >

                <BranchesMenu config={learningMapMenu}
                    overrides={hover.overrideTopLeftMenu(width)} />

                <hover.AppLogo width={width}>
                    {getAppLogo()}
                </hover.AppLogo>

                <hover.AdminNavigator>
                    <AdminNavigatorBullet />
                </hover.AdminNavigator>

                <DotsMenu config={sharedCfg.userMenu}
                    overrides={hover.overrideTopRightMenu(width)} />
            </hover.Menus >
        );

    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }
}

export const CodeEditorHoverMenus = withRouter(_CodeEditorHoverMenus);
