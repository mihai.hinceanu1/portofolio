import * as div from './package-version-preview.style';
import { CodeVersionCardBodyCfg } from '../../interfaces/code-version-card';
import { FramedImage } from '../framed-image/framed-image';
import { PreviewGraph } from '../preview-graph/preview-graph';
import { StatsBar } from '../stats-bar/stats-bar';
import { VersionPreviewImage } from '../version-preview-image/version-preview-image';
import { VersionPreviewStatisticsCards } from '../version-preview-statistics-cards/version-preview-statistics-cards';
import { VersionTitle } from '../version-title/version-title';
import * as React from 'react';
import { Dimensions } from 'react-native';

interface Props {
    callback: () => void;
    codeVersionCardBody: CodeVersionCardBodyCfg;
}

interface State {
    width: number;
}

/**
 * Package Version Preview is used to come
 * with extra details on a specific package version,
 * such as number of views, ranking, purchases
 */
export class PackageVersionPreview extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { codeVersionCardBody, callback } = this.props;
        const { width } = this.state;
        let isNarrow = width < 600;

        return (
            <div.VersionPreviewWrapper isNarrow={isNarrow}>

                <div.PackageVersionPreview isNarrow={isNarrow}>

                    {/* Close Button*/}
                    <div.closeIconButton onPress={() => callback()}>
                        <div.closeIconText data-cy='close-icon'>
                            X
                        </div.closeIconText>
                    </div.closeIconButton>

                    {/* Version Title*/}
                    <VersionTitle config={codeVersionCardBody.versionTitleCfg} override={'max-width: 100%;'} />

                    {/* Additional Description */}
                    {
                        !!codeVersionCardBody.additionalDescription &&
                        <div.AdditionalDescription data-cy='additional-description'>
                            {codeVersionCardBody.additionalDescription}
                        </div.AdditionalDescription>
                    }

                    {/* Stats Bar */}
                    <div.StatsBarWrapper data-cy='stats-bar-wrapper' isNarrow={isNarrow}>
                        <StatsBar isNarrow={isNarrow} config={codeVersionCardBody.statsBarCfg} />
                    </div.StatsBarWrapper>

                    {/* Statistics Overview */}
                    <div.StatisticsOverview data-cy='statistics-overview'>
                        <div.StatisticsTitle data-cy='statistics-title'>
                            Statistics Overview
                        </div.StatisticsTitle>

                        <div.StatisticsBody isNarrow={isNarrow}>

                            {/* Preview Graph*/}
                            <PreviewGraph isNarrow={isNarrow}/>

                            {/* Preview Statistics Cards*/}
                            <VersionPreviewStatisticsCards isNarrow={isNarrow}/>

                        </div.StatisticsBody>

                    </div.StatisticsOverview>

                    {/* Images */}
                    <div.Images data-cy='images'>
                        {
                            !!codeVersionCardBody.framedImagesCfg &&
                            codeVersionCardBody.framedImagesCfg.map((framedImage, index) =>
                                <FramedImage key={index} config={framedImage} override={'top: 15px;'} isNarrow={isNarrow}/>)
                        }

                        {
                            !!codeVersionCardBody.versionPreviewImagesCfg &&
                            codeVersionCardBody.versionPreviewImagesCfg.map((previewImg, index) =>
                                <VersionPreviewImage key={index} config={previewImg} override={'margin-left: 35px;'} />)
                        }
                    </div.Images>

                </div.PackageVersionPreview>
            </div.VersionPreviewWrapper>
        );
    }
}