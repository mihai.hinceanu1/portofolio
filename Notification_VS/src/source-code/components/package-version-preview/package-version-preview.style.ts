import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface ContentProps {
    isNarrow: boolean;
}

export const VersionPreviewWrapper = styled.View<ContentProps>`
    display: flex;
    flex-direction: column;
    ${props => !props.isNarrow ? 'width: 90% ' : 'width: 100%;'};
    align-self: center;
    margin-top: 25px;
    margin-bottom: 25px;
`;

export const PackageVersionPreview = styled.View<ContentProps>`
    width: 100%;
    background-color: white;
    ${props => !props.isNarrow ? 'padding: 80px;' : ' padding: 15px;'};
`;

export const AdditionalDescription = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$grey};
    margin-top: 15px;
`;

export const StatsBarWrapper = styled.View<ContentProps>`
    margin-top: 70px;
    margin-bottom: 70px;
    ${props => getStatsBarWrapperStyle(props.isNarrow)};
`;

export const StatisticsOverview = styled.View`
    display: flex;
    flex-direction: column;
`;

export const StatisticsTitle = styled.Text`
    font-size: ${font.$size16}px;
    font-weight: 600;
    margin-bottom: 5px;
`;

export const StatisticsBody = styled.View<ContentProps>`
    width: 100%;
    display: flex;
    ${props => props.isNarrow ? 'flex-direction: column' : 'flex-direction: row'};
    justify-content: space-between;
`;

export const Images = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
    flex-wrap: wrap;
    margin-top: 30px;
    flex-basis: 100%;
`;

export const closeIconButton = styled.TouchableOpacity`
    position: absolute;
    right: 15px;
    top: 10px;
`;

export const closeIconText = styled.Text`
    font-size: ${font.$size18}px;
    color: ${colors.$darkGrey};
    font-weight: 600;
`;

// ====== UTILS ======

export const getStatsBarWrapperStyle = (isNarrow: boolean) => {
    if (isNarrow) {
        return `
            width: 100%;
            justify-content: center;
            align-items: center;
        `;
    } else {
        return `
            width: 100%;
        `;
    }
};