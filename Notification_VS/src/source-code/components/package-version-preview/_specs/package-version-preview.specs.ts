/// <reference types="cypress" />

describe('Package Version Preview Tests', () => {
    it('open package version preview', () => {
        openPackageVersionPreview();
    });

    it('Version Title Is Rendered', () => {
        const wrapper = getElmnt('code-version-card', 'buttons');
        wrapper.within(() => {
            cy.get('[data-cy=button]').eq(1).click();
        });
        cy.get('[data-cy=version-title]').should('exist');
    });

    it('Additional Description Is Rendered', () => {
        cy.get('[data-cy=additional-description]').should('exist');
    });

    it('Stats Bar is Rendered', () => {
        cy.get('[data-cy=stats-bar]').should('exist');
    });

    it('Statistics Overview Are Rendered', () => {
        cy.get('[data-cy=statistics-overview]').should('exist');
    });

    it('Statistics Title Is Rendered', () => {
        cy.get('[data-cy=statistics-title]').then(div => {
            expect(div.text()).to.eq('Statistics Overview');
        });
    });

    it('Graph Preview', () => {
        cy.get('[data-cy=graph-wrapper]').should('exist');
    });

    it('Graph is Drawn', () => {
        cy.get('[data-cy=chart]').should('exist');
    });

    it('Statistics Cards Are Rendered', () => {
        cy.get('[data-cy=statistics-cards]').should('exist');
    });

    it('Preview Image Are Rendered', () => {
        cy.get('[data-cy=version-preview-image]').should('exist');
    });

    it('Framed Images Should Exist', () => {
        cy.get('[data-cy=framed-image]').should('exist');
    });

    it('Title is Corresponding with accessed version', () => {
        getElmnt('version-title', 'title').eq(0).then(div => {
            expect(div.text()).to.eq('Animated Clock v-2.0');
        });
    });
});

function openPackageVersionPreview() {
    cy.server();
    cy.visit('http://localhost:3000');
    cy.get('[data-cy=input]').eq(0).type('admin');
    cy.get('[data-cy=input]').eq(1).type('admin');
    cy.get('[data-cy=button]').click();
    cy.get('[data-cy=page-button]').eq(5).click();
}
// ====== SELECTORS ======

function getElmnt(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}