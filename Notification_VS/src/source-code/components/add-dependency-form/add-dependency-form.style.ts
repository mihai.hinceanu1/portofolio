import styled from 'styled-components/native';

export const AddDependencyForm = styled.View`
    margin: 20px 25px;
`;

export const Label = styled.Text``;

export const Actions = styled.View`
    margin-top: 20px;
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
`;

// ====== CONST ======

export const BUTTON_OVERRIDES = {
    root: `
        margin-left: 10px;
    `
};