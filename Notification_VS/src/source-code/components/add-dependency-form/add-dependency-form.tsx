import * as div from './add-dependency-form.style';
import { Button } from '../../../shared/components/vsc-button/button';
import { ButtonType } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';
import { IDependency } from '../../interfaces/floating-menu';
import { AddVersionDependencyReq } from '../../interfaces/package-version';
import { dependencies$, getDependencies } from '../../services/dependencies.service';
import { addPackageVersionDependency } from '../../services/package-version.service';
import React from 'react';
import { Picker } from 'react-native';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    pathName: string;
    closeModal: () => void;
}
interface State {
    dependency: string;
    version: string;
    dependencies: IDependency[];
}

export class AddDependencyForm extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            dependency: '',
            version: '',
            dependencies: null,
        };
    }

    public render() {
        const { closeModal } = this.props;
        const { dependency, dependencies, version } = this.state;
        return (
            <div.AddDependencyForm data-cy='add-dependency-form'>
                {/** Label */}
                <div.Label data-cy='dependency-label'>
                    Select technology
                </div.Label>

                {/** Select dependency */}
                <Picker selectedValue={dependency}
                    onValueChange={(dependency) => this.selectValue(dependency, 'dependency')}>

                    {
                        !!dependencies && dependencies.map(dependency =>
                            <Picker.Item key={dependency.pathName}
                                label={dependency.name}
                                value={dependency.name} />
                        )
                    }
                </Picker>

                {/** Versions */}
                {
                    !!dependency &&
                    <React.Fragment>
                        <div.Label data-cy='version-label'>
                            Select version
                        </div.Label>

                        <Picker selectedValue={version}
                            onValueChange={(version) => this.selectValue(version, 'version')}>
                            {
                                this.getDependencyVersions().map((version, index) =>
                                    <Picker.Item key={index}
                                        label={version}
                                        value={version} />
                                )

                            }
                        </Picker>

                    </React.Fragment>

                }

                {/** Submit/Cancel */}
                <div.Actions data-cy='actions'>
                    <Button type={ButtonType.GHOST}
                        text='Cancel'
                        callback={closeModal}
                        color={colors.$blue}
                        overrides={div.BUTTON_OVERRIDES} />

                    <Button type={ButtonType.GHOST}
                        text='Save'
                        callback={() => this.handleSubmit()}
                        color={colors.$blue}
                        overrides={div.BUTTON_OVERRIDES} />
                </div.Actions>
            </div.AddDependencyForm>
        );
    }

    public componentDidMount() {
        this.subscribeToDependencies();
        getDependencies();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToDependencies() {
        dependencies$().pipe(
            takeUntil(this.destroyed$)
        )
            .subscribe(dependencies => {
                /**
                 * Check if we get data and set a default dependency,
                 * otherwise the first dependency will appear by default
                 * in the picker but it can't be selected, therefore, no
                 * versions will appear.
                 */
                if (dependencies && dependencies.length > 0) {
                    this.setState({
                        dependencies,
                        dependency: dependencies[0].name,
                    });
                }
            });
    }

    private getDependencyVersions() {
        const { dependency, dependencies } = this.state;

        let versions = dependencies.find(iterator => iterator.name === dependency).versions;
        return versions;
    }

    private selectValue(value: string, field: string) {
        this.setState({
            ...this.state,
            [field]: value,
        });
    }

    private handleSubmit() {
        const { dependency, dependencies } = this.state;
        const { pathName, closeModal } = this.props;

        let dependencyPathName = dependencies.find(dep => dep.name === dependency).pathName;
        let addDependencyReq: AddVersionDependencyReq = {
            pathName,
            dependencyPathName,
        };

        addPackageVersionDependency(addDependencyReq);
        closeModal();
    }
}
