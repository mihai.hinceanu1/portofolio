import { InputOverrides } from '../../../shared/interfaces/input';
import styled from 'styled-components/native';

export const AddPackageForm = styled.View`
    margin: 20px 25px;
`;

export const Actions = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
`;

// ====== CONST ======

export const INPUT_OVERRIDES: InputOverrides = {
    root: `
        flex-grow: 1;
        width: auto;
    `
};

export const TEXTAREA_OVERRIDES = {
    root: `
        flex-grow: 1;
        width: auto;
    `,
};