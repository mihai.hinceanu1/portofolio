import * as div from './add-package-form.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { Textarea } from '../../../shared/components/textarea/textarea';
import { NewPackageConfig } from '../../interfaces/packages';
import { createPackage } from '../../services/packages.service';
import React from 'react';

interface Props {
    closeModal: () => void;
}
interface State {
    name: string;
    description: string;
}

/**
 * The form with the necessary data for creating a new package.
 * Also contains and handles the submit/cancel functionalities.
 */
export class AddPackageForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            name: '',
            description: '',
        };
    }

    public render() {
        const { closeModal } = this.props;
        const { name, description } = this.state;

        return (
            <div.AddPackageForm data-cy='add-package-form'>

                {/** Name field */}
                <Input value={name}
                    label='Package name'
                    onChangeText={(text) => this.handleTextChange(text, 'name')}
                    overrides={div.INPUT_OVERRIDES} />

                {/** Description field */}
                <Textarea value={description}
                    label='Package description'
                    onChangeText={(text) => this.handleTextChange(text, 'description')}
                    overrides={div.TEXTAREA_OVERRIDES} />

                {/** Submit/Cancel */}
                <div.Actions data-cy='actions'>
                    <Button config={{
                        text: 'Cancel',
                        onPress: closeModal,
                    }} />

                    <Button config={{
                        text: 'Create',
                        onPress: () => this.handleSubmit(),
                    }} />
                </div.Actions>
            </div.AddPackageForm>
        );
    }

    private handleTextChange(text: string, field: string) {
        this.setState({
            ...this.state,
            [field]: text,
        });
    }

    private handleSubmit() {
        const { closeModal } = this.props;
        const { name, description } = this.state;
        let newPackage: NewPackageConfig = {
            name,
            description,
        };
        createPackage(newPackage);
        closeModal();
    }
}