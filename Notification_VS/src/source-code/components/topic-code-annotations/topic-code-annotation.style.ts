import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const TopicCodeAnnotation = styled.View<{startRow: number}>`
    position: absolute;
    top: ${props => `${props.startRow * 15}px`};
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: 'red';

    flex-direction: row;
    width: 100%;
`;

interface Props {
    colorpalette?: string;
    active?: string;
}

export const TopicCodeAnnotationTexts = styled.View<Props>`
    background-color: ${props => getCustomAnnotationColor(props.colorpalette, props.active)};
    max-width: 250px;
    padding: 10px;
    border-radius: 3px;
`;

export const TopicCodeAnnotationName = styled.Text<Props>`
    color: ${colors.$link};
    font-size: ${props => props.active === 'true' ? '16px' : '14px'};
`;

export const TopicCodeAnnotationDescription = styled.Text<Props>`
    flex: 2;
    font-size: ${props => props.active === 'true' ? '14px' : '12px'};

    /** TODO double check the combine bellow in one condition for active and secondary theme */
    color: ${props => props.colorpalette === 'secondary' ?
        props.active === 'true' ? 'white' : 'black'
        : props.active === 'true' ? 'black' : 'white'};
    /* color: ${props => props.active === 'true' ? 'black' : 'white'}; */

`;

export const TopicCodeAnnotationLine = styled.View<Props>`
    background-color: ${props => props.colorpalette === 'secondary' ? 'black' : 'white'};
    height: 2px;
    flex: 1;
`;

// Background color based on active or not and the chosen color palette
function getCustomAnnotationColor(palette: string, isActive: string): string {
    let backgroundColor = 'inherit';

    if (isActive === 'true') {
        if (palette === 'secondary') {
            backgroundColor = 'black';
        } else {
            backgroundColor = 'white';
        }
    }

    return backgroundColor;
}