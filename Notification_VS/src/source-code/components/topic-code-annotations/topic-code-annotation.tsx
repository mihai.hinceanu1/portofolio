import * as div from './topic-code-annotation.style';
import { isNotUndefined } from '../../../shared/services/failsafe.utils';
import { CodeSnippet } from '../../interfaces/code-snippet';
import * as React from 'react';

interface Props {
    startRow: number;
    codeSnippet: CodeSnippet;
    isActive?: boolean;

    /**
     * Line and description are by default white (based on a dark background as in overlay)
     * for use on a white background added a secondary color theme
     */
    colorpalette?: string;
}

interface State { }

export class TopicCodeAnnotation extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { startRow, codeSnippet, isActive, colorpalette } = this.props;

        return (
            <div.TopicCodeAnnotation data-cy='topic-code-annotation' startRow={startRow}>
                {

                    isNotUndefined(codeSnippet) &&
                    <>
                        <div.TopicCodeAnnotationTexts active={isActive ? 'true' : 'false'}
                            colorpalette={colorpalette ? colorpalette : 'default'}>
                            {/** Name */}
                            <div.TopicCodeAnnotationName data-cy='topic-code-annotation-name'
                                active={isActive ? 'true' : 'false'}>
                                {codeSnippet.name}
                            </div.TopicCodeAnnotationName>

                            {/** Description */}
                            <div.TopicCodeAnnotationDescription data-cy='topic-code-annotation-description'
                                active={isActive ? 'true' : 'false'}
                                colorpalette={colorpalette ? colorpalette : 'default'}>
                                {codeSnippet.description}
                            </div.TopicCodeAnnotationDescription>
                        </div.TopicCodeAnnotationTexts>

                        {/** Line */}
                        <div.TopicCodeAnnotationLine
                            colorpalette={colorpalette ? colorpalette : 'default'} />
                    </>
                }
            </div.TopicCodeAnnotation>
        );
    }
}
