import { TopicCodeAnnotation } from './topic-code-annotation';
import { isNotUndefined } from '../../../shared/services/failsafe.utils';
import { CodeSnippet } from '../../interfaces/code-snippet';
import { SourceFile } from '../../interfaces/source-file';
import * as React from 'react';
import { View } from 'react-native';

interface Props {
    // markers: IMarker[]; // TODO Update to monaco
    markers: any[];
    codeSnippets: CodeSnippet[];
    totalNumberOfLines: number;
    pxScrolled: number;
    activeCodeSnippetId: string;
    sourceFile: SourceFile;

    /**
     * Line and description are by default white (based on a dark background as in overlay)
     * for use on a white background added a secondary color theme
     */
    colorpalette?: string;
}

interface State { }

/**
 * Shows annotations for each code marker rendered in CodeEditor
 * It must have markers as PROPS, possible annotations for each marker... WIP
 */
export class TopicCodeAnnotations extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            codeSnippets: [] as CodeSnippet[],
        };
    }

    public render() {
        const { markers, totalNumberOfLines, pxScrolled, activeCodeSnippetId, codeSnippets, colorpalette } = this.props;
        return (
            <View data-cy='topic-code-annotation'
                style={{
                    position: 'relative',
                    height: `${totalNumberOfLines * 15.5}px`,
                    top: `-${pxScrolled}px`,
                }}>
                {
                    // activeCodeSnippetId --> TODO failsafe for no activeCodeSnippetId
                    !!markers.length && !!codeSnippets.length &&
                    markers.map(
                        (marker, index) =>
                            isNotUndefined(codeSnippets) &&
                            <TopicCodeAnnotation key={index}
                                isActive={(isNotUndefined(codeSnippets[index])) ?
                                    activeCodeSnippetId === codeSnippets[index]._id : false}
                                startRow={marker.startRow - 2}
                                codeSnippet={codeSnippets[index]}
                                colorpalette={colorpalette ? colorpalette : 'default'} /> ,

                    )
                }
            </View>
        );
    }

}