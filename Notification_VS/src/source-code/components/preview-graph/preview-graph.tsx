import * as div from './preview-graph.style';
import { getBezierLineChart, testTimeSeries } from './preview-graph.utils';
import * as React from 'react';
import { LayoutChangeEvent, TouchableOpacity } from 'react-native';

interface Props {
    isNarrow: boolean;
 }

interface State {
    width: number;
    height: number;
    pressedButton: string;
}

export class PreviewGraph extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: 0,
            height: 0,
            pressedButton: 'views',
        };
    }

    public render() {
        const { isNarrow } = this.props;
        const { width, height, pressedButton } = this.state;

        return (
            <div.GraphWrapper data-cy='graph-wrapper' isNarrow={isNarrow}>
                <div.GraphHeader>
                    <div.Date>
                        3/31/2020
                    </div.Date>

                    <div.ContentModifier>
                        <TouchableOpacity onPress={() => this.setPressedButton('views')}>
                            <div.ViewsButtonText pressedButton={pressedButton}>
                                Views
                            </div.ViewsButtonText>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setPressedButton('purchases')}>
                            <div.PurchaseButtonText pressedButton={pressedButton}>
                                Purchases
                            </div.PurchaseButtonText>
                        </TouchableOpacity>
                    </div.ContentModifier>

                </div.GraphHeader>

                <div.Chart data-cy='chart'
                    onLayout={(event) => this.getChartLayout(event)} >
                    {
                        getBezierLineChart(testTimeSeries, width, height)
                    }
                </div.Chart>

            </div.GraphWrapper>
        );
    }

    private setPressedButton(button: string) {
        this.setState({
            pressedButton: button,
        });
    }

    private getChartLayout(event: LayoutChangeEvent) {
        var { width, height } = event.nativeEvent.layout;

        this.setState({ width: width, height: height });
    }
}