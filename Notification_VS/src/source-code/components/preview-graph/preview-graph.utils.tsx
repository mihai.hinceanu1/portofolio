import { TimeSeries } from '../../../lessons-catalog/interfaces/lesson-card-details';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import { LineChart } from 'react-native-chart-kit';

export const getBezierLineChart = (timeSeries: TimeSeries[], width: number, height: number) => {
    let labels = getLabelsAndValues(timeSeries).labels;
    let values = getLabelsAndValues(timeSeries).values;

    return (
        <LineChart
            data={{
                labels: labels,
                datasets: [{
                    data: values,
                }]
            }
            }
            width={width} // from react-native
            height={height}
            chartConfig={{
                strokeWidth: 3,
                backgroundColor: 'white',
                backgroundGradientFrom: 'white',
                backgroundGradientTo: 'white',
                decimalPlaces: 0,
                color: () => colors.$blue,
                labelColor: (opacity = 1) => `rgba(0,0,0, ${opacity})`,
                style: {
                    borderRadius: 16
                },
                propsForBackgroundLines: {
                    strokeDasharray: '',
                    stroke: colors.$lightGrey,
                },
                propsForLabels: {
                    fontWeight: 600,
                },
                propsForDots: {
                    r: '6',
                    stroke: 'red'
                }
            }}
            withOuterLines={false}
            withHorizontalLabels={true}
            withVerticalLabels={true}
            withDots={true}
            getDotColor={() => 'red'}
            bezier={true}
            style={{
                borderRadius: 16
            }}
        />
    );
};

export const getLabelsAndValues = (data: TimeSeries[]) => {
    const labels = data.map(el => el.date.toString().split('')[0]);
    const values = data.map(el => el.value);

    return ({
        labels: labels,
        values: values,
    });
};

export const testTimeSeries = [
    {
        date: new Date('2015-03-25'),
        value: 10,
    },
    {
        date: new Date('2015-03-26'),
        value: 21,
    },
    {
        date: new Date('2015-03-24'),
        value: 22,
    },
    {
        date: new Date('2015-03-23'),
        value: 23,
    },
    {
        date: new Date('2015-03-22'),
        value: 210,
    },
    {
        date: new Date('2015-03-21'),
        value: 5,
    },
    {
        date: new Date('2015-03-20'),
        value: 1,
    },
    {
        date: new Date('2015-03-19'),
        value: 333,
    },
    {
        date: new Date('2015-03-18'),
        value: 232,
    },
    {
        date: new Date('2015-03-26'),
        value: 123,
    },
    {
        date: new Date('2015-03-27'),
        value: 321,
    },
    {
        date: new Date('2015-03-28'),
        value: 1210,
    },
    {
        date: new Date('2015-03-29'),
        value: 102,
    },
    {
        date: new Date('2015-03-30'),
        value: 1210,
    },
];
