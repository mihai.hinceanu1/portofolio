import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const GraphWrapper = styled.View<{isNarrow: boolean}>`
    display: flex;
    flex-direction: column;
    ${props => props.isNarrow ? 'width: 100%' : 'width: 70%'};
`;

export const GraphHeader = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const Date = styled.Text`
    font-size: ${font.$size14}px;
    font-weight: 600;
`;

export const ContentModifier = styled.View`
    display: flex;
    flex-direction: row;
`;

export const ViewsButtonText = styled.Text<{pressedButton: string}>`
    font-size: ${font.$size14}px;
    font-weight: 600;
    margin-right: 15px;
    color: ${props => props.pressedButton === 'views' ? colors.$blue : colors.$darkGrey};
`;

export const PurchaseButtonText = styled.Text<{pressedButton: string}>`
    font-size: ${font.$size14}px;
    font-weight: 600;
    color: ${props => props.pressedButton === 'purchases' ? colors.$blue : colors.$darkGrey};
`;

export const Chart = styled.View`
    height: 200px;
    width: 100%;
    margin-top: 20px;
`;