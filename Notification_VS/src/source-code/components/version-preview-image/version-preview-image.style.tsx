import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import styled from 'styled-components/native';

interface Override {
    override: {};
}

interface SvgBackground {
    backgroundColor: string;
}

interface FrameDimensions {
    width?: number;
    height?: number;
    flexBasis?: number;
}

export const VersionPreviewImage = styled.View<Override>`
    display: flex;
    flex-direction: column;
    margin: 15px;
    ${props => props.override};
`;

export const Title = styled.Text`
    font-size: 17px;
    font-weight: 700;
    color: ${colors.$black};
`;

export const PreviewTitle = styled.View`
    align-items: flex-start;
    justify-content: center;
`;

export const ImageFrame = styled.View<FrameDimensions>`
    /* flex: 1; */
    width: ${props => getFrameDimensions(props).width}px;
    height: ${props => getFrameDimensions(props).height}px;
    position: relative;
`;

export const Image = styled.Image`
    resize-mode: contain;
    flex: 1;
`;

export const RoundedBgr = styled.View<SvgBackground>`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    position: absolute;
    justify-content: center;
    align-items: center;
    bottom: -15px;
    right: -15px;
    background-color: ${props => getSvgBackgroundColor(props.backgroundColor)};
`;

// ====== UTILS ======

// default value if color is not provided
const getSvgBackgroundColor = (color: string) => {
    return color ? color : '#0D8EF1';
};

export const getSvg = (svgPath: string, fill: string, backgroundColor?: string) => {
    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;

    return (
        <RoundedBgr data-cy='rounded-svg-bgr' backgroundColor={backgroundColor}>
            <Svg data-cy='preview-image-svg'
                width={width}
                height={height}
                fill='none'>
                <Path data-cy='svg-path' d={svgPath} fill={fill} />
            </Svg>
        </RoundedBgr>
    );
};

function getFrameDimensions(props: FrameDimensions): FrameDimensions {
    const MAX_HEIGHT = 200;
    const MAX_WIDTH = 400;
    const { width, height } = props;

    // set max value to width and calculate dynamic height to keep ratio
    if (width > MAX_WIDTH) {
        let newHeight = height * MAX_WIDTH / width;
        let newWidth = MAX_WIDTH;

        // if the new calculated height is still greater than MAX_HEIGHT
        // set height to max and recalculate width
        if (newHeight > MAX_HEIGHT) {
            return {
                width: newWidth * MAX_HEIGHT / newHeight,
                height: MAX_HEIGHT,
            };
        }

        return {
            width: MAX_WIDTH,
            height: height * MAX_WIDTH / width,
        };
    }

    // set max value to height and calculate dynamic width to keep ratio
    if (height > MAX_HEIGHT) {
        let newWidth = width * MAX_HEIGHT / height;
        let newHeight = MAX_HEIGHT;

        // if the newly calculated width is still greater than MAX_WIDTH
        // set width to max value and recalculate height
        if (newWidth > MAX_WIDTH) {
            return {
                height: newHeight * MAX_WIDTH / newWidth,
                width: MAX_WIDTH,
            };
        }

        return {
            width: width * MAX_HEIGHT / height,
            height: MAX_HEIGHT
        };
    }

    return { width, height };
}
