import { ALL, LATEST, POPULAR } from '../../../../shared/assets/icons';
import { colors } from '../../../../shared/style/colors';
import { VersionPreviewImageCfg } from '../../../interfaces/version-preview-image';

export const standard: VersionPreviewImageCfg = {
    imgPath: 'https://www.w3schools.com/css/img_forest.jpg',
    icon: {
        path: ALL,
        fill: 'white',
    }
};

export const customImage: VersionPreviewImageCfg = {
    imgPath: 'https://cdn.mos.cms.futurecdn.net/8qsCQQpvEhq4Y8T2czH7qY.jpg',
    icon: {
        path: LATEST,
        fill: 'white',
    }
};

export const customTitle: VersionPreviewImageCfg = {
    title: 'Hello',
    imgPath: 'https://i.pinimg.com/originals/e2/b8/2a/e2b82aded815e80351b929a77519adaa.jpg',
    icon: {
        path: LATEST,
        fill: 'white',
    }
};

export const customIcon: VersionPreviewImageCfg = {
    imgPath: 'https://www.smallbizgenius.net/wp-content/uploads/2019/06/smallbizgenius_favicon.png',
    icon: {
        path: POPULAR,
        fill: colors.$green,
    }
};