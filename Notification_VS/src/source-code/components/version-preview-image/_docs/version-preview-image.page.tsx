import * as samples from './version-preview-image-page.samples';
import * as cfg from './version-preview-image-page.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { VersionPreviewImage } from '../version-preview-image';
import * as React from 'react';

export class VersionPreviewImagePage extends ScrollableDocPage<{}> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width} nativeID='checkbox-page'>
                    <div.OverviewTitle>
                        Version Preview Image
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Preview image from certain lesson page with additional Title and SvgIcon.
                        Images size are maximum 400 width and 200 height. If they are greater than those
                        values, they are compressed so that each of them will keep its aspect ratio.
                    </div.OverviewDescription>
                </div.Overview>

                {/* STANDARD */}
                <div.Demo width={width} data-cy='standard'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <VersionPreviewImage config={cfg.standard} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.standard} />
                </div.Demo>

                {/* CUSTOM IMAGE*/}
                <div.Demo width={width} data-cy='custom-image'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Image
                        </div.DemoTitle>

                        <VersionPreviewImage config={cfg.customImage} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.customImage} />
                </div.Demo>

                {/* CUSTOM TITLE */}
                <div.Demo width={width} data-cy='custom-title'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Title
                        </div.DemoTitle>

                        <VersionPreviewImage config={cfg.customTitle} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.customTitle} />
                </div.Demo>

                {/* CUSTOM ICON */}
                <div.Demo width={width} data-cy='custom-icon'>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Custom Icon
                        </div.DemoTitle>

                        <VersionPreviewImage config={cfg.customIcon} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig}
                        srcCode={samples.customIcon} />
                </div.Demo>
            </>
        );

    }
}