export const standard = `
export const standard: VersionPreviewImageCfg = {
    title: 'Standard',
    imgPath: 'https://www.w3schools.com/css/img_forest.jpg',
    icon: {
        path: ALL,
        fill: 'white',
    }
};`;

export const customImage = `
export const customImage = VersionPreviewImageCfg = {
    imgPath: 'path/to/any/image',
}
`;

export const customTitle = `
export const customImage = VersionPreviewImageCfg = {
    title: 'Custom Title Inserted Here',
}`;

export const customIcon = `
export const customImage = VersionPreviewImageCfg = {
    icon: {
        path: POPULAR,
        fill: colors.$green,
    }
}`;
