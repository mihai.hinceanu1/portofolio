import * as div from './version-preview-image.style';
import { VersionPreviewImageCfg } from '../../interfaces/version-preview-image';
import * as React from 'react';
import { Image } from 'react-native';

interface Props {
    config: VersionPreviewImageCfg;

    /** Override CSS styles */
    override?: string;
}

interface State {
    width: number;
    height: number;
}

/**
 * Renders a preview image of the current state of the lesson
 * Depending on their needs, authors can add different previews to the current version
 */
export class VersionPreviewImage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            width: 0,
            height: 0
        };
    }

    public render() {
        const { width, height } = this.state;
        const { override, config } = this.props;
        const { path, fill } = config.icon;

        return (
            <div.VersionPreviewImage data-cy='version-preview-image' override={override}>

                {/** Title */}
                <div.PreviewTitle data-cy='preview-title'>
                    <div.Title data-cy='title'>
                        {config.title}
                    </div.Title>
                </div.PreviewTitle>

                {/** Image */}
                <div.ImageFrame data-cy='image-frame' width={width} height={height}>
                    <div.Image source={{ uri: `${config.imgPath}` }} />

                    {/** Svg Icon */}
                    {div.getSvg(path, fill)}
                </div.ImageFrame>
            </div.VersionPreviewImage>
        );
    }

    public componentDidMount() {
        this.getImageSize();
    }

    private getImageSize() {
        const { imgPath } = this.props.config;

        // retrieve size of the image
        Image.getSize(imgPath, (width: number, height: number) => {
            this.setState({ width, height });
        }, err => {
            console.error(err);
        });
    }
}
