/// <reference types="cypress" />

describe('Version Preview Image Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/source-code/version-preview-image');
    });

    it('Component Is Rendered', () => {
        getElmt('standard', 'version-preview-image').should('exist');
    });

    it('Title wrapper is rendered', () => {
        getElmt('standard', 'preview-title').should('exist');
    });

    it('Image is Rendered', () => {
        getElmt('standard', 'image-frame').should('exist');
    });

    it('Rounded Bgr is Rendered', () => {
        getElmt('standard', 'rounded-svg-bgr').should('exist');
    });

    it('Svg is Rendered', () => {
        getElmt('standard', 'preview-image-svg').should('exist');
    });

    it('Given Title is rendered successfully', () => {
        getElmt('custom-title', 'title').should(title => {
            expect(title.text()).to.eq('Custom Title Inserted Here');
        });
    });

    it('Given custom color, SVG fill is rendered with that color', () => {
        getElmt('custom-icon', 'preview-image-svg').within(_svg => {
            cy.get('path').should('have.attr', 'fill', '#00c3a7');
        });
    });

    it('Given large image it streches to 400x200 maximum', () => {
        getElmt('custom-image', 'image-frame').within(_image => {
            cy.get('img').its('width').should('not.be.greaterThan', 400);
            cy.get('img').its('height').should('not.be.greaterThan', 200);
        });
    });
});

// ====== UTILS ======

function getElmt(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}