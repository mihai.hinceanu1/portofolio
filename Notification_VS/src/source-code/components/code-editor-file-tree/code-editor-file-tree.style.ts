import { SEARCH } from '../../../shared/assets/icons';
import { TIcon } from '../../../shared/interfaces/icon';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const CodeEditorFileTree = styled.View`
    width: 20%;
    background-color: white;
    border-right-color: ${colors.$lightGrey};
    border-right-width: 0.5px;
    border-style: solid;
`;

export const Frame = styled.View`
    width: 215px;
    background-color: ${colors.$white};
    padding-bottom: 50px;
`;

export const Title = styled.Text`
    text-align: center;
    padding: 10px 15px;
    font-size: ${font.$size12}px;
    background-color: ${colors.$white};
    align-self: flex-start;
    z-index: 100;
`;

// ===== OVERRIDES =====

export const Input = {
    root: `
        width: 100%;
        margin-top: 10px;
    `,
    inputField: `
        border-radius: 20px;
        height: 30px;
        font-size: ${font.$size12}px;
        font-weight: 600;
    `,
};

// ===== ICONS =====

export const SearchIcon: TIcon = {
    type: 'svg',
    svgPath: SEARCH,
    fill: colors.$darkGrey,
};
