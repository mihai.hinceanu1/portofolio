import * as div from './code-editor-file-tree.style';
import { ICodeEditorTree } from '../../interfaces/code-editor-tree';
import { SourceFileTreeLevel } from '../../interfaces/source-file-tree';
import { CodeEditorTreeNode } from '../code-editor-tree-node/code-editor-tree-node';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps, ICodeEditorTree {
}

interface State {
    searchTerm: string;
}

/** Similar to file explorer in Visual Studio Code */
export class _CodeEditorFileTree extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            searchTerm: '',
        };
    }

    public render() {
        const { levels, collapseIcon, expandIcon, onLevelCollapse, onLevelExpand, onLevelClick } = this.props;

        return (
            <div.CodeEditorFileTree data-cy='code-editor-file-tree' >

                {/** Frame body */}
                <div.Frame data-cy='frame'>

                    {/** Tree Listing */}
                    {
                        !!levels &&
                        this.sortLevels().map((node, index) =>
                            <CodeEditorTreeNode key={index}
                                isFolder={node.isFolder}
                                level={node}
                                collapseIcon={collapseIcon}
                                expandIcon={expandIcon}
                                onLevelCollapse={(level: SourceFileTreeLevel) => onLevelCollapse && onLevelCollapse(level)}
                                onLevelExpand={(level: SourceFileTreeLevel) => onLevelExpand && onLevelExpand(level)}
                                onLevelClick={(level: SourceFileTreeLevel) => onLevelClick && onLevelClick(level)} />
                        )
                    }
                </div.Frame>
            </div.CodeEditorFileTree>
        );
    }

    public sortLevels() {
        const { levels } = this.props;
        levels.sort((obj1: SourceFileTreeLevel, obj2: SourceFileTreeLevel) => (obj1.isFolder === obj2.isFolder) ? 0 : obj1 ? -1 : 1);

        return levels;
    }
}

export const CodeEditorFileTree = withRouter<Props, any>(_CodeEditorFileTree);
