import { CodeSnippet } from '../../interfaces/code-snippet';
import * as React from 'react';
import { Text, View } from 'react-native';

interface Props {
    topicSnippet: CodeSnippet;
}

interface State { }

/**
 * Renders the snippets related with the active topic
 */
export class TopicAdminSnippet extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
    }

    public render() {
        const { topicSnippet } = this.props;
        return (
            !!topicSnippet &&
            <View data-cy='topic-admin-snippet'>
                <Text data-cy='snippet-description'>{topicSnippet.description}</Text>
            </View>
        );
    }
}