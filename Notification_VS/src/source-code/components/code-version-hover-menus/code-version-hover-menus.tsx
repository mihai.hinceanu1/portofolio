import { learningMapMenu } from '../../../lessons/components/lesson-hover-menus/lesson-hover-menus.configs';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { DotsMenu } from '../../../shared/components/dots-menu/dots-menu';
import * as sharedCfg from '../../../shared/constants/hover-menus.configs';
import * as hover from '../../../shared/style/hover-menus.style';
import * as React from 'react';
import { LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps { }

interface State {
    width: number;
}

class _CodeVersionHoverMenus extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: 0,
        };
    }

    public render() {
        const { width } = this.state;

        return (
            <hover.Menus data-cy='code-version-hover-menus'
                pointerEvents='box-none'
                onLayout={(e: LayoutChangeEvent) => this.updateWidth(e)}>

                {/* Learning Map Domains */}
                <BranchesMenu config={learningMapMenu}
                    overrides={hover.overrideTopLeftMenu(width)} />

                {/* User Menu */}
                <DotsMenu config={sharedCfg.userMenu}
                    overrides={hover.overrideTopRightMenu(width)} />

            </hover.Menus>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }
}

export const CodeVersionHoverMenus = withRouter(_CodeVersionHoverMenus);