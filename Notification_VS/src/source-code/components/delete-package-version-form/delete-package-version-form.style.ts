import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface IsNarrow {
    isNarrow: boolean;
}

export const DeletePackageVersionForm = styled.View<IsNarrow>`
    width: 300px;
    display: flex;
    flex-direction: column;
    background-color: white;
    justify-content: flex-start;
    align-items: center;
    align-self: center;
    ${props => !props.isNarrow && 'margin-top: 350px;'}
`;

export const CloseIconButton = styled.TouchableOpacity`
    width: 30px;
    height: 30px;
`;

export const CloseIconText = styled.Text`
    font-size: ${font.$size18}px;
    color: ${colors.$darkGrey};
    font-weight: 600;
`;

export const ButtonsWrapper = styled.View`
    margin-top: 15px;
    margin-bottom: 15px;
    width: 90%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;