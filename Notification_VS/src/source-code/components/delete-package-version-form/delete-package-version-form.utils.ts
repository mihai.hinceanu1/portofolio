import { ButtonConfig } from '../../../shared/interfaces/button';
import { font } from '../../../shared/style/font-sizes';

export const saveButtonCfg = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            text: 'Yes',
            fontWeight: 600,
            fontSize: font.$size14,
            width: 100,
        }
    );
};

export const cancelDeleteButtonCfg = (onPress: () => void): ButtonConfig => {
    return (
        {
            onPress: () => onPress(),
            text: 'No',
            fontWeight: 600,
            fontSize: font.$size14,
            width: 100,
        }
    );
};