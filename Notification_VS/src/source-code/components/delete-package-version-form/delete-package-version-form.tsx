import * as div from './delete-package-version-form.style';
import * as utils from './delete-package-version-form.utils';
import { EDIT_PACKAGE_DETAILS } from '../../../shared/assets/icons';
import { Button } from '../../../shared/components/button/button';
import { ModalBoxHeader } from '../../../shared/components/modal-box-header/modal-box-header';
import { DeletePackageVersionConfig } from '../../interfaces/source-code';
import { deletePackageVersion } from '../../services/package-version.service';
import * as React from 'react';

interface Props {
    close: () => void;
    versionId: string;
    packageId: string;
    isNarrow: boolean;
}

interface State { }

export class DeletePackageForm extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { isNarrow, close } = this.props;

        return (
            <div.DeletePackageVersionForm data-cy='delete-package-version-form' isNarrow={isNarrow}>

                {/* Modal Box Header */}
                <ModalBoxHeader icon={EDIT_PACKAGE_DETAILS}
                    title='DELETE PACKAGE VERSION'
                    closeModal={close} />

                {/* Buttons */}
                <div.ButtonsWrapper data-cy='buttons-wrapper'>
                    <Button config={utils.cancelDeleteButtonCfg(() => this.cancelDeletePackage())} />
                    <Button config={utils.saveButtonCfg(() => this.saveDeletePackage())} />
                </div.ButtonsWrapper>

            </div.DeletePackageVersionForm>
        );
    }

    private cancelDeletePackage() {
        const { close } = this.props;

        !!close && close();
    }

    private saveDeletePackage() {
        const { versionId, packageId, close } = this.props;

        const deleteConfig: DeletePackageVersionConfig = {
            packageVersionId: versionId,
            packageId
        };

        close();
        deletePackageVersion(deleteConfig);
    }
}