/// <reference types="cypress" />

describe('Floating Menu Specs', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/source-code/floating-menu');
    });

    it('Menu is Rendered', () => {
        cy.get('[data-cy=floating-menu]').should('exist');
    });

    it('Dependencies are rendered default', () => {
        getItem('floating-menu', 'dependencies').should('exist');
    });

    it('Selected Tab Title is Rendered', () => {
        getItem('body-title','title').should(title => {
            expect(title.text()).to.eq('Dependencies');
        });
    });

    it('Add Button Is Rendered', () => {
        cy.get('[data-cy=button]').should('exist');
    });

    it('Selected tab is blue', () => {
        cy.get('[data-cy=tab]').eq(0).should('have.css', 'background-color', 'rgb(13, 142, 241)');
    });

    it('Unselected tab is white', () => {
        cy.get('[data-cy=tab]').eq(1).should('have.css', 'background-color', 'rgb(255, 255, 255)');
    });

    it('Tabs are rendered', () => {
        getItem('floating-menu', 'tab').its('length').should('eq', 7);
    });

    it('Tab Callback', () => {
        getItem('floating-menu', 'tab').eq(1).click();
        getItem('body-title', 'title').should(title => {
            expect(title.text()).to.eq('Snippets');
        });
    });

    it('Clickable Elements', () => {
        getItem('floating-menu', 'tab').eq(2).click();
        cy.get('[data-cy=flow]').eq(0).click();
        cy.get('[data-cy=flow]').should('have.css', 'background-color', 'rgba(13, 142, 241, 0.1)');
    });

    it('X Button on Clickable Elements exists', () => {
        cy.get('[data-cy=close-button]').should('exist');
    });

    it('Close Button Works', () => {
        cy.get('[data-cy=close-button]').click();
        cy.get('[data-cy=flow]').eq(0).should('have.css', 'background-color', 'rgba(0, 0, 0, 0)');
    });

    it('Multiple Elements Can Be Clicked on Flows', () => {
        cy.get('[data-cy=flow]').eq(0).click();
        cy.get('[data-cy=flow]').eq(1).click();
        cy.get('[data-cy=close-button]').its('length').should('eq', 2);
    });

    it('Single Items Can Be Clicked Showcase', () => {
        cy.get('[data-cy=tab]').eq(6).click();
        cy.get('[data-cy=showcase]').eq(0).click();
        cy.get('[data-cy=showcase]').eq(1).click();
        cy.get('[data-cy=close-button]').its('length').should('eq', 1);
    });

    it('Snippet Tab Renders Snippets', () => {
        cy.get('[data-cy=tab]').eq(1).click();
        cy.get('[data-cy=snippet]').should('exist');
    });

    it('Flows Tab Renders Flows', () => {
        cy.get('[data-cy=tab]').eq(2).click();
        cy.get('[data-cy=flow]').should('exist');
    });

    it('Revisions Tab Renders Revisions', () => {
        cy.get('[data-cy=tab]').eq(3).click();
        cy.get('[data-cy=revision]').should('exist');
    });

    it('Pseudocode Tab Renders Pseudocode', () => {
        cy.get('[data-cy=tab]').eq(4).click();
        cy.get('[data-cy=pseudocode]').should('exist');
    });

    it('Annotation Tab Renders Annotations', () => {
        cy.get('[data-cy=tab]').eq(5).click();
        cy.get('[data-cy=annotations]').should('exist');
    });

    it('Showcase Tab Renders Showcases', () => {
        cy.get('[data-cy=tab]').eq(6).click();
        cy.get('[data-cy=showcase]').should('exist');
    });
});

// ====== SELECTORS ======

function getItem(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element}]`);
}

function getMultiple(wrapper, element1, element2) {
    return cy.get(`[data-cy=${wrapper}] [data-cy = ${element1}][data-cy = ${element2}]`);
}