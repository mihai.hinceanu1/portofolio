import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Dependency = styled.View`
    margin-top: 20px;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
    width: 100%;
    margin-left: 10px;
`;

export const DependencyBody = styled.View`
    display: flex;
    flex-direction: column;
    margin-left: 10px;
    width: 80%;
`;

export const TitleText = styled.Text`
    font-size: ${font.$size14}px;
`;

export const Description = styled.Text`
    margin-top: 5px;
    margin-bottom: 10px;
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;

export const Details = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
`;

export const DetailsText = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$darkGrey};
    font-weight: 500;
    margin-right: 15px;
`;

// ====== UTILS ======

// export const getDependencySvgs = (svgPath: string) => {
//     let boundingBox = pathBoundingBox(svgPath);
//     let { height, width } = boundingBox;

//     return (
//         <Svg data-cy='tab-svg'
//             width={width + 1}
//             height={height}
//             fill='none' style={{ backgroundColor: 'none', justifyContent: 'center', alignItems: 'center', marginLeft: 5, marginRight: 5 }} >
//             <Path data-cy='svg-path'
//                 d={svgPath} fill={colors.$blue} />
//         </Svg>
//     );
// };