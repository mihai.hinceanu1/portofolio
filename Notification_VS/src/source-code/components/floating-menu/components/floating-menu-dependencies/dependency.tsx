import { DOWNLOAD_SMALL } from './_assets/floating-menu-dependecies.assets';
import * as div from './dependency.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { colors } from '../../../../../shared/style/colors';
import { IDependency } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    dependency: IDependency;
}

interface State { }

export class Dependency extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { dependency } = this.props;

        return (
            <div.Dependency data-cy='dependency'>

                {/** Icon */}
                <Icon path={dependency.image} />

                <div.DependencyBody data-cy='dependency-body'>
                    {/** Title */}
                    <div.TitleText data-cy='title-text'>
                        {dependency.name}
                    </div.TitleText>

                    {/** Description */}
                    <div.Description data-cy='description'>
                        {dependency.description}
                    </div.Description>

                    {/** Details */}
                    <div.Details data-cy='details'>
                        <div.DetailsText data-cy='details-text'>
                            {dependency.versions[0]}
                        </div.DetailsText>

                        {/** Downloads */}
                        <Icon path={DOWNLOAD_SMALL} color={colors.$blue} />

                        <div.DetailsText data-cy='download-count'>
                            {dependency.monthlyDownloads}
                        </div.DetailsText>
                    </div.Details>
                </div.DependencyBody>
            </div.Dependency>
        );
    }
}