import * as div from './dependencies.style';
import { Dependency } from './dependency';
import { Button } from '../../../../../shared/components/vsc-button/button';
import { IDependency } from '../../../../interfaces/floating-menu';
import { getVersionDependencies, versionDependencies$ } from '../../../../services/dependencies.service';
import { getDependenciesButtonCfg } from '../../floating-menu.utils';
import { BUTTON_OVERRIDE } from '../floating-menu-body/floating-menu-body.style';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    addDependency: () => void;
    pathName: string;
}

interface State {
    dependencies: IDependency[];
}

/** List of main technologies used in a version of a code package. */
export class Dependencies extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            dependencies: null,
        };
    }

    public render() {
        const { addDependency } = this.props;
        const { dependencies } = this.state;

        return (
            <div.Dependencies data-cy='dependencies'>

                {/** Dependencies */}
                {
                    !!dependencies && dependencies.map((dependency, index) =>
                        <Dependency key={index} dependency={dependency} />
                    )
                }

                {/** Add dependency */}
                <Button {...getDependenciesButtonCfg(addDependency)} overrides={BUTTON_OVERRIDE} />
            </div.Dependencies>
        );
    }

    public componentDidMount() {
        this.subscribeToVersionDependencies();

        const { pathName } = this.props;
        getVersionDependencies(pathName);
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToVersionDependencies() {
        versionDependencies$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(dependencies => {
                this.setState({
                    dependencies,
                });
            });
    }
}
