import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Revision = styled.View`
    display: flex;
    flex-direction: column;
    padding: 10px 10px 5px 10px;
`;

export const Title = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const TitleText = styled.Text`
    font-size: ${font.$size14}px;
    margin-left: 5px;
`;

export const Description = styled.Text`
    margin-top: 5px;
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;
