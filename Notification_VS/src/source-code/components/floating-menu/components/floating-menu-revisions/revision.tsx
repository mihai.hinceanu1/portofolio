import { REVISION_SMALL } from './_assets/floating-menu-revision.assets';
import * as div from './revision.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { colors } from '../../../../../shared/style/colors';
import { IRevision } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    revision: IRevision;
}

interface State { }

export class Revision extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { revision } = this.props;

        return (
            <div.Revision data-cy='revision'>

                {/** Title */}
                <div.Title data-cy='title'>

                    {/** Icon */}
                    <Icon path={REVISION_SMALL} color={colors.$blue} />

                    {/** Text */}
                    <div.TitleText data-cy='title-text'>
                        {revision.title}
                    </div.TitleText>
                </div.Title>

                {/** Description */}
                <div.Description data-cy='description'>
                    {revision.description}
                </div.Description>
            </div.Revision>
        );
    }
}