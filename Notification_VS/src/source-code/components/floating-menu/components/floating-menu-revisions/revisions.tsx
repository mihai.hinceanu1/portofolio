import { Revision } from './revision';
import * as div from './revisions.style';
import { Button } from '../../../../../shared/components/vsc-button/button';
import { IRevision } from '../../../../interfaces/floating-menu';
import { revisionButtonCfg } from '../../floating-menu.utils';
import { BUTTON_OVERRIDE } from '../floating-menu-body/floating-menu-body.style';
import * as React from 'react';

interface Props {
    revisions: IRevision[];
}

interface State { }

export class Revisions extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { revisions } = this.props;

        return (
            <div.Revisions data-cy='revision'>

                {/** Revisions */}
                {
                    revisions.map((revision, index) =>
                        <Revision key={index} revision={revision} />
                    )
                }

                {/** Add revision */}
                <Button {...revisionButtonCfg} overrides={BUTTON_OVERRIDE} />
            </div.Revisions>
        );
    }
}