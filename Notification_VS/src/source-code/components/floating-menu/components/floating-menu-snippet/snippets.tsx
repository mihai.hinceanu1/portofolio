import { Snippet } from './snippet';
import * as div from './snippets.style';
import { ISnippet } from '../../../../interfaces/floating-menu';
import React from 'react';

interface Props {
    snippets: ISnippet[];
}

/** Iterate through all the snippets */
export const Snippets: React.FunctionComponent<Props> = (props: Props) => {
    const { snippets } = props;

    return (
        <div.Snippets data-cy='snippets'>
            {
                snippets.map((snippet, index) =>
                    <Snippet key={index} snippet={snippet} />
                )
            }
        </div.Snippets>
    );
};