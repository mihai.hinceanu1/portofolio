import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Snippet = styled.View`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    padding: 10px 10px 5px 10px;
`;

export const SnippetTitle = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const SnippetTitleText = styled.Text`
    font-size: ${font.$size14}px;
    margin-left: 5px;
`;

export const Description = styled.Text`
    margin-top: 5px;
    margin-bottom: 10px;
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;

export const Lines = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$darkGrey};
    font-weight: 500;
`;