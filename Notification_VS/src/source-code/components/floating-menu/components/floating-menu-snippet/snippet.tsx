import { SMALL_SNIPPET } from './_assets/floating-menu-snippet.assets';
import * as div from './snippet.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { colors } from '../../../../../shared/style/colors';
import { ISnippet } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    snippet: ISnippet;
 }

interface State { }

export class Snippet extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = { };
    }

    public render( ) {
        const { snippet } = this.props;

        return(
            <div.Snippet data-cy='snippet'>
                <div.SnippetTitle data-cy='snippet-title'>

                    <Icon path={SMALL_SNIPPET} color={colors.$blue} />

                    <div.SnippetTitleText data-cy='snippet-title-text'>
                        {snippet.title}
                    </div.SnippetTitleText>
                </div.SnippetTitle>

                <div.Description data-cy='description'>
                        {snippet.description}
                </div.Description>

                <div.Lines data-cy='lines'>
                    Ln {snippet.codeLines[0]} - {snippet.codeLines[1]}
                </div.Lines>
            </div.Snippet>
        );
    }
}