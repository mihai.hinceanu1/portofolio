import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

// ====== STYLE ======

export const FloatingMenuBody = styled.View`
    flex-grow: 1;
    max-width: 230px;
    width: 230px;
`;

export const Body = styled.ScrollView`
    height: 100%;
    max-height: 490px;
    width: 100%;
    background-color: ${colors.$white};
`;

export const inner = StyleSheet.create({
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }
});

export const BodyTitle = styled.View`
    padding-left: 20px;
    padding-top: 5px;
    padding-bottom: 5px;
    height: 30px;
    width: 120px;
    padding-right: 20px;
    position: absolute;
    top: -30px;
    justify-content: center;
    align-items: center;
    background-color: white;
`;

export const Title = styled.Text`
    color: ${colors.$darkGrey};
    font-size: ${font.$size12}px;
`;

// ====== UTILS ======

export const BUTTON_OVERRIDE = {
    root: 'align-self: center; margin: 20px;'
};
