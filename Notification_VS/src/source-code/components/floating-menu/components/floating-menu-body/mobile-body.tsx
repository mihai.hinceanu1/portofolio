import * as utils from './floating-menu-body.utils';
import * as div from './mobile-body.style';
import { IDropdownOption } from '../../../../../shared/interfaces/dropdown';
import { FloatingMenuBodyCfg } from '../../../../interfaces/floating-menu';
import React from 'react';

interface Props {
    pressedTab: IDropdownOption;
    body: FloatingMenuBodyCfg;
    pathName: string;
    addDependency: () => void;
}

interface State {
    annotationPressed: string;
    showcasePressed: string;
}

export class FloatingMenuMobileBody extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            annotationPressed: null,
            showcasePressed: null,
        };
    }

    public render() {
        return (
            <div.Body data-cy='body'>
                {this.getBody()}
            </div.Body>
        );
    }

    private getBody() {
        const { pathName, pressedTab, body, addDependency } = this.props;
        const { annotationPressed, showcasePressed } = this.state;

        return utils.getBodyContent(
            pathName,
            pressedTab.label,
            body,
            (annotation: string) => this.onAnnotationPress(annotation),
            annotationPressed,
            (showcase: string) => this.showcasePressed(showcase),
            showcasePressed,
            addDependency
        );
    }

    public onAnnotationPress(annotation: string) {
        this.setState({
            annotationPressed: annotation,
        });
    }

    public showcasePressed(showcase: string) {
        this.setState({
            showcasePressed: showcase,
        });
    }
}