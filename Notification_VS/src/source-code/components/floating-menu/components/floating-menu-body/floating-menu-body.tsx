import * as div from './floating-menu-body.style';
import * as utils from './floating-menu-body.utils';
import { FloatingMenuBodyCfg, FloatingMenuTabCfg } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    pressedTab: FloatingMenuTabCfg;
    body: FloatingMenuBodyCfg;
    pathName: string;
    addDependency: () => void;
}

interface State {
    annotationPressed: string;
    showcasePressed: string;
}

export class FloatingMenuBody extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            annotationPressed: null,
            showcasePressed: null,
        };
    }

    public render() {
        const { pressedTab } = this.props;

        return (
            <div.FloatingMenuBody data-cy='floating-menu-body'>

                {/** Title */}
                <div.BodyTitle data-cy='body-title'>
                    <div.Title data-cy='title'>
                        {pressedTab.tabText}
                    </div.Title>
                </div.BodyTitle>

                {/** Body */}
                <div.Body data-cy='body'
                    nativeID='body'
                    contentContainerStyle={div.inner.content}>

                    {/** Content */}
                    {
                        this.getBody()
                    }
                </div.Body>
            </div.FloatingMenuBody>
        );
    }

    private getBody() {
        const { pathName, pressedTab, body, addDependency } = this.props;
        const { annotationPressed, showcasePressed } = this.state;

        return utils.getBodyContent(
            pathName,
            pressedTab.tabText,
            body,
            (annotation: string) => this.onAnnotationPress(annotation),
            annotationPressed,
            (showcase: string) => this.showcasePressed(showcase),
            showcasePressed,
            addDependency
        );
    }

    public onAnnotationPress(annotation: string) {
        this.setState({
            annotationPressed: annotation,
        });
    }

    public showcasePressed(showcase: string) {
        this.setState({
            showcasePressed: showcase,
        });
    }
}