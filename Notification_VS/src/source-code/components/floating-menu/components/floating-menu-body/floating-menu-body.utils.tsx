import {
    FLOATING_MENU_TABS,
    FloatingMenuBodyCfg,
    IAnnotation,
    IFlow,
    IPseudocode,
    IRevision,
    IShowcase,
    ISnippet
    } from '../../../../interfaces/floating-menu';
import { Annotations } from '../floating-menu-annotations/annotations';
import { Dependencies } from '../floating-menu-dependencies/dependencies';
import { Flows } from '../floating-menu-flows/flows';
import { Pseudocodes } from '../floating-menu-pseudocode/pseudocodes';
import { Revisions } from '../floating-menu-revisions/revisions';
import { Showcases } from '../floating-menu-showcase/showcases';
import { Snippets } from '../floating-menu-snippet/snippets';
import * as React from 'react';

/** Return body content based on the selected tab */
export const getBodyContent = (
    pathName: string,
    pressedTab: string,
    floatingMenuBodyCfg: FloatingMenuBodyCfg,
    onPress: (annotation: string) => void,
    annotation: string,
    onPressShowcase: (showcase: string) => void,
    showcase: string,
    addDependency: () => void
) => {
    switch (pressedTab) {
        case FLOATING_MENU_TABS.DEPENDENCIES:
            return getDependencies(pathName, addDependency);
        case FLOATING_MENU_TABS.FLOWS:
            return getFlows(floatingMenuBodyCfg.flows);
        case FLOATING_MENU_TABS.SNIPPETS:
            return getSnippets(floatingMenuBodyCfg.snippets);
        case FLOATING_MENU_TABS.PSEUDOCODE:
            return getPseudocode(floatingMenuBodyCfg.pseudocode);
        case FLOATING_MENU_TABS.ANNOTATIONS:
            return getAnnotations(floatingMenuBodyCfg.annotations, onPress, annotation);
        case FLOATING_MENU_TABS.SHOWCASE:
            return getShowcase(floatingMenuBodyCfg.showcases, onPressShowcase, showcase);
        case FLOATING_MENU_TABS.REVISIONS:
            return getRevisions(floatingMenuBodyCfg.revisions);
    }
};

export const getRevisions = (revisions: IRevision[]) => {
    return <Revisions revisions={revisions} />;
};

export const getAnnotations = (cfg: IAnnotation[], onPress: (annotation: string) => void, annotationPressed: string) => {
    return <Annotations annotations={cfg} onPress={onPress} annotationPressed={annotationPressed} />;
};

export const getPseudocode = (cfg: IPseudocode[]) => {
    return <Pseudocodes pseudocodes={cfg} />;
};

export const getSnippets = (cfg: ISnippet[]) => {
    return <Snippets snippets={cfg} />;
};

export const getFlows = (flows: IFlow[]) => {
    return <Flows flows={flows} />;
};

export const getDependencies = (pathName: string, addDependency: () => void) => {
    return <Dependencies pathName={pathName} addDependency={addDependency} />;
};

export const getShowcase = (cfg: IShowcase[], onPress: (showcase: string) => void, showcasePressed: string) => {
    return <Showcases showcases={cfg} onPress={onPress} showcasePressed={showcasePressed} />;
};