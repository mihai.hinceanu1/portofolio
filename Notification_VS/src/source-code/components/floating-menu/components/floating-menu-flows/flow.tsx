import { FLOWS_SMALL } from './_assets/floating-menu-flows.assets';
import * as div from './flow.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { CROSS } from '../../../../../shared/assets/icons';
import { colors } from '../../../../../shared/style/colors';
import { IFlow } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    flow: IFlow;
}

interface State {
    pressed: boolean;
}

export class Flow extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            pressed: false,
        };
    }

    public render() {
        const { flow } = this.props;
        const { pressed } = this.state;

        return (
            <div.Flow data-cy='flow'
                pressed={pressed}
                onPress={() => this.setState({ pressed: !pressed })}>

                {/** Close */}
                {
                    pressed &&
                    <div.CloseButton data-cy='close-button'
                        onPress={() => this.onPressCloseButton()}>

                        {/** X icon */}
                        <Icon path={CROSS} color={colors.$grey} />
                    </div.CloseButton>
                }

                {/** Flow Title */}
                <div.FlowTitle data-cy='flow-title'>

                    {/** Icon */}
                    <Icon path={FLOWS_SMALL} color={colors.$blue} />

                    {/** Text */}
                    <div.FlowTitleText>
                        {flow.title}
                    </div.FlowTitleText>
                </div.FlowTitle>

                {/** Description */}
                <div.Description data-cy='description'>
                    {flow.description}
                </div.Description>

                {/** Snippet Number */}
                <div.SnippetNumber data-cy='snippet-number'>
                    {flow.numberOfSnippets} Snippets
                </div.SnippetNumber>
            </div.Flow>
        );
    }

    private onPressCloseButton() {
        const { pressed } = this.state;

        this.setState({
            pressed: !pressed,
        });
    }
}