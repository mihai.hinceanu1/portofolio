import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface FlowsProps {
    pressed: boolean;
}

export const Flow = styled.TouchableOpacity<FlowsProps>`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    padding: 10px;
    width: 100%;
    ${props => props.pressed && `background-color: ${colors.$lightBlue}`};
`;

export const FlowTitle = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const FlowTitleText = styled.Text`
    font-size: ${font.$size14}px;
    margin-left: 5px;
`;

export const CloseButton = styled.TouchableOpacity`
    position: absolute;
    right: 15px;
    top: 10px;
`;

export const Description = styled.Text`
    margin-top: 5px;
    margin-bottom: 10px;
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;

export const SnippetNumber = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$darkGrey};
`;
