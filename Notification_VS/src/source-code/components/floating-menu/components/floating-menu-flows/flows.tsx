import { Flow } from './flow';
import * as div from './flows.style';
import { Button } from '../../../../../shared/components/vsc-button/button';
import { IFlow } from '../../../../interfaces/floating-menu';
import { flowButtonCfg } from '../../floating-menu.utils';
import { BUTTON_OVERRIDE } from '../floating-menu-body/floating-menu-body.style';
import * as React from 'react';

interface Props {
    flows: IFlow[];
}

interface State {
    pressed: boolean;
}

export class Flows extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            pressed: false,
        };
    }

    public render() {
        const { flows } = this.props;

        return (
            <div.Flows data-cy='flows'>
                {/** Flows */}
                {
                    flows.map((flow, index) =>
                        <Flow key={index} flow={flow} />
                    )
                }

                {/** Add flow */}
                <Button {...flowButtonCfg} overrides={BUTTON_OVERRIDE} />
            </div.Flows>
        );
    }
}