import * as div from './floating-menu-tabs.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { FloatingMenuTabCfg } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    tabs: FloatingMenuTabCfg[];
    callback: (tab: FloatingMenuTabCfg) => void;
    pressedTab: FloatingMenuTabCfg;
}

/**
 * The tabs on the left side of floating menu.
 * They control the body content.
 */
export const FloatingMenuTabs: React.FunctionComponent<Props> = (props: Props) => {
    const { callback, pressedTab, tabs } = props;

    return (
        <div.FloatingMenuTabs data-cy='floating-menu-tabs'>
            {
                !!tabs &&
                tabs.map((tab, index) =>
                    <div.Tab data-cy='tab'
                        pressedTab={pressedTab.tabText}
                        tabText={tab.tabText}
                        key={index}
                        onPress={() => callback(tab)}>

                        {/** Icon */}
                        <Icon path={tab.asset} color={div.getSvgFill(pressedTab.tabText, tab.tabText)} width={40} height={40} />

                        <div.TabText data-cy='tab-text'
                            pressedTab={pressedTab.tabText}
                            tabText={tab.tabText}>
                            {
                                tab.tabText
                            }
                        </div.TabText>
                    </div.Tab>)
            }
        </div.FloatingMenuTabs>
    );
};