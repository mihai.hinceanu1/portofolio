import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface TabProps {
    pressedTab: string;
    tabText: string;
}

export const FloatingMenuTabs = styled.View`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    align-self: flex-start;
    border-right-width: 1px;
    border-color: ${colors.$lightGrey};
`;

export const Tab = styled.TouchableOpacity<TabProps>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 70px;
    width: 70px;
    background-color: ${props => getTabBackground(props.tabText, props.pressedTab)};
    border-style: solid;
    border-bottom-width: 0.5px;
    border-bottom-color: ${colors.$lightGrey};
`;

export const TabText = styled.Text<TabProps>`
    color: ${props => getTabTextColor(props.tabText, props.pressedTab)};
    font-size: ${font.$size10}px;
    text-align: center;
    margin-top: 5px;
`;

// ====== UTILS ======

export const getTabTextColor = (pressedTab: string, tabText: string) => {
    if (pressedTab === tabText) {
        return colors.$white;
    } else {
        return colors.$darkGrey;
    }
};

export const getTabBackground = (pressedTab: string, tabText: string) => {
    if (pressedTab === tabText) {
        return colors.$blue;
    } else {
        return colors.$white;
    }
};

export const getSvgFill = (pressedTab: string, tabText: string) => {
    if (pressedTab === tabText) {
        return colors.$white;
    } else {
        return colors.$blue;
    }
};
