import { Annotation } from './annotation';
import * as div from './annotations.style';
import { IAnnotation } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    annotations: IAnnotation[];
    onPress: (annotation: string) => void;
    annotationPressed: string;
}

interface State {
    annotationClose: boolean;
}

export class Annotations extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            annotationClose: true,
        };
    }

    public render() {
        const { annotations, annotationPressed, onPress } = this.props;
        // const { annotationClose } = this.state;

        return (
            <div.Annotations data-cy='annotations'>
                {
                    annotations.map((annotation, index) =>
                        <Annotation key={index}
                            annotation={annotation}
                            onPress={onPress}
                            annotationPressed={annotationPressed} />
                    )
                }
            </div.Annotations>
        );
    }

    // private onPressCloseButton() {
    //     this.setState({
    //         annotationClose: false,
    //     });
    // }

    // private onPressFloatingMenuAnnotations() {
    //     const { annotations: config, onPress } = this.props;
    //     onPress(config.title);
    //     this.setState({ annotationClose: true });
    // }
}