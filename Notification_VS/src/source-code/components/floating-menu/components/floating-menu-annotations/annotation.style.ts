import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface AnnotationProps {
    pressed: boolean;
}

export const Annotation = styled.TouchableOpacity<AnnotationProps>`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    width: 100%;
    padding: 15px 30px 5px 15px;
    background-color: ${props => props.pressed && colors.$lightBlue};
`;

export const CloseButton = styled.TouchableOpacity`
    position: absolute;
    right: 15px;
    top: 10px;
`;

export const CloseButtonText = styled.Text`
    font-size: ${font.$size12}px;
    color: black;
`;

export const AnnotationTitle = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const AnnotationTitleText = styled.Text`
    font-size: ${font.$size14}px;
    margin-left: 5px;
`;

export const Description = styled.Text`
    margin-top: 5px;
    margin-bottom: 10px;
    color: ${colors.$grey};
    font-size: ${font.$size12}px;
`;

export const LineAndColumn = styled.View`
    display: flex;
    flex-direction: row;
`;

export const Line = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$darkGrey};
    font-weight: 500;
`;

export const Column = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$darkGrey};
    font-weight: 500;
    margin-left: 10px;
`;