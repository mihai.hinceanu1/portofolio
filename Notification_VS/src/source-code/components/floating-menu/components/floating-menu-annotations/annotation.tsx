import { SMALL_ANNOTATION } from './_assets/floating-menu-annotations.assets';
import * as div from './annotation.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { colors } from '../../../../../shared/style/colors';
import { IAnnotation } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    annotation: IAnnotation;
    onPress: (annotation: string) => void;
    annotationPressed: string;
}

interface State {
    annotationClose: boolean;
}

export class Annotation extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            annotationClose: true,
        };
    }

    public render() {
        const { annotation, annotationPressed } = this.props;
        const { annotationClose } = this.state;

        return (
            <div.Annotation data-cy='annotation'
                pressed={annotationPressed === annotation.title && annotationClose}
                onPress={() => this.onPressFloatingMenuAnnotations()}>

                {
                    (annotationPressed === annotation.title && annotationClose) &&
                    <div.CloseButton data-cy='close-button' onPress={() => this.onPressCloseButton()}>
                        <div.CloseButtonText data-cy='close-button-text'>
                            X
                        </div.CloseButtonText>
                    </div.CloseButton>
                }

                {/** Title */}
                <div.AnnotationTitle data-cy='annotation-title'>
                    <Icon path={SMALL_ANNOTATION} color={colors.$blue} />

                    <div.AnnotationTitleText data-cy='annotation-title-text'>
                        {annotation.title}
                    </div.AnnotationTitleText>
                </div.AnnotationTitle>

                {/** Description */}
                <div.Description data-cy='description'>
                    {annotation.description}
                </div.Description>

                {/** Position */}
                <div.LineAndColumn data-cy='line-and-column'>
                    <div.Line data-cy='line'>
                        Ln {annotation.lines[0]} - {annotation.lines[1]}
                    </div.Line>

                    <div.Column data-cy='column'>
                        Col {annotation.column}
                    </div.Column>
                </div.LineAndColumn>

            </div.Annotation>
        );
    }

    private onPressCloseButton() {
        this.setState({
            annotationClose: false,
        });
    }

    private onPressFloatingMenuAnnotations() {
        const { annotation: config, onPress } = this.props;
        onPress(config.title);
        this.setState({ annotationClose: true });
    }
}