import { PSEUDOCODE_SMALL } from './_assets/floating-menu-pseudocode.assets';
import * as div from './pseudocode.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { colors } from '../../../../../shared/style/colors';
import { IPseudocode } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    pseudocode: IPseudocode;
}

interface State { }

export class Pseudocode extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { pseudocode } = this.props;

        return (
            <div.Pseudocode data-cy='pseudocode'>

                {/** Icon */}
                <Icon path={PSEUDOCODE_SMALL} color={colors.$blue} />

                {/** File Name */}
                <div.FileName data-cy='file-name'>
                    {pseudocode.filename}
                </div.FileName>
            </div.Pseudocode>
        );
    }
}