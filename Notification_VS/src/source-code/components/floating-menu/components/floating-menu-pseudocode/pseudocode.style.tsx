import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const Pseudocode = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    padding: 10px 10px 5px 10px;
`;

export const FileName = styled.Text`
    font-size: ${font.$size14}px;
    margin-left: 5px;
`;