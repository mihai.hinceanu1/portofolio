import { Pseudocode } from './pseudocode';
import * as div from './pseudocodes.style';
import { IPseudocode } from '../../../../interfaces/floating-menu';
import React from 'react';

interface Props {
    pseudocodes: IPseudocode[];
}

export const Pseudocodes: React.FunctionComponent<Props> = (props: Props) => {
    const { pseudocodes } = props;

    return (
        <div.Pseudocodes data-cy='pseudocodes'>
            {
                pseudocodes.map((pseudocode, index) =>
                    <Pseudocode key={index} pseudocode={pseudocode} />
                )
            }
        </div.Pseudocodes>
    );
}