import { colors } from '../../../../../shared/style/colors';
import { font } from '../../../../../shared/style/font-sizes';
import styled from 'styled-components/native';

// ====== PROPS ======

interface ShowcaseProps {
    pressed: boolean;
}

// ====== STYLE ======

export const Showcase = styled.TouchableOpacity<ShowcaseProps>`
    display: flex;
    flex-direction: row;
    width: 100%;
    padding: 10px 10px 5px 10px;
    border-bottom-color: ${colors.$lightGrey};
    border-bottom-width: 0.2px;
    border-style: solid;
    ${props => getShowcaseBgrColor(props)};
`;

export const CloseButton = styled.TouchableOpacity`
    position: absolute;
    right: 15px;
    top: 10px;
`;

export const CloseButtonText = styled.Text`
    font-size: ${font.$size12}px;
    color: black;
`;

export const Details = styled.View`
    display: flex;
    flex-direction: column;
`;

export const Image = styled.View`
`;

export const Title = styled.Text`
    font-size: ${font.$size14}px;
    font-weight: 500;
    margin-top: 5px;
`;

export const File = styled.Text`
    font-size: ${font.$size10}px;
    color: ${colors.$grey};
`;

export const ImgButton = styled.TouchableOpacity`
    margin-top: 20px;
    margin-bottom: 15px;
    display: flex;
    flex-direction: row;
`;

export const ImgButtonText = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$blue};
    margin-left: 5px;
`;

// ====== UTILS ======

function getShowcaseBgrColor(props: ShowcaseProps): string {
    if (props.pressed) {
        return `background-color: ${colors.$lightBlue};`;
    }
}