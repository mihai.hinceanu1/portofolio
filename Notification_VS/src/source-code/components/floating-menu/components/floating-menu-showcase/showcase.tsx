import { ADD_IMAGE_BIG } from './_assets/floating-menu-showcase.assets';
import * as div from './showcase.style';
import { Icon } from '../../../../../icons/components/icon/icon';
import { colors } from '../../../../../shared/style/colors';
import { IShowcase } from '../../../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    showcase: IShowcase;
    onPress: (showcase: string) => void;
    showcasePressed: string;
}

interface State {
    showcaseClose: boolean;
}

export class Showcase extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            showcaseClose: false,
        };
    }

    public render() {
        const { showcase, showcasePressed, onPress } = this.props;
        const { showcaseClose, } = this.state;

        return (
            <div.Showcase data-cy='showcase'
            pressed={showcasePressed === showcase.title && showcaseClose}
                onPress={() => { onPress(showcase.title); this.setState({ showcaseClose: true }); }}>

                {
                    (showcasePressed === showcase.title && showcaseClose) &&
                    <div.CloseButton data-cy='close-button'
                    onPress={() => this.onPressCloseButton()}>

                        <div.CloseButtonText data-cy='close-button-text'>
                            X
                        </div.CloseButtonText>
                    </div.CloseButton>
                }

                <div.Details data-cy='details'>
                    <div.Title data-cy='title'>
                        {showcase.title}
                    </div.Title>

                    <div.File data-cy='file'>
                        {showcase.file}
                    </div.File>

                    <div.ImgButton data-cy='img-button'>

                        <Icon path={ADD_IMAGE_BIG} color={colors.$blue} />

                        <div.ImgButtonText data-cy='img-button-text'>
                            + Add Image
                        </div.ImgButtonText>
                    </div.ImgButton>
                </div.Details>
            </div.Showcase>
        );
    }

    private onPressCloseButton() {
        this.setState({
            showcaseClose: false,
        });
    }

}