import { Showcase } from './showcase';
import * as div from './showcases.style';
import { Button } from '../../../../../shared/components/vsc-button/button';
import { IShowcase } from '../../../../interfaces/floating-menu';
import { showcaseButtonCfg } from '../../floating-menu.utils';
import { BUTTON_OVERRIDE } from '../floating-menu-body/floating-menu-body.style';
import React from 'react';

interface Props {
    showcases: IShowcase[];
    onPress: (showcase: string) => void;
    showcasePressed: string;
}

export const Showcases: React.FunctionComponent<Props> = (props: Props) => {
    const { showcases, onPress, showcasePressed } = props;

    return (
        <div.Showcases data-cy='showcases'>
            {/** Showcases */}
            {
                showcases.map((showcase, index) =>
                    <Showcase key={index} showcase={showcase} onPress={onPress} showcasePressed={showcasePressed} />
                )
            }

            {/** Preview button */}
            <Button {...showcaseButtonCfg} overrides={BUTTON_OVERRIDE} />
        </div.Showcases>
    );
};