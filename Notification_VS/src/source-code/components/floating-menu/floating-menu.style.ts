import styled from 'styled-components/native';

// ====== PROPS ======

interface Override {
    override: string;
}

// ====== STYLE ======

export const FloatingMenu = styled.View<Override>`
    display: flex;
    flex-direction: row;
    max-width: 300px;
    /** <!>Works only on web */
    filter: drop-shadow(0px 0px 16px rgba(0,0,0,0.1));
    ${props => props.override}
`;