import { FloatingMenuBody } from './components/floating-menu-body/floating-menu-body';
import { FloatingMenuTabs } from './components/floating-menu-tabs/floating-menu-tabs';
import * as div from './floating-menu.style';
import { FloatingMenuCfg, FloatingMenuTabCfg } from '../../interfaces/floating-menu';
import * as React from 'react';

interface Props {
    floatingMenuCfg: FloatingMenuCfg;
    /**
     * PathName of the package version, which will be sent to all children
     * because for some reason if using URL matching it comes as 'undefined'
     */
    pathName: string;

    /** Override CSS properties */
    overrides?: {
        root?: string;
    };
}

interface State {
    pressedTab: FloatingMenuTabCfg;
}

export class FloatingMenu extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            pressedTab: props.floatingMenuCfg.tabs[0],
        };
    }

    public render() {
        const { floatingMenuCfg, overrides, pathName } = this.props;
        const { pressedTab } = this.state;

        return (
            <div.FloatingMenu data-cy='floating-menu'
                override={!!overrides && overrides.root}>

                {/** Tabs */}
                <FloatingMenuTabs tabs={floatingMenuCfg.tabs}
                    pressedTab={pressedTab}
                    callback={(tab: FloatingMenuTabCfg) => this.onTabPress(tab)} />

                {/** Body */}
                <FloatingMenuBody body={floatingMenuCfg.body}
                    pathName={pathName}
                    pressedTab={pressedTab}
                    addDependency={floatingMenuCfg.addDependency} />
            </div.FloatingMenu>
        );
    }

    private onTabPress(value: FloatingMenuTabCfg) {
        this.setState({
            pressedTab: value,
        });
    }
}
