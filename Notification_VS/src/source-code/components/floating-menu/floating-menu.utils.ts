import * as asset from './components/floating-menu-tabs/_assets/floating-menu-tabs.assets';
import { PREVIEW } from '../../../shared/assets/icons';
import { ButtonType, IButton, IconPosition } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';
import { FLOATING_MENU_TABS, FloatingMenuCfg } from '../../interfaces/floating-menu';

export const floatingMenuConfig: FloatingMenuCfg = {
    tabs: [
        {
            asset: asset.DEPENDENCIES,
            tabText: FLOATING_MENU_TABS.DEPENDENCIES,
        },
        {
            asset: asset.SNIPPETS,
            tabText: FLOATING_MENU_TABS.SNIPPETS,

        },
        {
            asset: asset.CODE_FLOWS,
            tabText: FLOATING_MENU_TABS.FLOWS,

        },
        {
            asset: asset.REVISIONS,
            tabText: FLOATING_MENU_TABS.REVISIONS,

        },
        {
            asset: asset.PSEUDOCODE,
            tabText: FLOATING_MENU_TABS.PSEUDOCODE,
        },
        {
            asset: asset.ANNOTATIONS,
            tabText: FLOATING_MENU_TABS.ANNOTATIONS,
        },
        {
            asset: asset.SHOWCASE,
            tabText: FLOATING_MENU_TABS.SHOWCASE,
        }

    ],
    body: {
        dependencies: [],
        snippets: [
            {
                title: 'First Snippet',
                description: 'Prima descriere pentru primul code snippet gasit',
                codeLines: [3, 15],
            },
            {
                title: 'First Snippet',
                description: 'Prima descriere pentru primul code snippet gasit',
                codeLines: [3, 15],
            },
            {
                title: 'First Snippet',
                description: 'Prima descriere pentru primul code snippet gasit',
                codeLines: [3, 15],
            }, {
                title: 'First Snippet',
                description: 'Prima descriere pentru primul code snippet gasit',
                codeLines: [3, 15],
            }
        ],
        flows: [
            {
                title: 'Primul flow',
                description: 'Prima descriere pentru primul flow gasit',
                numberOfSnippets: 10,
            },
            {
                title: 'Primul flow',
                description: 'Prima descriere pentru primul flow gasit',
                numberOfSnippets: 10,
            },
            {
                title: 'Primul flow',
                description: 'Prima descriere pentru primul flow gasit',
                numberOfSnippets: 10,
            },
            {
                title: 'Primul flow',
                description: 'Prima descriere pentru primul flow gasit',
                numberOfSnippets: 10,
            }
        ],
        revisions: [
            {
                title: 'Primul revision',
                description: 'Prima descriere pentru primul revision gasit'
            },
            {
                title: 'Primul revision',
                description: 'Prima descriere pentru primul revision gasit'
            },
            {
                title: 'Primul revision',
                description: 'Prima descriere pentru primul revision gasit'
            },
            {
                title: 'Primul revision',
                description: 'Prima descriere pentru primul revision gasit'
            },
            {
                title: 'Primul revision',
                description: 'Prima descriere pentru primul revision gasit'
            },
        ],
        pseudocode: [
            {
                filename: 'main1.xml',
            },
            {
                filename: 'main2.xml',
            },
            {
                filename: 'main2.xml',
            }
        ],
        annotations: [
            {
                title: 'Title 1',
                description: 'prima descriere pentru prima annotare',
                lines: [1, 12],
                column: 12,
            },
            {
                title: 'Title 2',
                description: 'prima descriere pentru prima annotare',
                lines: [1, 12],
                column: 12,
            },
            {
                title: 'Title 3',
                description: 'prima descriere pentru prima annotare',
                lines: [1, 12],
                column: 12,
            }, {
                title: 'Title 4',
                description: 'prima descriere pentru prima annotare',
                lines: [1, 12],
                column: 12,
            }
        ],
        showcases: [
            {
                title: 'Primul showcase',
                file: 'main.lua'
            },
            {
                title: 'Al doilea showcase',
                file: 'main.lua'
            },
            {
                title: 'Al treilea showcase',
                file: 'main.lua'
            }
        ]
    }
};

export const showcaseButtonCfg: IButton = {
    type: ButtonType.GHOST,
    icon: {
        position: IconPosition.LEFT,
        path: PREVIEW
    },
    text: 'Preview',
    color: colors.$blue,
    callback: () => console.log('Button Pressed')
};

export function getDependenciesButtonCfg(callback: () => void): IButton {
    let dependenciesButtonCfg: IButton = {
        type: ButtonType.ROUNDED,
        color: colors.$blue,
        text: 'Add Dependency',
        callback,
    };

    return dependenciesButtonCfg;
}

export const flowButtonCfg: IButton = {
    type: ButtonType.ROUNDED,
    text: 'Add Flow',
    callback: () => console.log('Add Flow'),
    color: colors.$blue
};

export const revisionButtonCfg: IButton = {
    type: ButtonType.ROUNDED,
    text: 'Add Revision',
    callback: () => console.log('Add Revision'),
    color: colors.$blue
};
