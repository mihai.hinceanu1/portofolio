import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { FloatingMenu } from '../floating-menu';
import { floatingMenuConfig } from '../floating-menu.utils';
import * as React from 'react';

export class FloatingMenuPage extends ScrollableDocPage<{}> {

    constructor(props: ScrollableDocPageProps) {
        super(props);

        this.state = {
            ...this.state,
        };
    }

    public renderDemoPage() {
        let { width } = this.state;

        return (
            <>
                {/* Overview */}
                <div.Overview width={width}>
                    <div.OverviewTitle>
                        Floating Menu
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Floating menu was created in order to let the user
                        handle his Snippets, Annotations, Pseudocode, Flows,
                        Showcases and Dependencies that take part in his lesson.
                    </div.OverviewDescription>
                </div.Overview>

                {/** Standard */}
                <div.Demo data-cy='standard' width={width}>
                    <div.DemoArea>
                        <div.DemoTitle>
                            Standard
                        </div.DemoTitle>

                        <FloatingMenu pathName='' floatingMenuCfg={floatingMenuConfig} />
                    </div.DemoArea>

                    <CodeEditor {...codeSampleConfig} srcCode={''} />
                </div.Demo>
            </>
        );
    }
}