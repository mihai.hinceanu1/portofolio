import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { getPackageElementsColor, MILESTONES } from '../../services/packages-list.utils';
import Color from 'color-js/color';
import styled from 'styled-components/native';

// ====== PROPS ======
export const SPACING_WEB = 20;
export const SPACING_NARROW = 10;

interface BarItemProps {
    color: string;
    disabled: boolean;
}

interface AdaptiveProps {
    isNarrow: boolean;
}

interface MilestoneProps {
    milestone: MILESTONES;
    color: string;
    disabled: boolean;
}

// ====== STYLE ======

export const StatsBar = styled.View<AdaptiveProps>`
    flex-direction: row;
    align-items: center;
    min-width: 280px;
    justify-content: space-between;
    flex-wrap: wrap;
`;

export const Stat = styled.View<AdaptiveProps>`
    align-items: center;
    justify-content: center;
    flex-direction: row;
`;

export const Count = styled.Text<BarItemProps>`
    margin-left: 5px;
    font-weight: 600;
    color: ${props => getStatsBarColor(props)};
`;

export const Milestone = styled.Text<MilestoneProps & AdaptiveProps>`
    text-transform: uppercase;
    background-color: ${props => getBackgroundColor(props)};
    color: ${props => getPackageElementsColor(props.milestone, props.disabled, props.color)};
    ${props => getMilestoneAdaptiveStyle(props)};
`;

export const Tech = styled.View`
    margin-right: 5px;
    margin-left: 5px;
    min-width: 0;
    min-height: 0;
    overflow: hidden;
`;

export const CommentIcon = styled.View`
    margin-right: 5px;
    /* min-width: 0;
    min-height: 0;
    overflow: hidden; */
`;

export const NewComments = styled.Text`
    top: -3px;
    right: -3px;
    width: 10px;
    height: 10px;
    line-height: 10px;
    text-align: center;
    position: absolute;
    border-radius: 5px;
    align-items: center;
    justify-content: center;
    color: ${colors.$white};
    font-size: ${font.$size8}px;
    background-color: ${colors.$red};
    min-width: 0;
    min-height: 0;
    overflow: hidden;
`;

// ====== UTILS ======

function getStatsBarColor(props: BarItemProps) {
    if (props.disabled) {
        return colors.$grey;
    }

    return props.color ? props.color : 'black';
}

function getBackgroundColor(props: MilestoneProps) {
    let color = Color(getPackageElementsColor(props.milestone, props.disabled, props.color));

    if (props.disabled) {
        color = Color(colors.$grey);
    }

    /**
     * Get each rgb value.
     * color.getRed() etc... returns value between 0 and 1.
     * Multiply by 255 to get the correct value for rgba()
     */
    const red = 255 * color.getRed();
    const green = 255 * color.getGreen();
    const blue = 255 * color.getBlue();

    const lighter = `rgba(${red}, ${green}, ${blue}, 0.1)`;

    return lighter;
}

let getMilestoneAdaptiveStyle = (props: AdaptiveProps): string => {
    let style = '';

    if (props.isNarrow) {
        style = `
            font-weight: 500;
            padding: 0px 10px;
        `;
    } else {
        style = `
            font-weight: 600;
            padding: 2px 20px;
        `;
    }

    return style;
};
