import * as div from './stats-bar.style';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { IStatsBar } from '../../../shared/interfaces/stats-bar';
import { colors } from '../../../shared/style/colors';
import { getPackageElementsColor, MILESTONES } from '../../services/packages-list.utils';
import * as React from 'react';

interface Props {
    config: IStatsBar;
    isNarrow: boolean;
}

/**
 * Presentation component used to display stats like
 * awards, reactions, milestones, comments, lines of code,
 * and technology stack for a tutorial.
 */
export const StatsBar: React.FunctionComponent<Props> = (props: Props) => {
    const { isNarrow, config } = props;
    const {
        awards,
        color,
        reactions,
        comments,
        codeLines,
        techStack,
        disabled,
        newComments,
        milestone,
    } = config;

    return (
        <div.StatsBar data-cy='stats-bar' isNarrow={isNarrow}>

            {/** Awards */}
            <div.Stat data-cy='awards' isNarrow={isNarrow}>
                {/** Award Icon */}
                <Icon path={icons.AWARD} color={getPackageElementsColor(milestone, disabled, color)} />

                {/** Count */}
                <div.Count data-cy='awards-count'
                    color={color}
                    disabled={disabled}>
                    {awards}
                </div.Count>
            </div.Stat>

            {/** Reactions */}
            <div.Stat data-cy='reactions' isNarrow={isNarrow}>
                {/** Reactions Icon */}
                <Icon path={icons.HEART_GHOST} color={getPackageElementsColor(milestone, disabled, color)} />

                {/** Count */}
                <div.Count data-cy='reactions-count'
                    color={color}
                    disabled={disabled}>
                    {reactions}
                </div.Count>
            </div.Stat>

            {/** Milestone */}
            {
                !!getMilestoneLabel(milestone, isNarrow) &&
                <div.Milestone data-cy='milestone'
                    milestone={milestone}
                    isNarrow={isNarrow}
                    color={color}
                    disabled={disabled}>
                    {getMilestoneLabel(milestone, isNarrow)}
                </div.Milestone>
            }

            {/** Comments */}
            <div.Stat data-cy='comments' isNarrow={isNarrow}>
                <div.CommentIcon data-cy='comment-icon'>
                    {/** Comment icon */}
                    <Icon path={icons.COMMENTS} color={getPackageElementsColor(milestone, disabled, color)} />

                    {/** New Comments */}
                    {
                        newComments > 0 &&
                        <div.NewComments data-cy='new-comments'>
                            {newComments}
                        </div.NewComments>
                    }
                </div.CommentIcon>

                {/** Count */}
                <div.Count data-cy='comments-count'
                    color={color}
                    disabled={disabled}>
                    {getCommentsCount(isNarrow, comments)}
                </div.Count>
            </div.Stat>

            {/** Code Lines */}
            <div.Stat data-cy='code-lines' isNarrow={isNarrow}>
                {/** Code icon */}
                <Icon path={icons.CODE} color={getPackageElementsColor(milestone, disabled, color)} />

                <div.Count data-cy='code-lines-count'
                    color={color}
                    disabled={disabled}>
                    {codeLines}
                </div.Count>
            </div.Stat>

            {/** Tech Stack */}
            <div.Stat data-cy='tech-stack' isNarrow={isNarrow}>
                {
                    !!techStack.length &&
                    techStack.map(tech =>
                        <div.Tech data-cy='tech' key={tech}>
                            {renderTechStackIcon(tech, disabled)}
                        </div.Tech>
                    )
                }
            </div.Stat>
        </div.StatsBar>
    );
};

// ===== UTILS =====

function getMilestoneLabel(milestone: MILESTONES, isNarrow: boolean): string {
    let milestoneLabel: string = '';

    if (isNarrow) {
        switch (milestone) {
            case (MILESTONES.Bestseller): { milestoneLabel = 'BS'; break; }
            case (MILESTONES.Editors_Choice): { milestoneLabel = 'EC'; break; }
            case (MILESTONES.Highest_Rated): { milestoneLabel = 'HR'; break; }
            default: milestoneLabel = '';
        }
    } else {
        if (milestone !== MILESTONES.Package) {
            milestoneLabel = milestone;
        }
    }

    return milestoneLabel;
}

function getCommentsCount(isNarrow: boolean, comments: number): string {
    let commCount: string = comments.toString();

    if (!isNarrow) {
        commCount = `${comments} comments`;
    }

    return commCount;
}

function getTechIconPath(tech: string) {
    switch (tech) {
        case 'angular': {
            return {
                svgPath: icons.ANGULAR,
                color: colors.$red,
            };
        }

        case 'react': {
            return {
                svgPath: icons.REACT,
                color: colors.$blue,
            };
        }

        default:
            return;
    }
}

function renderTechStackIcon(tech: string, disabled?: boolean): JSX.Element {
    let icon = getTechIconPath(tech);
    const color = disabled ? colors.$grey : icon.color;

    return <Icon path={icon.svgPath} width={25} height={25} color={color} />;
}
