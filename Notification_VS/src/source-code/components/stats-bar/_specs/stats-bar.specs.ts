/// <reference types="cypress" />

describe('StatsBar Tests', () => {
    it('Successfully Loads The Page', () => {
        cy.visit('http://localhost:3000/components-catalog/source-code/stats-bar');

        cy.get('[data-cy=stats-bar-page')
            .should('have.length', 1);
    });

    it('Default is rendered', () => {
        getEl('default', 'stats-bar')
            .should('have.length', 1);
    });

    it('Default is rendering awards', () => {
        getEl('default', 'awards-count')
            .should('have.length', 1);

        getNestedElByTag('default', 'awards', 'path')
            .should('have.length', 1);
    });

    it('Default is rendering reactions', () => {
        getEl('default', 'reactions-count')
            .should('have.length', 1);

        getNestedElByTag('default', 'reactions', 'path')
            .should('have.length', 1);
    });

    it('Default is rendering comments', () => {
        getEl('default', 'comments-count')
            .should('have.length', 1);

        getNestedElByTag('default', 'comments', 'path')
            .should('have.length', 1);
    });

    it('Default is rendering code lines', () => {
        getEl('default', 'code-lines-count')
            .should('have.length', 1);

        getNestedElByTag('default', 'code-lines', 'path')
            .should('have.length', 1);
    });

    it('Techstack icons are rendered', () => {
        getEl('with-techstack', 'tech')
            .should('have.length.greaterThan', 1);
    });

    it('Given 2 techstack, 2 icons are rendered', () => {
        getEl('with-techstack', 'tech')
            .should('have.length', 2);
    });

    it(`Given EDITOR'S CHOICE milestone, it appears properly and the elements have a specific color`, () => {
        let ecBlue = 'rgb(13,142,241)';
        getEl('editors-choice', 'milestone')
            .should('have.text', `EDITOR'S CHOICE`);

        getElByTag('editors-choice', 'path').should('have.attr', 'fill', ecBlue);
    });

    it(`Given HIGHEST RATED milestone, it appears properly and the elements have a specific color`, () => {
        let hrGreen = '#00c3a7';
        getEl('highest-rated', 'milestone')
            .should('have.text', `HIGHEST RATED`);

        getElByTag('highest-rated', 'path').should('have.attr', 'fill', hrGreen);
    });

    it(`Given BESTSELLER milestone, it appears properly and the elements have a specific color`, () => {
        let bestsellerBlack = '#000000';
        getEl('bestseller', 'milestone')
            .should('have.text', `BESTSELLER`);

        getElByTag('bestseller', 'path').should('have.attr', 'fill', bestsellerBlack);
    });

    it(`Given BESTSELLER milestone and a custom color, the color has priority`, () => {
        const customColor = '#ea4335';

        getEl('milestone-color', 'milestone')
            .should('have.text', `BESTSELLER`);

        getElByTag('milestone-color', 'path').should('have.attr', 'fill', customColor);
    });

    it('Given custom color, displays icons with that color', () => {
        const color = '#ea4335';

        getNestedElByTag('color', 'awards', 'path')
            .should('have.attr', 'fill', color);

        getNestedElByTag('color', 'comments', 'path')
            .should('have.attr', 'fill', color);

        getNestedElByTag('color', 'code-lines', 'path')
            .should('have.attr', 'fill', color);

        getNestedElByTag('color', 'reactions', 'path')
            .should('have.attr', 'fill', color);
    });

    it('Disabled StatsBar is greyed out', () => {
        const color = 'rgb(196,196,196)';
        const spacedColor = 'rgb(196, 196, 196)';

        getNestedElByTag('disabled', 'awards', 'path')
            .should('have.attr', 'fill', color);

        getNestedElByTag('disabled', 'comments', 'path')
            .should('have.attr', 'fill', color);

        getNestedElByTag('disabled', 'code-lines', 'path')
            .should('have.attr', 'fill', color);

        getNestedElByTag('disabled', 'reactions', 'path')
            .should('have.attr', 'fill', color);

        getEl('disabled', 'awards-count')
            .should('have.css', 'color', spacedColor);

        getEl('disabled', 'comments-count')
            .should('have.css', 'color', spacedColor);

        getEl('disabled', 'code-lines-count')
            .should('have.css', 'color', spacedColor);

        getEl('disabled', 'reactions-count')
            .should('have.css', 'color', spacedColor);
    });

    it('Given "newcomments" prop, render new comments highlight', () => {
        getEl('new-comments-highlight', 'new-comments')
            .should('have.length', 1);
    });
});

// ===== UTILS =====

function getEl(wrapper, element) {
    return cy.get(`[data-cy=${wrapper}] [data-cy=${element}]`);
}

function getElByTag(wrapper, tag) {
    return cy.get(`[data-cy=${wrapper}] ${tag}`);
}

function getNestedElByTag(useCase, wrapper, tag) {
    return cy.get(`[data-cy=${useCase}] [data-cy=${wrapper}] ${tag}`);
}