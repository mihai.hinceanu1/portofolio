import { IStatsBar } from '../../../../shared/interfaces/stats-bar';
import { colors } from '../../../../shared/style/colors';
import { MILESTONES } from '../../../services/packages-list.utils';

export const DEFAULT: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Package,
    comments: 5,
    codeLines: 20,
    techStack: [],
};

/**
 * <!> Careful when changing this, the test relies
 * on techStack having exactly 2 technologies.
 */
export const WITH_TECHSTACK: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Package,
    comments: 5,
    codeLines: 20,
    techStack: ['angular', 'react'],
};

export const EDITORS_CHOICE: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Editors_Choice,
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    techStack: [],
};

export const HIGHEST_RATED: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Highest_Rated,
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    techStack: [],
};

export const BESTSELLER: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Bestseller,
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    techStack: [],
};

export const MILESTONE_CUSTOM_COLOR: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Bestseller,
    color: colors.$red,
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    techStack: [],
};

export const COLOR: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Package,
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    // set custom color
    color: colors.$red,
    techStack: ['angular', 'react'],
};

export const DISABLED: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Package,
    comments: 5,
    codeLines: 20,
    disabled: true,
    isEditorChoice: true,
    techStack: ['react'],
};

export const NEW_COMMENTS: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: MILESTONES.Package,
    comments: 5,
    newComments: 5,
    codeLines: 20,
    techStack: ['react'],
};
