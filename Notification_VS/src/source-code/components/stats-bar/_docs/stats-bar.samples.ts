import { colors } from '../../../../shared/style/colors';
import { MILESTONES } from '../../../services/packages-list.utils';

export const DEFAULT = `
const DEFAULT: IStatsBar = {
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    techStack: [],
};

<StatsBar config={DEFAULT} />
`;

export const WITH_TECHSTACK = `
const WITH_TECHSTACK_CODE: IStatsBar = {
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    techStack: ['angular', 'react'],
};

<StatsBar config={WITH_TECHSTACK_CODE} />
`;

export const EDITORS_CHOICE = `
const EDITORS_CHOICE: IStatsBar = {
    // Set the milestone
    milestone: ${MILESTONES.Editors_Choice},
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    techStack: [],
};

<StatsBar config={EDITORS_CHOICE} />
`;

export const HIGHEST_RATED = `
const HIGHEST_RATED: IStatsBar = {
    // Set the milestone
    milestone: ${MILESTONES.Editors_Choice},
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    techStack: [],
};

<StatsBar config={HIGHEST_RATED} />
`;

export const BESTSELLER = `
const BESTSELLER: IStatsBar = {
    // Set the milestone
    milestone: ${MILESTONES.Editors_Choice},
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    techStack: [],
};

<StatsBar config={BESTSELLER} />
`;

export const MILESTONE_CUSTOM_COLOR = `
const MILESTONE_CUSTOM_COLOR: IStatsBar = {
    awards: 2,
    reactions: 10,
    milestone: ${MILESTONES.Bestseller},
    color: ${colors.$red},
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    techStack: [],
    deprecated: false,
};

<StatsBar config={MILESTONE_CUSTOM_COLOR} />
`;

export const COLOR = `
const COLOR: IStatsBar = {
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    isEditorChoice: true,
    // set custom color
    color: '#ea445e',
    techStack: ['angular', 'react'],
};

<StatsBar config={COLOR} />
`;

export const DISABLED = `
const DISABLED: IStatsBar = {
    awards: 2,
    reactions: 10,
    comments: 5,
    codeLines: 20,
    // set disabled to true to grey out
    disabled: true,
    isEditorChoice: true,
    techStack: ['react'],
};

<StatsBar config={DISABLED} />
`;

export const NEW_COMMENTS = `
const NEW_COMMENTS: IStatsBar = {
    awards: 2,
    reactions: 10,
    comments: 5,
    hasNewComments: true,
    codeLines: 20,
    techStack: ['react'],
};

<StatsBar config={NEW_COMMENTS} />
`;