import * as samples from './stats-bar.samples';
import * as utils from './stats-bar.utils';
import { codeSampleConfig } from '../../../../components-catalog/constants/code-sample-config.const';
import * as div from '../../../../components-catalog/pages/demo-pages.shared-style';
import { ScrollableDocPage, ScrollableDocPageProps } from '../../../../components-catalog/pages/scrollable-doc-page/scrollable-doc-page';
import { CodeEditor } from '../../../../shared/components/code-editor/code-editor';
import { StatsBar } from '../stats-bar';
import * as React from 'react';

interface StatsBarState {}

export class StatsBarPage extends ScrollableDocPage<StatsBarState> {

    constructor(props: ScrollableDocPageProps) {
        super(props);
    }

    public renderDemoPage() {
        const { width } = this.state;

        return (
            <>
                {/** Overview */}
                <div.Overview width={width} data-cy='stats-bar-page'>
                    <div.OverviewTitle>
                        Stats Bar
                    </div.OverviewTitle>

                    <div.OverviewDescription>
                        Represents different statistics about lesson or package.
                        Mainly, it has the number of awards, number of comments,
                        number of reactions, number of lines of code, technology stack.
                        Also, it may have a badge representing one of the milestones a lesson/package can achieve.
                    </div.OverviewDescription>

                    {/** Default */}
                    <div.Demo data-cy='default' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Default Stats Bar
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.DEFAULT} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.DEFAULT} />
                    </div.Demo>

                    {/** With Tech Stack */}
                    <div.Demo data-cy='with-techstack' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Stats Bar With TechStack
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.WITH_TECHSTACK} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.WITH_TECHSTACK} />
                    </div.Demo>

                    {/** Editor's choice */}
                    <div.Demo data-cy='editors-choice' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Editor's Choice
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.EDITORS_CHOICE} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.EDITORS_CHOICE} />
                    </div.Demo>

                    {/** Highest rated */}
                    <div.Demo data-cy='highest-rated' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Highest rated
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.HIGHEST_RATED} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.HIGHEST_RATED} />
                    </div.Demo>

                    {/** Bestseller */}
                    <div.Demo data-cy='bestseller' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Bestseller
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.BESTSELLER} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.BESTSELLER} />
                    </div.Demo>

                    {/** Milestone set and custom color */}
                    <div.Demo data-cy='milestone-color' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Milestone set and custom color
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.MILESTONE_CUSTOM_COLOR} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.MILESTONE_CUSTOM_COLOR} />
                    </div.Demo>

                    {/** Custom color */}
                    <div.Demo data-cy='color' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Custom Color
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.COLOR} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.COLOR} />
                    </div.Demo>

                    {/** Disabled */}
                    <div.Demo data-cy='disabled' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                Disabled
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.DISABLED} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.DISABLED} />
                    </div.Demo>

                    {/** New comments */}
                    <div.Demo data-cy='new-comments-highlight' width={width} wide={true}>
                        <div.DemoArea>
                            <div.DemoTitle>
                                New comments highlight
                            </div.DemoTitle>

                            <StatsBar isNarrow={false} config={utils.NEW_COMMENTS} />
                        </div.DemoArea>

                        <CodeEditor {...codeSampleConfig}
                            srcCode={samples.NEW_COMMENTS} />
                    </div.Demo>
                </div.Overview>
            </>
        );
    }
}