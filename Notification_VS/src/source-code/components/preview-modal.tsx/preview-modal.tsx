import * as div from './preview-modal.style';
import * as overlay from '../../../shared/style/overlay.style';
import * as scroll from '../../../shared/style/scroll.style';
import { CodeVersionCardBodyCfg } from '../../interfaces/code-version-card';
import { PackageVersionPreview } from '../package-version-preview/package-version-preview';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
    callback: () => void;
    codeVersionCardBody: CodeVersionCardBodyCfg;
}

interface State {
    width: number;
}

export class PreviewModal extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { codeVersionCardBody, callback } = this.props;
        let { width } = this.state;

        return (
            <div.PreviewModal onLayout={e => this.updateWidth(e)}>
                {/* Hover Menus */}
                <LinearGradient colors={overlay.OVERLAY_GRADIENT}
                    style={overlay.style.linearGradient}>

                    <scroll.ScrollView data-cy='scroll-view'
                        contentContainerStyle={scroll.inner.content}>
                        <scroll.Center width={width}>
                            <PackageVersionPreview codeVersionCardBody={codeVersionCardBody} callback={callback} />

                        </scroll.Center>
                    </scroll.ScrollView>

                </LinearGradient>
            </div.PreviewModal>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }
}
