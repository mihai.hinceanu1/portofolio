import * as div from './update-package-version-form.style';
import { NEW_VERSION } from '../../../shared/assets/icons';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { UpdatePackageVersion } from '../../interfaces/package-version';
import { editPackageVersion } from '../../services/package-version.service';
import * as React from 'react';

interface Props {
    versionId: string;
    title: string;
    description: string;
    callback: () => void;
}

interface State {
    name: string;
    description: string;
    validationError: string;
}

export class UpdatePackageVersionForm extends React.Component<Props, State> {

    constructor(props: Props) {

        super(props);

        this.state = {
            name: props.title,
            description: props.description,
            validationError: '',
        };
    }

    public render() {
        const { name, description, validationError } = this.state;

        return (
            <div.UpdatePackageVersionForm data-cy='update-package-version-form'>
                <div.Header>
                    {
                        div.getFormIcon(NEW_VERSION)
                    }

                    <div.FormTitle>
                        EDIT VERSION
                    </div.FormTitle>

                </div.Header>

                <div.Body data-cy='update-version-form-body'>
                    <Input data-cy='version-name-input'
                        placeholder={'Version name...'}
                        label='Name'
                        error={validationError}
                        value={name}
                        overrides={{ root: 'width: 100%; margin: 0; padding: 0px;' }}
                        onChangeText={(text: string) => this.onChangeName(text)} />

                    <div.DescriptionWrapper>
                        <div.DescriptionLabel>
                            Description
                        </div.DescriptionLabel>

                        <div.Description data-cy='description-input'
                            underlineColorAndroid={'none'}
                            placeholder='Description...'
                            placeholderTextColor='#c4c4c4'
                            numberOfLines={10}
                            value={description}
                            onChangeText={(text: string) => this.onChangeDescription(text)}
                            multiline={true}
                        />

                    </div.DescriptionWrapper>
                </div.Body>

                <div.Footer>
                    <Button
                        overrides={{ root: 'margin: 0px 15px 0px 0px;' }}
                        config={{ text: 'Cancel', onPress: () => this.cancelButton() }} />

                    <Button
                        overrides={{ root: 'margin: 0;' }}
                        config={{ text: 'Submit', onPress: () => this.updatePackage() }} />
                </div.Footer>
            </div.UpdatePackageVersionForm>
        );
    }

    private onChangeName(text: string) {
        this.setState({
            name: text,
        });
    }

    private onChangeDescription(text: string) {
        this.setState({
            description: text,
        });
    }

    private cancelButton() {
        let { callback } = this.props;
        !!callback && callback();
    }

    private updatePackage() {
        let { callback, versionId } = this.props;
        let { name, description } = this.state;

        let configToSend: UpdatePackageVersion = {
            name: name,
            description: description,
            versionId,
        };

        if (name.length < 5) {
            this.setState({
                validationError: 'Name is too short.',
            });
        } else {
            this.setState({
                validationError: '',
            });
            editPackageVersion(configToSend);
            callback && callback();
        }
    }

}