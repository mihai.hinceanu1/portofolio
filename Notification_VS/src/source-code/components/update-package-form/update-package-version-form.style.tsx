import { Icon } from '../../../icons/components/icon/icon';
import { pathBoundingBox } from '../../../shared/services/raphael.utils';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import * as React from 'react';
import { Platform } from 'react-native';
import styled from 'styled-components/native';
// import Svg, { Path } from 'react-native-svg';

const isMobile = Platform.OS !== 'web';

export const UpdatePackageVersionForm = styled.View`
     width: 400px;
    height: 450px;
    display: flex;
    flex-direction: column;
    align-self: center;
    ${!isMobile && 'margin-top: 200px;'}
`;

export const Body = styled.View`
    padding: 40px;
    height: 320px;
    width: 100%;
    display: flex;
    flex-direction: column;
    background-color: white;

`;

export const Header = styled.View`
    background-color: ${colors.$blue};
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 80px;
    width: 100%;
`;

export const FormTitle = styled.Text`
    font-size: 28px;
    color: white;
    font-weight: 600;
`;

export const Footer = styled.View`
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    background-color: white;
    height: 15%;
    padding-right: 40px;
`;

export const Description = styled.TextInput`
    width: 100%;
    height: 125px;
    outline-color: none;
    padding: 5px;
    border-color: ${colors.$grey};
    border-width: 1px;
    border-style: solid;
`;

export const DescriptionWrapper = styled.View`
    margin: 20px 0px 0px 0px;
    justify-content: flex-start;
    align-items: flex-start;
`;

export const DescriptionLabel = styled.Text`
    font-size: ${font.$size12}px;
    color: ${colors.$labelColor};
    margin-bottom: 3px;
`;

export const CloseIconButton = styled.TouchableOpacity`
    position: absolute;
    right: 15px;
    top: 10px;
`;

export const CloseIconText = styled.Text`
    font-size: ${font.$size18}px;
    color: ${colors.$darkGrey};
    font-weight: 600;
`;

// ====== UTILS ======

export const getFormIcon = (svgPath: string) => {
    let boundingBox = pathBoundingBox(svgPath);
    let { height, width } = boundingBox;

    return (
        <Icon path={svgPath} height={height + 2} width={width+1} color={colors.$white}/>
    );
};