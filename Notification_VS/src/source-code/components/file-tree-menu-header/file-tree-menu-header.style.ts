import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const FileTreeMenuHeader = styled.View`
    height: 5%;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding-left: 20px;
    padding-right: 5px;
    border-bottom-color: ${colors.$lightGrey};
    border-bottom-width: 0.5px;
    border-bottom-style: solid;
`;

export const FileTreeMenuHeaderTitle = styled.Text`
    font-weight: 500;
    font-size: ${font.$size16};
`;

export const FileTreeMenuHeaderIcons = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 60px;
    justify-content: space-between;
`;