import * as div from './file-tree-menu-header.style';
import { Icon } from '../../../icons/components/icon/icon';
import { COLLAPSE, NEW_FILE, NEW_FOLDER } from '../../../shared/assets/icons';
import { SourceFileTreeLevel } from '../../interfaces/source-file-tree';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';

interface Props {
    currentFolder: SourceFileTreeLevel;
    openAddSrcFileModal: () => void;
}

interface State { }

export class FileTreeMenuHeader extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { openAddSrcFileModal } = this.props;

        return (
            <div.FileTreeMenuHeader >
                <div.FileTreeMenuHeaderTitle>
                    Root
                </div.FileTreeMenuHeaderTitle>

                <div.FileTreeMenuHeaderIcons>

                    <TouchableOpacity onPress={() => openAddSrcFileModal()}>
                        <Icon path={NEW_FILE} color={'black'} />
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Icon path={NEW_FOLDER} color={'black'} />
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Icon path={COLLAPSE} color={'black'} />
                    </TouchableOpacity>

                </div.FileTreeMenuHeaderIcons>
            </div.FileTreeMenuHeader>
        );
    }
}