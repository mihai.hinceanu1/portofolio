import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface FileProps {
    pressed?: boolean;
    width?: number;
}

export const CodeEditorHeader = styled.View<FileProps>`
    display: flex;
    flex-direction: row;
    width: 100%;
    height: 35px;
`;

export const Explorer = styled.View`
    display: flex;
    flex-direction: row;
    width: 20%;
    height: 100%;
    align-items: center;
    font-family: 'Open Sans';
    padding-left: 25px;
    background-color: #F2F4F5;

`;

export const ExplorerText = styled.Text`
    font-family: 'Open Sans';
    font-size: 13.5px;
    font-weight: 600;
    color: ${colors.$black};
`;

export const FilesDisplay = styled.View<FileProps>`
    height: 100%;
    width: ${props => props.width};
    padding-right: 30px;
    background-color: #F2F4F5;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const SelectedFile = styled.TouchableOpacity<FileProps>`
    align-items: center;
    display: flex;
    flex-direction: row;
    padding-left: 20px;
    padding-right: 15px;
    min-width: 120px;
    background-color: white;
    ${props => !props.pressed && 'background-color: rgb(240,240,240)'}
    ${props => getSelectedFileBorder(props.pressed)};
    margin-right: 2px;
`;

export const CloseFileWrapper = styled.TouchableOpacity``;

export const CloseFileIcon = styled.Text`
    font-size:${font.$size16};
    font-weight: 500;
    margin-left: 10px;
`;

export const SelectedFiles = styled.View`
    display: flex;
    flex-direction: row;
    max-width: 600px;
    overflow: hidden;
`;

export const SelectedFileText = styled.Text`
    font-size: 13.5px;
    color: ${colors.$black};
    margin-left: 5px;
`;

export const Preview = styled.TouchableOpacity`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
`;

export const PreviewText = styled.Text`
    font-size: ${font.$size16}px;
    font-weight: 500;
    color: ${colors.$black};
    margin-left: 7px;
`;

// ====== UTILS ======

const getSelectedFileBorder = (pressed: boolean) => {
    if (pressed) {
        return `border-left-color: ${colors.$blue};
        border-left-width: 2px;
        border-style: solid;`;
    }
    return '';
};
