import * as div from './code-editor-header.style';
import { Icon } from '../../../icons/components/icon/icon';
import { FILE, PREVIEW } from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import { SourceFileTreeLevel } from '../../interfaces/source-file-tree';
import * as React from 'react';

interface Props {
    width: number;
    onPressHeaderFile: (file: SourceFileTreeLevel) => void;
    files: SourceFileTreeLevel[];
    pressedFile: SourceFileTreeLevel;
    cutFileFromHeader: (file: SourceFileTreeLevel) => void;
}

interface State { }

export class CodeEditorHeader extends React.Component<Props, State> {

    constructor(props: Props) {

        super(props);

        this.state = {};
    }

    public render() {
        const { files, onPressHeaderFile, pressedFile, cutFileFromHeader, width } = this.props;

        return (
            <div.CodeEditorHeader data-cy='code-editor-header' width={width}>
                {/* Explorer */}
                <div.Explorer data-cy='explorer'>
                    <div.ExplorerText data-cy='explorer-text'>
                        Explorer
                    </div.ExplorerText>
                </div.Explorer>

                {/* File Display */}
                <div.FilesDisplay data-cy='files-display' width={width}>

                    {/* Selected Files */}
                    <div.SelectedFiles data-cy='selected-files'>
                        {
                            !!files && files.map(file =>
                                <div.SelectedFile key={file.name}
                                    data-cy='selected-file'
                                    onPress={() => onPressHeaderFile(file)} pressed={pressedFile === file}>
                                    {/** File icon */}
                                    <Icon path={FILE} color={colors.$blue} />

                                    {/* Selected File Text */}
                                    <div.SelectedFileText data-cy='selected-file-text'>
                                        {file.name}
                                    </div.SelectedFileText>

                                    {/* Close File Wrapper */}
                                    <div.CloseFileWrapper onPress={() => cutFileFromHeader(file)}>
                                        <div.CloseFileIcon>
                                            x
                                        </div.CloseFileIcon>
                                    </div.CloseFileWrapper>
                                </div.SelectedFile>
                            )
                        }
                    </div.SelectedFiles>

                    <div.Preview>
                        {/** Preview Icon */}
                        <Icon path={PREVIEW} color={colors.$black} height={20} width={20} />

                        {/* Preview Text */}
                        <div.PreviewText data-cy='preview-text'>
                            Preview
                        </div.PreviewText>
                    </div.Preview>
                </div.FilesDisplay>
            </div.CodeEditorHeader>
        );
    }
}
