import { PackageVersionsState } from '../../interfaces/state/package-versions.state';

export const packageVersionsInitialState: PackageVersionsState = {
    packageVersions: null,
}