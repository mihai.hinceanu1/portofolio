import { PackagesState } from '../../interfaces/state/packages-state';

export const packagesInitialState: PackagesState = {
    packages: null,

    admin: {
        uploadPackageImage: {
            response: null,
            error: null,
        }
    }
};