import { dependenciesInitialState } from './dependencies.init-state';
import { packageVersionsInitialState } from './package-versions.init-state';
import { packagesInitialState } from './packages.init-state';
import { sourceFileInitialState } from './source-file.init-state';
import { SourceCodeCommonState } from '../../interfaces/state/source-code-common.state';
import { SourceCodeState } from '../../interfaces/state/source-code.state';

export const sourceCodeCommonInitialState: SourceCodeCommonState = {
    showAdminNavigator: false,
};

export const sourceCodeInitialState: SourceCodeState = {
    sourceFile: sourceFileInitialState,
    common: sourceCodeCommonInitialState,
    dependenciesState: dependenciesInitialState,
    packagesState: packagesInitialState,
    packageVersions: packageVersionsInitialState,
};
