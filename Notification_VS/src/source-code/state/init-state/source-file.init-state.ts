import { SourceFileState } from '../../interfaces/state/source-file.state';

export const sourceFileInitialState: SourceFileState = {
    tree: null,
};
