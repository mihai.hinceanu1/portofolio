import { DependenciesState } from '../../interfaces/state/dependencies.state';

export const dependenciesInitialState: DependenciesState = {
    dependencies: null,
    versionDependencies: null,
};