import { AppState } from '../../../shared/interfaces/app.state';
import { ICodePackage } from '../../interfaces/packages';
import { UploadPackageImageResponse } from '../../interfaces/source-code';
import { SourceCodeState } from '../../interfaces/state/source-code.state';
import { SOURCE_CODE_MODULE } from '../source-code.selectors';
import { createSelector } from 'reselect';

export const PACKAGES = createSelector<AppState, SourceCodeState, ICodePackage[]>(
    SOURCE_CODE_MODULE,
    (state: SourceCodeState) => state.packagesState.packages,
);

export const UPLOAD_PACKAGE_IMAGE = createSelector<AppState, SourceCodeState, UploadPackageImageResponse>(
    SOURCE_CODE_MODULE,
    (state: SourceCodeState) => state.packagesState.admin.uploadPackageImage,
);