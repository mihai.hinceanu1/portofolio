import { AppState } from '../../../shared/interfaces/app.state';
import { SourceFileTree } from '../../interfaces/source-file-tree';
import { SourceCodeState } from '../../interfaces/state/source-code.state';
import { SOURCE_CODE_MODULE } from '../source-code.selectors';
import { createSelector } from 'reselect';

export const SOURCE_FILE_TREE = createSelector<AppState, SourceCodeState, SourceFileTree[]>(
    SOURCE_CODE_MODULE,
    state => state.sourceFile.tree,
);
