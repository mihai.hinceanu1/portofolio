import { AppState } from '../../../shared/interfaces/app.state';
import { SourceCodeState } from '../../interfaces/state/source-code.state';
import { SOURCE_CODE_MODULE } from '../source-code.selectors';
import { createSelector } from 'reselect';

export const ADMIN_NAVIGATOR = createSelector<AppState, SourceCodeState, boolean>(
    SOURCE_CODE_MODULE,
    state => state.common.showAdminNavigator,
);
