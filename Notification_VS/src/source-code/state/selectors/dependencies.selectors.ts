import { AppState } from '../../../shared/interfaces/app.state';
import { IDependency } from '../../interfaces/floating-menu';
import { SourceCodeState } from '../../interfaces/state/source-code.state';
import { SOURCE_CODE_MODULE } from '../source-code.selectors';
import { createSelector } from 'reselect';

export const DEPENDENCIES = createSelector<AppState, SourceCodeState, IDependency[]>(
    SOURCE_CODE_MODULE,
    state => state.dependenciesState.dependencies,
);

export const VERSION_DEPENDENCIES = createSelector<AppState, SourceCodeState, IDependency[]>(
    SOURCE_CODE_MODULE,
    state => state.dependenciesState.versionDependencies,
);