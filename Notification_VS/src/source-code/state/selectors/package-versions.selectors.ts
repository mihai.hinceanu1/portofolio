import { AppState } from '../../../shared/interfaces/app.state';
import { PackageVersion } from '../../interfaces/package-version';
import { SourceCodeState } from '../../interfaces/state/source-code.state';
import { createSelector, Selector } from 'reselect';

const SOURCE_CODE_MODULE: Selector<AppState, SourceCodeState> = state => state.sourceCode;

export const PACKAGE_VERSIONS = createSelector<AppState, SourceCodeState, PackageVersion[]>(
    SOURCE_CODE_MODULE,
    state => state.packageVersions.packageVersions,
);