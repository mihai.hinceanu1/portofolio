import { AppState } from '../../shared/interfaces/app.state';
import { SourceCodeState } from '../interfaces/state/source-code.state';
import { Selector } from 'reselect';
// import { SourceFile } from '../interfaces/source-file';
// import { SourceFileTree } from '../interfaces/source-file-tree';

// TODO Split in multiple selectors
export const SOURCE_CODE_MODULE: Selector<AppState, SourceCodeState> = (state: AppState) => state.sourceCode;

// export const SOURCE_FILES = createSelector<AppState, SourceCodeState, SourceFile[]>(
//     SOURCE_CODE_MODULE,
//     (state: SourceCodeState) => state.sourceFiles,
// );

// export const SOURCE_FILE_TREE = createSelector<AppState, SourceCodeState, SourceFileTree[]>(
//     SOURCE_CODE_MODULE,
//     (state: SourceCodeState) => state.sourceFileTree,
// );
