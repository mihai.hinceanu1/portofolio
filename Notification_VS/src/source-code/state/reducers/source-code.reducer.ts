import { dependenciesReducer } from './dependencies.reducer';
import { packageVersionsReducer } from './package-versions.reducer';
import { packagesReducer } from './packages.reducer';
import { sourceFileReducer } from './source-file.reducer';
import { Action } from '../../../shared/interfaces/shared';
import { SourceCodeCommonState } from '../../interfaces/state/source-code-common.state';
import { SourceCodeState } from '../../interfaces/state/source-code.state';
import * as commonActions from '../actions/source-code-common.actions';
import { sourceCodeCommonInitialState } from '../init-state/source-code.init-state';
import { combineReducers, Reducer } from 'redux';

export function sourceCodeReducer(): Reducer<SourceCodeState> {
    return combineReducers({
        common: commonReducer,
        dependenciesState: dependenciesReducer,
        packagesState: packagesReducer,
        sourceFile: sourceFileReducer,
        packageVersions: packageVersionsReducer,
    });
}

const commonReducer = (state: SourceCodeCommonState = sourceCodeCommonInitialState, action: Action<any>): SourceCodeCommonState => {
    switch (action.type) {

        case commonActions.TOGGLE_ADMIN_NAVIGATOR: {
            const { showAdminNavigator } = state;

            return {
                ...state,
                showAdminNavigator: !showAdminNavigator,
            };
        }

        default:
            return state;
    }
};
