import { Action } from '../../../shared/interfaces/shared';
import { DependenciesState } from '../../interfaces/state/dependencies.state';
import * as actions from '../actions/dependencies.actions';
import { ADD_VERSION_DEPENDENCY_OK } from '../actions/package-versions.actions';
import { dependenciesInitialState } from '../init-state/dependencies.init-state';

export function dependenciesReducer(state: DependenciesState = dependenciesInitialState, action: Action<any>): DependenciesState {
    switch (action.type) {

        case actions.GET_DEPENDENCIES_OK: {
            return {
                ...state,
                dependencies: action.payload,
            };
        }

        case actions.GET_DEPENDENCIES_FAIL: {
            return {
                ...state,
                dependencies: null,
            };
        }

        case actions.GET_VERSION_DEPENDENCIES_OK: {
            return {
                ...state,
                versionDependencies: action.payload,
            };
        }

        case actions.GET_VERSION_DEPENDENCIES_FAIL: {
            return {
                ...state,
                versionDependencies: null,
            };
        }

        case ADD_VERSION_DEPENDENCY_OK: {
            return {
                ...state,
                versionDependencies: action.payload,
            };
        }

        default:
            return state;
    }
}