import { Action } from '../../../shared/interfaces/shared';
import { PackagesState } from '../../interfaces/state/packages-state';
import * as actions from '../actions/packages.actions';
import { packagesInitialState } from '../init-state/packages.init-state';

export function packagesReducer(state: PackagesState = packagesInitialState, action: Action<any>): PackagesState {

    switch (action.type) {

        case actions.GET_PACKAGES_OK: {
            return {
                ...state,
                packages: action.payload,
            };
        }

        case actions.GET_PACKAGES_FAIL: {
            return {
                ...state,
                packages: [],
            };
        }

        case actions.CREATE_PACKAGE_OK: {
            let newPackage = action.payload;
            const { packages } = state;
            let newPackageCopy = [newPackage];
            let updatedPackages = [...newPackageCopy, ...packages];

            return {
                ...state,
                packages: updatedPackages,
            };
        }

        case actions.EDIT_PACKAGE_OK: {
            let updatedPackage = action.payload;
            const { packages } = state;

            let packageIndex = packages.findIndex(codePackage => codePackage._id === updatedPackage._id);
            let updatedPackages = [...packages];
            updatedPackages[packageIndex] = updatedPackage;

            return {
                ...state,
                packages: updatedPackages,
            };
        }

        case actions.DELETE_PACKAGE_OK: {
            let packageId = action.payload;

            const { packages } = state;
            let packagesCopy = [...packages];

            packagesCopy = packagesCopy.filter(pack => pack._id !== packageId);

            return {
                ...state,
                packages: packagesCopy,
            };

        }

        case actions.UPLOAD_PACKAGE_IMAGE_OK: {
            /** Find and update with the edited package */
            let updatedPackage = action.payload;
            let { packages } = state;

            packages = packages.map(pack => {
                if (pack._id === updatedPackage._id) {
                    return updatedPackage;
                }

                return pack;
            });

            /** Update the response */
            let uploadImage = { ...state.admin.uploadPackageImage };
            uploadImage.response = 'Image uploaded successfully';
            uploadImage.error = null;

            return {
                ...state,
                packages,
                admin: {
                    ...state.admin,
                    uploadPackageImage: uploadImage,
                },
            };
        }

        case actions.UPLOAD_PACKAGE_IMAGE_FAIL: {
            let uploadImage = { ...state.admin.uploadPackageImage };
            uploadImage.response = null;
            uploadImage.error = action.payload;

            return {
                ...state,
                admin: {
                    ...state.admin,
                    uploadPackageImage: uploadImage
                }
            };
        }

        default:
            return state;
    }
}