import { Action } from '../../../shared/interfaces/shared';
import { PackageVersionsState } from '../../interfaces/state/package-versions.state';
import * as actions from '../actions/package-versions.actions';
import { packageVersionsInitialState } from '../init-state/package-versions.init-state';

export function packageVersionsReducer(state: PackageVersionsState = packageVersionsInitialState, action: Action<any>): PackageVersionsState {

    switch (action.type) {

        case actions.EDIT_PACKAGE_VERSIONS_OK: {
            let packageVersion = action.payload;
            const { packageVersions } = state;
            let copyOfPackageVersions = [...packageVersions];

            copyOfPackageVersions = copyOfPackageVersions.map(version => version._id === packageVersion._id ? packageVersion : version);

            return {
                ...state,
                packageVersions: copyOfPackageVersions,
            };
        }

        case actions.DELETE_PACKAGE_VERSION_IMAGE_OK: {
            const { imgPath, versionId } = action.payload;
            const { packageVersions } = state;

            let updatedPackages = [...packageVersions].map(version => {
                if (version._id === versionId) {
                    let filteredImages = version.images.filter(image => image !== imgPath);
                    version.images = filteredImages;
                    return version;
                } else {
                    return version;
                }
            });

            return {
                ...state,
                packageVersions: updatedPackages,
            };
        }

        case actions.GET_ALL_PACKAGE_VERSIONS_OK: {
            return {
                ...state,
                packageVersions: action.payload,
            };
        }

        case actions.GET_ALL_PACKAGE_VERSIONS_FAIL: {
            return {
                ...state,
                packageVersions: [],
            };
        }

        case actions.GET_ALL_PACKAGE_VERSIONS_OK: {
            return {
                ...state,
                packageVersions: action.payload,
            };
        }

        case actions.GET_ALL_PACKAGE_VERSIONS_FAIL: {
            return {
                ...state,
                packageVersions: [],
            };
        }

        case actions.ADD_PACKAGE_VERSION_IMAGE_OK: {
            const response = action.payload;
            const { packageVersions } = state;

            let editedPackageVersions = [...packageVersions].map(version => {
                if (version._id === response.data.packageVersionId) {
                    version.images.push(response.data.imgPath);
                    return version;
                } else {
                    return version;
                }
            });

            return {
                ...state,
                packageVersions: editedPackageVersions,
            };
        }

        case actions.EDIT_PACKAGE_VERSION_IMAGE_OK: {
            const { packageVersions } = state;
            const response = action.payload;

            let editedPackages = [...packageVersions].map(pack => {
                if (pack._id === response.data.packageVersionId) {
                    let filteredImages = pack.images.map(image => image.includes(response.data.pathToReplace) ? response.data.imgPath : image);
                    pack.images = filteredImages;
                    return pack;
                } else {
                    return pack;
                }
            });

            return {
                ...state,
                packageVersions: editedPackages,
            };
        }

        case actions.DELETE_PACKAGE_VERSIONS_OK: {
            const { packageVersions } = state;
            const versionId = action.payload;
            let copyOfPackageVersions = [...packageVersions];

            copyOfPackageVersions = copyOfPackageVersions.filter(version => version._id !== versionId)

            return {
                ...state,
                packageVersions: copyOfPackageVersions,
            };
        }

        case actions.CREATE_PACKAGE_VERSIONS_OK: {
            const packageVersion = action.payload;
            const { packageVersions } = state;

            let packageVersionCopy = [packageVersion];
            let packages = [...packageVersionCopy, ...packageVersions];

            return {
                ...state,
                packageVersions: packages,
            };
        }

        default:
            return state;
    }
}