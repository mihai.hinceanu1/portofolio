import { Action } from '../../../shared/interfaces/shared';
import { SourceFileState } from '../../interfaces/state/source-file.state';
import * as actions from '../actions/source-files.actions';
import { sourceFileInitialState } from '../init-state/source-file.init-state';

export function sourceFileReducer(state: SourceFileState = sourceFileInitialState, action: Action<any>): SourceFileState {
    switch (action.type) {
        case actions.GET_SOURCE_FILES_TREE_OK: {
            return {
                ...state,
                tree: action.payload,
            };
        }

        default:
            return state;
    }
}
