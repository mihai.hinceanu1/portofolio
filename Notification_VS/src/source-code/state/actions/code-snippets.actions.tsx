import { Action } from '../../../shared/interfaces/shared';
import { CodeSnippet } from '../../interfaces/code-snippet';

// ====== CREATE CODE SNIPPET ======

export const CREATE_CODE_SNIPPET_REQ = 'CREATE_CODE_SNIPPET_REQ';
export const createCodeSnippetReq = (codeSnippet: CodeSnippet): Action<CodeSnippet> => ({
    type: CREATE_CODE_SNIPPET_REQ,
    payload: codeSnippet,
});

export const CREATE_CODE_SNIPPET_OK = 'CREATE_CODE_SNIPPET_OK';
export const createCodeSnippetOk = (): Action<null> => ({
    type: CREATE_CODE_SNIPPET_OK,
});

export const CREATE_CODE_SNIPPET_FAIL = 'CREATE_CODE_SNIPPET_FAIL';
export const createCodeSnippetFail = (error: Error): Action<Error> => ({
    type: CREATE_CODE_SNIPPET_FAIL,
    payload: error,
});
