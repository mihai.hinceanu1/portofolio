import { Action } from '../../../shared/interfaces/shared';
import { IDependency } from '../../interfaces/floating-menu';

export const GET_DEPENDENCIES_REQ = 'GET_DEPENDENCIES_REQ';
export const getDependenciesReq = (): Action<null> => ({
    type: GET_DEPENDENCIES_REQ,
});

export const GET_DEPENDENCIES_OK = 'GET_DEPENDENCIES_OK';
export const getDependenciesOk = (dependencies: IDependency[]): Action<IDependency[]> => ({
    type: GET_DEPENDENCIES_OK,
    payload: dependencies,
});

export const GET_DEPENDENCIES_FAIL = 'GET_DEPENDENCIES_FAIL';
export const getDependenciesFail = (error: string): Action<string> => ({
    type: GET_DEPENDENCIES_FAIL,
    payload: error,
});

export const GET_VERSION_DEPENDENCIES_REQ = 'GET_VERSION_DEPENDENCIES_REQ';
export const getVersionDependenciesReq = (pathName: string): Action<string> => ({
    type: GET_VERSION_DEPENDENCIES_REQ,
    payload: pathName,
});

export const GET_VERSION_DEPENDENCIES_OK = 'GET_VERSION_DEPENDENCIES_OK';
export const getVersionDependenciesOk = (dependencies: IDependency[]): Action<IDependency[]> => ({
    type: GET_VERSION_DEPENDENCIES_OK,
    payload: dependencies,
});

export const GET_VERSION_DEPENDENCIES_FAIL = 'GET_VERSION_DEPENDENCIES_FAIL';
export const getVersionDependenciesFail = (error: string): Action<string> => ({
    type: GET_VERSION_DEPENDENCIES_FAIL,
    payload: error,
});