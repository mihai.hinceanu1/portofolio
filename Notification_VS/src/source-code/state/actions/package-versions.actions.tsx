import { Action } from '../../../shared/interfaces/shared';
import { IDependency } from '../../interfaces/floating-menu';
import {
    AddPackageVersionImage,
    AddVersionDependencyReq,
    CreatePackageVersionCfg,
    DeletePackageVersionImage,
    EditPackageVersionImage,
    EditPackageVersionImageResponse,
    PackageVersion,
    UpdatePackageVersion,
    UploadPackageVersionImageResponse
    } from '../../interfaces/package-version';
import { DeletePackageVersionConfig } from '../../interfaces/source-code';

// ====== EDIT PACKAGE VERSION ======

export const EDIT_PACKAGE_VERSIONS_REQ = 'EDIT_PACKAGE_VERSIONS_REQ';
export const editPackageVersionReq = (version: UpdatePackageVersion): Action<UpdatePackageVersion> => ({
    type: EDIT_PACKAGE_VERSIONS_REQ,
    payload: version,
});

export const EDIT_PACKAGE_VERSIONS_OK = 'EDIT_PACKAGE_VERSIONS_OK';
export const editPackageVersionOk = (packageVersion: PackageVersion): Action<PackageVersion> => ({
    type: EDIT_PACKAGE_VERSIONS_OK,
    payload: packageVersion
});

export const EDIT_PACKAGE_VERSIONS_FAIL = 'EDIT_PACKAGE_VERSIONS_FAIL';
export const editPackageVersionFail = (error: Error): Action<Error> => ({
    type: EDIT_PACKAGE_VERSIONS_FAIL,
    payload: error,
});

// ====== GET ALL PACKAGE VERSIONS BY PACKAGE ID ======

export const GET_ALL_PACKAGE_VERSIONS_REQ = 'GET_ALL_PACKAGE_VERSIONS_REQ';
export const getAllPackageVersionsReq = (packagePathName: string): Action<string> => ({
    type: GET_ALL_PACKAGE_VERSIONS_REQ,
    payload: packagePathName,
});

export const GET_ALL_PACKAGE_VERSIONS_OK = 'GET_ALL_PACKAGE_VERSIONS_OK';
export const getAllPackageVersionsOk = (packageVersions: PackageVersion[]): Action<PackageVersion[]> => ({
    type: GET_ALL_PACKAGE_VERSIONS_OK,
    payload: packageVersions,
});

export const GET_ALL_PACKAGE_VERSIONS_FAIL = 'GET_ALL_PACKAGE_VERSIONS_FAIL';
export const getAllPackageVersionsFail = (error: Error): Action<Error> => ({
    type: GET_ALL_PACKAGE_VERSIONS_FAIL,
    payload: error,
});

// ====== DELETE PACKAGE VERSION ======

export const DELETE_PACKAGE_VERSIONS = 'DELETE_PACKAGE_VERSIONS';
export const deletePackageVersion = (deletePackageVersionConfig: DeletePackageVersionConfig):
    Action<DeletePackageVersionConfig> => ({
        type: DELETE_PACKAGE_VERSIONS,
        payload: deletePackageVersionConfig,
    });

export const DELETE_PACKAGE_VERSIONS_OK = 'DELETE_PACKAGE_VERSIONS_OK';
export const deletePackageVersionOk = (packageVersionId: string): Action<string> => ({
    type: DELETE_PACKAGE_VERSIONS_OK,
    payload: packageVersionId,
});

export const DELETE_PACKAGE_VERSIONS_FAIL = 'DELETE_PACKAGE_VERSIONS_FAIL';
export const deletePackageVersionFail = (error: Error): Action<Error> => ({
    type: DELETE_PACKAGE_VERSIONS_FAIL,
    payload: error,
});

// ====== DELETE_PACKAGE_VERSION_IMAGE ======

export const DELETE_PACKAGE_VERSION_IMAGE_REQ = 'DELETE_PACKAGE_VERSION_IMAGE_REQ';
export const deletePackageVersionImageReq = (request: DeletePackageVersionImage): Action<DeletePackageVersionImage> => ({
    type: DELETE_PACKAGE_VERSION_IMAGE_REQ,
    payload: request,
});

export const DELETE_PACKAGE_VERSION_IMAGE_OK = 'DELETE_PACKAGE_VERSION_IMAGE_OK';
export const deletePackageVersionImageOk = (request: DeletePackageVersionImage): Action<DeletePackageVersionImage> => ({
    type: DELETE_PACKAGE_VERSION_IMAGE_OK,
    payload: request,
});

export const DELETE_PACKAGE_VERSION_IMAGE_FAIL = 'DELETE_PACKAGE_VERSION_IMAGE_FAIL';
export const deletePackageVersionImageFail = (err: Error): Action<Error> => ({
    type: DELETE_PACKAGE_VERSION_IMAGE_FAIL,
    payload: err,
});

// ====== EDIT_PACKAGE_VERSION_IMAGE ======

export const EDIT_PACKAGE_VERSION_IMAGE_REQ = 'EDIT_PACKAGE_VERSION_IMAGE_REQ';
export const editPackageVersionImageReq = (request: EditPackageVersionImage): Action<EditPackageVersionImage> => ({
    type: EDIT_PACKAGE_VERSION_IMAGE_REQ,
    payload: request,
}) ;

export const EDIT_PACKAGE_VERSION_IMAGE_OK = `EDIT_PACKAGE_VERSION_IMAGE_OK`;
export const editPackageVersionImageOk = (response: EditPackageVersionImageResponse): Action<EditPackageVersionImageResponse> => ({
    type: EDIT_PACKAGE_VERSION_IMAGE_OK,
    payload: response,
});

export const EDIT_PACKAGE_VERSION_IMAGE_FAIL = 'EDIT_PACKAGE_VERSION_IMAGE_FAIL';
export const editPackageVersionImageFail = (err: Error): Action<Error> => ({
    type: EDIT_PACKAGE_VERSION_IMAGE_FAIL,
    payload: err,
});

// ====== ADD_PACKAGE_VERSION_IMAGE ======

export const ADD_PACKAGE_VERSION_IMAGE_REQ = 'ADD_PACKAGE_VERSION_IMAGE_REQ';
export const addPackageVersionImageReq = (request: AddPackageVersionImage): Action<AddPackageVersionImage> => ({
    type: ADD_PACKAGE_VERSION_IMAGE_REQ,
    payload: request,
});

export const ADD_PACKAGE_VERSION_IMAGE_OK = 'ADD_PACKAGE_VERSION_IMAGE_OK';
export const addPackageVersionImageOk = (response: UploadPackageVersionImageResponse): Action<UploadPackageVersionImageResponse> => ({
    type: ADD_PACKAGE_VERSION_IMAGE_OK,
    payload: response,
});

export const ADD_PACKAGE_VERSION_IMAGE_FAIL = 'ADD_PACKAGE_VERSION_IMAGE_FAIL';
export const addPackageVersionImageFail = (error: Error): Action<Error> => ({
    type: ADD_PACKAGE_VERSION_IMAGE_FAIL,
    payload: error,
});

// ====== CREATE PACKAGE VERSION ======

export const CREATE_PACKAGE_VERSIONS_REQ = 'CREATE_PACKAGE_VERSIONS_REQ';
export const createPackageVersionReq = (
    request: CreatePackageVersionCfg,
): Action<CreatePackageVersionCfg> => ({
    type: CREATE_PACKAGE_VERSIONS_REQ,
    payload: request,
});

export const CREATE_PACKAGE_VERSIONS_OK = 'CREATE_PACKAGE_VERSIONS_OK';
export const createPackageVersionOk = (packageVersion: PackageVersion): Action<PackageVersion> => ({
    type: CREATE_PACKAGE_VERSIONS_OK,
    payload: packageVersion
});

export const CREATE_PACKAGE_VERSIONS_FAIL = 'CREATE_PACKAGE_VERSIONS_FAIL';
export const createPackageVersionFail = (error: Error): Action<Error> => ({
    type: CREATE_PACKAGE_VERSIONS_FAIL,
    payload: error,
});

// ====== ADD DEPENDENCY ======

export const ADD_VERSION_DEPENDENCY_REQ = 'ADD_VERSION_DEPENDENCY_REQ';
export const addVersionDependencyReq = (addDependencyReq: AddVersionDependencyReq): Action<AddVersionDependencyReq> => ({
    type: ADD_VERSION_DEPENDENCY_REQ,
    payload: addDependencyReq,
});

export const ADD_VERSION_DEPENDENCY_OK = 'ADD_VERSION_DEPENDENCY_OK';
export const addVersionDependencyOk = (dependencies: IDependency[]): Action<IDependency[]> => ({
    type: ADD_VERSION_DEPENDENCY_OK,
    payload: dependencies,
});

export const ADD_VERSION_DEPENDENCY_FAIL = 'ADD_VERSION_DEPENDENCY_FAIL';
export const addVersionDependencyFail = (error: string): Action<string> => ({
    type: ADD_VERSION_DEPENDENCY_FAIL,
    payload: error,
});