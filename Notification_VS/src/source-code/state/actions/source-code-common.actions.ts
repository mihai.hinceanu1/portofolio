import { Action } from '../../../shared/interfaces/shared';

export const TOGGLE_ADMIN_NAVIGATOR = 'TOGGLE_ADMIN_NAVIGATOR';
export const toggleAdminNavigator = (): Action<null> => ({
    type: TOGGLE_ADMIN_NAVIGATOR,
    payload: null,
});
