import { Action } from '../../../shared/interfaces/shared';
import { ICodePackage, NewPackageConfig, UploadPackageImageConfig } from '../../interfaces/packages';
import { DeletePackageConfig } from '../../interfaces/source-code';

// ====== GET ALL PACKAGES ======

export const GET_PACKAGES_REQ = 'GET_PACKAGES_REQ';
export const getAllPackagesReq = (): Action<null> => ({
    type: GET_PACKAGES_REQ,
});

export const GET_PACKAGES_OK = 'GET_PACKAGES_OK';
export const getAllPackagesOk = (packages: ICodePackage[]): Action<ICodePackage[]> => ({
    type: GET_PACKAGES_OK,
    payload: packages,
});

export const GET_PACKAGES_FAIL = 'GET_PACKAGES_FAIL';
export const getAllPackagesFail = (error: Error): Action<Error> => ({
    type: GET_PACKAGES_FAIL,
    payload: error,
});

// ====== CREATE PACKAGE ======

export const CREATE_PACKAGE_REQ = 'CREATE_PACKAGE_REQ';
export const createPackageReq = (packages: NewPackageConfig): Action<NewPackageConfig> => ({
    type: CREATE_PACKAGE_REQ,
    payload: packages,
});

export const CREATE_PACKAGE_OK = 'CREATE_PACKAGE_OK';
export const createPackageOk = (codePackage: ICodePackage): Action<ICodePackage> => ({
    type: CREATE_PACKAGE_OK,
    payload: codePackage,
});

export const CREATE_PACKAGE_FAIL = 'CREATE_PACKAGE_FAIL';
export const createPackageFail = (error: Error): Action<Error> => ({
    type: CREATE_PACKAGE_FAIL,
    payload: error,
});

// ====== EDIT PACKAGE ======

export const EDIT_PACKAGE_REQ = 'EDIT_PACKAGE_REQ';
export const editPackageReq = (codePackage: ICodePackage): Action<ICodePackage> => ({
    type: EDIT_PACKAGE_REQ,
    payload: codePackage,
});

export const EDIT_PACKAGE_OK = 'EDIT_PACKAGE_OK';
export const editPackageOk = (codePackage: ICodePackage): Action<ICodePackage> => ({
    type: EDIT_PACKAGE_OK,
    payload: codePackage,
});

export const EDIT_PACKAGE_FAIL = 'EDIT_PACKAGE_FAIL';
export const editPackageFail = (error: Error): Action<Error> => ({
    type: EDIT_PACKAGE_FAIL,
    payload: error,
});

// ====== DELETE PACKAGE ======

export const DELETE_PACKAGE = 'DELETE_PACKAGE';
export const deletePackage = (deletePackageConfig: DeletePackageConfig): Action<DeletePackageConfig> => ({
    type: DELETE_PACKAGE,
    payload: deletePackageConfig,
});

export const DELETE_PACKAGE_OK = 'DELETE_PACKAGE_OK';
export const deletePackageOk = (packageId: string): Action<string> => ({
    type: DELETE_PACKAGE_OK,
    payload: packageId,
});

export const DELETE_PACKAGE_FAIL = 'DELETE_PACKAGE_FAIL';
export const deletePackageFail = (error: Error): Action<Error> => ({
    type: DELETE_PACKAGE_FAIL,
    payload: error,
});

// ====== UPLOAD PACKAGE IMAGE ======

export const UPLOAD_PACKAGE_IMAGE_REQ = 'UPLOAD_PACKAGE_IMAGE_REQ';
export const uploadImageReq = (config: UploadPackageImageConfig): Action<UploadPackageImageConfig> => ({
    type: UPLOAD_PACKAGE_IMAGE_REQ,
    payload: config,
});

export const UPLOAD_PACKAGE_IMAGE_OK = 'UPLOAD_PACKAGE_IMAGE_OK';
export const uploadImageOk = (codePackage: ICodePackage): Action<ICodePackage> => ({
    type: UPLOAD_PACKAGE_IMAGE_OK,
    payload: codePackage,
});

export const UPLOAD_PACKAGE_IMAGE_FAIL = 'UPLOAD_PACKAGE_IMAGE_FAIL';
export const uploadImageFail = (error: Error): Action<Error> => ({
    type: UPLOAD_PACKAGE_IMAGE_FAIL,
    payload: error,
});