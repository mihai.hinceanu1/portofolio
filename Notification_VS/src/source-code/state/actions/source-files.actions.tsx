import { Action } from '../../../shared/interfaces/shared';
import { AddSourceFile, SourceFile, UpdateSrcCode } from '../../interfaces/source-file';
import { SourceFileTree } from '../../interfaces/source-file-tree';

// ====== GET SOURCE FILES BY PACKAGE VERSION ID

export const GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_REQ = `GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_REQ`;
export const getSourceFilesByPackageVersionIdReq = (versionId: string): Action<string> => ({
    type: GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_REQ,
    payload: versionId,
});

export const GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_OK = `GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_OK`;
export const getSourceFilesByPackageVersionIdOk = (sourceFiles: SourceFile[]): Action<SourceFile[]> => ({
    type: GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_OK,
    payload: sourceFiles,
});

export const GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_FAIL = `GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_FAIL`;
export const getSourceFilesByPackageVersionIdFail = (error: Error): Action<Error> => ({
    type: GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_FAIL,
    payload: error,
});

// ====== GET SOURCE FILE TREE ======

export const GET_SOURCE_FILES_TREE_REQ = 'GET_SOURCE_FILES_TREE_REQ';
export const getSourceFilesTreeReq = (pathName: string): Action<string> => ({
    type: GET_SOURCE_FILES_TREE_REQ,
    payload: pathName,
});

export const GET_SOURCE_FILES_TREE_OK = 'GET_SOURCE_FILES_TREE_OK';
export const getSourceFilesTreeOk = (sourceFileTree: SourceFileTree[]): Action<SourceFileTree[]> => ({
    type: GET_SOURCE_FILES_TREE_OK,
    payload: sourceFileTree,
});

export const GET_SOURCE_FILES_TREE_FAIL = 'GET_SOURCE_FILES_TREE_FAIL';
export const getSourceFilesTreeFail = (err: Error): Action<Error> => ({
    type: GET_SOURCE_FILES_TREE_FAIL,
    payload: err
});

// ====== ADD SOURCE FILE ======

export const ADD_SOURCE_FILE_REQ = 'ADD_SOURCE_FILE_REQ';
export const addSourceFileReq = (req: AddSourceFile): Action<AddSourceFile> => ({
    type: ADD_SOURCE_FILE_REQ,
    payload: req,
});

export const ADD_SOURCE_FILE_OK = 'ADD_SOURCE_FILE_OK';
export const addSourceFileOk = (): Action<null> => ({
    type: ADD_SOURCE_FILE_OK,
});

export const ADD_SOURCE_FILE_FAIL = 'ADD_SOURCE_FILE_FAIL';
export const addSourceFileFail = (err: Error): Action<Error> => ({
    type: ADD_SOURCE_FILE_FAIL,
    payload: err,
});

export const UPDATE_SOURCE_FILE_CODE_REQ = 'UPDATE_SOURCE_FILE_CODE_REQ';
export const updateSourceFileCodeReq = (req: UpdateSrcCode): Action<UpdateSrcCode> => ({
    type: UPDATE_SOURCE_FILE_CODE_REQ,
    payload: req,
});

export const UPDATE_SOURCE_FILE_CODE_OK = 'UPDATE_SOURCE_FILE_CODE_OK';
export const updateSourceFileCodeOk = (): Action<null> => ({
    type: UPDATE_SOURCE_FILE_CODE_OK,
});

export const UPDATE_SOURCE_FILE_CODE_FAIL = 'UPDATE_SOURCE_FILE_CODE_FAIL';
export const updateSourceFileCodeFail = (err: Error): Action<Error> => ({
    type: UPDATE_SOURCE_FILE_CODE_FAIL,
    payload: err,
});