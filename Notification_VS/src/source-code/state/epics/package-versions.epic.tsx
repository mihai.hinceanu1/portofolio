import { AppState } from '../../../shared/interfaces/app.state';
import { Action } from '../../../shared/interfaces/shared';
import { TOASTR_TYPE } from '../../../shared/interfaces/toastr';
import { createToastr } from '../../../shared/services/shared.service';
import {
    AddPackageVersionImage,
    AddVersionDependencyReq,
    CreatePackageVersionCfg,
    DeletePackageVersionImage,
    EditPackageVersionImage,
    EditPackageVersionImageResponse,
    PackageVersion,
    UpdatePackageVersion,
    UploadPackageVersionImageResponse
    } from '../../interfaces/package-version';
import { DeletePackageVersionConfig } from '../../interfaces/source-code';
import * as packageVersionsWebapi from '../../webapis/package-versions.webapi';
import * as srcVersionsActions from '../actions/package-versions.actions';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

// ====== PACKAGE VERSIONS ======

export const getAllPackageVersions = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(srcVersionsActions.GET_ALL_PACKAGE_VERSIONS_REQ).pipe(
        map(action => action.payload),
        concatMap(packagePathName => packageVersionsWebapi.getAllPackageVersions(packagePathName).pipe(
            map((packageVersions) => srcVersionsActions.getAllPackageVersionsOk(packageVersions)),
            catchError(error => of(srcVersionsActions.getAllPackageVersionsFail(error))),
        )),
    );

export const addPackageVersionImage$ = (action$: ActionsObservable<Action<AddPackageVersionImage>>, _store: Store<AppState>) =>
    action$.ofType(srcVersionsActions.ADD_PACKAGE_VERSION_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(request => packageVersionsWebapi.addPackageVersionImage(request).pipe(
            map((response: UploadPackageVersionImageResponse) => {
                return srcVersionsActions.addPackageVersionImageOk(response);
            }),
            catchError(error => of(srcVersionsActions.addPackageVersionImageFail(error))),
        )),
    );

export const editPackageVersionImage$ = (action$: ActionsObservable<Action<EditPackageVersionImage>>, _store: Store<AppState>) =>
    action$.ofType(srcVersionsActions.EDIT_PACKAGE_VERSION_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(request => packageVersionsWebapi.editPackageVersionImage(request).pipe(
            map((response: EditPackageVersionImageResponse) => {
                return srcVersionsActions.editPackageVersionImageOk(response);
            }),
            catchError(error => of(srcVersionsActions.editPackageVersionImageFail(error))),
        ))
    );

export const editPackageVersion$ = (
    action$: ActionsObservable<Action<UpdatePackageVersion>>, _store: Store<AppState>,
) =>
    action$.ofType(srcVersionsActions.EDIT_PACKAGE_VERSIONS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => packageVersionsWebapi.editPackageVersion(request).pipe(
            map((packageVersion: PackageVersion) => {
                return srcVersionsActions.editPackageVersionOk(packageVersion);
            }),
            catchError(error => of(srcVersionsActions.editPackageVersionFail(error))),
        )),
    );

export const deletePackageVersion$ =
    (action$: ActionsObservable<Action<DeletePackageVersionConfig>>, _store: Store<AppState>) =>
        action$.ofType(srcVersionsActions.DELETE_PACKAGE_VERSIONS).pipe(
            map(action => action.payload),
            concatMap(deletePackageVersionConfig =>
                packageVersionsWebapi.deletePackageVersion(deletePackageVersionConfig).pipe(
                    map((packageVersionId: string) => {
                        return srcVersionsActions.deletePackageVersionOk(packageVersionId);
                    }),
                    catchError(error => of(srcVersionsActions.deletePackageVersionFail(error))),
                )),
        );

export const createPackageVersion$ = (
    action$: ActionsObservable<Action<CreatePackageVersionCfg>>, _store: Store<AppState>,
) =>
    action$.ofType(srcVersionsActions.CREATE_PACKAGE_VERSIONS_REQ).pipe(
        map(action => action.payload),
        concatMap(packageVersion => packageVersionsWebapi.createPackageVersion(packageVersion).pipe(
            map((_resp) => {
                return srcVersionsActions.createPackageVersionOk(_resp.packageVersion);
            }),
            catchError(error => of(srcVersionsActions.createPackageVersionFail(error))),
        )),
    );

export const deletePackageVersionImage$ = (
    action$: ActionsObservable<Action<DeletePackageVersionImage>>, _store: Store<AppState>
) =>
    action$.ofType(srcVersionsActions.DELETE_PACKAGE_VERSION_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(deletePackageVersionImage => packageVersionsWebapi.deletePackageVersionImage(deletePackageVersionImage).pipe(
            map((resp) => {
                return srcVersionsActions.deletePackageVersionImageOk(resp);
            }),
            catchError(error => of(srcVersionsActions.deletePackageVersionFail(error))),
        ))
    );

export const addVersionDependency$ = (action$: ActionsObservable<Action<AddVersionDependencyReq>>, _store: Store<AppState>) =>
    action$.ofType(srcVersionsActions.ADD_VERSION_DEPENDENCY_REQ).pipe(
        map(action => action.payload),
        concatMap(addDependencyReq => packageVersionsWebapi.addPackageVersionDependency(addDependencyReq).pipe(
            map(resp => {
                /** Notify the user */
                createToastr({
                    type: TOASTR_TYPE.SUCCESS,
                    message: 'Dependency added',
                });

                return srcVersionsActions.addVersionDependencyOk(resp);
            }),
            catchError(error => {
                /** Notify the user */
                createToastr({
                    type: TOASTR_TYPE.ERROR,
                    message: 'Dependency add failed. Please try again.',
                });

                return of(srcVersionsActions.addVersionDependencyFail(error));
            }),
        ))
    );
