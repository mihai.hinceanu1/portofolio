import { AppState } from '../../../shared/interfaces/app.state';
import { Action, Paginated } from '../../../shared/interfaces/shared';
import { TOASTR_TYPE } from '../../../shared/interfaces/toastr';
import { createToastr } from '../../../shared/services/shared.service';
import { ICodePackage, UploadPackageImageConfig } from '../../interfaces/packages';
import { DeletePackageConfig } from '../../interfaces/source-code';
import * as packagesWebapi from '../../webapis/packages.webapi';
import * as packagesActions from '../actions/packages.actions';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

// ====== PACKAGES ======

export const getAllPackages$ = (action$: ActionsObservable<Action<Paginated>>, _store: Store<AppState>) =>
    action$.ofType(packagesActions.GET_PACKAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(() => packagesWebapi.getAllPackages().pipe(
            map(packages => packagesActions.getAllPackagesOk(packages)),
            catchError(error => of(packagesActions.getAllPackagesFail(error))),
        )),
    );

export const createPackage$ = (action$: ActionsObservable<Action<ICodePackage>>, _store: Store<AppState>) =>
    action$.ofType(packagesActions.CREATE_PACKAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(packageVersion => packagesWebapi.createPackage(packageVersion).pipe(
            map((codePackage: ICodePackage) => packagesActions.createPackageOk(codePackage)),
            catchError(error => of(packagesActions.createPackageFail(error))),
        )),
    );

export const editPackage$ = (action$: ActionsObservable<Action<ICodePackage>>, _store: Store<AppState>) =>
    action$.ofType(packagesActions.EDIT_PACKAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(request => packagesWebapi.editPackage(request).pipe(
            map((codePackage: ICodePackage) => {
                return packagesActions.editPackageOk(codePackage);
            }),
            catchError(error => of(packagesActions.editPackageFail(error))),
        )),
    );

export const deletePackage$ = (action$: ActionsObservable<Action<DeletePackageConfig>>, _store: Store<AppState>) =>
    action$.ofType(packagesActions.DELETE_PACKAGE).pipe(
        map(action => action.payload),
        concatMap(deletePackageConfig => packagesWebapi.deletePackage(deletePackageConfig.packageId).pipe(
            map((packageId: string) => {
                return packagesActions.deletePackageOk(packageId);
            }),
            catchError(error => of(packagesActions.deletePackageFail(error))),
        )),
    );

// ====== UPLOAD IMAGE ======

export const uploadImage$ = (action$: ActionsObservable<Action<UploadPackageImageConfig>>, _store: Store<AppState>) =>
    action$.ofType(packagesActions.UPLOAD_PACKAGE_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(config => packagesWebapi.uploadImage(config).pipe(
            map((updatedPackage: ICodePackage) => {

                // notify user
                createToastr({
                    type: TOASTR_TYPE.SUCCESS,
                    message: 'Image uploaded Successfully',
                });

                return packagesActions.uploadImageOk(updatedPackage);
            }),
            catchError(error => of(packagesActions.uploadImageFail(error))),
        )),
    );