import { Action } from '../../../shared/interfaces/shared';
import * as dependenciesWebapi from '../../webapis/dependencies.webapi';
import * as dependenciesActions from '../actions/dependencies.actions';
import { AppState } from 'react-native';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getDependencies$ = (action$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    action$.ofType(dependenciesActions.GET_DEPENDENCIES_REQ).pipe(
        map(action => action.payload),
        concatMap(() => dependenciesWebapi.getDependencies().pipe(
            map(dependencies => dependenciesActions.getDependenciesOk(dependencies)),
            catchError(error => of(dependenciesActions.getDependenciesFail(error))),
        ))
    );

export const getVersionDependencies$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(dependenciesActions.GET_VERSION_DEPENDENCIES_REQ).pipe(
        map(action => action.payload),
        concatMap(pathName => dependenciesWebapi.getVersionDependencies(pathName).pipe(
            map(dependencies => dependenciesActions.getVersionDependenciesOk(dependencies)),
            catchError(error => of(dependenciesActions.getVersionDependenciesFail(error))),
        ))
    );