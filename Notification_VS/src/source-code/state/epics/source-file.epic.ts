import { AppState } from '../../../shared/interfaces/app.state';
import { Action } from '../../../shared/interfaces/shared';
import { AddSourceFile, UpdateSrcCode } from '../../interfaces/source-file';
import { getSourceFilesTree } from '../../services/source-files.service';
import * as sourceFilesWebApi from '../../webapis/source-files.webapi';
import * as sourceFilesActions from '../actions/source-files.actions';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getSourceFilesByPackageVersionId$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(sourceFilesActions.GET_SOURCE_FILES_BY_PACKAGE_VERSION_ID_REQ).pipe(
        map(action => action.payload),
        concatMap(versionId => sourceFilesWebApi.getSourceFilesByPackageVersionId(versionId).pipe(
            map((sourceFiles) => sourceFilesActions.getSourceFilesByPackageVersionIdOk(sourceFiles)),
            catchError(error => of(sourceFilesActions.getSourceFilesByPackageVersionIdFail(error))),
        ))
    );

export const getSourceFilesTree$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(sourceFilesActions.GET_SOURCE_FILES_TREE_REQ).pipe(
        map(action => action.payload),
        concatMap(versionId => sourceFilesWebApi.getSourceFileTreeLevel(versionId).pipe(
            map((sourceFileTree) => {
                return sourceFilesActions.getSourceFilesTreeOk(sourceFileTree);
            }),
            catchError(error => of(sourceFilesActions.getSourceFilesTreeFail(error)))
        ))
    );

export const addSourceFile$ = (action$: ActionsObservable<Action<AddSourceFile>>, _store: Store<AppState>) =>
    action$.ofType(sourceFilesActions.ADD_SOURCE_FILE_REQ).pipe(
        map(action => action.payload),
        concatMap(addSourceFileReq => sourceFilesWebApi.addSourceFile(addSourceFileReq).pipe(
            map((pathname: string) => {
                getSourceFilesTree(pathname);
                return sourceFilesActions.addSourceFileOk();
            }),
            catchError(error => of(sourceFilesActions.addSourceFileFail(error)))
        ))
    );

export const updateSourceFileCode$ = (action$: ActionsObservable<Action<UpdateSrcCode>>, _store: Store<AppState>) =>
    action$.ofType(sourceFilesActions.UPDATE_SOURCE_FILE_CODE_REQ).pipe(
        map(action => action.payload),
        concatMap(updateSrcCode => sourceFilesWebApi.updateSourceFileSrcCode(updateSrcCode).pipe(
            map((pathname: string) => {
                getSourceFilesTree(pathname);
                return sourceFilesActions.updateSourceFileCodeOk();
            }),
            catchError(error => of(sourceFilesActions.updateSourceFileCodeFail(error)))
        ))
    )