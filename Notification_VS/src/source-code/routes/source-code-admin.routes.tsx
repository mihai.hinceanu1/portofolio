import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { CodeEditorPage } from '../pages/code-editor-page/code-editor.page';
import { PackageVersionsPage } from '../pages/package-versions-page/package-versions.page';
import { PackagesListPage } from '../pages/packages-list/packages-list.page';
import * as React from 'react';
import { Route } from 'react-router';
// import { SourceCodeAdminPage } from '../pages/source-code-admin-page/source-code-admin.page';

/** Source Code Admin Routes */
export const sourceCodeAdminRoutes = (loginResponse: LoginResponse, key: string) => {
    return [
        // <Route path='/admin/source-code/:packagePath' key={key}
        //     render={props => renderPage(props, true, '/auth/login', SourceCodeAdminPage, loginResponse)} />,

        <Route path='/admin/packages-list' key={key}
            render={props => renderPage(props, true, '/auth/login', PackagesListPage, loginResponse)} />,

        <Route path='/admin/package-versions/:pathName' key={key}
            render={props => renderPage(props, true, '/auth/login', PackageVersionsPage, loginResponse)} />,

        <Route path='/admin/code-editor/:pathName' key={key}
            render={props => renderPage(props, true, '/auth/login', CodeEditorPage, loginResponse)} />
    ];
};