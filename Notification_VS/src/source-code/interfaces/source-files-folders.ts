import { CommonMetadata } from '../../shared/interfaces/common-metadata';

export interface SourceFileFolder extends CommonMetadata {
    pathName: string;
    packageVersionId: string;
    name: string;
    parentId: string;
}
