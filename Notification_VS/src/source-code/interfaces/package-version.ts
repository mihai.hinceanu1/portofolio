import { SourceFileTree } from './source-file-tree';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';
import { FileInput } from '../../shared/interfaces/file-input';

/**
 * A snapshot of the source files at a certain stage of the evolution.
 * Ex: Date picker Y Adding navigation buttons
 * For admin purposes we can list the versions without loading the files themselves (reduced payload ).
 */

export interface UploadPackageVersionImageResponse {
    imgPath: string;
    packageVersionId: string;
}

export interface EditPackageVersionImageResponse {
    pathToReplace: string;
    packageVersionId: string;
    imgPath: string;
}

export interface PackageVersion extends CommonMetadata {
    pathName: string;
    name: string;
    description: string;
    version: string;
    packageId: string;
    images: string[];
    dependencies: string[];
}

// DEPRECATE or REFACTOR
export interface PackageVersionExtended extends CommonMetadata {
    pathName: string;
    name: string;
    description: string;
    version: string;
}

// REVIEW
export interface PackageVersionTree extends PackageVersion {
    treeFiles: SourceFileTree[];
}

export interface CreatePackageVersionCfg {
    name: string;
    description: string;
    packagePathName: string;
}

export interface AddPackageVersionImage {
    versionId: string;
    file?: FileInput;
    imageUrl?: string;
}

export interface EditPackageVersionImage {
    versionId: string;
    file?: FileInput;
    imageUrl?: string;
    imageToReplacePath: string;
}

export interface UpdatePackageVersion {
    versionId: string;
    description: string;
    name: string;
}

export interface DeletePackageVersionImage {
    versionId: string;
    imgPath: string;
}

export interface AddVersionDependencyReq {
    pathName: string;
    dependencyPathName: string;
}