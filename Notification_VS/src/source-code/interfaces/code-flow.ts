import { CodeSnippetExtended } from './code-snippet';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';

export interface CodeFLow extends CodeFlowCommon {
    codeSnippets: CodeSnippetExtended[];
}

export interface CodeFlowCommon extends CommonMetadata {}

export interface CodeFlowDto extends CodeFlowCommon {
    codeSnippets: string[];
}

// ====== DEPRECATE ======

// MOVE to source code
export interface CodeFlowIds {
    _id: string;
    codeSnippetsIds: string[];
}

// REVIEW Why do we need this interface? Consider removing.
export interface CodeFLowOld {
    _id: string;
    codeSnippets: CodeSnippetExtended[];
}