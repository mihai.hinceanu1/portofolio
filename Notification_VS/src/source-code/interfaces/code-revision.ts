import { CodeSnippetExtended } from './code-snippet';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';

export interface CodeRevision extends CodeRevisionCommon {
    codeSnippets: CodeSnippetExtended[];
}

export interface CodeRevisionCommon extends CommonMetadata {}

export interface CodeRevisionDto extends CodeRevisionCommon {
    codeSnippets: string[];
}