import { PackageVersion } from '../package-version';

export interface PackageVersionsState {
    packageVersions: PackageVersion[];
}