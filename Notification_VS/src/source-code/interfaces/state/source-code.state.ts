import { DependenciesState } from './dependencies.state';
import { PackageVersionsState } from './package-versions.state';
import { PackagesState } from './packages-state';
import { SourceCodeCommonState } from './source-code-common.state';
import { SourceFileState } from './source-file.state';
// import { PackageVersion } from './package-version';
// import { ICodePackage } from './packages';
// import { UploadPackageImageResponse } from './source-code';
// import { SourceFile } from './source-file';
// import { SourceFileTree } from './source-file-tree';
/** TODO Refactor, separate admin section */
export interface SourceCodeState {
    common: SourceCodeCommonState;

    dependenciesState: DependenciesState;

    packagesState: PackagesState;

    packageVersions: PackageVersionsState;

    sourceFile: SourceFileState;

    /** Used in web app and admin. */
    // packageVersions: PackageVersion[];

    // sourceFileTree: SourceFileTree[];

    // sourceFiles: SourceFile[];
    // admin: {
    /** Toggle Admin Navigator */
    // showAdminNavigator: boolean;
    // };
}
