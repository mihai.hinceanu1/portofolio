import { IDependency } from '../floating-menu';

export interface DependenciesState {
    /** All technologies which can be added as dependencies. */
    dependencies: IDependency[];

    /** The dependencies for a version. */
    versionDependencies: IDependency[];
}