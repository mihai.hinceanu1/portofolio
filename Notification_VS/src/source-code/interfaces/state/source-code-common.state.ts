/**
 * Share data between every page from source-code module
 */
export interface SourceCodeCommonState {
    /** Toggle Admin Navigator */
    showAdminNavigator: boolean;
}
