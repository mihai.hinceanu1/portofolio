import { SourceFileTree } from '../source-file-tree';

export interface SourceFileState {
    tree: SourceFileTree[];
}