import { ICodePackage } from '../packages';
import { UploadPackageImageResponse } from '../source-code';

export interface PackagesState {
    packages: ICodePackage[];

    admin: {
        uploadPackageImage: UploadPackageImageResponse;
    };
}