export interface SourceFileTree {
    sourceFileTreeLevels: SourceFileTreeLevel[];
}

export interface SourceFileTreeLevel {
    _id?: string;
    packageVersionId: string;
    name: string;
    children: SourceFileTreeLevel[];
    code?: string;
    isRoot: boolean;
    parent?: string;
    description?: string;
    isFolder: boolean;
}