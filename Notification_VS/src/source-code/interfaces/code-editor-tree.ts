import { SourceFileTreeLevel } from './source-file-tree';

export interface ICodeEditorTree {
    levels: SourceFileTreeLevel[];
    
    collapseIcon?: string;

    /** Icon for expanding levels */
    expandIcon?: string;

    /** Additional functionality when any level expands */
    onLevelExpand?: (level?: SourceFileTreeLevel) => void;

    /** Additional functionality when any level collapses */
    onLevelCollapse?: (level?: SourceFileTreeLevel) => void;

    /** Custom trigger when level click */
    onLevelClick?: (level: SourceFileTreeLevel) => void;
}