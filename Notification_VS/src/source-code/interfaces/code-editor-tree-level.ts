import { SourceFileTree } from './source-file-tree';

export interface CodeEditorTreeLevel {
    _id: string;
    name: string;
    isRoot: boolean;
    icon: string;
    parent: string;
    isFolder: boolean;
    children: SourceFileTree[];
    sourceCode?: string;
}