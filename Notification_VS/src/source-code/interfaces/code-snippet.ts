import { SourceFile } from './source-file';
import { INanolesson } from '../../nano-lessons/interfaces/nano-lessons';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';
import { TopicSentiment } from '../../topics/interfaces/topic-sentiment';

/**
 * A code snippet is a selection of code from a source file.
 * Children topics can pick arbitrary code sections for annotating them with explanations.
 * One topic can declare multiple code snippets if it requires (for example from multiple files).
 */
export interface CodeSnippet extends CommonMetadata {
    pathName: string;
    name: string;
    description: string;
    sourceFileId: string;

    /** Starting and ending indexes from where the code snippet is trimmed from the source file. */
    position: CodePosition;
}

export interface CodePosition {
    start: number;
    end: number;
}

/**
 * Contains the sourceFile complete object nested inside
 * REVIEW I don't believe it's ok to have the source file between multiple code snippets from the same file
 */
export interface CodeSnippetExtended extends CommonMetadata {
    pathName: string;
    name: string;
    description: string;

    /** <!> Complete Source File */
    sourceFile: SourceFile;

    /** Starting and ending indexes from where the code snippet is trimmed from the source file. */
    position: {
        start: number;
        end: number;
    };
}

export interface CodeSnippetMarker {
    nanoLesson: INanolesson;
    comments: Comment;
    sentiment: TopicSentiment;
}

export interface CodeSnippetMarkersDto  {
    nanoLesson: string;
    comments: string;
    sentiment: string;
}