import { PackageVersion, PackageVersionExtended, PackageVersionTree } from './package-version';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';
import { FileInput } from '../../shared/interfaces/file-input';
import { MILESTONES } from '../services/packages-list.utils';

/**
 * A source files package is almost a mini git repository,
 * it has several versions of the same file sets.
 * This is useful to trace the history of a project's files.
 * From source files authors can extract code snippets and associate them with code blocks.
 * Eventually this feature might evolve to the point where actual git repos can be used.
 * For admin purposes we can list the packages without loading the files themselves (reduced payload ).
 */
export interface ICodePackage extends CommonMetadata {
    pathName: string;
    name: string;
    description: string;
    image: string;
    awards: number;
    reactions: number;
    /** Editor's Choice, Highest Rated, Bestseller, etc. */
    milestone: MILESTONES;
    comments: number;
    newComments: number;
    codeLines: number;
    /** @TODO Tech stack of max 3 length (primary techs, most important)
     * On PackageVersion can exist any number of technologies.
     */
    technologyStack: Technology[];
    deprecated: boolean;
}

/**
 * Flat bundle with all the required assets for a code package.
 * Lesson aggregation needs this format for simplicity and maintenance.
 */
export interface CodePackageDict {
    packages: ICodePackage[];
    versions: PackageVersion[];
}

// DEPRECATE or REFACTOR
export interface PackageExtended extends CommonMetadata {
    pathName: string;
    name: string;
    description: string;
    versions: PackageVersionExtended[];
}

// DEPRECATE
export interface PackageTree extends ICodePackage {
    packageVersions: PackageVersionTree[];
}

/**
 * Used for Code package technology stack.
 */
export interface Technology {
    name: string;
    icon: string;
}

/** Obj model for creating a new package. */
export interface NewPackageConfig {
    name: string;
    description: string;
}

/** Config for uploading an image and attaching it to a package */
export interface UploadPackageImageConfig {
    image: FileInput;
    packageId: string;
}