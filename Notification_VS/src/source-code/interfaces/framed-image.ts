/**
 * Frame Image Config used to
 * render framed image - fileTypeSvgFill
 * and title color are not mandatory
 */
export interface FramedImageCfg {

    /** Image Path from Image rendered inside frame */
    imagePath: string;

    /**
     * Svg rendered in bottom right
     * corner of the component to highlight the file
     */
    fileTypeSvg: string;

    /** Color for fileTypeSvg */
    fileTypeSvgFill?: string;

    /**
     * Title which highlights what
     * source file framed image represents
     */
    title: FramedTitleCfg;

    editButton?: boolean;
}

export interface FramedTitleCfg {

    /** Title text */
    name: string;

    /** Icon - represents used language */
    iconPath: string;

    /** Color of the title and icon */
    titleColor?: string;
}