/**
 * Config used to render stage card
 * callback and assets are not mandatory
 * Stage card will be used to highlight
 * in which stage of development is a user project
 */
export interface StageCardCfg {

    /** Title used to highlight the name of a specific stage */
    title: string;

    /** Describes a specific stage of the project */
    description: string;

    /** If the card is focused or not */
    isActive: string;

    /** Function called when the card is pressed */
    callback?: () => void;

    /** Array of assets displayed at the bottom of card
     * in order to display each technology used in
     * development process of the project
     */
    assets?: StageCardAssets[];
}

/** Asset config - Path to draw - Color used to fill the drawn path */
export interface StageCardAssets {
    path: string;
    fill?: string;
}