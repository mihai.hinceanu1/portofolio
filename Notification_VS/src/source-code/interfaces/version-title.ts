/**
 * Every prop is mandatory
 * Composes a title and a description
 * in order to give a short preview for the user
 * on the file version uploaded
 */
export interface VersionTitleCfg {

    /** Version is in final state or not */
    isLocked: boolean;

    /** Is the last version uploaded or not */
    isLast: boolean;

    /** If it is hovered the lock will be shown */
    // hovered: boolean;

    /** Title text representing the the version file title  */
    title: string;

    /** Description - short description for the version file */
    description: string;

    /** Index of the version uploaded */
    noOfVersion: number;
    relatedLessons?: string[];
}