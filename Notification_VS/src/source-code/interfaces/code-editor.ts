import { SourceFile } from './source-file';

export interface ITopicEditor {
    codeSnippetID: string;
    name: string;
    code: string;
    sourceFile?: SourceFile;
    position?: {
        start: number;
        end: number;
    };
    description: string;
}