import { FramedImageCfg } from './framed-image';
import { StageCardCfg } from './stage-card';
import { VersionPreviewImageCfg } from './version-preview-image';
import { VersionTitleCfg } from './version-title';
import { IStatsBar } from '../../shared/interfaces/stats-bar';
import { ITimelineProps } from '../components/timeline-bar/timeline-bar';
import { IVersionNavbarProps } from '../components/version-navbar/version-nav-bar';

/**
 * Interface representing the state of packages version page,
 * the state will be used as an array of CodeVersionCardCfg
 * A Card represents a CodeVersion on the timeline - each card
 * has a body and its props are described in CodeVersionCardBodyCfg
 * In body will be rendered the most info about a code version
 * Each key is mandatory, excepting the versionNavbarCfg
 * because Navbar has default props.
 */
export interface CodeVersionCardCfg {

    /** Stage Cards Config - Array */
    stageCards: StageCardCfg[];

    /**
     * Timeline - the first one
     * that takes part from the first card
     * should have isFirst prop on true
     */
    timelineBarCfg: ITimelineProps;

    /**
     * It will render all the info about of a code version
     * It is formed from VersionTitle, VersionNavbar, StatsBar
     * FramedImage(Array), VersionPreviewImage(Array)
     */
    codeVersionCardBodyCfg: CodeVersionCardBodyCfg;
    
}

export interface CodeVersionCardBodyCfg {

    /** Used to identify the parent package of the current code version */
    packageId: string;

    /** Used to identify which package version is rendered in the current code version card */
    packageVersionId: string;

    /** Version Title Cfg - highlights why the version exists */
    versionTitleCfg: VersionTitleCfg;

    /** Used to filter the source file - Version Navbar Cfg */
    versionNavbarCfg?: IVersionNavbarProps;

    /** Used to see notifications about a source file */
    statsBarCfg: IStatsBar;

    /**
     * Framed Image Cfg - A snippet on
     * a key part of code from the source file
     */
    framedImagesCfg: FramedImageCfg[];

    /**
     * A key snippet for the source file
     */
    versionPreviewImagesCfg: VersionPreviewImageCfg[];

    /**
     * Additional description used to give completion
     * on the basic description of the version title
     * It's not mandatory (used only if needed)
     */
    additionalDescription?: string;

    pathName: string;
}