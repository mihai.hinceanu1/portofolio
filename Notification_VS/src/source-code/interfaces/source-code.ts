import { CodeSnippet } from './code-snippet';

/**
 * Dictionary containing all code snippets from all higher order business objects.
 * This format is preferred by the lesson endpoint for easier maintenance.
 */
export interface CodeSnippetsDict {
    flows: CodeSnippet[];
    revisions: CodeSnippet[];
}

// REVIEW
export interface SourceCodeForDelete {
    [key: string]: DeletePackageConfig | DeletePackageVersionConfig | DeleteSourceFileConfig | DeleteCodeSnippetConfig;
    package: DeletePackageConfig;
    packageVersion: DeletePackageVersionConfig;
    sourceFile: DeleteSourceFileConfig;
    codeSnippet: DeleteCodeSnippetConfig;
}

// REVIEW
export interface ItemForDeleteConfig {
    type: 'package' | 'packageVersion' | 'sourceFile' | 'codeSnippet';
    item: DeletePackageConfig | DeletePackageVersionConfig | DeleteSourceFileConfig | DeleteCodeSnippetConfig;
}

// REVIEW
export interface AddCodeSnippetConfig {
    name: string;
    description: string;
    start: string;
    end: string;
    addCodeSnippetFileId: string;
    topicId: string;
}

export interface AddCodeSnippetToSourceFile {
    name: string;
    description: string;
    start: string;
    end: string;
    addCodeSnippetFileId: string;
}

// REVIEW
export interface FileConfig {
    name: string;
    description: string;
    mode: string;
    code: string;
    versionId: string;
    topicId: string;
}

// REVIEW
export interface PackagesVersionConfig {
    name: string;
    description: string;
    packageId: string;
    topicId: string;
}

// REVIEW
export interface PackagesConfig {
    name: string;
    description: string;
    topicId: string;
}

// REVIEW
export interface DeletePackageConfig {
    packageId: string;

    // Parent topic id
    // topicId: string;
}

export interface DeletePackageVersionConfig {
    packageVersionId: string;
    packageId: string;
}

// REVIEW
export interface DeleteSourceFileConfig {
    fileId: string;
    packageVersionId: string;

    // Parent topic id
    topicId: string;
}

// REVIEW
export interface DeleteCodeSnippetConfig {
    codeSnippetId: string;

    // Does not need parent file id as it has it within

    // Parent topic id
    topicId: string;
}

export interface UploadPackageImageResponse {
    response: string;
    error: string;
}