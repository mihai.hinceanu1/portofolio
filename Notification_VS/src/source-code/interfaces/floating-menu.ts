export interface FloatingMenuCfg {
    body: FloatingMenuBodyCfg;
    tabs: FloatingMenuTabCfg[];
    addDependency?: () => void;
}

export interface FloatingMenuTabCfg {
    asset: string;
    tabText: FLOATING_MENU_TABS;
}

export enum FLOATING_MENU_TABS {
    DEPENDENCIES = 'Dependencies',
    SNIPPETS = 'Snippets',
    FLOWS = 'Flows',
    REVISIONS = 'Revisions',
    PSEUDOCODE = 'Pseudocode',
    ANNOTATIONS = 'Annotations',
    SHOWCASE = 'Showcase',
}

export interface FloatingMenuBodyCfg {
    dependencies: IDependency[];
    snippets: ISnippet[];
    flows: IFlow[];
    revisions: IRevision[];
    pseudocode: IPseudocode[];
    annotations: IAnnotation[];
    showcases: IShowcase[];
}

export interface IShowcase {
    title: string;
    file: string;
    image?: string;
}

export interface IAnnotation {
    title: string;
    description: string;
    lines: number[];
    column: number;
}

export interface IRevision {
    title: string;
    description: string;
}

export interface IPseudocode {
    filename: string;
}

export interface IDependency {
    pathName: string;
    name: string;
    description: string;
    versions: string[];
    monthlyDownloads: number;
    image: string;
}

export interface ISnippet {
    title: string;
    description: string;
    codeLines: number[];
}

export interface IFlow {
    title: string;
    description: string;
    numberOfSnippets: number;
}
