export interface VersionPreviewImageCfg {

    /** Each preview might have a title, something representative */
    title?: string;

    /** Image that is centered in the container */
    imgPath: string;

    /**
     * Small svg icon representing the type of the image
     * Varieties included (App, Page, Module, Server App etc.)
     */
    icon: SVG;
}

// ===== PRIVATE =====

interface SVG {
    path: string;
    fill?: string;
}