import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/** Source files contain the code from which snippets are extracted. */
export interface SourceFile extends CommonMetadata {
    pathName?: string;
    name: string;
    packageVersionId: string;
    description: string;
    parentId: string;
    code: string;
}

export interface AddSourceFile {
    addSourceFile: AddSourceFileBody;
    packageVersionPathName: string;
}

export interface AddSourceFileBody {
    name: string;
    packageVersionId: string;
    parentId: string;
    code?: string;
}

export interface UpdateSrcCode {
    code: string;
    packageVersionPathName: string;
    srcFileId: string;
}