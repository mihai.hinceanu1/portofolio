import { TILE } from '../constants/map-world.const';
import * as React from 'react';
import { G, Path } from 'react-native-svg';
// import { Group, Shape } from '@react-native-community/art';

interface Props { }

/** Contour of a TILE from the floor of the map */
export const TileContourEntity: React.StatelessComponent<Props> = (/* props: Props */) => {
    return (
        /* Entity */
        /* In relation to the entity origin, we move the graphical assets */
        <G x={-TILE.width / 2 - 1.2} y={-TILE.height / 2 - 0.85}>

            {/* Render geometry */}
            {/* <Shape d={path} fill={graphData.fill || graphic.fill || 'black'} /> */}
            <Path d={drawTileContourPath()} fill={'blue'} />

        </G>
    );
};

/** Tile contour is a basic shape that renders a simple contour around a tile. */
export const drawTileContourPath = () => 'M0.006,20.156 L51.350,0.115 L102.766,20.152 L51.421,40.193 L0.006,' +
    '20.156 ZM51.352,1.188 L2.756,20.156 L51.420,39.121 L100.016,20.152 L51.352,1.188 Z';