import { GraphDimensions } from '../../graphs/interfaces/graphs';

/** <!> These values define our isometric ratio */
export const TILE: GraphDimensions = {
    width: 100,
    height: 39,
    image: '/maps/isometric-tile.png',
};