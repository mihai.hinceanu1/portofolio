import { GraphDimensions, GraphPosition } from '../../graphs/interfaces/graphs';
import { IsoPosition } from '../../maps/interfaces/maps';

/**
 * Converts isometric space coordinates (x,y,z) to screen coordinates (x,y).
 * Be aware that all objects should have the origin defined such that
 * the alignment with the grid actually makes sense.
 * For example, buildings should have the origin defined
 * in the center of bottom "rectangle face".
 */
export function mapIsometricPositionToScreenPosition(
    tileDimensions: GraphDimensions, isoPosition: IsoPosition,
): GraphPosition {
    let { height, width } = tileDimensions;
    let { x, y, z } = isoPosition;

    // Transform isometric coordinates => screen coordinates
    // +y => -y
    // +x => +x, -y
    // +z => +x, +y
    let position = {
        x: (
            // + (width / 2) // REVIEW this together with no origin offset rendered the tile at x1 y1 instead of x0 y0
            + (x * width / 2) // +x => +x, -y
            + (z * width / 2)// +z => +x, +y
        ),
        y: (
            // + (height / 2)
            - (y * height) // +y => -y
            - (x * height / 2) // +x => +x, -y
            + (z * height / 2) // +z => +x, +y
        ),
    };

    return position;
}