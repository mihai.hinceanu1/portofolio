
// import { mapIsometricPositionToScreenPosition } from './isometric.utils';
import { GraphPosition } from '../../graphs/interfaces/graphs';
import { MainBranchLine } from '../../maps/entities/category-branch/main-branch-line.entity';
import { TopicsCluster } from '../../maps/entities/topics-cluster.entity';
import { MapEntity, MapScene } from '../../maps/interfaces/entity';
import { mapIsometricPositionToScreenPosition } from './isometric.utils';
import { TileContourEntity } from '../entities/tile-contour.entity';
import * as React from 'react';
import { G, Path, Text } from 'react-native-svg';
import { TILE } from '../constants/map-world.const';
import { COORDINATES_DEBUGGING_TEXT_SIZE } from '../../maps/constants/global-map.const';
import { ENTITY_TYPE } from '../../maps/constants/entities-classes.const';

let development = false; // TODO Connect to real env vars

/**
 * Convert data entities to react-native-svg.
 * This is the actual graphical result on screen.
 * The entity.type property is parsed by the converter and it renders
 * the necessary graphical assets as svg shapes and images.
 * Depending on entity type, one or more graphical assets can be rendered in one group.
 */
export function mapSceneEntitiesToSVG(scene: MapScene, pan: GraphPosition): JSX.Element[] {
    let artCmps: JSX.Element[] = [];

    // Scene entities
    scene.layers.forEach(layer =>
        layer.entities.forEach(entity =>
            artCmps.push(
                mapEntityToSVG(entity, entity.id, pan),
            ),
        ),
    );

    return artCmps;
}

/**
 * Convert entity data into React elements for Svg.
 * These will be listed in the isometric map.
 */
function mapEntityToSVG(entity: MapEntity<any>, entityID: string, pan: GraphPosition): JSX.Element {

    // Position
    let position = mapIsometricPositionToScreenPosition(TILE, entity.components.position);

    // Add pan
    // <!> Pan to actually see the map (center)
    position.x += pan.x + TILE.width / 2;
    position.y += pan.y + TILE.height / 2;

    // Outer group controls the placement of the origin of the entity
    // This perfectly matches the center point of a tile
    let group =
        <G key={entityID} x={position.x} y={position.y}>
            <React.Fragment>
                {/** Entities */}
                {
                    renderEntityByType(entity.components.body)
                }

                {/** Developer visual origin marker and coordinates entities for easy debugging */}
                {development &&
                    <G x={-1} y={-1}>
                        {/* Origin marker (dev only) */}
                        <Path d={drawOriginMarker()} fill={'red'} opacity={0.5} />
                        {/* Coordinates (dev only) */}
                        {/** IsoMetric Coordinates */}
                        <Text x={0} y={0}
                            font={{
                                fontFamily: 'Helvetica Neue',
                                fontSize: COORDINATES_DEBUGGING_TEXT_SIZE,
                            }
                                // `${COORDINATES_DEBUGGING_TEXT_SIZE}px "Helvetica Neue", "Helvetica", Arial`
                            }
                            fill='black'
                            opacity={0.4}
                        // alignment='center'
                        >
                            x:{entity.components.position.x}&nbsp;
                            y:{entity.components.position.y}&nbsp;
                            z:{entity.components.position.z}&nbsp;
                        </Text>
                        {/** Screen Coordinates */}
                        <Text x={0} y={-10}
                            font={{
                                fontFamily: 'Helvetica Neue',
                                fontSize: COORDINATES_DEBUGGING_TEXT_SIZE,
                            }
                                // `${COORDINATES_DEBUGGING_TEXT_SIZE}px "Helvetica Neue", "Helvetica", Arial`
                            }
                            fill='black'
                            opacity={0.4}
                        // alignment='center'
                        >
                            x: {mapIsometricPositionToScreenPosition(TILE, entity.components.position).x}&nbsp;
                            y: {mapIsometricPositionToScreenPosition(TILE, entity.components.position).y}&nbsp;
                        </Text>

                    </G>}

            </React.Fragment>
        </G>;

    return group;
}

function renderEntityByType(body: any): JSX.Element {
    let entityCmp: JSX.Element;
    switch (body.type) {
        case ENTITY_TYPE.TileContour: {
            entityCmp = <TileContourEntity />;
            break;
        }

        case ENTITY_TYPE.MainBranch: {
            entityCmp = <MainBranchLine body={body} />;
            break;
        }

        // Topics Cluster contains Project Tags already
        case ENTITY_TYPE.TopicsCluster: {
            entityCmp = <TopicsCluster body={body} />;
            break;
        }

        // case ENTITY_TYPE.ProjectTags: {
        //     entityCmp = <ProjectTags body={body} />;
        //     break;
        // }
    }
    return entityCmp;
}

/** Tile contour is a basic shape that renders a simple contour around a tile. */
const drawOriginMarker = () =>
    `M3.000,2.000
    L2.000,2.000
    L2.000,3.000
    L1.000,3.000
    L1.000,2.000
    L-0.000,2.000
    L-0.000,1.000
    L1.000,1.000
    L1.000,-0.000
    L2.000,-0.000
    L2.000,1.000
    L3.000,1.000
    L3.000,2.000
    Z`;