import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { NanolessonsAdminPage } from '../pages/nanolessons-admin/nanolessons-admin.page';
import { SaveNanolessonPage } from '../pages/save-nanolesson/save-nanolesson.page';
import * as React from 'react';
import { Route } from 'react-router';

/** Nanolessons Admin Routes */
export const nanolessonsAdminRoutes = (loginResponse: LoginResponse, key: string) => {
    return [
        <Route path='/admin/nanolessons' key={key} exact={true}
            render={props => renderPage(props, true, '/auth/login', NanolessonsAdminPage, loginResponse)} />,

        <Route path='/admin/nanolessons/save' key='nanolesson-create' exact={true}
            render={props => renderPage(props, true, '/auth/login', SaveNanolessonPage, loginResponse)} />,
    ];
};
