import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';
import styled from 'styled-components/native';

interface ImageSectionProps extends ReactWebAttributes {
    hasPreview?: boolean;
}

export const ImageSection = styled.View<ImageSectionProps>`
    min-height: ${props => getMinHeight(props)};
`;

export const Preview = styled.View``;

export const Image = styled.Image`
    width: 100%;
    min-height: 100px;
    height: auto;
`;

export const Triggers = styled.View`
    align-items: center;
    justify-content: center;
    flex-direction: row;
    margin-top: 10px;
`;

export const Button = styled.TouchableOpacity`
    margin-left: 10px;
    margin-right: 10px;
`;

// ===== OVERRIDES =====

export const BUTTON_OVERRIDES = (difficulty: NanolessonDifficulty) => ({
    root: `
        border-width: 1px;
        width: 145px;
        height: 30px;
        border-color: ${getColorByDifficulty(difficulty)};
        align-self: center;
    `,
    text: `
        color: ${getColorByDifficulty(difficulty)};
    `,
});

// ===== UTILS =====
function getMinHeight(props: ImageSectionProps) {
    if (props.hasPreview) {
        return `130px`;
    } else {
        return `40px`;
    }
}
