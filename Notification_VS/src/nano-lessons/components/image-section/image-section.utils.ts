import { ButtonType, IButton } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';

export const ADD_IMAGE = (callback: () => void): IButton => ({
    callback: () => callback(),
    text: '+ Add Image',
    color: colors.$white,
    type: ButtonType.ROUNDED,
});
