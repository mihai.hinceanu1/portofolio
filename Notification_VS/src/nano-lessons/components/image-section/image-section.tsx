import * as div from './image-section.style';
import * as utils from './image-section.utils';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { UploadImage } from '../../../shared/components/upload-image/upload-image';
import { Button } from '../../../shared/components/vsc-button/button';
import { FileInput } from '../../../shared/interfaces/file-input';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import {
    imageModalVisible$,
    setCurrentNanolessonTab,
    toggleImageModal,
    uploadNanoImage,
    uploadNanoImageResponse$
    } from '../../services/nanolessons-admin.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {
    tab: INanolessonTab;
    difficulty: NanolessonDifficulty;
}
interface State {
    image: FileInput;
    isHover: boolean;
}

/**
 * Renders Image section from Nanolesson Tab Content
 * It has a button that opens an UploadImage modal if there is no image
 * or shows the image with update / delete triggers if image is provided
 */
export class ImageSection extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();
    private imageModal: JSX.Element = <UploadImage onClose={() => toggleImageModal(false)}
        onSavePress={(image: FileInput, inputUrl: string) => this.onModalSavePress(image, inputUrl)} />;

    constructor(props: Props) {
        super(props);

        this.state = {
            image: null,
            isHover: false,
        };
    }

    public render() {
        const { isHover } = this.state;
        const { tab, difficulty } = this.props;

        return (
            <div.ImageSection data-cy='image-section'
                hasPreview={!!tab.image}
                onMouseEnter={() => this.toggleHover(true)}
                onMouseLeave={() => this.toggleHover(false)}>

                {/** Add Image */}
                {
                    !tab.image &&
                    <Button {...utils.ADD_IMAGE(() => toggleImageModal(true))}
                        overrides={div.BUTTON_OVERRIDES(difficulty)} />
                }

                {/** Preview */}
                {
                    !!tab.image &&
                    <div.Preview data-cy='preview'>
                        <div.Image data-cy='preview'
                            source={{ uri: tab.image }}
                            resizeMode='contain' />

                        {/** Triggers */}
                        {
                            isHover &&
                            <div.Triggers data-cy='triggers'>
                                <div.Button onPress={() => this.onEditPress()}>
                                    <Icon path={icons.EDIT} color={getColorByDifficulty(difficulty)} />
                                </div.Button>

                                <div.Button onPress={() => this.onDeletePress()}>
                                    <Icon path={icons.CROSS} color={getColorByDifficulty(difficulty)} />
                                </div.Button>
                            </div.Triggers>
                        }

                    </div.Preview>
                }
            </div.ImageSection>
        );
    }

    public componentDidMount() {
        this.subscribeToImageModal();
        this.subscribeToImageUploadResponse();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToImageModal() {
        imageModalVisible$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isVisible => {
                if (isVisible) {
                    addAppOverlay(this.imageModal);
                } else {
                    removeAppOverlay(this.imageModal);
                }
            });
    }

    private onModalSavePress(image: FileInput, inputUrl: string) {
        const { tab } = this.props;

        // if user wants to upload by using URL input
        if (inputUrl) {
            const updatedTab = { ...tab, image: inputUrl };
            setCurrentNanolessonTab(updatedTab);
            toggleImageModal(false);
        } else if (image) {
            uploadNanoImage(image);
        }
    }

    private subscribeToImageUploadResponse() {
        uploadNanoImageResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(image => {
                let { tab } = this.props;
                tab = { ...tab, image };

                setCurrentNanolessonTab(tab);
            });
    }

    private toggleHover(bool: boolean) {
        this.setState({ isHover: bool });
    }

    private onEditPress() {
        toggleImageModal(true);
    }

    private onDeletePress() {
        let { tab } = this.props;

        tab = { ...tab, image: null };
        setCurrentNanolessonTab(tab);
    }
}
