import * as div from './nanolesson.style';
import { INanolesson, INanolessonTab } from '../../interfaces/nano-lessons';
import { NanolessonDetails } from '../nanolesson-details/nanolesson-details';
import { NanolessonTabs } from '../nanolesson-tabs/nanolesson-tabs';
import * as React from 'react';

interface Props {
    nanolesson: INanolesson;
    onClose?: () => void;
    overrides?: {
        root?: string;
    };
}

interface State {
    /** Current Active tab. BY default, the first one is active */
    activeTab: INanolessonTab;
}

/**
 * Nanolesson represents a piece of a lesson that is representative for a selected subject
 * Must be short and very intuitive, and most of the time it must have a link to a resource
 * (that can be internal or external, depending on the case) with additional explanation in case
 * the user doesn't understand the concept explained in the beginning.
 *
 * Offers the possibility to add multiple tabs . Each tab might contain a title, a description, image and link to resource
 */
class _Nanolesson extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            activeTab: props.nanolesson.tabs[0],
        };
    }

    public render() {
        const { nanolesson, overrides, onClose } = this.props;
        const { activeTab } = this.state;

        return (
            <div.Nanolesson data-cy='nanolesson'
                override={overrides && overrides.root}>

                {/** Tabs */}
                <NanolessonTabs tabs={nanolesson.tabs}
                    onTabClick={(tab: INanolessonTab) => this.onTabClick(tab)}
                    activeTab={activeTab}
                    difficulty={nanolesson.difficulty} />

                {/** Details */}
                <NanolessonDetails details={activeTab}
                    onClose={() => onClose()}
                    difficulty={nanolesson.difficulty} />

            </div.Nanolesson>
        );
    }

    private onTabClick(tab: INanolessonTab) {
        this.setState({ activeTab: tab });
    }
}

export const Nanolesson = _Nanolesson;
