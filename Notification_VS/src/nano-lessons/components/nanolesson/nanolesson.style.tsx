import styled from 'styled-components/native';

interface NanolessonProps {
    override?: string;
}

export const Nanolesson = styled.View<NanolessonProps>`
    flex-direction: row;
    align-items: flex-start;
    justify-content: center;
    width: 255px;
    ${props => props.override};
`;