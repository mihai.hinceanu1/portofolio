import { ButtonType, IButton } from '../../../shared/interfaces/button';
import { goTo } from '../../../shared/services/utils.service';
import { colors } from '../../../shared/style/colors';
import { INanoLink } from '../../interfaces/nano-lessons';

export const ADD_LINK = (callback: () => void): IButton => ({
    callback: () => callback(),
    text: '+ Add Link',
    color: colors.$white,
    type: ButtonType.ROUNDED,
});

export const LINK = (link: INanoLink): IButton => ({
    text: link.title,
    type: ButtonType.ROUNDED,
    color: colors.$white,
    callback: () => link.url && goTo(link.url),
});
