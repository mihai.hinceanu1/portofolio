import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';
import styled from 'styled-components/native';

export const LinkSection = styled.View<ReactWebAttributes>`
    min-height: 90px;
    margin-top: 10px;
`;

export const Triggers = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

export const Button = styled.TouchableOpacity`
    margin-left: 10px;
    margin-right: 10px;
`;

// ===== OVERRIDES =====

export const BUTTON_OVERRIDES = (difficulty: NanolessonDifficulty) => ({
    root: `
        border-width: 1px;
        width: 145px;
        height: 30px;
        border-color: ${getColorByDifficulty(difficulty)};
        align-self: center;
    `,
    text: `
        color: ${getColorByDifficulty(difficulty)};
    `,
});
