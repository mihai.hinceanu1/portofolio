import * as div from './link-section.style';
import * as utils from './link-section.utils';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { Button } from '../../../shared/components/vsc-button/button';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import { setCurrentNanolessonTab, toggleLinkModal } from '../../services/nanolessons-admin.service';
import * as React from 'react';

interface Props {
    tab: INanolessonTab;
    difficulty: NanolessonDifficulty;
}
interface State {
    isHover: boolean;
}

/**
 * Toggles AddLink button and the actual button link button (if exists)
 * onHover, edit/delete button is displayed to handle existing link
 */
export class LinkSection extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            isHover: false,
        };
    }

    public render() {
        const { isHover } = this.state;
        const { difficulty, tab } = this.props;
        const { link } = tab;

        return (
            <div.LinkSection data-cy='link-section'
                onMouseEnter={() => this.toggleHover(true)}
                onMouseLeave={() => this.toggleHover(false)}>

                    {/** Link Button */}
                    {this.renderLinkButton()}

                    {/** Triggers */}
                    {
                        (isHover && link) && // trigger hover only if there is a link
                        <div.Triggers data-cy='triggers'>
                            <div.Button onPress={() => this.onAddLinkPress()}>
                                <Icon path={icons.EDIT} color={getColorByDifficulty(difficulty)} />
                            </div.Button>

                            <div.Button onPress={() => this.onDeleteLinkPress()}>
                                <Icon path={icons.CROSS} color={getColorByDifficulty(difficulty)} />
                            </div.Button>
                        </div.Triggers>
                    }
            </div.LinkSection>
        );
    }

    private toggleHover(bool: boolean) {
        this.setState({ isHover: bool });
    }

    private renderLinkButton(): JSX.Element {
        const { link } = this.props.tab;
        const { difficulty } = this.props;

        if (!link || !link.title || !link.url) {
            return (
                <Button {...utils.ADD_LINK(this.onAddLinkPress.bind(this))}
                    overrides={div.BUTTON_OVERRIDES(difficulty)} />
            );
        }

        return (
            <Button {...utils.LINK(link)}
                overrides={div.BUTTON_OVERRIDES(difficulty)} />
        );
    }

    private onAddLinkPress() {
        toggleLinkModal(true);
    }

    private onDeleteLinkPress() {
        // empty title and url from link
        const tab = {
            ...this.props.tab,
            link: {
                title: '',
                url: '',
            },
        };

        setCurrentNanolessonTab(tab);
    }
}
