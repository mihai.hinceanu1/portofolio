import { ReactWebAttributes } from '../../../shared/interfaces/workarounds';
import styled from 'styled-components/native';

// ===== COMPONENTS =====

export const ExtendedNanolesson = styled.View<ReactWebAttributes>`
    margin-bottom: 50px;
`;

// ====== OVERRIDES =====

export const NanolessonOverride = {
    root: `
    `,
};
