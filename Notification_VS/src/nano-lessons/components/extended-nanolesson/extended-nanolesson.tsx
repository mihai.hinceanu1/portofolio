import * as div from './extended-nanolesson.style';
import { INanolesson } from '../../interfaces/nano-lessons';
import { NanolessonMenu } from '../nanolesson-menu/nanolesson-menu';
import { NanolessonStats } from '../nanolesson-stats/nanolesson-stats';
import { Nanolesson } from '../nanolesson/nanolesson';
import * as React from 'react';

interface Props {
    nanolesson: INanolesson;
}
interface State {
    menuOpen: boolean;
    isHover: boolean;
}

/**
 * Extends current Nanolesson component
 * Adds stats bar that shows different numbers regarding given nanolesson
 */
export class ExtendedNanolesson extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            menuOpen: false,
            isHover: false,
        };
    }

    public render() {
        const { nanolesson } = this.props;
        const { menuOpen, isHover } = this.state;

        return (
            <div.ExtendedNanolesson data-cy='extended-nanolesson'
                onMouseEnter={() => this.toggleHover(true)}
                onMouseLeave={() => this.toggleHover(false)}>

                {/** Stats */}
                <NanolessonStats nanolesson={nanolesson}
                    isHover={isHover}
                    onMenuPress={() => this.onMenuPress()} />

                {/** Nanolesson */}
                <Nanolesson nanolesson={nanolesson}
                    overrides={div.NanolessonOverride} />

                {
                    menuOpen &&
                    <NanolessonMenu nanolesson={nanolesson} />
                }

            </div.ExtendedNanolesson>
        );
    }

    private onMenuPress() {
        const { menuOpen } = this.state;

        this.setState({ menuOpen: !menuOpen });
    }

    private toggleHover(isHover: boolean) {
        this.setState({ isHover });
    }
}
