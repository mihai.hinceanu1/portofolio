import * as div from './confirm-delete.style';
import * as utils from './confirm-delete.utils';
import { Modal } from '../../../shared/components/modal/modal';
import { Button } from '../../../shared/components/vsc-button/button';
import {
    deleteNanolesson,
    deleteNanolessonError$,
    deleteNanolessonId$,
    toggleDeleteNanolessonModal
    } from '../../services/nanolessons-admin.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {}
interface State {
    nanolessonId: string;
    error: string;
}

export class ConfirmDelete extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            nanolessonId: null,
            error: null,
        };
    }

    public render() {
        const { error } = this.state;

        return (
            <Modal callback={() => this.onCancelPress()}>
                <div.ConfirmDelete data-cy='confirm-delete'>

                    {/** Title */}
                    <div.Title data-cy='title'>
                        Are you sure you want to delete?
                    </div.Title>

                    {/** Description */}
                    <div.Description data-cy='description'>
                        This action is irreversible. Once you accept it, there is no way back.
                    </div.Description>

                    {/** Buttons */}
                    <div.Buttons data-cy='buttons'>
                        <Button {...utils.CONFIRM(this.onConfirmPress.bind(this))} />
                        <Button {...utils.CANCEL(this.onCancelPress)} />
                    </div.Buttons>

                    {/** Error */}
                    {
                        !!error &&
                        <div.Error data-cy='error'>
                            {error}
                        </div.Error>
                    }

                </div.ConfirmDelete>
            </Modal>
        );
    }

    public componentDidMount() {
        this.subscribeToDeleteNanolessonId();
        this.subscribeToDeleteNanolessonError();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private onCancelPress() {
        toggleDeleteNanolessonModal(false);
    }

    private onConfirmPress() {
        const { nanolessonId } = this.state;

        deleteNanolesson(nanolessonId);
    }

    private subscribeToDeleteNanolessonId() {
        deleteNanolessonId$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(id => {
                this.setState({ nanolessonId: id });
            });
    }

    private subscribeToDeleteNanolessonError() {
        deleteNanolessonError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                if (error) {
                    const errorText = 'An error has occurred. Please try again later.';

                    this.setState({ error: errorText });
                } else {
                    this.setState({ error: null });
                }
            });
    }
}
