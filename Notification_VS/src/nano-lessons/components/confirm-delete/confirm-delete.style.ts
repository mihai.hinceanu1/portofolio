import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const ConfirmDelete = styled.View`
    background-color: ${colors.$white};
    padding: 50px;
    width: 400px;
`;

export const Title = styled.Text`
    font-size: ${font.$size18}px;
    font-weight: 600;
    margin-bottom: 10px;
    text-align: center;
`;

export const Description = styled.Text`
    font-size: ${font.$size14}px;
    font-weight: 300;
    margin-bottom: 50px;
    text-align: center;
`;

export const Buttons = styled.View`
    flex-direction: row;
    justify-content: space-around;
    width: 100%;
`;

export const Error = styled.Text`
    text-align: center;
    margin-top: 20px;
    font-size: ${font.$size14}px;
    color: ${colors.$red};
`;
