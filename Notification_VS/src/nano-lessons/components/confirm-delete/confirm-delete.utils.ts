import { ButtonType, IButton } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';

export const CONFIRM = (callback: () => void): IButton => ({
    text: 'YES, DELETE',
    color: colors.$red,
    type: ButtonType.GHOST,
    callback,
});

export const CANCEL = (callback: () => void): IButton => ({
    text: 'CANCEL',
    color: colors.$blue,
    type: ButtonType.FILL,
    callback,
});
