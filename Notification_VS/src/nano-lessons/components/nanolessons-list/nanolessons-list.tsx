import * as div from './nanolessons-list.style';
import { INanolesson } from '../../interfaces/nano-lessons';
import { ExtendedNanolesson } from '../extended-nanolesson/extended-nanolesson';
import * as React from 'react';
import { Text } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    nanolessons: INanolesson[];
}
interface State {}

class _NanolessonsList extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
    }

    public render() {
        const { nanolessons } = this.props;

        if (!nanolessons.length) {
            return <Text>No nanolessons found...</Text>;
        }

        return (
            <div.NanolessonsList data-cy='nanolessons-list'>
                {
                    nanolessons.length &&
                    nanolessons.map(nanolesson =>
                        <ExtendedNanolesson nanolesson={nanolesson}
                            key={nanolesson._id} />
                    )
                }
            </div.NanolessonsList>
        );
    }
}

export const NanolessonsList = withRouter<Props, any>(_NanolessonsList);
