import styled from 'styled-components/native';

export const NanolessonsList = styled.View`
    align-items: flex-start;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 100%;
    height: 100%;
    flex-direction: row;
    margin-top: 50px;
`;
