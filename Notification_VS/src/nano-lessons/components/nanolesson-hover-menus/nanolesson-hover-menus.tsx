import * as cfg from './nanolesson-hover-menus.configs';
import { Icon } from '../../../icons/components/icon/icon';
import { VS_LOGO } from '../../../shared/assets/icons';
import { BranchesMenu } from '../../../shared/components/branches-menu/branches-menu';
import { DotsMenu } from '../../../shared/components/dots-menu/dots-menu';
import * as sharedCfg from '../../../shared/constants/hover-menus.configs';
import { colors } from '../../../shared/style/colors';
import * as hover from '../../../shared/style/hover-menus.style';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {}

interface State {
    width: number;
}

/**
 * Contains
 * - Learning Map Domains
 * - User Menu
 */
class _NanolessonHoverMenus extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
        };
    }

    public render() {
        const { width } = this.state;

        return (
            <hover.Menus data-cy='nanolesson-hover-menus'
                pointerEvents='box-none'
                onLayout={e => this.updateWidth(e)}>

                {/** Learning Map Domains */}
                <BranchesMenu config={cfg.learningMapMenu}
                    overrides={hover.overrideTopLeftMenu(width)} />

                {/* Logo */}
                <hover.AppLogo width={width}>
                    <Icon path={VS_LOGO} width={100} height={35} color={colors.$blue} />
                </hover.AppLogo>

                {/* User Menu */}
                <DotsMenu config={sharedCfg.userMenu}
                    overrides={hover.overrideTopRightMenu(width)} />
            </hover.Menus>
        );
    }

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }
}

export const NanolessonHoverMenus = withRouter(_NanolessonHoverMenus);
