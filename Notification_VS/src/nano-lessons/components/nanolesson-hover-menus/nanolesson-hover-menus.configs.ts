import { mockLearningMapBranches } from '../../../lessons/mocks/branches';
import * as icn from '../../../shared/assets/icons';
import { BRANCHES_DIRECTIONS, STUB_DIRECTIONS } from '../../../shared/constants/branches-menu.const';
import { BranchesMenuConfig } from '../../../shared/interfaces/branches-menu';

/** Domains and categories available on the learnings map */
export const learningMapMenu: BranchesMenuConfig = {
    branches: mockLearningMapBranches,
    icon: {
        svgPath: icn.LEARNING_MAP,
    },
    branchesDirection: BRANCHES_DIRECTIONS.right,
    stubDirection: STUB_DIRECTIONS.bottom,
};
