import * as div from './extras.style';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { INanolesson, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import * as React from 'react';

interface Props {
    onReusablePress: () => void;
    onDifficultyPress: (difficulty: NanolessonDifficulty) => void;
    nanolesson: INanolesson;
}

export const Extras: React.FunctionComponent<Props> = (props: Props) => {
    const { onReusablePress, nanolesson, onDifficultyPress } = props;
    const { reusable, difficulty } = nanolesson;

    return (
        <div.Extras data-cy='extras'>

            {/** Reusable */}
            <div.Button data-cy='toggle-reusable'
                onPress={() => onReusablePress()}>
                    {
                        reusable &&
                        <Icon path={icons.REUSE}
                            color={getColorByDifficulty(difficulty)}
                            width={25}
                            height={25} />
                    }

                    {
                        !nanolesson.reusable &&
                        <Icon path={icons.NO_REUSE}
                            color={getColorByDifficulty(difficulty)}
                            width={25}
                            height={25} />
                    }
            </div.Button>

            {/** Difficulty */}
            <div.Difficulty data-cy='difficulty'>

                {/** Beginner */}
                <div.Button data-cy='toggle-difficulty'
                    onPress={() => onDifficultyPress(NanolessonDifficulty.Beginner)}>
                        <Icon path={icons.DIFFICULTY_BEGINNER}
                            color={difficulty === 'Beginner' ? colors.$nanolessonJunior : colors.$grey}
                            width={30}
                            height={20} />
                </div.Button>

                {/** Intermediate */}
                <div.Button data-cy='toggle-difficulty'
                    onPress={() => onDifficultyPress(NanolessonDifficulty.Intermediate)}>
                        <Icon path={icons.DIFFICULTY_INTERMEDIATE}
                            color={difficulty === 'Intermediate' ? colors.$nanolessonIntermediate : colors.$grey}
                            width={30}
                            height={30} />
                </div.Button>

                {/** Expert */}
                <div.Button data-cy='toggle-difficulty'
                    onPress={() => onDifficultyPress(NanolessonDifficulty.Expert)}>
                        <Icon path={icons.DIFFICULTY_EXPERT}
                            color={difficulty === 'Expert' ? colors.$nanolessonExpert : colors.$grey}
                            width={30}
                            height={40} />
                </div.Button>

            </div.Difficulty>
        </div.Extras>
    );
};
