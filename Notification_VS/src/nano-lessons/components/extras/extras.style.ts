import styled from 'styled-components/native';

export const Extras = styled.View`
    align-items: center;
    justify-content: space-between;
    flex-direction: row;
    margin-bottom: 10px;
`;

export const Button = styled.TouchableOpacity`
    justify-content: flex-end;
`;

export const Difficulty = styled.View`
    justify-content: space-between;
    flex-direction: row;
    width: 130px;
`;