import * as div from './nanolesson-title.style';
import * as React from 'react';

interface Props {
    title: string;
    description: string;
}

export const NanolessonTitle: React.FunctionComponent<Props> = (props: Props) => {
    const { title, description } = props;

    return (
        <div.NanolessonTitle data-cy='nanolesson-title'>

            {/** Title */}
            <div.Title data-cy='title'>
                {title}
            </div.Title>

            {/** Description */}
            <div.Description data-cy='description'>
                {description}
            </div.Description>
        </div.NanolessonTitle>
    );
};
