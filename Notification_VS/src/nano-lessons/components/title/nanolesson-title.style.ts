import { PAGE_WIDTH } from '../../../shared/constants/adaptive.const';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const NanolessonTitle = styled.View`
    width: 100%;
    max-width: ${PAGE_WIDTH}px;
`;

export const Title = styled.Text`
    font-size: ${font.$size22}px;
    font-weight: 600;
`;

export const Description = styled.Text`
    font-size: ${font.$size12}px;
    font-weight: 300;
`;
