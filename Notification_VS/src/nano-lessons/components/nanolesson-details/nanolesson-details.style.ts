import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { BOX_SHADOW_CSS, getShadow } from '../../../shared/style/shadow.style';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { NANOLESSON_TAB_HEIGHT } from '../../constants/nanolesson.const';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';
import styled from 'styled-components/native';

interface DetailProps {
    difficulty: NanolessonDifficulty;
}

export const NanolessonDetails = styled.View`
    width: 165px;
    ${getShadow(BOX_SHADOW_CSS)};
`;

export const Header = styled.View<DetailProps>`
    width: 100%;
    height: ${NANOLESSON_TAB_HEIGHT}px;
    background-color: ${props => getColorByDifficulty(props.difficulty)};
    align-items: center;
    justify-content: center;
`;

export const CloseIcon = styled.TouchableOpacity`
    position: absolute;
    right: 10px;
    top: 10px;
`;

export const Name = styled.Text`
    color: ${colors.$white};
    font-size: ${font.$size18}px;
    padding-left: 30px;
    padding-right: 30px;
`;

export const Title = styled.Text`
    font-weight: 600;
    font-size: ${font.$size14}px;
    ${getTextPadding()};
`;

export const Body = styled.View`
    padding-left: 30px;
    padding-right: 30px;
    background-color: ${colors.$white};
`;

export const Content = styled.Text`
    ${getTextPadding()};
`;

export const Image = styled.Image`
    width: 100%;
    height: 100px;
    margin-top: 30px;
`;

export const Link = styled.View`
    margin-top: 30px;
    margin-bottom: 30px;
`;

export const Button = styled.TouchableOpacity`
    width: 100%;
    min-height: 25px;
    align-items: center;
    justify-content: center;
    border-radius: 10px;
    border-width: 2px;
    border-color: ${colors.$black};
`;

export const ButtonText = styled.Text`
    text-transform: uppercase;
    font-weight: 600;
`;

// ===== UTILS =====

function getTextPadding() {
    return `
        padding-top: 20px;
    `;
}