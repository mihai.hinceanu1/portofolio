import * as div from './nanolesson-details.style';
import { Icon } from '../../../icons/components/icon/icon';
import { CROSS } from '../../../shared/assets/icons';
import { colors } from '../../../shared/style/colors';
import { INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import * as React from 'react';

interface Props {
    details: INanolessonTab;

    difficulty: NanolessonDifficulty;

    onClose?: () => void;
}

export const NanolessonDetails: React.FunctionComponent<Props> = (props: Props) => {
    const { details, difficulty, onClose } = props;
    const { name, title, content, image, link } = details;

    return (
        <div.NanolessonDetails data-cy='nanolesson-details'>

            {/** Header */}
            <div.Header data-cy='header' difficulty={difficulty}>

                {/** Close icon */}
                {
                    onClose &&
                    <div.CloseIcon data-cy='close-icon' onPress={() => onClose()}>
                        <Icon path={CROSS} color={colors.$white} width={10} height={10} />
                    </div.CloseIcon>
                }

                {/** Name */}
                <div.Name data-cy='name'>
                    {name}
                </div.Name>
            </div.Header>

            {/** Body */}
            <div.Body data-cy='body'>

                {/** Title */}
                <div.Title data-cy='title'>
                    {title}
                </div.Title>

                {/** Content */}
                {
                    !!content &&
                    <div.Content data-cy='content'>
                        {content}
                    </div.Content>
                }

                {/** Image */}
                {
                    !!image &&
                    <div.Image data-cy='image'
                        source={{ uri: image }}
                        resizeMode='contain' />
                }

                {/** Link */}
                {
                    !!link &&
                    <div.Link data-cy='link'>

                        {/** Button */}
                        <div.Button data-cy='button'>

                            {/** ButtonText */}
                            <div.ButtonText data-cy='button-text'>
                                {link.title}
                            </div.ButtonText>

                        </div.Button>

                    </div.Link>
                }

            </div.Body>

        </div.NanolessonDetails>
    );
};
