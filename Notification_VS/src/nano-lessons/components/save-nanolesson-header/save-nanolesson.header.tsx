import * as div from './save-nanolesson-header.style';
import * as utils from './save-nanolesson-header.utils';
import { Button } from '../../../shared/components/vsc-button/button';
import { INanolesson } from '../../interfaces/nano-lessons';
import * as React from 'react';

interface Props {
    onSavePress: () => void;
    onCancelPress: () => void;
    nanolesson: INanolesson;
}

export const SaveNanolessonHeader: React.FunctionComponent<Props> = (props: Props) => {
    const { onSavePress, onCancelPress, nanolesson } = props;

    return (
        <div.SaveNanolessonHeader data-cy='header'>

            {/** Title */}
            <div.Title data-cy='title'>Nanolesson Save</div.Title>

            {/** Actions */}
            <div.Actions data-cy='actions'>

                {/** Cancel */}
                <Button {...utils.CANCEL(onCancelPress, nanolesson.difficulty)}
                    overrides={div.CANCEL_BUTTON_OVERRIDES} />

                {/** Save */}
                <Button {...utils.SAVE(onSavePress, nanolesson.difficulty)}
                    overrides={div.SAVE_BUTTON_OVERRIDES} />
            </div.Actions>
        </div.SaveNanolessonHeader>
    );
};
