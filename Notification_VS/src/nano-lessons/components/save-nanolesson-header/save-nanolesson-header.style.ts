import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const SaveNanolessonHeader = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    align-self: center;
    max-width: 1000px;
    width: 100%;
    margin-top: 30px;
`;

export const Title = styled.Text`
    font-weight: 600;
    font-size: ${font.$size22}px;
`;

export const Actions = styled.View`
    flex-direction: row;
    align-items: center;
`;

// ===== OVERRIDES =====

export const CANCEL_BUTTON_OVERRIDES = {
    root: `
        width: 150px;
        height: 30px;
        background-color: transparent;
        box-shadow: none;
        margin-right: 25px;
    `,
};

export const SAVE_BUTTON_OVERRIDES = {
    root: `
        width: 205px;
        height: 40px;
    `,
};
