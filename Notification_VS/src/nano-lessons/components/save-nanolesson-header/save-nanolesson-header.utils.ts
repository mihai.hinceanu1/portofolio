import { ButtonType, IButton } from '../../../shared/interfaces/button';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';

export const CANCEL = (callback: () => void, difficulty: NanolessonDifficulty): IButton => ({
    callback: () => callback(),
    text: 'CANCEL',
    type: ButtonType.ROUNDED,
    inverted: true,
    color: getColorByDifficulty(difficulty),
});

export const SAVE = (callback: () => void, difficulty: NanolessonDifficulty): IButton => ({
    text: 'SAVE',
    callback: () => callback(),
    color: getColorByDifficulty(difficulty),
    type: ButtonType.ROUNDED,
});
