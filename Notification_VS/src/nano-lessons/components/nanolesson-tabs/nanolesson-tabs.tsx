import { NanolessonTab } from './nanolesson-tab';
import * as div from './nanolesson-tabs.style';
import { INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import * as React from 'react';

interface Props {
    /** Nanolesson's tabs */
    tabs: INanolessonTab[];

    /** Fired on tab click. Set different background color and change content */
    onTabClick: (tab: INanolessonTab) => void;

    /** Current active tab */
    activeTab: INanolessonTab;

    /** Type of nanolesson */
    difficulty: NanolessonDifficulty;
}

export const NanolessonTabs: React.FunctionComponent<Props> = (props: Props) => {
    const { tabs, onTabClick, activeTab, difficulty } = props;

    return (
        <div.NanolessonTabs data-cy='nanolesson-tabs'>

            {/** List of tabs */}
            {
                tabs.length &&
                tabs.map(tab =>
                    <NanolessonTab tab={tab}
                        key={tab.order}
                        onPress={(tab: INanolessonTab) => onTabClick(tab)}
                        active={activeTab.order === tab.order}
                        difficulty={difficulty} />
                )
            }
        </div.NanolessonTabs>
    );
};
