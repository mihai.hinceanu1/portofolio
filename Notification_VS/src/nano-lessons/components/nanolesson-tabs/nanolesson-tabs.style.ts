import { BOX_SHADOW_CSS } from '../../../shared/style/shadow.style';
import styled from 'styled-components/native';

export const NanolessonTabs = styled.View`
    ${BOX_SHADOW_CSS};
`;