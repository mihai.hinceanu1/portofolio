import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { NANOLESSON_TAB_HEIGHT, NANOLESSON_TAB_WIDTH } from '../../constants/nanolesson.const';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';
import styled from 'styled-components/native';

interface NanolessonTabProps {
    active: boolean;
    difficulty: NanolessonDifficulty;
}

export const NanolessonTab = styled.TouchableHighlight<NanolessonTabProps>`
    width: ${NANOLESSON_TAB_WIDTH}px;
    height: ${NANOLESSON_TAB_HEIGHT}px;
    background-color: ${props => getTabBackgroundColor(props)};
    border-bottom-width: 1px;
    border-bottom-color: ${colors.$grey};
    ${props => getFlexboxProperties(props.active)};
`;
/* ${props => getBorderRight(props)}; */

export const Title = styled.Text<NanolessonTabProps>`
    color: ${props => getColorByDifficulty(props.difficulty)};
    font-weight: 300;
    font-size: ${font.$size12}px;
    align-self: flex-start;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 10px;
`;

export const Icon = styled.Image`
    width: 70%;
    height: 70%;
`;

// ===== UTILS =====

function getTabBackgroundColor(props: NanolessonTabProps) {
    const { difficulty, active } = props;

    if (active) {
        return getColorByDifficulty(difficulty);
    } else {
        return colors.$nanolessonInactiveTab;
    }
}

// function getBorderRight(props: NanolessonTabProps) {
//     if (props.active) {
//         return `
//             border-right-width: 1px;
//             border-right-color: ${colors.$white};
//         `;
//     } else {
//         return `
//             border-right-width: 0px;
//         `;
//     }
// }

// Center children only if it's active
// Active means that icon is displayed
function getFlexboxProperties(active: boolean) {
    if (active) {
        return `
            align-items: center;
            justify-content: center;
        `;
    } else {
        return ``;
    }
}

// ===== OVERRIDES =====

export const ICON_OVERRIDES = {
    image: 'width: 50px; height: 50px;',
};
