import * as div from './nanolesson-tab.style';
import { Icon } from '../../../icons/components/icon/icon';
import { colors } from '../../../shared/style/colors';
import { INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import * as React from 'react';

interface Props {
    tab: INanolessonTab;

    onPress: (tab: INanolessonTab) => void;

    /** Specify if tab is currently focused */
    active: boolean;

    difficulty: NanolessonDifficulty;
}

export const NanolessonTab: React.FunctionComponent<Props> = (props: Props) => {
    const { tab, onPress, active, difficulty } = props;

    return (
        <div.NanolessonTab data-cy='nanolesson-tab'
            active={active}
            difficulty={difficulty}
            onPress={() => onPress(tab)}>

            <>
                {/** Icon */}
                {
                    active &&
                    <Icon path={tab.icon}
                        color={colors.$white}
                        overrides={div.ICON_OVERRIDES} />
                }

                {/** Title */}
                {
                    !active &&
                    <div.Title data-cy='title'
                        active={active}
                        difficulty={difficulty}>

                        {tab.name}

                    </div.Title>
                }
            </>

        </div.NanolessonTab>
    );
};
