import * as div from './nanolesson-edit-tab.style';
import { Icon } from '../../../icons/components/icon/icon';
import { toggleIconPicker } from '../../../icons/services/icons.service';
import * as icons from '../../../shared/assets/icons';
import { EditableInput } from '../../../shared/components/editable-input/editable-input';
import { colors } from '../../../shared/style/colors';
import { INanolessonTab } from '../../interfaces/nano-lessons';
import { setCurrentNanolessonTab } from '../../services/nanolessons-admin.service';
import * as React from 'react';
import { Text } from 'react-native';

interface Props {
    tab: INanolessonTab;
    onDeletePress: () => void;
    onPress: () => void;
    isActive: boolean;
}

interface State {
    name: string;
}

export class NanolessonEditTab extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            name: props.tab.name,
        };
    }

    public render() {
        const { name } = this.state;
        const { onDeletePress, isActive, onPress, tab } = this.props;

        return (
            <div.NanolessonEditTab data-cy='nanolesson-edit-tab'
                onPress={() => onPress()}
                isActive={isActive}>

                {/** Title */}
                <div.Title data-cy='title-frame'>
                    <EditableInput value={name}
                        placeholder='Your name here...'
                        overrides={div.EDITABLE_INPUT_OVERRIDES}
                        onSavePress={(text: string) => this.onSavePress(text)} />
                </div.Title>

                {/** Actions */}
                <div.Actions data-cy='actions'>

                    {/** Tab Icon Frame */}
                    <div.IconFrame data-cy='icon-frame'>

                        {/** Icon Selector */}
                        <div.IconSelector data-cy='icon-selector'
                            disabled={!isActive}
                            onPress={() => this.onChooseIcon()}>

                            <React.Fragment>
                                {
                                    !tab.icon &&
                                    <Text>Choose Icon</Text>
                                }

                                {
                                    tab.icon &&
                                    <Icon path={tab.icon} />
                                }
                            </React.Fragment>

                        </div.IconSelector>

                    </div.IconFrame>

                    {/** Delete */}
                    <div.Delete data-cy='delete'
                        onPress={() => onDeletePress()}
                        disabled={!isActive}>

                        <Icon path={icons.CROSS} color={colors.$grey} />

                    </div.Delete>
                </div.Actions>

            </div.NanolessonEditTab>
        );
    }

    private onSavePress(name: string) {
        this.setState({ name }, () => this.setCurrentTabName());
    }

    private setCurrentTabName() {
        const { name } = this.state;
        const { tab } = this.props;

        tab.name = name;
        setCurrentNanolessonTab(tab);
    }

    private onChooseIcon() {
        toggleIconPicker(true);
    }
}
