import { NanolessonEditTab } from './nanolesson-edit-tab';
import { NanolessonEditTabContent } from './nanolesson-edit-tab-content';
import * as div from './nanolesson-edit.style';
import * as utils from './utils/nanolesson-edit.utils';
import { Icon } from '../../../icons/components/icon/icon';
import { IIcon } from '../../../icons/interfaces/icon';
import { icon$ } from '../../../icons/services/icons.service';
import * as icons from '../../../shared/assets/icons';
import { Button } from '../../../shared/components/vsc-button/button';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import { colors } from '../../../shared/style/colors';
import { INanolesson, INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import { recalculateNanolessonTabOrder } from '../../services/nanolesson-services';
import { Extras } from '../extras/extras';
import { LinkModal } from '../link-modal/link-modal';
import * as React from 'react';
import { Text } from 'react-native';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
    currentNanolesson$,
    currentTab$,
    linkModalVisible$,
    setCurrentNanolesson,
    setCurrentNanolessonTab,
    toggleReusable,
    } from '../../services/nanolessons-admin.service';

interface Props {}
interface State {
    currentNanolesson: INanolesson;
    initialNanolesson: INanolesson;

    /**
     * Current active tab details are shown in the right side of nanolesson
     * On component loading, the first tab (if exists) will be active =
     */
    activeTab: INanolessonTab;
}

/**
 * Renders both tabs and content
 * Looks like a normal nanolesson, but with multiple external actions (add-new-tab, edit content etc)
 * Used only when creating or editing one nanolesson
 */
export class NanolessonEdit extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();
    private linkModal: JSX.Element = <LinkModal />;

    constructor(props: Props) {
        super(props);

        this.state = {
            initialNanolesson: null,
            currentNanolesson: null,
            activeTab: null,
        };
    }

    public render() {
        const { currentNanolesson, activeTab } = this.state;

        if (!currentNanolesson) {
            return (<Text> Loading...</Text>);
        }

        return (
            <div.NanolessonEdit data-cy='nanolesson-edit'>

                {/** Extras */}
                <Extras nanolesson={currentNanolesson}
                    onReusablePress={() => this.toggleReusable()}
                    onDifficultyPress={(difficulty: NanolessonDifficulty) => this.toggleDifficulty(difficulty)} />

                {/** Header */}
                <div.Header data-cy='header'
                    difficulty={currentNanolesson.difficulty}>

                    {/** New Nanolesson Icon */}
                    <Icon path={icons.NEW_NANO_BIG}
                        color={colors.$white}
                        width={50}
                        height={50} />

                    {/** Title */}
                    <div.Title data-cy='title'>
                        Save Nanolesson
                    </div.Title>
                </div.Header>

                {/** Body */}
                <div.Body data-cy='body'>

                    {/** Tabs */}
                    <div.Tabs data-cy='tabs'>
                        {
                            (currentNanolesson.tabs && !!currentNanolesson.tabs.length) &&
                            currentNanolesson.tabs.map(tab =>
                                <NanolessonEditTab tab={tab}
                                    isActive={activeTab && activeTab.order === tab.order}
                                    key={tab.order}
                                    onPress={() => this.onTabPress(tab)}
                                    onDeletePress={() => this.deleteTab(tab)} />
                            )
                        }

                        {/** Add TAB Button */}
                        <Button { ...utils.ADD_TAB_BUTTON(this.onAddNewTabPress.bind(this), currentNanolesson && currentNanolesson.difficulty)}
                            overrides={div.ADD_TAB_OVERRIDES} />

                    </div.Tabs>

                    {/** Content */}
                    <div.Content data-cy='content'>
                        {
                            activeTab &&
                            <NanolessonEditTabContent tab={activeTab} difficulty={currentNanolesson.difficulty} />
                        }
                    </div.Content>
                </div.Body>
            </div.NanolessonEdit>
        );
    }

    public componentDidMount() {
        this.subscribeToIconPickerSelection();
        this.subscribeToLinkModalVisible();
        this.subscribeToCurrentNanolesson();
        this.subscribeToCurrentTab();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // ===== SUBSCRIBERS =====

    private subscribeToIconPickerSelection() {
        icon$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(icon => {
                this.setSelectedIcon(icon);
            });
    }

    private subscribeToLinkModalVisible() {
        linkModalVisible$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isVisible => {
                if (isVisible) {
                    addAppOverlay(this.linkModal);
                } else {
                    removeAppOverlay(this.linkModal);
                }
            });
    }

    private subscribeToCurrentNanolesson() {
        currentNanolesson$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(currentNanolesson => {
                if (!currentNanolesson) {
                    this.initializeEmptyNanolesson();
                } else {
                    this.setState({ currentNanolesson });
                }
            });
    }

    private subscribeToCurrentTab() {
        currentTab$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(currentTab => {
                this.setState({ activeTab: currentTab });
            });
    }

    // ===== UTILS =====

    private setSelectedIcon(icon: IIcon) {
        const { activeTab, currentNanolesson } = this.state;

        // if there is no icon, exit
        if (!icon) {
            return;
        }

        currentNanolesson.tabs = currentNanolesson.tabs.map(tab => {
            if (tab.order === activeTab.order) {
                tab.icon = icon.path;

                return tab;
            }

            return tab;
        });

        setCurrentNanolesson(currentNanolesson);
    }

    private onAddNewTabPress() {
        const { currentNanolesson } = this.state;

        currentNanolesson.tabs = currentNanolesson.tabs.concat({
            name: '',
            order: currentNanolesson.tabs.length + 1,
            content: '',
            title: '',
        } as INanolessonTab);

        const latestTab = currentNanolesson.tabs[currentNanolesson.tabs.length - 1];

        setCurrentNanolesson(currentNanolesson);
        setCurrentNanolessonTab(latestTab);
    }

    private toggleReusable() {
        toggleReusable();
    }

    private toggleDifficulty(difficulty: NanolessonDifficulty) {
        let currentNanolesson = { ...this.state.currentNanolesson };

        currentNanolesson.difficulty = difficulty;

        setCurrentNanolesson(currentNanolesson);
    }

    /**
     * When creating a nanolesson, the object is empty (its value is "{}")
     * Some of the key are not available and the code might be pushed into
     * "Cannot read property X of undefined"
     * If there is no currentNanolesson available in state, we initialize
     * an entire Nanolesson object with empty values
     */
    private initializeEmptyNanolesson() {
        let nanolesson = utils.initializeNanolesson();

        setCurrentNanolesson(nanolesson);
    }

    private deleteTab(tab: INanolessonTab) {
        const { currentNanolesson } = this.state;

        currentNanolesson.tabs = currentNanolesson.tabs
            .filter(currentTab => currentTab.order !== tab.order);

        currentNanolesson.tabs = recalculateNanolessonTabOrder(currentNanolesson.tabs);

        setCurrentNanolesson(currentNanolesson);
        setCurrentNanolessonTab(currentNanolesson.tabs[0]);
    }

    private onTabPress(tab: INanolessonTab) {
        setCurrentNanolessonTab(tab);
    }
}
