import { colors } from '../../../shared/style/colors';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';
import styled from 'styled-components/native';

interface HeaderProps {
    difficulty: NanolessonDifficulty;
}

export const NanolessonEdit = styled.View`
    width: 450px;
    min-height: 300px;
`;

export const Header = styled.View<HeaderProps>`
    height: 150px;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    background-color: ${props => getHeaderBackgroundColor(props)};
`;

export const Title = styled.Text`
    text-transform: uppercase;
    color: white;
    font-weight: 600;
    font-size: 18px;
    margin-top: 5px;
`;

export const Body = styled.View`
    flex-direction: row;
    align-items: flex-start;
`;

export const Tabs = styled.View`
    width: 160px;
`;

export const Content = styled.View`
    width: 290px;
    background-color: ${colors.$white};
`;

// ===== UTILS =====

function getHeaderBackgroundColor(props: HeaderProps) {
    if (props.difficulty === NanolessonDifficulty.Beginner) {
        return colors.$nanolessonJunior;
    }

    if (props.difficulty === NanolessonDifficulty.Intermediate) {
        return colors.$nanolessonIntermediate;
    }

    if (props.difficulty === NanolessonDifficulty.Expert) {
        return colors.$nanolessonExpert;
    }
}

// ===== OVERRIDES =====

export const ADD_TAB_OVERRIDES = {
    root: `
        border: none;
    `,
    text: `
        font-weight: 600;
    `,
};
