import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface NanolessonEditTabProps {
    isActive: boolean;
}

export const NanolessonEditTab = styled.TouchableOpacity<NanolessonEditTabProps>`
    height: 140px;
    position: relative;
    margin-bottom: 2px;
    background-color: ${props => getBackgroundColor(props.isActive)};
`;

export const Title = styled.View`
`;

export const Actions = styled.View`
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 50px;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    padding-left: 15px;
    padding-right: 15px;
`;

export const Delete = styled.TouchableOpacity`
`;

export const IconFrame = styled.View``;

export const IconSelector = styled.TouchableHighlight`
    align-items: center;
    justify-content: center;
`;

// ===== OVERRIDES =====

export const EDITABLE_INPUT_OVERRIDES = {
    input: `
        font-size: ${font.$size14}px;
    `,
};

// ===== UTILS =====
function getBackgroundColor(isActive: boolean) {
    if (isActive) {
        return colors.$white;
    }

    return colors.$inactive;
}
