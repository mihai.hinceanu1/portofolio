import { ButtonType, IButton } from '../../../../shared/interfaces/button';
import { getColorByDifficulty } from '../../../../shared/style/shared.style';
import { INanolesson, INanolessonTab, NanolessonDifficulty } from '../../../interfaces/nano-lessons';

export const ADD_TAB_BUTTON = (callback: () => void, difficulty: NanolessonDifficulty): IButton => ({
    type: ButtonType.FLAT,
    color: getColorByDifficulty(difficulty),
    text: '+ ADD TAB',
    callback: () => callback(),
});

export function initializeNanolesson(): INanolesson {
    return {
        _id: null,
        created: 0,
        updated: 0,
        isActive: true,
        name: 'Default',
        difficulty: NanolessonDifficulty.Beginner,
        reusable: false,
        used: 0,
        tabs: [] as INanolessonTab[],
    };
}
