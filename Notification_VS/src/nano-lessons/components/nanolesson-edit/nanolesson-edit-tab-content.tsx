import * as div from './nanolesson-edit-tab-content.style';
import { EditableInput } from '../../../shared/components/editable-input/editable-input';
import { INanolessonTab, NanolessonDifficulty } from '../../interfaces/nano-lessons';
import { setCurrentNanolessonTab } from '../../services/nanolessons-admin.service';
import { ImageSection } from '../image-section/image-section';
import { LinkSection } from '../link-section/link-section';
import * as React from 'react';

interface Props {
    tab: INanolessonTab;

    /** Depending on the difficulty, colors might change */
    difficulty: NanolessonDifficulty;
}

interface State {
    tab: INanolessonTab;
}

/**
 * Content of selected Tab
 * It has a Title, Content (text), Link to external resource and Image.
 */
export class NanolessonEditTabContent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            tab: props.tab,

        };
    }

    static getDerivedStateFromProps(props: Props, state: State) {
        const { tab } = props;

        if (JSON.stringify(tab) !== JSON.stringify(state.tab)) {
            return { ...state, tab };
        }

        return state;
    }

    public render() {
        const { tab } = this.state;
        const { difficulty } = this.props;

        return (
            <div.NanolessonEditTabContent data-cy='nanolesson-edit-tab-content'>

                {/** Title */}
                <EditableInput value={tab.title}
                    placeholder='Your title here..'
                    onSavePress={(title: string) => this.onSavePress(title, 'title')} />

                {/** Content */}
                <EditableInput value={tab.content}
                    placeholder='Your content here..'
                    multiline={true}
                    onSavePress={(newContent: string) => this.onSavePress(newContent, 'content')} />

                {/** Add Image */}
                <ImageSection tab={tab} difficulty={difficulty} />

                {/** Link */}
                <LinkSection tab={tab} difficulty={difficulty} />
            </div.NanolessonEditTabContent>
        );
    }

    private onSavePress(value: string, key: string) {
        const tab = { ...this.state.tab, [key]: value };

        setCurrentNanolessonTab(tab);
    }
}
