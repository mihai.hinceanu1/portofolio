import * as div from './link-modal.style';
import * as utils from './link-modal.utils';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { Modal } from '../../../shared/components/modal/modal';
import { INanolessonTab } from '../../interfaces/nano-lessons';
import { currentTab$, setCurrentNanolessonTab, toggleLinkModal } from '../../services/nanolessons-admin.service';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { EditableInput } from '../../../shared/components/editable-input/editable-input';

interface Props {}

interface State {
    url: string;
    title: string;
    currentTab: INanolessonTab;
}

/**
 * Modal with an Edit form
 * Let the user edit the external link available in nanolesson content
 */
export class LinkModal extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            url: '',
            title: '',
            currentTab: null,
        };
    }

    public render() {
        const { url, title } = this.state;

        return (
            <Modal callback={() => this.closeModal()} overrides={div.MODAL_OVERRIDES}>

                {/** White Box */}
                <div.WhiteBox data-cy='white-box'>

                    {/** Title */}
                    <div.Title data-cy='title'>
                        Attach link to Nanolesson
                    </div.Title>

                    {/** Name */}
                    <Input value={title}
                        placeholder='Title'
                        label={title && 'Title'}
                        onChangeText={(text: string) => this.onChangeText(text, 'title')}
                        overrides={div.INPUT_OVERRIDES} />

                    {/** Link */}
                    <Input value={url}
                        placeholder='Url'
                        label={url && 'Url'}
                        // multiline={true}
                        onChangeText={(text: string) => this.onChangeText(text, 'url')}
                        overrides={div.INPUT_OVERRIDES} />

                    <Button config={utils.SAVE(this.onSavePress.bind(this))}
                        overrides={div.BUTTON_OVERRIDES} />

                </div.WhiteBox>

            </Modal>
        );
    }

    public componentDidMount() {
        this.subscribeToCurrentTab();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // ===== SUBSCRIBERS =====

    private subscribeToCurrentTab() {
        currentTab$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(currentTab => {
                this.setCurrentTab(currentTab);
            });
    }

    private closeModal() {
        toggleLinkModal(false);
    }

    private onChangeText(text: string, key: string) {
        this.setState({
            ...this.state,
            [key]: text,
        });
    }

    private onSavePress() {
        const { title, url, currentTab } = this.state;

        const newCurrentTab: INanolessonTab = {
            ...currentTab,
            link: {
                title, url
            }
        };

        setCurrentNanolessonTab(newCurrentTab);
        toggleLinkModal(false);
    }

    private setCurrentTab(tab: INanolessonTab) {
        this.setState({ currentTab: tab });

        if (tab.link) {
            const { url, title } = tab.link;

            this.setState({ url, title });
        }
    }
}
