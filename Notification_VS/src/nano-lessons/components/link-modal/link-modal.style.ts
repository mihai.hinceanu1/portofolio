import { InputOverrides } from '../../../shared/interfaces/input';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const WhiteBox = styled.View`
    background-color: ${colors.$white};
    width: 300px;
    height: 270px;
`;

export const Title = styled.Text`
    text-align: center;
    font-size: ${font.$size18}px;
    padding: 10px 0px;
`;

// ===== OVERRIDES =====

export const MODAL_OVERRIDES = {
    root: `
        align-items: center;
        justify-content: center;
    `,
};

export const INPUT_OVERRIDES: InputOverrides = {
    root: `
        width: 100%;
    `,
};

export const BUTTON_OVERRIDES = {
    root: `
        align-self: center;
    `,
};
