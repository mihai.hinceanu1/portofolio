import { ButtonConfig } from '../../../shared/interfaces/button';
import { colors } from '../../../shared/style/colors';

export const SAVE = (
    onPress: () => void,
): ButtonConfig => ({
    text: 'SAVE',
    onPress: () => onPress(),
    filled: true,
    color: colors.$blue,
});
