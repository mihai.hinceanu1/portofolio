import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

interface TextProps {
    disabled?: boolean;
}

export const NanolessonMenu = styled.View`
    position: absolute;
    top: 30px;
    left: 0;
    width: 100%;
    background-color: white;
    align-items: center;
    justify-content: space-around;
    padding: 10px 25px;
`;

export const Button = styled.TouchableOpacity`
    width: 100%;
`;

export const Text = styled.Text<TextProps>`
    padding-top: 5px;
    padding-bottom: 5px;
    color: ${props => props.disabled ? colors.$grey : 'black'};
`;
