import * as div from './nanolesson-menu.style';
import { INanolesson } from '../../interfaces/nano-lessons';
import { setCurrentNanolesson, setDeleteNanolessonId, toggleDeleteNanolessonModal } from '../../services/nanolessons-admin.service';
import { History } from 'history';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps {
    nanolesson: INanolesson;
}

const _NanolessonMenu: React.FunctionComponent<Props> = (props: Props) => {
    const { nanolesson, history } = props;

    return (
        <div.NanolessonMenu data-cy='nanolesson-menu'>

            {/** Edit */}
            <div.Button data-cy='button' onPress={() => onEditPress(nanolesson, history)}>
                <div.Text data-cy='text'>
                    Edit
                </div.Text>
            </div.Button>

            {/** Delete */}
            <div.Button data-cy='button' onPress={() => onDeletePress(nanolesson)}>
                <div.Text data-cy='text'>
                    Delete
                </div.Text>
            </div.Button>

            {/** Duplicate and Edit */}
            <div.Button data-cy='button'
                disabled={true}
                onPress={() => onDuplicateEditPress(nanolesson)}>

                <div.Text data-cy='text' disabled={true}>
                    Duplicate and Edit
                </div.Text>
            </div.Button>

        </div.NanolessonMenu>
    );
};

function onEditPress(nanolesson: INanolesson, history: History) {
    setCurrentNanolesson(nanolesson);
    history.push('/admin/nanolessons/save');
}

function onDeletePress(nanolesson: INanolesson) {
    toggleDeleteNanolessonModal(true);
    setDeleteNanolessonId(nanolesson._id);
}

function onDuplicateEditPress(nanolesson: INanolesson) {
    console.log(nanolesson, 'duplicate');
}

export const NanolessonMenu = withRouter<Props, React.FunctionComponent<Props>>(_NanolessonMenu);
