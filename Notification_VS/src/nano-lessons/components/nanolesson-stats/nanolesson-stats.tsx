import * as div from './nanolesson-stats.style';
import { Icon } from '../../../icons/components/icon/icon';
import * as icons from '../../../shared/assets/icons';
import { INanolesson } from '../../interfaces/nano-lessons';
import * as React from 'react';

interface Props {
    nanolesson: INanolesson;
    onMenuPress: () => void;
    isHover: boolean;
}
interface State {}

/**
 * Data is currently mocked. No backend functionality provided at the moment
 * Only the menu (edit/delete) is working, the others are just for visualisation
 */
export class NanolessonStats extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        const { onMenuPress, isHover, nanolesson } = this.props;

        return (
            <div.NanolessonStats data-cy='nanolesson-stats'>

                    {/** Reuse */}
                    <div.Stat data-cy='stat'>
                        <Icon path={icons.REUSE}
                            color={div.getIconColor(isHover, nanolesson.difficulty)}
                            width={25}
                            height={25} />

                        <div.Value data-cy='value' isHover={isHover}>
                            412
                        </div.Value>
                    </div.Stat>

                    {/** Duplicates */}
                    <div.Stat data-cy='stat'>
                        <Icon path={icons.DUPLICATES}
                            color={div.getIconColor(isHover, nanolesson.difficulty)}
                            width={25}
                            height={25} />

                        <div.Value data-cy='value' isHover={isHover}>
                            555
                        </div.Value>
                    </div.Stat>

                    {/** Categories */}
                    <div.Stat data-cy='stat'>
                        <Icon path={icons.CATEGORIES}
                            color={div.getIconColor(isHover, nanolesson.difficulty)}
                            width={25}
                            height={25} />
                    </div.Stat>

                    {/** Stats */}
                    <div.Stat data-cy='stat'>
                        <Icon path={icons.STATS}
                            color={div.getIconColor(isHover, nanolesson.difficulty)}
                            width={25}
                            height={25} />
                    </div.Stat>

                    {/** Menu */}
                    <div.Stat data-cy='stat'>
                        <div.Button data-cy='button'
                            onPress={() => onMenuPress()}>

                            <Icon path={icons.DOTTED_MENU}
                                color={div.getIconColor(isHover, nanolesson.difficulty)}
                                width={10}
                                height={25} />
                        </div.Button>
                    </div.Stat>
            </div.NanolessonStats>
        );
    }
}
