import { colors } from '../../../shared/style/colors';
import { getColorByDifficulty } from '../../../shared/style/shared.style';
import { NanolessonDifficulty } from '../../interfaces/nano-lessons';
import styled from 'styled-components/native';

interface ValueProps {
    isHover: boolean;
}

export const NanolessonStats = styled.View`
    flex-direction: row;
    justify-content: space-between;
    margin-bottom: 10px;
`;

export const Stat = styled.View`
    flex-direction: row;
    align-items: center;
`;

export const Value = styled.Text<ValueProps>`
    margin-left: 5px;
    color: ${props => getValueColor(props)};
`;

export const Button = styled.TouchableOpacity``;

// ===== UTILS =====

export function getIconColor(isHover: boolean, difficulty: NanolessonDifficulty) {
    if (isHover) {
        return getColorByDifficulty(difficulty);
    }

    return colors.$grey;
}

function getValueColor(props: ValueProps) {
    if (props.isHover) {
        return colors.$darkGrey;
    }

    return colors.$grey;
}
