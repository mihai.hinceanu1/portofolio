import * as div from './nanolesson-modal.style';
import { LessonCurrentNanolesson } from '../../../lessons/interfaces/lesson-current-nanolesson';
import { lessonCurrentNanolesson$ } from '../../../lessons/services/lessons.service';
import { showCurrentNanolesson } from '../../../lessons/services/lessons.service';
import { INanolesson } from '../../interfaces/nano-lessons';
import { currentNanolesson$, getCurrentNanolesson } from '../../services/nanolessons-lesson.services';
import { Nanolesson } from '../nanolesson/nanolesson';
import * as React from 'react';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props {}
interface State {
    lessonNanolesson: LessonCurrentNanolesson;
    nanolesson: INanolesson;
}

export class NanolessonModal extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            lessonNanolesson: null,
            nanolesson: null,
        };
    }

    public render() {
        const { nanolesson, lessonNanolesson } = this.state;

        console.log('++ lessonNanolesson', lessonNanolesson);

        return (
            <div.NanolessonModal data-cy='nanolesson-modal'
                top={lessonNanolesson && lessonNanolesson.position.y + lessonNanolesson.pxScrolled}
                left={lessonNanolesson && lessonNanolesson.position.x}>

                {
                    nanolesson &&
                    <Nanolesson nanolesson={nanolesson} onClose={() => this.onNanolessonClose()} />
                }
            </div.NanolessonModal>
        );
    }

    public componentDidMount() {
        this.subscribeToLessonNanolesson();
        this.subscribeToCurrentNanolesson();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private subscribeToLessonNanolesson() {
        lessonCurrentNanolesson$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(lessonNanolesson => {
                this.setState({ lessonNanolesson });
                getCurrentNanolesson(lessonNanolesson.nanolesson);
            });
    }

    private subscribeToCurrentNanolesson() {
        currentNanolesson$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(nanolesson => {
                this.setState({ nanolesson });
            });
    }

    private onNanolessonClose() {
        showCurrentNanolesson(false);
    }
}
