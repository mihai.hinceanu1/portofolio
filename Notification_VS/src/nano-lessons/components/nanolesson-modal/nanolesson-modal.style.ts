import styled from 'styled-components/native';

interface NanolessonModalProps {
    top?: number;
    left?: number;
}

export const NanolessonModal = styled.View<NanolessonModalProps>`
    max-width: 255px;
    position: absolute;
    top: ${props => props.top}px;
    left: ${props => props.left}px;
`;
