import { AppState } from '../../../shared/interfaces/app.state';
import { FileInput } from '../../../shared/interfaces/file-input';
import { Action } from '../../../shared/interfaces/shared';
import { INanolesson } from '../../interfaces/nano-lessons';
import * as webapi from '../../webapis/nanolessons.webapi';
import * as actions from '../actions/nanolessons-admin.actions';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getNanolessons$ = (action$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    action$.ofType(actions.GET_NANOLESSONS_REQ).pipe(
        map(action => action.payload),
        concatMap(() => webapi.getNanolessons().pipe(
            map(nanolessons => actions.getNanolessonsOk(nanolessons)),
            catchError(error => of(actions.getNanolessonsFail(error.response.data))),
        )),
    );

export const uploadNanoImage$ = (action$: ActionsObservable<Action<FileInput>>, _store: Store<AppState>) =>
    action$.ofType(actions.UPLOAD_IMAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(image => webapi.uploadNanoImage(image).pipe(
            map(response => actions.uploadImageOk(response)),
            catchError(error => of(actions.uploadImageFail(error.response.data))),
        )),
    );

export const saveNanolesson$ = (action$: ActionsObservable<Action<INanolesson>>, _store: Store<AppState>) =>
    action$.ofType(actions.SAVE_NANOLESSON_REQ).pipe(
        map(action => action.payload),
        concatMap(nanolesson => webapi.saveNanolesson(nanolesson).pipe(
            map(nanolesson => actions.saveNanolessonOk(nanolesson)),
            catchError(error => of(actions.saveNanolessonFail(error.response.data))),
        )),
    );

export const deleteNanolesson$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(actions.DELETE_NANOLESSON_REQ).pipe(
        map(action => action.payload),
        concatMap(nanoId => webapi.deleteNanolesson(nanoId).pipe(
            map(response => actions.deleteNanolessonOk(response)),
            catchError(error => of(actions.deleteNanolessonFail(error.response.data))),
        )),
    );
