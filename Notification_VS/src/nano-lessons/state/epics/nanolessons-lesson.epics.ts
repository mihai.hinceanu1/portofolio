import { AppState } from '../../../shared/interfaces/app.state';
import { Action } from '../../../shared/interfaces/shared';
import * as webapi from '../../webapis/nanolessons.webapi';
import * as actions from '../actions/nanolessons-lesson.actions';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getCurrentNanolesson$ = (action$: ActionsObservable<Action<string>>, _store$: Store<AppState>) =>
    action$.ofType(actions.GET_NANOLESSON_BY_ID_REQ).pipe(
        map(action => action.payload),
        concatMap(id => webapi.getNanolessonById(id).pipe(
            map(nanolesson => actions.getNanolessonByIdOk(nanolesson)),
            catchError(err => of(actions.getNanolessonByIdFail(err))),
        )),
    );
