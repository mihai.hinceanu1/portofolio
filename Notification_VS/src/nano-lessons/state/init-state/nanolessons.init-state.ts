import { nanolessonsAdminInitState } from './nanolessons-admin.init-state';
import { nanolessonsLessonInitState } from './nanolessons-lesson.init-state';
import { NanolessonsState } from '../../interfaces/nano-lessons.state';

export const nanolessonsInitState: NanolessonsState = {
    lesson: nanolessonsLessonInitState,
    admin: nanolessonsAdminInitState,
};
