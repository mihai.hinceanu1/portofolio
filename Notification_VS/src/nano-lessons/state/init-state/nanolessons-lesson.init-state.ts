import { NanolessonsLessonState } from '../../interfaces/nanolessons-lesson.state';

export const nanolessonsLessonInitState: NanolessonsLessonState = {
    currentNanolesson: null,
};
