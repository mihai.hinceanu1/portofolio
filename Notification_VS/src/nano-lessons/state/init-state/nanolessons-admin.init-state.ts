import { NanolessonsAdminState } from '../../interfaces/nanolesson-admin.state';

export const nanolessonsAdminInitState: NanolessonsAdminState = {
    nanolessons: null,
    error: null,
    linkModalVisible: false,
    imageModalVisible: false,
    currentNanolesson: null,
    currentTab: null,
    currentUploadedImage: null,
    uploadImage: {
        response: null,
        error: null,
    },
    saveNanolesson: {
        response: null,
        error: null,
    },
    deleteNanolesson: {
        error: null,

        confirmModal: false,

        nanolessonId: null,
    },
};
