import { NanolessonsAdminState } from '../../interfaces/nanolesson-admin.state';
import * as adminActions from '../actions/nanolessons-admin.actions';
import { nanolessonsAdminInitState } from '../init-state/nanolessons-admin.init-state';

export const nanolessonsAdminReducer = (state: NanolessonsAdminState = nanolessonsAdminInitState, action: any): NanolessonsAdminState => {
    switch (action.type) {
        case adminActions.GET_NANOLESSONS_OK: {
            return {
                ...state,
                nanolessons: action.payload,
            };
        }

        case adminActions.GET_NANOLESSONS_FAIL: {
            return {
                ...state,
                error: action.payload,
            };
        }

        case adminActions.TOGGLE_LINK_MODAL: {
            return {
                ...state,
                linkModalVisible: action.payload,
            };
        }

        case adminActions.TOGGLE_IMAGE_MODAL: {
            return {
                ...state,
                imageModalVisible: action.payload,
            };
        }

        case adminActions.SET_CURRENT_NANOLESSON: {
            const nanolesson = action.payload;

            return {
                ...state,
                currentNanolesson: action.payload,
                currentTab: nanolesson.tabs && nanolesson.tabs[0],

                /**
                 * if user enters again on save nanolesson page after updating any other nanolesson
                 */
                saveNanolesson: {
                    ...state.saveNanolesson,
                    response: null,
                    error: null,
                },
            };
        }

        case adminActions.SET_CURRENT_NANOLESSON_TAB: {
            const currTab = action.payload;
            const nanolesson = state.currentNanolesson;

            if (nanolesson) {
                nanolesson.tabs = nanolesson.tabs.map(tab => {
                    if (tab.order === currTab.order) {
                        return currTab;
                    }

                    return tab;
                });
            }

            return {
                ...state,
                currentNanolesson: nanolesson,
                currentTab: action.payload,
            };
        }

        case adminActions.SET_CURRENT_IMAGE_UPLOADED: {
            return {
                ...state,
                currentUploadedImage: action.payload,
            };
        }

        case adminActions.UPLOAD_IMAGE_OK: {
            const currState = { ...state };

            // GET from config file
            currState.uploadImage.response = 'http://localhost:8080/' + action.payload;
            currState.uploadImage.error = null;
            currState.imageModalVisible = false;

            return {
                ...currState,
            };
        }

        case adminActions.UPLOAD_IMAGE_FAIL: {
            const currState = { ...state };

            currState.uploadImage.response = null;
            currState.uploadImage.error = action.payload;

            return {
                ...state,
                ...currState,
            };
        }

        // ===== SAVE NANOLESSON =====

        case adminActions.SAVE_NANOLESSON_OK: {
            const currState = { ...state };

            currState.saveNanolesson.response = action.payload;
            currState.saveNanolesson.error = 'Some error here';

            return {
                ...state,
                ...currState,
            };
        }

        case adminActions.SAVE_NANOLESSON_FAIL: {
            const currState = { ...state };

            currState.saveNanolesson.response = null;
            currState.saveNanolesson.error = action.payload;

            return {
                ...state,
                ...currState,
            };
        }

        // ====== DELETE NANOLESSON =====

        case adminActions.DELETE_NANOLESSON_OK: {
            let currState = { ...state };
            const id = action.payload;

            currState.nanolessons = currState.nanolessons
                .filter(nanolesson => nanolesson._id !== id);
            currState.deleteNanolesson.nanolessonId = null;
            currState.deleteNanolesson.confirmModal = false;

            return {
                ...state,
                ...currState,
            };
        }

        case adminActions.DELETE_NANOLESSON_FAIL: {
            const currState = { ...state };

            currState.deleteNanolesson.error = action.payload;

            return {
                ...state,
                ...currState,
            };
        }

        case adminActions.SHOW_NANOLESSON_DELETE_CONFIRM: {
            const currState = { ...state };

            currState.deleteNanolesson.confirmModal = action.payload;

            return currState;
        }

        case adminActions.SET_DELETE_NANOLESSON_ID: {
            const currState = { ...state };

            currState.deleteNanolesson.nanolessonId = action.payload;

            return currState;
        }

        // ===== TOGGLE REUSABLE =====

        case adminActions.TOGGLE_REUSABLE: {
            let currentNanolesson = { ...state.currentNanolesson };
            currentNanolesson.reusable = !currentNanolesson.reusable;

            return {
                ...state,
                currentNanolesson,
            };
        }

        // ===== CLEAR CURRENT NANOLESSON =====

        case adminActions.CLEAR_CURRENT_NANOLESSON: {
            return {
                ...state,
                currentNanolesson: null,
                currentTab: null,
                currentUploadedImage: null,
                uploadImage: {
                    response: null,
                    error: null,
                },
                saveNanolesson: {
                    response: null,
                    error: null,
                },
            };
        }

        default:
            return state;
    }
};
