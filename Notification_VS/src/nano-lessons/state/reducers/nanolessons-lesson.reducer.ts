import { NanolessonsLessonState } from '../../interfaces/nanolessons-lesson.state';
import * as actions from '../actions/nanolessons-lesson.actions';
import { nanolessonsLessonInitState } from '../init-state/nanolessons-lesson.init-state';

export function nanolessonsLessonReducer(state: NanolessonsLessonState = nanolessonsLessonInitState, action: any): NanolessonsLessonState {
    switch (action.type) {
        case actions.GET_NANOLESSON_BY_ID_OK: {
            return {
                ...state,
                currentNanolesson: action.payload,
            };
        }

        default:
            return state;
    }
}
