import { nanolessonsAdminReducer } from './nanolessons-admin.reducer';
import { nanolessonsLessonReducer } from './nanolessons-lesson.reducer';
import { combineReducers } from 'redux';

/** Combine every reducer from nanolesson module */
export function nanolessonsReducers() {
    return combineReducers({
        admin: nanolessonsAdminReducer,
        lesson: nanolessonsLessonReducer,
    });
}
