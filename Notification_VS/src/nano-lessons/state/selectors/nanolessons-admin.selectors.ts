import { AppState } from '../../../shared/interfaces/app.state';
import { FileInput } from '../../../shared/interfaces/file-input';
import { INanolesson, INanolessonTab } from '../../interfaces/nano-lessons';
import { NanolessonsAdminState } from '../../interfaces/nanolesson-admin.state';
import { createSelector, Selector } from 'reselect';

const NANOLESSONS_MODULE: Selector<AppState, NanolessonsAdminState> = (state: AppState) => state.nanolessons.admin;

export const NANOLESSONS = createSelector<AppState, NanolessonsAdminState, INanolesson[]>(
    NANOLESSONS_MODULE,
    state => state.nanolessons,
);

export const NANOLESSONS_ERROR = createSelector<AppState, NanolessonsAdminState, string>(
    NANOLESSONS_MODULE,
    state => state.error,
);

export const LINK_MODAL_VISIBLE = createSelector<AppState, NanolessonsAdminState, boolean>(
    NANOLESSONS_MODULE,
    state => state.linkModalVisible,
);

export const IMAGE_MODAL_VISIBLE = createSelector<AppState, NanolessonsAdminState, boolean>(
    NANOLESSONS_MODULE,
    state => state.imageModalVisible,
);

export const CURRENT_NANOLESSON = createSelector<AppState, NanolessonsAdminState, INanolesson>(
    NANOLESSONS_MODULE,
    state => state.currentNanolesson,
);

export const CURRENT_TAB = createSelector<AppState, NanolessonsAdminState, INanolessonTab>(
    NANOLESSONS_MODULE,
    state => state.currentTab,
);

export const CURRENT_UPLOADED_IMAGE = createSelector<AppState, NanolessonsAdminState, FileInput>(
    NANOLESSONS_MODULE,
    state => state.currentUploadedImage,
);

export const UPLOAD_IMAGE_RESPONSE = createSelector<AppState, NanolessonsAdminState, string>(
    NANOLESSONS_MODULE,
    state => state.uploadImage.response,
);

export const UPLOAD_IMAGE_ERROR = createSelector<AppState, NanolessonsAdminState, string>(
    NANOLESSONS_MODULE,
    state => state.uploadImage.error,
);

export const SAVE_NANOLESSON_RESPONSE = createSelector<AppState, NanolessonsAdminState, INanolesson>(
    NANOLESSONS_MODULE,
    state => state.saveNanolesson.response,
);

export const SAVE_NANOLESSON_ERROR = createSelector<AppState, NanolessonsAdminState, string>(
    NANOLESSONS_MODULE,
    state => state.saveNanolesson.error,
);

export const DELETE_NANOLESSON_ERROR = createSelector<AppState, NanolessonsAdminState, string>(
    NANOLESSONS_MODULE,
    state => state.deleteNanolesson.error,
);

export const SHOW_DELETE_NANOLESSON_CONFIRM = createSelector<AppState, NanolessonsAdminState, boolean>(
    NANOLESSONS_MODULE,
    state => state.deleteNanolesson.confirmModal,
);

export const DELETE_NANOLESSON_ID = createSelector<AppState, NanolessonsAdminState, string>(
    NANOLESSONS_MODULE,
    state => state.deleteNanolesson.nanolessonId,
);
