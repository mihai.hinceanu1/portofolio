import { AppState } from '../../../shared/interfaces/app.state';
import { INanolesson } from '../../interfaces/nano-lessons';
import { NanolessonsLessonState } from '../../interfaces/nanolessons-lesson.state';
import { createSelector, Selector } from 'reselect';

const NANOLESSONS_MODULE: Selector<AppState, NanolessonsLessonState> = state => state.nanolessons.lesson;

export const CURRENT_NANOLESSON = createSelector<AppState, NanolessonsLessonState, INanolesson>(
    NANOLESSONS_MODULE,
    state => state.currentNanolesson,
);
