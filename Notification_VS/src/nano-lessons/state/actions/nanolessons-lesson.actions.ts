import { Action } from '../../../shared/interfaces/shared';
import { INanolesson } from '../../interfaces/nano-lessons';

export const GET_NANOLESSON_BY_ID_REQ = 'GET_NANOLESSON_BY_ID_REQ';
export const getNanolessonByIdReq = (id: string): Action<string> => ({
    type: GET_NANOLESSON_BY_ID_REQ,
    payload: id,
});

export const GET_NANOLESSON_BY_ID_OK = 'GET_NANOLESSON_BY_ID_OK';
export const getNanolessonByIdOk = (nanolesson: INanolesson): Action<INanolesson> => ({
    type: GET_NANOLESSON_BY_ID_OK,
    payload: nanolesson,
});

export const GET_NANOLESSON_BY_ID_FAIL = 'GET_NANOLESSON_BY_ID_FAIL';
export const getNanolessonByIdFail = (error: string): Action<string> => ({
    type: GET_NANOLESSON_BY_ID_FAIL,
    payload: error,
});
