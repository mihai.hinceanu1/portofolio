import { FileInput } from '../../../shared/interfaces/file-input';
import { Action } from '../../../shared/interfaces/shared';
import { INanolesson, INanolessonTab } from '../../interfaces/nano-lessons';

export const GET_NANOLESSONS_REQ = 'GET_NANOLESSON_REQ';
export const getNanolessonReq = (): Action<null> => ({
    type: GET_NANOLESSONS_REQ,
    payload: null,
});

export const GET_NANOLESSONS_OK = 'GET_NANOLESSONS_OK';
export const getNanolessonsOk = (nanolessons: INanolesson[]): Action<INanolesson[]> => ({
    type: GET_NANOLESSONS_OK,
    payload: nanolessons,
});

export const GET_NANOLESSONS_FAIL = 'GET_NANOLESSONS_FAIL';
export const getNanolessonsFail = (error: string): Action<string> => ({
    type: GET_NANOLESSONS_FAIL,
    payload: error,
});

export const TOGGLE_LINK_MODAL = 'TOGGLE_LINK_MODAL';
export const toggleLinkModal = (bool: boolean): Action<boolean> => ({
    type: TOGGLE_LINK_MODAL,
    payload: bool,
});

export const TOGGLE_IMAGE_MODAL = 'TOGGLE_IMAGE_MODAL';
export const toggleImageModal = (bool: boolean): Action<boolean> => ({
    type: TOGGLE_IMAGE_MODAL,
    payload: bool,
});

export const SET_CURRENT_NANOLESSON = 'SET_CURRENT_NANOLESSON';
export const setCurrentNanolesson = (nanolesson: INanolesson): Action<INanolesson> => ({
    type: SET_CURRENT_NANOLESSON,
    payload: nanolesson,
});

export const SET_CURRENT_NANOLESSON_TAB = 'SET_CURRENT_NANOLESSON_TAB';
export const setCurrentNanolessonTab = (tab: INanolessonTab): Action<INanolessonTab> => ({
    type: SET_CURRENT_NANOLESSON_TAB,
    payload: tab,
});

export const SET_CURRENT_IMAGE_UPLOADED = 'SET_CURRENT_IMAGE_UPLOADED';
export const setCurrentImageUploaded = (image: FileInput): Action<FileInput> => ({
    type: SET_CURRENT_IMAGE_UPLOADED,
    payload: image,
});

export const UPLOAD_IMAGE_REQ = 'UPLOAD_IMAGE_REQ';
export const uploadImageReq = (image: FileInput): Action<FileInput> => ({
    type: UPLOAD_IMAGE_REQ,
    payload: image,
});

export const UPLOAD_IMAGE_OK = 'UPLOAD_IMAGE_OK';
export const uploadImageOk = (path: string): Action<string> => ({
    type: UPLOAD_IMAGE_OK,
    payload: path,
});

export const UPLOAD_IMAGE_FAIL = 'UPLOAD_IMAGE_FAIL';
export const uploadImageFail = (error: string): Action<string> => ({
    type: UPLOAD_IMAGE_FAIL,
    payload: error,
});

export const SAVE_NANOLESSON_REQ = 'SAVE_NANOLESSON_REQ';
export const saveNanolessonReq = (nanolesson: INanolesson): Action<INanolesson> => ({
    type: SAVE_NANOLESSON_REQ,
    payload: nanolesson,
});

export const SAVE_NANOLESSON_OK = 'SAVE_NANOLESSON_OK';
export const saveNanolessonOk = (nanolesson: INanolesson): Action<INanolesson> => ({
    type: SAVE_NANOLESSON_OK,
    payload: nanolesson,
});

export const SAVE_NANOLESSON_FAIL = 'SAVE_NANOLESSON_FAIL';
export const saveNanolessonFail = (error: string): Action<string> => ({
    type: SAVE_NANOLESSON_FAIL,
    payload: error,
});

export const TOGGLE_REUSABLE = 'TOGGLE_REUSABLE';
export const toggleReusable = (): Action<null> => ({
    type: TOGGLE_REUSABLE,
    payload: null,
});

export const CLEAR_CURRENT_NANOLESSON = 'CLEAR_CURRENT_NANOLESSON';
export const clearCurrentNanolesson = (): Action<null> => ({
    type: CLEAR_CURRENT_NANOLESSON,
    payload: null,
});

// ===== DELETE NANOLESSON =====

export const DELETE_NANOLESSON_REQ = 'DELETE_NANOLESSON_REQ';
export const deleteNanolessonReq = (id: string): Action<string> => ({
    type: DELETE_NANOLESSON_REQ,
    payload: id,
});

export const DELETE_NANOLESSON_OK = 'DELETE_NANOLESSON_OK';
export const deleteNanolessonOk = (id: string): Action<string> => ({
    type: DELETE_NANOLESSON_OK,
    payload: id,
});

export const DELETE_NANOLESSON_FAIL = 'DELETE_NANOLESSON_FAIL';
export const deleteNanolessonFail = (error: string): Action<string> => ({
    type: DELETE_NANOLESSON_FAIL,
    payload: error,
});

export const SHOW_NANOLESSON_DELETE_CONFIRM = 'SHOW_NANOLESSON_DELETE_CONFIRM';
export const showNanolessonDeleteConfirm = (show: boolean): Action<boolean> => ({
    type: SHOW_NANOLESSON_DELETE_CONFIRM,
    payload: show,
});

export const SET_DELETE_NANOLESSON_ID = 'SET_DELETE_NANOLESSON_ID';
export const setDeleteNanolessonId = (id: string): Action<string> => ({
    type: SET_DELETE_NANOLESSON_ID,
    payload: id,
});
