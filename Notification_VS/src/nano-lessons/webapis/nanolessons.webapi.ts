import { FileInput } from '../../shared/interfaces/file-input';
import axios from '../../shared/services/axios.service';
import { INanolesson } from '../interfaces/nano-lessons';
import { from, Observable } from 'rxjs';

export const getNanolessons = (): Observable<INanolesson[]> =>
    from<Promise<INanolesson[]>>(
        axios.get<INanolesson[]>('/nanolessons')
            .then(res => res.data),
);

export const getNanolessonById = (id: string): Observable<INanolesson> =>
    from(
        axios.get(`/nanolessons/${id}`)
            .then(res => res.data),
    );

export const uploadNanoImage = (image: FileInput): Observable<string> => {
    let formData = new FormData();
    formData.append('file', image as any);

    return from<Promise<string>>(
        axios.post<string>(`/nanolessons/image`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(res => res.data)
    );
};

export const saveNanolesson = (nanolesson: INanolesson): Observable<INanolesson> =>
    from<Promise<INanolesson>>(
        axios.post<INanolesson>('/nanolessons', { ...nanolesson })
            .then(res => res.data),
    );

export const deleteNanolesson = (nanolessonId: string): Observable<string> =>
    from<Promise<string>>(
        axios.delete(`/nanolessons/${nanolessonId}`)
            .then(res => res.data),
    );
