import * as div from './nanolessons-admin.page.style';
import { PAGE_HEADER } from './utils/nanolesson-admin.utils';
import { PageHeader } from '../../../shared/components/page-header/page-header';
import { IDropdownOption } from '../../../shared/interfaces/dropdown';
import { addAppOverlay, removeAppOverlay } from '../../../shared/services/app-overlays.service';
import * as scroll from '../../../shared/style/scroll.style';
import { TaxonomyTree } from '../../../taxonomy/components/taxonomy-tree/taxonomy-tree';
import { TaxonomyLevel } from '../../../taxonomy/interfaces/taxonomy-level';
import { getTaxonomyTree, tree$ } from '../../../taxonomy/services/taxonomy.service';
import { ConfirmDelete } from '../../components/confirm-delete/confirm-delete';
import { NanolessonHoverMenus } from '../../components/nanolesson-hover-menus/nanolesson-hover-menus';
import { NanolessonsList } from '../../components/nanolessons-list/nanolessons-list';
import { INanolesson } from '../../interfaces/nano-lessons';
import { deleteNanolessonConfirmModal$, getNanolessons, nanolessons$ } from '../../services/nanolessons-admin.service';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props extends RouteComponentProps {}
interface State {
    width: number;
    tree: TaxonomyLevel[];

    /**
     * Send active option to pageheader (handle press here, in the parent)
     * Callback sets state with option clicked here
     */
    activeOption: IDropdownOption;

    /** Render nanolessons in custom list */
    nanolessons: INanolesson[];
}

/**
 * List of all nanolessons available for one user.
 * Create, Update, Edit, Delete nanolessons with an intuitive UI
 * Nanolessons that are available everywhere are also included
 */
class _NanolessonsAdminPage extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();
    private confirmDeleteModal: JSX.Element = <ConfirmDelete />;

    constructor(props: Props) {
        super(props);

        this.state = {
            width: Dimensions.get('window').width,
            tree: [],
            activeOption: null,
            nanolessons: [],
        };
    }

    public render() {
        const { width, nanolessons, tree } = this.state;

        return (
            <div.NanolessonAdminPage data-cy='nanolesson-admin-page'
                onLayout={e => this.updateWidth(e)}>

                {/** Nanolesson Menus */}
                <NanolessonHoverMenus />

                {/** ScrollView */}
                <scroll.ScrollView data-cy='scroll-view'
                    contentContainerStyle={scroll.inner.content}>

                    {/** Taxonomy tree */}
                    {
                        width >= 1650 &&
                        <TaxonomyTree levels={tree}
                            overrides={div.TAXONOMY_OVERRIDES} />
                    }

                    <scroll.Center width={div.calculateCenterWidth(width)}>

                        {/** Header */}
                        <PageHeader config={this.getPageHeader()}
                            overrides={div.PAGE_HEADER_OVERRIDES(width)} />

                        {/** Nanolessons List */}
                        <NanolessonsList nanolessons={nanolessons} />

                    </scroll.Center>
                </scroll.ScrollView>
            </div.NanolessonAdminPage>
        );
    }

    public componentDidMount() {
        this.subscribeToTaxonomyTree();
        this.subscribeToNanolessons();
        this.subscribeToConfirmDeleteModal();

        getTaxonomyTree();
        getNanolessons();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // ===== SUBSCRIBERS =====

    private subscribeToTaxonomyTree() {
        tree$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(tree => {
                this.setState({ tree });
            });
    }

    private subscribeToNanolessons() {
        nanolessons$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(nanolessons => {
                this.setState({ nanolessons });
            });
    }

    private subscribeToConfirmDeleteModal() {
        deleteNanolessonConfirmModal$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isVisible => {
                if (isVisible) {
                    addAppOverlay(this.confirmDeleteModal);
                } else {
                    removeAppOverlay(this.confirmDeleteModal);
                }
            });
    }

    // ===== UTILS =====

    private updateWidth(event: LayoutChangeEvent) {
        this.setState({
            width: event.nativeEvent.layout.width,
        });
    }

    private getPageHeader() {
        const { activeOption } = this.state;

        return PAGE_HEADER(
            this.onPageHeaderOptionClick.bind(this),
            this.onNewNanolessonClick.bind(this),
            activeOption,
        );
    }

    private onPageHeaderOptionClick(option: IDropdownOption) {
        const { history } = this.props;

        history.push({
            search: `?nanolessonsType=${option._id}`
        });

        this.setState({ activeOption: option });
    }

    private onNewNanolessonClick() {
        const { history } = this.props;

        history.push('/admin/nanolessons/save');
    }
}

export const NanolessonsAdminPage = withRouter<Props, any>(_NanolessonsAdminPage);
