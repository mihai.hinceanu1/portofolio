import styled from 'styled-components/native';

export const NanolessonAdminPage = styled.View`
    flex: 1;
    align-items: center;
    flex-direction: row;
`;

// ===== UTILS =====

export function calculateCenterWidth(width: number) {
    if (width <= 1650) {
        return width * 75 / 100;
    }

    return width;
}

// ===== OVERRIDES =====

export const TAXONOMY_OVERRIDES = {
    root: `
        position: absolute;
        left: 50px;
        top: 150px;
    `,
};

export const PAGE_HEADER_OVERRIDES = (width: number) => {
    let overrideStyles = {
        root: 'margin-top: 50px;',
    };

    if (width <= 1100) {
        overrideStyles.root = 'margin-top: 120px;';
    }

    return overrideStyles;
};
