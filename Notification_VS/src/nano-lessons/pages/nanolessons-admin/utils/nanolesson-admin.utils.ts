import { NEW_NANO } from '../../../../shared/assets/icons';
import { IDropdownOption } from '../../../../shared/interfaces/dropdown';
import { Filter, HeaderConfig, PageHeaderButton } from '../../../../shared/interfaces/page-header';
import * as qs from 'query-string';

export const PAGE_HEADER = (
    optionClick: (option: IDropdownOption) => void,
    onButtonClick: () => void,
    activeOption: IDropdownOption,
): HeaderConfig => {
    const title = 'Nanolesson Admin';

    const description = 'Learn programming like a professional';

    const options = [{
        _id: '1',
        label: 'All Nanos',
        callback: (option: IDropdownOption) => optionClick(option),
    }, {
        _id: '2',
        label: 'My nanos',
        callback: (option: IDropdownOption) => optionClick(option),
    }, {
        _id: '3',
        label: 'Later use',
        callback: (option: IDropdownOption) => optionClick(option),
    }];

    const button: PageHeaderButton = {
        text: 'New Nano',
        icon: {
            type: 'svg',
            svgPath: NEW_NANO,
        },
        onPress: () => onButtonClick(),
    };

    const filterBar = {
        filters: [{
            _id: '1',
            label: 'All',
            callback: () => console.log('All')
        }, {
            _id: '2',
            label: 'Only mine',
            callback: () => console.log('Only mine'),
        }],
        selectFilter: (filter: Filter) => console.log(filter),
    };

    return {
        title,
        description,
        options,
        activeOption: activeOption || options[0],
        button,
        filterBar,
    };
};

export const getCurrentQueryString = (search: string) => {
    return qs.parse(search);
};
