import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const CreateNanolessonPage = styled.View`
    flex-direction: column;
    align-items: flex-start;
`;

export const Content = styled.View`
    align-self: center;
    justify-content: space-evenly;
    flex-direction: row;
    width: 100%;
    margin-top: 50px;
`;

export const ErrorText = styled.Text`
    text-align: right;
    color: ${colors.$red};
    font-weight: 500;
    font-size: ${font.$size16}px;
    max-width: 1000px;
    width: 100%;
    align-self: center;
    margin-top: 20px;
`;
