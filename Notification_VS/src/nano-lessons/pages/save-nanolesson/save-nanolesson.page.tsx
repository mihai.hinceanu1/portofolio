import * as div from './save-nanolesson.page.style';
import { IconPicker } from '../../../icons/components/icon-picker/icon-picker';
import { IIcon } from '../../../icons/interfaces/icon';
import {
    icons$,
    setIcon,
    toggleIconPicker,
    toggleIconPicker$
    } from '../../../icons/services/icons.service';
import { NanolessonEdit } from '../../components/nanolesson-edit/nanolesson-edit';
import { SaveNanolessonHeader } from '../../components/save-nanolesson-header/save-nanolesson.header';
import { INanolesson } from '../../interfaces/nano-lessons';
import {
    clearCurrentNanolesson,
    currentNanolesson$,
    saveNanolesson,
    saveNanolessonError$,
    saveNanolessonResponse$
    } from '../../services/nanolessons-admin.service';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Props extends RouteComponentProps {}
interface State {
    nanolesson: INanolesson;
    icons: IIcon[];
    selectedIcon: IIcon;
    isIconPickerOpen: boolean;

    /** Display error from API call (below header) */
    error: string;
}

/**
 * Dynamically create/update nanolessons
 * Max number of tabs are 5. It has also an IconPicker that let's
 * user to choose different icons available in our platform
 */
class _SaveNanolessonPage extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state= {
            nanolesson: {} as INanolesson,
            icons: null,
            selectedIcon: null,
            isIconPickerOpen: false,
            error: null,
        };
    }

    public render() {
        const { icons, isIconPickerOpen, nanolesson, error } = this.state;

        return (
            <div.CreateNanolessonPage data-cy='create-nanolesson-page'>

                {/** Header */}
                <SaveNanolessonHeader onSavePress={() => this.onSavePress()}
                    nanolesson={nanolesson}
                    onCancelPress={() => this.onCancelPress()} />

                {/** Error */}
                {
                    !!error &&
                    <div.ErrorText data-cy='error-text'>
                        {error}
                    </div.ErrorText>
                }

                <div.Content data-cy='content'>

                    {/** Icon Picker */}
                    {
                        isIconPickerOpen &&
                        <IconPicker icons={icons}
                            onIconClick={(icon: IIcon) => this.onIconClick(icon)} />
                    }

                    {/** Nanolesson Edit */}
                    <NanolessonEdit />

                </div.Content>

            </div.CreateNanolessonPage>
        );
    }

    public componentDidMount() {
        this.subscribeToIcons();
        this.subscribeToToggleIconPicker();
        this.subscribeToCurrentNanolesson();
        this.subscribeToSaveNanolessonError();
        this.subscribeToSaveNanolessonResponse();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // ===== SUBSCRIBERS =====

    private subscribeToIcons() {
        icons$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(icons => {
                this.setState({ icons });
            });
    }

    private subscribeToToggleIconPicker() {
        toggleIconPicker$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(isIconPickerOpen => {
                this.setState({ isIconPickerOpen });
            });
    }

    private subscribeToCurrentNanolesson() {
        currentNanolesson$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(nanolesson => {
                this.setState({ nanolesson });
            });
    }

    private subscribeToSaveNanolessonError() {
        saveNanolessonError$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(error => {
                this.setState({ error });
            });
    }

    private subscribeToSaveNanolessonResponse() {
        const { history } = this.props;

        saveNanolessonResponse$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(response => {
                if (response) {
                    history.push('/admin/nanolessons');
                }
            });
    }

    // ===== EVENT HANDLERS =====

    private onIconClick(icon: IIcon) {
        setIcon(icon);
        toggleIconPicker(false);
    }

    private onSavePress() {
        const { nanolesson } = this.state;

        saveNanolesson(nanolesson);
    }

    private onCancelPress() {
        const { history } = this.props;

        clearCurrentNanolesson();
        history.push('/admin/nanolessons');
    }
}

export const SaveNanolessonPage = withRouter<Props, any>(_SaveNanolessonPage);
