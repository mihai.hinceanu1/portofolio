export enum NANOLESSON_DIFFICULTIES {
    Junior = 'Junior',
    Intermediate = 'Intermediate',
    Expert = 'Expert',
}
