export const NANOLESSON_TAB_WIDTH = 90;
export const NANOLESSON_TAB_HEIGHT = 80;
export const NANOLESSON_CONNECTOR_WIDTH = 40;
// export const NANOLESSON_CIRCLE_DIM = 10; // modified this to fit with topic marker in global map
export const NANOLESSON_CIRCLE_DIM = 13;
export const NANOLESSON_SPACING = 20;
export const NANOLESSON_CONTENT_WIDTH = 1.6 * NANOLESSON_TAB_WIDTH - 2 * NANOLESSON_SPACING;

export enum TOOLTIP_TYPE {
    'text',
    'image',
    'project',
    'topic',
}