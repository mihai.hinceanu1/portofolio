import { CommonMetadata } from '../../shared/interfaces/common-metadata';

export interface INanolesson extends CommonMetadata {
    /** Short nanolesson name */
    name: string;

    /** Nanolesson can be of 3 types */
    difficulty: NanolessonDifficulty;

    /** Allow nanolesson to be visible to other authors */
    reusable: boolean;

    /** Number of times nanolesson have been used in the app */
    used: number;

    /** Left side tabs, that switch content depending on the active one */
    tabs: INanolessonTab[];
}

export enum NanolessonDifficulty {
    Beginner = 'Beginner',
    Intermediate = 'Intermediate',
    Expert = 'Expert',
}

export interface INanolessonTab {
    /** Order of the tab in the left panel */
    order: number;

    /** Name that renders on top of the nanolesson */
    name: string;

    /** Title of the content */
    title: string;

    /** Specific icon for each tab */
    icon: string;

    /** Descriptive text that explains the concept of the current active tab */
    content?: string;

    /** Suggestive image representing the concept described */
    image?: string;

    /** Link to source (can be external or vsc) */
    link: INanoLink;
}

export interface INanoLink {
    /** Path to resource */
    url: string;

    /** Title displayed in button */
    title: string;
}
