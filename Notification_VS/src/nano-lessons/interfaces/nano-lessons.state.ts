import { NanolessonsAdminState } from './nanolesson-admin.state';
import { NanolessonsLessonState } from './nanolessons-lesson.state';

export interface NanolessonsState {

    /** Lesson Page Related */
    lesson: NanolessonsLessonState;

    /** Admin Dashboard Related */
    admin: NanolessonsAdminState;
}
