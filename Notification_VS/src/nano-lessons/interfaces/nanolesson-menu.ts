export interface INanolessonMenuItem {
    /** Used for React 'key' */
    id: number;

    /** Name rendered in UI */
    name: string;

    /** Search name in Query String */
    search: string;
}