import { INanolesson, INanolessonTab } from './nano-lessons';
import { FileInput } from '../../shared/interfaces/file-input';

export interface NanolessonsAdminState {
    /** All nanolessons fetch by userId */
    nanolessons: INanolesson[];

    /** Short text describing error when fetching nanolessons */
    error: string;

    /** Toggle Nanolesson Content Link Modal (both edit / create) */
    linkModalVisible: boolean;

    /** Toggle Nanolesson Content Image Modal for edit / add */
    imageModalVisible: boolean;

    currentNanolesson: INanolesson;

    currentTab: INanolessonTab;

    currentUploadedImage: FileInput;

    /** Upload modal related. Populate after SAVE button pressed */
    uploadImage: {
        response: string;

        error: string;
    };

    saveNanolesson: {
        response: INanolesson;

        error: string;
    };

    deleteNanolesson: {
        error: string;

        confirmModal: boolean;

        /**
         * id of nanolesson that, after confirmation, is sent to
         * the WebApi to be deleted
         */
        nanolessonId: string;
    }
}