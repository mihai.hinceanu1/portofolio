import { INanolesson } from './nano-lessons';

export interface NanolessonsLessonState {
    currentNanolesson: INanolesson;
}
