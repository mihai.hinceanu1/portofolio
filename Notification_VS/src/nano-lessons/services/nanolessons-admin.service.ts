import { FileInput } from '../../shared/interfaces/file-input';
import { store, store$ } from '../../shared/services/state.service';
import { INanolesson, INanolessonTab } from '../interfaces/nano-lessons';
import * as actions from '../state/actions/nanolessons-admin.actions';
import * as selectors from '../state/selectors/nanolessons-admin.selectors';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ===== NANOLESSONS =====

export function getNanolessons() {
    store.dispatch(
        actions.getNanolessonReq(),
    );
}

export const nanolessons$ = () => store$.pipe(
    map(state => selectors.NANOLESSONS(state)),
    skipWhile(nanolessons => !nanolessons),
    distinctUntilChanged(),
);

// ===== MODALS =====

export function toggleLinkModal(flag: boolean) {
    store.dispatch(
        actions.toggleLinkModal(flag),
    );
}

export const linkModalVisible$ = () => store$.pipe(
    map(state => selectors.LINK_MODAL_VISIBLE(state)),
    skipWhile(flag => flag === null),
    distinctUntilChanged(),
);

export function toggleImageModal(flag: boolean) {
    store.dispatch(
        actions.toggleImageModal(flag),
    );
}

export const imageModalVisible$ = () => store$.pipe(
    map(state => selectors.IMAGE_MODAL_VISIBLE(state)),
    skipWhile(flag => flag === null),
    distinctUntilChanged(),
);

// ===== CURRENT NANOLESSON =====

export function setCurrentNanolesson(nanolesson: INanolesson) {
    store.dispatch(
        actions.setCurrentNanolesson(nanolesson),
    );
}

export const currentNanolesson$ = () => store$.pipe(
    map(state => selectors.CURRENT_NANOLESSON(state)),
    distinctUntilChanged(),
);

export function setCurrentNanolessonTab(tab: INanolessonTab) {
    store.dispatch(
        actions.setCurrentNanolessonTab(tab),
    );
}

export const currentTab$ = () => store$.pipe(
    map(state => selectors.CURRENT_TAB(state)),
    distinctUntilChanged(),
);

export function clearCurrentNanolesson() {
    store.dispatch(
        actions.clearCurrentNanolesson(),
    );
}

export function setCurrentUploadedImage(image: FileInput) {
    store.dispatch(
        actions.setCurrentImageUploaded(image),
    );
}

export const currentUploadedImage$ = () => store$.pipe(
    map(state => selectors.CURRENT_UPLOADED_IMAGE(state)),
    skipWhile(image => !image),
    distinctUntilChanged(),
);

// ===== UPLOAD IMAGE =====

export function uploadNanoImage(image: FileInput) {
    store.dispatch(
        actions.uploadImageReq(image),
    );
}

export const uploadNanoImageResponse$ = () => store$.pipe(
    map(state => selectors.UPLOAD_IMAGE_RESPONSE(state)),
    distinctUntilChanged(),
);

export const uploadNanoImageError$ = () => store$.pipe(
    map(state => selectors.UPLOAD_IMAGE_ERROR(state)),
    distinctUntilChanged(),
);

// ====== SAVE NANOLESSON ======

export function saveNanolesson(nanolesson: INanolesson) {
    store.dispatch(
        actions.saveNanolessonReq(nanolesson),
    );
}

export const saveNanolessonResponse$ = () => store$.pipe(
    map(state => selectors.SAVE_NANOLESSON_RESPONSE(state)),
    distinctUntilChanged(),
);

export const saveNanolessonError$ = () => store$.pipe(
    map(state => selectors.SAVE_NANOLESSON_ERROR(state)),
    distinctUntilChanged(),
);

// ===== TOGGLE REUSABLE =====

export function toggleReusable() {
    store.dispatch(
        actions.toggleReusable(),
    );
}

// ===== DELETE NANOLESSON ======

export function deleteNanolesson(id: string) {
    store.dispatch(
        actions.deleteNanolessonReq(id),
    );
}

export const deleteNanolessonError$ = () => store$.pipe(
    map(state => selectors.DELETE_NANOLESSON_ERROR(state)),
    distinctUntilChanged(),
);

export function toggleDeleteNanolessonModal(flag: boolean) {
    store.dispatch(
        actions.showNanolessonDeleteConfirm(flag),
    );
}

export const deleteNanolessonConfirmModal$ = () => store$.pipe(
    map(state => selectors.SHOW_DELETE_NANOLESSON_CONFIRM(state)),
    distinctUntilChanged(),
);

export function setDeleteNanolessonId(id: string) {
    store.dispatch(
        actions.setDeleteNanolessonId(id),
    );
}

export const deleteNanolessonId$ = () => store$.pipe(
    map(state => selectors.DELETE_NANOLESSON_ID(state)),
    distinctUntilChanged(),
);
