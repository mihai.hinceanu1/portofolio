import { INanolessonTab } from '../interfaces/nano-lessons';

/** @TODO Move to utils */
export function recalculateNanolessonTabOrder(tabs: INanolessonTab[]) {
    return tabs.map((tab, index) => {
        tab.order = index;

        return tab;
    });
}
