import { store, store$ } from '../../shared/services/state.service';
import { INanolesson } from '../interfaces/nano-lessons';
import * as actions from '../state/actions/nanolessons-lesson.actions';
import * as selectors from '../state/selectors/nanolessons-lesson.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export function getCurrentNanolesson(id: string) {
    store.dispatch(
        actions.getNanolessonByIdReq(id),
    );
}

export const currentNanolesson$ = (): Observable<INanolesson> => store$.pipe(
    map(state => selectors.CURRENT_NANOLESSON(state)),
    skipWhile(nanolesson => nanolesson === null),
    distinctUntilChanged(),
);
