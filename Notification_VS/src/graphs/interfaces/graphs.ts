/** Screen dimensions defined in browser pixels. */
export interface GraphDimensions {
    width: number;
    height: number;
    image?: string; // TODO REVIEW, why is it here
    [key: string]: any; // TODO REVIEW, why is it here
}

/** Screen position means browser pixels on screen. */
export interface GraphPosition {
    x: number;
    y: number;
}