import { Feedback } from './feedback';

export interface FeedbackState {

    /** This is the full DTO of the current selected feedback. */
    feedback: Feedback;

    /** Stores the feedbacks summry DTOs. */
    feedbacks: Feedback[];

    /**
     * After a search query is finished the results are stored here.
     * This search is specialised solely on feedbacks.
     */
    searchFeedbacksResults: Feedback[];

}