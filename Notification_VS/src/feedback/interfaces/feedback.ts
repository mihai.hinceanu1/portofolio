import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/** Users can send feedback. */
export interface Feedback extends CommonMetadata {

    /** Name of the author */
    name: string;

    message: string;
    email: string;
}