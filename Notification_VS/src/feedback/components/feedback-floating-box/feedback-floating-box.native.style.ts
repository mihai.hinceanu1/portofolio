import { FEEDBACK_BUTTON_WIDTH } from '../../constants/feedback-dimensions.const';
import styled from 'styled-components/native';

export const FeedbackFloatingBox = styled.View`
    z-index: 5;
    width: 200px;
    height: 300px;
    bottom: 0;
    right: ${FEEDBACK_BUTTON_WIDTH}px;
    background-color: white;
    border-radius: 3px;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.3);
    padding: 10px;
`;
// position: fixed;