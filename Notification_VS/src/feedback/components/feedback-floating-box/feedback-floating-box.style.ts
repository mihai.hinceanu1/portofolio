import { FEEDBACK_BUTTON_WIDTH } from '../../constants/feedback-dimensions.const';
import styled, { keyframes } from 'styled-components';

const bringUp = keyframes`
    from {
        bottom: -250px;
    }
    to {
        bottom: 0px;
    }
`;

export const FeedbackFloatingBox = styled.div`
    z-index: 5;
    width: 200px;
    height: 250px;
    bottom: 0;
    animation: ${bringUp} 200ms ease-in;
    right: ${FEEDBACK_BUTTON_WIDTH}px;
    background-color: white;
    border-radius: 3px;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.3);
    padding: 10px;
`;
// position: fixed;