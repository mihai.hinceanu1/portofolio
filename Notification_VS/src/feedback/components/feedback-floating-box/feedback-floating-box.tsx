import * as div from './feedback-floating-box.style';
import { Button } from '../../../shared/components/button/button';
import { Input } from '../../../shared/components/input/input';
import { ButtonConfig } from '../../../shared/interfaces/button';
import { Feedback } from '../../interfaces/feedback';
import { createFeedback } from '../../services/feedback.service';
import * as React from 'react';
import { Text } from 'react-native';

interface Props { }

interface State {
    userName: string;
    userEmail: string;
    userFeedback: string;
}

export class FeedbackFloatingBox extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            userName: '',
            userEmail: '',
            userFeedback: '',
        };
    }

    public render() {
        const { userName, userEmail, userFeedback } = this.state;

        let buttonConfig: ButtonConfig = {
            text: 'Submit',
            onPress: () => this.handleSubmitFeedback(),
        };

        return (
            <div.FeedbackFloatingBox data-cy='feedback-floating-box'>

                <Text>Enter your feedback</Text>

                <Input label='Your name'
                    value={userName}
                    onChangeText={(value: string) => this.handleTextChange(value, 'userName')} />

                <Input label='Your email'
                    value={userEmail}
                    onChangeText={(value: string) => this.handleTextChange(value, 'userEmail')} />

                <Input label='Your feedback'
                    value={userFeedback}
                    onChangeText={(value: string) => this.handleTextChange(value, 'userFeedback')} />

                <Button config={buttonConfig}
                // text='Submit'
                // onPress={() => this.handleSubmitFeedback()}
                />

            </div.FeedbackFloatingBox>
        );
    }

    private handleTextChange(value: string, type: string) {
        this.setState({
            ...this.state,
            [type]: value,
        });
    }

    private handleSubmitFeedback() {
        const { userName, userEmail, userFeedback } = this.state;
        let feedbackConfig = {
            email: userEmail,
            message: userFeedback,
            name: userName,
        } as Feedback;

        this.setState({
            userEmail: '',
            userFeedback: '',
            userName: '',
        });

        createFeedback(feedbackConfig);
    }
}