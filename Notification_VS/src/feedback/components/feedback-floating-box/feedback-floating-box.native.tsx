import * as div from './feedback-floating-box.native.style';
import { Input } from '../../../shared/components/input/input';
import * as React from 'react';
import { Text } from 'react-native';

interface Props {}

interface State {
    userEmail: string;
    userFeedback: string;
}

export class FeedbackFloatingBox extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            userEmail: '',
            userFeedback: '',
        };
    }

    public render() {
        const { userEmail, userFeedback } = this.state;

        return (
            <div.FeedbackFloatingBox data-cy='feedback-floating-box'>

                <Text>Enter your feedback</Text>

                <Input
                    label='Your email'
                    value={userEmail}
                    onChangeText={() => {}}/>

                <Input
                    label='Your feedback'
                    value={userFeedback}
                    onChangeText={() => {}}/>

            </div.FeedbackFloatingBox>
        );
    }
}