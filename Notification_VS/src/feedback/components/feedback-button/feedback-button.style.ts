import { colors } from '../../../shared/style/colors';
import { FEEDBACK_BUTTON_WIDTH } from '../../constants/feedback-dimensions.const';
import styled from 'styled-components/native';

export const FeedbackButton = styled.TouchableOpacity`
    bottom: 10px;
    right: 0;
    width: ${FEEDBACK_BUTTON_WIDTH}px;
    height: 100px;
    z-index: 5;
    background-color: ${colors.$activeCodeTab};
    align-items: center;
    justify-content: center;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
`;
// position: fixed;

export const FeedbackButtonText = styled.Text`
    transform: rotate(90deg);
    color: white;
    font-weight: 600;
`;