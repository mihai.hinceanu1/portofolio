import * as div from './feedback-button.style';
import * as React from 'react';

interface Props {
    handleFeedbackClick: () => void;
}

interface State { }

// TODO convert to functional component
export class FeedbackButton extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        const { handleFeedbackClick } = this.props;

        return (
            <div.FeedbackButton data-cy='feedback-button' onPress={handleFeedbackClick}>

                <div.FeedbackButtonText>Feedback</div.FeedbackButtonText>

            </div.FeedbackButton>
        );
    }
}