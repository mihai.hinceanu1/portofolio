import * as actions from './feedback.actions';
import { feedbackInitialState } from './feedback.init-state';
import { Feedback } from '../interfaces/feedback';
import { FeedbackState } from '../interfaces/feedback.state';

export const feedbackReducer = (state: FeedbackState = feedbackInitialState, action: any): FeedbackState => {

    switch (action.type) {

        case actions.GET_FEEDBACK_OK: {
            return {
                ...state,
                feedback: action.payload as Feedback,
            };
        }

        case actions.GET_ALL_FEEDBACKS_OK: {
            return {
                ...state,
                feedbacks: action.payload as Feedback[],
            };
        }

        case actions.SEARCH_FEEDBACKS_OK: {
            return {
                ...state,
                searchFeedbacksResults: action.payload as Feedback[],
            };
        }

        default:
            return state;
    }

};
