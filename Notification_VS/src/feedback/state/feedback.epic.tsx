import * as feedbackActions from './feedback.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { Action, Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { Feedback } from '../interfaces/feedback';
import * as feedbackService from '../services/feedback.service';
import * as feedbackWebapi from '../webapis/feedback.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getFeedback$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(feedbackActions.GET_FEEDBACK_REQ).pipe(
        map(action => action.payload),
        concatMap(id => feedbackWebapi.getFeedback(id).pipe(
            map(feedback => feedbackActions.getFeedbackOk(feedback)),
            catchError(error => of(feedbackActions.getFeedbackFail(error))),
        )),
    );

export const editFeedback$ = (action$: ActionsObservable<Action<Feedback>>, _store: Store<AppState>) =>
    action$.ofType(feedbackActions.EDIT_FEEDBACK_REQ).pipe(
        map(action => action.payload),
        concatMap(request => feedbackWebapi.editFeedback(request).pipe(
            map(() => {
                feedbackService.getAllFeedbacks();
                return feedbackActions.editFeedbackOk();
            }),
            catchError(error => of(feedbackActions.editFeedbackFail(error))),
        )),
    );

export const deleteFeedback$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(feedbackActions.DELETE_FEEDBACK).pipe(
        map(action => action.payload),
        concatMap(id => feedbackWebapi.deleteFeedback(id).pipe(
            map(() => {
                feedbackService.getAllFeedbacks();
                return feedbackActions.deleteFeedbackOk();
            }),
            catchError(error => of(feedbackActions.deleteFeedbackFail(error))),
        )),
    );

export const createFeedback$ = (action$: ActionsObservable<Action<Feedback>>, _store: Store<AppState>) =>
    action$.ofType(feedbackActions.CREATE_FEEDBACK_REQ).pipe(
        map(action => action.payload),
        concatMap(feedback => feedbackWebapi.createFeedback(feedback).pipe(
            map(() => {
                feedbackService.getAllFeedbacks();
                return feedbackActions.createFeedbackOk();
            }),
            catchError(error => of(feedbackActions.createFeedbackFail(error))),
        )),
    );

export const getAllFeedbacks$ = (action$: ActionsObservable<Action<Paginated>>, _store: Store<AppState>) =>
    action$.ofType(feedbackActions.GET_ALL_FEEDBACKS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => feedbackWebapi.getAllFeedbacks(request).pipe(
            map(feedbacks => feedbackActions.getAllFeedbacksOk(feedbacks)),
            catchError(error => of(feedbackActions.getAllFeedbacksFail(error))),
        )),
    );

export const searchFeedbacks = (action$: ActionsObservable<Action<PaginatedSearchReq>>, _store: Store<AppState>) =>
    action$.ofType(feedbackActions.SEARCH_FEEDBACKS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => feedbackWebapi.searchFeedbacksByKeyword(request).pipe(
            map(feedbacks => feedbackActions.searchFeedbacksOk(feedbacks)),
            catchError(error => of(feedbackActions.searchFeedbacksFail(error))),
        )),
    );