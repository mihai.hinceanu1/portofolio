import { AppState } from '../../shared/interfaces/app.state';
import { Feedback } from '../interfaces/feedback';
import { FeedbackState } from '../interfaces/feedback.state';
import { createSelector, Selector } from 'reselect';

const FEEDBACK_MODULE: Selector<AppState, FeedbackState> = (state: AppState) => state.feedback;

/** List feedbacks details in admin */
export const FEEDBACK = createSelector<AppState, FeedbackState, Feedback>(
    FEEDBACK_MODULE,
    (state: FeedbackState) => state.feedback,
);

/** List all feedbacks in admin */
export const FEEDBACKS = createSelector<AppState, FeedbackState, Feedback[]>(
    FEEDBACK_MODULE,
    (state: FeedbackState) => state.feedbacks,
);

export const SEARCH_FEEDBACKS_RESULTS = createSelector<AppState, FeedbackState, Feedback[]>(
    FEEDBACK_MODULE,
    (state: FeedbackState) => state.searchFeedbacksResults,
);