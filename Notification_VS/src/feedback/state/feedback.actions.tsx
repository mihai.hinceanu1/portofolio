import { Action, Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { Feedback } from '../interfaces/feedback';

// ====== GET FEEDBACK ======

export const GET_FEEDBACK_REQ = 'GET_FEEDBACK_REQ';
export const getFeedbackReq = (id: string): Action<string> => ({
    type: GET_FEEDBACK_REQ,
    payload: id,
});

export const GET_FEEDBACK_OK = 'GET_FEEDBACK_OK';
export const getFeedbackOk = (feedback: Feedback): Action<Feedback> => ({
    type: GET_FEEDBACK_OK,
    payload: feedback,
});

export const GET_FEEDBACK_FAIL = 'GET_FEEDBACK_FAIL';
export const getFeedbackFail = (error: Error): Action<Error> => ({
    type: GET_FEEDBACK_FAIL,
    payload: error,
});

// ====== EDIT FEEDBACK ======

export const EDIT_FEEDBACK_REQ = 'EDIT_FEEDBACK_REQ';
export const editFeedbackReq = (feedback: Feedback): Action<Feedback> => ({
    type: EDIT_FEEDBACK_REQ,
    payload: feedback,
});

export const EDIT_FEEDBACK_OK = 'EDIT_FEEDBACK_OK';
export const editFeedbackOk = (): Action<null> => ({
    type: EDIT_FEEDBACK_OK,
});

export const EDIT_FEEDBACK_FAIL = 'EDIT_FEEDBACK_FAIL';
export const editFeedbackFail = (error: Error): Action<Error> => ({
    type: EDIT_FEEDBACK_FAIL,
    payload: error,
});

// ====== DELETE FEEDBACK ======

export const DELETE_FEEDBACK = 'DELETE_FEEDBACK';
export const deleteFeedback = (id: string): Action<string> => ({
    type: DELETE_FEEDBACK,
    payload: id,
});

export const DELETE_FEEDBACK_OK = 'DELETE_FEEDBACK_OK';
export const deleteFeedbackOk = (): Action<null> => ({
    type: DELETE_FEEDBACK_OK,
});

export const DELETE_FEEDBACK_FAIL = 'DELETE_FEEDBACK_FAIL';
export const deleteFeedbackFail = (error: Error): Action<Error> => ({
    type: DELETE_FEEDBACK_FAIL,
    payload: error,
});

// ====== CREATE FEEDBACK ======

export const CREATE_FEEDBACK_REQ = 'CREATE_FEEDBACK_REQ';
export const createFeedbackReq = (feedback: Feedback): Action<Feedback> => ({
    type: CREATE_FEEDBACK_REQ,
    payload: feedback,
});

export const CREATE_FEEDBACK_OK = 'CREATE_FEEDBACK_OK';
export const createFeedbackOk = (): Action<null> => ({
    type: CREATE_FEEDBACK_OK,
});

export const CREATE_FEEDBACK_FAIL = 'CREATE_FEEDBACK_FAIL';
export const createFeedbackFail = (error: Error): Action<Error> => ({
    type: CREATE_FEEDBACK_FAIL,
    payload: error,
});

// ====== GET ALL FEEDBACKS ======

export const GET_ALL_FEEDBACKS_REQ = 'GET_ALL_FEEDBACKS_REQ';
export const getAllFeedbacksReq = (request: Paginated): Action<Paginated> => ({
    type: GET_ALL_FEEDBACKS_REQ,
    payload: request,
});

export const GET_ALL_FEEDBACKS_OK = 'GET_ALL_FEEDBACKS_OK';
export const getAllFeedbacksOk = (feedbacks: Feedback[]): Action<Feedback[]> => ({
    type: GET_ALL_FEEDBACKS_OK,
    payload: feedbacks,
});

export const GET_ALL_FEEDBACKS_FAIL = 'GET_ALL_FEEDBACKS_FAIL';
export const getAllFeedbacksFail = (error: Error): Action<Error> => ({
    type: GET_ALL_FEEDBACKS_FAIL,
    payload: error,
});

// ====== SEARCH FEEDBACKS ======

export const SEARCH_FEEDBACKS_REQ = 'SEARCH_FEEDBACKS_REQ';
export const searchFeedbacksReq = (request: PaginatedSearchReq): Action<PaginatedSearchReq> => ({
    type: SEARCH_FEEDBACKS_REQ,
    payload: request,
});

export const SEARCH_FEEDBACKS_OK = 'SEARCH_FEEDBACKS_OK';
export const searchFeedbacksOk = (feedbacks: Feedback[]): Action<Feedback[]> => ({
    type: SEARCH_FEEDBACKS_OK,
    payload: feedbacks,
});

export const SEARCH_FEEDBACKS_FAIL = 'SEARCH_FEEDBACKS_FAIL';
export const searchFeedbacksFail = (error: Error): Action<Error> => ({
    type: SEARCH_FEEDBACKS_FAIL,
    payload: error,
});