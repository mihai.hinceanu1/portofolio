import { APP_CFG } from '../../shared/config/app.config';
import { Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { Feedback } from '../interfaces/feedback';
import axios from 'axios';
import { from, Observable } from 'rxjs';

export const getFeedback = (id: string): Observable<Feedback> =>
    from<Promise<Feedback>>(
        axios.get<Feedback>(`${APP_CFG.api}/feedback/feedbacks/${id}`)
            .then(res => res.data),
    );

export const editFeedback = (feedback: Feedback): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`${APP_CFG.api}/feedback/feedbacks/${feedback._id}`, feedback)
            .then(res => res.data),
    );

export const deleteFeedback = (id: string): Observable<null> =>
    from<Promise<null>>(
        axios.delete(`${APP_CFG.api}/feedback/feedbacks/${id}`)
            .then(res => res.data),
    );

export const createFeedback = (feedback: Feedback): Observable<null> =>
    from<Promise<null>>(
        axios.post<null>(`${APP_CFG.api}/feedback/feedbacks`, feedback)
            .then(res => res.data),
    );

export const getAllFeedbacks = (
    paginated: Paginated = { page: 0, perPage: 20 },
): Observable<Feedback[]> =>
    from<Promise<Feedback[]>>(
        axios.get<Feedback[]>(`${APP_CFG.api}/feedback/feedbacks/${paginated.page}/${paginated.perPage}`)
            .then(res => res.data),
    );

export const searchFeedbacksByKeyword = (request: PaginatedSearchReq): Observable<Feedback[]> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<Feedback[]>>(
        axios.get<Feedback[]>(`${APP_CFG.api}/feedback/feedbacks/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};