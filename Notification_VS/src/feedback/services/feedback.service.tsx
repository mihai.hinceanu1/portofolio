import { Paginated, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { store, store$ } from '../../shared/services/state.service';
import { Feedback } from '../interfaces/feedback';
import * as actions from '../state/feedback.actions';
import * as sel from '../state/feedback.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// ====== GET FEEDBACK ======

export function getFeedback(id: string) {
    store.dispatch(
        actions.getFeedbackReq(id),
    );
}

export const feedback$ = () => store$.pipe(
    map(state => sel.FEEDBACK(state)),
    skipWhile( feedback => !feedback ),
    distinctUntilChanged(),
);

// ====== EDIT FEEDBACK ======

export function editFeedback(feedback: Feedback) {
    store.dispatch(
        actions.editFeedbackReq(feedback),
    );
}

// ====== DELETE FEEDBACK ======

export function deleteFeedback(id: string) {
    store.dispatch(
        actions.deleteFeedback(id),
    );
}

// ====== CREATE FEEDBACK ======

export function createFeedback(feedback: Feedback) {
    store.dispatch(
        actions.createFeedbackReq(feedback),
    );
}

// ====== GET ALL FEEDBACKS ======

export function getAllFeedbacks(request?: Paginated) {
    store.dispatch(
        actions.getAllFeedbacksReq(request),
    );
}

export const feedbacks$ = (): Observable<Feedback[]> => store$.pipe(
    map(state => sel.FEEDBACKS(state)),
    skipWhile( feedbacks => !feedbacks ),
    distinctUntilChanged(),
);

// ======  SEARCH FEEDBACKS ======

export function searchFeedbacks(request: PaginatedSearchReq) {
    store.dispatch(
        actions.searchFeedbacksReq(request),
    );
}

export const feedbacksSearchResults$ = (): Observable<Feedback[]> => store$.pipe(
    map(state => sel.SEARCH_FEEDBACKS_RESULTS(state)),
    skipWhile( feedbacks => !feedbacks ),
    distinctUntilChanged(),
);