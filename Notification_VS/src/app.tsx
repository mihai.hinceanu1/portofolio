import { appRoutes } from './app.routes';
import * as div from './app.style';
import { LoginResponse } from './auth/interfaces/login';
import { loginResponse$ } from './auth/services/auth.service';
import { getIcons } from './icons/services/icons.service';
import { ToastrOverlay } from './shared/components/toastr/toastr-overlay';
import { addAppOverlay, appOverlays$ } from './shared/services/app-overlays.service';
import { screenClicked, screenTouched } from './shared/services/self-close-signal.service';
import { persistor, store } from './shared/services/state.service';
import { colors } from './shared/style/colors';
import { font } from './shared/style/font-sizes';
import * as React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Provider } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { PersistGate } from 'redux-persist/es/integration/react';

// All variables bellow as model variables can be used everywhere, without importing (web & mobile).
EStyleSheet.build({
    ...colors,
    ...font,
});

interface Props extends RouteComponentProps<never> { }

interface State {
    loginResponse: LoginResponse;
    appOverlays: JSX.Element[];
}

class _App extends React.Component<Props, State> {

    constructor(props: Props, context: any) {
        super(props, context);
        this.state = {
            loginResponse: null,
            appOverlays: null,
        };
    }

    public render() {
        const { loginResponse, appOverlays } = this.state;

        // REVIEW
        // No empty render
        // if (!loginResponse) { return null; }

        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <div.AppSafeArea data-cy='app-safe-area'
                        onTouchStart={e => screenTouched(e)}
                        onClick={e => screenClicked(e)}>

                        {/* RESTORE after login page fix */}
                        {/* Page Loading */}
                        {/* <PageLoadingOverlay /> */}

                        {/* Overlays */}
                        {appOverlays}

                        {/* App Routes */}
                        <>{appRoutes(loginResponse)}</>

                    </div.AppSafeArea>
                </PersistGate>
            </Provider>
        );
    }

    public componentDidMount() {
        this.subscribeToLoginResponse();
        this.subscribeToAppOverlays();
        this.addActionConfirmationOverlay();

        getIcons();
    }

    private subscribeToLoginResponse() {
        loginResponse$()
            .subscribe(loginResponse => {
                this.setState({ loginResponse });
            });
    }

    private subscribeToAppOverlays() {
        appOverlays$
            .subscribe(appOverlays => {
                this.setState({ appOverlays });
            });
    }

    private addActionConfirmationOverlay() {
        let actionConfirmOverlay: JSX.Element = <ToastrOverlay />;

        addAppOverlay(actionConfirmOverlay);
    }

}

export const App = withRouter(_App);
