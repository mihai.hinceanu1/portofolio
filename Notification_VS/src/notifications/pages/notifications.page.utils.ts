//Constants
export const ACTIVITYFEED = 'Activity Feed';
export const FILTERS = 'Filters';
export const RECENT = 'Recent';
export const UNREAD = 'Unread';
export const MESSAGEBOARD = 'Message Board';
