import styled from 'styled-components/native';
import { MOBILE_WIDTH } from '../../shared/constants/adaptive.const';

interface NotificationsProps {
    override: number;
}

export const NotificationsPage = styled.View<NotificationsProps>`
    width: 100%;
    height: 100%;
    display: ${props => props.override <=  MOBILE_WIDTH ? 'initial' : 'flex'};
    flex-direction: ${props => props.override <=  MOBILE_WIDTH ? 'column' : 'row'};
    justify-content: center;
    padding-top: 80px;
    overflow-y: scroll;
`;

export const LeftSection = styled.View<NotificationsProps>`
    flex-direction: ${props => props.override <= MOBILE_WIDTH ? 'row' : 'none'};
`;

export const CenterSection = styled.View`

`;

export const RightSection = styled.View`

`;