import * as div from './notifications.page.style';
import * as CONSTANT from './notifications.page.utils';
import { ActivityFeed } from '../components/activity-feed/activity-feed';
import { Filters } from '../components/filters/filters';
import { MessageBoard } from '../components/message-board/message-board';
import { Notifications } from '../components/notification/notification';
import { Panel } from '../components/panel/notification-panel';
import { IActivityFeed } from '../interfaces/activity-feed.interface';
import { IMessage } from '../interfaces/messages.interface';
import { INotification } from '../interfaces/notification.interface';
import * as React from 'react';
import { Dimensions, LayoutChangeEvent } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
    getMessages,
    getNotifications,
    messages$,
    notifications$,
    getFilteredNotifications,
    getNumberOfMessages
} from '../services/notifications.service';

interface Props extends RouteComponentProps { }
interface State {
    unread: INotification[];
    recent: INotification[];
    activityFeed: IActivityFeed;
    messages: IMessage[];
    screenWidth: number;
}

class _NotificationsPage extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            unread: [],
            recent: [],
            activityFeed: {notifications: 0, messages: 0},
            messages: [],
            screenWidth: Dimensions.get('screen').width,
        };
    }

    public render() {
        const { unread, recent, messages, activityFeed, screenWidth } = this.state;

        return (
            <div.NotificationsPage data-cy='notifications-page' onLayout={(e) => this.updateWidth(e)} override={screenWidth}>

                {/** Details Section */}
                <div.LeftSection override={screenWidth} data-cy='section'>

                    {/** Activity Feed */}
                    <Panel title={CONSTANT.ACTIVITYFEED}>
                        <ActivityFeed activityFeed={activityFeed} />
                    </Panel>

                    {/** Filters */}
                    <Panel title={CONSTANT.FILTERS}>
                        <Filters />
                    </Panel>
                </div.LeftSection>

                {/** Notifications Panel */}
                <div.CenterSection data-cy='section'>

                    {/** Unread */}
                    <Panel title={CONSTANT.UNREAD}>
                        <Notifications notifications={unread} />
                    </Panel>

                    {/** Recent */}
                    <Panel title={CONSTANT.RECENT}>
                        <Notifications notifications={recent} />
                    </Panel>
                </div.CenterSection>

                {/** Messages Panel */}
                <div.RightSection data-cy='section'>

                    {/** Message Board */}
                    <Panel title={CONSTANT.MESSAGEBOARD}>
                        <MessageBoard messages={messages} />
                    </Panel>
                </div.RightSection>

            </div.NotificationsPage>
        );
    }

    public componentDidMount() {
        this.subscribeToNotifications();
        this.subscribeToMessages();

        getNotifications();
        getMessages();
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    private updateWidth(e: LayoutChangeEvent) {
        const size = e.nativeEvent.layout.width;
        this.setState({screenWidth: size});
    }

    private subscribeToMessages() {
        messages$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(messages => {
                let obj = getNumberOfMessages(messages);
                
                this.setState({messages: obj.result, activityFeed: { ...this.state.activityFeed, messages: obj.countMessage}})
            });
    }

    private subscribeToNotifications() {
        notifications$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(notifications => {

                // subscribe to all notifications and then filter all using service function
                let obj = getFilteredNotifications(notifications);

                this.setState({unread: obj.unread, recent: obj.recent, activityFeed: {...this.state.activityFeed, notifications: obj.countUnread}});
            });
    }
}

export const NotificationsPage = withRouter<Props, any>(_NotificationsPage);
