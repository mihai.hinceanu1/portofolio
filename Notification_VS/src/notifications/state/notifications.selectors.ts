import { AppState } from '../../shared/interfaces/app.state';
import { createSelector, Selector } from 'reselect';

const NOTIFICATION_MODULE: Selector<AppState, any> = (state: AppState) => state.notifications;

export const MESSAGE = createSelector<AppState, any, any[]>(
    NOTIFICATION_MODULE,
    (state: any) => {
        return state.messages
    }
);

export const NOTIFICATION = createSelector<AppState, any, any[]>(
    NOTIFICATION_MODULE,
    (state: any) => state.notifications
);