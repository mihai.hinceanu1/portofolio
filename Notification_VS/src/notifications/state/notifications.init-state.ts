import { NotificationState } from "../interfaces/notification-state.interface";
import { INotification } from "../interfaces/notification.interface";
import { IMessage } from '../interfaces/messages.interface';


export const notificationsInitialState: NotificationState = {

    // Array with notifications
    notifications: [] as INotification[],

    // Array with messages
    messages: [] as IMessage[],
}