import * as Actions from './notifications.actions';
import { AppState } from '../../shared/interfaces/app.state';
import { Action } from '../../shared/interfaces/shared';
import * as notification_webApi from '../webapis/notifications.webapi';
import * as message_webApi from '../webapis/messages.webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

export const getNotificationArray$ = (action$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    action$.ofType(Actions.GET_NOTIFICATIONS_REQ).pipe(
        map(action => action.payload),
        concatMap(() => notification_webApi.getNotifications().pipe(
            map(arr => Actions.getNotificationsOk(arr)),
            catchError(err => of(Actions.getNotificationsFail(err)))
        )
        ));

export const getMessageArray$ = (action$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    action$.ofType(Actions.GET_MESSAGES_REQ).pipe(
        map(action => action.payload),
        concatMap(() => message_webApi.getMessages().pipe(
            map(arr => Actions.getMessagesOk(arr)),
            catchError(err => of(Actions.getMessagesFail(err)))
        )
        ));