import * as Actions from './notifications.actions';
import { notificationsInitialState } from './notifications.init-state';
import { NotificationState } from '../interfaces/notification-state.interface';

export const notificationsReducer = (state: NotificationState = notificationsInitialState, action: any): NotificationState => {
    switch (action.type) {
        case Actions.GET_NOTIFICATIONS_OK: {
            return {
                ...state,
                notifications: action.payload as any
            };
        }

        case Actions.GET_NOTIFICATIONS_FAIL: {
            return {
                ...state,
                // arrayError: { type: 'fail', message: 'Fail to load Notifications !!' },
            };
        }

        case Actions.GET_MESSAGES_OK: {
            return {
                ...state,
                messages: action.payload as any
            };
        }

        case Actions.GET_MESSAGES_FAIL: {
            return {
                ...state,
                // arrayError: { type: 'fail', message: 'Fail to load Notifications !!' },
            };
        }

        default:
            return state;
    }
}