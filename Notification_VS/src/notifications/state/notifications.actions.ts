import { Action } from '../../shared/interfaces/shared';
import { INotification } from '../interfaces/notification.interface';

// Notifications requests actions
export const GET_NOTIFICATIONS_REQ = 'GET_NOTIFICATIONS_REQ';
export const getNotificationsReq = (): Action<null> => ({
    type: GET_NOTIFICATIONS_REQ,
    payload: null,
});

export const GET_NOTIFICATIONS_OK = 'GET_NOTIFICATIONS_OK';
export const getNotificationsOk = (notifications: INotification): Action<INotification> => ({
    type: GET_NOTIFICATIONS_OK,
    payload: notifications
});

export const GET_NOTIFICATIONS_FAIL = 'GET_NOTIFICATIONS_FAIL';
export const getNotificationsFail = (error: Error): Action<Error> => ({
    type: GET_NOTIFICATIONS_FAIL,
    payload: error
});

// Messages requests actions
export const GET_MESSAGES_REQ = 'GET_MESSAGES_REQ';
export const getMessagesReq = (): Action<null> => ({
    type: GET_MESSAGES_REQ,
    payload: null,
});

export const GET_MESSAGES_OK = 'GET_MESSAGES_OK';
export const getMessagesOk = (messages: any): Action<any> => ({
    type: GET_MESSAGES_OK,
    payload: messages
});

export const GET_MESSAGES_FAIL = 'GET_MESSAGES_FAIL';
export const getMessagesFail = (error: Error): Action<Error> => ({
    type: GET_MESSAGES_FAIL,
    payload: error
});