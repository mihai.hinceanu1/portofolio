import { store, store$ } from '../../shared/services/state.service';
import * as actions from '../state/notifications.actions';
import * as selectors from '../state/notifications.selectors';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';
import { INotification } from '../interfaces/notification.interface';
import { IFilteredNotifications } from '../interfaces/filtered-notifications.interface';
import { IMessage } from '../interfaces/messages.interface';

export function getNotifications() {
    store.dispatch(
        actions.getNotificationsReq()
    );
}

export const messages$ = () => store$.pipe(
    map(state => selectors.MESSAGE(state)),
    skipWhile(arr => arr === undefined),
    distinctUntilChanged()
);

export function getMessages() {
    store.dispatch(
        actions.getMessagesReq()
    );
}

export const notifications$ = () => store$.pipe(
    map(state => selectors.NOTIFICATION(state)),
    skipWhile(arr => arr === undefined),
    distinctUntilChanged()
);

export const getFilteredNotifications = (notifications: INotification[]): IFilteredNotifications => {

    let result: IFilteredNotifications = { unread: [], recent: [], countUnread: 0, countRecent: 0};

    result.unread = notifications.filter(unread => unread.unread === true);
    result.countUnread = result.unread.length;

    result.recent = notifications.filter(recent => recent.unread === false);
    result.countRecent = result.recent.length;
    
    return result
}

export const getNumberOfMessages = (messages: IMessage[]) => {

    let result: IMessage[] = [];

    // In message board are displayed a specific number of messages. Now is set to 5
    result = messages.slice(0, 5);

    return { result, countMessage: messages.length}
}

export const getActivityFeed = () => {
    
}


