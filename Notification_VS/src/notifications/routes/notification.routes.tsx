import { LoginResponse } from '../../auth/interfaces/login';
import { renderPage } from '../../shared/services/utils.service';
import { NotificationsPage } from '../pages/notifications.page';
import * as React from 'react';
import { Route } from 'react-router';

export const notificationRoutes = (loginResponse: LoginResponse) => {
    return [
        <Route path='/notifications' key='notifications-page'
            render={props => renderPage(props, true, '/auth/login', NotificationsPage, loginResponse)} />
    ];
};
