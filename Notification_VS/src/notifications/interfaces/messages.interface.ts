export interface IMessage {
    _id: string;
    title: string;
    autor: string;
    message: string;
    isUnread: boolean;
}