import { INotification } from "./notification.interface";
import { IMessage } from "./messages.interface";

export interface NotificationState {
    notifications: INotification[];
    messages: IMessage[];
}