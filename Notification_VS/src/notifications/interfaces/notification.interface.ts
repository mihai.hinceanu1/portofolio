import { CommonMetadata } from '../../shared/interfaces/common-metadata';

export interface INotification extends CommonMetadata {
    _id: string;
    icon: string;
    title: string;
    howOld: string;
    type: string;
    unread: boolean;
}