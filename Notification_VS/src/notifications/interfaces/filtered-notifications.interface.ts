import { INotification } from './notification.interface';

export interface IFilteredNotifications {
    unread: INotification[];
    recent: INotification[];

    countUnread: number;
    countRecent: number;
}