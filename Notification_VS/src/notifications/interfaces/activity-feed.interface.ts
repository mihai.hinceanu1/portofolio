export interface IActivityFeed {
    // number of messages but not sure if it's about all or unreaded messages;
    messages: number;

    // number of notifications but not sure if it's about recent or unreaded notifications;
    notifications: number;

}