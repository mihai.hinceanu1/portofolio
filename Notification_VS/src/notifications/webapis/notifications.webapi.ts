import axios from 'axios';
import { from, Observable } from 'rxjs';

export const getNotifications = (): Observable<any> => from<Promise<any>>(
    axios.get('http://localhost:8080/api/notifications').then(res => res.data)
)
