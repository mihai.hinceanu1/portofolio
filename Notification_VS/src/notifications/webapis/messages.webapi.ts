import axios from 'axios';
import { from, Observable } from 'rxjs';

export const getMessages = (): Observable<any> => from<Promise<any>>(
    axios.get('http://localhost:8080/api/messages').then(res => res.data)
)