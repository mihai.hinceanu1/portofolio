import * as React from 'react';
import * as div from './message-item.style';
import { IMessage } from '../../../interfaces/messages.interface';

interface Props {
    message: IMessage;
}

export const MessageItem: React.FunctionComponent<Props> = (props: Props) => {
    const { message } = props;

    return (
        <div.MessageItem data-cy='message-item'>
            <div.Header data-cy='header'>
                <div.Title data-cy='title'>{message.title}</div.Title>
                <div.Autor data-cy='autor'>- {message.autor}</div.Autor>
            </div.Header>

            <div.Body data-cy='body'>
                <div.Text data-cy='text'>{message.message}</div.Text>
            </div.Body>
        </div.MessageItem>
    )
}