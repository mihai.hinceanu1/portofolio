import styled from 'styled-components/native';
import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';

export const MessageItem = styled.View`
    width: auto;
    height: auto;
    padding: 10px 0;
`;

export const Header = styled.View`
    width: auto;
    height: 25px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const Title = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size14}px;
    color: ${colors.$black};
`;

export const Autor = styled.Text`
    margin-left: 5px;
    font-family: sans-serif;
    font-size: ${font.$size12}px;
    color: ${colors.$grey};
`;

export const Body = styled.View`
    width: auto;
    height: 30px;
    justify-content: center;
`;

export const Text = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size12}px;
    color: ${colors.$darkGrey};
    padding: 3px 0;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;