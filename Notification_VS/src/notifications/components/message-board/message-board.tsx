import * as React from 'react';
import * as div from './message-board.style';
import { IMessage } from '../../interfaces/messages.interface';
import { ContainerActions } from '../panel/actions/container-actions';
import { MessageItem } from './utils/message-item';

interface Props {
    messages: IMessage[]
}

export const MessageBoard: React.FunctionComponent<Props> = (props: Props) => {

    const { messages } = props;

    return (
        <div.Messageboard data-cy='message-board'>

            {/* If messages array is empty, show a message */}
            {
                (messages.length === 0) &&
                <div.NoMessage data-cy='no-message'>
                    <div.Text data-cy='text'>No message found!</div.Text>
                </div.NoMessage>
            }

            {/* Messages list section */}
            <div.Section data-cy='section'>
                {
                    messages.map(message =>
                        <MessageItem message={message} key={message._id} />
                    )
                }
            </div.Section>

            <ContainerActions>
                <div.Action onPress={() => console.log('View all messages !!')} data-cy='action'>
                    <div.ActionText data-cy='action-text'>View all messages</div.ActionText>
                </div.Action>
            </ContainerActions>

        </div.Messageboard>
    );
};