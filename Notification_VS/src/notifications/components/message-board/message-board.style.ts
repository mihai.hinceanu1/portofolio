import styled from 'styled-components/native';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';

interface MessageBoardProps {
    override?: string;
}

export const Messageboard = styled.View<MessageBoardProps>`
    min-height: 90px;
    width: 300px;
    ${props => props.override};
`;

export const Section = styled.View`
    padding: 0 15px;
`;

export const NoMessage = styled.View`
    width: auto;
    height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const Text = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size14}px;
    color: ${colors.$darkGrey}
`;

export const Action = styled.TouchableOpacity`
    padding: 5px 0;
`;

export const ActionText = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size14}px;
    color: ${colors.$grey};
`;