import * as React from 'react';
import * as div from './activity-feed.style';

import { IActivityFeed } from '../../interfaces/activity-feed.interface';
import { ContainerActions } from '../panel/actions/container-actions';
import { FeedItem } from './utils/feed-item';
import { ACTIVITYFEED } from '../../constants/notifications.constants';

interface Props {
    activityFeed: IActivityFeed;
    overrides?: {
        root?: string;
    };
}

export const ActivityFeed: React.FunctionComponent<Props> = (props: Props) => {

    const { activityFeed, overrides } = props;
    const { messages, notifications } = activityFeed;

    return (
        <div.ActivityFeed override={overrides && overrides.root} data-cy='activity-feed'>

            {/* Visual elements that show you how many unreaded notifications and messages you have*/}
            <div.Feeds data-cy='feeds'>

                {/* Number and label are required */}
                <FeedItem number={messages} label={ACTIVITYFEED.messages$} />
                <FeedItem number={notifications} label={ACTIVITYFEED.notifications$} />
            </div.Feeds>

            {/* User options */}
            <ContainerActions>

                <div.Action onPress={() => console.log('Delete all notifications !!!')} data-cy='action'>
                    <div.ActionText data-cy='action-text'>Delete all notifications</div.ActionText>
                </div.Action>

                <div.Action onPress={() => console.log('Mark all as read !!')} data-cy='action'>
                    <div.ActionText data-cy='action-text'>Mark all as read</div.ActionText>
                </div.Action>

            </ContainerActions>
        </div.ActivityFeed>
    );
};