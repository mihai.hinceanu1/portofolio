import * as React from 'react';
import * as div from './feed-item.style';

interface Props { 
    number: number;
    label: string;
}

export const FeedItem: React.FunctionComponent<Props> = (props: Props) => {
    
    const { number, label } = props;

    return (
        <div.FeedItem data-cy='feed-item'>
            <div.Counter data-cy='counter'>{number}</div.Counter>
            <div.Text data-cy='text'>{label}</div.Text>
        </div.FeedItem>
    )
}