import styled from 'styled-components/native';
import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';

export const FeedItem = styled.View`
    width: 50%;
    height: inherit;
    display: flex;
    align-items: center;
`;

export const Counter = styled.Text`
    color: ${colors.$blue};
    font-family: sans-serif;
    font-size: ${font.$size40}px;
    font-weight: 600;
`;

export const Text = styled.Text`
    fonst-family: sans-serif;
    font-size: ${font.$size14}px;
`;