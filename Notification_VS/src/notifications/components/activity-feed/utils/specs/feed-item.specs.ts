/// <reference types='cypress' />

describe('Feed Item Specs', () => {

    it('Gets, types and asserts', () => {

        cy.visit('http://localhost:3000/notifications');

        getEl('login-form', 'input').eq(0).type('admin');
        getEl('login-form', 'input').eq(1).type('admin');

        getEl('login-form', 'button').click();
        cy.wait(2000);
        cy.visit('http://localhost:3000/notifications');

        cy.get('[data-cy=feed-item]').should('exist');
        getEl('feed-item', 'counter').should('exist');
        getEl('feed-item', 'text').should('exist');
    });
});

function getEl(parent, child) {
    return cy.get(`[data-cy=${parent}] [data-cy=${child}]`);
}