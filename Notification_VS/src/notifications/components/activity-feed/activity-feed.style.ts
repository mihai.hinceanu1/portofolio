import styled from 'styled-components/native';
import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';

interface ActivityProps {
    override?: string;
}

export const ActivityFeed = styled.View<ActivityProps>`
    min-width: 200px;
    width: 230px;
    ${props => props.override};
`;

export const Feeds = styled.View`
    height: 90px;
    display: flex;
    flex-direction: row;
`;

export const Action = styled.TouchableHighlight`
    padding: 2px 0;
`;

export const ActionText = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size14}px;
    color: ${colors.$grey};
`;