import styled from "styled-components/native";
import { colors } from '../../../shared/style/colors';
import { BOX_SHADOW_CSS } from '../../../shared/style/shadow.style';

export const Panel = styled.View`
    min-width: 150px;
    height: fit-content;
    width: auto;
    margin: 10px;
    background-color: ${colors.$white};
    ${BOX_SHADOW_CSS};
`; 