import * as React from 'react';
import * as div from './container-actions.style';

interface Props { }

// This is a container component for a set-list of actions.
// Activity feed has "Delete all notifications" and "Mark all as read"
// Message Board has "View all Messages"
// Childrens comes from parent component as InnerTag

export class ContainerActions extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div.AllActions data-cy='all-actions'>

                {/* List of actions */}
                {this.props.children}
            </div.AllActions>
        )
    }
}