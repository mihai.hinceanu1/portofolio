import styled from 'styled-components/native';

export const AllActions = styled.View`
    width: inherit;
    justify-content: center;
    align-items: center;
    height: auto;
    position: relative;
    bottom: 0;
    margin-bottom: 15px;
`;
