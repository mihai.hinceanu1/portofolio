import * as div from './container-title.style';
import * as CONSTANT from '../../../pages/notifications.page.utils';
import * as React from 'react';

interface Props {
    title: string;
}

interface State {}

// This component is used on each panel as title.
// The title string comes from parent as props.
// There is a dummy condition for Unread and Recent.

export class ContainerTitle extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public render() {
        const { title } = this.props;

        return(
            <div.Title component={title} data-cy='title'>

                {/* Title string */}
                <div.Text data-cy='text'>{title}</div.Text>

                {/* If condition is true, show an action */}
                { (CONSTANT.UNREAD === title || CONSTANT.RECENT === title) &&
                    <div.Action onPress={() => console.log('mark as read !!!')} data-cy='action'>Mark as read</div.Action>
                }
            </div.Title>
        );
    };
};