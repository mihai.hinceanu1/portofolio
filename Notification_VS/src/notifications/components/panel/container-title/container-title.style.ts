import styled from 'styled-components/native';
import { colors } from '../../../../shared/style/colors';
import { font } from '../../../../shared/style/font-sizes';

interface TitleProps {
    component: string;
}

export const Title = styled.View<TitleProps>`
    width: auto;
    height: 40px;
    padding-left: ${props => padding(props.component)};
    padding-right: ${props => padding(props.component)};
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: ${props => alignment(props.component)};
`;

export const Text = styled.Text`
    font-family: sans-serif;
    font-weight: bold;
    font-size: ${font.$size14}px;
    color: ${colors.$black};
`;

export const Action = styled.Text`
    font-family: sans-serif;
    font-size: small;
    color: ${colors.$darkGrey};
`;

let padding = (component: string) => {
    if (component === 'Unread' || component === 'Recent') {
        return '10px'
    } else {
        return 'none'
    }
}

let alignment = (component: string) => {
    if (component === 'Unread' || component === 'Recent') {
        return 'space-between'
    } else {
        return 'center'
    }
}