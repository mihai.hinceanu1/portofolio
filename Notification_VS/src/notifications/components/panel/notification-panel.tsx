import { ContainerTitle } from './container-title/container-title';
import * as div from './notification-panel.style';
import * as React from 'react';

interface Props {
    title?: string;
}

interface State { }

// This component is the main container for each component that describe a content.
// As you see, we have a title and childrens that comes from notification page as InnerTag List.

export class Panel extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const { title, children } = this.props;

        return(
            <div.Panel data-cy='panel'>

                {/** Title */}
                { title &&
                <ContainerTitle title={title} />}

                {/** Children */}
                {children}
            </div.Panel>
        );
    }
}
