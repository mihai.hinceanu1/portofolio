import * as div from './notification.style';
import { provideAge } from './notification.utils';
import { Icon } from '../../../icons/components/icon/icon';
import { Button } from '../../../shared/components/button/button';
import { colors } from '../../../shared/style/colors';
import { INotification } from '../../interfaces/notification.interface';
import * as React from 'react';

interface Props {
    notifications: INotification[];
}

interface State {
}

export class Notifications extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        }
    }

    public render() {
        const { notifications } = this.props;

        return (
            <div.Notifications data-cy='notifications'>
                {
                    notifications.map((notification: INotification) =>
                        <div.NotificationItem key={notification._id} data-cy='notification-item'>

                            {/* Notification Icon */}
                            <div.NotificationIcon>
                                <Icon path={notification.icon}
                                    color={colors.$blue}
                                    overrides={div.ICON_OVERRIDES} />
                            </div.NotificationIcon>

                            {/* Notification Body structure */}
                            <div.NotificationBody data-cy='notification-body'>

                                {/* Title is a long description. */}
                                <div.Title data-cy='title'>
                                    {notification.title}
                                </div.Title>

                                {/* Most of notifications have this button, but not sure for what */}
                                <Button config={{
                                    text: notification.type,
                                    onPress: () => console.log('Button is pressed !!')}}
                                    overrides={div.BUTTON_OVERRIDES}/>

                                {/* Notification Footer */}
                                <div.Footer data-cy='footer'>
                                    <div.Element data-cy='notification-old'>{provideAge(notification.created)}</div.Element>
                                    <div.Element data-cy='notification-type'>{notification.type}</div.Element>
                                </div.Footer>
                            </div.NotificationBody>

                        </div.NotificationItem>
                    )
                }

            </div.Notifications>
        );
    }
}
