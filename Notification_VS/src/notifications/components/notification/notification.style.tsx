import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';
// import { pathBoundingBox } from '../../../shared/services/raphael.utils';

export const Notifications = styled.View`
    min-height: 150px;
`;

export const NotificationItem = styled.View`
    width: auto;
    height: auto;
    padding: 15px 10px;
    display: flex;
    flex-direction: row;
`;

export const NotificationIcon = styled.View`
    padding: 0 10px;
`;

export const NotificationBody = styled.View`
    margin-left: 10px;
    width: 400px;
    min-height: 80px;
    height: auto;
    display: flex;
    flex-direction: column;
`;

export const Title = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size14}px;
    color: ${colors.$darkGrey};
    flex: auto;
`;

export const Footer = styled.View`
    width: auto;
    height: 20px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
`;

export const Element = styled.Text`
    font-family: sans-serif;
    font-size: ${font.$size12}px;
    color: ${colors.$darkGrey};
    margin-right: 20px;
`;

// ===== OVERRIDES =====

export const ICON_OVERRIDES = {
    image: 'width: 40px; height: 40px;',
};

export const BUTTON_OVERRIDES = {
    root: 'min-height: 35px;',
};
