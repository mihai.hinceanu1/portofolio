import moment from 'moment';

export const provideAge = (created: number) => {
    
    return moment(created).from(moment());
}