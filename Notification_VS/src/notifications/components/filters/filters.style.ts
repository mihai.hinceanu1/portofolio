import styled from 'styled-components/native';
// import { colors } from '../../../shared/style/colors';
// import { font } from '../../../shared/style/font-sizes';

interface FilterProps {
    override?: string;
}

export const Filters = styled.View<FilterProps>`
    min-width: 200px;
    width: 230px;
    ${props => props.override};
`;

export const FilterItem = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 5px 20px;
`;

export const FilterHeader = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

export const Section = styled.View`
    margin: 15px 0;
`;