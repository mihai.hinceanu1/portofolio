import React from 'react';
import * as div from './filters.style';
import { Text } from 'react-native';
import { SwitchButton } from '../../../shared/components/switch-button/switch-button';
import { CustomCornerButton } from './utils/custom-corner-button';

interface Props { }

interface State {
    comments: boolean;
    updates: boolean;
    recommandations: boolean;
}

export class Filters extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            comments: true,
            updates: true,
            recommandations: false,
        };
    }

    public render() {

        const { comments, updates, recommandations } = this.state;

        return (
            <div.Filters data-cy='filters'>
                <div.FilterHeader data-cy='filter-header'>
                    <CustomCornerButton config={{
                        type: 'left',
                        width: 95,
                        title: 'All',
                        isActive: true
                    }} />
                    <CustomCornerButton config={{
                        type: 'right',
                        width: 95,
                        title: 'Unread',
                        isActive: false
                    }} />
                </div.FilterHeader>

                <div.Section data-cy='section'>
                    <div.FilterItem>
                        <Text>Comments</Text>
                        <SwitchButton config={{ default: comments, onPress: () => this.onSwitchChange('comments', comments) }} />
                    </div.FilterItem>

                    <div.FilterItem>
                        <Text>Updates</Text>
                        <SwitchButton config={{ default: updates, onPress: () => this.onSwitchChange('updates', updates)}} />
                    </div.FilterItem>

                    <div.FilterItem>
                        <Text>Recommandations</Text>
                        <SwitchButton config={{ default: recommandations, onPress: () => this.onSwitchChange('recommandations', recommandations)}} />
                    </div.FilterItem>
                </div.Section>

            </div.Filters>
        );
    };

    private onSwitchChange(key: string, value: boolean) {

        this.setState({
            ...this.state,
            [key]: !value,
        });
    };
};