import React from 'react';
import * as div from './custom-corner-button.style';

interface Props {
    config: {
        type: string;
        width: number;
        title: string;
        isActive: boolean;
    }
}

interface State {
    isActive: boolean;
}

export class CustomCornerButton extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            isActive: props.config.isActive,
        }
    }

    render() {
        const { type, width, title } = this.props.config;
        const { isActive } = this.state;

        return (
            <div.CustomCornerButton
                type={type}
                isActive={isActive}
                width={width}
                onPress={() => this.toggleButton()}
                data-cy='custom-corner-button'>

                <div.Text isActive={isActive} data-cy='text'>{title}</div.Text>
            </div.CustomCornerButton>
        );
    }

    private toggleButton() {
        this.setState({isActive: !this.state.isActive})
    }
};
