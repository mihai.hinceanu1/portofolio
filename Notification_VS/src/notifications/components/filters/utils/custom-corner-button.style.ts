import styled from 'styled-components/native';
import { colors } from '../../../../shared/style/colors';

interface ButtonProps {
    // type can be "left", "center", "right" to know how corners would be rounded
    type: string;

    width: number;
    isActive: boolean;
}

interface TextProps {
    isActive: boolean;
}

export const CustomCornerButton = styled.TouchableHighlight<ButtonProps>`
    width: ${props => props.width}px;
    height: 26px;
    padding: 0 10px;
    background-color: ${(props) => props.isActive ? colors.$blue : colors.$white};
    justify-content: center;
    align-items: center;
    border-width: 1px;
    border-color: ${colors.$blue};
    border-style: solid;
    ${(props) => setCorners(props.type)};
`;

export const Text = styled.Text<TextProps>`
    font-family: sans-serif;
    color: ${(props) => props.isActive ? colors.$white : colors.$blue};
`;

const setCorners = (type: any) => {
    switch (type) {
        case 'left':
            return `
                border-top-left-radius: 13px;
                border-bottom-left-radius: 13px;
                    `;

        case 'right':
            return `
                border-top-right-radius: 13px;
                border-bottom-right-radius: 13px;
                    `;
        default:
            break;
    }
}