import { ReactWebAttributes } from './shared/interfaces/workarounds';
import { colors } from './shared/style/colors';
import styled from 'styled-components/native';

export const AppSafeArea = styled.SafeAreaView<ReactWebAttributes>`
    width: 100%;
    height: 100%;
    background-color: ${colors.$appBgr};
`;