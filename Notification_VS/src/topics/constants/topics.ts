/**
 * Content blocks available to render lessons.
 * <!> WARNING
 * This enum will be extended with custom types from community modules.
 */
export enum BLOCK_TYPES {
    'text',
    'image',
    'imageSlider',
    'codeSnippet',
    'codeFlow',
    'codeRevision',
    'layout',
}

/** Relations between content blocks. */
export enum TOPIC_RELATIONS {

    /**
     * Extra help blocks are rendered on demand
     * These blocks are dedicated to juniors that need more assistance to understand a concept.
     */
    'extraHelp',
}

/**
 * A Topic can be used as a topic or as a lesson
 * They have the same structure but the content their content
 * is not so similar.
 */
export enum TOPIC_TYPE {
    'lesson',
    'project',
}