/**
 * REVIEW if DEPRECATED or not
 *
 * All user preferences for a topic are booleans.
 * Therefore, they are crammed in one endpoint.
 */
export enum SENTIMENTS {
    'goal',
    'thumbsUp',
    'thumbsDown',
    'laugh',
    'party',
    'confused',
    'favorite',
};