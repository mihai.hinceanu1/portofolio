import { Lesson } from '../../lessons/interfaces/lesson';
import { Paginated, PaginatedChildrenReq, PaginatedSearchReq } from '../../shared/interfaces/shared';
import axios from '../../shared/services/axios.service';
import { ITopic } from '../interfaces/topic';
import { from, Observable } from 'rxjs';
import { TopicSentiment/* , TopicSentimentIdAndName */ } from '../interfaces/topic-sentiment';
// import { TopicPageResp } from '../interfaces/topic-page-resp';
import {
    NewTopicResponse,
    // TopicPageReq,
    PopularTopicsTop,
    TaxonomyNewTopic,
    TopicsAdminNewChildTopic,
} from '../interfaces/topics';

export const getTopicsByUser = (): Observable<Lesson[]> =>
    from<Promise<Lesson[]>>(
        axios.get<Lesson[]>(`topics-user/topics`)
            .then(res => res.data),
    );

/**
 * OLD ONES
 * TODO
 * Check if needed, if not delete
 */
export const getFirstPopularTopics = (): Observable<PopularTopicsTop[]> =>
    from<Promise<PopularTopicsTop[]>>(
        axios.get<PopularTopicsTop[]>(`/topics/topics/popularity/${10}`)
            .then(res => res.data),
    );

// DEPRECATED to be deleted or replaced
// export const getTopicPage = (
//     { pathName, childPathName = '', userId = '' }: TopicPageReq,
// ): Observable<TopicPageResp> =>
//     from<Promise<TopicPageResp>>(
//         axios.get<TopicPageResp>(`/topics/topics-page/${pathName}/${childPathName}/${userId}`)
//             .then(res => res.data),
//     );

export const getNewTopicPage = (pathname: string): Observable<Lesson> =>
    from<Promise<Lesson>>(
        axios.get<Lesson>(`/topics/topic-page/${pathname}`)
            .then(res => res.data),
    );

export const getTopic = (id: string): Observable<ITopic> =>
    from<Promise<ITopic>>(
        axios.get<ITopic>(`/topics/topics/${id}`)
            .then(res => res.data),
    );

export const getTopicByPathName = (topicPath: string): Observable<ITopic> =>
    from<Promise<ITopic>>(
        axios.get<ITopic>(`/topics/topics/pathname/${topicPath}`)
            .then(res => res.data),
    );

export const editTopic = (topic: ITopic): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`/topics/topics/${topic._id}`, topic)
            .then(res => res.data),
    );

export const deleteTopic = (id: string): Observable<null> =>
    from<Promise<null>>(
        axios.delete(`/topics/topics/${id}`)
            .then(res => res.data),
    );

/** Webapi - Create new topic from taxonomy page */
export const createTopic = (topicCfg: TaxonomyNewTopic): Observable<string> => {
    return from<Promise<string>>(
        axios.post<string>(`/topics/topics`, topicCfg)
            .then(res => res.data),
    );
};

/** Webapi - Create new CHILD topic from topic admin page */
export const createChildTopic = (childTopicCfg: TopicsAdminNewChildTopic): Observable<NewTopicResponse> => {
    return from<Promise<NewTopicResponse>>(
        axios.post<NewTopicResponse>(`/topics/topics/child-topic`, childTopicCfg)
            .then(res => res.data),
    );
};

export const getAllTopics = ({ page = 0, perPage = 20 }: Paginated): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/${page}/${perPage}`)
            .then(res => res.data),
    );

export const getTopicsByNumber = (numberOfTopics: number): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/latest/${numberOfTopics}`)
            .then(res => res.data),
    );

export const getAllAdminTopics = (): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics`)
            .then(res => res.data),
    );

export const searchTopicsByKeyword = (request: PaginatedSearchReq): Observable<ITopic[]> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};

// DEPRECATED
// /** @CEZAR please have a look I get unauthorized here */
// export const editTopicSentiment = (request: TopicSentimentIdAndName): Observable<string> => {
//     let { sentimentId, sentimentName } = request;
//     return from<Promise<null>>(
//         axios.post<null>(`/topics/sentiment/${sentimentId}`, {
//             sentimentName,
//         })
//             .then(res => res.data),
//     );
// };

/** Useful for rendering additional info on maps when hovering topics/projects */
export const getDomainTopics = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/domain/${uuid}/${page}/${perPage}`)
            .then(res => res.data),
    );

/** Useful for rendering additional info on maps when hovering topics/projects */
export const getCategoryTopics = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/category/${uuid}/${page}/${perPage}`)
            .then(res => res.data),
    );

/** Useful for rendering additional info on maps when hovering topics/projects */
export const getTopLevelTopicTopics = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(
            `/topics/topics/category/${uuid}/${page}/${perPage}`,
        )
            .then(res => res.data),
    );

/** Useful for rendering additional info on maps when hovering topics/projects */
export const getChildrenTopics = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/children/${uuid}/${page}/${perPage}`)
            .then(res => res.data),
    );

export const getRecentTopics = ({ page = 0, perPage = 20 }: Paginated): Observable<ITopic[]> =>
    from<Promise<ITopic[]>>(
        axios.get<ITopic[]>(`/topics/topics/recent/${page}/${perPage}`)
            .then(res => res.data),
    );

export const getTopicSentimentByTopicId = (topicId: string): Observable<TopicSentiment[]> =>
    from<Promise<TopicSentiment[]>>(
        axios.get<TopicSentiment[]>(`/topics/topics/sentiments/${topicId}`)
            .then(res => res.data),
    );
