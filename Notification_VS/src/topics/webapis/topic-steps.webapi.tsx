import { APP_CFG } from '../../shared/config/app.config';
import { Paginated, PaginatedChildrenReq, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { TopicStep } from '../interfaces/topic-step';
import axios from 'axios';
import { from, Observable } from 'rxjs';

export const getTopicStep = (id: string): Observable<TopicStep> =>
    from<Promise<TopicStep>>(
        axios.get<TopicStep>(`${APP_CFG.api}/topics/topic-steps/${id}`)
            .then(res => res.data),
    );

export const editTopicStep = (topicStep: TopicStep): Observable<null> =>
    from<Promise<null>>(
        axios.put<null>(`${APP_CFG.api}/topics/topic-steps/${topicStep._id}`, topicStep)
            .then(res => res.data),
    );

export const deleteTopicStep = (id: string): Observable<null> =>
    from<Promise<null>>(
        axios.delete(`${APP_CFG.api}/topics/topic-steps/${id}`)
            .then(res => res.data),
    );

export const createTopicStep = (topicStep: TopicStep): Observable<null> =>
    from<Promise<null>>(
        axios.post<null>(`${APP_CFG.api}/topics/topic-steps`, topicStep)
            .then(res => res.data),
    );

export const getAllTopicSteps = (
    { page = 0, perPage = 20 }: Paginated,
): Observable<TopicStep[]> =>
    from<Promise<TopicStep[]>>(
        axios.get<TopicStep[]>(`${APP_CFG.api}/topics/topic-steps/${page}/${perPage}`)
            .then(res => res.data),
    );

export const searchTopicStepsByKeyword = (request: PaginatedSearchReq): Observable<TopicStep[]> => {
    let { query, page = 0, perPage = 0 } = request;
    return from<Promise<TopicStep[]>>(
        axios.get<TopicStep[]>(`${APP_CFG.api}/topics/topic-steps/search/${query}/${page}/${perPage}`)
            .then(res => res.data),
    );
};

export const getTopicSteps = (
    { uuid, page = 0, perPage = 20 }: PaginatedChildrenReq,
): Observable<TopicStep[]> =>
    from<Promise<TopicStep[]>>(
        axios.get<TopicStep[]>(`${APP_CFG.api}/topics/topic-steps/topic/${uuid}/${page}/${perPage}`)
            .then(res => res.data),
    );