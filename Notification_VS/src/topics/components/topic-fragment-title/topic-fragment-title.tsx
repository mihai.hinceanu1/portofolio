import * as React from 'react';
import { Text, View } from 'react-native';

interface Props {
    name: string;
}

/**
 * Here we render the title of a topic
 */
export const TopicFragmentTitle: React.StatelessComponent<Props> = (props: Props) => {
    const { name } = props;

    return (
        <View data-cy='topic-fragment-title'>
            <Text>
                {name}
            </Text>
        </View>
    );
};
