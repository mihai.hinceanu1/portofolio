import * as div from './topic-image.style';
import * as React from 'react';
import { Image } from 'react-native';

interface Props {
    block: any;
}

interface State { }

export class TopicImage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public render() {
        console.log('this is img props: ', this.props);
        return (
            <div.ImagePanel>
                <Image style={{height: 100, width: 130}} 
                source={require('../../../lessons/assets/css.jpg')} 
                loadingIndicatorSource={require('../../../lessons/assets/css.jpg')}></Image>
            </div.ImagePanel>
        )
    }
}