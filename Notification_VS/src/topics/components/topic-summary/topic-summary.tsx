import { TopicSummaryItem } from './topic-summary-item';
import * as div from './topic-summary.style';
import { ITopicSummary } from '../../../layouts/interfaces/topic-layout';
import { FitImage } from '../../../shared/components/fit-image/fit-image';
import * as React from 'react';

interface Props {
    summary: ITopicSummary;
    thumbnail: string;
}

interface State { }

export class TopicSummary extends React.Component<Props, State> {

    public render() {
        const { summary, thumbnail } = this.props;

        return (
            <div.TopicSummary data-cy='topic-summary'>

                <div.Title>
                    <div.Icon>
                        <FitImage imgPath={thumbnail} />
                    </div.Icon>

                    <div.Title2>
                        Summary
                    </div.Title2>
                </div.Title>

                <div.Summaries data-cy='topic-summaries-list'>
                    {
                        summary.features &&
                        summary.features.map(feature =>
                            <TopicSummaryItem key={feature._id} feature={feature} />,
                        )
                    }
                </div.Summaries>

            </div.TopicSummary>
        );
    }
}
