import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

export const SummaryElementTitle = styled.Text`
    font-size: ${font.$size16}px;
    font-weight: 400;
`;

export const SummaryElementText = styled.Text`
    font-size: ${font.$size12}px;
    font-weight: 300;
    color: rgb(120, 120, 120);
`;

export const TopicSummaryItem = styled.View`
    width: 250px;
`;
