import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import { Platform } from 'react-native';
import styled from 'styled-components/native';

const isMobile = Platform.OS !== 'web';

export const TopicSummary = styled.View`
    margin: auto;
    margin-top: 75px;
    display: flex;
    flex-direction: column;
    margin-bottom: 75px;
    justify-content: center;
    ${isMobile && 'align-self: center;'}
`;

export const Title2 = styled.Text`
    font-size: ${font.$size18}px;
    color: ${colors.$menuBtnTextColor};
    font-weight: 600;
`;

export const Summaries = styled.View`
    justify-content: space-around;
    align-items: center;
    flex-direction: row;
    margin-top: 50px;
    flex-wrap: wrap;
    ${isMobile && 'left: 50px;'}
`;

export const Icon = styled.View`
    width: 20px;
    height: 20px;
    align-self: center;
    margin-right: 5px;
`;

export const Title = styled.View`
    flex-direction: row;
    justify-content: center;
    align-self: center;
`;