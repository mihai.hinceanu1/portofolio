import * as div from './topic-summary-item.style';
import { TopicSummaryFeature } from '../../../layouts/interfaces/topic-layout';
import * as React from 'react';

interface Props {
    feature: TopicSummaryFeature;
}

interface State { }

export class TopicSummaryItem extends React.Component<Props, State> {

    public render() {
        const { feature } = this.props;

        return (
            <div.TopicSummaryItem data-cy='topic-summary-item' key={feature._id}>

                <div.SummaryElementTitle data-cy='summary-element-title'>
                    {feature.label}
                </div.SummaryElementTitle>

                <div.SummaryElementText data-cy='summary-element-description'>
                    {feature.description}
                </div.SummaryElementText>

            </div.TopicSummaryItem>
        );
    }
}