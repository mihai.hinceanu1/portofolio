import * as div from './topic-content.style';
import { CommentsSection } from '../../../comments/components/comments-section/comments-section';
import { UserComment } from '../../../comments/interfaces/comments-utils';
import { Image } from '../../../images/interfaces/image';
import { CodeSnippet } from '../../../source-code/interfaces/code-snippet';
import { SourceFile } from '../../../source-code/interfaces/source-file';
import { ITopic } from '../../interfaces/topic';
import { TopicSentiment } from '../../interfaces/topic-sentiment';
import * as React from 'react';
import { Subject } from 'rxjs';
// import { TopicText } from './topic-text/topic-text';
// import { TopicAdminImages } from '../../../images/components/topic-admin-images/topic-admin-images';
// import { isNotUndefined } from '../../../shared/utils/failsafe.utils';

interface Props {
    config: {
        topic: ITopic;
        codeSnippets: CodeSnippet[];
        sourceFiles: SourceFile[];
        images: Image[];
        sentiment: TopicSentiment;
        // Optional because of topics-overlay.
        topicComments?: UserComment[];
        mainTopicId?: string;
    };
    mainTopicId?: string;
}

interface State {
    topicOnOverlay: boolean;
    textColor: string;
}

/**
 * Renders text and media contents of a topic.
 * Contents can be: texts, images, code, etc...
 * Each topic defines a certain order for all these fragments.
 * Topics can have multiple steps.
 * Each topic step can activate tooltips or other GUI components.
 */
export class TopicContent extends React.Component<Props, State> {

    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);
        this.state = {
            textColor: null,
            topicOnOverlay: null,
        };
    }

    public render() {
        const { config, mainTopicId } = this.props;
        const { topic, topicComments } = config;

        return (
            <div.TopicContent data-cy='topic-content'>
                {/* Title */}

                {
                    topic.isProject &&
                    <div.IconAndText>
                        {/* <FontAwesomeIcon icon={faLaptopCode} style={{ marginRight: '10px' }} /> */}

                        <div.TopicTitle data-cy='title'>
                            {topic && topic.name}
                        </div.TopicTitle>
                    </div.IconAndText>
                }

                {
                    !topic.isProject &&
                    <div.TopicTitle data-cy='title'>
                        {topic && topic.name}
                    </div.TopicTitle>
                }

                {
                    topic &&
                    <div.TopicDescription data-cy='topic-description'>
                        "{topic.description}"
                    </div.TopicDescription>
                }

                <div.TopicSection data-cy='content'>
                    {/* Fragments */}
                    <div.TopicFragments>
                        {
                            // (topic && topic.contentOrder) && topic.contentOrder.map(
                            //     (contentId, i) => {
                            //         let content = this.getTopicFragment(contentId, config);

                            //         return (
                            //             content &&
                            //             <div.TopicFragment key={i} data-cy='fragment'>
                            //                 {content}
                            //             </div.TopicFragment>
                            //         );
                            //     },

                            // )
                        }
                    </div.TopicFragments>

                    {/* Comments */}
                    <div.Comments data-cy='comments-container'>
                        {
                            topicComments &&
                            <CommentsSection topicComments={topicComments} topicId={topic._id}
                                mainTopicId={mainTopicId} />
                        }
                    </div.Comments>
                </div.TopicSection>

                {/** Topic Sentiments */}
            </div.TopicContent>
        );
    }

    public componentWillUnmount() {
        this.destroyed$.next();
    }

    // RESTORE
    // private getTopicFragment(_id: string, _config: Props['config']): JSX.Element {

        // let { textColor: color } = this.state;
        // let { topic } = config;

        // let text = topic.texts.find(text => text._id === id);

        // // get all codeSnippetsIds
        // let codeFlow = topic.codeFlow.find(codeSnip => codeSnip._id === id);

        // if (text) {
        //     let config = { text, topic };
        //     let textTooltips = getTextTooltips(topic, text._id);

        //     return (
        //         <TopicText config={config} color={color} textTooltips={textTooltips} />
        //     );
        // } else if (codeFlow) {

        //     let { codeSnippets, sourceFiles } = config;
        //     let codeSnippetsArray: CodeSnippet[] = [];

        //     // get entire codeSnippets objects
        //     if (codeSnippets !== undefined && codeSnippets.length > 0 &&
        //         sourceFiles !== undefined && sourceFiles.length > 0) {

        //         codeFlow.codeSnippetsIds.forEach(id => {
        //             codeSnippetsArray.push(codeSnippets.find(cs => cs._id === id));
        //         });
        //         return;
        //         // <TopicEditor codeSnippets={codeSnippetsArray} sourceFiles={sourceFiles} />
        //         // @Refactoring using topic-section.component.tsx
        //     }

        // } else if (topic.imagesSlider.length) {
        //     if (isNotUndefined(config.images)) {
        //         let topicGallery = this.getTopicGallery(topic, id, [...config.images]);
        //         let imageTooltips = topic.tooltips.image;

        //         return (
        //             <TopicAdminImages topicGallery={topicGallery} imageTooltips={imageTooltips} />
        //         );
        //     }
        //     return;

        // }

    //     return null;
    // }

    // private getTopicGallery(topic: Topic, contentId: string, images: Image[]) {
    //     let topicGallery: Image[] = [];

    //     // Find the gallery that has the same id as the content order id given.
    //     let gallery = topic.imagesSlider.find(gallery => gallery._id === contentId);

    //     // Get only the images included in gallery.
    //     if (isNotUndefined(gallery)) {
    //         topicGallery = images.filter(image => gallery.imageIds.includes(image._id));

    //         return topicGallery;
    //     }
    //     return;
    // }
}

// // TODO update return any to JSX node interface
// function getTextTooltips(topic: Topic, textId: string) {
//     let textTooltips = topic.tooltips.content.filter(tt => tt.textId === textId);
//     return textTooltips;
// }
