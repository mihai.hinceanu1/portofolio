import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

export const TopicContent = styled.View`
    margin-bottom: 50px;
    padding: 50px;
    margin-left: 30px;
    flex-direction: column;
    justify-content: space-around;
`;

export const TopicTitle = styled.Text`
    font-weight: 600;
    font-size: 20px;
    color: ${colors.$menuBtnTextColor};
`;

export const TopicDescription = styled.Text`
    color: ${colors.$labelColor};
    padding: 10px 0 0 0;
    font-style: italic;
`;

export const TopicSection = styled.View`
    /* display: flex; */
    /* position: relative; */
    flex-direction: row;
    flex-wrap: wrap;
`;

export const TopicFragments = styled.View`
    flex: 7;
    min-width: 430px;
    margin-right: 80px;
`;

export const TopicFragment = styled.View`
    margin-top: 10px;
`;

export const Comments = styled.View`
    flex: 3;
    min-width: 342px;
`;

export const IconAndText = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

/* background-color: ${props => getRandomRgb(props.key)}; */
// DEBUGGING -> Remove after
// function getRandomRgb(i: number) {
//     var num = Math.round(0xffffff * Math.random());
//     // num += i;
//     var r = num >> 16;
//     var g = num >> 8 & 255;
//     var b = num & 255;
//     return 'rgb(' + r + ', ' + g + ', ' + b + ')';
// }