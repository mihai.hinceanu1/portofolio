/// <reference types="cypress" />

/** <!>TODO Update tests */
describe('Topic sentiments tests', () => {
    it('Goes to lesson page', () => {
        cy.login();
        cy.get('[data-cy=page-button]').eq(0).click();
        cy.url().should('include', 'http://localhost:3000/lesson');
    });
});