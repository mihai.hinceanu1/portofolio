import * as div from './topic-sentiments.style';
import { TopicSentimentNew } from '../../interfaces/topic-sentiment';
import * as React from 'react';
// import { editTopicSentiment } from '../../services/topics-sentiment.service';

interface Props {
    sentiment: TopicSentimentNew;
}

interface State {}

// TODO Recycle in the new design
export class TopicSentiments extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
        };

    }

    public render() {
        const { sentiment } = this.props;

        return (
            <div.TopicSentiments data-cy='topic-sentiments'>
                <div.SentimentElement data-cy='sentiment-thumbs-up'>
                    {/* <Icon
                        icon={'/sentiments/upDarkGrey.png'}
                        iconHovered={'/sentiments/upBlue.png'}
                        width={20}
                        height={20}
                        onHoverOverlayMessage={'Like'}
                        onPress={() => this.sentimentPressed('thumbsUp')}
                    /> */}

                    <div.Text>
                        {sentiment.thumbsUp.length}
                    </div.Text>
                </div.SentimentElement>

                <div.SentimentElement>
                    {/* <Icon
                        icon={'/sentiments/downDarkGrey.png'}
                        iconHovered={'/sentiments/downBlue.png'}
                        width={20}
                        height={20}
                        onHoverOverlayMessage={'Dislike'}
                        onPress={() => this.sentimentPressed('thumbsDown')}
                    /> */}

                    <div.Text>
                        {sentiment.thumbsDown.length}
                    </div.Text>
                </div.SentimentElement>

                <div.SentimentElement>
                    {/* <Icon
                        icon={'/sentiments/favoriteDarkGrey.png'}
                        iconHovered={'/sentiments/favoriteBlue.png'}
                        width={20}
                        height={20}
                        onHoverOverlayMessage={'Favorite'}
                        onPress={() => this.sentimentPressed('faved')}
                    /> */}

                    <div.Text>
                        {sentiment.faved.length}
                    </div.Text>
                </div.SentimentElement>

                <div.SentimentElement>
                    {/* <Icon
                        icon={'/sentiments/confusedDarkGrey.png'}
                        iconHovered={'/sentiments/confusedBlue.png'}
                        width={20}
                        height={20}
                        onHoverOverlayMessage={'Confused'}
                        onPress={() => this.sentimentPressed('confused')}
                    /> */}

                    <div.Text>
                        {sentiment.confused.length}
                    </div.Text>
                </div.SentimentElement>

                <div.SentimentElement>
                    {/* <Icon
                        icon={'/sentiments/laughDarkGrey.png'}
                        iconHovered={'/sentiments/laughBlue.png'}
                        width={20}
                        height={20}
                        onHoverOverlayMessage={'Laugh'}
                        onPress={() => this.sentimentPressed('laugh')}
                    /> */}

                    <div.Text>
                        {sentiment.laugh.length}
                    </div.Text>
                </div.SentimentElement>

                <div.SentimentElement>
                    {/* <Icon
                        icon={'/sentiments/partyDarkGrey.png'}
                        iconHovered={'/sentiments/partyBlue.png'}
                        width={20}
                        height={20}
                        onHoverOverlayMessage={'Party'}
                        onPress={() => this.sentimentPressed('party')}
                    /> */}

                    <div.Text>
                        {sentiment.party.length}
                    </div.Text>
                </div.SentimentElement>
            </div.TopicSentiments >
        );
    }

    // DEPRECATED ? REFACTOR
    // private sentimentPressed(_sentimentName: string) {
        // const { sentiment } = this.props;
        // let config = {
        //     sentimentId: sentiment._id,
        //     sentimentName,
        // };

        // editTopicSentiment(config);
    // }
}
