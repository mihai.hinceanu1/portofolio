import styled from 'styled-components/native';

export const TopicSentiments = styled.View`
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 50px;
`;

export const Text = styled.Text`
    font-weight: 600;
    margin-right: 20px;
`;

export const SentimentElement = styled.View`
    width: 50px;
    height: 20px;
    flex-direction: row;
`;

export const SentimentButton = styled.TouchableOpacity``;