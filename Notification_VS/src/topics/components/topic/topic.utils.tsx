import { ITopic } from '../../interfaces/topic';
import { TopicImage } from '../topic-image/topic-image';
import { TopicText } from '../topic-text/topic-text';
import * as React from 'react';
import { Text } from 'react-native';

export const getSortedTopicBlocks = (topic: ITopic): JSX.Element[] => {
    let blocksEls: JSX.Element[] = [];
    let blockTypes = Object.keys(topic.blocks);

    // Render Topic Blocks
    blockTypes.forEach(type => {
        let blocks = topic.blocks[type];

        blocksEls = [
            ...blocksEls,
            ...blocks.map(block => renderBlock(block, type)),
        ];
    });

    return blocksEls;
};

// ====== PRIVATE ======

const renderBlock = (block: any, type: string): JSX.Element => {
    let blocksEls: JSX.Element;
    console.log('types list: ', type);

    if (type === 'text') {
        return <TopicText text={block.text} markers={block.markers} key={block._id}/>;
    }

    if (type === 'image') {
        return <TopicImage block={block} key={block._id} />;
    }

    if (type === 'imagesSlider') {
        return <Text key={block._id}>Aici trebuie sa fie componenta ImagesSlider</Text>
    }

    if (type === 'codeSnippet') {
        return <Text key={block._id}>Aici trebuie sa fie componenta CodeSnippet</Text>
    }

    if (type === 'codeFlow') {
        return <Text key={block._id}>Aici trebuie sa fie componenta CodeFlow</Text>
    }

    if (type === 'codeRevision') {
        return <Text key={block._id}>Sa nu uitam de CodeRevision</Text>
    }

    return blocksEls;
};

// DEPRECATED - DELETE after total refactor of lesson page
// TODO Split to separate component
// private renderTopicFragment(content: ContentOrder) {
//     const { topic, isExpertOn } = this.props;

//     switch (content.componentPath) {
//         case 'texts': {
//             let text = topic.texts.find(text => text._id === content._id);
//             let zIndex = topic.texts.length - topic.texts.indexOf(text) + 1;

//             let textContent = combineTextWithNanolessons(text.content, text.contentTooltips);
//             let extendedContent = combineTextWithNanolessons(text.extendedContent, text.extendedContentTooltips);

//             return <TopicText
//                 zIndex={zIndex}
//                 textContent={textContent}
//                 unfragmentedExtendedText={text.extendedContent}
//                 extendedContent={extendedContent}
//                 isExpertOn={isExpertOn}

//                 key={text._id} />;
//         }

//         case 'summary': {
//             /** TODO implement Topic Summary(better version of current Topic Summary) :) */
//             let summary: ITopicSummary = topic.summary;

//             return <TopicSummary
//                 summary={summary}
//                 key={summary._id} thumbnail={topic.thumbnail} />;
//         }

//         case 'imagesSlider': {
//             let imageList = topic.imagesSlider.find(list => list._id === content._id);

//             /** TODO expert mode  */
//             return <TopicImages
//                 images={imageList.images}
//                 isExpertOn={isExpertOn}

//                 key={content._id} />;
//         }

//         /** TODO handle CodeSnippets */
//         case 'codeFlow': {
//             let codeFlow = topic.codeFlow.find(list => list._id === content._id);

//             return <TopicEditor
//                 codeSnippets={codeFlow.codeSnippets} key={content._id} />;
//         }

//         default:
//             return;
//     }
// }