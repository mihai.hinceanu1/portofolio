import { colors } from '../../../shared/style/colors';
import { font } from '../../../shared/style/font-sizes';
import styled from 'styled-components/native';

interface TitleProps {
    isMainTopic: boolean;
}

export const TopicTitle = styled.View`
    flex-direction: column;
`;

export const Title = styled.Text<TitleProps>`
    font-size: ${props => getTitleSize(props)}px;
    font-weight: 400;
    color: ${colors.$black};
`;

export const Description = styled.Text`
    font-size: ${font.$size14}px;
    color: ${colors.$topicSubtitle};
`;

// ====== UTILS ======

function getTitleSize(props: TitleProps) {
    return props.isMainTopic === true ? font.$size40 : font.$size22;
}
