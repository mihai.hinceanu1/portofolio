/// <reference types="cypress" />

/** <!>TODO Update tests */
describe('Topic title tests', () => {
    it('Goes to lesson page', () => {
        cy.login();
        cy.get('[data-cy=page-button]').eq(0).click();
        cy.url().should('include', 'http://localhost:3000/lesson');
    });

    it('Checks if text is rendered in title text container', () => {
        cy.get('[data-cy=topic-title]').eq(0).should(div => {
            const text = div.text();
            expect(text.length).to.be.greaterThan(0);
        });
    });
});