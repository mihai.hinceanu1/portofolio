import styled from 'styled-components/native';

export const Topic = styled.View`
    margin-bottom: 50px;
    width: 100%;
    max-width: 680px;
`;

export const TopicBlocks = styled.View`
    margin-top: 30px;
`;