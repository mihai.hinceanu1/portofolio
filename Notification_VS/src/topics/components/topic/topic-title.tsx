import * as div from './topic-title.style';
import { ITopic } from '../../interfaces/topic';
import * as React from 'react';

interface Props {
    topic: ITopic;
    isMainTopic: boolean;
}

/** Renders the title of the topic section */
export const TopicTitle: React.StatelessComponent<Props> = (props: Props) => {
    const { topic, isMainTopic } = props;

    return (
            <div.TopicTitle data-cy='topic-title'>

                {/** Title */}
                <div.Title data-cy='title' isMainTopic={isMainTopic}>
                    {topic.name}
                </div.Title>

                {/** Description */}
                <div.Description data-cy='description'>
                    {topic.description}
                </div.Description>

            </div.TopicTitle>
        // <div.TopicTitle data-cy='topic-title'>

        //     {/** Title */}
        //     <div.Title data-cy='title' isMainTopic={isMainTopic}>
        //         {topic.name}
        //     </div.Title>

        //     {/** Description */}
        //     <div.Description data-cy='description'>
        //         {topic.description}
        //     </div.Description>

        // </div.TopicTitle>
    );
};
