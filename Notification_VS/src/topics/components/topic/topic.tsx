import { TopicTitle } from './topic-title';
import * as div from './topic.style';
import { getSortedTopicBlocks } from './topic.utils';
import { ITopic } from '../../interfaces/topic';
import * as React from 'react';
import { Panel } from '../../../notifications/components/panel/notification-panel';

interface Props {
    topic: ITopic;
    isMainTopic?: boolean;
    isExpertOn?: boolean;
}

interface State { }

/**
 * Renders the content blocks of a topic
 * We need to define complex media types mixed with the lesson content.
 * Using a solution such as magic tags like in Wordpress won't work for us.
 * There are too many dynamic connections between text and complex media types.
 * All topics composed of content blocks because
 */
export class
    Topic extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render() {
        const { topic, isMainTopic } = this.props;

        return (
            <Panel>
                <div.Topic data-cy='topic'>

                    {/* Title */}
                    <TopicTitle topic={topic} isMainTopic={isMainTopic} />

                    {/* Blocks */}
                    <div.TopicBlocks data-cy='topic-blocks'>
                        {
                            getSortedTopicBlocks(topic)
                        }
                    </div.TopicBlocks>
                </div.Topic>
            </Panel>
        );
    }
}
