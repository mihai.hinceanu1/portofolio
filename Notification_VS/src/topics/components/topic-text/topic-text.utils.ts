import { TextMarkerDto } from '../../interfaces/text-markers';
import { TextNanolesson } from '../../interfaces/topic-text-nanolesson';

export function splitText(text: string, markers: TextMarkerDto[]) {
    let textClone = text;
    let splitTextArr: TextNanolesson[] = [];
    let latestPos = 0;

    /**
     * @TODO make sure markers are sorted asc before spliting text
     */

    markers.forEach(marker => {
        let { start, end } = marker.position;

        // latestPos might be the first char of the string
        if (latestPos !== start) {
            splitTextArr.push({
                text: textClone.substring(latestPos, start),
                nanolesson: null,
            });
        }

        splitTextArr.push({
            text: textClone.substring(start, end),
            nanolesson: marker.nanoLesson,
        });

        latestPos = end;
    });

    if (textClone.substring(latestPos, textClone.length)) {
        splitTextArr.push({
            text: textClone.substring(latestPos, textClone.length),
            nanolesson: null,
        });
    }

    return splitTextArr;
}
