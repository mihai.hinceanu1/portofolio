import { colors } from '../../../shared/style/colors';
import styled from 'styled-components/native';

interface TextProps {
    hasNanolesson: boolean;
}

export const LessonText = styled.View`
    flex-direction: row;
`;

export const Text = styled.Text<TextProps>`
    background-color: ${props => getBackgroundColor(props)};
`;

// ===== UTILS =====

function getBackgroundColor(props: TextProps) {
    if (props.hasNanolesson) {
        return colors.$blue;
    }

    return 'transparent';
}
