import * as div from './topic-text.style';
import * as utils from './topic-text.utils';
import { LessonCurrentNanolesson } from '../../../lessons/interfaces/lesson-current-nanolesson';
import { setLessonCurrentNanolesson, showCurrentNanolesson } from '../../../lessons/services/lessons.service';
import { TextMarkerDto } from '../../interfaces/text-markers';
import { TextNanolesson } from '../../interfaces/topic-text-nanolesson';
import * as React from 'react';
import { GestureResponderEvent, LayoutChangeEvent, Text } from 'react-native';

interface Props {
    text: string;
    markers: TextMarkerDto[];
}
interface State {
    textNanolesson: TextNanolesson[];
}

/**
 * Renders simple text combined with Nanolessons
 */
export class TopicText extends React.Component<Props, State> {
    // used as key for textNanolesson mapping
    private id: number = 0;

    // refs to current chunks of text
    private textRefs: Text[] = [];

    constructor(props: Props) {
        super(props);

        this.state = {
            textNanolesson: null,
        };
    }

    public render() {
        const { textNanolesson } = this.state;

        return (
            <div.LessonText data-cy='lesson-text' onLayout={ev => this.onLayout(ev)}>
                <Text data-cy='text-wrapper' style={{ flexDirection: 'row' }}>
                    {
                        textNanolesson &&
                        textNanolesson.map((tn, index) =>
                            <div.Text key={this.id++}
                                onPress={event => this.onTextPress(tn.nanolesson, event)}
                                hasNanolesson={!!tn.nanolesson}
                                ref={ref => this.textRefs[index] = ref}>

                                {tn.text}

                            </div.Text>
                        )
                    }
                </Text>
            </div.LessonText>
        );
    }

    public componentDidMount() {
        this.loadTextNanolesson();
    }

    private loadTextNanolesson() {
        const { text, markers } = this.props;

        let textNanolesson = utils.splitText(text, markers);

        this.setState({ textNanolesson });
    }

    private onLayout(_ev: LayoutChangeEvent) {
        // console.log('++ ev.nativeEvent', ev.nativeEvent);
    }

    private onTextPress(nanolesson: string, event: GestureResponderEvent) {
        if (!nanolesson) {
            return;
        }

        const lessonCurrentNanolesson: LessonCurrentNanolesson = {
            nanolesson,
            position: {
                x: event.nativeEvent.pageX,
                y: event.nativeEvent.pageY,
            },
            pxScrolled: 0,
        };

        showCurrentNanolesson(true);
        setLessonCurrentNanolesson(lessonCurrentNanolesson);
    }
}
