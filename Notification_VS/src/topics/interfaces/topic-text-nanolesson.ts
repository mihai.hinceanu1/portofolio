/**
 * Contains the chunk of text and the nanolesson attached to it.
 * The nanolesson key might be null, which means that there is no nanolesson
 * for that chunk of text
 */
export interface TextNanolesson {
    text: string;
    nanolesson: string;
}
