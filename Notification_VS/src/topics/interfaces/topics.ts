import { ITopic } from './topic';

// ====== ROUTE QUERY PARAMS ======

// RESTORE
/** Query params used to control the progress of the topic page */
export interface TopicPageQueryParams {

    /** Besides topics, a topic page has sections (ex: comments) */
    section?: string;

    /** Topic path name */
    topic?: string;

    /** Visible topic state (step) */
    step?: string;

    // PARKED - We decided not to bind the overlays to query params
    // Opening such a route would confuse the user
    /** Topics requested for quick study in overlay mode */
    /* overlay: string[] */

    /** Progress made for each step */
    /* overlayStep: string[] */

}

// REVIEW
export interface PopularTopicsTop {
    topic: ITopic;
    reacts: number;
}

// ====== TOPICS OVERLAY ======

// REVIEW, seems to be no longer used
/** Topic featured in search overlay */
export interface IFeaturedTopic {
    _id: string;
    name: string;
    pathName: string;

    /** Limited to 200 chars */
    content: string;
}

/** Related topics displayed in search overlay */
export interface IRelatedTopic {
    _id: string;
    name: string;
    pathName: string;
}

// DEPRECATE Not used anymore
/** Far easier to find an replace compared to array. */
export interface TopicsDict {
    [topicUuid: string]: ITopic;
}

/** Paths used to get the topic page and selected subtopic. */
export interface TopicPageReq {

    /** Main topic path name. */
    pathName: string;

    /** Current (selected) child topic. */
    childPathName: string;

    /** Id of the current user the request. */
    userId: string;
}

/** Newly created topic */
export interface NewTopicResponse {
    message: string;
    id: string;
}

/** Used for creating a new topic from taxonomy page */
export interface TaxonomyNewTopic {
    name: string;
    pathName: string;
    parentId: string;
}

/** Used for creating a new CHILD topic from topic admin page */
export interface TopicsAdminNewChildTopic {
    name: string;

    /**
     * The parentPathName of the parent(main) topic
     * it is used in epic to get the admin page after creating a new child topic
     */
    parentPathName: string;

    /** The pathName of the newly created CHILD topic */
    pathName: string;

    parentTopicId: string;
}