import { TopicText } from './topic-text';
import { Image } from '../../images/interfaces/image';
import { ImagesSlider } from '../../images/interfaces/images-slider';
import { Layout } from '../../layouts/interfaces/layout';
import { CodeFLow } from '../../source-code/interfaces/code-flow';
import { CodeRevision } from '../../source-code/interfaces/code-revision';
import { CodeSnippet } from '../../source-code/interfaces/code-snippet';
import { BLOCK_TYPES } from '../constants/topics';

/**
 * Topic blocks in dictionary format.
 * Dictionary was selected because it has greater typescript coverage.
 * It is also easier to extend with plugins.
 * All these arrays are aggregated from the db.
 *
 * <!> WARNING
 * Community modules that add new data types need to merge new props into this interface.
 */
export interface TopicBlocksDict {
    [key: string]: any[]; // Union types cannot be merged

    text: TopicText[];
    image: Image[];
    imagesSlider: ImagesSlider[];
    codeSnippet: CodeSnippet[];
    codeFlow: CodeFLow[];
    codeRevision: CodeRevision[];
    layout: Layout[];

    // ... To be dynamically extended with community data types
}

/**
 * Lessons are split between topics.
 * Topics are split in blocks.
 */
export interface TopicBlockOrderFlag {
    type: BLOCK_TYPES;
    blockId: string;
    order: number;
}