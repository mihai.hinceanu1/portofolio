import { CommonMetadata } from '../../shared/interfaces/common-metadata';

// REVIEW ? DEPRECATE
/**
 * Aggregation of users feelings towards a topic.
 * Missing props are considered to have "0" number value implicitly.
 */
export interface TopicSentiment {

    // TODO Get rid of any, group sentiments in one uniform block
    [prop: string]: any;

    topicId: string;
    numOfThumbsUp?: number;
    numOfThumbsDown?: number;
    numOfLaugh?: number; // Happy face
    numOfParty?: number; // Party icon
    numOfConfused?: number; // Confused face
    numOfFaved?: number; // Love icon
}

// REVIEW ? DEPRECATE
/**  Arrays of  thumbsUp,  thumbsDown, laugh, party, confused, faved with user IDs */
export interface TopicSentimentNew extends CommonMetadata {
    thumbsUp: string[];
    thumbsDown: string[];
    laugh: string[];
    party: string[];
    confused: string[];
    faved: string[];
}

// DEPRECATED ? REFACTOR
// export interface TopicSentimentIdAndName {
//     sentimentId: string;
//     sentimentName: string;
// }