import { AttachedTopic, AttachedTopicDto } from './topic-attachments';
import { TopicBlockOrderFlag, TopicBlocksDict } from './topic-blocks';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * A topic is a small part of a lesson focused on a single study subject.
 * Topics can exist standalone or as part of lessons.
 * Topics can be related to other topics.
 * Topics can be associated to taxonomy levels, layouts, and many other content types.
 */
export interface ITopic extends TopicCommon {

    /**
     * Blocks connected to this topic
     * <!> The blocks are never stored in the topic in sorted order.
     * The sorting happens just before rendering the page.
     * We avoid the sorting here because we would be forced to use type any.
     */
    blocks: TopicBlocksDict;

    /** Topics can have other topics attached. */
    attached: AttachedTopic[];

}

export interface TopicCommon extends CommonMetadata {

    /** Title displayed for navigation. */
    name: string;

    /** The url where this topic can be found. */
    pathName: string;

    /** The topic is project oriented, focused on practice, not on theory. */
    isProject: boolean;

    /** Suggestive image for the topic. Displayed in search results and roadmap. */
    thumbnail: string;

    /** Can be accessed by simply hovering a topic link or in the search results */
    description: string;

    // difficulty: string; // RESTORE
}

export interface TopicDto extends TopicCommon {

    /** Order of text fragments, code snippets, images and other data types. */
    blocksOrder: TopicBlockOrderFlag[];

    attached: AttachedTopicDto[];

}