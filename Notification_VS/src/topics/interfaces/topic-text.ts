import { TextMarker, TextMarkerDto } from './text-markers';
import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * One child topic (project headline) can contain multiple fragments of code and text.
 * Therefore we need to separate the text in smaller parts.
 * Besides texts there are many other lesson content blocks.
 * - Images, Layouts, CodeFlows, CodeSnippets, etc
 */
export interface TopicText extends TopicTextCommon {
    markers: TextMarker[];
}

export interface TopicTextCommon extends CommonMetadata {
    text: string;
}

export interface TopicTextDto extends TopicTextCommon {
    markers: TextMarkerDto[];
}

// REVIEW ALL OF THESE BELLOW