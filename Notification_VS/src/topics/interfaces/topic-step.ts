// RESTORE
/**
 * Topics can have steps that are designed to showcase a certain part of the content.
 * Tooltips, highlights and various other UI interactions can be triggered by a certain topic step.
 * Multiple steps in a topic work together as a slideshow presentation.
 * Topic steps are stored together with the topic object in the database.
 * Topic steps cannot be used as a standalone unit of information independent of a topic.
 * Topics steps can control at the same time helpers (topics, tooltips) for code, content and images.
 *
 * <!> Part of TopicModel, therefore, it does not extend CommonMetadata
 */
export interface TopicStep {

    _id: string;

    /** Used in admin to identify tooltips in human a readable way. */
    name: string;

    /** Syncs the url query string params */
    pathName: string;

    /** Short fragment of text explaining a particular detail about the topic */
    description: string;

    /**
     * Tooltips activated by this topic step.
     * Tooltips data is stored already on the `Topic` object.
     * Therefore, we do not cache the tooltips in `TopicSteps` object.
     */
    tooltipsIds: string[];

}