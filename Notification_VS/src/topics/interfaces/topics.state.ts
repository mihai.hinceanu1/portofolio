import { PopularTopicsTop, TopicsDict } from './topics';
import { ITopicSummary } from '../../layouts/interfaces/topic-layout';
import { Lesson } from '../../lessons/interfaces/lesson';
import { PackageVersion } from '../../source-code/interfaces/package-version';
import { ICodePackage } from '../../source-code/interfaces/packages';
import { SourceFile } from '../../source-code/interfaces/source-file';

export interface TopicsState {

    // TODO Move to lesson module
    expertMode: boolean;

    // DEPRECATE No longer used
    /**
     * Each topic is stored in the state store only once.
     * No duplicates are allowed.
     * All the other state store topic arrays simply use the UUIDs to indicate which topic is rendered.
     * This approach works in sync with the REST api topic page, which was built to reduce payload and avoid duplicates.
     * <!> Only topics need such a setup because of the multiple relations that they can have.
     */
    uniqueTopics: TopicsDict;

    // TODO rename to topics
    topicPage: Lesson;

    // TODO REVIEW it does not make sense
    firstPopularTopics: PopularTopicsTop[];

    // DEPRECATED
    /**
     * Main topic displayed in the topic/project page.
     * This object contains actually a lot of information that will be rendered in the page.
     * Additional DTOs are retrieved at the same time using the TopicPage endpoint.
     * Faved, Goal, Children, Code snippets, Images, Author, Primers, Taxonomy,  etc...
     */
    mainTopicId: string;

    // DEPRECATED
    versionId: string;

    // DEPRECATED
    addCodeSnippetFileId: string;

    // DEPRECATED
    /** Children topics of the main topic. */
    childrenTopicsIds: string[];

    // TODO RESTORE ?
    /**
     * Current child topic is specified in the URL and rendered in the GUI a bit differently.
     * Main and children topics.
     * Main is considered selected when the query param for current child is not defined.
     */
    currentTopicPath: string;

    // TODO RESTORE ?
    /**
     * For each topic id we match a topic step id (main & children).
     * Not persisted on the server or between navigation events.
     */
    topicsStepsIds: { [key: string]: number };

    // MOVE OR DELETE - THey do not belong here
    packages: ICodePackage[];
    packageVersions: PackageVersion[];
    sourceFiles: SourceFile[];

    /** Popular topics are displayed at the end of the topic page only (for the main topic). */
    popularTopicsIds: string[];

    // DEPRECATED
    // TODO REVIEW, design seems odd
    /** Sentiment is an aggregation of all the votes of all users */
    // sentiment: TopicSentimentIdAndName;

    // REVIEW is this used?
    /**
     * Each topic can have a summary connected to it
     * Renders a small idea about the topic subject
     */
    summaries: ITopicSummary[];

    // CODE SMELL - REFACTOR
    // The need to reset modals each time a new one opens indicates something better can be done.
    // Only one modal allowed at a time.
    // This means we need to use an enum.
    // MOVE OR DELETE - THey do not belong here
    modal: {
        addImageModal: boolean;
        addCodeSnippetModal: boolean;
        // addPrimerTopicModal: boolean; // PARKED ? OR DELETE
        addFileModal: boolean;
        codeWithAnnotationModal: boolean;
        deleteCommentConfirmModal: boolean;
        addNewChildTopicModal: boolean;
        seeAllCommentsModal: boolean;
        [key: string]: boolean;
    };

    // REVIEW is it used?
    /** Topic overlay is used to quickly render a topic over the current lesson. */
    overlay: {

        /** The topics overlay is visible when opening an auto-link for reading more about a topic. */
        isVisible: boolean;

        /**
         * Each time overlay is opened, is will be from this codeSnippetId
         * Used to highlight the active codeSnippet the overlay was opened from
         */

        activeCodeSnippetId: string;

        /**
         * Topic currently displayed in overlay/preview mode.
         * Usually triggered from autolinks tooltips.
         */
        topicId: string;

        /**
         * For each topic id we match a topic step id.
         * Not persisted on hte server or between navigation events.
         */
        topicsStepsIds: { [key: string]: number };
    };

    // MOVE from here
    globalMap: {

        /** Topics selection by domain. Used while hovering map elements. */
        domainTopicsIds: string[];

        /** Topics selection by category. Used while hovering map elements. */
        categoryTopicsIds: string[];

        /** Topics selection by topLevelTopic. Used while hovering map elements. */
        topLevelTopicTopicsIds: string[];

    };

    // REVIEW Delete this, no longer needed
    localMap: {

        /** Renders the distinct branches of the local map. */
        categoriesTopicsIds: string[];

        /** Main node in the graph (hub and spokes). */
        hubTopicId: string;

        /** Topics listed on the local map branches (categories). */
        topicsIds: string[];
    };

    // TODO REVIEW CODE SMELL, something is odd here
    user: {

        sentimentResponse: string;
    };

}
