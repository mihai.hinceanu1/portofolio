import { ITopic } from './topic';

// DEPRECATE ALL OF THESE
// Fundamentally wrong approach :(
// ids are passed via route params

// DEPRECATE
export interface EditTopicResp {
    message: string;
    id: string;
}

// DEPRECATE
export interface TopicCodeSnippetsEdit {
    snippetId: string;
    topic: ITopic;
}

// DEPRECATE
export interface TopicImageEdit {
    imageId: string;
    topic: ITopic;
}

// DEPRECATE
export interface ChildrenTopicCrud {
    childId: string;
    topicId: string;
}

// DEPRECATE
export interface TopicMenuActions {
    type: string;
    topicId: string;
    content?: {
        _id: string;
        elementId?: string;
    };
}
