import { ITopic } from './topic';
import { TOPIC_RELATIONS } from '../constants/topics';

/**
 * Blocks can be attached to other blocks
 * Depending on the relation type the GUI renders differently.
 * Ex: Topics can have extra help topics attached to them.
 */
export interface AttachedTopic {
    role: TOPIC_RELATIONS;
    topic: ITopic;
    order: number;
}

export interface AttachedTopicDto {
    role: TOPIC_RELATIONS;
    topic: string;
    order: number;
}