import { CommonMetadata } from '../../shared/interfaces/common-metadata';

/**
 * Minimalist form for the related topic.
 * Used to render the lesson navigators.
 */
export interface RelatedTopic {
    _id: string;
    name: string;
    pathName: string;
    isProject: boolean;
}

export interface RelatedTopicsDto extends CommonMetadata {

    /**
     * Generated from ML scripts.
     * Indicates the estimated strength of the relation.
     * Relations under a certain threshold will not be displayed.
     */
    weight: number;

    /**
     * Topic relations will be stored as edges on a graph
     * <!> Later we are going to use a graph DB to store these relations
     */
    edge: [string, string];
}