import { TopicSentiment } from './topic-sentiment';
import { Comment } from '../../comments/interfaces/comment';
import { INanolesson } from '../../nano-lessons/interfaces/nano-lessons';

/**
 * Markers are used to isolate text characters as words.
 * These markers are then used as anchors for comments, sentiments and nano lessons.
 * Markers are generated from the server only for the words that are worth commenting.
 * By design, users are not allowed to create markers.
 * Keeping one marker to one word relation makes far more easy to manage text interactions.
 */
export interface TextMarker extends TextMarkerCommon {
    nanoLesson: INanolesson;
    comments: Comment;
    sentiment: TopicSentiment;
}

/** Common metadata between server and frontend. */
export interface TextMarkerCommon {

    /** Marker area defined as string indexes. */
    position: {
        start: number;
        end: number;
    };
}

/** References to other objects in mongoDB */
export interface TextMarkerDto extends TextMarkerCommon {
    nanoLesson: string;
    comments: string;
    sentiment: string;
}