import * as topicsModalsActions from './actions/topics-modals.actions';
import * as actions from './actions/topics.actions';
import { topicsInitialState } from './topics.init-state';
import { Lesson } from '../../lessons/interfaces/lesson';
import { Action } from '../../shared/interfaces/shared';
import { PopularTopicsTop } from '../interfaces/topics';
import { TopicsState } from '../interfaces/topics.state';
// import { resetModals } from '../services/topics.utils';

export const topicsReducer = (state: TopicsState = topicsInitialState, action: Action<any>): TopicsState => {

    switch (action.type) {
        case actions.GET_NEW_TOPIC_PAGE_OK: {
            return {
                ...state,
                topicPage: action.payload as Lesson,
            };
        }

        case actions.GET_FIRST_POPULAR_TOPICS_OK: {
            return {
                ...state,
                firstPopularTopics: action.payload as PopularTopicsTop[],
            };
        }

        case actions.SET_EXPERT_MODE: {
            return {
                ...state,
                expertMode: action.payload as boolean,
            };
        }

        // DEPRECATED ? REFACTOR
        // case actions.SET_ADD_PRIMER_TOPIC_MODAL_OPEN: {
        //     let newModalsState = resetModals(state);

        //     return {
        //         ...state,
        //         modal: {
        //             ...newModalsState,
        //             addPrimerTopicModal: action.payload as boolean,
        //         },
        //     };
        // }

        case actions.SET_ADD_NEW_CHILD_TOPIC_MODAL: {
            // let newModalsState = resetModals(state);

            return {
                ...state,
                modal: {
                    ...state.modal,
                    addNewChildTopicModal: action.payload as boolean,
                },
            };
        }

        case topicsModalsActions.SET_CODE_WITH_ANNOTATION_MODAL: {
            // let newModalsState = resetModals(state);

            return {
                ...state,
                modal: {
                    ...state.modal,
                    codeWithAnnotationModal: action.payload as boolean,
                },
            };
        }

        case topicsModalsActions.SET_DELETE_COMMENT_CONFIRM_MODAL: {
            // let newModalsState = resetModals(state);

            return {
                ...state,
                modal: {
                    ...state.modal,
                    deleteCommentConfirmModal: action.payload as boolean,
                },
            };
        }

        case topicsModalsActions.SET_SEE_ALL_COMMENTS_MODAL: {
            // let newModalsState = resetModals(state);

            return {
                ...state,
                modal: {
                    ...state.modal,
                    seeAllCommentsModal: action.payload as boolean,
                },
            };
        }

        case actions.GET_OVERLAY_TOPIC_OK: {
            let topicId = action.payload as string;
            return {
                ...state,
                overlay: {
                    ...state.overlay,
                    topicId,
                },
            };
        }

        case actions.CACHE_CURRENT_TOPIC_PATH: {
            return {
                ...state,
                currentTopicPath: action.payload as string,
            };
        }

        case actions.CACHE_CURRENT_TOPIC_STEP_ID: {
            let { currentTopicPath } = state;

            // Fail safe - Not all pages have a topic selected
            if (!currentTopicPath) return state;

            let newState: TopicsState = {
                ...state,
                topicsStepsIds: {
                    ...state.topicsStepsIds,
                },
            };

            newState.topicsStepsIds[currentTopicPath] = action.payload as number;

            return newState;
        }

        case actions.CACHE_OVERLAY_TOPIC_STEP: {

            return {
                ...state,
                /* overlayTopicStepId: action.payload as number, */
            };
        }

        case actions.TOGGLE_TOPICS_OVERLAY: {
            let isVisible: boolean = action.payload ? action.payload : !state.overlay.isVisible;
            return {
                ...state,
                overlay: {
                    ...state.overlay,
                    isVisible,
                },
            };
        }

        default:
            return state;
    }
};
