import { TopicsState } from '../interfaces/topics.state';

export const topicsInitialState: TopicsState = {
    expertMode: false,
    topicPage: null,
    overlay: null,
    modal: null,
    versionId: null,
    addCodeSnippetFileId: null,
    sourceFiles: null,
    user: null,
    firstPopularTopics: null,
    // sentiment: null, // DEPRECATED
    childrenTopicsIds: null,
    currentTopicPath: null,
    globalMap: null,
    localMap: null,
    mainTopicId: null,
    packageVersions: null,
    packages: null,
    popularTopicsIds: null,
    summaries: null,
    topicsStepsIds: null,
    uniqueTopics: null,
};