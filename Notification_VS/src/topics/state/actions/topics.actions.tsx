import { Lesson } from '../../../lessons/interfaces/lesson';
import { Action, PaginatedSearchReq } from '../../../shared/interfaces/shared';
import { ITopic } from '../../interfaces/topic';
import { PopularTopicsTop, TaxonomyNewTopic, TopicPageReq } from '../../interfaces/topics';

// ====== GET TOPIC ======
// Used in the topic page

// DEPRECATED ? REFACTOR
// export const GET_TOPIC_REQ = 'GET_TOPIC_REQ';
// export const getTopicReq = (id: string): Action<string> => ({
//     type: GET_TOPIC_REQ,
//     payload: id,
// });

// DEPRECATED
// export const GET_TOPIC_OK = 'GET_TOPIC_OK';
// export const getTopicOk = (topic: ITopic): Action<ITopic> => ({
//     type: GET_TOPIC_OK,
//     payload: topic,
// });

// DEPRECATED
// export const GET_TOPIC_FAIL = 'GET_TOPIC_FAIL';
// export const getTopicFail = (error: Error): Action<Error> => ({
//     type: GET_TOPIC_FAIL,
//     payload: error,
// });

export const GET_USER_TOPICS_REQ = 'GET_USER_TOPICS_REQ';
export const getUserTopicsReq = (): Action<null> => ({
    type: GET_USER_TOPICS_REQ,
    payload: null,
});

// ====== GET TOPICS BY USER FOR ADMIN ======

// DEPRECATED ? REFACTOR
// export const GET_USER_TOPICS_OK = 'GET_USER_TOPICS_OK';
// export const getUserTopicsOk = (topics: Lesson[]): Action<Lesson[]> => ({
//     type: GET_USER_TOPICS_OK,
//     payload: topics,
// });

// DEPRECATED
// export const GET_USER_TOPICS_FAIL = 'GET_USER_TOPICS_FAIL';
// export const getUserTopicsFail = (): Action<null> => ({
//     type: GET_USER_TOPICS_FAIL,
//     payload: null,
// });

// ====== GET TOPIC PAGE ======

export const GET_NEW_TOPIC_PAGE_REQ = 'GET_NEW_TOPIC_PAGE_REQ';
export const getNewTopicPageReq = (pathName: string): Action<string> => ({
    type: GET_NEW_TOPIC_PAGE_REQ,
    payload: pathName,
});

export const GET_NEW_TOPIC_PAGE_OK = 'GET_NEW_TOPIC_PAGE_OK';
export const getNewTopicPageOk = (topicPage: Lesson): Action<Lesson> => ({
    type: GET_NEW_TOPIC_PAGE_OK,
    payload: topicPage,
});

export const GET_NEW_TOPIC_PAGE_FAIL = 'GET_NEW_TOPIC_PAGE_FAIL';
export const getNewTopicPageFail = (error: Error): Action<Error> => ({
    type: GET_NEW_TOPIC_PAGE_FAIL,
    payload: error,
});

// DEPRECATED ? REFACTOR
// ====== GET RELATED TOPICS ======

// export const GET_RELATED_TOPICS_REQ = 'GET_RELATED_TOPICS_REQ';
// export const getRelatedTopicsReq = (): Action<null> => ({
//     type: GET_RELATED_TOPICS_REQ,
// });

// export const GET_RELATED_TOPICS_OK = 'GET_RELATED_TOPICS_OK';
// export const getRelatedTopicsOk = (topic: ITopic): Action<ITopic> => ({
//     type: GET_RELATED_TOPICS_OK,
//     payload: topic,
// });

// export const GET_RELATED_TOPICS_FAIL = 'GET_RELATED_TOPICS_FAIL';
// export const getRelatedTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_RELATED_TOPICS_FAIL,
//     payload: error,
// });

// ====== GET TOPIC PAGE ======

export const GET_TOPIC_PAGE_REQ = 'GET_TOPIC_PAGE_REQ';
export const getTopicPageReq = (cfg: TopicPageReq): Action<TopicPageReq> => ({
    type: GET_TOPIC_PAGE_REQ,
    payload: cfg,
});

// DEPRECATED to be deleted or replaced ? REFACTOR
// export const GET_TOPIC_PAGE_OK = 'GET_TOPIC_PAGE_OK';
// export const getTopicPageOk = (topicPage: TopicPageResp): Action<TopicPageResp> => ({
//     type: GET_TOPIC_PAGE_OK,
//     payload: topicPage,
// });

// DEPRECATED
// export const GET_TOPIC_PAGE_FAIL = 'GET_TOPIC_PAGE_FAIL';
// export const getTopicPageFail = (error: Error): Action<Error> => ({
//     type: GET_TOPIC_PAGE_FAIL,
//     payload: error,
// });

// ====== EDIT TOPIC ======

export const EDIT_TOPIC_REQ = 'EDIT_TOPIC_REQ';
export const editTopicReq = (topic: ITopic): Action<ITopic> => ({
    type: EDIT_TOPIC_REQ,
    payload: topic,
});

export const EDIT_TOPIC_OK = 'EDIT_TOPIC_OK';
export const editTopicOk = (): Action<null> => ({
    type: EDIT_TOPIC_OK,
});

export const EDIT_TOPIC_FAIL = 'EDIT_TOPIC_FAIL';
export const editTopicFail = (error: Error): Action<Error> => ({
    type: EDIT_TOPIC_FAIL,
    payload: error,
});

// ====== DELETE TOPIC ======

export const DELETE_TOPIC = 'DELETE_TOPIC';
export const deleteTopic = (id: string): Action<string> => ({
    type: DELETE_TOPIC,
    payload: id,
});

export const DELETE_TOPIC_OK = 'DELETE_TOPIC_OK';
export const deleteTopicOk = (): Action<null> => ({
    type: DELETE_TOPIC_OK,
});

export const DELETE_TOPIC_FAIL = 'DELETE_TOPIC_FAIL';
export const deleteTopicFail = (error: Error): Action<Error> => ({
    type: DELETE_TOPIC_FAIL,
    payload: error,
});

// ====== CREATE TOPIC ======

export const CREATE_TOPIC_REQ = 'CREATE_TOPIC_REQ';
export const createTopicReq = (topicCfg: TaxonomyNewTopic): Action<TaxonomyNewTopic> => ({
    type: CREATE_TOPIC_REQ,
    payload: topicCfg,
});

export const CREATE_TOPIC_OK = 'CREATE_TOPIC_OK';
export const createTopicOk = (resp: string): Action<string> => ({
    type: CREATE_TOPIC_OK,
    payload: resp,
});

export const CREATE_TOPIC_FAIL = 'CREATE_TOPIC_FAIL';
export const createTopicFail = (error: Error): Action<Error> => ({
    type: CREATE_TOPIC_FAIL,
    payload: error,
});

// ====== CREATE CHILD TOPIC ======

// DEPRECATED ? REFACTOR
// export const CREATE_CHILD_TOPIC_REQ = 'CREATE_CHILD_TOPIC_REQ';
// export const createChildTopicReq = (childTopicCfg: TopicsAdminNewChildTopic): Action<TopicsAdminNewChildTopic> => ({
//     type: CREATE_CHILD_TOPIC_REQ,
//     payload: childTopicCfg,
// });

// export const CREATE_CHILD_TOPIC_OK = 'CREATE_CHILD_TOPIC_OK';
// export const createChildTopicOk = (resp: string): Action<string> => ({
//     type: CREATE_CHILD_TOPIC_OK,
//     payload: resp,
// });

// export const CREATE_CHILD_TOPIC_FAIL = 'CREATE_CHILD_TOPIC_FAIL';
// export const createChildTopicFail = (error: Error): Action<Error> => ({
//     type: CREATE_CHILD_TOPIC_FAIL,
//     payload: error,
// });

// ====== GET ADMIN TOPIC ======
// Used in the admin page

// DEPRECATED ? REFACTOR
// export const GET_ADMIN_TOPIC_REQ = 'GET_ADMIN_TOPIC_REQ';
// export const getAdminTopicReq = (id: string): Action<string> => ({
//     type: GET_ADMIN_TOPIC_REQ,
//     payload: id,
// });

// export const GET_ADMIN_TOPIC_OK = 'GET_ADMIN_TOPIC_OK';
// export const getAdminTopicOk = (topic: ITopic): Action<ITopic> => ({
//     type: GET_ADMIN_TOPIC_OK,
//     payload: topic,
// });

// export const GET_ADMIN_TOPIC_FAIL = 'GET_ADMIN_TOPIC_FAIL';
// export const getAdminTopicFail = (error: Error): Action<Error> => ({
//     type: GET_ADMIN_TOPIC_FAIL,
//     payload: error,
// });

// DEPRECATED
export const SET_ADMIN_TOPIC = 'SET_ADMIN_TOPIC';
export const setAdminTopic = (topic: ITopic): Action<ITopic> => ({
    type: SET_ADMIN_TOPIC,
    payload: topic,
});

// Used to get the admin topic by it's path for the admin topic page.
export const GET_ADMIN_TOPIC_BY_PATH_REQ = 'GET_ADMIN_TOPIC_BY_PATH_REQ';
export const getAdminTopicByPathReq = (topicPath: string): Action<string> => ({
    type: GET_ADMIN_TOPIC_BY_PATH_REQ,
    payload: topicPath,
});

// GET TOPICS BY NUMBER
export const GET_ADMIN_TOPICS_BY_NUMBER_REQ = 'GET_ADMIN_TOPICS_BY_NUMBER_REQ';
export const getAdminTopicsByNumber = (numberOfTopics: number): Action<number> => ({
    type: GET_ADMIN_TOPICS_BY_NUMBER_REQ,
    payload: numberOfTopics,
});

export const GET_ADMIN_TOPICS_BY_NUMBER_OK = 'GET_ADMIN_TOPICS_BY_NUMBER_OK';
export const getAdminTopicsByNumberOk = (topics: ITopic[]): Action<ITopic[]> => ({
    type: GET_ADMIN_TOPICS_BY_NUMBER_OK,
    payload: topics,
});

export const GET_ADMIN_TOPICS_BY_NUMBER_FAIL = 'GET_ADMIN_TOPICS_BY_NUMBER_FAIL';
export const getAdminTopicByNumberFail = (error: Error): Action<Error> => ({
    type: GET_ADMIN_TOPICS_BY_NUMBER_FAIL,
    payload: error,
});

// Last Topic Edited.
export const LAST_TOPIC_EDITED = 'LAST_TOPIC_EDITED';
export const lastTopicEdited = (editTopic: ITopic): Action<ITopic> => ({
    type: LAST_TOPIC_EDITED,
    payload: editTopic,
});

// DEPRECATED
// Selected Primer Topic.
// export const SELECTED_PRIMER_TOPIC = 'SELECTED_PRIMER_TOPIC';
// export const selectedPrimerTopic = (primerTopic: ITopic): Action<ITopic> => ({
//     type: SELECTED_PRIMER_TOPIC,
//     payload: primerTopic,
// });

// ====== GET ALL TOPICS ======
// Used in admin to list all topics.
// Improve with sorting params.
export const GET_ALL_ADMIN_TOPICS_REQ = 'GET_ALL_ADMIN_TOPICS_REQ';
export const getAllAdminTopicsReq = (): Action<ITopic[]> => ({
    type: GET_ALL_ADMIN_TOPICS_REQ,
    payload: null,
});

export const GET_ALL_ADMIN_TOPICS_OK = 'GET_ALL_ADMIN_TOPICS_OK';
export const getAllAdminTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
    type: GET_ALL_ADMIN_TOPICS_OK,
    payload: topics,
});

export const GET_ALL_ADMIN_TOPICS_FAIL = 'GET_ALL_ADMIN_TOPICS_FAIL';
export const getAllAdminTopicsFail = (error: Error): Action<Error> => ({
    type: GET_ALL_ADMIN_TOPICS_FAIL,
    payload: error,
});

// GET FIRST POPULAR TOPICS
export const GET_FIRST_POPULAR_TOPICS_REQ = 'GET_FIRST_POPULAR_TOPICS_REQ';
export const getFirstPopularTopicsReq = (): Action<null> => ({
    type: GET_FIRST_POPULAR_TOPICS_REQ,
    payload: null,
});

export const GET_FIRST_POPULAR_TOPICS_OK = 'GET_FIRST_POPULAR_TOPICS_OK';
export const getFirstPopularTopicsOk = (topics: PopularTopicsTop[]): Action<PopularTopicsTop[]> => ({
    type: GET_FIRST_POPULAR_TOPICS_OK,
    payload: topics,
});

export const GET_FIRST_POPULAR_TOPICS_FAIL = 'GET_FIRST_POPULAR_TOPICS_FAIL';
export const getFirstPopularTopicsFail = (error: Error): Action<Error> => ({
    type: GET_FIRST_POPULAR_TOPICS_FAIL,
    payload: error,
});

// DEPRECATE ? REFACTOR
// // Older (paginated version)
// export const GET_ALL_TOPICS_REQ = 'GET_ALL_TOPICS_REQ';
// export const getAllTopicsReq = (request: Paginated): Action<Paginated> => ({
//     type: GET_ALL_TOPICS_REQ,
//     payload: request,
// });

// export const GET_ALL_TOPICS_OK = 'GET_ALL_TOPICS_OK';
// export const getAllTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
//     type: GET_ALL_TOPICS_OK,
//     payload: topics,
// });

// export const GET_ALL_TOPICS_FAIL = 'GET_ALL_TOPICS_FAIL';
// export const getAllTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_ALL_TOPICS_FAIL,
//     payload: error,
// });

// DEPRECATE ? REFACTOR
// ====== SEARCH TOPICS ======

export const SEARCH_TOPICS_REQ = 'SEARCH_TOPICS_REQ';
export const searchTopicsReq = (request: PaginatedSearchReq): Action<PaginatedSearchReq> => ({
    type: SEARCH_TOPICS_REQ,
    payload: request,
});

export const SEARCH_TOPICS_OK = 'SEARCH_TOPICS_OK';
export const searchTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
    type: SEARCH_TOPICS_OK,
    payload: topics,
});

export const SEARCH_TOPICS_FAIL = 'SEARCH_TOPICS_FAIL';
export const searchTopicsFail = (error: Error): Action<Error> => ({
    type: SEARCH_TOPICS_FAIL,
    payload: error,
});

// DEPRECATE ? REFACTOR
// // ====== GET DOMAIN TOPICS ======
// // Used while hovering map elements.

// export const GET_DOMAIN_TOPICS_REQ = 'GET_DOMAIN_TOPICS_REQ';
// export const getDomainTopicsReq = (request: PaginatedChildrenReq): Action<PaginatedChildrenReq> => ({
//     type: GET_DOMAIN_TOPICS_REQ,
//     payload: request,
// });

// export const GET_DOMAIN_TOPICS_OK = 'GET_DOMAIN_TOPICS_OK';
// export const getDomainTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
//     type: GET_DOMAIN_TOPICS_OK,
//     payload: topics,
// });

// export const GET_DOMAIN_TOPICS_FAIL = 'GET_DOMAIN_TOPICS_FAIL';
// export const getDomainTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_DOMAIN_TOPICS_FAIL,
//     payload: error,
// });

// ====== GET CATEGORY TOPICS ======
// Used while hovering map elements.

// DEPRECATE ? REFACTOR
// export const GET_CATEGORY_TOPICS_REQ = 'GET_CATEGORY_TOPICS_REQ';
// export const getCategoryTopicsReq = (request: PaginatedChildrenReq): Action<PaginatedChildrenReq> => ({
//     type: GET_CATEGORY_TOPICS_REQ,
//     payload: request,
// });

// export const GET_CATEGORY_TOPICS_OK = 'GET_CATEGORY_TOPICS_OK';
// export const getCategoryTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
//     type: GET_CATEGORY_TOPICS_OK,
//     payload: topics,
// });

// export const GET_CATEGORY_TOPICS_FAIL = 'GET_CATEGORY_TOPICS_FAIL';
// export const getCategoryTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_CATEGORY_TOPICS_FAIL,
//     payload: error,
// });

// ====== GET TOP LEVEL TOPIC TOPICS ======
// Used while hovering map elements.

// DEPRECATE ? REFACTOR
// export const GET_TOP_LEVEL_TOPIC_TOPICS_REQ = 'GET_TOP_LEVEL_TOPIC_TOPICS_REQ';
// export const getTopLevelTopicTopicsReq = (request: PaginatedChildrenReq): Action<PaginatedChildrenReq> => ({
//     type: GET_TOP_LEVEL_TOPIC_TOPICS_REQ,
//     payload: request,
// });

// export const GET_TOP_LEVEL_TOPIC_TOPICS_OK = 'GET_TOP_LEVEL_TOPIC_TOPICS_OK';
// export const getTopLevelTopicTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
//     type: GET_TOP_LEVEL_TOPIC_TOPICS_OK,
//     payload: topics,
// });

// export const GET_TOP_LEVEL_TOPIC_TOPICS_FAIL = 'GET_TOP_LEVEL_TOPIC_TOPICS_FAIL';
// export const getTopLevelTopicTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_TOP_LEVEL_TOPIC_TOPICS_FAIL,
//     payload: error,
// });

// ====== GET CHILDREN TOPICS ======
//  Used while hovering map elements.

// DEPRECATE ? REFACTOR
// export const GET_CHILDREN_TOPICS_REQ = 'GET_CHILDREN_TOPICS_REQ';
// export const getChildrenTopicsReq = (id: string): Action<string> => ({
//     type: GET_CHILDREN_TOPICS_REQ,
//     payload: id,
// });

// export const GET_CHILDREN_TOPICS_OK = 'GET_CHILDREN_TOPICS_OK';
// export const getChildrenTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
//     type: GET_CHILDREN_TOPICS_OK,
//     payload: topics,
// });

// export const GET_CHILDREN_TOPICS_FAIL = 'GET_CHILDREN_TOPICS_FAIL';
// export const getChildrenTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_CHILDREN_TOPICS_FAIL,
//     payload: error,
// });

// ====== CACHE CURRENT TOPIC PATH ======

// DEPRECATE ? REFACTOR
export const CACHE_CURRENT_TOPIC_PATH = 'CACHE_CURRENT_TOPIC_PATH';
export const cacheCurrentTopicPath = (pathName: string): Action<string> => ({
    type: CACHE_CURRENT_TOPIC_PATH,
    payload: pathName,
});

// ====== GET RECENT TOPICS ======

// DEPRECATE ? REFACTOR
// export const GET_RECENT_TOPICS_REQ = 'GET_RECENT_TOPICS_REQ';
// export const getRecentTopicsReq = (request: Paginated): Action<Paginated> => ({
//     type: GET_RECENT_TOPICS_REQ,
//     payload: request,
// });

// export const GET_RECENT_TOPICS_OK = 'GET_RECENT_TOPICS_OK';
// export const getRecentTopicsOk = (topics: ITopic[]): Action<ITopic[]> => ({
//     type: GET_RECENT_TOPICS_OK,
//     payload: topics,
// });

// export const GET_RECENT_TOPICS_FAIL = 'GET_RECENT_TOPICS_FAIL';
// export const getRecentTopicsFail = (error: Error): Action<Error> => ({
//     type: GET_RECENT_TOPICS_FAIL,
//     payload: error,
// });

// ====== GET OVERLAY TOPIC ======

// DEPRECATE ? REFACTOR
export const GET_OVERLAY_TOPIC_REQ = 'GET_OVERLAY_TOPIC_REQ';
export const getOverlayTopicReq = (id: string): Action<string> => ({
    type: GET_OVERLAY_TOPIC_REQ,
    payload: id,
});

export const GET_OVERLAY_TOPIC_OK = 'GET_OVERLAY_TOPIC_OK';
export const getOverlayTopicOk = (topic: ITopic): Action<ITopic> => ({
    type: GET_OVERLAY_TOPIC_OK,
    payload: topic,
});

export const GET_OVERLAY_TOPIC_FAIL = 'GET_OVERLAY_TOPIC_FAIL';
export const getOverlayTopicFail = (error: Error): Action<Error> => ({
    type: GET_OVERLAY_TOPIC_FAIL,
    payload: error,
});

// ====== TOGGLE TOPICS OVERLAY ======

// RESTORE
export const TOGGLE_TOPICS_OVERLAY = 'TOGGLE_TOPICS_OVERLAY';
export const toggleTopicsOverlay = (isVisible: boolean): Action<boolean> => ({
    type: TOGGLE_TOPICS_OVERLAY,
    payload: isVisible,
});

// ====== CACHE CURRENT TOPIC STEP ======

// DEPRECATE ? REFACTOR
export const CACHE_CURRENT_TOPIC_STEP_ID = 'CACHE_CURRENT_TOPIC_STEP_ID';
export const cacheCurrentTopicStepId = (id: number): Action<number> => ({
    type: CACHE_CURRENT_TOPIC_STEP_ID,
    payload: id,
});

// ====== CACHE OVERLAY TOPIC STEP ======

// DEPRECATE ? REFACTOR
export const CACHE_OVERLAY_TOPIC_STEP = 'CACHE_OVERLAY_TOPIC_STEP';
export const cacheOverlayTopicStep = (topicStep: number): Action<number> => ({
    type: CACHE_OVERLAY_TOPIC_STEP,
    payload: topicStep,
});

export const SET_EXPERT_MODE = 'SET_EXPERT_MODE';
export const setExpertMode = (expertMode: boolean): Action<boolean> => ({
    type: SET_EXPERT_MODE,
    payload: expertMode,
});

// ====== MODAL FLAGS(ON/OFF SWITCHES) ======

// DEPRECATED
// export const SET_ADD_PRIMER_TOPIC_MODAL_OPEN = 'SET_ADD_PRIMER_TOPIC_MODAL_OPEN';
// export const setAddPrimerTopicModalOpen = (addPrimerTopicModalOpen: boolean): Action<boolean> => ({
//     type: SET_ADD_PRIMER_TOPIC_MODAL_OPEN,
//     payload: addPrimerTopicModalOpen,
// });

export const SET_ADD_NEW_CHILD_TOPIC_MODAL = 'SET_ADD_NEW_CHILD_TOPIC_MODAL';
export const setAddNewChildTopicModal = (flag: boolean): Action<boolean> => ({
    type: SET_ADD_NEW_CHILD_TOPIC_MODAL,
    payload: flag,
});

// DEPRECATED ? REFACTOR
// ====== EDIT TOPIC SENTIMENT ======

// export const EDIT_TOPIC_SENTIMENT_REQ = 'EDIT_TOPIC_SENTIMENT_REQ';
// export const editTopicSentimentReq = (request: TopicSentimentIdAndName): Action<TopicSentimentIdAndName> => ({
//     type: EDIT_TOPIC_SENTIMENT_REQ,
//     payload: request,
// });

// export const EDIT_TOPIC_SENTIMENT_OK = 'EDIT_TOPIC_SENTIMENT_OK';
// export const editTopicSentimentOk = (response: string): Action<string> => ({
//     type: EDIT_TOPIC_SENTIMENT_OK,
//     payload: response,
// });

// export const EDIT_TOPIC_SENTIMENT_FAIL = 'EDIT_TOPIC_SENTIMENT_FAIL';
// export const editTopicSentimentFail = (error: Error): Action<Error> => ({
//     type: EDIT_TOPIC_SENTIMENT_FAIL,
//     payload: error,
// });