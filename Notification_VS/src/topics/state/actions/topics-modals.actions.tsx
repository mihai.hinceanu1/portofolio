import { Action } from '../../../shared/interfaces/shared';

export const SET_CODE_WITH_ANNOTATION_MODAL = 'SET_CODE_WITH_ANNOTATION_MODAL';
export const setCodeWithAnnotationModal = (isVisible: boolean): Action<boolean> => ({
    type: SET_CODE_WITH_ANNOTATION_MODAL,
    payload: isVisible,
});

// RESTORE IN COMMENTS
export const SET_DELETE_COMMENT_CONFIRM_MODAL = 'SET_DELETE_COMMENT_CONFIRM_MODAL';
export const setDeleteCommentConfirmModal = (isVisible: boolean): Action<boolean> => ({
    type: SET_DELETE_COMMENT_CONFIRM_MODAL,
    payload: isVisible,
});

// RESTORE IN COMMENTS
export const SET_SEE_ALL_COMMENTS_MODAL = 'SET_SEE_ALL_COMMENTS_MODAL';
export const setSeeAllCommentsModal = (seeAllCommentsModal: boolean): Action<boolean> => ({
    type: SET_SEE_ALL_COMMENTS_MODAL,
    payload: seeAllCommentsModal,
});