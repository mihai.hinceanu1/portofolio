import { ITopicSummary } from '../../layouts/interfaces/topic-layout';
import { Lesson } from '../../lessons/interfaces/lesson';
import { AppState } from '../../shared/interfaces/app.state';
import { ITopic } from '../interfaces/topic';
import { TopicStep } from '../interfaces/topic-step';
import { PopularTopicsTop, TopicsDict } from '../interfaces/topics';
import { TopicsState } from '../interfaces/topics.state';
import { createSelector, Selector } from 'reselect';
// import { TopicSentimentIdAndName } from '../../interfaces/topic-sentiment';

const TOPICS_MODULE: Selector<AppState, TopicsState> = (state: AppState) => state.topics;

/** All unique topics stored currently in the state store. */
export const UNIQUE_TOPICS = createSelector<AppState, TopicsState, TopicsDict>(
    TOPICS_MODULE,
    state => state.uniqueTopics,
);

export const TOPIC_PAGE = createSelector<AppState, TopicsState, Lesson>(
    TOPICS_MODULE,
    state => state.topicPage,
);

export const FIRST_POPULAR_TOPICS = createSelector<AppState, TopicsState, PopularTopicsTop[]>(
    TOPICS_MODULE,
    state => state.firstPopularTopics,
);

/** One admin topic selected by id for taxonomy page, to display topic info attached to a level */

/** Current topic/project rendered in the topic/project page */
export const MAIN_TOPIC = createSelector<AppState, TopicsState, ITopic>(
    TOPICS_MODULE,
    state => {
        let { uniqueTopics } = state;

        // Fail safe
        if (!uniqueTopics) return;

        return state.uniqueTopics[state.mainTopicId];
    },
);

export const CHILDREN_TOPICS = createSelector<AppState, TopicsState, ITopic[]>(
    TOPICS_MODULE,
    state => {
        if (!state.childrenTopicsIds) { return; }
        return state.childrenTopicsIds.map(id => state.uniqueTopics[id]);
    },
);

export const SUMMARIES_TOPICS = createSelector<AppState, TopicsState, ITopicSummary[]>(
    TOPICS_MODULE,
    (state: TopicsState) => state.summaries,
);

// export const RECENT_TOPICS = createSelector<AppState, TopicsState, Topic[]>(
//     TOPICS_MODULE,
//     state => {
//         if (!state.recentTopicsIds) { return; }
//         return state.recentTopicsIds.map(id => state.uniqueTopics[id]);
//     },
// );

export const CURRENT_TOPIC = createSelector<AppState, TopicsState, ITopic>(
    TOPICS_MODULE,
    state => getCurrTopicFromState(state),
);

var getCurrTopicFromState = (state: TopicsState) => {
    let { uniqueTopics } = state;

    // Fail safe
    if (!uniqueTopics) return;

    // Selected
    let ids = Object.keys(uniqueTopics);
    let topics = ids.map(id => uniqueTopics[id]);
    let match: ITopic = topics.find(topic => topic.pathName === state.currentTopicPath);

    return match;
};

export const CURRENT_TOPIC_STEP = createSelector<AppState, TopicsState, ITopic, TopicStep>(
    TOPICS_MODULE,
    CURRENT_TOPIC,
    (_state: TopicsState, _currTopic: ITopic) => {

        // RESTORE
        // // Fail safe
        // if (!currTopic || !currTopic.steps) return;
        // if (!state.topicsStepsIds) return;

        // let index = state.topicsStepsIds[currTopic.pathName];
        // let step: TopicStep = currTopic.steps[index];

        // return step;
        return null;
    },
);

export const OVERLAY_TOPIC = createSelector<AppState, TopicsState, ITopic>(
    TOPICS_MODULE,
    state => {

        // Fail safe - Topics not loaded yet
        if (!state.uniqueTopics) return undefined;

        return state.uniqueTopics[state.overlay.topicId];
    },
);

// TODO fix Topic interface
export const OVERLAY_TOPIC_STEP = createSelector<AppState, TopicsState, any, TopicStep>(
    // export const OVERLAY_TOPIC_STEP = createSelector<AppState, TopicsState, Topic, TopicStep>(
    TOPICS_MODULE,
    OVERLAY_TOPIC,
    (_state: TopicsState, _overlayTopic: ITopic) => {

        // RESTORE
        // Fail safe - Topic not yet loaded
        // if (!overlayTopic || !overlayTopic.steps) return undefined;

        // let index = state.overlay.topicsStepsIds[overlayTopic._id];
        // return overlayTopic.steps[index];
        return null;
    },
);

export const TOPICS_OVERLAY_VISIBILITY = createSelector<AppState, TopicsState, boolean>(
    TOPICS_MODULE,
    state => state.overlay.isVisible,
);

export const POPULAR_TOPICS = createSelector<AppState, TopicsState, ITopic[]>(
    TOPICS_MODULE,
    state => {
        if (!state.uniqueTopics || !state.popularTopicsIds) { return; }
        let allTopics: ITopic[] = Object.keys(state.uniqueTopics)
            .map(uuid => state.uniqueTopics[uuid]);

        let popularTopics: ITopic[] = allTopics.filter(topic =>
            state.popularTopicsIds.includes(topic._id),
        );

        return popularTopics;
    },
);

// DEPRECATED
// export const TOPICS_SENTIMENT = createSelector<AppState, TopicsState, TopicSentimentIdAndName>(
//     TOPICS_MODULE,
//     state => state.sentiment,
// );

export const ADD_EXPERT_MODE = createSelector<AppState, TopicsState, boolean> (
    TOPICS_MODULE,
    (state: TopicsState) => state.expertMode,
);

/** ON/OFF MODAL */

// DEPRECATED
// export const ADD_PRIMER_TOPIC_MODAL_OPEN = createSelector<AppState, TopicsState, boolean>(
//     TOPICS_MODULE,
//     (state: TopicsState) => state.modal.addPrimerTopicModal,
// );

export const CODE_WITH_ANNOTATION_MODAL = createSelector<AppState, TopicsState, boolean>(
    TOPICS_MODULE,
    (state: TopicsState) => state.modal.codeWithAnnotationModal,
);

export const ADD_NEW_CHILD_TOPIC_MODAL = createSelector<AppState, TopicsState, boolean>(
    TOPICS_MODULE,
    (state: TopicsState) => state.modal.addNewChildTopicModal,
);