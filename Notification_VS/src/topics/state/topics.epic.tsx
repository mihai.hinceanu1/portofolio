import * as topicsActions from './actions/topics.actions';
import { Action, PaginatedSearchReq } from '../../shared/interfaces/shared';
import { ITopic } from '../interfaces/topic';
import * as topicsWebapi from '../webapis/topics.webapi';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';

/**
 * DEPRECATE
 * New Epics
 * TODO remove old ones if not necessary
 */
export const getNewTopicPage$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(topicsActions.GET_NEW_TOPIC_PAGE_REQ).pipe(
        map(action => action.payload),
        concatMap(pathname => {
            return topicsWebapi.getNewTopicPage(pathname).pipe(
                map(topicPage => {
                    return topicsActions.getNewTopicPageOk(topicPage);
                }),
                catchError(error => {
                    return (of(topicsActions.getNewTopicPageFail(error)));
                }),
            );

        }),
    );

// ====== TOPICS ======

export const getTopicByPathName$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(topicsActions.GET_ADMIN_TOPIC_BY_PATH_REQ).pipe(
        map(action => action.payload),
        concatMap(pathName => topicsWebapi.getTopicByPathName(pathName).pipe(
            map(topic => topicsActions.setAdminTopic(topic)),
            // catchError(error => of(topicsActions.))
        )),
    );

export const getAdminTopicsByNumber$ = (action$: ActionsObservable<Action<number>>) =>
    action$.ofType(topicsActions.GET_ADMIN_TOPICS_BY_NUMBER_REQ).pipe(
        map(action => action.payload),
        concatMap(numberOfTopics => topicsWebapi.getTopicsByNumber(numberOfTopics).pipe(
            map(topics => topicsActions.getAdminTopicsByNumberOk(topics),
                catchError(error => of(topicsActions.getAdminTopicByNumberFail(error)))),
        )),
    );

export const getFirstPopularTopics = (action$: ActionsObservable<Action<null>>) =>
    action$.ofType(topicsActions.GET_FIRST_POPULAR_TOPICS_REQ).pipe(
        map(action => action.payload),
        concatMap(() => topicsWebapi.getFirstPopularTopics().pipe(
            map(firstPopularTopics => topicsActions.getFirstPopularTopicsOk(firstPopularTopics),
                catchError(error => of(topicsActions.getFirstPopularTopicsFail(error)))),
        )),
    );

export const editTopic$ = (action$: ActionsObservable<Action<ITopic>>) =>
    action$.ofType(topicsActions.EDIT_TOPIC_REQ).pipe(
        map(action => action.payload),
        concatMap(topic => topicsWebapi.editTopic(topic).pipe(
            map(() => {
                // topicsService.getTopicPage();
                // topicsService.getAllAdminTopics();

                // TODO Return edited topic from the same EP.
                // topicsService.getTopicById(topic._id); // Update only the changed topic (Save to uniqueTopic)
                return topicsActions.editTopicOk(); // Update only the changed topic (Save to uniqueTopic)
            }),
            catchError(error => of(topicsActions.editTopicFail(error))),
        )),
    );

export const deleteTopic$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(topicsActions.DELETE_TOPIC).pipe(
        map(action => action.payload),
        concatMap(id => topicsWebapi.deleteTopic(id).pipe(
            map(() => {
                // TODO Update state store.
                // topicsService.getAllTopics();
                return topicsActions.deleteTopicOk();
            }),
            catchError(error => of(topicsActions.deleteTopicFail(error))),
        )),
    );

export const getAllAdminTopics$ = (action$: ActionsObservable<Action<null>>) =>
    action$.ofType(topicsActions.GET_ALL_ADMIN_TOPICS_REQ).pipe(
        map(action => action.payload),
        concatMap(() => topicsWebapi.getAllAdminTopics().pipe(
            map(adminTopics => topicsActions.getAllAdminTopicsOk(adminTopics)),
            catchError(err => of(topicsActions.getAllAdminTopicsFail(err))),
        )),
    );

export const searchTopics = (action$: ActionsObservable<Action<PaginatedSearchReq>>) =>
    action$.ofType(topicsActions.SEARCH_TOPICS_REQ).pipe(
        map(action => action.payload),
        concatMap(request => topicsWebapi.searchTopicsByKeyword(request).pipe(
            map(topics => topicsActions.searchTopicsOk(topics)),
            catchError(error => of(topicsActions.searchTopicsFail(error))),
        )),
    );

export const getOverlayTopic$ = (action$: ActionsObservable<Action<string>>) =>
    action$.ofType(topicsActions.GET_OVERLAY_TOPIC_REQ).pipe(
        map(action => action.payload),
        concatMap(topicPath => topicsWebapi.getTopic(topicPath).pipe(
            map(topic => topicsActions.getOverlayTopicOk(topic)),
            catchError(error => of(topicsActions.getOverlayTopicFail(error))),
        )),
    );

// DEPRECATED ? REFACTOR
// export const editTopicSentiment$ = (action$: ActionsObservable<Action<TopicSentimentIdAndName>>) =>
//     action$.ofType(topicsActions.EDIT_TOPIC_SENTIMENT_REQ).pipe(
//         map(action => action.payload),
//         concatMap(request => {
//             return topicsWebapi.editTopicSentiment(request).pipe(
//                 map(response => topicsActions.editTopicSentimentOk(response)),
//                 catchError(error => of(topicsActions.editTopicSentimentFail(error))),
//             );
//         }),
//     );
