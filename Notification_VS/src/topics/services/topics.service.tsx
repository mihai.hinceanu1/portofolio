import { ITopicSummary } from '../../layouts/interfaces/topic-layout';
import { Lesson } from '../../lessons/interfaces/lesson';
import { store, store$ } from '../../shared/services/state.service';
import { ITopic } from '../interfaces/topic';
import {
    PopularTopicsTop,
    TaxonomyNewTopic,
    TopicPageReq,
    TopicsDict
    } from '../interfaces/topics';
import * as topicsModalsActions from '../state/actions/topics-modals.actions';
import * as actions from '../state/actions/topics.actions';
import * as sel from '../state/topics.selectors';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

// RENAME to topic$
export const topicPage$ = (): Observable<Lesson> => store$.pipe(
    map(state => sel.TOPIC_PAGE(state)),
    skipWhile(topicPage => !topicPage),
);

// DEPRECATED - No longer used
export function getTopicPageNew(pathname: string) {
    store.dispatch(
        // DEPRECATE, it's no longe going to be needed
        actions.getNewTopicPageReq(pathname),
    );
}

export function getUserTopics() {
    store.dispatch(
        actions.getUserTopicsReq(),
    );
}

export const uniqueTopics$ = (): Observable<TopicsDict> => store$.pipe(
    map(state => sel.UNIQUE_TOPICS(state)),
    skipWhile(topics => !topics),
    distinctUntilChanged(),
);

export const firstPopularTopics$ = (): Observable<PopularTopicsTop[]> => store$.pipe(
    map(state => sel.FIRST_POPULAR_TOPICS(state)),
    skipWhile(firstPopularTopics => !firstPopularTopics),
    distinctUntilChanged(),
);

export function getFirstPopularTopics() {
    store.dispatch(
        actions.getFirstPopularTopicsReq(),
    );
}

export const mainTopic$ = (): Observable<ITopic> => store$.pipe(
    map(state => sel.MAIN_TOPIC(state)),
    skipWhile(topic => !topic),
    distinctUntilChanged(),
);

export const childrenTopics$ = (): Observable<ITopic[]> => store$.pipe(
    map(state => sel.CHILDREN_TOPICS(state)),
    skipWhile(topics => !topics),
    distinctUntilChanged(),
);

// export const recentTopics$ = (): Observable<Topic[]> => store$.pipe(
//     map(state => sel.RECENT_TOPICS(state)),
//     skipWhile(topics => !topics),
//     distinctUntilChanged(),
// );

export const summariesTopics$ = (): Observable<ITopicSummary[]> => store$.pipe(
    map(state => sel.SUMMARIES_TOPICS(state)),
    skipWhile(summaries => !summaries),
    distinctUntilChanged(),
);

export const expertMode$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.ADD_EXPERT_MODE(state)),
    skipWhile(expertMode => expertMode === undefined),
    distinctUntilChanged(),
);

export function setExpertMode(expertMode: boolean) {
    store.dispatch(
        actions.setExpertMode(expertMode),
    );
}

// ====== MODAL ON/OFF SWITCH ======

export const codeWithAnnotationModal$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.CODE_WITH_ANNOTATION_MODAL(state)),
    skipWhile(codeWithAnnotationModal => codeWithAnnotationModal === undefined),
    distinctUntilChanged(),
);

export function setCodeWithAnnotationModal(isVisiblle: boolean) {
    store.dispatch(
        topicsModalsActions.setCodeWithAnnotationModal(isVisiblle),
    );
}

export const addNewChildTopicTopicModal$ = (): Observable<boolean> => store$.pipe(
    map(state => sel.ADD_NEW_CHILD_TOPIC_MODAL(state)),
    skipWhile(flag => flag === undefined),
    distinctUntilChanged(),
);

export function setAddNewChildTopicTopicModal(isToggled: boolean) {
    store.dispatch(
        actions.setAddNewChildTopicModal(isToggled),
    );
}

// ====== GET TOPIC PAGE ======

/** Used to render the topic page together with all related DTOs. */
export function getTopicPage(req: TopicPageReq) {
    store.dispatch(
        actions.getTopicPageReq(req),
    );
}

// ====== EDIT TOPIC ======

export function editTopic(topic: ITopic) {
    store.dispatch(
        actions.editTopicReq(topic),
    );
}

// ====== DELETE TOPIC ======

export function deleteTopic(id: string) {
    store.dispatch(
        actions.deleteTopic(id),
    );
}

/** Create new topic from taxonomy page */
export function createTopic(topicCfg: TaxonomyNewTopic) {
    store.dispatch(
        actions.createTopicReq(topicCfg),
    );
}
