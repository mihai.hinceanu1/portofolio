const webpack = require('webpack');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const { override, disableEsLint, babelInclude, addBabelPlugins } = require('customize-cra');
const path = require('path');

/**
 * Custom webpack setup built for the web build
 * We needed to integrate monaco, disable console warnings and many more things
 */
function customWebpackSetup(config, env) {

    // Disable hot module warning in console
    config.resolve.alias['react-dom'] = '@hot-loader/react-dom';

    // Enable react native linear gradient for the web
    // Hint from here: https://github.com/react-native-community/react-native-svg/issues/1106
    config.resolve.alias['react-native'] = 'react-native-web';
    config.resolve.alias['react-native-linear-gradient'] = 'react-native-web-linear-gradient';

    config.plugins = [
        ...config.plugins,

        // Disable react dev tools warning in console
        new webpack.DefinePlugin({
            '__REACT_DEVTOOLS_GLOBAL_HOOK__': '({ isDisabled: true })',
        }),

        new MonacoWebpackPlugin({
            // Available options are documented at
            // https://github.com/Microsoft/monaco-editor-webpack-plugin#options
            languages: ['javascript', 'html', 'css'],
        }),
    ];

    // Reduce HMR console messages
    config.devServer = {
        clientLogLevel: 'warning$',
    };

    return config;
}

/**
 * Customize CRA is an additional way to work with rewire-react-app
 * It provides custom helper functions and it uses the composition pattern
 * Luckily we can als write our own function, thus we managed to keep the original setup
 */
module.exports = override(
    customWebpackSetup,

    // TODO REVIEW why useEslintRc is not working
    // We do not want to completely disable the linter in dev server

    // Take back control on es lint
    // useEslintRc(path.resolve(__dirname, '.eslintrc.json')),
    disableEsLint(),

    // Enable class properties for react native chart kit
    babelInclude([
        path.resolve('src'),
        path.resolve('node_modules/react-native-chart-kit'),

        // Enable linear gradient
        path.resolve('node_modules/react-native-linear-gradient'),
    ]),

    addBabelPlugins(
        '@babel/plugin-proposal-class-properties'
    )
);