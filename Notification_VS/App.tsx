import { App } from './src/app';
import { Router } from './src/routing';
import * as React from 'react';
import { AppRegistry, YellowBox } from 'react-native';
(global as any).process.version = '0.1.0';

YellowBox.ignoreWarnings(['Require cycle']);
console.ignoredYellowBox = ['Require cycle'];
console.disableYellowBox = true;

// Render the app for mobile (android, ios)
export default class AppMobile extends React.Component<never, never> {
    public render() {
        return (
            <Router>
                <App />
            </Router>
        );
    }
}

AppRegistry.registerComponent('visual_school', () => AppMobile);