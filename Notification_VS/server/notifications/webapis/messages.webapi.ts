import { MessagesRepository } from '../services/messages.service';
import { Router } from 'express';
import * as reset from '../services/notifications-reset.service';

export const MessagesWebapi = Router();

MessagesWebapi.get('/', async (_req, res) => {
    try {
        let notifications = await MessagesRepository.getAll();

        res.send(notifications);
    } catch (err) {
        res.status(500).send(err);
    }
});

MessagesWebapi.post('/', async (_req, res) => {
    try {
        let notifications = await MessagesRepository.resetMessages();

        res.send(notifications);
    } catch (err) {
        res.status(500).send(err);
    }
});