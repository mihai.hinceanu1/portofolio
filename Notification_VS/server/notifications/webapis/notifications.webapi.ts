import { NotificationsRepository } from '../services/notifications.service';
import { Router } from 'express';
import { INotificationModel } from '../models/notification.model';

export const NotificationsWebapi = Router();

NotificationsWebapi.get('/', async (_req, res) => {
    try {
        let notifications = await NotificationsRepository.getAll();

        res.send(notifications);
    } catch (err) {
        res.status(500).send(err);
    }
});

NotificationsWebapi.get('/:id', async (req, res) => {
    try {
        const { id } = req.params;

        let notification = await NotificationsRepository.getById(id);

        res.send(notification);
    } catch (err) {
        res.status(500).send(err);
    }
});

NotificationsWebapi.post('/reset', async (_req, res) => {
    try {
        let notifications = await NotificationsRepository.resetNotifications();

        res.send(notifications);
    } catch (err) {
        res.status(500).send(err);
    }
});
