import { CommonMetadataSchema } from '../../shared/models/common-metadata';
import { INotification } from '../interfaces/notification';
import { Document, model, Schema } from 'mongoose';

export let NotificationSchema: Schema = new Schema({
    ...CommonMetadataSchema.obj,
    icon: { type: String, required: true },
    title: { type: String, required: true },
    type: String,
    unread: Boolean
}, {
    versionKey: false,
});

// Model
export interface INotificationModel extends INotification, Document { }
export const NotificationModel = model<INotificationModel>('notifications_notifications', NotificationSchema);