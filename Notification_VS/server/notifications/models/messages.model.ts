import { CommonMetadataSchema } from '../../shared/models/common-metadata';
import { IMessage } from '../interfaces/message';
import { Document, model, Schema } from 'mongoose';

export let MessageSchema: Schema = new Schema({
    ...CommonMetadataSchema.obj,
    title: { type: String, required: true},
    autor: String,
    message: String,
    isUnread: Boolean
}, {
    versionKey: false,
});

MessageSchema.set('toJSON', {
    transform: (_doc: Document, message: IMessage, _options: any) => {
        return message;
    },

    /**
     * Virtuals transforms "_id" to "id". In .NET Core, we will have only "id"s, not "_id"s.
     * Transform right now it will help us on the frontend side...
     * On the Node part, _id will still be used
     */
    // virtuals: true,
});

// Model
export interface IMessageModel extends IMessage, Document { }
export const MessageModel = model<IMessageModel>('messages_messages', MessageSchema);