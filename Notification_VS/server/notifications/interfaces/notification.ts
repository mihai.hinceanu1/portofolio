import { CommonMetadata } from '../../../src/shared/interfaces/common-metadata';

export interface INotification extends CommonMetadata {
    
    title: string;
    howOld: number;
    type: string;
    unread: boolean;
}
