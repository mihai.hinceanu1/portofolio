import { CommonMetadata } from '../../../src/shared/interfaces/common-metadata';

export interface IMessage extends CommonMetadata {
    
    title: string;
    autor: string;
    message: string;
    isUnread: boolean;
}
