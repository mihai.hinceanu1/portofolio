import { NotificationsWebapi } from './webapis/notifications.webapi';
import { MessagesWebapi } from './webapis/messages.webapi';
import express from 'express';

export class Notifications {
    constructor(app: express.Express) {
        this.setupNotificationsWebapi(app);
        this.setupMessagesWebapi(app);
    }

    private setupNotificationsWebapi(app: express.Express) {
        app.use(
            '/api/notifications',
            NotificationsWebapi,
        );
    }

    private setupMessagesWebapi(app: express.Express) {
        app.use(
            '/api/messages',
            MessagesWebapi,
        );
    }
}
