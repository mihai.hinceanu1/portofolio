import { requireUncached } from '../../shared/services/utils.service';
import { MessageModel } from '../models/messages.model';
import { IMessage } from '../interfaces/message';
import { MongoError } from '../../../src/shared/interfaces/shared';

export abstract class MessagesRepository {
    public static async getAll() {
        try {
            
            let messages = await MessageModel.find({});

            return messages;
        }
        catch (err) {
            throw err;
        }
    }

    public static async resetMessages() {
        return new Promise((resolve, reject) => {
            MessageModel.remove({}, err => {
                if (err) reject(err);
        
                let sample = requireUncached('../../notifications/mocks/messages.mocks.json');
        
                MessageModel.create(...sample, (err: Error) => {
                    if (err) reject(err);
        
                    resolve('messages_messages');
                });
            });
        })
    }
}