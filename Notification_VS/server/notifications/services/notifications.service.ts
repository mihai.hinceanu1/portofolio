import { NotificationModel } from '../models/notification.model';
import { requireUncached } from '../../shared/services/utils.service';

export abstract class NotificationsRepository {
    public static async getAll() {
        try {

            let notifications = await NotificationModel.find({});

            return notifications;
        }
        catch (err) {
            throw err;
        }
    }

    public static async getById(id: string) {
        try {
            let notification = await NotificationModel.find({ _id: id });

            return notification;
        } catch (err) {
            throw err;
        }
    }

    public static async resetNotifications() {
        return new Promise((resolve, reject) => {
            NotificationModel.remove({}, err => {
                if (err) reject(err);

                let sample = requireUncached('../../notifications/mocks/notifications.mocks.json');

                NotificationModel.create(...sample, (err: Error) => {
                    if (err) reject(err);

                    resolve('notifications ready');
                });
            });
        })
    }
}