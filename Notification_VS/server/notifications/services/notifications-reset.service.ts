import { requireUncached } from '../../shared/services/utils.service';
import { NotificationModel } from '../models/notification.model';

export async function resetNotificationsCollections() {
    try {
        let collectionResponse = await resetNotifications();

        return collectionResponse;
    } catch(err) {
        throw err;
    }
}

async function resetNotifications() {
    try {
        let samples = requireUncached('../../notifications/mocks/notifications.mocks.json');

        await NotificationModel.remove({});
        await NotificationModel.create(...samples);

        return 'notifications_notifications';
    } catch (err) {
        throw err;
    }
}
