import { MongoError } from './../src/shared/interfaces/shared';
import { Auth } from './auth/auth';
import { Comments } from './comments/comments';
import { Feedback } from './feedback/feedback';
import { Icons } from './icons/icons';
import { Images } from './images/images';
import { Layouts } from './layouts/layouts';
import { LessonsCatalog } from './lesson-catalog/lesson-catalog';
import { Lessons } from './lessons/lessons';
import { Maps } from './maps/maps';
import { NanoLessons } from './nano-lessons/nano-lessons';
import { Notifications } from './notifications/notifications';
import { Search } from './search/search';
import { passportStrategy } from './shared/services/passport.service';
import { writeToAPILog } from './shared/services/write-to-file.service';
import { Shared } from './shared/shared';
import { SourceCode } from './source-code/source-code';
import { Taxonomy } from './taxonomy/taxonomy';
import { Topics } from './topics/topics';
import { User as Users } from './users/users';
import { SERVER_CFG } from '../config/server.config';
import bodyParser from 'body-parser';
import log from 'colors';
import cors from 'cors';
import express from 'express';
import fileUpload from 'express-fileupload';
import RateLimit from 'express-rate-limit';
import fs from 'fs';
import { Server } from 'http';
import mongoose from 'mongoose';
import passport from 'passport';
import path from 'path';

// Constants
/* const PUBLIC_DIR: string = path.join(__dirname, '../public'); */
const IS_DEVELOPMENT: boolean = process.env.NODE_ENV !== 'production';
// <!> When working on the server solution, we need fast server boot times.
//     This can be achieved by rendering static files.
// IS_DEVELOPMENT: boolean = false,
/** During tests we want to inhibit some parts of the entire startup sequence. */
let IS_TESTING: boolean = false;

// Compiler and app
let app: express.Express = express();
let server: Server;

export class ServerApp {

    // Modules
    public auth: Auth;
    public comments: Comments;
    public feedback: Feedback;
    public gallery: Images;
    public icons: Icons;
    public layouts: Layouts;
    public lessons: Lessons;
    public lessonCatalog: LessonsCatalog;
    public maps: Maps;
    public search: Search;
    public shared: Shared;
    public sourceCode: SourceCode;
    public taxonomy: Taxonomy;
    public topics: Topics;
    public users: Users;
    public nanoLessons: NanoLessons;
    public notifications: Notifications;

    constructor(isTesting: boolean = false) {
        IS_TESTING = isTesting;

        this.setupBodyParser();
        this.setupCors();
        this.setupFileUpload();
        this.setupRateLimiter();
        this.setupAPILog();
        this.passportInit();
        this.initModules(); // <!> Init modules and webapis after body parser (routes order is important).
        this.connectToDatabase();
        this.servePublicFiles();
    }

    /** Listen for incoming requests */
    public listen(PORT: number, callback?: () => {}): void {
        server = app.listen(PORT, callback);
    }

    public close(callback: () => {}): void {
        server.close(callback);
    }

    private initModules() {
        this.auth = new Auth(app);
        this.comments = new Comments(app);
        this.feedback = new Feedback(app);
        this.gallery = new Images(app);
        this.icons = new Icons(app);
        this.layouts = new Layouts(app);
        this.lessons = new Lessons(app);
        this.maps = new Maps(app);
        this.search = new Search(app);
        this.shared = new Shared(app);
        this.sourceCode = new SourceCode(app);
        this.taxonomy = new Taxonomy(app);
        this.topics = new Topics(app);
        this.users = new Users(app);
        this.nanoLessons = new NanoLessons(app);
        this.lessonCatalog = new LessonsCatalog(app);
        this.notifications = new Notifications(app);
    }

    /** Connect to the mongo database */
    private connectToDatabase() {
        mongoose.connect(
            SERVER_CFG.databaseUrl,
            (err: MongoError) => {
                if (err) {
                    console.log(log.red(err.message));
                } else {
                    console.log(log.green('Connection to MongoDB successfully'));
                }
            }
        );
    }

    /** Passport initialization function */
    private passportInit() {
        if (IS_TESTING === true) {
            return;
        }
        app.use(passport.initialize());
        app.use(passport.session());

        passportStrategy(passport);
    }

    /** Parse PUT req.body instead of getting undefined */
    private setupBodyParser() {
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
    }

    private setupCors() {
        app.use(cors());
    }

    /** Setup rate limiter for DOS attacks handling */
    private setupRateLimiter() {
        if (IS_TESTING === true) {
            return;
        }

        // A rate limiter can be set for each API Endpoint if desired
        const rateLimit = RateLimit({
            windowMs: 10 * 60 * 1000, // 15 minutes
            max: 2000, // Limit each IP to 100 requests per windowMs
            message: 'Too many requests',
        });

        app.use('/api', rateLimit);
    }

    /** Log every api call in a file */
    private setupAPILog() {
        if (IS_TESTING === true) {
            return;
        }
        const filePath = path.join(__dirname, '..', '/logs/api-calls.txt');

        // Create file on server start
        const createStream = fs.createWriteStream(filePath);
        createStream.end();

        app.use('/api', (req, _res, next) => {
            const baseUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
            writeToAPILog('api-calls.txt', req.connection.remoteAddress, req.method, baseUrl);
            next();
        });
    }

    /** Expose static resources to the public. */
    private servePublicFiles() {
        if (IS_TESTING === true) {
            return;
        }
        app.use('/assets', express.static(__dirname + '/assets'));
        app.use('/media', express.static(__dirname + '/media'));
        app.use('/uploads', express.static(__dirname + '/uploads'));
    }

    /** File Uploading setup middleware */
    private setupFileUpload() {
        app.use(fileUpload());
    }
}
