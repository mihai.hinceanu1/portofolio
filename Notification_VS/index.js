import { AppRegistry, Platform } from 'react-native';
import App from './App';

AppRegistry.registerComponent('vsc', () => App);

if (Platform.OS === 'web') {
    const rootTag = document.getElementById('root') || document.getElementById('main');
    AppRegistry.runApplication('vsc', { rootTag });
}
