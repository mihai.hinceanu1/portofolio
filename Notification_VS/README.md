## Starting the app - if emulators are already set
* Install latest node server and mongo database.
* `mongod` - Start the database. (If on windows - MacOS and Linux install Mongo as a service, which boots on OS opening)
* `npm run server` - Run the NodeJS server - Soon to be replaced by .NET Core
* `npm run web` - Run the frontend app for web.
* `npm run ios` - Run the frontend app for iOS Emulators.
* `npm run android` - Run the frontend app for Android Simulators.
* http://localhost:8080/api/shared/reset-database - Visit this endpoint to seed the database with mock data. Only before the first visit (will be automatic).

## Optional
* `npm run emulator-server` - Run the react-native server. In case the emulator server fails you need to restart it again

## Starting the app - new machines (Android)
* download `Android Studio` from [here](https://developer.android.com/studio)
* install <b>react-native-cli</b> : `npm install -g react-native`
* after downloading <b>Android Studio</b>, follow <b>Create an AVD (virtual device)</b> section from [here](https://developer.android.com/studio/run/managing-avds) and install an instance of your choice
* open Environment Variables and add the following in the <i>System variables</i>
    * ANDROID_SDK_ROOT: C:\Users\\{user}\AppData\Local\Android\Sdk (this is the default Sdk path)
    * Add to <b>Path</b> the following:
        * C:\Users\\{user}\AppData\Local\Android\Sdk\platform-tools
        * C:\Users\\{user}\AppData\Local\Android\Sdk\tools
        * C:\Users\\{user}\AppData\Local\Android\Sdk\bin

* In order to check the installation was successfully completed, type `adb` in any terminal and check if the command is executed.
* After all these steps, running `npm run android` should be enough in order to bootstrap the native app.
* More details can be found in <b>VSC Platform / Workflows / VSC Bootstrapping / Mobile Dev Env </b>

## Development routes
* http://localhost:8080 - Visual School app.
* http://localhost:8080/docs/api/visual-school - Swagger enpoints documentation. (Currently Unavailable until .Net Migration)
* http://localhost:8080/api - Webapi.

## Development commands
TODO: Update this list

* `npm build` - Build the frontend app.
* `npm test` - Automatic UI testing using Cypress.
* `http-server --cors` - Run this command in the build folder to quickly check if the build result is running properly.
* 'npm run cloc' - This command will give a report over the total amount of code written (lines of code). Needs Strawbery Pearl installed.
* `genMongoUuid()` - Type this command in the browser console to get a new ID for mongo. This is useful when creating new mocks.

## Tips
* **Linting** - We use both `Eslint` and `Tslint` (Javascript and Typescript). Before using the linters we need to install `Tslint` and `Eslint` extensions in vscode. After installing the linter extensions open User Settings and add the following setting: `"eslint.enable": true,` to enable eslinting. Tslinting is enabled simply by having the linter extension installed.
* **Debugging** - For debugging the server app using the browser console, use the NIM chrome extension. If the NIM extension does not automatically launch the console, update the configuration host with `127.0.0.1` instead of `localhost`.
* **ConEmu** Use ConEmu to control multiple terminals in the same window. A task can be configured and triggered automatically or on click. For Mac follow this tutorial: https://apple.stackexchange.com/a/101040/287222

        cmd /k npm run server -new_console:d:"C:\Projects\visual-school"
        cmd /k npm run veb -cur_console:d:"C:\Projects\visual-school"

* **Reset the database** - POST to this endpoint http://localhost:8080/api/shared/reset-database. All documents will be deleted and replaced with the mock data. After changing the mock objects it is best to restart the server app from the terminal. Required files are cached during the lifespan of a session. This means the reset script will fail to add the new changed data in the db.
* **Debugging tools** - Several debuggin tools are available as Chrome extensions
    * Redux dev tools - Inspect the state store.
    * React dev tools - Inspect react apps.
    * Node Inspector Manager - Display the output of the server app in the browser console. This has the added advantage of rendering objects similar to the regular console. This makes it far easier to inspect complex objects from the server. However there is a problem with the color codes right now, which makes this inspector a bit harder to use.
* **User snippets in VSCode** - Add in VSCode the following user snippet. It will help you to quickly type cnosle logs that can be filtered in the browser console by searching for the triple + symbol. Notice that I don't write the triple plus here because I do not want false positvies when I search in VScode for removing them. To add a snippet press `Ctrl + Shift + P` to show command inpout and type `Snippets`. Then chose the language and add the snippet down bellow. Also update double plus to triple plus. Do it for `Javascript`, `Javascript React`, `Typescript`, `Typescript React`.
```json
"Console_Log": {
    "prefix": "cns",
    "body": [
        "console.log('++ ${1:varName}', ${1:array});"
    ],
    "description": "Console log"
}
```
* **ConEmu** - Default folder: `cmd "-cur_console:d:C:\Projects"`.
* **VScode extensions** - Consider installing the following extensions:
    - Tslint + Eslint - To keep a consistent writing style.
    - Sort Typescript Imports - Enable autosave and sort by path in VScode settings.
    - Trailing Spaces - Keep an eye on formatting.
    - Change case - Quickly change selected text to any case.
    - Git blame - Find the guilty guy. No violence please! :)
    - Auto Close Tag
    - Auto Rename Tag
    - Atom One Dark/Lite Theme - Better color coding, less noise on screen.
* **Problems with npm install on Ubuntu** - Delete `package.lock`. Delete `node_modules`. cmd: `sudo npm cache --force clean`, npm install.

## Recommended User Settings (vscode)
* `"javascript.implicitProjectConfig.checkJs": true` - Enable Typescript for Javascript
* `"eslint.enable": true` - Enable linting for Javascript
* `"workbench.editor.enablePreview": false,` - If you want to keep all files opened
* `"explorer.confirmDragAndDrop": false`- Confirms are annoying
* `"explorer.confirmDelete": false`
* `"editor.codeLens": false` - Code lens can be annoying
* `"editor.find.autoFindInSelection": true` - Automatically replace only in selection (skip clicking the button)
* `"files.trimTrailingWhitespace": false` - Auto remove white space on save.