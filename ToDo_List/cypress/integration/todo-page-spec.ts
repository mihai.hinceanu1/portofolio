describe('TodoPage Tests', () => {

    it('Homepage loads successfully', () => {
        cy.visit('http://localhost:3000/');
    })

    it('Test new task button', () => {
        cy.get('.NewTaskButton').should('exist')
    })
});