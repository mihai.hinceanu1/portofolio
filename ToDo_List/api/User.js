const express = require('express');
const User = require('../server/db/User');
const route = express.Router();

route.post('/', async (req, res) => {
    const { firstName, lastName } = req.body;
    let user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    let userModel = new User(user);
    await userModel.save();
    res.json(userModel)
});

route.delete('/:userId', async (req, res, next) => {
    const id = req.params.userId;
    User.remove({_id: id}).exec()
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({ error: err }));
})

route.get('/', async (req, res, next) => {
    User.find().exec()
    .then(userList => res.status(200).json(userList))
    .catch(err => res.status(500).json({ error: err }));
})

route.get('/:userId', async (req, res, next) => {
    const id = req.params.userId;
    User.findById(id).exec().then(user => {
        if(user) {
            res.status(200).json(user)
        } else {
            res.status(404).json({message: 'User not found!'})
        }
    }).catch(err => {
        res.status(500).json({ error: err })
    });
})

module.exports = route;
