const express = require('express');
const Status = require('../server/db/Status');
const route = express.Router();

route.get('/', async (req, res, next) => {
    Status.find().exec()
    .then((status) => {
        res.status(200).json(status)
    })
    .catch(err => res.status(500).json({ error: err }));
})

// route.post('/', async(req, res) => {
//     let status = {};
//     status.code = 'important';
//     status.label = 'Flagged';
//     status.value = 0;
//     let statusModel = new Status(status);
//     await statusModel.save();
//     res.json(statusModel)
// });

module.exports = route;