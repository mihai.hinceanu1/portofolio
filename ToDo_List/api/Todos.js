const express = require('express');
const Todos = require('../server/db/Todos');
const route = express.Router();

// insert here
route.post('/', async(req, res) => {
    const { title, description } = req.body;
    let task = {};
    task.status = 'pending';
    task.title = title;
    task.description = description;
    task.isImportant = false;
    task.date = new Date();
    let taskModel = new Todos(task);
    await taskModel.save();
    res.json(taskModel)
});

// this is the update methode
route.post('/:todoId', async (req, res) => {
    const id = req.params.todoId;
    Todos.findByIdAndUpdate(id, req.body).exec()
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({error: err}))
})

// delete here
route.delete('/:todoId', async (req, res, next) => {
    const id = req.params.todoId;
    Todos.deleteOne({_id: id}).exec()
    .then(result => res.status(200).send(id))
    .catch(err => res.status(500).json({ error: err}));
})

// get all tasks order by desc
route.get('/', async (req, res, next) => {
    Todos.find().sort({'date': -1}).exec()
    .then((todoList) => {
        res.status(200).json(todoList)
    })
    .catch(err => res.status(500).json({ error: err }));
})

// get task by id
route.get('/:todoId', async (req, res, next) => {
    const id = req.params.todoId;
    Todos.findById(id).exec().then(todo => {
        if(todo) {
            res.status(200).json(todo)
        } else {
            res.status(404).json({message: 'Task not found!'})
        }
    }).catch(err => {
        res.status(500).json({ error: err })
    });
})

module.exports = route;
