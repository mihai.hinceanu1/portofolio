const mongoose = require('mongoose');

const todos = new mongoose.Schema({
    status: {
        type: String
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    isImportant: {
        type: Boolean
    },
    date: {
        type: Date
    }
});

module.exports = Todos = mongoose.model('todos', todos);