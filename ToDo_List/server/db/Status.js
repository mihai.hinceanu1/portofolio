const mongoose = require('mongoose');

const status = new mongoose.Schema({
    code: {
        type: String
    },
    label: {
        type: String
    },
    value: {
        type: Number
    }
});

module.exports = Status = mongoose.model('status', status);