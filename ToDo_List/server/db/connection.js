const mongoose = require('mongoose');

const URI = "mongodb+srv://mihai:1qaz@todo0-phmt1.mongodb.net/test?retryWrites=true&w=majority";

const connectDB = async () => {

    mongoose.set('useFindAndModify', false);
    await mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log('DB connected !!');
}

module.exports = connectDB;

