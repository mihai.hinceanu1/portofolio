const express = require('express');
const connectDB = require('./db/connection');
const app = express();
const cors = require('cors')
const UserRouter = require('../api/User');
const TodoRouter = require('../api/Todos');
const StatusRouter = require('../api/Status');

connectDB();

app.use(cors())
app.use(express.json({extended: false}))

app.use('/api/User', UserRouter);
app.use('/api/Todos', TodoRouter);
app.use('/api/updateTodo', TodoRouter);
app.use('/api/Status', StatusRouter);


const Port = process.env.Port || 8080;
app.listen(Port, ()=> console.log(`Server is running on port ${Port}!!`));