import { initState } from '../../example/state/init-state';
import { AppState } from '../interfaces/app-state';

export const appInitialState: AppState = {
    todos: initState,
};
