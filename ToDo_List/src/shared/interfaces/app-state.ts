import { IState } from '../../example/interfaces/state-interface';

export interface AppState {
    todos: IState;
}

