import { IStatusList } from './status-interface';
import { ITask } from './task-interface';
import { IError } from './error-interface';

export interface IState {
    statusList: IStatusList[];
    arrayError: IError;
    searchArray: ITask[];

    // all tasks
    tasks: ITask[];

    // in progress tasks
    progress: ITask[];

    // pending
    pending: ITask[];

    // done
    done: ITask[];

    // important
    important: ITask[];

}