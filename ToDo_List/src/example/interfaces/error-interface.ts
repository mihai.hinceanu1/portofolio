export interface IError {
    type: String,
    message: String
}