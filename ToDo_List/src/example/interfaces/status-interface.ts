export interface IStatusList {
    code: string,
    label: string,
    value: number
}