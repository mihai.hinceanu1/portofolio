export interface ITask {
    _id: string;
    status: string;
    title: string;
    description: string;
    isImportant: boolean;
    date: Date;
}