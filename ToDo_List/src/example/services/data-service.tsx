import { store, store$ } from '../../shared/services/state.service';
import { ITask } from '../interfaces/task-interface';
import * as Actions from '../state/actions';
import * as Selectors from '../state/selectors';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';

export function getTodos() {
    store.dispatch(
        Actions.getTodosReq()
    );
}

export function addTodo(todo: ITask) {
    store.dispatch(
        Actions.addTodoReq(todo)
    )
}

export function deleteTodo(id: string) {
    store.dispatch(
        Actions.deleteTodoReq(id)
    )
}

export function updateTodo(todo: ITask) {
    store.dispatch(
        Actions.updateTodoReq(todo)
    )
}

export function getSearched(input: string) {
    store.dispatch(
        Actions.getSearched(input)
    )
}

export function getStatusList() {
    store.dispatch(
        Actions.getStatusReq()
    )
}

export const StatusListArray$ = () => store$.pipe(
    map(state => Selectors.STATUS_LIST(state)),
    skipWhile(arr => arr === undefined),
    distinctUntilChanged()
)

export const AlertStatus$ = () => store$.pipe(
    map(state => Selectors.ALERT_STATUS(state)),
    skipWhile(arr => arr === undefined),
    distinctUntilChanged()
)

export const TodosArray$ = () => store$.pipe(
    map(state => Selectors.TODOS_BY_STATUS(state)),
    skipWhile(todos => todos === undefined),
    distinctUntilChanged(),
);
