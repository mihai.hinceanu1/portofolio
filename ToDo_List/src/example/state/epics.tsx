import * as Actions from './actions';
import { AppState } from '../../shared/interfaces/app-state';
import { Action } from '../../shared/interfaces/shared';
import * as webApi from '../webapis/webapi';
import { Store } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, concatMap, map } from 'rxjs/operators';
import { ITask } from '../interfaces/task-interface';

export const getTodoArray$ = (action$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    action$.ofType(Actions.GET_TODOS_REQ).pipe(
        map(action => action.payload),
        concatMap(() => webApi.getTodo().pipe(
            map(arr => Actions.getTodosOk(arr)),
            catchError(err => of(Actions.getTodosFail(err)))
        )
        ));

export const getStatusList$ = (actionn$: ActionsObservable<Action<null>>, _store: Store<AppState>) =>
    actionn$.ofType(Actions.GET_STATUS_REQ).pipe(
        map(action => action.payload),
        concatMap(() => webApi.getStatus().pipe(
            map(arr => Actions.getStatusOk(arr)),
            catchError(err => of(Actions.getStatusFail(err)))
        ))
    );

export const addTodo$ = (action$: ActionsObservable<Action<ITask>>, _store: Store<AppState>) =>
    action$.ofType(Actions.ADD_TODO_REQ).pipe(
        map(action => action.payload),
        concatMap((todo) => webApi.addTodo(todo).pipe(
            map(arr => Actions.addTodoOk(arr)),
            catchError(err => of(Actions.addTodoFail(err)))
        ))
    );

export const deleteToto$ = (action$: ActionsObservable<Action<string>>, _store: Store<AppState>) =>
    action$.ofType(Actions.DELETE_TODO_REQ).pipe(
        map(action => action.payload),
        concatMap((id) => webApi.deleteTodo(id).pipe(
            map(id => Actions.deleteTodoOk(id)),
            catchError(err => of(Actions.deleteTodoFail(err)))
        ))
    );

export const updateTodo$ = (action$: ActionsObservable<Action<ITask>>, _store: Store<AppState>) =>
    action$.ofType(Actions.UPDATE_TODO_REQ).pipe(
        map(action => action.payload),
        concatMap((todo) => webApi.updateTodo(todo).pipe(
            map(todo => Actions.updateTodoOk(todo)),
            catchError(err => of(Actions.updateTodoFail(err)))
        ))
    );