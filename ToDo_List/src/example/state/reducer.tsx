import * as Actions from './actions';
import { initState } from './init-state';
import { IState } from '../interfaces/state-interface';
import { IStatusList } from '../interfaces/status-interface';
import { ITask } from '../interfaces/task-interface';

export const iReducer = (state: IState = initState, action: any): IState => {
    switch (action.type) {
        case Actions.GET_TODOS_OK: {
            return {
                ...state,
                tasks: action.payload as ITask[]
            };
        }

        case Actions.GET_TODOS_FAIL: {
            return {
                ...state,
                arrayError: { type: 'fail', message: 'Fail to load Tasks !!' },
            };
        }

        case Actions.GET_SEARCHED: {
            const { tasks } = state;
            let q = tasks.filter(task => task.title.includes(action.payload))
            return {
                ...state,
                searchArray: q as ITask[]
            }
        }

        case Actions.GET_STATUS_OK: {
            return {
                ...state,
                statusList: action.payload as IStatusList[]
            }
        }

        case Actions.GET_STATUS_FAIL: {
            return {
                ...state,
                arrayError: { type: 'fail', message: 'Fail to load Status !!' },
            }
        }

        case Actions.ADD_TODO_OK: {
            const q = [...state.tasks];
            q.unshift(action.payload);
            return {
                ...state,
                tasks: q as ITask[],
                arrayError: { type: 'ok', message: 'Insert successfully !!' },
            }
        }

        case Actions.ADD_TODO_FAIL: {
            return {
                ...state,
                arrayError: { type: 'fail', message: 'Fail to add new task !!' },
            }
        }

        case Actions.UPDATE_TODO_OK: {
            const { tasks } = state;
            const i = tasks.findIndex(task => task._id === action.payload._id)
            tasks[i] = action.payload
            return {
                ...state,
                tasks,
                arrayError: { type: 'ok', message: 'Update successfully !!' },
            }
        }

        case Actions.UPDATE_TODO_FAIL: {
            console.log('actions error: ', action.payload)
            return {
                ...state,
                arrayError: { type: 'fail', message: 'Fail to update task !!' },
            }
        }

        case Actions.DELETE_TODO_OK: {
            const { tasks } = state;
            const z = tasks.filter(task => task._id !== action.payload);
            return {
                ...state,
                tasks: z as ITask[],
                arrayError: { type: 'ok', message: 'Delete successfully !!' },
            }
        }

        case Actions.DELETE_TODO_FAIL: {
            return {
                ...state,
                arrayError: { type: 'fail', message: 'Fail to delete !!!' },
            };
        }

        default: {
            return state;
        }
    }
};
