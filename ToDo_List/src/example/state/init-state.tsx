import { IState } from '../interfaces/state-interface';
import { IStatusList } from '../interfaces/status-interface';
import { ITask } from '../interfaces/task-interface';
import { IError } from '../interfaces/error-interface';

export const initState: IState = {

    // all tasks error
    arrayError: {} as IError,

    // status list
    statusList: [] as IStatusList[],

    // search list
    searchArray: [] as ITask[],

    // all tasks
    tasks: [] as ITask[],

    // in progress tasks
    progress: [] as ITask[],

    // pending
    pending: [] as ITask[],

    // done
    done: [] as ITask[],

    // important
    important: [] as ITask[],
}