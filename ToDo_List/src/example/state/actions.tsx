import { Action } from '../../shared/interfaces/shared';
import { ITask } from '../interfaces/task-interface';

// actions for get todos from db
export const GET_TODOS_REQ = 'GET_TODOS_REQ';
export const getTodosReq = (): Action<null> => ({
    type: GET_TODOS_REQ,
    payload: null,
});

export const GET_TODOS_OK = 'GET_TODOS_OK';
export const getTodosOk = (tasks: ITask[]): Action<ITask[]> => ({
    type: GET_TODOS_OK,
    payload: tasks
});

export const GET_TODOS_FAIL = 'GET_TODOS_FAIL';
export const getTodosFail = (error: any): Action<any> => ({
    type: GET_TODOS_FAIL,
    payload: error
});

// actions for search
export const GET_SEARCHED = 'GET_SEARCHED';
export const getSearched = (input: string): Action<string> => ({
    type: GET_SEARCHED,
    payload: input
})

// actions for delete todo from db
export const DELETE_TODO_REQ = 'DELETE_TODO_REQ';
export const deleteTodoReq = (id: string): Action<string> => ({
    type: DELETE_TODO_REQ,
    payload: id
});

export const DELETE_TODO_OK = 'DELETE_TODO_OK';
export const deleteTodoOk = (id: string): Action<string> => ({
    type: DELETE_TODO_OK,
    payload: id
});

export const DELETE_TODO_FAIL = 'DELETE_TODO_FAIL';
export const deleteTodoFail = (error: any): Action<any> => ({
    type: DELETE_TODO_FAIL,
    payload: error
});

// actions for update a todo in db
export const UPDATE_TODO_REQ = 'UPDATE_TODO_REQ';
export const updateTodoReq = (todo: ITask): Action<ITask> => ({
    type: UPDATE_TODO_REQ,
    payload: todo
});

export const UPDATE_TODO_OK = 'UPDATE_TODO_OK';
export const updateTodoOk = (todo: ITask): Action<ITask> => ({
    type: UPDATE_TODO_OK,
    payload: todo
});

export const UPDATE_TODO_FAIL = 'UPDATE_TODO_FAIL';
export const updateTodoFail = (error: any): Action<any> => ({
    type: DELETE_TODO_FAIL,
    payload: error
});

// actions for adding a new todo in db
export const ADD_TODO_REQ = 'ADD_TODO_REQ';
export const addTodoReq = (todo: ITask): Action<ITask> => ({
    type: ADD_TODO_REQ,
    payload: todo
});

export const ADD_TODO_OK = 'ADD_TODO_OK';
export const addTodoOk = (todo: ITask): Action<ITask> => ({
    type: ADD_TODO_OK,
    payload: todo
});

export const ADD_TODO_FAIL = 'ADD_TODO_FAIL';
export const addTodoFail = (error: any): Action<any> => ({
    type: DELETE_TODO_FAIL,
    payload: error
});

// actions for get status list from db
export const GET_STATUS_REQ = 'GET_STATUS_REQ';
export const getStatusReq = (): Action<null> => ({
    type: GET_STATUS_REQ,
    payload: null,
});

export const GET_STATUS_OK = 'GET_STATUS_OK';
export const getStatusOk = (status: any): Action<any> => ({
    type: GET_STATUS_OK,
    payload: status
});

export const GET_STATUS_FAIL = 'GET_STATUS_FAIL';
export const getStatusFail = (error: any): Action<any> => ({
    type: GET_STATUS_FAIL,
    payload: error
});