import { AppState } from '../../shared/interfaces/app-state';
import { IState } from '../interfaces/state-interface';
import { IStatusList } from '../interfaces/status-interface';
import { ITask } from '../interfaces/task-interface';
import { createSelector, Selector } from 'reselect';
import { IError } from '../interfaces/error-interface';

const MODULE: Selector<AppState, IState> = (state: AppState) => state.todos;

export const STATUS_LIST = createSelector<AppState, IState, IStatusList[]>(
    MODULE,
    (state: IState) => state.statusList
);

export const ALERT_STATUS = createSelector<AppState, IState, IError>(
    MODULE,
    (state: IState) => state.arrayError
)

export const TODOS_BY_STATUS = createSelector<AppState, IState, any>(
    MODULE,
    (state: IState) => {
        let { pending, tasks, progress, important, done, searchArray } = state;

        pending = tasks.filter((task: ITask) => task.status === 'pending');
        progress = tasks.filter((task: ITask) => task.status === 'progress');
        done = tasks.filter((task: ITask) => task.status === 'done');
        important = tasks.filter((task: ITask) => task.isImportant === true);

        return { pending, tasks, progress, important, done, searchArray };
    }
);
