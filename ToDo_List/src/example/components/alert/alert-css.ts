import styled from 'styled-components/native';

export const Text = styled.Text`
    color: white;
`;

export const FailContainer = styled.View`
    align-items: center; 
    background-color: red; 
    width: 230px;
`;

export const SuccessContainer = styled.View`
    align-items: center; 
    background-color: lime; 
    width: 230px;
`;