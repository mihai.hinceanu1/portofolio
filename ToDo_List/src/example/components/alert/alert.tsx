import * as React from 'react';
import * as div from './alert-css';
import { IError } from '../../interfaces/error-interface';

interface State { };
interface Props {
    error: IError;
};

export class Alert extends React.Component<Props, State> {
    render() {
        const { error } = this.props;

        if (error.type === 'ok') {
            return (
                <div.SuccessContainer><div.Text>{error.message}</div.Text></div.SuccessContainer>
            )
        } else {
            return (
                <div.FailContainer><div.Text>{error.message}</div.Text></div.FailContainer>
            )
        }

    }
}