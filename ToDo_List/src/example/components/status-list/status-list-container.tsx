import * as React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { StatusList } from '../status-list/status-list';
import { IStatusList } from '../../interfaces/status-interface';

interface State {}
interface Props {
    statusList: IStatusList[];
    onFilter: (type: string, label: string) => void;
}

export class StatusListContainer extends React.Component<Props, State> {

    render() {
        return (
            <View style={{ marginLeft: 30, width: 140 }}>
                <Text style={styles.title}>To Do List</Text>
                <FlatList style={{ marginBottom: 30 }} data={this.props.statusList} renderItem={({ item }) =>
                    <StatusList
                        onPressItem={() => this.props.onFilter(item.code, item.label)}
                        label={item.label} value={item.value} code={item.code} />}
                    keyExtractor={(item) => item.code}></FlatList>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold', marginTop: 30, marginBottom: 20
    }
})