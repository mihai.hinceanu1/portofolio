import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Bundle } from '../status-list/bundle';

interface State { }
interface Props {
    code: string;
    label: string;
    value: number;
    onPressItem: (type: string, label: string) => void;
}

export class StatusList extends React.Component<Props, State> {
    render() {
        return (
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.props.onPressItem(this.props.code, this.props.label)}>
                <Text style={{ paddingVertical: 10, width: 110 }}>{this.props.label}</Text>
                <Bundle value={this.props.value} code={this.props.code}></Bundle>
            </TouchableOpacity>
        )
    };
}
