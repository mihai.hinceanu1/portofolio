import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';

interface State { }
interface Props {
    code: string;
    value: number;
}

export class Bundle extends React.Component<Props, State> {

    render() {
        return (
            <View style={{ justifyContent: 'center' }}>
                <Text style={this.getStyle(this.props.code)}>{this.props.value}</Text>
            </View>
        )
    }

    getStyle(code: string) {
        switch (code) {
            case 'all':
                return styles.allbundle;
            case "pending":
                return styles.pendingbundle;
            case 'progress':
                return styles.progressbundle;
            case 'done':
                return styles.donebundle;
            case 'important':
                return styles.flaggedbundle;
            default:
                break;
        }
    }
}

const styles = StyleSheet.create({
    allbundle: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 30,
        height: 20,
        textAlign: 'center'
    },
    pendingbundle: {
        backgroundColor: '#808080',
        borderRadius: 10,
        width: 30,
        height: 20,
        textAlign: 'center',
        color: 'white'
    },
    progressbundle: {
        backgroundColor: '#0d8ef1',
        borderRadius: 10,
        width: 30,
        height: 20,
        textAlign: 'center',
        color: 'white'
    },
    donebundle: {
        backgroundColor: '#00c3a7',
        borderRadius: 10,
        width: 30,
        height: 20,
        textAlign: 'center',
        color: 'white'
    },
    flaggedbundle: {
        backgroundColor: '#ea4335',
        borderRadius: 10,
        width: 30,
        height: 20,
        textAlign: 'center',
        color: 'white'
    }
})