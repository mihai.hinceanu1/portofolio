import * as React from 'react';
import { View } from 'react-native';
import * as div from '../body/task-css';
// import { ITask } from '../../interfaces/task-interface';

interface State {
    title: string;
    description: string;
};
interface Props {
    isOn: boolean;
    isFocused: (item: any) => void;
};

export class EditableTask extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            title: '',
            description: ''
        }
    }
    render() {
        return (
            <div.Popup>
                <div.ETaskContainer>
                    <div.EStatusIcon source={require('../../../assets/status_icons/in-progress.svg')}></div.EStatusIcon>
                    <div.ETask>
                        <View style={{ width: 395 }}>
                            <div.ETitle placeholder='To do title ...'
                                // autoFocus={this.props.isOn} 
                                onBlur={() => this.props.isFocused(this.state)}
                                onChangeText={(text) => this.setState({ title: text })}></div.ETitle>

                            <div.EDescription placeholder='To do description ...'
                                // autoFocus={this.props.isOn} 
                                onBlur={() => this.props.isFocused(this.state)}
                                onChangeText={(text) => this.setState({ description: text })}></div.EDescription>
                        </View>
                    </div.ETask>
                </div.ETaskContainer>
            </div.Popup>

        )
    }
}