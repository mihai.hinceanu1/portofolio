import styled from 'styled-components/native';
import div from 'styled-components';

export const hover = div.div`
    display: flex;
`;

export const TaskContainer = styled.View`
    margin-bottom: 30px;
    flex-direction: row;
`;

export const Popup = styled.View`
    position: absolute;
    height: 100vh;
    width: 100%;
    justify-content: center;
    background-color: #8f919280;
`;

export const ETaskContainer = styled.View`
    align-self: center;
    align-items: center;
    background-color: white;
    width: 500px;
    height: 150px;
    justify-content: center;
    flex-direction: row;
`;

export const StatusIcon = styled.Image`
    width: 16px;
    height: 16px;
    margin-top: 2px;
`;

export const EStatusIcon = styled.Image`
    width: 16px;
    height: 16px;
    margin-top: 2px;
`;

export const Task = styled.View`
    margin-left: 10px;
    flex-direction: row;
`;

export const ETask = styled.View`
    margin-left: 10px;
    flex-direction: row;
`;

export const ETitle = styled.TextInput`
    border-bottom-width: 1px;
    border-bottom-color: #e2e2e2;
    border-bottom-style: solid;
    height: 25px;
`;

export const Description = styled.Text`
    font-size: 12px;
    color: #a8a8a8;
    margin-top: 10px;
`;

export const EDescription = styled.TextInput`
    border-bottom-width: 1px;
    border-bottom-color: #e2e2e2;
    border-bottom-style: solid;
    height: 25px;
`;

export const Actions = styled.View`
    align-item: center;
    flex-direction: row;
`;

export const Important = styled.Image`
    width: 16px; 
    height: 17px; 
    margin-left: 5px;
`;

export const Remove = styled.Image`
    width: 16px; 
    height: 17px; 
    margin-left: 15px;
`;