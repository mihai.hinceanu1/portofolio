import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import * as div from '../body/task-css';
import { ITask } from '../../interfaces/task-interface';

interface State {
    isVisible: boolean;
};
interface Props {
    taskProps: ITask,
    deleteTask: () => void,
    makeImportant: () => void,
    changeTaskStatus: () => void;
};

export class Task extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { isVisible: false }
    }

    render() {
        const { taskProps } = this.props;
        const { isVisible } = this.state;
        return (
            // <div.hover onMous></div.hover>
            <div.hover onMouseEnter={() => this.setState({ isVisible: true })} onMouseLeave={() => this.setState({ isVisible: false })}>
                <div.TaskContainer>
                    <TouchableOpacity onPress={() => this.props.changeTaskStatus()}>
                        <div.StatusIcon
                            source={this.getIcon(taskProps.status)}
                        ></div.StatusIcon>
                    </TouchableOpacity>
                    <div.Task>
                        <View style={{ width: 395 }}>
                            <Text>{taskProps.title}</Text>
                            <div.Description>{taskProps.description}</div.Description>
                        </View>

                        <div.Actions>
                            {(isVisible === true || taskProps.isImportant === true) &&
                                <TouchableOpacity onPress={() => this.props.makeImportant()}>
                                    <div.Important source={this.getImportance(taskProps.isImportant)}></div.Important>
                                </TouchableOpacity>
                            }
                            {(isVisible === true) &&
                                <TouchableOpacity onPress={() => this.props.deleteTask()}>
                                    <div.Remove source={require('../../../assets/status_icons/trash.svg')}></div.Remove>
                                </TouchableOpacity>
                            }
                        </div.Actions>
                    </div.Task>
                </div.TaskContainer>
            </div.hover >
        )
    }

    getImportance(status: boolean) {
        if (status === true) {
            return require('../../../assets/status_icons/iflag.svg')
        } else {
            return require('../../../assets/status_icons/flag.svg')
        }
    }

    getIcon(status: string) {
        switch (status) {
            case 'progress':
                return require('../../../assets/status_icons/in-progress.svg');
            case 'done':
                return require('../../../assets/status_icons/done.svg');
            default:
                return require('../../../assets/status_icons/empty.svg');
        }
    }
}
