import { ITask } from '../../interfaces/task-interface';
import * as div from '../body/body-css';
import { Task } from '../body/task';
import * as React from 'react';
import { FlatList, View } from 'react-native';

interface State { };
interface Props {
    tasks: ITask[],
    title: string,
    deleteTask: (id: string) => void
    makeImportant: (task: ITask) => void
    changeTaskStatus: (task: ITask) => void;
};

export class Body extends React.Component<Props, State> {
    render() {
        const { tasks } = this.props;

        return (
            <div.ToDoListContainer>
                {tasks.length === 0 ?
                    <div.NoTodos>
                        <div.ErrorMsg>No tasks found !!</div.ErrorMsg>
                    </div.NoTodos>
                    :
                    <View>
                        <div.Title>{this.props.title}</div.Title>

                        <div.FlatList>
                            <FlatList data={this.props.tasks} renderItem={({ item }) =>

                                <Task deleteTask={() => this.props.deleteTask(item._id)}
                                    makeImportant={() => this.props.makeImportant(item)}
                                    changeTaskStatus={() => this.props.changeTaskStatus(item)}
                                    taskProps={item}></Task>}>
                            </FlatList>
                        </div.FlatList>
                    </View>
                }
            </div.ToDoListContainer>
        )
    }

}