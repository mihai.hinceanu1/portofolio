import styled from 'styled-components/native';

export const ToDoListContainer = styled.View`
    margin-left: 40px;
    width: 510px;
    height: calc(100vh - 130px);
    
`;

export const Title = styled.Text`
    font-weight: bold;
    margin-top: 30px;
    margin-bottom: 40px;
`;

export const FlatList = styled.View`
    height: calc(100vh - 218px);
`;

export const NoTodos = styled.View`
    width: 470px;
    height: inherit;
    justify-content: center;
    align-items: center;
`;

export const ErrorMsg = styled.Text`
    font-size: medium;
    color: darkgrey;
`;