import * as React from 'react';
import * as div from './search-box-css';

interface State { };
interface Props {
    onSearch: (target: string) => void;
 };

export class SearchBox extends React.Component<Props, State> {
    render() {
        return (
            <div.SearchBoxContainer>
                <div.Icon source={require('../../../assets/search.svg')}></div.Icon>
                <div.Input placeholder="Search for a to do" onChangeText={(target) => this.props.onSearch(target)}></div.Input>
            </div.SearchBoxContainer>
        )
    }
}