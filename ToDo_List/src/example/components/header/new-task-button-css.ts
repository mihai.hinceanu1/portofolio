import styled from 'styled-components/native';

export const Button = styled.View`
    width: 200px; 
    background-color: #2196f3; 
    border-radius: 20px; 
    height: 35px; 
    justify-content: center; 
    text-align: center;
    shadow-color: black;
    shadowOpacity: 0.2;
    shadow-radius: 20px;
`;

export const TextBtn = styled.Text`
    font-family: sans-serif;
    font-weight: 500;
    color: white;
`;
