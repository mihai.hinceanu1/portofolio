import * as React from 'react';
import * as div from './new-task-button-css';

interface State { };
interface Props {
    onAddNewTask: () => void;
 };

export class NewTaskButton extends React.Component<Props, State> {
    render() {
        return (
            <div.Button>
                <div.TextBtn onPress={() => this.props.onAddNewTask()}>+ NEW TASK</div.TextBtn>
            </div.Button>
        )
    }
}