import styled from 'styled-components/native';

export const SearchBoxContainer = styled.View`
    flex-direction: row;
    align-items: center;
    background-color: white;
    border-radius: 20px;
    width: 320px;
    height: 35px;
    margin-left: 30px;
    padding-left: 5px;
    shadow-color: black;
    shadowOpacity: 0.2;
    shadow-radius: 20px;
`;

export const Icon = styled.Image`
    padding: 10px;
    margin: 5px;
    width: 25px;
    height: 25px;
    resize-mode: stretch;
    align-items: center;
    color: #2196f3;
`;
 export const Input = styled.TextInput`
    padding-top: 10px;
    padding-right: 10px;
    padding-bottom: 10px;
    backgroud-color: white;
    color: darkgrey;
    height: 35px;
    width: 250px;
 `;