import { ITask } from '../interfaces/task-interface';
import axios from 'axios';
import { from, Observable } from 'rxjs';

export const addUser = (data: any): Observable<any> => from<Promise<any>>(
    axios.post('http://localhost:8080/api/User', data)
)

export const getUser = (): Observable<any> => from<Promise<any>>(
    axios.get('http://localhost:8080/api/User').then(res => res.data)
)

export const addTodo = (data: any): Observable<any> => from<Promise<any>>(
    axios.post('http://localhost:8080/api/Todos', data).then(res => res.data)
)

export const getTodo = (): Observable<ITask[]> => from<Promise<ITask[]>>(
    axios.get('http://localhost:8080/api/Todos').then(res => res.data)
)

export const deleteTodo = (id: string): Observable<string> => from<Promise<string>>(
    axios.delete('http://localhost:8080/api/Todos/'+ id).then(res => res.data)
)

export const updateTodo = (task: ITask): Observable<ITask> => from<Promise<ITask>>(
    axios.post('http://localhost:8080/api/updateTodo/'+ task._id, task)
)

export const getStatus = (): Observable<any> => from<Promise<any>>(
    axios.get('http://localhost:8080/api/Status').then(res => res.data)
)