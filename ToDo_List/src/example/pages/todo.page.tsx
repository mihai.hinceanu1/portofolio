import * as div from './todo-css';
import { Body } from '../components/body/body';
import { EditableTask } from '../components/body/editable-task';
import { NewTaskButton } from '../components/header/new-task-button';
import { SearchBox } from '../components/header/search-box';
import { StatusListContainer } from '../components/status-list/status-list-container';
import { Alert } from '../components/alert/alert';
import { IStatusList } from '../interfaces/status-interface';
import { ITask } from '../interfaces/task-interface';
import { IError } from '../interfaces/error-interface';
import * as React from 'react';
import { View } from 'react-native';
import { RouteComponentProps, withRouter } from 'react-router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
    getTodos,
    addTodo,
    updateTodo,
    deleteTodo,
    getSearched,
    getStatusList,
    TodosArray$,
    StatusListArray$,
    AlertStatus$
} from '../services/data-service';


interface Props extends RouteComponentProps { }

interface State {
    tasks: ITask[];
    pending: ITask[];
    progress: ITask[];
    done: ITask[];
    important: ITask[];
    searchArray: ITask[];
    arrayError: IError;
    statusList: IStatusList[];
    title: string;
    currentPage: string;
    warning: boolean;
    success: boolean;
    editable: boolean;
}

class _ToDo extends React.Component<Props, State> {
    private destroyed$ = new Subject<void>();

    constructor(props: Props) {
        super(props);

        this.state = {
            tasks: [],
            pending: [],
            progress: [],
            done: [],
            important: [],
            searchArray: [],
            arrayError: { type: null, message: null },
            statusList: [],
            title: 'All',
            currentPage: 'all',
            warning: false,
            success: false,
            editable: false,
        };
    }

    public componentDidMount() {
        this.subscribeToTodoArray();
        getTodos();
        this.subscribeToStatusList();
        getStatusList();
        this.subscribeToAlertStatus();
    }

    public render() {

        let { statusList, tasks, pending, progress, done, important, editable, title, currentPage, arrayError, searchArray } = this.state;

        // overwrite status value with array length
        statusList.filter(q => q.code === 'all').map(arr => arr.value = tasks.length)
        statusList.filter(q => q.code === 'pending').map(arr => arr.value = pending.length)
        statusList.filter(q => q.code === 'progress').map(arr => arr.value = progress.length)
        statusList.filter(q => q.code === 'done').map(arr => arr.value = done.length)
        statusList.filter(q => q.code === 'important').map(arr => arr.value = important.length)

        let source = [] as ITask[];
        switch (currentPage) {
            case 'all':
                source = tasks;
                break;
            case 'pending':
                source = pending;
                break;
            case 'progress':
                source = progress;
                break;
            case 'done':
                source = done;
                break;
            case 'important':
                source = important;
                break;
            case 'search':
                source = searchArray;

        }

        return (
            <View>
                <div.HeaderContainer>
                    <div.HeaderContainerLB></div.HeaderContainerLB>
                    <div.HeaderContainerRB>
                        <NewTaskButton onAddNewTask={() => this.onAddNewTask()}></NewTaskButton>
                        <SearchBox onSearch={(value) => this.searchTask(value)}></SearchBox>
                    </div.HeaderContainerRB>
                </div.HeaderContainer>

                <div.BodyContainer>
                    <div.BodyContainerLB>
                        <StatusListContainer
                            statusList={statusList}
                            onFilter={(type: string, label: string) => this.getFilteredState(type, label)}
                        ></StatusListContainer>

                        {(this.state.warning === true) &&
                            <Alert error={arrayError}></Alert>
                        }

                        {(this.state.success === true) &&
                            <Alert error={arrayError}></Alert>
                        }
                    </div.BodyContainerLB>

                    <div.BodyContainerRB>
                        <Body tasks={source} title={title}
                            deleteTask={(id: string) => this.deleteTask(id)}
                            makeImportant={(task: ITask) => this.editTask('imp', task)}
                            changeTaskStatus={(task: ITask) => this.editTask('sts', task)}></Body>
                    </div.BodyContainerRB>
                </div.BodyContainer>


                {(editable === true) &&
                    <EditableTask isOn={editable} isFocused={(item: any) => this.focusState(item)}></EditableTask>
                }
            </View >
        );

    }

    private getFilteredState(currentPage: string, label: string) {
        this.setState({ currentPage: currentPage, title: label });
    }

    private searchTask(input: string) {        
        if (input.length === 0)  {
            this.setState({ currentPage: 'all', title: 'All'})
        } else {
            getSearched(input);
            this.setState({ currentPage: 'search', title: 'Searching ...'});
        }
    }

    private focusState(item: any) {
        if (item.title.length <= 0 && item.description.length <= 0) this.setState({ editable: false })
        if (item.title.length > 0 && item.description.length > 0) {
            addTodo(item);
            // this.populateTasks();

            this.setState({ editable: false });
            this.alertStatus();
        }
    }

    private onAddNewTask() {
        this.setState({ editable: true })
    }

    private editTask(action: string, task: ITask) {

        switch (action) {
            case 'imp':
                const how = task.isImportant;
                (how === true) ? task.isImportant = false : task.isImportant = true
                updateTodo(task);
                break;

            case 'sts':
                if (window.confirm(`Are you sure you want to change status for ${task.title}?`) === false) return;
                const sts = task.status;
                switch (sts) {
                    case 'pending':
                        task.status = 'progress'
                        updateTodo(task);
                        break;
                    case 'progress':
                        task.status = 'done'
                        updateTodo(task);
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }
    }

    private deleteTask(id: string) {
        if (window.confirm('Are you sure you want to delete this task?') === false) return;

        deleteTodo(id);
        this.alertStatus()
    }

    private subscribeToTodoArray() {
        TodosArray$().subscribe(obj => {
            this.setState({
                tasks: obj.tasks,
                pending: obj.pending,
                progress: obj.progress,
                done: obj.done,
                important: obj.important,
                searchArray: obj.searchArray
            });
        });
    }

    private subscribeToAlertStatus() {
        AlertStatus$().pipe(
            takeUntil(this.destroyed$)
        )
            .subscribe(error => {
                this.setState({ arrayError: error })
                this.alertStatus();
            })
    }

    private subscribeToStatusList() {
        StatusListArray$().pipe(
            takeUntil(this.destroyed$),
        )
            .subscribe(arr => {
                this.setState({ statusList: arr });
            })
    }

    private alertStatus() {
        if (this.state.arrayError.type === 'fail') {
            this.setState({ warning: true })
            setTimeout(() => {
                this.setState({ warning: false, arrayError: { type: null, message: null } })
            }, 3000);
        } else {
            this.setState({ success: true })
            setTimeout(() => {
                this.setState({ success: false, arrayError: { type: null, message: null } })
            }, 3000);
        }
    }

    // *** This function populate todos in database
    // private populateTasks() {
    //     for (let i = 1; i < 21; i++) {
    //         let obj: ITask = {_id: '', status: '', title: '', description: '', isImportant: false, date: new Date()};
    //         const arr = ['pending', 'progress', 'done']
    //         obj.status = arr[Math.floor(Math.random() * arr.length)];
    //         obj.title = 'New task ' + i;
    //         obj.description = 'This is a long text as description ' + i;
    //         obj.isImportant = false;
    //         addTodo(obj);
    //     }
    // }

}

export const ToDo = withRouter<Props, any>(_ToDo);
