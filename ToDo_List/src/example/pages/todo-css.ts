import styled from 'styled-components/native';

export const HeaderContainer = styled.View`
    flex-direction: row;
    justify-content: center; 
    height: 100px;
`;

export const HeaderContainerLB = styled.View`
    width: 230px;
    marginHorizontal: 20px;
`;

export const HeaderContainerRB = styled.View`
    width: 550px;
    marginHorizontal: 20px; 
    flexDirection: row;
    alignItems: center;
`;

export const BodyContainer = styled.View`
    flexDirection: row;
    justifyContent: center;
`;

export const BodyContainerLB = styled.View`
    width: 230px; 
    height: fit-content; 
    shadowColor: black; 
    shadowOffset: { width: 2, height: 2, };
    shadowOpacity: 0.25; 
    shadowRadius: 2px; 
    marginHorizontal: 20px; 
    backgroundColor: white;
`;

export const BodyContainerRB = styled.View`
    width: 550px;
    height: calc(100vh - 130px);
    shadowColor: black;
    shadowOffset: { width: 1, height: 1, };
    shadowOpacity: 0.25; 
    shadowRadius: 2px; 
    marginHorizontal: 20px;
    backgroundColor: white;
    
`;